#ifndef soofea_h___
#define soofea_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007  Michael Hammer and Manfred Ulz
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include "soofea_config.h"

#ifdef SOOFEA_HAVE_NAMESPACES
    #define SOOFEA_NAMESPACE(X)        namespace X {
    #define SOOFEA_NAMESPACE_END       }
    #define SOOFEA_USING_NAMESPACE(X)  using namespace X;
#else
    #define SOOFEA_NAMESPACE(X)
    #define SOOFEA_NAMESPACE_END
    #define SOOFEA_USING_NAMESPACE(X)
#endif

#endif // soofea_h___
