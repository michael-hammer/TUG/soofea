#ifndef lapack_h__
#define lapack_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

extern "C"
{
  //------------------------------------------------------------------
  //  Purpose                                                                     
  //  =======                                                                     
  //                                                                              
  //  DGETRF computes an LU factorization of a general M-by-N matrix A            
  //  using partial pivoting with row interchanges.                               
  //                                                                              
  //  The factorization has the form                                              
  //     A = P * L * U                                                            
  //  where P is a permutation matrix, L is lower triangular with unit            
  //  diagonal elements (lower trapezoidal if m > n), and U is upper              
  //  triangular (upper trapezoidal if m < n).                                    
  //                                                                              
  //  This is the right-looking Level 3 BLAS version of the algorithm.            
  //                                                                              
  //  Arguments                                                                   
  //  =========                                                                   
  //                                                                              
  //  M       (input) INTEGER                                                     
  //          The number of rows of the matrix A.  M >= 0.                        
  //                                                                              
  //  N       (input) INTEGER                                                     
  //          The number of columns of the matrix A.  N >= 0.                     
  //                                                                              
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)            
  //          On entry, the M-by-N matrix to be factored.                         
  //          On exit, the factors L and U from the factorization                 
  //          A = P*L*U; the unit diagonal elements of L are not stored.          
  //                                                                              
  //  LDA     (input) INTEGER                                                     
  //          The leading dimension of the array A.  LDA >= max(1,M).             
  //                                                                              
  //  IPIV    (output) INTEGER array, dimension (min(M,N))                        
  //          The pivot indices; for 1 <= i <= min(M,N), row i of the             
  //          matrix was interchanged with row IPIV(i).                           
  //                                                                              
  //  INFO    (output) INTEGER                                                    
  //          = 0:  successful exit                                               
  //          < 0:  if INFO = -i, the i-th argument had an illegal value          
  //          > 0:  if INFO = i, U(i,i) is exactly zero. The factorization        
  //                has been completed, but the factor U is exactly               
  //                singular, and division by zero will occur if it is used       
  //                to solve a system of equations.                               
  //                                                                              
  //  =====================================================================
  //
  extern void dgetrf_(int *m, int *n, double *A, int *lda, int *ipiv, int *info);

  //------------------------------------------------------------------
  //  Purpose
  //  =======
  //
  //  DPOSVX uses the Cholesky factorization A = U**T*U or A = L*L**T to
  //  compute the solution to a real system of linear equations
  //     A * X = B,
  //  where A is an N-by-N symmetric positive definite matrix and X and B
  //  are N-by-NRHS matrices.
  //
  //  Error bounds on the solution and a condition estimate are also
  //  provided.
  //
  //  Description
  //  ===========
  //
  //  The following steps are performed:
  //
  //  1. If FACT = 'E', real scaling factors are computed to equilibrate
  //     the system:
  //        diag(S) * A * diag(S) * inv(diag(S)) * X = diag(S) * B
  //     Whether or not the system will be equilibrated depends on the
  //     scaling of the matrix A, but if equilibration is used, A is
  //     overwritten by diag(S)*A*diag(S) and B by diag(S)*B.
  //
  //  2. If FACT = 'N' or 'E', the Cholesky decomposition is used to
  //     factor the matrix A (after equilibration if FACT = 'E') as
  //        A = U**T* U,  if UPLO = 'U', or
  //        A = L * L**T,  if UPLO = 'L',
  //     where U is an upper triangular matrix and L is a lower triangular
  //     matrix.
  //
  //  3. If the leading i-by-i principal minor is not positive definite,
  //     then the routine returns with INFO = i. Otherwise, the factored
  //     form of A is used to estimate the condition number of the matrix
  //     A.  If the reciprocal of the condition number is less than machine
  //     precision, INFO = N+1 is returned as a warning, but the routine
  //     still goes on to solve for X and compute error bounds as
  //     described below.
  //
  //  4. The system of equations is solved for X using the factored form
  //     of A.
  //
  //  5. Iterative refinement is applied to improve the computed solution
  //     matrix and calculate error bounds and backward error estimates
  //     for it.
  //
  //  6. If equilibration was used, the matrix X is premultiplied by
  //     diag(S) so that it solves the original system before
  //     equilibration.
  //
  //  Arguments
  //  =========
  //
  //  FACT    (input) CHARACTER*1
  //          Specifies whether or not the factored form of the matrix A is
  //          supplied on entry, and if not, whether the matrix A should be
  //          equilibrated before it is factored.
  //          = 'F':  On entry, AF contains the factored form of A.
  //                  If EQUED = 'Y', the matrix A has been equilibrated
  //                  with scaling factors given by S.  A and AF will not
  //                  be modified.
  //          = 'N':  The matrix A will be copied to AF and factored.
  //          = 'E':  The matrix A will be equilibrated if necessary, then
  //                  copied to AF and factored.
  //
  //  UPLO    (input) CHARACTER*1
  //          = 'U':  Upper triangle of A is stored;
  //          = 'L':  Lower triangle of A is stored.
  //
  //  N       (input) INTEGER
  //          The number of linear equations, i.e., the order of the
  //          matrix A.  N >= 0.
  //
  //  NRHS    (input) INTEGER
  //          The number of right hand sides, i.e., the number of columns
  //          of the matrices B and X.  NRHS >= 0.
  //
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the symmetric matrix A, except if FACT = 'F' and
  //          EQUED = 'Y', then A must contain the equilibrated matrix
  //          diag(S)*A*diag(S).  If UPLO = 'U', the leading
  //          N-by-N upper triangular part of A contains the upper
  //          triangular part of the matrix A, and the strictly lower
  //          triangular part of A is not referenced.  If UPLO = 'L', the
  //          leading N-by-N lower triangular part of A contains the lower
  //          triangular part of the matrix A, and the strictly upper
  //          triangular part of A is not referenced.  A is not modified if
  //          FACT = 'F' or 'N', or if FACT = 'E' and EQUED = 'N' on exit.
  //
  //          On exit, if FACT = 'E' and EQUED = 'Y', A is overwritten by
  //          diag(S)*A*diag(S).
  //
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  //
  //  AF      (input or output) DOUBLE PRECISION array, dimension (LDAF,N)
  //          If FACT = 'F', then AF is an input argument and on entry
  //          contains the triangular factor U or L from the Cholesky
  //          factorization A = U**T*U or A = L*L**T, in the same storage
  //          format as A.  If EQUED .ne. 'N', then AF is the factored form
  //          of the equilibrated matrix diag(S)*A*diag(S).
  //
  //          If FACT = 'N', then AF is an output argument and on exit
  //          returns the triangular factor U or L from the Cholesky
  //          factorization A = U**T*U or A = L*L**T of the original
  //          matrix A.
  //
  //          If FACT = 'E', then AF is an output argument and on exit
  //          returns the triangular factor U or L from the Cholesky
  //          factorization A = U**T*U or A = L*L**T of the equilibrated
  //          matrix A (see the description of A for the form of the
  //          equilibrated matrix).
  //
  //  LDAF    (input) INTEGER
  //          The leading dimension of the array AF.  LDAF >= max(1,N).
  //
  //  EQUED   (input or output) CHARACTER*1
  //          Specifies the form of equilibration that was done.
  //          = 'N':  No equilibration (always true if FACT = 'N').
  //          = 'Y':  Equilibration was done, i.e., A has been replaced by
  //                  diag(S) * A * diag(S).
  //          EQUED is an input argument if FACT = 'F'; otherwise, it is an
  //          output argument.
  //
  //  S       (input or output) DOUBLE PRECISION array, dimension (N)
  //          The scale factors for A; not accessed if EQUED = 'N'.  S is
  //          an input argument if FACT = 'F'; otherwise, S is an output
  //          argument.  If FACT = 'F' and EQUED = 'Y', each element of S
  //          must be positive.
  //
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  //          On entry, the N-by-NRHS right hand side matrix B.
  //          On exit, if EQUED = 'N', B is not modified; if EQUED = 'Y',
  //          B is overwritten by diag(S) * B.
  //
  //  LDB     (input) INTEGER
  //          The leading dimension of the array B.  LDB >= max(1,N).
  //
  //  X       (output) DOUBLE PRECISION array, dimension (LDX,NRHS)
  //          If INFO = 0 or INFO = N+1, the N-by-NRHS solution matrix X to
  //          the original system of equations.  Note that if EQUED = 'Y',
  //          A and B are modified on exit, and the solution to the
  //          equilibrated system is inv(diag(S))*X.
  //
  //  LDX     (input) INTEGER
  //          The leading dimension of the array X.  LDX >= max(1,N).
  //
  //  RCOND   (output) DOUBLE PRECISION
  //          The estimate of the reciprocal condition number of the matrix
  //          A after equilibration (if done).  If RCOND is less than the
  //          machine precision (in particular, if RCOND = 0), the matrix
  //          is singular to working precision.  This condition is
  //          indicated by a return code of INFO > 0.
  //
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The estimated forward error bound for each solution vector
  //          X(j) (the j-th column of the solution matrix X).
  //          If XTRUE is the true solution corresponding to X(j), FERR(j)
  //          is an estimated upper bound for the magnitude of the largest
  //          element in (X(j) - XTRUE) divided by the magnitude of the
  //          largest element in X(j).  The estimate is as reliable as
  //          the estimate for RCOND, and is almost always a slight
  //          overestimate of the true error.
  //
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)
  //          The componentwise relative backward error of each solution
  //          vector X(j) (i.e., the smallest relative change in
  //          any element of A or B that makes X(j) an exact solution).
  //
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (3*N)
  //
  //  IWORK   (workspace) INTEGER array, dimension (N)
  //
  //  INFO    (output) INTEGER
  //          = 0: successful exit
  //          < 0: if INFO = -i, the i-th argument had an illegal value
  //          > 0: if INFO = i, and i is
  //                <= N:  the leading minor of order i of A is
  //                       not positive definite, so the factorization
  //                       could not be completed, and the solution has not
  //                       been computed. RCOND = 0 is returned.
  //                = N+1: U is nonsingular, but RCOND is less than machine
  //                       precision, meaning that the matrix is singular
  //                       to working precision.  Nevertheless, the
  //                       solution and error bounds are computed because
  //                       there are a number of situations where the
  //                       computed solution can be more accurate than the
  //                       value of RCOND would suggest.
  //
  //  =====================================================================
  //
  extern void dposvx_( char* fact, char* uplo, int* n, int* nrhs, double* a, 
		       int *lda, double* af, int *ldaf, char *equed,
		       double *s, double *b, int *ldb, double *x, int *ldx, 
		       double *rcond, double* ferr, double *berr, double *work,
		       int *iwork, int *info );

  //------------------------------------------------------------------
  //  Purpose   
  //  =======   
  //
  //  DGESVX uses the LU factorization to compute the solution to a real   
  //  system of linear equations   
  //     A * X = B,   
  //  where A is an N-by-N matrix and X and B are N-by-NRHS matrices.   
  //
  //  Error bounds on the solution and a condition estimate are also   
  //  provided.   
  //
  //  Description   
  //  ===========   
  //
  //  The following steps are performed:   
  //
  //  1. If FACT = 'E', real scaling factors are computed to equilibrate   
  //     the system:   
  //        TRANS = 'N':  diag(R)*A*diag(C)     *inv(diag(C))*X = diag(R)*B   
  //        TRANS = 'T': (diag(R)*A*diag(C))**T *inv(diag(R))*X = diag(C)*B   
  //        TRANS = 'C': (diag(R)*A*diag(C))**H *inv(diag(R))*X = diag(C)*B   
  //     Whether or not the system will be equilibrated depends on the   
  //     scaling of the matrix A, but if equilibration is used, A is   
  //     overwritten by diag(R)*A*diag(C) and B by diag(R)*B (if TRANS='N')   
  //     or diag(C)*B (if TRANS = 'T' or 'C').   
  //
  //  2. If FACT = 'N' or 'E', the LU decomposition is used to factor the   
  //     matrix A (after equilibration if FACT = 'E') as   
  //        A = P * L * U,   
  //     where P is a permutation matrix, L is a unit lower triangular   
  //     matrix, and U is upper triangular.   
  //
  //  3. If some U(i,i)=0, so that U is exactly singular, then the routine   
  //     returns with INFO = i. Otherwise, the factored form of A is used   
  //     to estimate the condition number of the matrix A.  If the   
  //     reciprocal of the condition number is less than machine precision,   
  //     INFO = N+1 is returned as a warning, but the routine still goes on   
  //     to solve for X and compute error bounds as described below.   
  //
  //  4. The system of equations is solved for X using the factored form   
  //     of A.   
  //
  //  5. Iterative refinement is applied to improve the computed solution   
  //     matrix and calculate error bounds and backward error estimates   
  //     for it.   
  //
  //  6. If equilibration was used, the matrix X is premultiplied by   
  //     diag(C) (if TRANS = 'N') or diag(R) (if TRANS = 'T' or 'C') so   
  //     that it solves the original system before equilibration.   
  //
  //  Arguments   
  //  =========   
  //
  //  FACT    (input) CHARACTER*1   
  //          Specifies whether or not the factored form of the matrix A is   
  //          supplied on entry, and if not, whether the matrix A should be   
  //          equilibrated before it is factored.   
  //          = 'F':  On entry, AF and IPIV contain the factored form of A.   
  //                  If EQUED is not 'N', the matrix A has been   
  //                  equilibrated with scaling factors given by R and C.   
  //                  A, AF, and IPIV are not modified.   
  //          = 'N':  The matrix A will be copied to AF and factored.   
  //          = 'E':  The matrix A will be equilibrated if necessary, then   
  //                  copied to AF and factored.   
  //
  //  TRANS   (input) CHARACTER*1   
  //          Specifies the form of the system of equations:   
  //          = 'N':  A * X = B     (No transpose)   
  //          = 'T':  A**T * X = B  (Transpose)   
  //          = 'C':  A**H * X = B  (Transpose)   
  //
  //  N       (input) INTEGER   
  //          The number of linear equations, i.e., the order of the   
  //          matrix A.  N >= 0.   
  //
  //  NRHS    (input) INTEGER   
  //          The number of right hand sides, i.e., the number of columns   
  //          of the matrices B and X.  NRHS >= 0.   
  //
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)   
  //          On entry, the N-by-N matrix A.  If FACT = 'F' and EQUED is   
  //          not 'N', then A must have been equilibrated by the scaling   
  //          factors in R and/or C.  A is not modified if FACT = 'F' or   
  //          'N', or if FACT = 'E' and EQUED = 'N' on exit.   
  //
  //          On exit, if EQUED .ne. 'N', A is scaled as follows:   
  //          EQUED = 'R':  A := diag(R) * A   
  //          EQUED = 'C':  A := A * diag(C)   
  //          EQUED = 'B':  A := diag(R) * A * diag(C).   
  //
  //  LDA     (input) INTEGER   
  //          The leading dimension of the array A.  LDA >= max(1,N).   
  //
  //  AF      (input or output) DOUBLE PRECISION array, dimension (LDAF,N)   
  //          If FACT = 'F', then AF is an input argument and on entry   
  //          contains the factors L and U from the factorization   
  //          A = P*L*U as computed by DGETRF.  If EQUED .ne. 'N', then   
  //          AF is the factored form of the equilibrated matrix A.   
  //
  //          If FACT = 'N', then AF is an output argument and on exit   
  //          returns the factors L and U from the factorization A = P*L*U   
  //          of the original matrix A.   
  //
  //          If FACT = 'E', then AF is an output argument and on exit   
  //          returns the factors L and U from the factorization A = P*L*U   
  //          of the equilibrated matrix A (see the description of A for   
  //          the form of the equilibrated matrix).   
  //
  //  LDAF    (input) INTEGER   
  //          The leading dimension of the array AF.  LDAF >= max(1,N).   
  //
  //  IPIV    (input or output) INTEGER array, dimension (N)   
  //          If FACT = 'F', then IPIV is an input argument and on entry   
  //          contains the pivot indices from the factorization A = P*L*U   
  //          as computed by DGETRF; row i of the matrix was interchanged   
  //          with row IPIV(i).   
  //
  //          If FACT = 'N', then IPIV is an output argument and on exit   
  //          contains the pivot indices from the factorization A = P*L*U   
  //          of the original matrix A.   
  //
  //          If FACT = 'E', then IPIV is an output argument and on exit   
  //          contains the pivot indices from the factorization A = P*L*U   
  //          of the equilibrated matrix A.   
  //
  //  EQUED   (input or output) CHARACTER*1   
  //          Specifies the form of equilibration that was done.   
  //          = 'N':  No equilibration (always true if FACT = 'N').   
  //          = 'R':  Row equilibration, i.e., A has been premultiplied by   
  //                  diag(R).   
  //          = 'C':  Column equilibration, i.e., A has been postmultiplied   
  //                  by diag(C).   
  //          = 'B':  Both row and column equilibration, i.e., A has been   
  //                  replaced by diag(R) * A * diag(C).   
  //          EQUED is an input argument if FACT = 'F'; otherwise, it is an   
  //          output argument.   
  //
  //  R       (input or output) DOUBLE PRECISION array, dimension (N)   
  //          The row scale factors for A.  If EQUED = 'R' or 'B', A is   
  //          multiplied on the left by diag(R); if EQUED = 'N' or 'C', R   
  //          is not accessed.  R is an input argument if FACT = 'F';   
  //          otherwise, R is an output argument.  If FACT = 'F' and   
  //          EQUED = 'R' or 'B', each element of R must be positive.   
  //
  //  C       (input or output) DOUBLE PRECISION array, dimension (N)   
  //          The column scale factors for A.  If EQUED = 'C' or 'B', A is   
  //          multiplied on the right by diag(C); if EQUED = 'N' or 'R', C   
  //          is not accessed.  C is an input argument if FACT = 'F';   
  //          otherwise, C is an output argument.  If FACT = 'F' and   
  //          EQUED = 'C' or 'B', each element of C must be positive.   
  //
  //  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)   
  //          On entry, the N-by-NRHS right hand side matrix B.   
  //          On exit,   
  //          if EQUED = 'N', B is not modified;   
  //          if TRANS = 'N' and EQUED = 'R' or 'B', B is overwritten by   
  //          diag(R)*B;   
  //          if TRANS = 'T' or 'C' and EQUED = 'C' or 'B', B is   
  //          overwritten by diag(C)*B.   
  //
  //  LDB     (input) INTEGER   
  //          The leading dimension of the array B.  LDB >= max(1,N).   
  //
  //  X       (output) DOUBLE PRECISION array, dimension (LDX,NRHS)   
  //          If INFO = 0 or INFO = N+1, the N-by-NRHS solution matrix X   
  //          to the original system of equations.  Note that A and B are   
  //          modified on exit if EQUED .ne. 'N', and the solution to the   
  //          equilibrated system is inv(diag(C))*X if TRANS = 'N' and   
  //          EQUED = 'C' or 'B', or inv(diag(R))*X if TRANS = 'T' or 'C'   
  //          and EQUED = 'R' or 'B'.   
  //
  //  LDX     (input) INTEGER   
  //          The leading dimension of the array X.  LDX >= max(1,N).   
  //
  //  RCOND   (output) DOUBLE PRECISION   
  //          The estimate of the reciprocal condition number of the matrix   
  //          A after equilibration (if done).  If RCOND is less than the   
  //          machine precision (in particular, if RCOND = 0), the matrix   
  //          is singular to working precision.  This condition is   
  //          indicated by a return code of INFO > 0.   
  //
  //  FERR    (output) DOUBLE PRECISION array, dimension (NRHS)   
  //          The estimated forward error bound for each solution vector   
  //          X(j) (the j-th column of the solution matrix X).   
  //          If XTRUE is the true solution corresponding to X(j), FERR(j)   
  //          is an estimated upper bound for the magnitude of the largest   
  //          element in (X(j) - XTRUE) divided by the magnitude of the   
  //          largest element in X(j).  The estimate is as reliable as   
  //          the estimate for RCOND, and is almost always a slight   
  //          overestimate of the true error.   
  //
  //  BERR    (output) DOUBLE PRECISION array, dimension (NRHS)   
  //          The componentwise relative backward error of each solution   
  //          vector X(j) (i.e., the smallest relative change in   
  //          any element of A or B that makes X(j) an exact solution).   
  //
  //  WORK    (workspace/output) DOUBLE PRECISION array, dimension (4*N)   
  //          On exit, WORK(1) contains the reciprocal pivot growth   
  //          factor norm(A)/norm(U). The "max absolute element" norm is   
  //          used. If WORK(1) is much less than 1, then the stability   
  //          of the LU factorization of the (equilibrated) matrix A   
  //          could be poor. This also means that the solution X, condition   
  //          estimator RCOND, and forward error bound FERR could be   
  //          unreliable. If factorization fails with 0<INFO<=N, then   
  //          WORK(1) contains the reciprocal pivot growth factor for the   
  //          leading INFO columns of A.   
  //
  //  IWORK   (workspace) INTEGER array, dimension (N)   
  //
  //  INFO    (output) INTEGER   
  //          = 0:  successful exit   
  //          < 0:  if INFO = -i, the i-th argument had an illegal value   
  //          > 0:  if INFO = i, and i is   
  //                <= N:  U(i,i) is exactly zero.  The factorization has   
  //                       been completed, but the factor U is exactly   
  //                       singular, so the solution and error bounds   
  //                       could not be computed. RCOND = 0 is returned.   
  //                = N+1: U is nonsingular, but RCOND is less than machine   
  //                       precision, meaning that the matrix is singular   
  //                       to working precision.  Nevertheless, the   
  //                       solution and error bounds are computed because   
  //                       there are a number of situations where the   
  //                       computed solution can be more accurate than the   
  //                       value of RCOND would suggest.   
  //
  //  =====================================================================
  //
  extern int dgesvx_(char *fact, char *trans, int *n, int *nrhs,
		     double *a, int *lda, double *af, int *ldaf, 
		     int *ipiv, char *equed, double *r__, double *c__, 
		     double *b, int *ldb, double *x, int *ldx, double *rcond,
		     double *ferr, double *berr, double *work, int *iwork, int *info);


  //------------------------------------------------------------------
  //  Purpose
  //  =======
  //
  //  DGETRI computes the inverse of a matrix using the LU factorization
  //  computed by DGETRF.
  //
  //  This method inverts U and then computes inv(A) by solving the system
  //  inv(A)*L = inv(U) for inv(A).
  //
  //  Arguments
  //  =========
  //
  //  N       (input) INTEGER
  //          The order of the matrix A.  N >= 0.
  //
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  //          On entry, the factors L and U from the factorization
  //          A = P*L*U as computed by DGETRF.
  //          On exit, if INFO = 0, the inverse of the original matrix A.
  //
  //  LDA     (input) INTEGER
  //          The leading dimension of the array A.  LDA >= max(1,N).
  //
  //  IPIV    (input) INTEGER array, dimension (N)
  //          The pivot indices from DGETRF; for 1<=i<=N, row i of the
  //          matrix was interchanged with row IPIV(i).
  //
  //  WORK    (workspace/output) DOUBLE PRECISION array, dimension (LWORK)
  //          On exit, if INFO=0, then WORK(1) returns the optimal LWORK.
  //
  //  LWORK   (input) INTEGER
  //          The dimension of the array WORK.  LWORK >= max(1,N).
  //          For optimal performance LWORK >= N*NB, where NB is
  //          the optimal blocksize returned by ILAENV.
  //
  //          If LWORK = -1, then a workspace query is assumed; the routine
  //          only calculates the optimal size of the WORK array, returns
  //          this value as the first entry of the WORK array, and no error
  //          message related to LWORK is issued by XERBLA.
  //
  //  INFO    (output) INTEGER
  //          = 0:  successful exit
  //          < 0:  if INFO = -i, the i-th argument had an illegal value
  //          > 0:  if INFO = i, U(i,i) is exactly zero; the matrix is
  //                singular and its inverse could not be computed.
  //
  //  =====================================================================
  //
  extern void dgetri_( int* n, double* a, int* lda, int* ipiv, 
		       double* work, int* lwork, int* info);

  //  =====================================================================
  //  Purpose   
  //  =======   
  //
  //  DSYTRD reduces a real symmetric matrix A to real symmetric   
  //  tridiagonal form T by an orthogonal similarity transformation:   
  //  Q**T * A * Q = T.   
  //
  //  Arguments   
  //  =========   
  //
  //  UPLO    (input) CHARACTER*1   
  //          = 'U':  Upper triangle of A is stored;   
  //          = 'L':  Lower triangle of A is stored.   
  //
  //  N       (input) INTEGER   
  //          The order of the matrix A.  N >= 0.   
  //
  //  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)   
  //          On entry, the symmetric matrix A.  If UPLO = 'U', the leading   
  //          N-by-N upper triangular part of A contains the upper   
  //          triangular part of the matrix A, and the strictly lower   
  //          triangular part of A is not referenced.  If UPLO = 'L', the   
  //          leading N-by-N lower triangular part of A contains the lower   
  //          triangular part of the matrix A, and the strictly upper   
  //          triangular part of A is not referenced.   
  //          On exit, if UPLO = 'U', the diagonal and first superdiagonal   
  //          of A are overwritten by the corresponding elements of the   
  //          tridiagonal matrix T, and the elements above the first   
  //          superdiagonal, with the array TAU, represent the orthogonal   
  //          matrix Q as a product of elementary reflectors; if UPLO   
  //          = 'L', the diagonal and first subdiagonal of A are over-   
  //          written by the corresponding elements of the tridiagonal   
  //          matrix T, and the elements below the first subdiagonal, with   
  //          the array TAU, represent the orthogonal matrix Q as a product   
  //          of elementary reflectors. See Further Details.   
  //
  //  LDA     (input) INTEGER   
  //          The leading dimension of the array A.  LDA >= max(1,N).   
  //
  //  D       (output) DOUBLE PRECISION array, dimension (N)   
  //          The diagonal elements of the tridiagonal matrix T:   
  //          D(i) = A(i,i).   
  //
  //  E       (output) DOUBLE PRECISION array, dimension (N-1)   
  //          The off-diagonal elements of the tridiagonal matrix T:   
  //          E(i) = A(i,i+1) if UPLO = 'U', E(i) = A(i+1,i) if UPLO = 'L'.   
  //
  //  TAU     (output) DOUBLE PRECISION array, dimension (N-1)   
  //          The scalar factors of the elementary reflectors (see Further   
  //          Details).   
  //
  //  WORK    (workspace/output) DOUBLE PRECISION array, dimension (LWORK)   
  //          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.   
  //
  //  LWORK   (input) INTEGER   
  //          The dimension of the array WORK.  LWORK >= 1.   
  //          For optimum performance LWORK >= N*NB, where NB is the   
  //          optimal blocksize.   
  //
  //          If LWORK = -1, then a workspace query is assumed; the routine   
  //          only calculates the optimal size of the WORK array, returns   
  //          this value as the first entry of the WORK array, and no error   
  //          message related to LWORK is issued by XERBLA.   
  //
  //  INFO    (output) INTEGER   
  //          = 0:  successful exit   
  //          < 0:  if INFO = -i, the i-th argument had an illegal value   
  //
  //  Further Details   
  //  ===============   
  //
  //  If UPLO = 'U', the matrix Q is represented as a product of elementary   
  //  reflectors   
  //
  //     Q = H(n-1) . . . H(2) H(1).   
  //
  //  Each H(i) has the form   
  //
  //     H(i) = I - tau * v * v'   
  //
  //  where tau is a real scalar, and v is a real vector with   
  //  v(i+1:n) = 0 and v(i) = 1; v(1:i-1) is stored on exit in   
  //  A(1:i-1,i+1), and tau in TAU(i).   
  //
  //  If UPLO = 'L', the matrix Q is represented as a product of elementary   
  //  reflectors   
  //
  //     Q = H(1) H(2) . . . H(n-1).   
  //
  //  Each H(i) has the form   
  //
  //     H(i) = I - tau * v * v'   
  //
  //  where tau is a real scalar, and v is a real vector with   
  //  v(1:i) = 0 and v(i+1) = 1; v(i+2:n) is stored on exit in A(i+2:n,i),   
  //  and tau in TAU(i).   
  //
  //  The contents of A on exit are illustrated by the following examples   
  //  with n = 5:   
  //
  //  if UPLO = 'U':                       if UPLO = 'L':   
  //
  //    (  d   e   v2  v3  v4 )              (  d                  )   
  //    (      d   e   v3  v4 )              (  e   d              )   
  //    (          d   e   v4 )              (  v1  e   d          )   
  //    (              d   e  )              (  v1  v2  e   d      )   
  //    (                  d  )              (  v1  v2  v3  e   d  )   
  //
  //  where d and e denote diagonal and off-diagonal elements of T, and vi   
  //  denotes an element of the vector defining H(i).   
  //
  //  =====================================================================   

  extern int dsytrd_(char *uplo, int *n, double *a, int *lda,
		     double *d_, double *e, double *tau, double *work,
		     int *lwork, int *info);

  //  Purpose   
  //  =======   
  //
  //  DSBTRD reduces a real symmetric band matrix A to symmetric   
  //  tridiagonal form T by an orthogonal similarity transformation:   
  //  Q**T * A * Q = T.   
  //
  //  Arguments   
  //  =========   
  //
  //  VECT    (input) CHARACTER*1   
  //          = 'N':  do not form Q;   
  //          = 'V':  form Q;   
  //          = 'U':  update a matrix X, by forming X*Q.   
  //
  //  UPLO    (input) CHARACTER*1   
  //          = 'U':  Upper triangle of A is stored;   
  //          = 'L':  Lower triangle of A is stored.   
  //
  //  N       (input) INTEGER   
  //          The order of the matrix A.  N >= 0.   
  //
  //  KD      (input) INTEGER   
  //          The number of superdiagonals of the matrix A if UPLO = 'U',   
  //          or the number of subdiagonals if UPLO = 'L'.  KD >= 0.   
  //
  //  AB      (input/output) DOUBLE PRECISION array, dimension (LDAB,N)   
  //          On entry, the upper or lower triangle of the symmetric band   
  //          matrix A, stored in the first KD+1 rows of the array.  The   
  //          j-th column of A is stored in the j-th column of the array AB   
  //          as follows:   
  //          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;   
  //          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).   
  //          On exit, the diagonal elements of AB are overwritten by the   
  //          diagonal elements of the tridiagonal matrix T; if KD > 0, the   
  //          elements on the first superdiagonal (if UPLO = 'U') or the   
  //          first subdiagonal (if UPLO = 'L') are overwritten by the   
  //          off-diagonal elements of T; the rest of AB is overwritten by   
  //          values generated during the reduction.   
  //
  //  LDAB    (input) INTEGER   
  //          The leading dimension of the array AB.  LDAB >= KD+1.   
  //
  //  D       (output) DOUBLE PRECISION array, dimension (N)   
  //          The diagonal elements of the tridiagonal matrix T.   
  //
  //  E       (output) DOUBLE PRECISION array, dimension (N-1)   
  //          The off-diagonal elements of the tridiagonal matrix T:   
  //          E(i) = T(i,i+1) if UPLO = 'U'; E(i) = T(i+1,i) if UPLO = 'L'.   
  //
  //  Q       (input/output) DOUBLE PRECISION array, dimension (LDQ,N)   
  //          On entry, if VECT = 'U', then Q must contain an N-by-N   
  //          matrix X; if VECT = 'N' or 'V', then Q need not be set.   
  //
  //          On exit:   
  //          if VECT = 'V', Q contains the N-by-N orthogonal matrix Q;   
  //          if VECT = 'U', Q contains the product X*Q;   
  //          if VECT = 'N', the array Q is not referenced.   
  //
  //  LDQ     (input) INTEGER   
  //          The leading dimension of the array Q.   
  //          LDQ >= 1, and LDQ >= N if VECT = 'V' or 'U'.   
  //
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (N)   
  //
  //  INFO    (output) INTEGER   
  //          = 0:  successful exit   
  //          < 0:  if INFO = -i, the i-th argument had an illegal value   
  //
  //  Further Details   
  //  ===============   
  //
  //  Modified by Linda Kaufman, Bell Labs.   
  //
  //  =====================================================================   

  extern int dsbtrd_(char *vect, char *uplo, int *n, int *kd, 
		     double *ab, int *ldab, double *d__, double *e, 
		     double *q, int *ldq, double *work, int *info);

  //------------------------------------------------------------------
  //  Purpose   
  //  =======   
  //
  //  DSTEQR computes all eigenvalues and, optionally, eigenvectors of a   
  //  symmetric tridiagonal matrix using the implicit QL or QR method.   
  //  The eigenvectors of a full or band symmetric matrix can also be found   
  //  if DSYTRD or DSPTRD or DSBTRD has been used to reduce this matrix to   
  //  tridiagonal form.   
  //
  //  Arguments   
  //  =========   
  //
  //  COMPZ   (input) CHARACTER*1   
  //          = 'N':  Compute eigenvalues only.   
  //          = 'V':  Compute eigenvalues and eigenvectors of the original   
  //                  symmetric matrix.  On entry, Z must contain the   
  //                  orthogonal matrix used to reduce the original matrix   
  //                  to tridiagonal form.   
  //          = 'I':  Compute eigenvalues and eigenvectors of the   
  //                  tridiagonal matrix.  Z is initialized to the identity   
  //                  matrix.   
  //
  //  N       (input) INTEGER   
  //          The order of the matrix.  N >= 0.   
  //
  //  D       (input/output) DOUBLE PRECISION array, dimension (N)   
  //          On entry, the diagonal elements of the tridiagonal matrix.   
  //          On exit, if INFO = 0, the eigenvalues in ascending order.   
  //
  //  E       (input/output) DOUBLE PRECISION array, dimension (N-1)   
  //          On entry, the (n-1) subdiagonal elements of the tridiagonal   
  //          matrix.   
  //          On exit, E has been destroyed.   
  //
  //  Z       (input/output) DOUBLE PRECISION array, dimension (LDZ, N)   
  //          On entry, if  COMPZ = 'V', then Z contains the orthogonal   
  //          matrix used in the reduction to tridiagonal form.   
  //          On exit, if INFO = 0, then if  COMPZ = 'V', Z contains the   
  //          orthonormal eigenvectors of the original symmetric matrix,   
  //          and if COMPZ = 'I', Z contains the orthonormal eigenvectors   
  //          of the symmetric tridiagonal matrix.   
  //          If COMPZ = 'N', then Z is not referenced.   
  //
  //  LDZ     (input) INTEGER   
  //          The leading dimension of the array Z.  LDZ >= 1, and if   
  //          eigenvectors are desired, then  LDZ >= max(1,N).   
  //
  //  WORK    (workspace) DOUBLE PRECISION array, dimension (max(1,2*N-2))   
  //          If COMPZ = 'N', then WORK is not referenced.   
  //
  //  INFO    (output) INTEGER   
  //          = 0:  successful exit   
  //          < 0:  if INFO = -i, the i-th argument had an illegal value   
  //          > 0:  the algorithm has failed to find all the eigenvalues in   
  //                a total of 30*N iterations; if INFO = i, then i   
  //                elements of E have not converged to zero; on exit, D   
  //                and E contain the elements of a symmetric tridiagonal   
  //                matrix which is orthogonally similar to the original   
  //                matrix.   
  //
  //  =====================================================================   
  extern int dsteqr_(char *compz, int *n, double *d__, 
		     double *e, double *z_, int *ldz, double *work, 
		     int *info);
}

#endif // lapack_h__
