#ifndef soofea_assert_h___
#define soofea_assert_h___

#include <soofea_config.h>
#include <cassert>

#ifdef SOOFEA_HAVE_ASSERT
#define SOOFEA_ASSERT(expr) assert(expr)
#else
#define SOOFEA_ASSERT(expr) ((void)0)
#endif

#endif // soofea_assert_h___
