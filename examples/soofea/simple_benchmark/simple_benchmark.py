#! /usr/bin/env python

import sys,os
sys.path.append( os.path.join(os.path.abspath(sys.path[0]),"../../../python" ) )

from argparse import ArgumentParser
import os.path
import numpy as np
from matplotlib import rc
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.font_manager as fmgr
from matplotlib.figure import SubplotParams
from xml.etree import ElementTree

from SOOFEA import *
import SOOFEA.Model
# from SOOFEA.Base import Configuration, IDObject

font_size = 11
line_width = 0.6

rc('font', **{'family':'serif','serif':['Computer Modern Roman'],'size':font_size} )
rc('text', **{'usetex':True} )

#------------------------------------------------------------------
def openResults( flgm_result, flga_result ):
    global force_FLGA
    global force_FLGM

    print "Load FLGA forces"
    solver = Solver()
    model_handler = solver.createModelHandler(flgm_result,IDObject.ID.XML,True)
    model_handler.read()
    model_handler.readResults()
    force_FLGA = loadForces( model_handler )
    del solver
    print "Load FLGM forces"
    model_handler = solver.createModelHandler(flga_result,IDObject.ID.XML,True)
    model_handler.read()
    model_handler.readResults()
    force_FLGM = loadForces( model_handler )
    del solver

    return [force_FLGM,force_FLGA]

#------------------------------------------------------------------
def getXMLContainer( tree, tagname ):
    container = tree.getroot().find( tagname )
    return( container )

#------------------------------------------------------------------
def loadForces( model_handler ):
    xml_file = open( filename, 'rw' )
    tree = ElementTree.parse( xml_file )
    result_container = getXMLContainer( tree, 'result' )
    if result_container == None:
        raise Exception('No Results found in input file!')

    node_numbers = [25,24,23]
    for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
        time[time_stamp.index()] = time_stamp.time()
        for node_num in node_numbers:
            force_x[time_stamp.index()] = force_x[time_stamp.index()] + \
                                          model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.X).getInternal( time_stamp )
            force_y[time_stamp.index()] = force_y[time_stamp.index()] + \
                                          model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.Y).getInternal( time_stamp )
    return [force_x,force_y,time]

#------------------------------------------------------------------
def plotForces(force_FLGM,force_FLGA,frict):
    fig = plt.figure(figsize=(5.9,4.),dpi=72.27,facecolor='white')
    ax = fig.gca()
    ax.set_xlim(0,8)
    ax.set_ylim(-2000,6000)
    plt.xlabel('Time $t$ [sec]')
    plt.ylabel('Reaction Forces [N]')
    p1, = ax.plot( force_FLGM[2], force_FLGM[1] , '-', color=r'#cccccc' )
    p2, = ax.plot( force_FLGM[2], force_FLGM[0] , '-', color=r'#cccccc' )
    p3, = ax.plot( force_FLGA[2], force_FLGA[1] , '--', color='black')
    p4, = ax.plot( force_FLGA[2], force_FLGA[0] , '-.', color='black' )
    if( frict ):
        ax.legend([p3,p4,p1],['Vertical reaction force','Horizontal reaction force','Mortar side normal overlay'], \
                  loc=9,bbox_to_anchor=(0.35,0.6),prop={'size':font_size})
        ax.axhline( y=force_FLGM[0][23] , linestyle="-", c="red" )
        ax.axhline( y=force_FLGM[0][77] , linestyle="-", c="red" )
        ax.text(4.5,force_FLGM[0][23]+100 ,'Slip/Stick change',color='red')
    else:
        ax.legend([p3,p4,p1],['Vertical reaction force','Horizontal reaction force','Mortar side normal overlay'], \
                  loc=9,bbox_to_anchor=(0.5,0.55),prop={'size':font_size})

    ax.axvline( x=1 , linestyle='-', c='k', linewidth=0.2 )
    ax.axvline( x=4 , linestyle='-', c='k', linewidth=0.2 )
    ax.axvline( x=7 , linestyle='-', c='k', linewidth=0.2 )
    ax.axhline( y=0 , linestyle='-', c='k', linewidth=0.2 )
    ax.annotate('Stop move downwards\nand start sliding',xy=(1,3000),xytext=(1.2,3300),arrowprops=dict(arrowstyle="->",facecolor='black'))
    ax.annotate('Change sliding direction',xy=(4,3000),xytext=(4.2,3300),arrowprops=dict(arrowstyle="->",facecolor='black'))
    ax.annotate('Stop sliding\nand move upwards',xy=(7,3700),xytext=(4.7,4000),arrowprops=dict(arrowstyle="->",facecolor='black'),multialignment='right')
    plt.show()

#------------------------------------------------------------------
def plotDeformation(model_handler, time_stamp_index, axes):
    # eps_path = os.path.join( output_dir , problem_name + "_" + str(time_stamp_index) + ".pdf" )
    time_stamp = model_handler.getModel().getTimeBar().getTimeStamp( time_stamp_index )
    for edge in model_handler.getModel().getEdgeIterator():
        edge.plot( axes , time_stamp , 1. )

#------------------------------------------------------------------
def initMovieFigure():
    fig = plt.figure( figsize=(4.5,2.8), dpi=204.8, facecolor='white',
                      subplotpars=SubplotParams(bottom=0.15,top=0.92,left=0.16,right=1) )
    axes_force = fig.add_subplot(121)
    for key,spine in axes_force.spines.iteritems():
        spine.set_linewidth(0.5)
    axes_force.set_xlim(0,8)
    axes_force.set_ylim(-2000,6000)
    plt.xlabel('Time $t$ [sec]')
    plt.ylabel('Reaction Forces [N]')

    axes_def = fig.add_subplot(122)
    axes_def.set_yticks([])
    axes_def.set_aspect(aspect='equal')
    axes_def.set_autoscale_on(False)
    axes_def.set_xlim(-15,115)
    axes_def.set_ylim(-10,175)
    #        for [loc, spine] in axes.spines.iteritems():
    #            spine.set_visible(False)
    axes_def.spines['left'].set_visible(False)
    axes_def.spines['right'].set_visible(False)
    axes_def.spines['top'].set_visible(False)
    axes_def.spines['bottom'].set_position(('axes',1))

    for tick in axes_def.xaxis.get_major_ticks():
        tick.label.set_fontsize(7)

    for l in axes_def.get_xticklines():
        l.set_markersize( l.get_markersize()*0.7 )

    return [fig, axes_force, axes_def]

#------------------------------------------------------------------
if __name__ == '__main__':
    mu = True
    output_dir = '/afs/fest.tugraz.at/user/mueli/vcs/git/rigorosum/images/simple_benchmark_pdf'

    if mu:
        filename = 'simple_benchmark_FLGM_mu03.xml'
        problem_name = 'simple_benchmark_mu03'
    else:
        filename = 'simple_benchmark_FLGM.xml'
        problem_name = 'simple_benchmark'

    solver = Solver()
    model_handler = solver.createModelHandler(filename,IDObject.ID.XML,True)
    model_handler.read()
    model_handler.readResults()

    force = loadForces( model_handler )
    for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
        print "timestamp: "+str(time_stamp.index())+" | "+str(model_handler.getModel().getTimeBar().getLastFinishedTimeStamp().index())+"\r",
        sys.stdout.flush()
        output_path = os.path.join( output_dir , problem_name + "_" + str(time_stamp.index()) + ".pdf" )

        [fig, axes_force, axes_def] = initMovieFigure()

        plotDeformation(model_handler, time_stamp.index(), axes_def)
        p1, = axes_force.plot( force[2][:time_stamp.index()], force[1][:time_stamp.index()] , 'r-')
        p2, = axes_force.plot( force[2][:time_stamp.index()], force[0][:time_stamp.index()] , 'b-' )

        fig.savefig( output_path, format='PDF', bbox_inches='tight', dpi=204.8 )
        fig.clear()
