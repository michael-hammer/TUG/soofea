#! /usr/bin/env python

import sys, os, math
sys.path.append( os.path.join(os.path.abspath(sys.path[0]),"../../../python" ) )

from soofea.io import loadLagrange
from soofea import Solver
import soofea.model
from soofea.base import Configuration, IDObject

import math
import operator

import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.figure import SubplotParams
import numpy as np

font_size = 7
line_width = 0.6

rc('font', **{'family':'serif','serif':['Computer Modern Roman'],'size':font_size} )
rc('text', **{'usetex':True} )
rc('lines',linewidth=line_width)

def plotContactForces( ):
    time_stamp = 5
    node_list = [2,30,31,32,33,34,35,36,37,38,39,40,41,42,42,43,44,4]

    files = []
    results = []
    labels = []
    styles = []

    # Reference solution
    files.append( '/nfs/home/mueli/vcs/git/soofea/examples/soofea/thick_cylinder/thick_cylinder_interface_LA_16_16.xml' )
    results.append( loadLagrange( files[-1] , node_list ) )
    labels.append(None)
    styles.append('k--')

    # 16-13
    files.append( '/nfs/home/mueli/vcs/git/soofea/examples/soofea/thick_cylinder/thick_cylinder_interface_LM_16_13.xml' )
    results.append( loadLagrange( files[-1] , node_list ) )
    labels.append('Mortar side 16-13')
    styles.append('k+')   

    files.append( '/nfs/home/mueli/vcs/git/soofea/examples/soofea/thick_cylinder/thick_cylinder_interface_LA_16_13.xml' )
    results.append( loadLagrange( files[-1] , node_list ) )
    labels.append('Averaged 16-13')
    styles.append('kx')
    
    error_16_13 = {}
    for node_num in node_list:
        error_16_13[node_num] = (results[-2]['time_stamp'][time_stamp][node_num] - results[-1]['time_stamp'][time_stamp][node_num]) / \
            results[0]['time_stamp'][time_stamp][node_num]

    # 16-21
    files.append( '/nfs/home/mueli/vcs/git/soofea/examples/soofea/thick_cylinder/thick_cylinder_interface_LM_16_21.xml' )
    results.append( loadLagrange( files[-1] , node_list) )  
    labels.append('Mortar side 16-21')
    styles.append('k.')

    files.append( '/nfs/home/mueli/vcs/git/soofea/examples/soofea/thick_cylinder/thick_cylinder_interface_LA_16_21.xml' )
    results.append( loadLagrange( files[-1] , node_list) )
    labels.append('Averaged 16-21')
    styles.append('ks')

    error_16_21 = {}
    for node_num in node_list:
        error_16_21[node_num] = (results[-2]['time_stamp'][time_stamp][node_num] - results[-1]['time_stamp'][time_stamp][node_num]) / \
            results[0]['time_stamp'][time_stamp][node_num]
   
    width = 3.2
    height = width / (1.+math.sqrt(5)) * 4.
    
    fig = plt.figure( figsize=(width,height), dpi=72.27, subplotpars=SubplotParams(left=0.15,right=0.95,top=0.95) )
    axes = fig.add_subplot(211)
    axes.set_xlim([0,math.pi/2])
    axes.set_ylim([30.,50.])

    for node in node_list:
        temp_phi = {}
        for key,coord in results[0]['coords'].iteritems():
            temp_phi[key] = math.atan(coord[1]/coord[0])
    sorted_node_phi = sorted( temp_phi.iteritems(), key=operator.itemgetter(1) )
    sorted_phi = [phi for (node_num,phi) in sorted_node_phi]

    for i in range(len(files)):
        lagrange = [ (-1)*results[i]['time_stamp'][time_stamp][node_num] for (node_num,phi) in sorted_node_phi]
        axes.plot(sorted_phi,lagrange, styles[i], label=labels[i], markersize=6, markerfacecolor='None')

    axes.legend(loc='lower center',prop={'size':font_size})
    axes.get_legend().get_frame().set_linewidth(line_width)
    for key,spine in axes.spines.iteritems():
        spine.set_linewidth(line_width)
    plt.xlabel('Angle $\\varphi [Radian]$')
    plt.ylabel('Value of Lagrange $\lambda_N [N/mm^2]$')

    axes = fig.add_subplot(212)
    error = [ math.fabs(error_16_13[node_num]*100.) for (node_num,phi) in sorted_node_phi]
    axes.plot(sorted_phi,error, 'k+', label='16-13 mesh')
    error = [ math.fabs(error_16_21[node_num]*100.) for (node_num,phi) in sorted_node_phi]
    axes.plot(sorted_phi,error, 'k.', label='16-21 mesh')
    axes.legend(loc='upper center',prop={'size':font_size})
    axes.get_legend().get_frame().set_linewidth(line_width)
    for key,spine in axes.spines.iteritems():
        spine.set_linewidth(line_width)
    plt.xlabel('Angle $\\varphi [Radian]$')
    plt.ylabel('Relative deviation in $[\%]$')
    
    plt.show()
    
def plotMesh(model_handler, time_stamp_index, axes):
    # time_stamps = [0,10,120,230,285,340]
    # time_stamps = [0,65,120,175]
    # path = os.path.join( output_dir , problem_name + "_" + str(time_stamp_index) + ".pdf" )
    time_stamp = model_handler.getModel().getTimeBar().getTimeStamp( time_stamp_index )
    # print "Plotting time stamp "+str(time_stamp.index())+" at time "+str(time_stamp.time())
    for element in model_handler.getModel().getElementIterator():
        element.plot( axes , time_stamp , 1. , lineweight=0.5 )

def initMeshFigure():
    fig = plt.figure( figsize=(1.6,1.6), dpi=72.27, subplotpars=SubplotParams(left=0,bottom=0,right=1,top=1) )
    axes = fig.add_subplot(111)
    axes.set_xticks([])
    axes.set_yticks([])
    axes.set_aspect(aspect='equal')
    axes.set_autoscale_on(False)
    axes.set_xlim(-0.1,1.2)
    axes.set_ylim(-0.1,1.2)
    for [loc, spine] in axes.spines.iteritems():
        spine.set_visible(False)

    return [fig, axes]

if __name__ == '__main__':
    # plotContactForces()

    mesh_versions = ["16_13","16_16","16_21"]
    output_dir = '/nfs/home/mueli/vcs/git/comp_mech_mortar/images/thick_cylinder'
    problem_name = 'thick_cylinder'
    filename = '/nfs/home/mueli/vcs/git/soofea/examples/soofea/thick_cylinder/thick_cylinder_interface_LA_'

    last_time_stamp = 5

    #for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
    for mesh_version in mesh_versions:
        solver = Solver()
        model_handler = solver.createModelHandler(filename+mesh_version+'.xml',IDObject.ID.XML,True)
        model_handler.read()
        model_handler.readResults()

        time_stamp = model_handler.getModel().getTimeBar().getTimeStamp(5)
        print "mesh_version: "+mesh_version+"\r",
        sys.stdout.flush()
        output_path = os.path.join( output_dir , problem_name + "_" + mesh_version + ".pdf" )

        [fig, axes] = initMeshFigure()

        plotMesh(model_handler, time_stamp.index(), axes)
        fig.savefig( output_path, format='PDF', bbox_inches='tight', dpi=204.8 )
        fig.clear()
        
        del solver
