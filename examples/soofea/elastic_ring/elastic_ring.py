#! /usr/bin/env python

import sys,os
sys.path.append( os.path.join(os.path.abspath(sys.path[0]),"../../../python" ) )

import os.path
import numpy as np
from matplotlib import rc
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.font_manager as fmgr
from matplotlib.ticker import MaxNLocator
from matplotlib.figure import SubplotParams
from xml.etree import ElementTree

from SOOFEA import *
import SOOFEA.Model
from SOOFEA.Base import Configuration, IDObject

# for comput_mech
font_size = 11
line_width = 0.6

# for dissertation
# font_size = 10
# line_width = 1.0

rc('font', **{'family':'serif','serif':['Computer Modern Roman'],'size':font_size} )
rc('text', **{'usetex':True} )
rc('lines',linewidth=line_width)

#------------------------------------------------------------------
def openResults( flgm_result, flga_result ):
    global force_FLGA
    global force_FLGM

    print "Load FLGM forces"
    force_FLGM = loadForces( flgm_result )
    print "Load FLGA forces"
    force_FLGA = loadForces( flga_result )

    return [force_FLGM,force_FLGA]

# #------------------------------------------------------------------
# def loadForces( filename ):
#     solver = Solver()
#     model_handler = solver.createModelHandler(filename,IDObject.ID.XML,True)
#     model_handler.read()
#     model_handler.readResults()
#
#     step_amounts = model_handler.getModel().getTimeBar().getLastFinishedTimeStamp().index()+1
#
#     time = np.zeros(step_amounts)
#     force_x = np.zeros(step_amounts)
#     force_y = np.zeros(step_amounts)
#
#     node_numbers = [218,264,310]
#     for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
#         time[time_stamp.index()] = time_stamp.time()
#         for node_num in node_numbers:
#             force_x[time_stamp.index()] = force_x[time_stamp.index()] + \
#                                           model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.X).getInternal( time_stamp )
#             force_y[time_stamp.index()] = force_y[time_stamp.index()] + \
#                                           model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.Y).getInternal( time_stamp )
#     del solver
#
#     return [force_x,force_y,time]

#------------------------------------------------------------------
def getXMLContainer( tree, tagname ):
    container = tree.getroot().find( tagname )
    return( container )

#------------------------------------------------------------------
def loadForces( filename ):
    xml_file = open( filename, 'rw' )
    tree = ElementTree.parse( xml_file )
    result_container = getXMLContainer( tree, 'result' )
    if result_container == None:
        raise Exception('No Results found in input file!')

    node_numbers = [218,264,310]
    force_x = []
    force_y = []
    time = []
    for time_stamp in result_container:
        time.append(float(time_stamp.get('time')))
        force_x.append(0.)
        force_y.append(0.)
        for node_result in list(time_stamp)[0]:
            if int(node_result.get('N')) in node_numbers:
                force_str = node_result.get('force').split(' ')
                force_x[-1] += float(force_str[0])
                force_y[-1] += float(force_str[1])

    return [force_x, force_y, time]

#------------------------------------------------------------------
def plotSingleResultForce(model_handler):
    time_step_amount = model_handler.getModel().getTimeBar().getLastFinishedTimeStamp().index()+1
    time = np.zeros(time_step_amount)
    force_x = np.zeros(time_step_amount)
    force_y = np.zeros(time_step_amount)

    # both
    # node_numbers = [218,264,310,355,309,263]
    # only left
    node_numbers = [218,264,310]
    for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
        time[time_stamp.index()] = time_stamp.time()
        for node_num in node_numbers:
            force_x[time_stamp.index()] = force_x[time_stamp.index()] + \
                                          model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.X).getInternal( time_stamp )
            force_y[time_stamp.index()] = force_y[time_stamp.index()] + \
                                          model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.Y).getInternal( time_stamp )

    fig = plt.figure(figsize=(5.8,3.),dpi=72.27,facecolor='white',subplotpars=SubplotParams(bottom=0.15))
    ax = fig.gca()
    plt.xlabel('Time $t$ [sec]')
    plt.ylabel('Reaction Forces [N]')
    ax.plot( time, force_y , 'g-' , time, force_x , 'b-' )

    ax.legend(('Vertical reaction force','Horizontal reaction force',),loc='upper left',prop={'size':font_size})
    ax.axvline(time[23], linestyle='--', color='gray')

#    if( frict ):
#        ax.axhline( y=force_x[18] , linestyle="-", c="red" )
#        ax.text(4.5,force_x[18]+100 ,'Slip/Stick change',color='red')
#        ax.axhline( y=force_x[57] , linestyle="-", c="red" )
#        ax.text(1.5,force_x[57]-350 ,'Slip/Stick change',color='red')
#    else:
#        ax.legend(('Vertical reaction force','Horizontal reaction force',),loc=9,bbox_to_anchor=(0.5,0.5),prop={'size':font_size})
    plt.show()

#------------------------------------------------------------------
def plotForces( force_FLGM , force_FLGA , friction ):
    #fig = plt.figure(figsize=(3.3,3.),dpi=72.27,facecolor='white',subplotpars=SubplotParams(left=0.18,bottom=0.15,right=0.95))
    fig = plt.figure(figsize=(5.,2.5),dpi=72.27,facecolor='white',subplotpars=SubplotParams(left=0.18,bottom=0.15))
    ax = fig.gca()
    if not friction:
        ax.set_ylim([-5000,6000])
#    else:
#        ax.set_ylim([-8000,16000])
#        ax.yaxis.set_major_locator(MaxNLocator(12))

    plt.xlabel('Time $t$ [sec]')
    plt.ylabel('Reaction Forces [N]')
    p1, = ax.plot( force_FLGM[2], force_FLGM[1] , '-', color=r'#cccccc' )
    p2, = ax.plot( force_FLGM[2], force_FLGM[0] , '-', color=r'#cccccc' )
    p3, = ax.plot( force_FLGA[2], force_FLGA[1] , '--', color='black')
    p4, = ax.plot( force_FLGA[2], force_FLGA[0] , '-.', color='black' )

    if not friction:
        ax.legend([p3,p4,p1], \
                      ['Vertical av. normal','Horizontal av. normal','Mortar side normal'], \
                      loc=9,bbox_to_anchor=(0.85,0.62),prop={'size':font_size})
    else:
        ax.legend([p3,p4,p1], \
                      ['Vertical av. normal','Horizontal av. normal','Mortar side normal'], \
                      loc=9,bbox_to_anchor=(0.85,0.68),prop={'size':font_size})

    ax.get_legend().get_frame().set_linewidth(line_width)
    for key,spine in ax.spines.iteritems():
        spine.set_linewidth(line_width)

    plt.show()

#------------------------------------------------------------------
def plotTimeLine(input_file,output_dir,problem_name):
    solver = Solver()
    model_handler = solver.createModelHandler(input_file,IDObject.ID.XML,True)
    model_handler.read()
    model_handler.readResults()

    time_stamps = [0,75,113,150]

    for time_stamp_index in time_stamps:
        fig_time_line = plt.figure( figsize=(1.55,0.91), dpi=72.27, subplotpars=SubplotParams(left=0,bottom=0,right=1,top=1) )
        axes = fig_time_line.add_subplot(111)
        axes.set_xticks([])
        axes.set_yticks([])
        axes.set_aspect(aspect='equal')
        axes.set_autoscale_on(False)
        axes.set_xlim(-130,130)
        axes.set_ylim(-1,151)
        for [loc, spine] in axes.spines.iteritems():
            spine.set_visible(False)

        eps_path = os.path.join( output_dir , problem_name + "_" + str(time_stamp_index) + ".pdf" )

        time_stamp = model_handler.getModel().getTimeBar().getTimeStamp( time_stamp_index )

        print "Plotting time stamp "+str(time_stamp.index())+" at time "+str(time_stamp.time())
        
        for edge in model_handler.getModel().getEdgeIterator():
            edge.plot( axes , time_stamp , 1. )

        fig_time_line.savefig( eps_path, format='PDF' , bbox_inches='tight' , pad_inches = 0. )
        plt.close(fig_time_line)
