#! /usr/bin/env python

import sys,os
sys.path.append( os.path.join(os.path.abspath(sys.path[0]),"../../../python" ) )

import numpy as np
from matplotlib import rc
import matplotlib.pyplot as plt
import matplotlib.lines as lines
from matplotlib.figure import SubplotParams
from IPython import embed
from argparse import ArgumentParser
from xml.etree import ElementTree
from matplotlib.ticker import MaxNLocator
from matplotlib.figure import SubplotParams

from SOOFEA import *
import SOOFEA.Model
from SOOFEA.Base import Configuration, IDObject

from SOOFEA.Geometry.primitives import Rectangle
from SOOFEA.Geometry.primitives import QuarterCircle
from SOOFEA.Model.mesh import Mesh

# for comput mech
font_size = 11
line_width = 0.6

# for dissertation
# font_size = 10
# line_width = 1.0

rc('font', **{'family':'serif','serif':['Computer Modern Roman'],'size':font_size} )
rc('text', **{'usetex':True} )
rc('lines',linewidth=line_width)

#------------------------------------------------------------------
def openResults( flgm_result, flga_result ):
    global force_FLGA
    global force_FLGM

    print "Load FLGM forces"
    solver = Solver()
    model_handler = solver.createModelHandler(flgm_result,IDObject.ID.XML,True)
    model_handler.read()
    model_handler.readResults()
    force_FLGM = loadForces( model_handler )
    del solver
    print "Load FLGA forces"
    solver = Solver()
    model_handler = solver.createModelHandler(flga_result,IDObject.ID.XML,True)
    model_handler.read()
    model_handler.readResults()
    force_FLGA = loadForces( flga_result )
    del solver

    return [force_FLGM,force_FLGA]

# #------------------------------------------------------------------
# def loadForces( filename ):
#     solver = Solver()
#     model_handler = solver.createModelHandler(filename,IDObject.ID.XML,True)
#     model_handler.read()
#     model_handler.readResults()
#
#     step_amounts = model_handler.getModel().getTimeBar().getLastFinishedTimeStamp().index()+1
#
#     time = np.zeros(step_amounts)
#     force_x = np.zeros(step_amounts)
#     force_y = np.zeros(step_amounts)
#
#     # node_numbers = range(480,487)
#     node_numbers = range(1806,1818+1)
#     for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
#         time[time_stamp.index()] = time_stamp.time()
#         for node_num in node_numbers:
#             force_x[time_stamp.index()] = force_x[time_stamp.index()] + \
#                                           model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.X).getInternal( time_stamp )
#             force_y[time_stamp.index()] = force_y[time_stamp.index()] + \
#                                           model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.Y).getInternal( time_stamp )
#     del solver
#
#     return [force_x,force_y,time]

#------------------------------------------------------------------
def getXMLContainer( tree, tagname ):
    container = tree.getroot().find( tagname )
    return( container )

#------------------------------------------------------------------
def loadForces( model_handler, fine_mesh = False ):
    xml_file = open( filename, 'rw' )
    tree = ElementTree.parse( xml_file )
    result_container = getXMLContainer( tree, 'result' )
    if result_container == None:
        raise Exception('No Results found in input file!')

    if fine_mesh:
        node_numbers = range(1806,1818+1)
    else:
        node_numbers = range(480,487)

    for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
        time[time_stamp.index()] = time_stamp.time()
        for node_num in node_numbers:
            force_x[time_stamp.index()] = force_x[time_stamp.index()] + \
                                          model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.X).getInternal( time_stamp )
            force_y[time_stamp.index()] = force_y[time_stamp.index()] + \
                                          model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.Y).getInternal( time_stamp )
    return [force_x,force_y,time]

#------------------------------------------------------------------
def plotForces( force_FLGM , force_FLGA , friction ):
    fig = plt.figure(figsize=(5.9,4.),dpi=72.27,facecolor='white',subplotpars=SubplotParams(bottom=0.15,left=0.15))
    # fig = plt.figure(figsize=(3.3,2.2),dpi=72.27,facecolor='white',subplotpars=SubplotParams(bottom=0.15,left=0.18))
    ax = fig.gca()
    # ax.set_ylim([-1500,4500])
    plt.xlabel('Time $t$ [sec]')
    plt.ylabel('Reaction Forces [N]')
    p1, = ax.plot( force_FLGM[2], force_FLGM[1] , '-', color=r'#808080' )
    p2, = ax.plot( force_FLGM[2], force_FLGM[0] , '-', color=r'#808080' )
    p3, = ax.plot( force_FLGA[2], force_FLGA[1] , '--', color='black')
    p4, = ax.plot( force_FLGA[2], force_FLGA[0] , '-.', color='black' )

    if( friction ):
        ax.legend([p3,p4,p1], \
                  ['Vertical reaction force (averaged)','Horizontal reaction force (averaged)','Mortar side normal'], \
                  loc=9,bbox_to_anchor=(0.5,0.9),prop={'size':font_size})
    else:
        ax.legend([p3,p4,p1], \
                  ['Vertical reaction force (averaged)','Horizontal reaction force (averaged)','Mortar side normal'], \
                  loc=9,bbox_to_anchor=(0.5,0.7),prop={'size':font_size})

    ax.get_legend().get_frame().set_linewidth(line_width)
    for key,spine in ax.spines.iteritems():
        spine.set_linewidth(line_width)

    plt.show()

#------------------------------------------------------------------
def plotSingleResultForce( model_handler ):
    step_amounts = model_handler.getModel().getTimeBar().getLastFinishedTimeStamp().index()+1

    time = np.zeros(step_amounts)
    force_x = np.zeros(step_amounts)
    force_y = np.zeros(step_amounts)

    node_numbers = range(1806,1818+1)
    for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
        time[time_stamp.index()] = time_stamp.time()
        for node_num in node_numbers:
            force_x[time_stamp.index()] = force_x[time_stamp.index()] + \
                                          model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.X).getInternal( time_stamp )
            force_y[time_stamp.index()] = force_y[time_stamp.index()] + \
                                          model_handler.getModel().getNode(node_num).getForceLoad().getValue(CoordSys.CoordID.Y).getInternal( time_stamp )

#   fig = plt.figure(figsize=(6.,4.),dpi=72.27,facecolor='white')
    fig = plt.figure(figsize=(3.3,2.2),dpi=72.27,facecolor='white')
    ax = fig.gca()
    plt.xlabel('Time $t$ [sec]')
    plt.ylabel('Reaction Forces [N]')
    p1, = ax.plot( time , force_y, 'g-')
    p2, = ax.plot( time , force_x, 'b-' )

    ax.legend([p1,p2], \
              ['Vertical reaction force (averaged normal)','Horizontal reaction force (averaged normal)'], \
              loc=9,bbox_to_anchor=(0.5,0.85),prop={'size':font_size})

    plt.show()

#------------------------------------------------------------------
def readModel( file_name ):
    solver = Solver()
    model_handler = solver.createModelHandler(file_name,IDObject.ID.XML,True)
    model_handler.read()
    model_handler.readResults()
    return solver, model_handler

#------------------------------------------------------------------
def plotDeformation(model_handler, time_stamp_index, axes):
    # time_stamps = [0,10,120,230,285,340]
    # time_stamps = [0,65,120,175]
    # path = os.path.join( output_dir , problem_name + "_" + str(time_stamp_index) + ".pdf" )
    time_stamp = model_handler.getModel().getTimeBar().getTimeStamp( time_stamp_index )
    # print "Plotting time stamp "+str(time_stamp.index())+" at time "+str(time_stamp.time())
    for edge in model_handler.getModel().getEdgeIterator():
        edge.plot( axes , time_stamp , 1. , lineweight=0.5 )

#------------------------------------------------------------------
def initMovieFigure():
    fig = plt.figure( figsize=(4.5,2.8), dpi=204.8, facecolor='white',
                      subplotpars=SubplotParams(bottom=0.01,top=0.98,left=0.13,right=0.98,hspace=0.24) )
    axes_force = fig.add_subplot(211)
    for key,spine in axes_force.spines.iteritems():
        spine.set_linewidth(0.5)
    axes_force.set_xlim(0,16)
    axes_force.set_ylim(-1500,3000)
    plt.xlabel('Time $t$ [sec]')
    plt.ylabel('Reaction Forces [N]')

    axes_def = fig.add_subplot(212)
    axes_def.set_xticks([])
    axes_def.set_yticks([])
    axes_def.set_aspect(aspect='equal')
    axes_def.set_autoscale_on(False)
    axes_def.set_xlim(-30,330)
    axes_def.set_ylim(-10,120)
    for key, spine in axes_def.spines.iteritems():
        spine.set_visible(False)

    return [fig, axes_force, axes_def]

#------------------------------------------------------------------
if __name__ == '__main__':
    mu = True
    fine_mesh = False
    output_dir = '/afs/fest.tugraz.at/user/mueli/vcs/git/rigorosum/images/deep_ironing_pdf'

    if mu:
        problem_name = 'deep_ironing_mu03'
        filename = 'deep_ironing_FLGM_mu03.xml'
    else:
        problem_name = 'deep_ironing'
        filename = 'deep_ironing_FLGM.xml'

    solver = Solver()
    model_handler = solver.createModelHandler(filename,IDObject.ID.XML,True)
    model_handler.read()
    model_handler.readResults()

    force = loadForces( model_handler , fine_mesh )
    for time_stamp in model_handler.getModel().getTimeBar().getTimeStampIterator():
        print "timestamp: "+str(time_stamp.index())+" | "+str(model_handler.getModel().getTimeBar().getLastFinishedTimeStamp().index())+"\r",
        sys.stdout.flush()
        output_path = os.path.join( output_dir , problem_name + "_" + str(time_stamp.index()) + ".pdf" )

        [fig, axes_force, axes_def] = initMovieFigure()

        plotDeformation(model_handler, time_stamp.index(), axes_def)
        p1, = axes_force.plot( force[2][:time_stamp.index()], force[1][:time_stamp.index()] , 'r-')
        p2, = axes_force.plot( force[2][:time_stamp.index()], force[0][:time_stamp.index()] , 'b-' )

        fig.savefig( output_path, format='PDF', bbox_inches='tight', dpi=204.8 )
        fig.clear()

#------------------------------------------------------------------
class Mesher:
    model_handler = None
    xml_data='<data>\
  <time_bar>\
  <te N="1" end_time="1" step_amount="10">\
  <progress N="1" dof="1" id="const"/>\
  <progress N="2" dof="2 3" id="ramp"/>\
  </te>\
  <te N="2" end_time="11" step_amount="220">\
  <progress N="3" dof="1" id="const"/>\
  <progress N="4" dof="3 4" id="ramp"/>\
  </te>\
  <te N="3" end_time="16" step_amount="110">\
  <progress N="3" dof="1" id="const"/>\
  <progress N="4" dof="4 5" id="ramp"/>\
  </te>\
  </time_bar>\
</data>'
    xml_data_tree = ElementTree.fromstring(xml_data)

    def __init__( self, filename , version = '2x2' , corner_type = 'rounded' ):
        self.version_ = version

        if version == '8x8':
            self.l_split_slab = 160
            self.h_split_slab = 30
            self.l_split_block = 8
            self.h_split_block = 8
        if version == '4x4':
            self.l_split_slab = 80
            self.h_split_slab = 20
            self.l_split_block = 4
            self.h_split_block = 4
        if version == '2x2':
            self.l_split_slab = 30
            self.h_split_slab = 10
            self.l_split_block = 2
            self.h_split_block = 2
        if version == '1x1':
            self.l_split_slab = 15
            self.h_split_slab = 5
            self.l_split_block = 1
            self.h_split_block = 1

        self.solver = Solver()
        self.model_handler = self.solver.createModelHandler( filename , IDObject.ID.XML, False )
        self.model_handler.getModel().setGlobal(2,IDObject.ID.CARTESIAN,True,False,IDObject.ID.MORTAR_FIXED_POINT,1e-18,IDObject.ID.LU)
        self.model_handler.getModel().addElementType(1, IDObject.ID.QUAD, IDObject.ID.MORTAR_FLGA, 1, [2,2], [10] )
        self.model_handler.getModel().getElementType(1).addProperty("height", 1.0)
        self.model_handler.getModel().addMaterial(1, IDObject.ID.NEO_HOOKEAN )
        self.model_handler.getModel().getMaterial(1).addProperty("youngs_modulus", 1.e2)
        self.model_handler.getModel().getMaterial(1).addProperty("poisson_ratio", 0.3)
        self.model_handler.getModel().addMaterial(2, IDObject.ID.NEO_HOOKEAN )
        self.model_handler.getModel().getMaterial(2).addProperty("youngs_modulus", 1.e3)
        self.model_handler.getModel().getMaterial(2).addProperty("poisson_ratio", 0.3)
        # Contact
        # Prescribed DOF/Load
        # TimeBar

        mesh = Mesh()
        if( corner_type == 'rounded' ):
            self.createRoundedMesh( mesh )
            # mesh.plot()
        else:
            raise BaseException('Not Implemented!')

        mesh.addToModel( self.model_handler.getModel() )
        self.model_handler.write()
        self.model_handler.flush()

    def createRoundedMesh(self, mesh):
        slab_rectangle = Rectangle( [0,0] , 300 , 80 , self.l_split_slab , self.h_split_slab )
        slab_rectangle.appendNodes( mesh )
        slab_rectangle.appendElements( mesh , material_number=1 )
        slab_rectangle.appendBoundaries( mesh )

        block_rectangle_1 = Rectangle( [10,91] , 30 , 20 , self.l_split_block*3 , self.h_split_block*2 )
        block_rectangle_1.appendNodes( mesh )
        block_rectangle_1.appendElements( mesh , material_number=2 )

        block_rectangle_2 = Rectangle( [20,81] , 10 , 10 , self.l_split_block , self.h_split_block )
        block_rectangle_2.appendNodes( mesh )
        block_rectangle_2.appendElements( mesh , material_number=2 )

        quarter_circle_1 = QuarterCircle( self.version_ )
        quarter_circle_1.move([10,81])
        quarter_circle_1.appendNodes( mesh )
        quarter_circle_1.appendElements( mesh , material_number=2 )

        quarter_circle_2 = QuarterCircle( self.version_ )
        quarter_circle_2.move([10,81])
        quarter_circle_2.mirror( 'y', 25 )
        quarter_circle_2.appendNodes( mesh )
        quarter_circle_2.appendElements( mesh , material_number=2 )

        mesh.addBoundary( 'arc',  mesh.getBoundaryAmount()+1, quarter_circle_1.getBoundaryNodes('arc') )
        mesh.addBoundary( 'line', mesh.getBoundaryAmount()+1, block_rectangle_2.getBoundaryNodes('bottom') )
        mesh.addBoundary( 'arc',  mesh.getBoundaryAmount()+1, quarter_circle_2.getBoundaryNodes('arc') )
        mesh.addBoundary( 'line', mesh.getBoundaryAmount()+1, block_rectangle_1.getBoundaryNodes('right') )
        mesh.addBoundary( 'line', mesh.getBoundaryAmount()+1, block_rectangle_1.getBoundaryNodes('top') )
        mesh.addBoundary( 'line', mesh.getBoundaryAmount()+1, block_rectangle_1.getBoundaryNodes('left') )

        mesh.concentrateNodes()

    def createSharp(self):
        pass
