length = 100.;
mesh_div = 7;

Point(1) = {0, 0, 0, 1.0};
Point(2) = {length, 0, 0, 1.0};
Line(1) = {1,2};
Transfinite Line {1} = mesh_div+1;

surface[] = Extrude{0,length,0} {
	Line{1}; Layers{mesh_div}; Recombine;
};

Physical Line("mesh") = {1,surface[2],-surface[0],surface[3]};
Physical Surface("mesh") = {surface[1]};
