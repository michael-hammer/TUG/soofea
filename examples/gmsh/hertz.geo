radius = 50;
width_angle = 20/180*Pi;
refine_angle = 5/180*Pi;
l = radius*Sin(width_angle);
l2 = radius*Sin(refine_angle);

cl1 = 10;
cl2 = 0.1;

Point(1) = {-l, radius/2, 0, cl1};
Point(2) = { l, radius/2, 0, cl1};
Point(3) = { 0,   radius, 0, cl1};

Point(4) = { -l, radius*(1-Cos(width_angle)), 0, cl1};
Point(5) = {-l2, radius*(1-Cos(refine_angle)), 0, cl2};
Point(6) = { l2, radius*(1-Cos(refine_angle)), 0, cl2};
Point(7) = {  l, radius*(1-Cos(width_angle)), 0, cl1};

Point(8)  = {  l,        0, 0, cl1};
Point(9)  = { l2,        0, 0, cl2};
Point(10) = {-l2,        0, 0, cl2};
Point(11) = { -l,        0, 0, cl1};
Point(12) = { -l,-radius/2, 0, cl1};
Point(13) = {  l,-radius/2, 0, cl1};
 
Line(1) = {1,4};
Circle(2) = {4, 3, 5};
Circle(3) = {5, 3, 6};
Circle(4) = {6, 3, 7};
Line(5) = {7,2};
Line(6) = {2,1};

Line(7) = {8,9};
Line(8) = {9,10};
Line(9) = {10,11};
Line(10) = {11,12};
Line(11) = {12,13};
Line(12) = {13,8};

// Top Mesh
Line Loop(20) = {1,2,3,4,5,6};
Plane Surface(20) = {20};
Physical Line("top_mesh") = {1,2,3,4,5,6};
Physical Surface("top_mesh") = {20};

//Bottom Mesh
Line Loop(21) = {7,8,9,10,11,12};
Plane Surface(21) = {21};
Physical Line("bottom_mesh") = {7,8,9,10,11,12};
Physical Surface("bottom_mesh") = {21};

Mesh.Algorithm = 5;
Mesh.RecombineAll = 1;
Mesh.SubdivisionAlgorithm = 1;
Mesh.SaveGroupsOfNodes = 1;
