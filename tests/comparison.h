#ifndef comparison_h___
#define comparison_h___

#include <cfloat>
#include <cmath>
#include <iostream>

#define PREC_FACT 5

//------------------------------------------------------------------
template<class T>
class Comparison
{
public:
  bool equal(T a, T b);
  bool zero(T a);

};

template<>
bool Comparison<double>::equal(double a, double b)
{
  //std::cout << fabs(a - b) << "  " << ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * PREC_FACT * DBL_EPSILON) << std::endl;
  return( fabs(a - b) <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * PREC_FACT * DBL_EPSILON) );
}

template<>
bool Comparison<float>::equal(float a, float b)
{
  return( fabs(a - b) <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * PREC_FACT * FLT_EPSILON) );
}

template<class T>
bool Comparison<T>::equal(T a, T b)
{
  return( a == b );
}

template<>
bool Comparison<double>::zero(double a)
{
  return( fabs(a) <= PREC_FACT * DBL_EPSILON );
}

template<>
bool Comparison<float>::zero(float a)
{
  return( fabs(a) <= PREC_FACT * FLT_EPSILON );
}

template<class T>
bool Comparison<T>::zero(T a)
{
  return( a == 0 );
}

#endif //comparison_h___
