#include <iostream>

extern "C"
{
  extern void printarray2d_(double* A, unsigned* m, unsigned* n);
  extern void printarray3d_(double* A, unsigned* m, unsigned* n, unsigned* o);
  extern void array2d_(double* A, unsigned* m, unsigned* n);
  extern void array3d_(double* A, unsigned* m, unsigned* n, unsigned* o);
}

//#define SHOW_2D

int main()
{
  unsigned m = 3;
  unsigned n = 2;
  unsigned o = 4;

#ifdef SHOW_2D

  std::cout << "Set Array in C++ A("<<m<<","<<n<<") [A(m,n)]"<< std::endl;
  double B[m][n];
  double *B_one = (double*)B;
  for( unsigned i = 0 ; i < m ; i++ )
    {
      for( unsigned j = 0 ; j < n ; j++ )
	{
	  B[i][j] = (i+1)*10+(j+1);
	}
    }

  std::cout << "Memory print:" << std::endl;
  for( unsigned count = 0 ; count < m*n ; count++)
    std::cout << B_one[count] << std::endl;
  printarray2d_(B_one,&m,&n);  

  array2d_(B_one,&m,&n);
  std::cout << "Memory print:" << std::endl;
  for( unsigned count = 0 ; count < m*n ; count++)
    std::cout << B_one[count] << std::endl;
  std::cout << "C++ A[i][j]:" << std::endl;
  for( unsigned i = 0 ; i < m ; i++ )
    {
      for( unsigned j = 0 ; j < n ; j++ )
	{
	  std::cout << "("<<(i+1)<<","<<(j+1)<<") = "<< B[i][j] << std::endl;
	}
    }

#else

  std::cout << "Set Array in C++ A("<<m<<","<<n<<","<<o<<") [A(m,n,o)]"<< std::endl;
  double A[m][n][o];
  double *A_one = (double*)A;
  for( unsigned i = 0 ; i < m ; i++ )
    {
      for( unsigned j = 0 ; j < n ; j++ )
	{
	  for( unsigned k = 0 ; k < o ; k++ )
	    {
	      A[i][j][k] = (i+1)*100+(j+1)*10+(k+1);
	    }
	}
    }
  std::cout << "Memory print:" << std::endl;
  for( unsigned count = 0 ; count < m*n*o ; count++)
    std::cout << A_one[count] << std::endl;
  printarray3d_(A_one,&m,&n,&o);

  array3d_(A_one,&m,&n,&o);
  std::cout << "Memory print:" << std::endl;
  for( unsigned count = 0 ; count < m*n*o ; count++)
    std::cout << A_one[count] << std::endl;
  std::cout << "C++ A[i][j][k]:" << std::endl;
  for( unsigned i = 0 ; i < m ; i++ )
    {
      for( unsigned j = 0 ; j < n ; j++ )
	{
	  for( unsigned k = 0 ; k < o ; k++ )
	    {
	      std::cout << "("<<(i+1)<<","<<(j+1)<<","<<(k+1)<<") = "<< A[i][j][k] << std::endl;
	    }
	}
    }

#endif
}
