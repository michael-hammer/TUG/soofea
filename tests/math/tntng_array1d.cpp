#include <iostream>
#include <tntng/tntng_array1d.h>

int main()
{
  unsigned size=5;

  // Test uninitialized constructor
  TNTng::Array1D<double> array1(size);

  // Test initialized constructor
  TNTng::Array1D<double> array2(size,1.);

  // Test operator[] and operator()
  for( unsigned count=0 ; count < size; count++)
    {
      if( array2[count] != 1.)
	return -1;
      if( array2(count+1) != 1.)
	return -1;
    }

  // Test constructor with data_array
  double *data = new double[size];
  for( unsigned count=0 ; count < size; count++)
    {
      data[count] = (double)(count+1);
    }
  TNTng::Array1D<double> array3(size,data);
  for( unsigned count=0 ; count < size; count++)
    {
      if( array3[count] != (double)(count+1))
	return -1;
      if( array3(count+1) != (double)(count+1))
	return -1;
    }

  // Test Copy constructor()
  TNTng::Array1D<double> array5(array2);  
  for( unsigned count=0 ; count < size; count++)
    {
      if( array5[count] != 1.)
	return -1;
    }

  // Test operator T*()
  double* array3_data(array3);
  for( unsigned count=0 ; count < size; count++)
    {
      if( array3_data[count] != (count+1) )
	return -1;
    }

  // Test write operation on elements
  for( unsigned count=0 ; count < size; count++)
    {
      array3[count] = (double)count;
      if( array3[count] != (double)count )
	return -1;
      array3(count+1) = (double)count;
      if( array3[count] != (double)count )
	return -1;
    }

  // Test operator= with Array assignement in Construction
  TNTng::Array1D<double> array4 = array3;
  for( unsigned count=0 ; count < size; count++)
    {
      if( array4[count] != (double)count )
	return -1;
    }

  // Test operator= with Array assignement
  array5 = array4;
  for( unsigned count=0 ; count < size; count++)
    {
      if( array5[count] != (double)count )
	return -1;
    }

  // Test operator= with constant assignement
  array5 = 2.;
  for( unsigned count=0 ; count < size; count++)
    {
      if( array5[count] != 2.)
	return -1;
    }

  // Test .copy()

  // Test prettyPrint()
  array5.prettyPrint();

  return 0;
}
