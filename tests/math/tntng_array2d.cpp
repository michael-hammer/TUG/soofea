#include <iostream>
#include "tntng/tntng_array2d.h"

int main()
{
  unsigned m=3;
  unsigned n=3;

  // Test uninitialized constructor
  TNTng::Array2D<double> array1(m,n);

  // Test initialized constructor
  TNTng::Array2D<double> array2(m,n,1.);

  // Test operator[] and operator()
  for( unsigned i=0 ; i < m; i++ )
    {
      for( unsigned j=0 ; j < n ; j++ )
	{
	  if( array2(i+1,j+1) != 1.)
	    return -1;
	  array2(i+1,j+1) = (i+1)*(j+1);
	  if( array2(i+1,j+1) != (i+1)*(j+1) )
	    return -1;
	}
    }

  // Test constructor with data_array
  double *data = new double[m*n];
  for( unsigned count=0 ; count < m*n; count++ )
    {
      data[count] = (double)(count+1);
    }
  TNTng::Array2D<double> array3(m,n,data);

  // Check ROW vs. COLUMN Major
  for( unsigned i=0 ; i < m; i++ )
    {
      for( unsigned j=0 ; j < n ; j++ )
	{	  
#ifdef TNTNG_ROW_MAJOR
	  if( array3(i+1,j+1) != (j+1+i*n) )
	      return -1;
#endif
#ifdef TNTNG_COLUMN_MAJOR
	  if( array3(i+1,j+1) != (i+1+j*m) )
	      return -1;
#endif
	}
    }

  // Test Copy constructor()
  TNTng::Array2D<double> array4(array3);

  // Test operator T*()
  double* array3_data(array3);
  for( unsigned count = 0 ; count < m*n ; count++ )
    {
      if( array3_data[count] != (count+1) )
	return -1;
    }

  // Test operator= with Array assignement in Construction
  TNTng::Array2D<double> array5 = array3;

  // Test operator= with Array assignement

  // Test operator= with constant assignement
  // Test .copy()

  // Test prettyPrint()
  array3.prettyPrint();

  return 0;
}

