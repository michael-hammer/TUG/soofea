#include "test_tensor.h"

SOOFEA_USING_NAMESPACE(SoofeaTest)
  
//------------------------------------------------------------------
TestTensor::TestTensor()
{}

//------------------------------------------------------------------
TestTensor::~TestTensor()
{}

//------------------------------------------------------------------
void TestTensor::testTensor4dminorSym()
{
  std::cout << "---" << std::endl;
  std::cout << "testTensor4dminorSym" << std::endl;
  
  unsigned long dim_a = 3;
  unsigned long dim_b = 3;
  unsigned long dim_c = 3;
  unsigned long dim_d = 3;

  unsigned long size2[] = {dim_a, dim_b};
  unsigned long size4[] = {dim_a, dim_b, dim_c, dim_d};

  // minor symmetries
  double A[] = {
    2.20741493e+00,1.23074700e-01,6.20517968e-01,1.23074700e-01,-4.52341708e-01,2.49691640e-02,6.20517968e-01,2.49691640e-02,-9.23564826e-01,
    7.73064688e-02,1.18651803e+00,-6.29275639e-02,1.18651803e+00,-3.51766192e-02,-2.53215660e-03,-6.29275639e-02,-2.53215660e-03,-5.35856001e-02,
    4.15484169e-01,-6.70801443e-02,7.18465814e-01,-6.70801443e-02,-1.89056991e-01,-1.36090937e-02,7.18465814e-01,-1.36090937e-02,-2.87996191e-01,
    7.73064688e-02,1.18651803e+00,-6.29275639e-02,1.18651803e+00,-3.51766192e-02,-2.53215660e-03,-6.29275639e-02,-2.53215660e-03,-5.35856001e-02,
    -5.41004635e-01,-6.19797319e-02,-3.12489386e-01,-6.19797319e-02,1.10924114e+00,-1.25743317e-02,-3.12489386e-01,-1.25743317e-02,4.97218445e-01,
    1.78999792e-02,-2.88996133e-03,-1.45706058e-02,-2.88996133e-03,-8.14499434e-03,8.70646999e-01,-1.45706058e-02,8.70646999e-01,-1.24075144e-02,
    4.15484169e-01,-6.70801443e-02,7.18465814e-01,-6.70801443e-02,-1.89056991e-01,-1.36090937e-02,7.18465814e-01,-1.36090937e-02,-2.87996191e-01,
    1.78999792e-02,-2.88996133e-03,-1.45706058e-02,-2.88996133e-03,-8.14499434e-03,8.70646999e-01,-1.45706058e-02,8.70646999e-01,-1.24075144e-02,
    -6.66410292e-01,-6.10949679e-02,-3.08028583e-01,-6.10949679e-02,3.43100567e-01,-1.23948324e-02,-3.08028583e-01,-1.23948324e-02,1.42634638e+00};
  
//   // major and minor symmetries
//   double AA[] = {
//     1.79810680e+05,3.81095925e+02,1.91920908e+03,3.81095925e+02,1.66730254e+05,1.18463959e+02,1.91920908e+03,1.18463959e+02,1.67048587e+05,
//     3.81095925e+02,5.55772650e+03,1.18452144e+03,5.55772650e+03,-1.14326744e+03,-6.14598381e+01,1.18452144e+03,-6.14598381e+01,-9.12028424e+02,
//     1.91920908e+03,1.18452144e+03,1.12947438e+04,1.18452144e+03,-5.49357881e+03,3.06268850e+02,1.12947438e+04,3.06268850e+02,-4.84067870e+03,
//     3.81095925e+02,5.55772650e+03,1.18452144e+03,5.55772650e+03,-1.14326744e+03,-6.14598381e+01,1.18452144e+03,-6.14598381e+01,-9.12028424e+02,
//     1.66730254e+05,-1.14326744e+03,-5.49357881e+03,-1.14326744e+03,1.84738997e+05,-3.20511797e+02,-5.49357881e+03,-3.20511797e+02,1.72924649e+05,
//     1.18463959e+02,-6.14598381e+01,3.06268850e+02,-6.14598381e+01,-3.20511797e+02,5.48281886e+03,3.06268850e+02,5.48281886e+03,-2.70298935e+02,
//     1.91920908e+03,1.18452144e+03,1.12947438e+04,1.18452144e+03,-5.49357881e+03,3.06268850e+02,1.12947438e+04,3.06268850e+02,-4.84067870e+03,
//     1.18463959e+02,-6.14598381e+01,3.06268850e+02,-6.14598381e+01,-3.20511797e+02,5.48281886e+03,3.06268850e+02,5.48281886e+03,-2.70298935e+02,
//     1.67048587e+05,-9.12028424e+02,-4.84067870e+03,-9.12028424e+02,1.72924649e+05,-2.70298935e+02,-4.84067870e+03,-2.70298935e+02,1.83105612e+05};
  
  // minor symmetries
  double B[] = {
    8.07737369e+00,2.65854907e-01,1.56508049e+00,2.65854907e-01,-2.73447815e+00,7.89414174e-02,1.56508049e+00,7.89414174e-02,-5.33944750e+00,
    1.63550250e-01,3.23202607e+00,-3.28752006e-01,3.23202607e+00,-9.51713076e-02,-1.65819901e-02,-3.28752006e-01,-1.65819901e-02,-9.48447440e-02,
    1.02635187e+00,-3.50446300e-01,6.57142136e-01,-3.50446300e-01,-5.97243048e-01,-1.04059496e-01,6.57142136e-01,-1.04059496e-01,-5.95193713e-01,
    1.63550250e-01,3.23202607e+00,-3.28752006e-01,3.23202607e+00,-9.51713076e-02,-1.65819901e-02,-3.28752006e-01,-1.65819901e-02,-9.48447440e-02,
    -3.21535582e+00,-1.61669575e-01,-9.51744323e-01,-1.61669575e-01,1.85686903e+00,-4.80052279e-02,-9.51744323e-01,-4.80052279e-02,2.76981568e+00,
    5.54259217e-02,-1.89250974e-02,-1.11411526e-01,-1.89250974e-02,-3.22528240e-02,1.97499767e+00,-1.11411526e-01,1.97499767e+00,-3.21421542e-02,
    1.02635187e+00,-3.50446300e-01,6.57142136e-01,-3.50446300e-01,-5.97243048e-01,-1.04059496e-01,6.57142136e-01,-1.04059496e-01,-5.95193713e-01,
    5.54259217e-02,-1.89250974e-02,-1.11411526e-01,-1.89250974e-02,-3.22528240e-02,1.97499767e+00,-1.11411526e-01,1.97499767e+00,-3.21421542e-02,
    -3.86201786e+00,-1.04185332e-01,-6.13336171e-01,-1.04185332e-01,1.87760912e+00,-3.09361895e-02,-6.13336171e-01,-3.09361895e-02,3.56963182e+00};
  
  double a[] = {
    4.5 , 3.3 , 5.5,
    3.3 , 1.3 , 5.2,
    5.5 , 5.2 , 7.5};
  
  Math::Tensor<double> iden4d(size4, 4); 
  iden4d.identity4d();

  Math::Tensor<double> Tens4dA(size4, 4); 
  Tens4dA.setArrayData(A);
  cout << "Tens4dA ABCD" << endl;
  Tens4dA("ABCD").prettyPrint("ABCD");
  
  Math::Tensor<double> Tens4dAinv(size4, 4);
  Tens4dAinv = Tens4dA.inverse4d();
  Tens4dAinv("ABCD");
  cout << "Tens4dAinv ABCD" << endl;
  Tens4dAinv("ABCD").prettyPrint("ABCD");

  Math::Tensor<double> Tens4dB(size4, 4); 
  Tens4dB.setArrayData(B);
  cout << "Tens4dB ABCD" << endl;
  Tens4dB("ABCD").prettyPrint("ABCD");
  
  Math::Tensor<double> Tens4dBinv(size4, 4);
  Tens4dBinv = Tens4dB.inverse4d();
  Tens4dBinv("ABCD");
  cout << "Tens4dBinv ABCD" << endl;
  Tens4dBinv("ABCD").prettyPrint("ABCD");

  Math::Tensor<double> C(size4, 4);
  C("ijmn").mul(Tens4dA("ijkl"),Tens4dB("klmn"));
  //cout << "C ijmn" << endl;
  //C.prettyPrint("ijmn");
  C += iden4d;

  Math::Tensor<double> Cinv(size4, 4);
  Cinv = C.inverse4d();
  Cinv("ijmn");
  cout << "Cinv ijmn" << endl;
  Cinv("ijmn").prettyPrint("ijmn");

  Math::Tensor<double> Cinv2(size4, 4);
  Cinv2("mnij").mul(Tens4dBinv("mnkl"),Tens4dAinv("klij"));
  Cinv2 += iden4d;

  cout << "Cinv ijmn" << endl;
  Cinv2("ijmn").prettyPrint("ijmn");


  Math::Tensor<double> BinvA(size4, 4);
  BinvA = Tens4dBinv;
  BinvA += Tens4dA;
  Math::Tensor<double> BinvAinv(size4, 4);
  BinvAinv = BinvA.inverse4d();
  BinvAinv("ijmn");
  //cout << "BinvAinv ijmn" << endl;
  //BinvAinv("ijmn").prettyPrint("ijmn");

  Math::Tensor<double> BBA(size4, 4);
  BBA("ijmn").mul(Tens4dBinv("ijkl"),BinvAinv("klmn"));
  cout << "test ijmn" << endl;
  BBA.prettyPrint("ijmn");




// 
// 
// 
//   Math::Tensor<double> inv2test(size4, 4);
//   inv2test("ABCD").mul(Tens4d("ABmn"),inv2("DCnm"));
//   cout << "inv2 ABCD" << endl;
//   inv2test("ABCD").prettyPrint("CDAB");
// 
//   unsigned long size66[] = {6,6};
//   Math::Tensor<double> test166(size66, 2);
//   test166.convert4dSymTo2d(Tens4d);
//   cout << "test166" << endl;
//   test166.prettyPrint();
//   Math::Tensor<double> test266(size66, 2);
//   test266.convert4dSymTo2d(inv1);
//   cout << "test266" << endl;
//   test266.prettyPrint();
// 
//   Math::Tensor<double> W(size66, 2);
//   unsigned long pos[] = {0,0};
//   W.write(pos, 1.0);
//   pos[0] = 1; pos[1] = 1;
//   W.write(pos, 1.0);
//   pos[0] = 2; pos[1] = 2;
//   W.write(pos, 1.0);
//   pos[0] = 3; pos[1] = 3;
//   W.write(pos, sqrt(2.0));
//   pos[0] = 4; pos[1] = 4;
//   W.write(pos, sqrt(2.0));
//   pos[0] = 5; pos[1] = 5;
//   W.write(pos, sqrt(2.0));
// 
//   Math::Tensor<double> test166M(size66, 2);
//   Math::Tensor<double> dum(size66, 2);
//   dum("ab").mul(test166("ac"),W("cb"));
//   test166M("ac").mul(W("ab"),dum("bc"));
//   cout << "test166 Miehe" << endl;
//   test166M.prettyPrint();
//   Math::Tensor<double> test266M(size66, 2);
//   dum("ab").mul(test266("ac"),W("cb"));
//   test266M("ac").mul(W("ab"),dum("bc"));
//   cout << "test266 Miehe" << endl;
//   test266M.prettyPrint();
// 
//   Math::Tensor<double> invTaTb(size4, 4);
//   invTaTb = TaTb.inverse4d();
//   invTaTb("ABCD");
//   cout << "invTaTb ABCD" << endl;
//   invTaTb("ABCD").prettyPrint("ABCD");
// 
//   Math::Tensor<double> invTa(size4, 4);
//   invTa = Ta.inverse4d();
//   invTa("ABCD");
//   //cout << "invTa ABCD" << endl;
//   //invTa("ABCD").prettyPrint("ABCD");
//   Math::Tensor<double> invTb(size4, 4);
//   invTb = Tb.inverse4d();
//   invTb("ABCD");
//   //cout << "invTb ABCD" << endl;
//   //invTb("ABCD").prettyPrint("ABCD");
// 
//   Math::Tensor<double> invTaTb2(size4, 4);
//   invTaTb2("ijkl").mul(invTb("ijmn"),invTa("mnkl"));
//   cout << "invTaTb2 ABCD" << endl;
//   invTaTb2("ABCD").prettyPrint("ABCD");
// 
//   invTaTb2.scale(-1.0);
//   Math::Tensor<double> testtt(size4, 4);
//   testtt("ABCD").add(invTaTb("ABCD"),invTaTb2("ABCD"));
//   cout << "test ABCD" << endl;
//   testtt("ABCD").prettyPrint("ABCD");

  // test
  cout << "Test Inverse of 4dAsym" << endl;
  Math::Tensor<double> TestA(size4, 4);
  TestA.setArrayData(A);
  cout << "TestA ABCD" << endl;
  TestA("ABCD").prettyPrint("ABCD");

  Math::Tensor<double> Testa(size2, 2);
  Testa.setArrayData(a);
  cout << "Testa AB" << endl;
  Testa("AB").prettyPrint();

  Math::Tensor<double> Testb(size2, 2);
  Testb("kl").mul(Testa("ij"),TestA("ijkl"));
  cout << "Testa * TestA AB" << endl;
  Testb("AB").prettyPrint();

  Math::Tensor<double> TestAinv(size4, 4);
  TestAinv = TestA.inverse4d();
  TestAinv("ABCD");
  cout << "TestAinv ABCD" << endl;
  TestAinv("ABCD").prettyPrint("ABCD");

  Math::Tensor<double> Testa2(size2, 2);
  Testa2("kl").mul( Testb("ij"),TestAinv("ijkl"));
  cout << "Testb * TestAinv AB" << endl;
  Testa2("AB").prettyPrint();

  CPPUNIT_ASSERT( 0 < EPS);

  unsigned long size66[] = {6,6};
  Math::Tensor<double> T66(size66, 2);
  Math::Tensor<double> T66inv(size66, 2);
  T66.convert4dSymTo2d(Tens4dA);
  //cout << "T66" << endl;
  //T66.prettyPrint();
  Math::LinAlg lin_alg;
  lin_alg.lu_inverse(T66,T66inv);
  cout << "T66inv" << endl;
  T66inv.prettyPrint();
  
  Math::Tensor<double> Tens4dAinv2(size4, 4);
  Tens4dAinv2 = Tens4dA.inverse4d();
  Tens4dAinv2("ABCD");
  //cout << "Tens4dAinv ABCD" << endl;
  //Tens4dAinv("ABCD").prettyPrint("ABCD");
  Math::Tensor<double> T66inv2(size66, 2);
  T66inv2.convert4dSymTo2d(Tens4dAinv2);
  cout << "T66inv2" << endl;
  T66inv2.prettyPrint();
}

//------------------------------------------------------------------
void TestTensor::testTensor4dmajorSym()
{
  std::cout << "---" << std::endl;
  std::cout << "testTensor4dmajorSym" << std::endl;

  unsigned long dim_a = 3;
  unsigned long dim_b = 3;
  unsigned long dim_c = 3;
  unsigned long dim_d = 3;

  unsigned long size4[] = {dim_a, dim_b, dim_c, dim_d};
  Math::Tensor<double> Tens4d(size4, 4);
  Math::Tensor<double> result4d(size4, 4);
  Math::Tensor<double> identity(size4, 4);
  unsigned long pos4[] = {0,0,0,0};
  double value = 0;

  for ( unsigned long count1 = 1; count1 < dim_a+1; count1++)
    {
      for ( unsigned long count2 = 1; count2 < dim_b+1; count2++)
	{
	  for ( unsigned long count3 = 1; count3 < dim_c+1; count3++)
	    {
	      for ( unsigned long count4 = 1; count4 < dim_d+1; count4++)
		{ 
		  pos4[0] = count1-1; pos4[1] = count2-1; pos4[2] = count3-1; pos4[3] = count4-1;
		  value = 0;
		  if (count1 == count2)
		    value += 40;
		  if (count3 == count4)
		    value += 40;
		  if ( (count1 == count2) && (count3 == count4) )
		    value += 66;
		  Tens4d.write(pos4, value);
		}
	    }
	}
    }

  cout << "Tens4d ABCD" << endl;
  Tens4d("ABCD").prettyPrint("ABCD");
  cout << "Tens4d DCAB" << endl;
  Tens4d("ABCD").prettyPrint("DCAB");
  
  identity.identity4d();

  cout << "identity ABCD" << endl;
  identity("ABCD").prettyPrint("ABCD");
  
  result4d("ABCD").mul(Tens4d("ABmn"),identity("mnCD"));
  cout << "result4d ABCD = Tens4d ABmn * identity mnCD" << endl;
  result4d("ABCD").prettyPrint("ABCD");

  double res = 0;
  result4d.scale(-1.0);
  identity("ABCD").add(Tens4d("ABCD"),result4d("ABCD"));

  for ( unsigned long count1 = 1; count1 < dim_a+1; count1++)
    {
      for ( unsigned long count2 = 1; count2 < dim_b+1; count2++)
	{
	  for ( unsigned long count3 = 1; count3 < dim_c+1; count3++)
	    {
	      for ( unsigned long count4 = 1; count4 < dim_d+1; count4++)
		{ 
		  pos4[0] = count1-1; pos4[1] = count2-1; pos4[2] = count3-1; pos4[3] = count4-1;
		  res += fabs( identity.get(pos4) );
		}
	    }
	}
    }
  cout << "result: " << res << ", should be 0" << endl;
  CPPUNIT_ASSERT( res < EPS);
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( SoofeaTest::TestTensor::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
