#include "test_num_int.h"

SOOFEA_USING_NAMESPACE(SoofeaTest)

const std::string TestNumInt::DOC_NAME = "conf_test_num_int";
  
//------------------------------------------------------------------
TestNumInt::TestNumInt()
{}

//------------------------------------------------------------------
TestNumInt::~TestNumInt()
{}

//------------------------------------------------------------------
void TestNumInt::scalar3D(Math::IntegrationPoint* int_point, double* value)
{
  *value = int_point -> r() * int_point -> s() * int_point -> t() ;
}

//------------------------------------------------------------------
void TestNumInt::scalar2D(Math::IntegrationPoint* int_point, double* value)
{
  *value = int_point -> r() * int_point -> s() ;
}

//------------------------------------------------------------------
void TestNumInt::array2D(Math::IntegrationPoint* int_point, Math::Array<double>* array)
{
  long unsigned size[2];

  for( unsigned row_counter = 0 ; row_counter != (array -> getSize())[0] ; ++row_counter )
    {
      for( unsigned col_counter = 0 ; col_counter != (array -> getSize())[0] ; ++col_counter )
	{
	  size[0] = row_counter;
	  size[1] = col_counter;

	  array -> get(size) = int_point -> r() * int_point -> s() ;
	}
    }
}

//------------------------------------------------------------------
void TestNumInt::array3D(Math::IntegrationPoint* int_point, Math::Array<double>* array)
{
  long unsigned size[2];

  for( unsigned row_counter = 0 ; row_counter != (array -> getSize())[0] ; ++row_counter )
    {
      for( unsigned col_counter = 0 ; col_counter != (array -> getSize())[0] ; ++col_counter )
	{
	  size[0] = row_counter;
	  size[1] = col_counter;

	  array -> get(size) = int_point -> r()*int_point -> r()
	                       + int_point -> s()*int_point -> t();
	  //array -> get(size) = int_point -> r() * int_point -> s() * int_point -> t() + 1.;
	}
    }
}

//------------------------------------------------------------------
void TestNumInt::testRectangular()
{
  std::cout << "testRectangular()" << std::endl;

  std::vector<unsigned> int_points;
  int_points.push_back(5);
  int_points.push_back(2);
  int_points.push_back(3);

  double result = 0.;

  try {
    Math::NumInt num_int;

    std::vector<Math::IntegrationPoint*>* int_point_vector = 
      Math::NumIntIO::getInstance().getIntegrationPointsWithPoints(Math::RECTANGULAR,
								   Math::GAUSS_LEGENDRE,
								   int_points);
    Math::NumIntFunctorScalar<TestNumInt> functor_scalar( this, &TestNumInt::scalar3D );
    result = num_int.integrateScalar( functor_scalar, 
				      *int_point_vector );
  } catch(const BaseException& exc) {
    CPPUNIT_FAIL( exc.msg() );
  }

  std::cout << "Rectangular Result = " << result << std::endl;
  CPPUNIT_ASSERT( fabs( result ) < EPSILON );
}

//------------------------------------------------------------------
void TestNumInt::testRectangularArray()
{
  std::cout << "testRectangularArray()" << std::endl;

  unsigned dimension = 2;
  long unsigned size[2] = {3,3};

  std::vector<unsigned> int_points;
  int_points.push_back(5);
  int_points.push_back(8);
  int_points.push_back(2);

  Math::Array<double>* result = 0;

  try {
    Math::NumInt num_int;

    std::vector<Math::IntegrationPoint*>* int_point_vector = 
      Math::NumIntIO::getInstance().getIntegrationPointsWithPoints(Math::RECTANGULAR,
								   Math::GAUSS_LEGENDRE,
								   int_points);
    std::vector<Math::IntegrationPoint*>::iterator int_iter = int_point_vector -> begin();
  
  
    Math::NumIntFunctorArray<TestNumInt> functor_scalar( this, &TestNumInt::array3D );
    result = num_int.integrateArray( functor_scalar, 
				     *int_point_vector,
				     size,
				     dimension);
  } catch(const BaseException& exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
  
  result -> prettyPrint();
}

//------------------------------------------------------------------
void TestNumInt::testTriangle()
{
  std::cout << "testTriangle()" << std::endl;

  std::vector<unsigned> int_points;
  int_points.push_back(3);

  double result = 0;

  try {
    Math::NumInt num_int;

    std::vector<Math::IntegrationPoint*>* int_point_vector = 
      Math::NumIntIO::getInstance().getIntegrationPointsWithPoints(Math::TRIANGLE,
								   Math::GAUSS_LEGENDRE,
								   int_points);

    Math::NumIntFunctorScalar<TestNumInt> functor_scalar( this, &TestNumInt::scalar2D );
    result = num_int.integrateScalar( functor_scalar, 
				      *int_point_vector );
  } catch(const BaseException& exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
  
  std::cout << "Triangle Result = " << result << std::endl;
  CPPUNIT_ASSERT( fabs( result - 1./24. ) < EPSILON );  
}

//------------------------------------------------------------------
void TestNumInt::testTriangleArray()
{
  std::cout << "testTriangleArray()" << std::endl;

  unsigned dimension = 2;
  long unsigned size[2] = {3,3};

  std::vector<unsigned> int_points;
  int_points.push_back(3);

  Math::Array<double>* result = 0;

  try {
    Math::NumInt num_int;

    std::vector<Math::IntegrationPoint*>* int_point_vector = 
      Math::NumIntIO::getInstance().getIntegrationPointsWithPoints(Math::TRIANGLE,
								   Math::GAUSS_LEGENDRE,
								   int_points);
  
    Math::NumIntFunctorArray<TestNumInt> functor_array( this, &TestNumInt::array2D );

    result = num_int.integrateArray( functor_array, 
				     *int_point_vector,
				     size,
				     dimension);
  } catch(const BaseException& exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
  
  result -> prettyPrint();
}

//------------------------------------------------------------------
void TestNumInt::testNumIntHandler()
{
  std::cout << "testNumIntHandler() " << std::endl;
  
  std::vector<unsigned> points_for_each_dimension;
  points_for_each_dimension.push_back( 4 );
  points_for_each_dimension.push_back( 3 );
  points_for_each_dimension.push_back( 8 );

  long unsigned size[2] = {3,3};

  Math::Array<double> array_2D(size,2);
  Math::Array<double> array_3D(size,2);

  try {
    Math::NumIntHandler my_num_int_handler(Math::RECTANGULAR,
					   Math::GAUSS_LEGENDRE,
					   points_for_each_dimension);

    Math::NumIntFunctorArray<TestNumInt> functor_2D( this, &TestNumInt::array2D );
    Math::NumIntFunctorArray<TestNumInt> functor_3D( this, &TestNumInt::array3D );

    my_num_int_handler.addArray(&functor_2D, &array_2D );
    my_num_int_handler.addArray(&functor_3D, &array_3D );

    my_num_int_handler.go();
  } catch(const BaseException& exc) {
    CPPUNIT_FAIL( exc.msg() );
  }

  std::cout << "NumIntHandler test 2D:" << std::endl;
  array_2D.prettyPrint();

  std::cout << "NumIntHandler test 3D:" << std::endl;
  array_3D.prettyPrint();
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  fs::path main_file_path( fs::system_complete( fs::path( argv[0], fs::native ) ).branch_path() );
  fs::path config = main_file_path / fs::path(  TestNumInt::DOC_NAME+".xml" );
  fs::path logging = main_file_path / fs::path( TestNumInt::DOC_NAME+".log" );

  try {
    Logger::createLogger( logging , Logger::DEBUG );
    Configuration::createConfiguration( config , Configuration::RO );
    Logger::setPriority( Configuration::getInstance().getLoggerPriorityLevel() );
  } catch(const IOException& exc) {
    std::cout << exc.msg() << std::endl;
    Configuration::terminate();
    Logger::terminate();
    return( 1 );
  }

  Math::NumIntIO::createNumIntIO(Configuration::getInstance().getMathNumIntConfigFN());

  CppUnit::TextUi::TestRunner runner;
  bool was_successful = false;

  try {
    runner.addTest( SoofeaTest::TestNumInt::suite() );
    was_successful = runner.run( );
  } catch(const BaseException& exc) {
    std::cout << exc.msg() << std::endl;
    was_successful = false;
  }

  Math::NumIntIO::terminate();
  Configuration::terminate();
  Logger::terminate();

  return( was_successful ? 0 : 1 );
}
