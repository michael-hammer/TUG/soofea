#include "test_solver.h"

SOOFEA_USING_NAMESPACE(SoofeaTest)
  
//------------------------------------------------------------------
TestSolver::TestSolver()
{}

//------------------------------------------------------------------
TestSolver::~TestSolver()
{}

//------------------------------------------------------------------
void TestSolver::testCholeskySolver()
{
  std::cout << "---" << std::endl;
  std::cout << "Cholesky Solver" << std::endl;

  Math::LinAlg lin_alg;

  double matrix_data_symmetric[] = {
    1.25,0.25,0.25,
    0.25,2.25,0.25,
    0.25,0.25,3.25};

  double disturbance_data[] = {1.e+01,2.e+01,3.e+00};

  unsigned long size[] = {3,3};
  Math::Matrix2D<double> A(size);
  Math::Vector<double> x(size[0]);
  Math::Vector<double> b(size[0]);
  
  try {
    A.setArrayData(matrix_data_symmetric);
    b.setArrayData(disturbance_data);
    lin_alg.cholesky_solve(A,b,x);
  }
  catch(MathLinAlgException& exc) {
    cout << exc.msg() << endl;
  }
  
  cout << "Matrix A:" << endl;
  A.prettyPrint();
  cout << "Disturbance Vector b:" << endl;
  b.prettyPrint();
  cout << "Solution Vector x:" << endl;
  x.prettyPrint();

  double value = 0;
  double target_result[] = {6.4, 8.2, -0.2};
  for (unsigned long count = 0; count < 3; count++)
    {
      value += (x.get(&count)-target_result[count]);
    }

  CPPUNIT_ASSERT( value < EPS);
}

//------------------------------------------------------------------
void TestSolver::testLUSolver()
{
  std::cout << "---" << std::endl;
  std::cout << "LU Solver" << std::endl;

  Math::LinAlg lin_alg;

  double matrix_data_asymmetric[] = {
    2.00,2.00,3.00,
    4.00,5.00,6.00,
    7.00,8.00,3.00};

  double disturbance_data[] = {5.e-01,7.5e+00,7.5e+00};

  unsigned long size[] = {3,3};
  Math::Matrix2D<double> A(size);
  Math::Vector<double> x(size[0]);
  Math::Vector<double> b(size[0]);
  
  try {
    A.setArrayData(matrix_data_asymmetric);
    b.setArrayData(disturbance_data);
    lin_alg.lu_solve(A,b,x);
  }
  catch(MathLinAlgException& exc) {
    cout << exc.msg() << endl;
  }
  
  cout << "Matrix A:" << endl;
  A.prettyPrint();
  cout << "Disturbance Vector b:" << endl;
  b.prettyPrint();
  cout << "Solution Vector x:" << endl;
  x.prettyPrint();

  double value = 0;
  double target_result[] = {-6.4, 6.5, 0.1};
  for (unsigned long count = 0; count < 3; count++)
    {
      value += (x.get(&count)-target_result[count]);
    }


  std::cout << "Second test, unchecked with CPPUNIT_ASSERT" << std::endl;
  double matrix_data_asymmetric2[] = {
    18,32,53,74,95,
    26,82,512,131,131,
    73,84,94,10,11,
    100,13,12,11,11,
    13,24,35,46,57};

  double disturbance_data2[] = {12,13,56,67,89};

  unsigned long size2[] = {5,5};
  Math::Matrix2D<double> A2(size2);
  Math::Vector<double> x2(size2[0]);
  Math::Vector<double> b2(size2[0]);
  
  try {
    A2.setArrayData(matrix_data_asymmetric2);
    b2.setArrayData(disturbance_data2);
    lin_alg.lu_solve(A2,b2,x2);
  }
  catch(MathLinAlgException& exc) {
    cout << exc.msg() << endl;
  }
  
  cout << "Matrix A2:" << endl;
  A2.prettyPrint();
  cout << "Disturbance Vector b2:" << endl;
  b2.prettyPrint();
  cout << "Solution Vector x2:" << endl;
  x2.prettyPrint();



  CPPUNIT_ASSERT( value < EPS);
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( SoofeaTest::TestSolver::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
