#ifndef blitz_matrix_h___
#define blitz_matrix_h___

#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/extensions/HelperMacros.h>

class BlitzMatrix : public CppUnit::TestFixture
{
public:
  CPPUNIT_TEST_SUITE( BlitzMatrix );

  CPPUNIT_TEST( testEquality );
  CPPUNIT_TEST( testUnEquality );

  CPPUNIT_TEST_SUITE_END();

  void testEquality();
  void testUnEquality();
};

#endif // blitz_matrix_h___
