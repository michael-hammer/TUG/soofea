#include "blitz_array.h"

#include <string>
#include <iostream>

//------------------------------------------------------------------
void BlitzArray::testEquality()
{
  blitz::Array<double,2> A(3,3, blitz::ColumnMajorArray<2>());
  blitz::Array<double,2> B(3,3);

  // Attention - here we have Fortran style order!
  A=1., 4., 7., 2., 5., 8., 3., 6., 9.;

  // C like row major order
  B=1., 2., 3., 
    4., 5., 6., 
    7., 8., 9.;
  
  std::cout << A << std::endl;
  std::cout << B << std::endl;

  double* data_A = A.data();
  double* data_B = B.data();

  std::cout << "data_A : ";
  for( unsigned count = 0 ; count < 9 ; count++ )
    {
      std::cout << data_A[count] << " ";
    }
  std::cout << std::endl;
  std::cout << "data_B : ";
  for( unsigned count = 0 ; count < 9 ; count++ )
    {
      std::cout << data_B[count] << " ";
    }
  std::cout << std::endl;

  blitz::Array<double,2> C(3,3, blitz::FortranArray<2>());

  C = A+B;

  std::cout << C << std::endl;

  C = A*B;

  std::cout << C << std::endl;

  blitz::firstIndex i;
  blitz::secondIndex j;
  blitz::thirdIndex k;
  blitz::Array<double,3> D(3,3,3);

  D = A(i,j) * B(j,k);

  std::cout << D << std::endl;

  CPPUNIT_ASSERT( true );
}

//------------------------------------------------------------------
void BlitzArray::testUnEquality()
{
  CPPUNIT_ASSERT( 0 != 1 );
}

//------------------------------------------------------------------
int main()
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( BlitzArray::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
