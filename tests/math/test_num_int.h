#ifndef test_num_int_h___
#define test_num_int_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <vector>

#include <exceptions.h>
#include <soofea.h>
#include <configuration.h>

#include "../num_int_io.h"
#include "../num_int_handler.h"

#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

SOOFEA_NAMESPACE(SoofeaTest)

#define EPSILON 1.e-15
     
  //------------------------------------------------------------------
  /**
     Test NumInt class
  */
class TestNumInt : 
  public CppUnit::TestFixture
{
public:
  TestNumInt();
  virtual ~TestNumInt();

  static const std::string DOC_NAME;
  
  CPPUNIT_TEST_SUITE( TestNumInt );
  CPPUNIT_TEST( testRectangular );
  CPPUNIT_TEST( testRectangularArray );
  CPPUNIT_TEST( testTriangle );
  CPPUNIT_TEST( testTriangleArray );
  CPPUNIT_TEST( testNumIntHandler );
  CPPUNIT_TEST_SUITE_END();
  
  virtual void testRectangular();
  virtual void testRectangularArray();
  virtual void testTriangleArray();
  virtual void testTriangle();
  virtual void testNumIntHandler();
protected:
  void scalar3D(Math::IntegrationPoint* int_point, double* value);
  void array3D(Math::IntegrationPoint* int_point, Math::Array<double>* array);
  void scalar2D(Math::IntegrationPoint* int_point, double* value);
  void array2D(Math::IntegrationPoint* int_point, Math::Array<double>* array);
};

SOOFEA_NAMESPACE_END

#endif // test_num_int_h___
