#ifndef blitz_array_h___
#define blitz_array_h___

#include <blitz/array.h>

#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

class BlitzArray : public CppUnit::TestFixture
{
public:
  CPPUNIT_TEST_SUITE( BlitzArray );

  CPPUNIT_TEST( testEquality );
  CPPUNIT_TEST( testUnEquality );

  CPPUNIT_TEST_SUITE_END();

  void testEquality();
  void testUnEquality();
};

#endif // blitz_array_h___
