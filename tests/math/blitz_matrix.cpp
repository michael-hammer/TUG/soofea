#include "blitz_matrix.h"

#include <cppunit/ui/text/TestRunner.h>

#include <blitz/matrix.h>

#include <string>
#include <iostream>

using namespace blitz;

//------------------------------------------------------------------
void BlitzMatrix::testEquality()
{
  Matrix<double> A(3,3);
  Matrix<double> B(3,3);

//  A=1., 2., 3.,
//    4., 5., 6.,
//    7., 8., 9.;
//
//  B=1., 2., 3.,
//    4., 5., 6.,
//    7., 8., 9.;

  std::cout << A << std::endl;
  std::cout << B << std::endl;

  CPPUNIT_ASSERT( true );
}

//------------------------------------------------------------------
void BlitzMatrix::testUnEquality()
{
  CPPUNIT_ASSERT( 0 != 1 );
}

//------------------------------------------------------------------
int main()
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( BlitzMatrix::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
