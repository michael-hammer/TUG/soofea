c     ------------------------------------------------------------------
c     Prints 2D Array created in C with A[m][n] as Fortran 2D Array 
c     A(i,j)
      SUBROUTINE PRINTARRAY2D( A , m , n )
      INTEGER m,n
      DOUBLE PRECISION A(m,n)
      PRINT'(A)','Fortran A(i,j):'
      DO 10 i = 1,m
         DO 10 j = 1,n
            PRINT'(A,I1,A,I1,A,F3.0)','(',i,',',j,') = ',A(i,j)
 10   CONTINUE
      END
c
c     ------------------------------------------------------------------
c     Creates 2D Array as Fortran 2D Array A(m,n)
      SUBROUTINE ARRAY2D( A , m , n )
      DOUBLE PRECISION A(m,n)
      INTEGER m,n
      PRINT'(A,I1,A,I1,A)','Set Array in Fortran A(',m,',',n,
     +     ') [A(m,n)]'
      DO 20 i = 1,m
         DO 20 j = 1,n
            A(i,j) = i*10+j
 20   CONTINUE
      END
c
c     ------------------------------------------------------------------
c     Prints 3D Array created in C with A[m][n][o] as Fortran 3D Array 
c     A(i,j,k)
      SUBROUTINE PRINTARRAY3D( A , m , n , o )
      INTEGER m,n,o
      DOUBLE PRECISION A(m,n,o)
      PRINT'(A)','Fortran A(i,j,k):'
      DO 10 i = 1,m
         DO 10 j = 1,n
            DO 10 k = 1,o
               PRINT'(A,I1,A,I1,A,I1,A,F4.0)','(',i,',',j,',',k,') = ',
     +              A(i,j,k)
 10   CONTINUE
      END
c
c     ------------------------------------------------------------------
c     Creates 3D Array as Fortran 3D Array A(m,n,o)
      SUBROUTINE ARRAY3D( A , m , n , o )
      INTEGER m,n,o
      DOUBLE PRECISION A(m,n,o)
      PRINT'(A,I1,A,I1,A,I1,A)','Set Array in Fortran A(',m,',',n,',',o,
     +     ') [A(m,n)]'
      DO 20 i = 1,m
         DO 20 j = 1,n
            DO 20 k = 1,o
               A(i,j,k) = i*100+j*10+k
 20   CONTINUE
      END
