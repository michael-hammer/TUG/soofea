#ifndef test_element_iterator_h___
#define test_element_iterator_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include <soofea.h>

#include "../../model/element.h"

#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "../template/element_iterator.h"
#include "../../model/node.h"
#include "../../geometry/coord_sys.h"

SOOFEA_NAMESPACE(SoofeaTest)

  //------------------------------------------------------------------
  /**
     Test ElementIterator class
  */
class TestElementIterator : 
  public CppUnit::TestFixture
{
public:
  TestElementIterator();
  virtual ~TestElementIterator();
  
  CPPUNIT_TEST_SUITE( TestElementIterator );
  CPPUNIT_TEST( testIter );
  CPPUNIT_TEST_SUITE_END();
  
  virtual void testIter();
private:
  Node* node_1_;
  Node* node_2_;
  Node* node_3_;

  Element* element_;
};

SOOFEA_NAMESPACE_END

#endif // test_element_iterator_h___
