#include "test_element_iterator.h"

SOOFEA_USING_NAMESPACE(SoofeaTest)

//------------------------------------------------------------------
TestElementIterator::TestElementIterator()
{
//  node_1_ = new Node(1,0.134234,0.234553,0);
//  node_2_ = new Node(2,2.252345,-0.52354,0);
//  node_3_ = new Node(3,2.485685,2.856988,0);

//  Element::NodeNumberVector* node_number_vector = new Element::NodeNumberVector;
//  node_number_vector -> push_back(1);
//  node_number_vector -> push_back(2);
//  node_number_vector -> push_back(3);
//
//  element_ = new Element(1,1,node_number_vector,1);
}

//------------------------------------------------------------------
TestElementIterator::~TestElementIterator()
{
//  delete node_1_;
//  delete node_2_;
//  delete node_3_;
}

//------------------------------------------------------------------
void TestElementIterator::testIter()
{
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( SoofeaTest::TestElementIterator::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}

