#ifndef test_linear_shape_h___
#define test_linear_shape_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include <cmath>

#include "soofea.h"
#include "test_shape.h"
#include "model/shape/linear_shape.h"

#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
     
//------------------------------------------------------------------
/**
   Test LinearShape class
*/
class TestLinearShape : 
  public TestShape ,
  public CppUnit::TestFixture
{
public:
  TestLinearShape();
  virtual ~TestLinearShape() {}
  
  CPPUNIT_TEST_SUITE( TestLinearShape );
  CPPUNIT_TEST( testShape );
  CPPUNIT_TEST( testDerivativeShape );
  CPPUNIT_TEST_SUITE_END();
  
  virtual void testShape();
  virtual void testDerivativeShape();
private:
  LinearShape shape1;
  LinearShape shape2;
};

#endif // test_linear_shape_h___
