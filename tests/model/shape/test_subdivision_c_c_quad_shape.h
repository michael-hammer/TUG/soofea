#ifndef test_subdivision_c_c_quad_shape_h___
#define test_subdivision_c_c_quad_shape_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * ------------------------------------------------------------------*/

#include <cfloat>

#include "soofea.h"
#include "test_shape.h"
#include "model/shape/subdivision_c_c_quad_shape.h"
#include "../../comparison.h"

#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#define EPS 5.0*DBL_EPSILON

//------------------------------------------------------------------
class TestSubdivisionCCQuadShape : 
  public TestShape ,
  public CppUnit::TestFixture
{
public:
  TestSubdivisionCCQuadShape();
  virtual ~TestSubdivisionCCQuadShape() {}
    
  CPPUNIT_TEST_SUITE( TestSubdivisionCCQuadShape );
  CPPUNIT_TEST( testShape );
  CPPUNIT_TEST( testDerivativeShape );
  CPPUNIT_TEST( testSecondDerivativeShape );
  CPPUNIT_TEST_SUITE_END();
  
  virtual void testShape();
  virtual void testDerivativeShape();
  virtual void testSecondDerivativeShape();

private:
  SubdivisionCCQuadShape shape_standard;
  SubdivisionCCQuadShape shape_boundary_in_s;
  SubdivisionCCQuadShape shape_corner;
};

#endif // test_subdivision_c_c_quad_shape_h___
