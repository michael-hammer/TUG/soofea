#include "test_quad_shape.h"

#include <iostream>

//------------------------------------------------------------------
TestQuadShape::TestQuadShape() :
  shape1(1),
  shape2(2)
{}

//------------------------------------------------------------------
void TestQuadShape::testShape()
{
  NodeIndex* node_index = 0;

  // test node values on 1.
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,-1.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,-1.,1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.,1.) == 1. );
  delete node_index;
  // test zero values (a few examples - list would be too long for
  // all node combinations)
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,-1.,-1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,-1.,-1.) == 0. );
  delete node_index;
  // test points inbetween (selection)

  // test node values on 1.
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(2,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,0.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.) == 1. );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,1.) == 1. );
  delete node_index;
  // test zero values
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,-1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,-1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(2,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,0.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,-1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 0. );
  delete node_index;

  /** @todo test points inbetween, add more tests, What about higher order? **/
}

//------------------------------------------------------------------
void TestQuadShape::testDerivativeShape()
{
  NodeIndex* node_index = 0;

  // test derivative
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( shape1.derivativeShape(0,*node_index,0,-1.,-1.) == -0.5 );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( shape1.derivativeShape(1,*node_index,-1.,-1.) == 0.5 );
  CPPUNIT_ASSERT( shape1.derivativeShape(0,*node_index,-1.,0.) == -0.25 );
  delete node_index;

  /** @todo test points inbetween, add more tests, What about higher order? **/
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( TestQuadShape::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
