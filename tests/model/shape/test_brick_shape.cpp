#include "test_brick_shape.h"

SOOFEA_USING_NAMESPACE(SoofeaTest)

#include <iostream>

//------------------------------------------------------------------
void TestBrickShape::testShape()
{
  NodeIndex* node_index = 0;

  // test node values on 1.
  node_index = new NodeIndex(0,0,0);
  CPPUNIT_ASSERT( shape.shape(*node_index,-1.,-1.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,0,0);
  CPPUNIT_ASSERT( shape.shape(*node_index,1.,-1.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,1,0);
  CPPUNIT_ASSERT( shape.shape(*node_index,-1.,1.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,1,0);
  CPPUNIT_ASSERT( shape.shape(*node_index,1.,1.,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,0,1);
  CPPUNIT_ASSERT( shape.shape(*node_index,-1.,-1.,1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,0,1);
  CPPUNIT_ASSERT( shape.shape(*node_index,1.,-1.,1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,1,1);
  CPPUNIT_ASSERT( shape.shape(*node_index,-1.,1.,1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,1,1);
  CPPUNIT_ASSERT( shape.shape(*node_index,1.,1.,1.) == 1. );
  delete node_index;
  // test zero values (a few examples - list would be too long for
  // all node combinations)
  node_index = new NodeIndex(0,0,0);
  CPPUNIT_ASSERT( shape.shape(*node_index,-1.,-1.,1.) == 0. );
  CPPUNIT_ASSERT( shape.shape(*node_index,1.,-1.,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,0,1);
  CPPUNIT_ASSERT( shape.shape(*node_index,1.,-1.,-1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,1,0);
  CPPUNIT_ASSERT( shape.shape(*node_index,-1.,-1.,-1.) == 0. );
  delete node_index;
  // test points inbetween (selection)
  node_index = new NodeIndex(0,0,0);
  CPPUNIT_ASSERT( shape.shape(*node_index,0.,-1.,-1.) == 0.5 );
  delete node_index;
  node_index = new NodeIndex(0,0,1);
  CPPUNIT_ASSERT( shape.shape(*node_index,-1.,0,1.) == 0.5 );
  delete node_index;

//  // test node values on 1. (also not all of them)
//  node_index = new NodeIndex(0,0,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,-1.,-1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(2,0,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,-1.,-1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(0,2,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,1.,-1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(2,2,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,1.,-1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(0,0,2);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,-1.,1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(2,0,2);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,-1.,1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(0,2,2);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,1.,1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(2,2,2);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,1.,1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(1,0,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,-1.,-1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(0,1,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,0.,-1.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(0,0,1);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,-1.,0.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(0,0,1);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,-1.,0.) == 1. );
//  delete node_index;
//  node_index = new NodeIndex(1,0,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,-1.,-1.) == 1. );
//  delete node_index;
//  // test zero values
//  node_index = new NodeIndex(0,0,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,0.,-1.) == 0. );
//  delete node_index;
//  node_index = new NodeIndex(2,0,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,-1.,-1.) == 0. );
//  delete node_index;
//  node_index = new NodeIndex(0,2,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.,0.) == 0. );
//  delete node_index;
//  node_index = new NodeIndex(2,2,0);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.,-1.) == 0. );
//  delete node_index;
//  node_index = new NodeIndex(0,0,2);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.,0.,1.) == 0. );
//  delete node_index;
//  node_index = new NodeIndex(2,0,2);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,-1.,0.) == 0. );
//  delete node_index;
//  node_index = new NodeIndex(0,2,2);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.,1.) == 0. );
//  delete node_index;
//  node_index = new NodeIndex(2,2,2);
//  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.,1.) == 0. );
//  delete node_index;

  /** @todo test points inbetween, add more tests, What about higher order? **/
}

//------------------------------------------------------------------
void TestBrickShape::testDerivativeShape()
{
  NodeIndex* node_index = 0;

  // test derivative
  node_index = new NodeIndex(0,0,0);
  CPPUNIT_ASSERT( shape.derivativeShape(0,*node_index,-1.,-1.,-1.) == -0.5 );
  delete node_index;
  node_index = new NodeIndex(0,1,0);
  CPPUNIT_ASSERT( shape.derivativeShape(1,*node_index,-1.,-1.,-1.) == 0.5 );
  delete node_index;
  node_index = new NodeIndex(0,1,2);
  CPPUNIT_ASSERT( shape.derivativeShape(0,*node_index,-1.,0.,0.) == -0.25 );
  delete node_index;

  /** @todo test points inbetween, add more tests, What about higher order? **/
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( TestBrickShape::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
