/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include "test_linear_shape.h"

#include <iostream>

//------------------------------------------------------------------
TestLinearShape::TestLinearShape() :
  shape1(1),
  shape2(2)
{}

//------------------------------------------------------------------
void TestLinearShape::testShape()
{
  NodeIndex* node_index = 0;

  // test node values on 1.
  node_index = new NodeIndex(0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.) == 1. );
  delete node_index;
  // test zero values (a few examples - list would be too long for
  // all node combinations)
  node_index = new NodeIndex(0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,-1.) == 0. );
  delete node_index;
  // test points inbetween (selection)
  node_index = new NodeIndex(0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.) == 0.5 );
  delete node_index;
  node_index = new NodeIndex(1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.) == 0.5 );
  delete node_index;

  // test node values on 1. (also not all of them)
  node_index = new NodeIndex(0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.) == 1. );
  delete node_index;
  node_index = new NodeIndex(2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.) == 1. );
  delete node_index;
  // test zero values
  node_index = new NodeIndex(0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,-1.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.) == 0. );
  delete node_index;

  /** @todo test points inbetween, add more tests, What about higher order? **/
}

//------------------------------------------------------------------
void TestLinearShape::testDerivativeShape()
{
  NodeIndex* node_index = 0;

  // test derivative
  node_index = new NodeIndex(0);
  CPPUNIT_ASSERT( shape1.derivativeShape(0,*node_index,-1.) == -0.5 );
  delete node_index;
  node_index = new NodeIndex(1);
  CPPUNIT_ASSERT( shape1.derivativeShape(0,*node_index,1.) == 0.5 );
  delete node_index;

  // test derivative second order
  node_index = new NodeIndex(0);
  CPPUNIT_ASSERT( shape2.derivativeShape(0,*node_index,-1.) == -1.5 );
  delete node_index;
  node_index = new NodeIndex(1);
  CPPUNIT_ASSERT( shape2.derivativeShape(0,*node_index,0.) == 0. );
  delete node_index;
  node_index = new NodeIndex(2);
  CPPUNIT_ASSERT( shape2.derivativeShape(0,*node_index,1.) == 1.5 );
  delete node_index;

  /** @todo test points inbetween, add more tests, What about higher order? **/
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( TestLinearShape::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
