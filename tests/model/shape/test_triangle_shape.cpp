#include "test_triangle_shape.h"

#include <iostream>

//------------------------------------------------------------------
TestTriangleShape::TestTriangleShape() :
  shape1(1),
  shape2(2)
{}

//------------------------------------------------------------------
void TestTriangleShape::testShape()
{
  NodeIndex* node_index = 0;

  // test node values on 1.
  node_index = new NodeIndex(1,0,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.,0.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,1,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.,1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,0,1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.,0.) == 1. );
  delete node_index;
  // test zero values
  node_index = new NodeIndex(1,0,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.,0.) == 0. );
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,1,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.,0.) == 0. );
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.,0.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,0,1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,1.,0.) == 0. );
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.,1.) == 0. );
  delete node_index;
  // test points inbetween
  node_index = new NodeIndex(1,0,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.5,0.2) == 0.5 );
  delete node_index;
  node_index = new NodeIndex(0,1,0);
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.2,0.5) == 0.5 );
  delete node_index;
  node_index = new NodeIndex(0,0,1);
  CPPUNIT_ASSERT( shape1.shape(*node_index,0.25,0.25) == 0.5 );
  delete node_index;

  // test node values on 1.
  node_index = new NodeIndex(2,0,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,2,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,0,2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,1,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.5) == 1. );
  delete node_index;
  node_index = new NodeIndex(0,1,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.5) == 1. );
  delete node_index;
  node_index = new NodeIndex(1,0,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.) == 1. );
  delete node_index;
  // test zero values
  node_index = new NodeIndex(2,0,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.5) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1,0,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.5) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,2,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.5) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.5) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,0,2);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.5) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.5) == 0. );
  delete node_index;
  node_index = new NodeIndex(1,1,0);
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.5) == 0. );
  delete node_index;
  node_index = new NodeIndex(0,1,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.5) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 0. );
  delete node_index;
  node_index = new NodeIndex(1,0,1);
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.5,0.5) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.5) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,1.,0.) == 0. );
  CPPUNIT_ASSERT( shape2.shape(*node_index,0.,1.) == 0. );
  delete node_index;

  /** @todo test points inbetween, add more tests, What about higher order? **/
}

//------------------------------------------------------------------
void TestTriangleShape::testDerivativeShape()
{
  NodeIndex* node_index = 0;
  double L_1 = 0.2345235;
  double L_2 = 0.4765687;

  // test derivative (here value is constant because of linearity of shape)
  // L_1,L_2,L_3
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(0,0,0,L_1,L_2) - 0.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(1,0,0,L_1,L_2) - 0.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(2,0,0,L_1,L_2) - 1.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(0,1,0,L_1,L_2) - 1.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(1,1,0,L_1,L_2) - 0.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(2,1,0,L_1,L_2) - 0.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(0,0,1,L_1,L_2) - 0.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(1,0,1,L_1,L_2) - 1.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShapeL(2,0,1,L_1,L_2) - 0.) < DBL_EPSILON );
  // r,s,t
  node_index = new NodeIndex(1,0,0);
  CPPUNIT_ASSERT( fabs(shape1.derivativeShape(0,*node_index,L_1,L_2) - 1.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShape(1,*node_index,L_1,L_2) - 0.) < DBL_EPSILON );
  delete node_index;
  node_index = new NodeIndex(0,1,0);
  CPPUNIT_ASSERT( fabs(shape1.derivativeShape(0,*node_index,L_1,L_2) - 0.) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShape(1,*node_index,L_1,L_2) - 1.) < DBL_EPSILON );
  delete node_index;
  node_index = new NodeIndex(0,0,1);
  CPPUNIT_ASSERT( fabs(shape1.derivativeShape(0,*node_index,L_1,L_2) - (-1.)) < DBL_EPSILON );
  CPPUNIT_ASSERT( fabs(shape1.derivativeShape(1,*node_index,L_1,L_2) - (-1.)) < DBL_EPSILON );
  delete node_index;

  /** @todo test points inbetween, add more tests, What about higher order? **/
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( TestTriangleShape::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
