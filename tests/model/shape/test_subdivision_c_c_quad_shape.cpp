#include "test_subdivision_c_c_quad_shape.h"

#include <iostream>

//------------------------------------------------------------------
TestSubdivisionCCQuadShape::TestSubdivisionCCQuadShape() :
  shape_standard(SubdivisionCCQuadShape::BASIC),
  shape_boundary_in_s(SubdivisionCCQuadShape::BOUNDARY),
  shape_corner(SubdivisionCCQuadShape::CORNER)
{}

//------------------------------------------------------------------
void TestSubdivisionCCQuadShape::testShape()
{
  //check partition of unity
  bz::Array<double,1>* shape_tensor_standard = 0;
  bz::Array<double,1>* shape_tensor_boundary_in_s = 0;
  bz::Array<double,1>* shape_tensor_corner = 0;

  int rows = 10;
  int cols = 10;
  
  for( int row_count = 0; row_count <= rows; ++row_count )
    {
      for( int col_count = 0; col_count <= cols; ++col_count )
  	{
  	  double r = -1.0+2.0/static_cast<double>(cols)*static_cast<double>(col_count);
  	  double s = -1.0+2.0/static_cast<double>(rows)*static_cast<double>(row_count);

  	  shape_tensor_standard = shape_standard.shapeTensor( r, s );
  	  //std::cout << *shape_tensor_standard << std::endl;
  	  double shape_sum_standard = sum(*shape_tensor_standard);
  	  CPPUNIT_ASSERT( Comparison<double>().equal( shape_sum_standard , 1. ) );

  	  shape_tensor_boundary_in_s = shape_boundary_in_s.shapeTensor( r, s );
  	  //std::cout << *shape_tensor_boundary_in_s << std::endl;
  	  double shape_sum_boundary_in_s = sum(*shape_tensor_boundary_in_s);
  	  CPPUNIT_ASSERT( Comparison<double>().equal( shape_sum_boundary_in_s , 1. ) );

  	  shape_tensor_corner = shape_corner.shapeTensor( r, s );
  	  //std::cout << *shape_tensor_corner << std::endl;
  	  double shape_sum_corner = sum(*shape_tensor_corner);
  	  CPPUNIT_ASSERT( Comparison<double>().equal( shape_sum_corner , 1. ) );

  	  delete shape_tensor_standard;
  	  delete shape_tensor_boundary_in_s;
  	  delete shape_tensor_corner;
  	}
    }
  
  //check central quad nodes shape function values for standard element
  NodeIndex* node_index = 0;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,-1.0,-1.0) , 4.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,+1.0,-1.0) , 1.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,-1.0,+1.0) , 1.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,+1.0,+1.0) , 1.0/36.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,+1.0,-1.0) , 4.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,-1.0,-1.0) , 1.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,+1.0,+1.0) , 1.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,-1.0,+1.0) , 1.0/36.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,-1.0,+1.0) , 4.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,-1.0,-1.0) , 1.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,+1.0,+1.0) , 1.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,+1.0,-1.0) , 1.0/36.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,+1.0,+1.0) , 4.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,-1.0,+1.0) , 1.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,+1.0,-1.0) , 1.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.shape(*node_index,-1.0,-1.0) , 1.0/36.0 ) );
  delete node_index;

  //check zero values of shape functions at two points distance for standard element
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,+1.0,+10.0) ) );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,+1.0,+10.0) ) );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,+1.0,+10.0) ) );
  delete node_index;
  node_index = new NodeIndex(0,3);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,+1.0,+10.0) ) );
  delete node_index;

  node_index = new NodeIndex(3,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-1.0,+10.0) ) );
  delete node_index;
  node_index = new NodeIndex(3,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-1.0,+10.0) ) );
  delete node_index;
  node_index = new NodeIndex(3,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-1.0,+10.0) ) );
  delete node_index;
  node_index = new NodeIndex(3,3);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-1.0,+10.0) ) );
  delete node_index;

  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-10.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-10.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(2,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-10.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(3,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-10.0,+1.0) ) );
  delete node_index;

  node_index = new NodeIndex(0,3);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-10.0,-1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,3);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-10.0,-1.0) ) );
  delete node_index;
  node_index = new NodeIndex(2,3);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-10.0,-1.0) ) );
  delete node_index;
  node_index = new NodeIndex(3,3);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.shape(*node_index,-10.0,-1.0) ) );
  delete node_index;



  //check central quad nodes shape function values for boundary in s element
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,-1.0,-1.0) , 2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,+1.0,-1.0) , 1.0/9.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,-1.0,+1.0) , 1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,+1.0,+1.0) , 1.0/36.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.shape(*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,+1.0,-1.0) , 4.0/9.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.shape(*node_index,-1.0,+1.0)  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,+1.0,+1.0) , 1.0/9.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,-1.0,-1.0) , 1.0/6.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,+1.0,-1.0) , 1.0/36.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,-1.0,+1.0) , 2.0/3.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,+1.0,+1.0) , 1.0/9.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.shape(*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,+1.0,-1.0) , 1.0/9.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.shape(*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.shape(*node_index,+1.0,+1.0) , 4.0/9.0 ) );
  delete node_index;



  //check central quad nodes shape function values for corner element
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,-1.0,-1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,+1.0,-1.0) , 1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,-1.0,+1.0) , 1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,+1.0,+1.0) , 1.0/36.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.shape(*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,+1.0,-1.0) , 2.0/3.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.shape(*node_index,-1.0,+1.0)  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,+1.0,+1.0) , 1.0/9.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.shape(*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.shape(*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,-1.0,+1.0) , 2.0/3.0  ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,+1.0,+1.0) , 1.0/9.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.shape(*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.shape(*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.shape(*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.shape(*node_index,+1.0,+1.0) , 4.0/9.0 ) );
  delete node_index;
}

//------------------------------------------------------------------
void TestSubdivisionCCQuadShape::testDerivativeShape()
{
  //check zero sum
  bz::Array<double,2>* derived_shape_tensor_standard = 0;
  bz::Array<double,2>* derived_shape_tensor_boundary_in_s = 0;
  bz::Array<double,2>* derived_shape_tensor_corner = 0;

  int rows = 15;
  int cols = 12;
 
  for( int row_count = 0; row_count <= rows; ++row_count )
    {
      for( int col_count = 0; col_count <= cols; ++col_count )
  	{
  	  double r = -1.0+2.0/static_cast<double>(cols)*static_cast<double>(col_count);
  	  double s = -1.0+2.0/static_cast<double>(rows)*static_cast<double>(row_count);

  	  derived_shape_tensor_standard = shape_standard.derivativeShapeTensor( r, s );
  	  double derived_shape_sum_standard_0 = sum((*derived_shape_tensor_standard)(bz::Range::all(),0));
  	  double derived_shape_sum_standard_1 = sum((*derived_shape_tensor_standard)(bz::Range::all(),1));
  	  CPPUNIT_ASSERT( Comparison<double>().zero( derived_shape_sum_standard_0 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( derived_shape_sum_standard_1 ) );
  	  delete derived_shape_tensor_standard;

  	  derived_shape_tensor_boundary_in_s = shape_boundary_in_s.derivativeShapeTensor( r, s );
  	  double derived_shape_sum_boundary_in_s_0 = sum((*derived_shape_tensor_boundary_in_s)(bz::Range::all(),0));
  	  double derived_shape_sum_boundary_in_s_1 = sum((*derived_shape_tensor_boundary_in_s)(bz::Range::all(),1));
  	  CPPUNIT_ASSERT( Comparison<double>().zero( derived_shape_sum_boundary_in_s_0 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( derived_shape_sum_boundary_in_s_1 ) );
  	  delete derived_shape_tensor_boundary_in_s;

  	  derived_shape_tensor_corner = shape_corner.derivativeShapeTensor( r, s );
  	  double derived_shape_sum_corner_0 = sum((*derived_shape_tensor_corner)(bz::Range::all(),0));
  	  double derived_shape_sum_corner_1 = sum((*derived_shape_tensor_corner)(bz::Range::all(),1));
  	  CPPUNIT_ASSERT( Comparison<double>().zero( derived_shape_sum_corner_0 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( derived_shape_sum_corner_1 ) );
  	  delete derived_shape_tensor_corner;
  	}
    }

  //check central quad nodes shape function derivative values with respect to r for standard element
  NodeIndex* node_index = 0;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(0,*node_index,+1.0,-1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(0,*node_index,+1.0,+1.0) , -1.0/12.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(0,*node_index,-1.0,-1.0) , 1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(0,*node_index,-1.0,+1.0) , 1.0/12.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(0,*node_index,+1.0,-1.0) , -1.0/12.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(0,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(0,*node_index,-1.0,-1.0) , 1.0/12.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(0,*node_index,-1.0,+1.0) , 1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(0,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s for standard element
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(1,*node_index,-1.0,+1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(1,*node_index,+1.0,+1.0) , -1.0/12.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(1,*node_index,-1.0,+1.0) , -1.0/12.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(1,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(1,*node_index,-1.0,-1.0) , 1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(1,*node_index,+1.0,-1.0) , 1.0/12.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(1,*node_index,-1.0,-1.0) , 1.0/12.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.derivativeShape(1,*node_index,+1.0,-1.0) , 1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.derivativeShape(1,*node_index,+1.0,+1.0) ) );
  delete node_index;



  //check central quad nodes shape function derivative values with respect to r for boundary in s element
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,-1.0,-1.0) , -2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,+1.0,-1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,-1.0,+1.0) , -1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,+1.0,+1.0) , -1.0/12.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,-1.0,-1.0) , 2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,-1.0,+1.0) , 1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,-1.0,-1.0) , -1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,+1.0,-1.0) , -1.0/12.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,-1.0,+1.0) , -2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,-1.0,-1.0) , 1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(0,*node_index,-1.0,+1.0) , 2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(0,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s for boundary in s element
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(1,*node_index,-1.0,+1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(1,*node_index,+1.0,+1.0) , -1.0/12.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(1,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(1,*node_index,-1.0,-1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(1,*node_index,+1.0,-1.0) , 1.0/12.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.derivativeShape(1,*node_index,+1.0,-1.0) , 1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.derivativeShape(1,*node_index,+1.0,+1.0) ) );
  delete node_index;



  //check central quad nodes shape function derivative values with respect to r for corner element
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,-1.0,-1.0) , -1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,+1.0,-1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,-1.0,+1.0) , -1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,+1.0,+1.0) , -1.0/12.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,-1.0,-1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,-1.0,+1.0) , 1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,-1.0,+1.0) , -2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(0,*node_index,-1.0,+1.0) , 2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(0,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s for corner element
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,-1.0,-1.0) , -1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,+1.0,-1.0) , -1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,-1.0,+1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,+1.0,+1.0) , -1.0/12.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,+1.0,-1.0) , -2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,-1.0,-1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,+1.0,-1.0) , 1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.derivativeShape(1,*node_index,+1.0,-1.0) , 2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.derivativeShape(1,*node_index,+1.0,+1.0) ) );
  delete node_index;
}

//------------------------------------------------------------------
void TestSubdivisionCCQuadShape::testSecondDerivativeShape()
{
  //check zero sum
  bz::Array<double,2>* second_derivative_shape_tensor_standard = 0;
  bz::Array<double,2>* second_derivative_shape_tensor_boundary_in_s = 0;
  bz::Array<double,2>* second_derivative_shape_tensor_corner = 0;

  int rows = 17;
  int cols = 5;
 
  for( int row_count = 0; row_count <= rows; ++row_count )
    {
      for( int col_count = 0; col_count <= cols; ++col_count )
  	{
  	  double r = -1.0+2.0/static_cast<double>(cols)*static_cast<double>(col_count);
  	  double s = -1.0+2.0/static_cast<double>(rows)*static_cast<double>(row_count);

  	  second_derivative_shape_tensor_standard = shape_standard.secondDerivativeShapeTensor( r, s );
  	  double second_derivative_shape_sum_standard_00 =
	    sum((*second_derivative_shape_tensor_standard)(bz::Range::all(),0));
  	  double second_derivative_shape_sum_standard_01 =
	    sum((*second_derivative_shape_tensor_standard)(bz::Range::all(),1));
  	  double second_derivative_shape_sum_standard_10 =
	    sum((*second_derivative_shape_tensor_standard)(bz::Range::all(),2));
  	  double second_derivative_shape_sum_standard_11 =
	    sum((*second_derivative_shape_tensor_standard)(bz::Range::all(),3));
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_standard_00 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_standard_01 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_standard_10 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_standard_11 ) );
  	  delete second_derivative_shape_tensor_standard;

 	  second_derivative_shape_tensor_boundary_in_s = shape_boundary_in_s.secondDerivativeShapeTensor( r, s );
  	  double second_derivative_shape_sum_boundary_in_s_00 =
	    sum((*second_derivative_shape_tensor_boundary_in_s)(bz::Range::all(),0));
  	  double second_derivative_shape_sum_boundary_in_s_01 =
	    sum((*second_derivative_shape_tensor_boundary_in_s)(bz::Range::all(),1));
  	  double second_derivative_shape_sum_boundary_in_s_10 =
	    sum((*second_derivative_shape_tensor_boundary_in_s)(bz::Range::all(),2));
  	  double second_derivative_shape_sum_boundary_in_s_11 =
	    sum((*second_derivative_shape_tensor_boundary_in_s)(bz::Range::all(),3));
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_boundary_in_s_00 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_boundary_in_s_01 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_boundary_in_s_10 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_boundary_in_s_11 ) );
  	  delete second_derivative_shape_tensor_boundary_in_s;

 	  second_derivative_shape_tensor_corner = shape_corner.secondDerivativeShapeTensor( r, s );
  	  double second_derivative_shape_sum_corner_00 =
	    sum((*second_derivative_shape_tensor_corner)(bz::Range::all(),0));
  	  double second_derivative_shape_sum_corner_01 =
	    sum((*second_derivative_shape_tensor_corner)(bz::Range::all(),1));
  	  double second_derivative_shape_sum_corner_10 =
	    sum((*second_derivative_shape_tensor_corner)(bz::Range::all(),2));
  	  double second_derivative_shape_sum_corner_11 =
	    sum((*second_derivative_shape_tensor_corner)(bz::Range::all(),3));
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_corner_00 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_corner_01 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_corner_10 ) );
  	  CPPUNIT_ASSERT( Comparison<double>().zero( second_derivative_shape_sum_corner_11 ) );
  	  delete second_derivative_shape_tensor_corner;
  	}
    }

  //check central quad nodes shape function derivative values with respect to r and r for standard element
  NodeIndex* node_index = 0;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) , -4.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) ,  2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) ,  1.0/6.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ,  2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) , -4.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ,  1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) ,  1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) , -4.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) ,  2.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ,  1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ,  2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , -4.0/3.0 ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to r and s for standard element
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) , 1.0/4.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,1,*node_index,-1.0,+1.0), -1.0/4.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) , -1.0/4.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) , 1.0/4.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s and r for standard element
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) , 1.0/4.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,0,*node_index,-1.0,+1.0), -1.0/4.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) , -1.0/4.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) , 1.0/4.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_standard.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s and s for standard element
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) , -4.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) ,  2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) ,  1.0/6.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) , -4.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) ,  1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) ,  2.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ,  2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) ,  1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) , -4.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(2,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ,  1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) ,  2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_standard.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , -4.0/3.0 ) );
  delete node_index;



  //check central quad nodes shape function derivative values with respect to r and r for boundary in s element
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) , 2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , 1.0/6.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) , -4.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) , 1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , 2.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , -4.0/3.0 ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to r and s for boundary in s element
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) , 1.0/4.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,-1.0,+1.0), -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) , -1.0/4.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s and r for boundary in s element
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) , 1.0/4.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,-1.0,+1.0), -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) , -1.0/4.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s and s for boundary in s element
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) , -2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) , -1.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) ,  1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) ,  1.0/6.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) , -4.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , 2.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,2);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ,  1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) ,  1.0/6.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) , -2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,2);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) , 2.0/3.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_boundary_in_s.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , -4.0/3.0 ) );
  delete node_index;



  //check central quad nodes shape function derivative values with respect to r and r for corner element
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , 1.0/6.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , 2.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) , -2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,0,*node_index,+1.0,+1.0) , -4.0/3.0 ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to r and s for corner element
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) , 1.0/4.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) , -1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) , -1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(0,1,*node_index,-1.0,-1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(0,1,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s and r for corner element
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) , 1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) , 1.0/4.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) , -1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) , -1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) , -1.0/2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,0,*node_index,-1.0,-1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,0,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,0,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,0,*node_index,+1.0,+1.0) ) );
  delete node_index;

  //check central quad nodes shape function derivative values with respect to s and s for corner element
  node_index = new NodeIndex(0,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) , 1.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , 1.0/6.0 ) );
  delete node_index;
  node_index = new NodeIndex(0,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) , -2.0 ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , -1.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,0);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , 2.0/3.0 ) );
  delete node_index;
  node_index = new NodeIndex(1,1);
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,-1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,+1.0,-1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().zero( shape_corner.secondDerivativeShape(1,1,*node_index,-1.0,+1.0) ) );
  CPPUNIT_ASSERT( Comparison<double>().equal( shape_corner.secondDerivativeShape(1,1,*node_index,+1.0,+1.0) , -4.0/3.0 ) );
  delete node_index;
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( TestSubdivisionCCQuadShape::suite() );
  bool was_successful = runner.run( );
  return( was_successful ? 0 : 1 );
}
