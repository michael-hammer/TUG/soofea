/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include "test_xml_input_handler.h"

SOOFEA_USING_NAMESPACE(SoofeaTest)

InputHandler* TestXMLInputHandler::xml_input_handler_ = 0;

//------------------------------------------------------------------
TestXMLInputHandler::TestXMLInputHandler()
{}

//------------------------------------------------------------------
TestXMLInputHandler::~TestXMLInputHandler()
{}

//------------------------------------------------------------------
void TestXMLInputHandler::testGlobal()
{
  try {
    Global* global = xml_input_handler_ -> getGlobal();
    global -> prettyPrint();
    delete global;
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testContact()
{
  try {
    Contact* contact = new Contact;
    xml_input_handler_ -> get(*contact);
    contact -> prettyPrint();
    delete contact;
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }

}

//------------------------------------------------------------------
void TestXMLInputHandler::testNode()
{
  Global* global = 0;
  try {
    global = xml_input_handler_ -> getGlobal();
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }

  CoordSys* coord_sys=0;
  switch( global -> getCoordSysID() )
    {
    case CoordSys::CARTESIAN:
      coord_sys = new CartesianCoordSys(global -> getDimension());
    default:
      break;
    }

  try {
    for( InputHandler::NodeIterator iter = xml_input_handler_ -> begin<Node>(); 
	 iter != xml_input_handler_ -> end<Node>(); 
	 ++iter)
      {
	iter -> setCoordSys( coord_sys );
	iter -> prettyPrint();
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }

  delete coord_sys;
  delete global;
}

//------------------------------------------------------------------
void TestXMLInputHandler::testElement()
{
  try {
    for( InputHandler::ElementIterator iter = xml_input_handler_ -> begin<Element>(); 
	 iter != xml_input_handler_ -> end<Element>(); 
	 ++iter)
      {
	iter -> prettyPrint();
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testElementType()
{
  try {
    for( InputHandler::ElementTypeIterator iter = xml_input_handler_ -> begin<ElementType>(); 
	 iter != xml_input_handler_ -> end<ElementType>(); 
	 ++iter)
      {
	iter -> prettyPrint();
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testMaterial()
{
  try {
  for( InputHandler::MaterialIterator iter = xml_input_handler_ -> begin<Material>(); 
       iter != xml_input_handler_ -> end<Material>(); 
       ++iter)
    {
      iter -> prettyPrint();
      delete &(*iter);
    }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testDataSet()
{
  try {
    for( InputHandler::DataSetIterator iter = xml_input_handler_ -> begin<DataSet>(); 
	 iter != xml_input_handler_ -> end<DataSet>(); 
	 ++iter)
      {
	iter -> prettyPrint();	
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testPrescribedDOF()
{
  try {
    for( InputHandler::PrescribedDOFIterator iter = xml_input_handler_ -> begin<PrescribedDOF>(); 
	 iter != xml_input_handler_ -> end<PrescribedDOF>(); 
	 ++iter)
      {
	iter -> prettyPrint();	
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testPrescribedDOFCase()
{
  try {
    for( InputHandler::PrescribedDOFCaseIterator iter = xml_input_handler_ -> begin<PrescribedDOFCase>(); 
	 iter != xml_input_handler_ -> end<PrescribedDOFCase>(); 
	 ++iter)
      {
	iter -> prettyPrint();
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testLoad()
{
  try {
    for( InputHandler::LoadIterator iter = xml_input_handler_ -> begin<Load>(); 
	 iter != xml_input_handler_ -> end<Load>(); 
	 ++iter)
      {
	iter -> prettyPrint();
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testLoadCase()
{
  try {
    for( InputHandler::LoadCaseIterator iter = xml_input_handler_ -> begin<LoadCase>(); 
	 iter != xml_input_handler_ -> end<LoadCase>(); 
	 ++iter)
      {
	iter -> prettyPrint();
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
void TestXMLInputHandler::testBoundary()
{
  try {
    for( InputHandler::BoundaryIterator iter = xml_input_handler_ -> begin<Boundary>(); 
	 iter != xml_input_handler_ -> end<Boundary>(); 
	 ++iter)
      {
	iter -> prettyPrint();
	delete &(*iter);
      }
  } catch(const IOException &exc) {
    CPPUNIT_FAIL( exc.msg() );
  }
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  fs::path main_file_path( fs::system_complete( fs::path( argv[0], fs::native ) ).branch_path() );
  fs::path config = main_file_path / fs::path( TestInputHandler::DOC_NAME+".xml" );
  fs::path logging = main_file_path / fs::path( TestInputHandler::DOC_NAME+".log" );

  try {
    Logger::createLogger( logging , Logger::DEBUG );
    Configuration::createConfiguration( config , Configuration::RO );
    Logger::setPriority( Configuration::getInstance().getLoggerPriorityLevel() );
  } catch (const IOException &exc ) {
    std::cout << exc.msg() << std::endl;
    Configuration::terminate();
    Logger::terminate();
    return( 1 );
  }

  XMLHandlerFactory xml_handler_factory;
  TestXMLInputHandler::xml_input_handler_ =  xml_handler_factory.createInputHandler();
  Document doc(Configuration::getInstance().getIOInputFN(),IOType::XML);

  try {
    TestXMLInputHandler::xml_input_handler_ -> openDocument(doc);
  } catch (const IOException& exc ) {
    std::cout << exc.msg() << std::endl;
    delete TestXMLInputHandler::xml_input_handler_;
    return( 1 );
  }

  CppUnit::TextUi::TestRunner runner;
  runner.addTest( SoofeaTest::TestXMLInputHandler::suite() );
  bool was_successful = runner.run( );

  Configuration::terminate();
  Logger::terminate();
  delete TestXMLInputHandler::xml_input_handler_;

  return( was_successful ? 0 : 1 );
}
