#ifndef test_xml_input_handler_h___
#define test_xml_input_handler_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include "test_input_handler.h"

#include "../io/xml_handler_factory.h"
#include "../../configuration/configuration.h"

SOOFEA_NAMESPACE(SoofeaTest)

//------------------------------------------------------------------
class TestXMLInputHandler : 
  public TestInputHandler ,
  public CppUnit::TestFixture
{
public:
  TestXMLInputHandler();
  virtual ~TestXMLInputHandler();
  static InputHandler* xml_input_handler_;
  
  CPPUNIT_TEST_SUITE( TestXMLInputHandler );
  CPPUNIT_TEST( testGlobal );
  CPPUNIT_TEST( testContact );
  CPPUNIT_TEST( testNode );
  CPPUNIT_TEST( testElement );
  CPPUNIT_TEST( testElementType );
  CPPUNIT_TEST( testMaterial );
  CPPUNIT_TEST( testDataSet );
  CPPUNIT_TEST( testPrescribedDOF );
  CPPUNIT_TEST( testPrescribedDOFCase );
  CPPUNIT_TEST( testLoad );
  CPPUNIT_TEST( testLoadCase );
  CPPUNIT_TEST( testBoundary );
  CPPUNIT_TEST_SUITE_END();
 
  virtual void testGlobal();
  virtual void testContact();
  virtual void testNode();
  virtual void testElement();
  virtual void testElementType();
  virtual void testMaterial();
  virtual void testDataSet();
  virtual void testPrescribedDOF();
  virtual void testPrescribedDOFCase();
  virtual void testLoad();
  virtual void testLoadCase();
  virtual void testBoundary();
};

SOOFEA_NAMESPACE_END

#endif // test_xml_input_handler_h___
