<?xml version="1.0"?>
<soofea>
*format "%i"
  <global dim="*GenData(1)" coord_sys_type="*GenData(2)"/>
  <node>
*set elems(all)
*loop nodes
*format "%i"
    <n N="*NodesNum"*\
*format "%.10f"
 x="*NodesCoord(1,real)"*\
*if((GenData(1,int))>=2)
*format "%.10f" 
 y="*NodesCoord(2,real)"*\
*endif
*if((GenData(1,int))==3)
*format "%.10f" 
 z="*NodesCoord(3,real)"*\
*endif
/>
*end nodes
  </node>
  <boundary>
     <!-- Here insert the geometry entries (<b id="line">,<b id="arc">) -->
  </boundary>
  <element>
*loop elems
    <e gnn="*ElemsConec" m="*ElemsMat" et="1" N ="*ElemsNum" />
*end elems
  </element>
  <element_type>
    <et id="*GenData(3)" real_const_set="*operation(nmats+1)" N="1" />
  </element_type>
  <constraint>
*Set Cond Point-Displacement *nodes
*set var condnum = 0
*loop nodes *OnlyInCond
*set var condnum = operation(condnum+1)
    <c id="node" gnn="*NodesNum"*\
*for(count=1;count<=3;count=count+1)
*set var buffer=cond(*operation(2*count-1),real)
*if(buffer>0.05)
*format "%.5f%i"
*if(count==1)
 x="*cond(*operation(2*count))"*\
*elseif(count==2)
 y="*cond(*operation(2*count))"*\
*else
 z="*cond(*operation(2*count))"*\
*endif
*endif
*endfor
N="*condnum" />
*end nodes
  </constraint>
  <load>
*set var loadnum = 0
*Set Cond Point-Force *nodes
*set var loadnum = operation(loadnum+1)
*loop nodes *OnlyInCond
*format "%i%.5f%.5f%.5f%i"
    <l id="force" gnn="*NodesNum" vector="*cond(1) *cond(2) *cond(3)" N="*loadnum" />
*end nodes
  </load>
  <material>
*loop materials
    <m id="*MatProp(0)" real_const_set="*MatNum" N="*MatNum" />
*end materials
  </material>
</soofea>
