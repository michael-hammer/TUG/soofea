#! /usr/bin/env python

from xml.etree import ElementTree
from argparse import ArgumentParser
from string import Template
import sys, os, codecs

header_template = os.path.join( os.path.split( os.path.realpath(__file__) )[0] , 'constants.h.template' )
cpp_template = os.path.join( os.path.split( os.path.realpath(__file__) )[0] , 'constants.cpp.template' )
py_template = os.path.join( os.path.split( os.path.realpath(__file__) )[0] , 'constants.py.template' )
verbose=False

py_resolve_template = Template('''//------------------------------------------------------------------
struct PyResolve
{
  static std::string resolveID_ID(${class_name}::${enum_typename} id) {
    return( ${class_name}::resolveID( id ) );
  }

  static ${class_name}::${enum_typename} resolveID_string(std::string id) {
    return( ${class_name}::resolveID( id ) );
  }
};''')

cpp_resolve_template = Template('''//------------------------------------------------------------------
const std::string& ${class_name}::resolveID(${enum_typename} id)
{
  switch(id) {
  ${resolve_id}default:
    return( NAID_STR );
  };
}

//------------------------------------------------------------------
${class_name}::${enum_typename} ${class_name}::resolveID(const std::string& id_string)
{
  ${resolve_string}
  return( IDObject::NAID );
}''')

h_enum_template = Template('''  typedef enum ${enum_typename}_ {
${enum_ids}
    NAID
  } ${enum_typename};

  static const std::string& resolveID(${enum_typename} id);
  static ${enum_typename} resolveID(const std::string& id_string);''')

#------------------------------------------------------------------
def multpleEntries( tree , bidirectional ):
    ids = set()
    strings = set()
    for constant in list(tree.getroot()):
        if constant.attrib['id'] in ids:
            raise Exception("Found id '"+constant.attrib['id']+"' twice in input file")
        ids.add(constant.attrib['id'])

        if bidirectional:
            if (constant.attrib['string'] in strings):
                raise Exception("Found string '"+constant.attrib['string']+"' twice in input file")
            strings.add(constant.attrib['string'])

#------------------------------------------------------------------
def underscoreToCamelCase( underscore_string ):
    camel_case = ''
    for word in underscore_string.split("_"):
        # some special cases
        if word == 'id':
            camel_case += 'ID'
            continue
        camel_case += word.capitalize()
    return camel_case

#------------------------------------------------------------------
def createEnumDefinition( tree ):
    enum = tree.getroot().attrib['enum']
    if verbose:
        print "Enum typename: "+enum

    enum_ids = ''
    for constant in list(tree.getroot()):
        enum_ids += '    %(id)s,\n' % {'id':constant.attrib['id']}

    return h_enum_template.substitute( enum_typename=enum, enum_ids=enum_ids )

#------------------------------------------------------------------
def createPyEnumDefinition( tree ):
    enum = tree.getroot().attrib['enum']
    class_name = tree.getroot().attrib['name']
    enum_defs = ''
    enum_defs += "enum_<%(class_name)s::%(enum)s>(\"%(enum)s\")\n" % {'class_name': class_name, 'enum': enum}
    for constant in list(tree.getroot()):
        enum_defs += "    .value(\"%(id)s\",%(class_name)s::%(id)s)\n" % {'class_name': class_name, 'id': constant.attrib['id'] }
    enum_defs += ";\n"
    return enum_defs

#------------------------------------------------------------------
def createEnumResolve( tree ):
    class_name = tree.getroot().attrib['name']
    string_suffix = tree.getroot().attrib['str_suffix']
    resolve_id = ''
    resolve_string = ''
    for constant in list(tree.getroot()):
        resolve_id += '''case %(id)s:
    return( %(id)s%(suffix)s );
  ''' % {'id':constant.attrib['id'], 'suffix':string_suffix}
        resolve_string += '''if( id_string == %(id)s%(suffix)s )
    return( %(id)s );
  ''' % {'id':constant.attrib['id'], 'suffix':string_suffix}

    return cpp_resolve_template.substitute( enum_typename=tree.getroot().attrib['enum'], \
                                            class_name=class_name, \
                                            resolve_id=resolve_id, \
                                            resolve_string=resolve_string )

#------------------------------------------------------------------
def createStringDefinition( tree ):
    string_suffix = tree.getroot().attrib['str_suffix']
    string_defs = ''
    for constant in list(tree.getroot()):
        string_defs += '  static const std::string %(id)s%(suffix)s;\n' % {'id':constant.attrib['id'], 'suffix':string_suffix}
    string_defs += '  static const std::string NAID%(suffix)s;\n' % {'suffix':string_suffix}
    return string_defs

#------------------------------------------------------------------
def createPyStringDefinition( tree ):
    string_suffix = tree.getroot().attrib['str_suffix']
    class_name = tree.getroot().attrib['name']
    string_defs = ''
    for constant in list(tree.getroot()):
        string_defs += '    .def_readonly("%(id)s",&%(class_name)s::%(id)s%(suffix)s)\n' \
            % {'id':constant.attrib['id'], 'suffix':string_suffix, 'class_name': class_name}
    return string_defs

#------------------------------------------------------------------
def createStringInit( tree ):
    string_suffix = tree.getroot().attrib['str_suffix']
    class_name = tree.getroot().attrib['name']
    string_inits = ''
    for constant in list(tree.getroot()):
        string_inits += 'const std::string %(class_name)s::%(id)s%(suffix)s = "%(str)s";\n' % \
                        {'id':constant.attrib['id'],\
                         'class_name':class_name,\
                         'str':constant.attrib['string'],\
                         'suffix':string_suffix}
    string_inits += 'const std::string %(class_name)s::NAID%(suffix)s = "NAID";\n' % {'class_name':class_name, 'suffix':string_suffix}
    return string_inits

#------------------------------------------------------------------
def main():
    parser = ArgumentParser(description="SOOFEA Constants", \
                            epilog="A code preprocessor which creates a class with the constants defined by XML file")
    parser.add_argument('input', help="Open INPUT file as constants configuration xml file", metavar="INPUT")
    parser.add_argument('output', help="The class is written to OUTPUT(.h|.cpp) file ", metavar="OUTPUT")
    parser.add_argument('-m','--mode', help="Either create the 'c++' (default) class code or the enum bindings for 'boost.python'",
                        default='c++')
    parser.add_argument('-v',"--verbose", action='store_true', \
                        help="Be more verbose about what we are doing")
    args = parser.parse_args()

    verbose = args.verbose

    output_base_name = os.path.splitext(args.output)[0]

    if not os.path.exists( args.input ):
        print "Input file does not exist, for help use '--help'"
        sys.exit(1)

    if os.path.exists( output_base_name+'.h' ) or os.path.exists( output_base_name+'.cpp' ):
        print "Output file does exist and will be overwritten!"

    if args.verbose:
        print "Input file = '"+args.input+"'"
        print "Output files = '"+output_base_name+".h' and '"+output_base_name+".cpp'"

    config_file = codecs.open( args.input, 'r', 'utf-8' )
    tree = ElementTree.parse( config_file )

    try:
        if( 'enum' in tree.getroot().attrib ):
            multpleEntries( tree , bidirectional=True )
        else:
            multpleEntries( tree , bidirectional=False )
    except Exception as exc:
        print exc
        sys.exit(1)

    class_name = tree.getroot().attrib['name']
    if( args.mode == 'c++' ):
        output_h_file = open( output_base_name+'.h', 'w' )
        output_cpp_file = open( output_base_name+'.cpp', 'w' )
        header = Template( open( header_template, 'r' ).read() )
        cpp = Template( open( cpp_template, 'r' ).read() )

        if( class_name != underscoreToCamelCase( os.path.splitext( os.path.basename(args.output))[0] ) ) and args.verbose:
            print "File name of output and configured class name do not coincide in our naming scheme!"
            if args.verbose:
                print "Class name: "+class_name

        string_suffix = '_STR'
        enum_definition = ''
        enum_resolve = ''
        if( 'enum' in tree.getroot().attrib ):
            enum_definition = createEnumDefinition( tree )
            enum_resolve = createEnumResolve( tree )

        string_defs = createStringDefinition( tree )
        string_inits = createStringInit( tree )

        output_h_file.write( header.substitute( class_name=class_name, \
                                                input_file=(os.path.split(os.path.splitext(args.input)[0])[1]), \
                                                enum_definition=enum_definition, string_constants=string_defs ) )
        output_cpp_file.write( cpp.substitute( output_file=os.path.split(args.output)[1]+".h",\
                                               input_file=args.input,\
                                               string_constants_init=string_inits, class_name=class_name , enum_resolve=enum_resolve  ) )
    elif( args.mode == 'boost.python' ):
        output_file = open( output_base_name+'.h', 'w' )
        tpl = Template( open( py_template, 'r' ).read() )

        string_defs = createPyStringDefinition( tree )
        enum_defs = ''
        enum_resolve = ''
        if( 'enum' in tree.getroot().attrib ):
            enum_defs = createPyEnumDefinition( tree )
            enum_resolve = py_resolve_template.substitute( class_name=class_name, \
                                                               enum_typename=tree.getroot().attrib['enum'] )

        output_file.write( tpl.substitute( class_name=class_name, \
                                               input_file=(os.path.split(os.path.splitext(args.input)[0])[1]), \
                                               enum_defs=enum_defs, string_defs=string_defs, enum_resolve=enum_resolve ) )

#------------------------------------------------------------------
if __name__ == "__main__":
    main()
