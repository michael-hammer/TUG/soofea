#
# Locate Intel Threading Building Blocks include paths and libraries
# CPPunit can be found at http://cppunit.sourceforge.net 
# Written by Michael Hammer, michael _at_ derhammer.net

# This module defines
# TBB_INCLUDE_DIR, where to find ptlib.h, etc.
# TBB_LIBRARIES, the libraries to link against to use pwlib.
# TBB_FOUND, If false, don't try to use pwlib.

FIND_PATH(TBB_INCLUDE_DIR tbb/task_scheduler_init.h
    /usr/local/include
    /usr/include
)

set( PATH )
set( TBB_LIBS tbb tbbmalloc )
set( TBB_LIBS_DEBUG tbb_debug tbbmalloc_debug )
FIND_MULTIPLE_LIBRARIES( TBB_LIBRARIES TBB_LIBS PATH )
FIND_MULTIPLE_LIBRARIES( TBB_LIBRARIES_DEBUG TBB_LIBS_DEBUG PATH )

SET(TBB_FOUND 0)
IF(TBB_INCLUDE_DIR)
  IF(TBB_LIBRARIES)
    SET(TBB_FOUND 1)
    MESSAGE(STATUS "Found Intel TBB")
  ENDIF(TBB_LIBRARIES)
ENDIF(TBB_INCLUDE_DIR)
