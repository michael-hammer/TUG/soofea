#
# Locate BLAS include paths and libraries
# BLAS can be found at www.netlib.org/blas
# Written by Michael Hammer, michael _at_ derhammer.net

# This module defines
# BLAS_INCLUDE_DIR, where to find ptlib.h, etc.
# BLAS_LIBRARIES, the libraries to link against to use pwlib.
# BLAS_FOUND, If false, don't try to use pwlib.

FIND_LIBRARY(BLAS_LIBRARIES blas
  PATHS
    "$ENV{BLAS}/lib/win32"
    /usr/local/lib
    /usr/lib
)

SET(BLAS_FOUND 0)
IF(BLAS_LIBRARIES)
  SET(BLAS_FOUND 1)
  MESSAGE(STATUS "Found BLAS")
ENDIF(BLAS_LIBRARIES)

MARK_AS_ADVANCED(
  BLAS_INCLUDE_DIR
  BLAS_LIBRARIES
) 
