#
# Locate CPPUnit include paths and libraries
# CPPunit can be found at http://cppunit.sourceforge.net
# Written by Michael Hammer, michael _at_ derhammer.net

# This module defines
# CPPUNIT_INCLUDE_DIR, where to find ptlib.h, etc.
# CPPUNIT_LIBRARIES, the libraries to link against to use pwlib.
# CPPUNIT_FOUND, If false, don't try to use pwlib.

FIND_PATH(CPPUNIT_INCLUDE_DIR cppunit/TestAssert.h
    /usr/local/include
    /usr/include
)

FIND_LIBRARY(CPPUNIT_LIBRARIES
  NAMES 
    cppunit
  PATHS
    /usr/local/lib
    /usr/lib
)

SET(CPPUNIT_FOUND 0)
IF(CPPUNIT_INCLUDE_DIR)
  IF(CPPUNIT_LIBRARIES)
    SET(CPPUNIT_FOUND 1)
    MESSAGE(STATUS "Found CPPUnit")
  ENDIF(CPPUNIT_LIBRARIES)
ENDIF(CPPUNIT_INCLUDE_DIR)

MARK_AS_ADVANCED(
  CPPUNIT_INCLUDE_DIR
  CPPUNIT_LIBRARIES
) 