# add a unit test, which is executed when running make test
macro(SOOFEA_ADD_UNIT_TEST _test_NAME)
    set(_srcList ${ARGN})
    set(_targetName UNIT_${_test_NAME})
    soofea_add_unit_test_executable( ${_test_NAME} ${_srcList})
    add_test(${_targetName} ${EXECUTABLE_OUTPUT_PATH}/tests/${_test_NAME})
endmacro()

# add a unit test executable
macro(SOOFEA_ADD_UNIT_TEST_EXECUTABLE _target_NAME)
  include_directories(${CPPUNIT_INCLUDE_DIR})
  add_executable(${_target_NAME} ${ARGN})
  set_target_properties(${_target_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${EXECUTABLE_OUTPUT_PATH}/tests)
  target_link_libraries(${_target_NAME} ${CPPUNIT_LIBRARIES})
endmacro()

# # add ctest 
# macro (SOOFEA_ADD_TEST _test_NAME)
#     set(_srcList ${ARGN})
#     set(_targetName ${_test_NAME})
#     soofea_add_test_executable( ${_test_NAME} ${_srcList})
#     add_test( ${_targetName} ${EXECUTABLE_OUTPUT_PATH}/${_test_NAME} )
# endmacro (SOOFEA_ADD_TEST)

# # add test executable
# macro (SOOFEA_ADD_TEST_EXECUTABLE _target_NAME)
#    set(_add_executable_param)
#    include_directories (${CPPUNIT_INCLUDE_DIR})
#    add_executable(${_target_NAME} ${_add_executable_param} ${ARGN})
#    target_link_libraries(${_target_NAME})
# endmacro (SOOFEA_ADD_TEST_EXECUTABLE)

# add subdirectory sources - in the CmakeLists.txt there has to be a variable 
# called: ${subdir}_SOURCES which contains all sources. This variable contains
# afterwards all source files and can be user in add_library or add_executable
macro(ADD_SUBDIR_SOURCES _subdir_NAME)
  get_directory_property( ${_subdir_NAME}_SRCS DIRECTORY ${_subdir_NAME} DEFINITION ${_subdir_NAME}_SOURCES )
  foreach( _runner ${${_subdir_NAME}_SRCS} )
    set( ${_subdir_NAME}_SOURCES ${${_subdir_NAME}_SOURCES} ${_subdir_NAME}/${_runner} )
  endforeach( _runner )
endmacro(ADD_SUBDIR_SOURCES)

macro(FIND_MULTIPLE_LIBRARIES _store_ _library_list_ _paths_)
  foreach( LIB ${${_library_list_}} )
    set( LIB_FOUND ${LIB}-NOTFOUND )
    find_library( LIB_FOUND NAMES ${LIB} PATHS ${${_paths_}} NO_DEFAULT_PATHS )
    if( LIB_FOUND )
      LIST( APPEND ${_store_} ${LIB_FOUND} )
    endif( LIB_FOUND )
  endforeach( LIB )
endmacro(FIND_MULTIPLE_LIBRARIES)
