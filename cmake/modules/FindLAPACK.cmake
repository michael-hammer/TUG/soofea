#
# Locate LAPACK include paths and libraries
# LAPACK can be found at www.netlib.org/lapack
# Written by Michael Hammer, michael _at_ derhammer.net

# This module defines
# LAPACK_INCLUDE_DIR, where to find ptlib.h, etc.
# LAPACK_LIBRARIES, the libraries to link against to use pwlib.
# LAPACK_FOUND, If false, don't try to use pwlib.

SET(LAPACK_FOUND 0)
SET(LAPACK_LIBRARIES)

set( MKL_LIBS mkl_intel_lp64 mkl_intel_thread mkl_core )
set( MKL_PATHS /opt/intel/mkl/lib/intel64 )
FIND_MULTIPLE_LIBRARIES( LAPACK_LIBRARIES MKL_LIBS MKL_PATHS )
IF(LAPACK_LIBRARIES)
  FIND_PATH(LAPACK_INCLUDE_DIR mkl_lapack.h
    /opt/intel/mkl/include
    )
  SET(LAPACK_FOUND 1)
  SET(LAPACK_TYPE "mkl")
  MESSAGE(STATUS "Found Intel MKL LAPACK")
  RETURN()
ENDIF(LAPACK_LIBRARIES)

# Then we look for a standard liblapack implementation
FIND_LIBRARY(LAPACK_LIBRARIES lapack
  PATHS
  /usr/local/lib
  /usr/lib
  "$ENV{LAPACK}/lib/win32"
)
IF(LAPACK_LIBRARIES)
  FIND_PATH(LAPACK_INCLUDE_DIR lapack.h
    ${PROJECT_SOURCE_DIR}/include
    )
  SET(LAPACK_FOUND 1)
  SET(LAPACK_TYPE "ref")
  MESSAGE(STATUS "Found LAPACK")
  RETURN()
ENDIF(LAPACK_LIBRARIES)
