#
# Locate blitz++ include paths and libraries
# blitz++ can be found at http://www.oonumerics.org/blitz/
# Written by Michael Hammer, michael.hammer_at_tugraz.at

# This module defines
# BLITZ_INCLUDE_DIR, where to find ptlib.h, etc.
# BLITZ_LIBRARIES, the libraries to link against to use pwlib.
# BLITZ_FOUND, If false, don't try to use pwlib.

FIND_PATH(BLITZ_INCLUDE_DIR blitz/blitz.h
    "$ENV{BLITZ}"
    /usr/local/include
    /usr/include
)

FIND_LIBRARY(BLITZ_LIBRARIES blitz
  PATHS
    "$ENV{BLITZ}/lib"
    /usr/local/lib
    /usr/lib
)

SET(BLITZ_FOUND 0)
IF(BLITZ_INCLUDE_DIR)
  IF(BLITZ_LIBRARIES)
    SET(BLITZ_FOUND 1)
    MESSAGE(STATUS "Found Blitz++")
  ENDIF(BLITZ_LIBRARIES)
ENDIF(BLITZ_INCLUDE_DIR)

MARK_AS_ADVANCED(
  BLITZ_INCLUDE_DIR
  BLITZ_LIBRARIES
) 