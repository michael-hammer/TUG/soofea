#
# Locate GIDPOST library
# Written by Michael Hammer, michael _at_ derhammer.net

# This module defines
# GIDPOST_LIBRARIES, the libraries to link against.
# GIDPOST_FOUND, If false, don't try to use pwlib.

FIND_LIBRARY(GIDPOST_LIBRARIES gidpost
  PATHS
    "$ENV{GIDPOST}/lib"
    /usr/local/lib
    /usr/lib
)

SET(GIDPOST_FOUND 0)
IF(GIDPOST_LIBRARIES)
  SET(GIDPOST_FOUND 1)
  MESSAGE(STATUS "Found GiDpost")
ENDIF(GIDPOST_LIBRARIES)

MARK_AS_ADVANCED(
  GIDPOST_INCLUDE_DIR
  GIDPOST_LIBRARIES
) 