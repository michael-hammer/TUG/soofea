<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" version="0.81">
  <!--  <xsd:include schemaLocation="common.xsd"/> -->

  <xs:simpleType name="int_array">
    <xs:restriction base="xs:string">
      <xs:pattern value="(([0-9])* )*([0-9])+"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="float_array">
    <xs:restriction base="xs:string" />
    <!-- Here we should enhance with appropriate pattern
      <xs:pattern value="(([0-9])*.([0-9])* )*"/>
    </xs:restriction> -->
  </xs:simpleType>

  <xs:simpleType name="dimension">
    <xs:restriction base="xs:integer">
      <xs:minInclusive value="1"/>
      <xs:maxInclusive value="3"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="coord_sys_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="cartesian"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="analysis_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="linear"/>
      <xs:enumeration value="newton_raphson"/>
      <xs:enumeration value="mortar.fixed_point"/>
      <xs:enumeration value="mortar.semi_smooth"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="sle_solution_method_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="cholesky"/>
      <xs:enumeration value="lu"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="global">
    <xs:attribute name="id" type="xs:string"/>
    <xs:attribute name="dim" type="dimension" use="required"/>
    <xs:attribute name="coord_sys_type" type="coord_sys_id" use="required"/>
    <xs:attribute name="analysis_type" type="analysis_id" use="required"/>
    <xs:attribute name="sle_solution_method" type="sle_solution_method_id" use="required"/>
    <xs:attribute name="convergence_criteria" type="xs:float"/>
  </xs:complexType>
  
  <xs:complexType name="contact">
    <xs:attribute name="mortar" type="int_array"/>
    <xs:attribute name="non_mortar" type="int_array"/>
    <xs:attribute name="penalty" type="xs:float"/>
    <xs:attribute name="friction" type="xs:string"/>
    <xs:attribute name="mu" type="xs:float"/>
  </xs:complexType>
  
  <xs:complexType name="node">
    <xs:attribute name="N" type="xs:integer" use="required"/>
    <xs:attribute name="x" type="xs:float"/>
    <xs:attribute name="y" type="xs:float"/>
    <xs:attribute name="z" type="xs:float"/>
    <xs:attribute name="u" type="xs:float"/>
    <xs:attribute name="v" type="xs:float"/>
    <xs:attribute name="w" type="xs:float"/>
    <xs:attribute name="f" type="float_array"/>
    <xs:attribute name="lagrange" type="float_array"/>
  </xs:complexType>

  <xs:complexType name="node_container">
    <xs:attribute name="N" type="xs:integer" use="required"/>
    <xs:attribute name="type" type="xs:integer" use="required"/>
    <xs:attribute name="gnn" type="int_array" use="required"/>
  </xs:complexType>

  <xs:complexType name="element">
    <xs:complexContent>
      <xs:extension base="node_container">
	<xs:attribute name="m" type="xs:integer" use="required"/>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="boundary">
    <xs:attribute name="N" type="xs:integer" use="required"/>
    <xs:attribute name="gnn" type="int_array"/>
    <xs:attribute name="gen" type="int_array"/>
    <xs:attribute name="gfn" type="int_array"/>
  </xs:complexType>
  <xs:complexType name="boundary_container"> 
    <xs:sequence>
      <xs:element name="loop" maxOccurs="unbounded">
	<xs:complexType>
	  <xs:sequence>
	    <xs:element name="b" type="boundary"  maxOccurs="unbounded"/>
	  </xs:sequence>
	  <xs:attribute name="N" type="xs:integer" use="required"/>
	</xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>

  <xs:simpleType name="shape_type_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="linear"/>
      <xs:enumeration value="quad"/>
      <xs:enumeration value="brick"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="type_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="continuum"/>
      <xs:enumeration value="boundary"/>
      <xs:enumeration value="mortar.PGM"/>
      <xs:enumeration value="mortar.LGM"/>
      <xs:enumeration value="mortar.LGA"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="target_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="edge"/>
      <xs:enumeration value="face"/>
      <xs:enumeration value="element"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="type">
    <xs:attribute name="N" type="xs:integer" use="required"/>
    <xs:attribute name="id" type="type_id" use="required"/>
    <xs:attribute name="target" type="target_id" use="required"/>
    <xs:attribute name="int_points" type="int_array" use="required"/>
    <xs:attribute name="shape_order" type="xs:integer" use="required"/>
    <xs:attribute name="shape_type" type="shape_type_id" use="required"/>
  </xs:complexType>

  <xs:complexType name="prescribed_dof">
    <xs:attribute name="N" type="xs:integer" use="required"/>
    <xs:attribute name="gnn" type="int_array"/>
    <xs:attribute name="gbn" type="xs:integer"/>
    <xs:attribute name="u" type="xs:float"/>
    <xs:attribute name="v" type="xs:float"/>
    <xs:attribute name="w" type="xs:float"/>
    <xs:attribute name="t" type="xs:float"/>
  </xs:complexType>

  <xs:complexType name="prescribed_load">
    <xs:attribute name="N" type="xs:integer" use="required"/>
    <xs:attribute name="gnn" type="int_array"/>
    <xs:attribute name="gbn" type="xs:integer"/> 
    <xs:attribute name="fS" type="float_array"/>
    <xs:attribute name="p" type="xs:float"/>
    <xs:attribute name="q" type="xs:float"/>
  </xs:complexType>

  <xs:complexType name="progress">
    <xs:attribute name="id" use="required">
      <xs:simpleType>
	<xs:restriction base="xs:string">
	  <xs:enumeration value="const"/>
	  <xs:enumeration value="ramp"/>
	</xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="dof" type="int_array"/>
    <xs:attribute name="load" type="int_array"/>
  </xs:complexType>

  <xs:complexType name="time_element">
    <xs:sequence>
      <xs:element name="progress" type="progress" maxOccurs="unbounded"/>
    </xs:sequence>
    <xs:attribute name="N" type="xs:integer" use="required"/>
    <xs:attribute name="end_time" type="xs:float" use="required"/>
    <xs:attribute name="step_amount" type="xs:integer" use="required"/>
  </xs:complexType>

  <xs:complexType name="material">
    <xs:sequence>
      <xs:element name="prop" type="property" maxOccurs="unbounded"/>
    </xs:sequence>    
    <xs:attribute name="N" type="xs:integer" use="required"/>
    <xs:attribute name="id" use="required">
      <xs:simpleType>
	<xs:restriction base="xs:string">
	  <xs:enumeration value="neo_hookean"/>
	  <xs:enumeration value="st_venant_kirchhoff"/>
	</xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>

  <xs:complexType name="property">
    <xs:attribute name="id" type="xs:string" use="required"/>
    <xs:attribute name="value" type="xs:string" use="required"/>
    <xs:attribute name="type" use="required">
      <xs:simpleType>
	<xs:restriction base="xs:string">
	  <xs:enumeration value="double"/>
	  <xs:enumeration value="int"/>
	</xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>

  <xs:complexType name="result">
    <xs:sequence>
      <xs:element name="time_stamp" maxOccurs="unbounded">
	<xs:complexType>
	  <xs:sequence>
	    <xs:element name="node">
	      <xs:complexType>
		<xs:sequence>
		  <xs:element name="n" type="node" maxOccurs="unbounded"/>
		</xs:sequence>
	      </xs:complexType>
	    </xs:element>
	  </xs:sequence>
	  <xs:attribute name="N" type="xs:integer" use="required"/>
	  <xs:attribute name="time" type="xs:float" use="required"/>
	</xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="last_time_index" type="xs:integer" use="required"/>
  </xs:complexType>

  <xs:simpleType name="file_type_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="XML"/>
      <xs:enumeration value="GiD"/>
      <xs:enumeration value="TXT"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="priority_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="EMERG"/>
      <xs:enumeration value="FATAL"/>
      <xs:enumeration value="ALERT"/>
      <xs:enumeration value="CRIT"/>
      <xs:enumeration value="ERROR"/>
      <xs:enumeration value="WARN"/>
      <xs:enumeration value="NOTICE"/>
      <xs:enumeration value="INFO"/>
      <xs:enumeration value="DEBUG"/>
      <xs:enumeration value="NOTSET"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="write_id">
    <xs:restriction base="xs:string">
      <xs:enumeration value="no_write"/>
      <xs:enumeration value="final"/>
      <xs:enumeration value="time_step"/>
      <xs:enumeration value="iteration"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="file_path_holder">
    <xs:all minOccurs="0">
      <xs:element name="file">
	<xs:complexType>
	  <xs:attribute name="name" type="xs:string" use="required"/>
	  <xs:attribute name="type" type="file_type_id"/>
	</xs:complexType>
      </xs:element>
    </xs:all>	      
  </xs:complexType>
  
  <xs:complexType name="logging">
    <xs:complexContent>
      <xs:extension base="file_path_holder">
	<xs:attribute name="priority" type="priority_id"/>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="configuration">
    <xs:all>
      <xs:element name="output">
	<xs:complexType>
	  <xs:complexContent>
	    <xs:extension base="file_path_holder">
	      <xs:attribute name="write" type="write_id"/>
	    </xs:extension>
	  </xs:complexContent>
	</xs:complexType>
      </xs:element>
      <xs:element name="math">
	<xs:complexType>
	  <xs:sequence>
	    <xs:element name="num_int_config" type="file_path_holder"/>
	  </xs:sequence>	    
	</xs:complexType>
      </xs:element>	  
      <xs:element name="logging" type="logging"/>
    </xs:all>
  </xs:complexType>

  <xs:element name="soofea">
    <xs:complexType>
      <xs:all>
	<xs:element name="global" type="global"/>
	<xs:element name="configuration" type="configuration"/>
	<xs:element name="contact" type="contact" minOccurs="0"/>
	<xs:element name="node">
	  <xs:complexType>
	    <xs:sequence>
	      <xs:element name="n" type="node" maxOccurs="unbounded"/>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="edge" minOccurs="0">
	  <xs:complexType>
	    <xs:sequence>
	      <xs:element name="ed" type="node_container" maxOccurs="unbounded"/>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="face" minOccurs="0">
	  <xs:complexType>
	    <xs:sequence>
	      <xs:element name="f" type="node_container" maxOccurs="unbounded"/>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="element">
	  <xs:complexType>
	    <xs:sequence>
	      <xs:element name="el" type="element" maxOccurs="unbounded"/>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="boundary" type="boundary_container" minOccurs="0"/>
	<xs:element name="type">
	  <xs:complexType>
	    <xs:sequence>
	      <xs:element name="type" type="type" maxOccurs="unbounded"/>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="prescribed_dof">
	  <xs:complexType>
	    <xs:sequence>
	      <xs:element name="dof" type="prescribed_dof" maxOccurs="unbounded"/>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="prescribed_load" minOccurs="0">
	  <xs:complexType>
	    <xs:sequence minOccurs="0" maxOccurs="unbounded">
	      <xs:element name="load" type="prescribed_load" maxOccurs="unbounded"/>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="time_bar" minOccurs="0">
	  <xs:complexType>
	    <xs:sequence>
	      <xs:element name="te" type="time_element">
	      </xs:element>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="material">
	  <xs:complexType>
	    <xs:sequence>
	      <xs:element name="m" type="material" maxOccurs="unbounded"/>
	    </xs:sequence>
	  </xs:complexType>
	</xs:element>
	<xs:element name="result" type="result" minOccurs="0"/>
      </xs:all>
      <xs:attribute name="version" type="xs:string" use="required"/>
    </xs:complexType>
  </xs:element>
</xs:schema>
