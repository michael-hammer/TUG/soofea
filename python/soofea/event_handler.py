""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2007-2010 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

from subprocess import Popen
from number_cruncher import NumberCruncher

#------------------------------------------------------------------
class EventHandler :
    #------------------------------------------------------------------
    def __init__(self, finish_hook) :
        self.finished = finish_hook        

    #------------------------------------------------------------------
    def startCalc(self, filename) :
        self.number_cruncher = NumberCruncher( self.finishedCalc, self.runningCalc, filename )
        self.number_cruncher.start()

    #------------------------------------------------------------------
    def stopCalc(self ):
        Popen("kill -9 "+str(self.pid), shell=True)
        self.finished()

    #------------------------------------------------------------------
    def finishedCalc(self) :
        self.finished()

    #------------------------------------------------------------------
    def runningCalc(self, pid) :
        self.pid = pid

