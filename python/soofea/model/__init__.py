from .._SOOFEA import Model
from .._SOOFEA import Node
from .._SOOFEA import Element
from .._SOOFEA import Edge

from ..injector import Injector

from soofea.geometry import CoordSys

class MyNode(Injector,Node):
    def __str__(self):
        return "Node: " + str(self.getNumber()) + "\t lagrangian coord. = "

    def plot( self , axes , time_stamp, deformation_factor=1. ):
        point = self.getEulerianPoint(time_stamp,deformation_factor)
        axes.text( point.getCoordinate(CoordSys.CoordID.X) , point.getCoordinate(CoordSys.CoordID.Y) , str( self.getNumber() ) )

class MyElement(Injector,Element):
    def plot( self , axes , time_stamp, deformation_factor=1.,lineweight=0.5 ):
        ### @fixme works only for quadrilaterals! -> Therefor should be part of element spezializations
        for i in range(0,4):
            point_1 = self.getLocalNode(i).getEulerianPoint(time_stamp,deformation_factor)
            if ( i == 3 ):
                point_2 = self.getLocalNode(0).getEulerianPoint(time_stamp,deformation_factor)
            else:
                point_2 = self.getLocalNode(i+1).getEulerianPoint(time_stamp,deformation_factor)
            axes.plot( [ point_1.getCoordinate(CoordSys.CoordID.X) , point_2.getCoordinate(CoordSys.CoordID.X) ] , \
                       [ point_1.getCoordinate(CoordSys.CoordID.Y) , point_2.getCoordinate(CoordSys.CoordID.Y) ] , 'k-', lw=lineweight )

class MyEdge(Injector,Edge):
    def plot( self , axes , time_stamp, deformation_factor=1.,lineweight=0.5 ):
        point_1 = self.getLocalNode(0).getEulerianPoint(time_stamp,deformation_factor)
        point_2 = self.getLocalNode(1).getEulerianPoint(time_stamp,deformation_factor)
        axes.plot( [ point_1.getCoordinate(CoordSys.CoordID.X) , point_2.getCoordinate(CoordSys.CoordID.X) ] , \
                   [ point_1.getCoordinate(CoordSys.CoordID.Y) , point_2.getCoordinate(CoordSys.CoordID.Y) ] , 'k-', lw=lineweight )
