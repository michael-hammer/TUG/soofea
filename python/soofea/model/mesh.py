""" ------------------------------------------------------------------
This file is part of soofea.

soofea - Software for Object Oriented Finite Element Analysis
Copyright (C) 2010  Michael Hammer

soofea is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

import re, os
import matplotlib.pyplot as plt
import soofea.model
import soofea.Base
from xml.etree import ElementTree

class Mesh:
    def __init__(self):
        # (x,y,z,t)
        self.node_dict = {}
        # ( material_number, element_type_number, node_numbers )
        self.element_dict = {}
        # ( type_number, node_numbers )
        self.edge_dict = {}
        # ( type_number, node_numbers )
        self.face_dict = {}
        # ( type, node_numbers )
        self.boundary_dict = {}      

    def addNode( self, node_number , x , y , z ):
        self.node_dict[node_number] = {}
        self.node_dict[node_number]['coords'] = [x,y,z]

    def getNodeAmount( self ):
        return len( self.node_dict )
        
    def addElement( self, element_number, node_number, gmsh_number ):
        self.element_dict[element_number] = {}
        self.element_dict[element_number]['nodes'] = node_number
        self.element_dict[element_number]['gmsh_number'] = gmsh_number

    def getElementAmount( self ):
        return len( self.element_dict )

    def addEdge( self, edge_number, node_numbers, gmsh_number ):
        self.edge_dict[edge_number] = {}
        self.edge_dict[edge_number]['nodes'] = node_numbers
        self.edge_dict[edge_number]['gmsh_number'] = gmsh_number

    def getEdgeAmount( self ):
        return len( self.edge_dict )

    def addFace( self, face_number, node_numbers, gmsh_number ):
        self.face_dict[face_number] = {}
        self.face_dict[face_number]['nodes'] = node_numbers
        self.face_dict[face_number]['gmsh_number'] = gmsh_number

    def getFaceAmount( self ):
        return len( self.face_dict )

    def appendTargetToBoundary( self, boundary_nr, target_nr , target_type , mesh_nr ):
        if not boundary_nr in self.boundary_dict:
            self.boundary_dict[boundary_nr] = {}
            self.boundary_dict[boundary_nr]['targets'] = []

        self.boundary_dict[boundary_nr]['targets'].append(target_nr)
        self.boundary_dict[boundary_nr]['target_type'] = target_type
        self.boundary_dict[boundary_nr]['mesh_nr'] = mesh_nr
        
    def concentrateNodes( self ):
        base_nodes = []
        doubled_nodes = []
        node_keylist = self.node_dict.keys()
        node_keylist.sort()

        for i,key in enumerate(node_keylist):
            for j in range(i+1,len(node_keylist)):
                if( abs( self.node_dict[node_keylist[i]][0] - self.node_dict[node_keylist[j]][0] ) < 1e-10 and
                    abs( self.node_dict[node_keylist[i]][1] - self.node_dict[node_keylist[j]][1] ) < 1e-10 ):
                    if node_keylist[j] not in doubled_nodes :
                        base_nodes.append( node_keylist[i] )
                        doubled_nodes.append( node_keylist[j] )

        for i, double_node in enumerate(doubled_nodes):
            print "Delete to base node " + str(base_nodes[i]) + " node " + str(doubled_nodes[i])

            for dummy,element in self.element_dict.iteritems():
                # for number_iter in range(len(element.node_numbers_)):
                for idx,node_number in enumerate(element[2]):
                    if node_number == doubled_nodes[i]:
                        element[2][idx] = base_nodes[i]
                        
            for dummy,boundary in self.boundary_dict.iteritems():               
                #for number_iter in range(len(boundary.node_numbers_)):
                for idx,node_number in enumerate(boundary[1]):
                    if node_number == doubled_nodes[i]:
                        boundary[1][idx] = base_nodes[i]

            del self.node_dict[doubled_nodes[i]]

        node_keylist = self.node_dict.keys()
        node_keylist.sort()
        counter = 1
        for node_number in node_keylist:
            if node_number != counter:
                self.renumberNode( node_number , counter )
            counter = counter + 1

    def renumberNode( self, node_number, new_number ) :
        self.node_dict[new_number] = self.node_dict[node_number]
        del self.node_dict[node_number]

        for dummy,element in self.element_dict.iteritems():
            for idx,element_node_number in enumerate(element[2]):
                if element_node_number == node_number:
                    element[2][idx] = new_number
                    
        for dummy,boundary in self.boundary_dict.iteritems():
            for idx,boundary_node_number in enumerate(boundary[1]):
                if boundary_node_number == node_number:
                    boundary[1][idx] = new_number

    def plot(self):
        fig = plt.figure()
        axes = fig.add_subplot(111)
        axes.clear()
        axes.set_xticks([])
        axes.set_yticks([])
        axes.set_aspect(aspect='equal')
        axes.set_autoscale_on(True)

        for index,element in self.element_dict.iteritems():
            for i in range(0,4):
                point_1 = self.node_dict[element[2][i]]
                if ( i == 3 ):
                    point_2 = self.node_dict[element[2][0]]
                else:
                    point_2 = self.node_dict[element[2][i+1]]

                axes.plot( [ point_1[0], point_2[0] ] , \
                           [ point_1[1], point_2[1] ] , 'k-', lw=0.5 )
        fig.show()

    def __str__(self):
        output = ""
        
        for index, node in self.node_dict.iteritems():
            output += "Node "+str(index)+" "+str(node['coords'])+"\n"

        for index, edge in self.edge_dict.iteritems():
            output += "Edge "+str(index)+" "+str(edge['nodes'])+"\n"

        for index, face in self.face_dict.iteritems():
            output += "Face "+str(index)+" "+str(face['nodes'])+"\n"

        for index, element in self.element_dict.iteritems():
            output += "Element "+str(index)+" "+str(element['nodes'])+"\n"

        for index, boundary in self.boundary_dict.iteritems():
            output += "Boundary "+str(index)+" "+str(boundary['targets'])+" | mesh = "+str(boundary['mesh_nr'])+"\n"

        return( output )
            
    def addToModel( self, model ):
        raise Exception("`soofea.Model.mesh.addtoModel()` has to be rewritten - no working implementation yet")

    def getXMLContainer( self, tree, tagname ):
        container = tree.getroot().find( tagname )
        if( container != None):
            container.clear()
        else:
            container = ElementTree.SubElement(tree.getroot(), tagname)
        return( container )

    def insertMeshIntoXML( self , tree ):
        container = self.getXMLContainer( tree, soofea.Base.XMLBaseConstants.NODE_CONTAINER )
        for index, node in self.node_dict.iteritems():
            ElementTree.SubElement(container, soofea.Base.XMLBaseConstants.NODE,\
                                   attrib={soofea.Base.XMLBaseConstants.NUM:str(index),\
                                           soofea.Base.XMLBaseConstants.X_COORD:str(node['coords'][0]),\
                                           soofea.Base.XMLBaseConstants.Y_COORD:str(node['coords'][1]),\
                                           soofea.Base.XMLBaseConstants.Z_COORD:str(node['coords'][2])})

        if self.edge_dict:
            container = self.getXMLContainer( tree, soofea.Base.XMLBaseConstants.EDGE_CONTAINER )
            for index, edge in self.edge_dict.iteritems():
                ElementTree.SubElement(container, soofea.Base.XMLBaseConstants.EDGE,\
                                           attrib={soofea.Base.XMLBaseConstants.NUM:str(index),\
                                                       soofea.Base.XMLBaseConstants.GLOBAL_NODE_NUM:' '.join([str(i) for i in edge['nodes']]),\
                                                       soofea.Base.XMLBaseConstants.TYPE:'2' })

        if self.face_dict:
            container = self.getXMLContainer( tree, soofea.Base.XMLBaseConstants.FACE_CONTAINER )
            for index, face in self.face_dict.iteritems():
                ElementTree.SubElement(container, soofea.Base.XMLBaseConstants.FACE,\
                                       attrib={soofea.Base.XMLBaseConstants.NUM:str(index),\
                                               soofea.Base.XMLBaseConstants.GLOBAL_NODE_NUM:' '.join([str(i) for i in face['nodes']]),\
                                                   soofea.Base.XMLBaseConstants.TYPE:'3' })


        container = self.getXMLContainer( tree, soofea.Base.XMLBaseConstants.ELEMENT_CONTAINER )
        for index, element in self.element_dict.iteritems():
            ElementTree.SubElement(container, soofea.Base.XMLBaseConstants.ELEMENT,\
                                       attrib={soofea.Base.XMLBaseConstants.NUM:str(index),\
                                                   soofea.Base.XMLBaseConstants.GLOBAL_NODE_NUM:' '.join([str(i) for i in element['nodes']]),\
                                                   soofea.Base.XMLBaseConstants.TYPE:'1',\
                                                   soofea.Base.XMLBaseConstants.MATERIAL:'1'})

        container = self.getXMLContainer( tree, soofea.Base.XMLBaseConstants.BOUNDARY_CONTAINER )
        current_loop = 0
        for index, boundary in self.boundary_dict.iteritems():
            if current_loop != boundary['mesh_nr']:
                current_loop = boundary['mesh_nr']
                loop_container = ElementTree.SubElement(container, soofea.Base.XMLBaseConstants.LOOP,\
                                                        attrib={soofea.Base.XMLBaseConstants.NUM:str(current_loop)})                       
            boundary_elem = ElementTree.SubElement(loop_container,soofea.Base.XMLBaseConstants.BOUNDARY,\
                                              attrib={soofea.Base.XMLBaseConstants.NUM:str(index)})
            if( boundary['target_type'] == 'edge' ):
                boundary_elem.set(soofea.Base.XMLBaseConstants.GLOBAL_EDGE_NUM,' '.join([str(i) for i in boundary['targets']]))
            elif(boundary['target_type'] == 'face'):
                boundary_elem.set(soofea.Base.XMLBaseConstants.GLOBAL_FACE_NUM,' '.join([str(i) for i in boundary['targets']]))
                                           
class GMSHMesh( Mesh ):
    element_types = { 1:'2-node_line', \
                      3:'4-node_quadrangle', \
                      5:'8-node_hexahedron', \
                      15:'1-node_point' }
    
    def __init__( self, filename , dimension ):
        self.dimension_ = dimension
        Mesh.__init__(self)
        input_path = os.path.abspath( filename )
        if not os.path.exists( input_path ):
            raise BaseException( 'GMSH file you want to parse does not exist' )

        self.input_file_ = open(input_path,'r')

    def __del__( self ):
        self.input_file_.close()

    def elementContainerState( self, line_data , old_state ):
        state = None
        if( self.element_types[ int(line_data[1]) ] == '1-node_point' ):
            state = 'points'           
        elif( self.element_types[ int(line_data[1]) ] == '2-node_line' ):
            state = 'edges'           
        elif( self.element_types[ int(line_data[1]) ] == '4-node_quadrangle' ):
            if self.dimension_ == 2:
                state = 'elements'
            elif self.dimension_ == 3:
                state = 'faces'
        elif( self.element_types[ int(line_data[1]) ] == '8-node_hexahedron' ):
            state = 'elements'           
        elif not state:
            raise BaseException('We do not know element type : '+line_data[1])

        if( state != old_state ):
            print "We are on new state "+state
        return( state )

    def findMeshNr( self, id_nr ):
        count = 1
        for mesh_name, ref_nrs in self.meshes.iteritems():
            if id_nr in ref_nrs:
                return( count )
            count += 1

    def read( self ):
        state = 'foo'
        self.act_edge_nr = 1
        self.act_face_nr = 1
        self.act_element_nr = 1
        self.meshes = {}
        self.boundary_ids = []

        for line in self.input_file_:
            if( state == 'foo'):
                if( line.rstrip(' \n') == '$PhysicalNames'):
                    state = 'physical'
                    print "Reading physical names..."
                    continue

                if( line.rstrip(' \n') == '$Nodes'):
                    state = 'nodes'
                    print "Reading nodes..."
                    continue

                if( line.rstrip(' \n') == '$Elements'):
                    state = 'elements_container'
                    print "-> Reading element container..."
                    continue

            # Physical groups------------------------------------------------------------
            if( state == 'physical' ):
                if( line.rstrip(' \n') == '$EndPhysicalNames' ):
                    state = 'foo'
                    continue

                line_data = line.split()
                if len(line_data) == 1:
                    continue

                mesh_name = line_data[2].strip('" ')
                if mesh_name in self.meshes:
                    self.meshes[mesh_name].append( int(line_data[1]) )
                else:
                    self.meshes[mesh_name] = [int(line_data[1])]

            # Nodes ------------------------------------------------------------------
            if( state == 'nodes' ):
                if( line.rstrip(' \n') == '$EndNodes' ):
                    state = 'foo'
                    continue
               
                node_data = line.split()
                if len(node_data) == 1:
                    # We are on first line
                    self.node_amount = node_data[0]
                    continue

                self.addNode( int(node_data[0]),float(node_data[1]),float(node_data[2]),float(node_data[3]) )

            # Element Container------------------------------------------------------------
            if( state == 'elements_container' or state == 'elements' or state == 'faces' or state == 'edges' or state == 'points'):
                if( line.rstrip(' \n') == '$EndElements' ):
                    print "-> Leaving Element Container"
                    state = 'foo'
                    continue

                line_data = line.split()

                if len( line_data ) == 1:
                    # We are hopefully on first line
                    self.element_amount = line_data[0]
                    continue

                state = self.elementContainerState( line_data , state )

                # Points----------------------------------------------------------------
                if( state == 'points' ):
                    pass

                # Edges------------------------------------------------------------
                if( state == 'edges' ):
                    nr_of_tags = int(line_data[2])
                    if( nr_of_tags < 2 ):                       
                        raise "BaseException('We need the id of the line were the edge lies on')"

                    boundary_id = line_data[4]
                    if not boundary_id in self.boundary_ids:
                        self.boundary_ids.append( boundary_id )
                    self.findMeshNr( int(line_data[0]) )
                    self.appendTargetToBoundary( self.boundary_ids.index(boundary_id)+1, self.act_edge_nr , 'edge' , self.findMeshNr( int(line_data[3]) ) )
                    self.addEdge( self.act_edge_nr , [int(node_num) for node_num in line_data[(3+nr_of_tags):]] , int(line_data[0]) )
                    self.act_edge_nr += 1
                    continue

                # Faces-----------------------------------------------------------------
                if( state == 'faces'):
                    nr_of_tags = int(line_data[2])
                    if( nr_of_tags < 2 ):                       
                        raise "BaseException('We need the id of the surface were the face lies on')"

                    boundary_id = line_data[4]
                    if not boundary_id in self.boundary_ids:
                        self.boundary_ids.append( boundary_id )
                    
                    self.appendTargetToBoundary( self.boundary_ids.index(boundary_id)+1, self.act_face_nr , 'face', self.findMeshNr( int(line_data[3]) ) )
                    self.addFace( self.act_face_nr , [int(node_num) for node_num in line_data[(3+nr_of_tags):]] , int(line_data[0]) )
                    self.act_face_nr += 1
                    continue

                # Elements--------------------------------------------------------------
                if( state == 'elements' ):
                    nr_of_tags = int(line_data[2])

                    self.addElement( self.act_element_nr, \
                                     [int(node_num) for node_num in line_data[(3+nr_of_tags):] ] , \
                                     int(line_data[0]) ) # ATM all Elements get the same ElementTypeNumber
                    self.act_element_nr = self.act_element_nr + 1
