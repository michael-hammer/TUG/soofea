""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2007-2010 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

import threading, time, os
# import gamin

#------------------------------------------------------------------
class LogWorker(threading.Thread) :
    SLEEP_INTERVALL = 0.1

    #------------------------------------------------------------------
    def __init__(self, append_hook, path):
        threading.Thread.__init__(self)
        self.stop = False
#        self.watch_mon = gamin.WatchMonitor()
        self.appender = append_hook
        self.path = os.path.abspath( path )
        self.act_position = 0
        self.stop_it = False
        self.lock = threading.Lock()

    #------------------------------------------------------------------
    def getText( self ):
        self.fd = open( self.path , 'r' )
        self.fd.seek( self.act_position , os.SEEK_SET )
        text = self.fd.read()
        self.act_position = self.fd.tell()
        self.fd.close()
        return(text)

    #------------------------------------------------------------------
    def callback( self, path, event ):
  #      if( event == gamin.GAMChanged ):
  #          self.appender( self.getText() )
        pass

    #------------------------------------------------------------------
    def run( self ):
        if( not os.path.isfile( self.path ) ):
            open( self.path, 'w' ).close()

        self.appender( self.getText() )
#        self.watch_mon.watch_file( self.path , self.callback )
#        while( not self.stop_it ):
#            time.sleep(self.SLEEP_INTERVALL)
#            if self.watch_mon.event_pending() > 0 :
#                self.watch_mon.handle_one_event()
#
#        self.watch_mon.stop_watch( self.path )

    #------------------------------------------------------------------
    def halt( self ):
        self.lock.acquire()
        self.stop_it = True
        self.lock.release()

#------------------------------------------------------------------
class LogViewer:
    #------------------------------------------------------------------
    def __init__(self):
        self.worker = 0

    #------------------------------------------------------------------
    def start( self, append_hook, path ):
        self.worker = LogWorker( append_hook, path )
        self.worker.start()

    #------------------------------------------------------------------
    def stop( self ):
        if self.worker :
            self.worker.halt()
        self.worker = None
