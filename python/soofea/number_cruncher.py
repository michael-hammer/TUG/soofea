""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2007-2010 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

import threading, os, sys
from subprocess import Popen

#------------------------------------------------------------------
class NumberCruncher(threading.Thread):
    #------------------------------------------------------------------
    def __init__( self, finish_hook, run_hook, filename ) :
        threading.Thread.__init__(self)
        self.filename = filename
        self.finished = finish_hook
        self.running = run_hook
        
    #------------------------------------------------------------------
    def run( self ) :
        proc = Popen( os.path.abspath(sys.path[0])+"/soofea -f "+ str(self.filename), shell=True )
        self.running( proc.pid )
        proc.wait()
        self.finished()
