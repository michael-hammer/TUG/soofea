""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2007-2010 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

import sys, os, sip, time
from argparse import ArgumentParser
from PyQt4 import QtCore, QtGui
from IPython import embed

from main_window_ui import Ui_main_window
import icons_rc

from main_tab import MainTab
from start_widget import StartWidget
from about_dialog import AboutDialog
from version import VersionDialog

import SOOFEA
from SOOFEA import *
from SOOFEA.Base import Configuration, IDObject
from SOOFEA.exceptions import IOException
from SOOFEA.event_handler import EventHandler

#------------------------------------------------------------------
class QSOOFEA(QtGui.QMainWindow):
    TITLE = "SOOFEA - Software for Finite Element Analysis"

    #------------------------------------------------------------------
    def __init__(self,filename=None,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.event_handler = EventHandler( self.finishedCalculation )
        self.ui = Ui_main_window()
        self.ui.setupUi(self)
        QtCore.QObject.connect(self.ui.action_open,QtCore.SIGNAL("triggered()"), self.openDialog)
        QtCore.QObject.connect(self.ui.action_save,QtCore.SIGNAL("triggered()"), self.saveFile)
        QtCore.QObject.connect(self.ui.action_save_as,QtCore.SIGNAL("triggered()"), self.saveAsFile)
        QtCore.QObject.connect(self.ui.action_about,QtCore.SIGNAL("triggered()"), self.aboutDialog)
        QtCore.QObject.connect(self.ui.action_version,QtCore.SIGNAL("triggered()"), self.versionDialog)
        QtCore.QObject.connect(self.ui.start_button,QtCore.SIGNAL("clicked()"), self.startCalculation)
        QtCore.QObject.connect(self.ui.stop_button,QtCore.SIGNAL("clicked()"), self.stopCalculation)
        QtCore.QObject.connect(self.ui.quit_button,QtCore.SIGNAL("clicked()"), self.quit)
        self.content = StartWidget()
        self.setModifiedMarker( False )
        if( filename ):
            self.openFile(filename)
        self.setContent()
        
    #------------------------------------------------------------------
    def setModifiedMarker( self, modified ):
        if modified:
            self.setWindowTitle(QtGui.QApplication.translate("main_window", "** " + self.TITLE + " **", None, QtGui.QApplication.UnicodeUTF8))
            self.ui.action_save.setEnabled(True)
            self.ui.action_save_as.setEnabled(True)
        else:
            self.setWindowTitle(QtGui.QApplication.translate("main_window", self.TITLE , None, QtGui.QApplication.UnicodeUTF8))        
            self.ui.action_save.setEnabled(False)
            self.ui.action_save_as.setEnabled(False)

    #------------------------------------------------------------------
    def openFile(self, filename):
        sip.delete(self.content)
        try :
            self.content = MainTab( self, filename )
            self.ui.start_button.setEnabled(True)
            self.filename = filename
        except IOException, exc:
            self.content = StartWidget()
            self.content.ui.textBrowser.setText(str(exc))
            
        self.setContent()
        
    #------------------------------------------------------------------
    def setContent(self):
        sip.delete(self.ui.main_widget.layout())
        gridlayout = QtGui.QGridLayout(self.ui.main_widget)
        gridlayout.addWidget( self.content )
        gridlayout.setContentsMargins(0,0,0,0)
        self.ui.main_widget.setLayout(gridlayout)

    #------------------------------------------------------------------
    def saveFile(self):
        if not self.filename:
            self.saveAsFile()
        else:
            self.content.config.write()
            self.setModifiedMarker( False )

    #------------------------------------------------------------------
    def saveAsFile(self):
        self.filename = str(QtGui.QFileDialog.getSaveFileName(self, "Save Calculation As", os.getcwd(), "XML file (*.xml)"))
        self.content.setConfigurationPath( self.filename )
        self.content.config.write()
        self.setModifiedMarker( False )

    #------------------------------------------------------------------
    def openDialog(self):
        path = str(QtGui.QFileDialog.getOpenFileName(self, "Open Calculation File", os.getcwd(), "XML file (*.xml)"))
        if os.path.isfile( path ) :
            self.openFile( path )
            self.setModifiedMarker( False )
        
    #------------------------------------------------------------------
    def aboutDialog(self):
        self.about_dialog = AboutDialog()
        self.about_dialog.show()

    #------------------------------------------------------------------
    def versionDialog(self):
        self.version_dialog = VersionDialog()
        self.version_dialog.show()

    #------------------------------------------------------------------
    def startCalculation(self):
        if( self.content.modified ):
            if not self.filename:
                self.saveAsFile()
            else:
                self.saveFile()

        self.ui.stop_button.setEnabled(True)
        self.ui.start_button.setEnabled(False)

        self.event_handler.startCalc( self.filename )

    #------------------------------------------------------------------
    def stopCalculation(self):
        self.event_handler.stopCalc()

    #------------------------------------------------------------------
    def finishedCalculation(self):
        self.ui.stop_button.setEnabled(False)
        self.ui.start_button.setEnabled(True)

    #------------------------------------------------------------------
    def quit(self):
        QtGui.QMainWindow.close(self)

    #------------------------------------------------------------------
    def modified(self):
        self.setModifiedMarker( True )

#------------------------------------------------------------------
def main():
    parser = ArgumentParser(description="SOOFEA - Software for Object Oriented Finite Element Analysis")

    parser.add_argument('-v','--version', action='version', version="%(prog)s version <"+str(SOOFEA.version())+">")
    parser.add_argument('-nogui', action="store_true", default=False,
                        help="Should a graphical session be started")
    parser.add_argument('file', help="Open FILE as SOOFEA configuration xml file", metavar="FILE")
                      
    args = parser.parse_args()

    if not args.nogui:
        app = QtGui.QApplication(sys.argv)
        myapp = QSOOFEA(filename=args.file)
        myapp.show()
        return( app.exec_() )
    else:
        solver = Solver( args.file )
        model_handler = solver.createModelHandler()
        model_handler.read()
        model_handler.readResults()
        embed()
