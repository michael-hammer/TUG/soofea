""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2007-2010 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

import sip, os, threading
from PyQt4 import QtCore, QtGui

from main_tab_ui import Ui_main_tab

from SOOFEA import *
from SOOFEA.Base import Configuration, IDObject
from SOOFEA.GUI.Visualization.canvas import QtCanvas

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

#------------------------------------------------------------------
class MainTab(QtGui.QWidget):
    tab_dict = {'Configuration':0,'Visualization':1}
    min_deformation_factor_ = 0.
    max_deformation_factor_ = 10.
    
    #------------------------------------------------------------------
    def __init__(self,parent,file_path):
        QtGui.QWidget.__init__(self,parent)
        self.modified = False
        self.ui = Ui_main_tab()
        self.ui.setupUi(self)

        layout = QtGui.QGridLayout(self.ui.canvas_widget)
        self.canvas = QtCanvas(self.ui.canvas_widget,self)
        # self.canvas = QtCanvas(self.ui.canvas_widget,self)
        layout.addWidget(self.canvas,0,0,1,1)

        self.file_path_ = file_path
        self.solver_ = Solver()
        self.model_handler_ = self.solver_.createModelHandler( self.file_path_ , IDObject.ID.XML, True )
        self.model_handler_.read()
        self.model_handler_.readResults()
    
        self.initiateCombos()
        self.setConfiguration()

        QtCore.QObject.connect(self.ui.input_file_button,QtCore.SIGNAL("clicked()"),self.openInputFile)
        QtCore.QObject.connect(self.ui.output_file_button,QtCore.SIGNAL("clicked()"),self.openOutputFile)
        QtCore.QObject.connect(self.ui.num_int_file_button,QtCore.SIGNAL("clicked()"),self.openNumIntFile)
        QtCore.QObject.connect(self.ui.logfile_button,QtCore.SIGNAL("clicked()"),self.openLogfile)
        QtCore.QObject.connect(self.ui.eps_output_save,QtCore.SIGNAL("clicked()"),self.saveEps)
        QtCore.QObject.connect(self.ui.eps_output_dir_select,QtCore.SIGNAL("clicked()"),self.setEpsOutputDir)

        QtCore.QObject.connect(self.ui.input_file_edit,QtCore.SIGNAL("editingFinished()"),self.setInput)
        QtCore.QObject.connect(self.ui.output_file_edit,QtCore.SIGNAL("editingFinished()"),self.setOutput)
        QtCore.QObject.connect(self.ui.num_int_file_edit,QtCore.SIGNAL("editingFinished()"),self.editNumIntFile)
        QtCore.QObject.connect(self.ui.logfile_edit,QtCore.SIGNAL("editingFinished()"),self.editLogfile)

        QtCore.QObject.connect(self.ui.output_write_mode,QtCore.SIGNAL("currentIndexChanged(int)"),self.setWriteMode)
        QtCore.QObject.connect(self.ui.logger_priority,QtCore.SIGNAL("currentIndexChanged(int)"),self.setLoggerPriority)

        QtCore.QObject.connect(self.ui.deformation_slider,QtCore.SIGNAL("valueChanged(int)"),self.sigDeformation)
        QtCore.QObject.connect(self.ui.deformation,QtCore.SIGNAL("editingFinished()"),self.sigDeformation)
        QtCore.QObject.connect(self.ui.time_stamp_slider,QtCore.SIGNAL("valueChanged(int)"),self.sigTimeStamp)
        QtCore.QObject.connect(self.ui.time_stamp,QtCore.SIGNAL("editingFinished()"),self.sigTimeStamp)

        QtCore.QObject.connect(self,QtCore.SIGNAL("sigModified"),self.parent().modified)

        self.ui.eps_output_dir.setText( self.extractBasePath("") )
        self.ui.tabWidget.setCurrentIndex(1)
        self.setDeformation( 100./(self.max_deformation_factor_-self.min_deformation_factor_) * 1. )
        self.setTimeStamp( 0 )
        self.canvas.plot(self.model_handler_.getModel())

    #------------------------------------------------------------------
    def __del__(self):
        del self.solver_;

    #------------------------------------------------------------------
    def initiateCombos(self):
        self.ui.input_file_type.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.GID )))
        self.ui.input_file_type.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.XML )))

        self.ui.output_file_type.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.GID )))
        self.ui.output_file_type.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.XML )))

        self.ui.output_write_mode.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.NO_WRITE )))
        self.ui.output_write_mode.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.FINAL_WRITE )))
        self.ui.output_write_mode.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.CONTACT_SET_WRITE )))
        self.ui.output_write_mode.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.TIME_STEP_WRITE )))        
        self.ui.output_write_mode.addItem(_fromUtf8( IDObject.resolveID( IDObject.ID.ITERATION_WRITE )))
    
    #------------------------------------------------------------------
    def setConfiguration(self):
        input_path = os.path.abspath( Configuration.getInstance().getIOInputFN() )
        input_type_str = IDObject.resolveID( Configuration.getInstance().getIOInputType() )
        output_path = os.path.abspath( Configuration.getInstance().getIOOutputFN() )
        output_type_str = IDObject.resolveID( Configuration.getInstance().getIOOutputType() )
        self.ui.input_file_edit.setText(input_path)      
        self.ui.input_file_type.setCurrentIndex( self.ui.input_file_type.findText( QtCore.QString( input_type_str ) , QtCore.Qt.MatchExactly ) )
        self.ui.output_file_edit.setText(output_path)
        self.ui.output_file_type.setCurrentIndex( self.ui.output_file_type.findText( QtCore.QString( output_type_str ) , QtCore.Qt.MatchExactly ) )
        write_str = IDObject.resolveID( Configuration.getInstance().getWrite() )
        self.ui.output_write_mode.setCurrentIndex( self.ui.output_file_type.findText( QtCore.QString( output_type_str ) , QtCore.Qt.MatchExactly ) )
        
        num_int_path = os.path.abspath( Configuration.getInstance().getMathNumIntConfigFN() );
        self.ui.num_int_file_edit.setText(num_int_path)

        # fixme: logger priority!!
        log_path = os.path.abspath( Configuration.getInstance().getLogFN() );
        self.ui.logfile_edit.setText( log_path )
       

    #------------------------------------------------------------------
    def fileDialog( self, caption, directory, filter ):
        return str( QtGui.QFileDialog.getOpenFileName( self , caption, directory, filter ) )

    #------------------------------------------------------------------
    def extractBasePath( self , file_path ):
        base_path = ''
        if file_path :
            base_path = os.path.dirname(os.path.abspath( file_path ))
        if not os.path.isdir( base_path ):
            base_path = os.path.dirname(os.path.abspath( self.file_path_ ))
        return base_path
        
    #------------------------------------------------------------------
    def openInputFile( self ):
        base_path = self.extractBasePath( str(self.ui.input_file_edit.text()) )
        path = os.path.abspath( self.fileDialog( "Input file", base_path , "XML file (*.xml);;All files (*)" ) )
        if( path != self.ui.input_file_edit.text() ):
            self.sigModified()
            self.ui.input_file_edit.setText( path )
            # self.config_.setInput( filename = path )

    #------------------------------------------------------------------
    def setInput( self ):
        if( os.path.abspath( str(self.ui.input_file_edit.text()) ) != \
            os.path.abspath( Configuration.getInstance().getIOInputFN() ) ) :
            self.sigModified()
            # self.config_.setInput( str(self.ui.input_file_edit.text()) , Configuration.input_type_default__ )

    #------------------------------------------------------------------
    def openOutputFile( self ):
        base_path = self.extractBasePath( str(self.ui.output_file_edit.text()) )
        path = os.path.abspath( self.fileDialog( "Output file", base_path , "XML file (*.xml);;All files (*)" ) )
        if( path != self.ui.output_file_edit.text() ):
            self.sigModified()
            self.ui.output_file_edit.setText( path )
            # self.config_.setOutput( filename = path )

    #------------------------------------------------------------------
    def setOutput( self ):
        if( os.path.abspath( str(self.ui.output_file_edit.text()) ) != \
            os.path.abspath( Configuration.getInstance().getIOOutputFN() ) ) :
            self.sigModified()
            # self.config_.setOutput( str(self.ui.output_file_edit.text()) , Configuration.output_type_default__ ,
            #                         str(self.ui.write_output_check.checkState() / 2) )

    #------------------------------------------------------------------
    def setWriteMode( self , index ):
        write_mode_str = IDObject.resolveID( Configuration.getInstance().getWrite() )
        if( index != self.ui.output_write_mode.findText( QtCore.QString( write_mode_str ) , QtCore.Qt.MatchExactly ) ):
            self.sigModified()
            # self.config_.set....

    #------------------------------------------------------------------
    def openNumIntFile( self ):
        base_path = self.extractBasePath( str(self.ui.num_int_file_edit.text()) )
        path = os.path.abspath( self.fileDialog( "Numerical integration weights", base_path , "XML file (*.xml);;All files (*)" ) )
        if( path != self.ui.num_int_file_edit.text() ):
            self.sigModified()
            self.ui.num_int_file_edit.setText( path )
            # self.config_.setNumInt( path )

    #------------------------------------------------------------------
    def editNumIntFile( self ):
        if( os.path.abspath( str(self.ui.num_int_file_edit.text()) ) != \
            os.path.abspath( Configuration.getInstance().getMathNumIntConfigFN() ) ) :
            self.sigModified()
            # self.config_.setNumInt( str(self.ui.num_int_file_edit.text()) )

    #------------------------------------------------------------------
    def openLogfile( self ):
        base_path = self.extractBasePath( str(self.ui.logfile_edit.text()) )
        path = os.path.abspath( self.fileDialog( "Logfile", base_path , 'Logfiles (*.log);;All files (*)' ) )
        if( path != self.ui.logfile_edit.text() ):
            self.sigModified()
            self.ui.logfile_edit.setText( path )
            # self.config_.setLogging( filename = path , 
            #                          priority = str(self.ui.logger_priority.currentText()) )

    #------------------------------------------------------------------
    def editLogfile( self ):
        if( os.path.abspath( str(self.ui.logfile_edit.text()) ) != \
            os.path.abspath( Configuration.getInstance().getLogFN() ) ) :
            self.sigModified()
            # self.config_.setLogging( filename = str(self.ui.logfile_edit.text()) , 
            #                          priority = str(self.ui.logger_priority.currentText()) )

    #------------------------------------------------------------------
    def setLoggerPriority( self , index ):
        logger_priority_str = resolveLevel( Configuration.getInstance().getLoggerPriorityLevel() )
        if( index != self.ui.logger_priority.findText( QtCore.QString( logger_priority_str ) , QtCore.Qt.MatchExactly ) ):
            self.sigModified()
            # self.config_.setLogging( filename = str(self.ui.logfile_edit.text()) , 
            #                          priority = str(self.ui.logger_priority.currentText()) )

    #------------------------------------------------------------------
    def setDeformation( self, deformation_factor_percentage=-1 ):
        if( deformation_factor_percentage >= 0 ):
            self.canvas.deformation_factor = self.min_deformation_factor_ + \
                deformation_factor_percentage*(self.max_deformation_factor_-self.min_deformation_factor_)/100.
        else:
            self.canvas.deformation_factor = float(self.ui.deformation.text())

        self.ui.deformation.setText( str( self.canvas.deformation_factor ) )
        self.ui.deformation_slider.setSliderPosition( self.canvas.deformation_factor/(self.max_deformation_factor_-self.min_deformation_factor_)*100.  )
        
    #------------------------------------------------------------------
    def sigDeformation( self, deformation_factor_percentage=-1 ):
        self.setDeformation( deformation_factor_percentage )
        self.canvas.plot(self.model_handler_.getModel())

    #------------------------------------------------------------------
    def setTimeStamp( self, time_stamp_percentage=-1 ):
        last_index = self.model_handler_.getModel().getTimeBar().getLastFinishedTimeStamp().index()
        if( time_stamp_percentage >= 0 ):
            self.canvas.time_stamp_index = time_stamp_percentage*last_index/100
        else:
            self.canvas.time_stamp_index = int( self.ui.time_stamp.text() )

        if self.canvas.time_stamp_index != 0:
            self.ui.time_stamp_slider.setSliderPosition( float(self.canvas.time_stamp_index)/last_index * 100 )
        else:
            self.ui.time_stamp_slider.setSliderPosition( 0.0 )
        self.ui.time_stamp.setText( str( self.canvas.time_stamp_index ) )

    #------------------------------------------------------------------
    def sigTimeStamp( self, time_stamp_percentage=-1 ):
        self.setTimeStamp( time_stamp_percentage )
        self.canvas.plot(self.model_handler_.getModel())

    #------------------------------------------------------------------
    def sigModified(self):
        self.modified = True
        self.emit(QtCore.SIGNAL("sigModified"))

    #------------------------------------------------------------------
    def saveEps(self):
        (problem_name, ext) = os.path.splitext( os.path.basename(self.file_path_) )
        eps_path = os.path.join( str(self.ui.eps_output_dir.text()), problem_name + "_" + str(self.canvas.time_stamp_index) + ".eps" )
        self.canvas.plotToFile(self.model_handler_.getModel(),eps_path)

    #------------------------------------------------------------------
    def setEpsOutputDir(self):
        base_path = self.extractBasePath( str(self.ui.eps_output_dir.text()) )
        directory = os.path.abspath( str( QtGui.QFileDialog.getExistingDirectory( self , "EPS output directory", base_path , options=QtGui.QFileDialog.ShowDirsOnly ) ) )
        self.ui.eps_output_dir.setText( directory )
