import os,sys
from PyQt4 import QtCore, QtGui
from about_dialog_ui import Ui_about_dialog

class AboutDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.ui = Ui_about_dialog()
        self.ui.setupUi(self)
        self.ui.text_browser.setSource(QtCore.QUrl(os.path.abspath(sys.path[0] + '/../etc/soofea/info.html')))
