""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2007-2010 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

from PyQt4 import QtCore, QtGui
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.figure import SubplotParams
import matplotlib.pyplot as plt

import SOOFEA.Model

#------------------------------------------------------------------
class Canvas(FigureCanvas):
    def __init__(self):
        self.fig_ = Figure( facecolor='white', subplotpars=SubplotParams(left=0.05,bottom=0.05,right=0.95,top=0.95) )
        self.axes_ = self.fig_.add_subplot(111)
        FigureCanvas.__init__(self, self.fig_)
        self.deformation_factor = 1.
        self.time_stamp_index = 0

    def matplotlibPlot(self,model,axes):
        axes.clear()
        axes.set_xticks([])
        axes.set_yticks([])
        axes.set_aspect(aspect='equal')
        axes.set_autoscale_on(True)

        time_stamp = model.getTimeBar().getTimeStamp( self.time_stamp_index )
        for element in model.getElementIterator():
            element.plot( axes , time_stamp , self.deformation_factor )
#        for edge in model.getEdgeIterator():
#            edge.plot( axes , time_stamp , self.deformation_factor )
#        for node in model.getNodeIterator():
#            node.plot( axes , time_stamp , self.deformation_factor )

        for [loc, spine] in axes.spines.iteritems():
            spine.set_visible(False)
    
    def plotToFile(self,model,filename):
        fig = plt.figure( figsize=(6,3.78), dpi=72.27, subplotpars=SubplotParams(left=0,bottom=0,right=1,top=1) )
        self.matplotlibPlot( model, fig.add_subplot(111) )
        fig.savefig( filename, format='EPS' , bbox_inches='tight' , pad_inches = 0. )

    def plot(self,model):
        self.matplotlibPlot( model, self.axes_ )
        self.fig_.canvas.draw()
    
#------------------------------------------------------------------
class QtCanvas(Canvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent, main_tab):
        Canvas.__init__(self)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
