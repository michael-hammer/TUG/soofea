""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2010  Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

from SOOFEA.Model.mesh import Mesh
from SOOFEA.Model import Node
from SOOFEA.Model import Element
# from soofea.model.boundary import LineBoundary, ArcBoundary

import math

#------------------------------------------------------------------
class Rectangle:
    # ATM we can only mesh quadrilaterals

    def __init__(self, base_coord, length, height, length_splits, height_splits ):
        self.base_coord_ = [float(i) for i in base_coord]
        self.length_ = float(length)
        self.height_ = float(height)
        self.length_splits_ = length_splits
        self.height_splits_ = height_splits
            
    def appendNodes( self, mesh ):
        self.node_offset_ = mesh.getNodeAmount() # we have to start with new node numbers        
        for length_iter in range(self.length_splits_+1):
            for height_iter in range(self.height_splits_+1):
                x = self.base_coord_[0] + self.length_/self.length_splits_*length_iter
                y = self.base_coord_[1] + self.height_/self.height_splits_*height_iter
                node_number = height_iter*(self.length_splits_+1) + (length_iter+1) + self.node_offset_
                mesh.addNode( node_number , x , y , 0 , 0  )

    def appendElements( self, mesh , material_number = 1 , element_type_number = 1 ):
        element_offset = mesh.getElementAmount() # we have to start with new element numbers
        for height_iter in range(self.height_splits_):
            for length_iter in range(self.length_splits_):
                element_number = height_iter*self.length_splits_ + length_iter+1 + element_offset
                node_numbers = [ height_iter*(self.length_splits_+1) + length_iter+1 + self.node_offset_ , \
                                     height_iter*(self.length_splits_+1) + length_iter+2 + self.node_offset_ , \
                                     (height_iter+1)*(self.length_splits_+1) + length_iter+2 + self.node_offset_ , \
                                     (height_iter+1)*(self.length_splits_+1) + length_iter+1 + self.node_offset_ ]
                mesh.addElement( element_number, material_number, node_numbers, element_type_number )

    def getBoundaryNodes( self, id_ ):
        if( id_ == 'bottom' ):
            return [i+self.node_offset_ for i in range(1,self.length_splits_+2)]
        elif( id_ == 'right' ):
            return [i*(self.length_splits_+1)+self.node_offset_ for i in range(1,self.height_splits_+2) ]
        elif( id_ == 'top' ):
            return [i+self.node_offset_ for i in range( (self.length_splits_+1)*(self.height_splits_+1) , (self.length_splits_+1)*(self.height_splits_) , -1)]
        elif( id_ == 'left' ):
            return [i*(self.length_splits_+1)+1+self.node_offset_ for i in range(self.height_splits_,-1,-1) ]
        else:
            raise BaseException('Not Implemented!')

    def appendBoundaries( self, mesh ):
        boundary_offset = mesh.getBoundaryAmount()
        node_numbers = self.getBoundaryNodes('bottom')
        mesh.addBoundary( 'line', boundary_offset+1 , node_numbers )
        node_numbers = self.getBoundaryNodes('right')
        mesh.addBoundary( 'line', boundary_offset+2 , node_numbers )
        node_numbers = self.getBoundaryNodes('top')
        mesh.addBoundary( 'line', boundary_offset+3 , node_numbers )       
        node_numbers = self.getBoundaryNodes('left')
        mesh.addBoundary( 'line', boundary_offset+4 , node_numbers )

#------------------------------------------------------------------
class RingSegment:
    def __init__(self, center, r_i, r_a, alpha_1, alpha_2 , alpha_splits , r_splits ):
        self.center_ = center
        self.r_i_ = r_i
        self.r_a_ = r_a
        self.alpha_1_ = alpha_1
        self.alpha_2_ = alpha_2
        self.alpha_splits_ = alpha_splits
        self.r_splits_ = r_splits

    def appendNodes( self, mesh ):
        pass
# FIXME    
#        self.node_offset_ = mesh.getNodeAmount() # we have to start with new node numbers        
#        for alpha_iter in range(self.alpha_splits_+1):
#            for r_iter in range(self.r_splits_+1):
#                angle = (self.alpha_1_ + alpha_iter*(self.alpha_2_ - self.alpha_1_)/self.alpha_splits_) * math.pi/180
#                radius = (self.r_a_ + r_iter*float(self.r_i_ - self.r_a_)/self.r_splits_)
#                x = self.center_[0] + radius * math.cos( angle )
#                y = self.center_[1] + radius * math.sin( angle )
#                node_number = r_iter*(self.alpha_splits_+1) + (alpha_iter+1) + self.node_offset_
#                mesh.addNode( Node( node_number , [x , y] ) )

    def appendElements( self, mesh , material_number = 1 , element_type_number = 1 ):
        pass
# FIXME
#        element_offset = mesh.getElementAmount() # we have to start with new element numbers
#        for alpha_iter in range(self.alpha_splits_):
#            for r_iter in range(self.r_splits_):
#                element_number = alpha_iter*self.r_splits_+(r_iter+1)  + element_offset
#                node_numbers = [ r_iter*(self.alpha_splits_+1) + alpha_iter+1 + self.node_offset_ , \
#                                     r_iter*(self.alpha_splits_+1) + alpha_iter+2 + self.node_offset_ , \
#                                     (r_iter+1)*(self.alpha_splits_+1) + alpha_iter+2 + self.node_offset_ , \
#                                     (r_iter+1)*(self.alpha_splits_+1) + alpha_iter+1 + self.node_offset_ ]
#                mesh.addElement( Element( element_number, node_numbers, material_number , element_type_number ) )
                
    def appendBoundaries( self, mesh ):
        pass
# FIXME    
#        boundary_offset = mesh.getBoundaryAmount()
#        node_numbers = [i+self.node_offset_ for i in range(1,self.alpha_splits_+2)]
#        mesh.addBoundary( ArcBoundary( boundary_offset+1 , node_numbers ) )
#        node_numbers = [i*(self.alpha_splits_+1)+self.node_offset_ for i in range(1,self.r_splits_+2) ]
#        mesh.addBoundary( LineBoundary( boundary_offset+2 , node_numbers ) )
#        node_numbers = [i+self.node_offset_ for i in range( (self.alpha_splits_+1)*(self.r_splits_+1) , (self.alpha_splits_+1)*(self.r_splits_) , -1 )]
#        mesh.addBoundary( ArcBoundary( boundary_offset+3 , node_numbers ) )       
#        node_numbers = [i*(self.alpha_splits_+1)+1+self.node_offset_ for i in range(self.r_splits_,-1,-1) ]
#        mesh.addBoundary( LineBoundary( boundary_offset+4 , node_numbers ) ) 

#------------------------------------------------------------------
class QuarterCircle:
    def __init__(self, type):
        self.coords = []
        self.elements = []
        self.boundaries = []

        if( type == '4x4' ):
            self.coords.append([10.000000000,0.0000000000])
            self.coords.append([10.000000000,2.5000000000])
            self.coords.append([6.1731648445,0.7612051964])
            self.coords.append([7.1108070699,3.4361484923])
            self.coords.append([10.000000000,5.0000000000])
            self.coords.append([7.7128895832,5.5839380613])
            self.coords.append([4.8620462257,4.9300250766])
            self.coords.append([6.0293290936,6.0892012367])
            self.coords.append([10.000000000,7.5000000000])
            self.coords.append([2.9289336205,2.9289312363])
            self.coords.append([7.7114307438,7.7549046187])
            self.coords.append([5.5130514718,7.7536405721])
            self.coords.append([3.3791236935,7.2103214436])
            self.coords.append([10.000000000,10.000000000])
            self.coords.append([7.5000000000,10.000000000])
            self.coords.append([0.7612047195,6.1731662750])
            self.coords.append([5.0000000000,10.000000000])
            self.coords.append([2.5000000000,10.000000000])
            self.coords.append([0.0000000000,10.000000000])

            self.elements.append([9,14,15,11])
            self.elements.append([18,19,16,13])
            self.elements.append([5,9,11,6])
            self.elements.append([17,18,13,12])
            self.elements.append([15,17,12,11])
            self.elements.append([2,5,6,4])
            self.elements.append([1,2,4,3])
            self.elements.append([10,3,4,7])
            self.elements.append([16,10,7,13])
            self.elements.append([6,11,12,8])
            self.elements.append([4,6,8,7])
            self.elements.append([12,13,7,8])

            self.boundaries.append( ('arc',[19,16,10,3,1]) )
            self.boundaries.append( ('line',[1,2,5,9,14]) )
            self.boundaries.append( ('line',[14,15,17,18,19]) )

    def move( self, move ):
        for node_coord in self.coords:
            node_coord[0] = node_coord[0] + move[0]
            node_coord[1] = node_coord[1] + move[1]

    def mirror( self, mirror_coord, coord ):
        for node_coord in self.coords:
            if( mirror_coord == 'x' ):
                node_coord[1] = coord - (node_coord[1] - coord)
            if( mirror_coord == 'y'):
                node_coord[0] = coord - (node_coord[0] - coord)
        for element in self.elements: # after mirror the sense of rotation has to be fixed!
            element.reverse()
        for boundary in self.boundaries:
            boundary[1].reverse()

    def getBoundaryNodes( self, id_ ):
        if( id_ == 'arc' ):
            return [ node_num + self.node_offset_ for node_num in self.boundaries[0][1] ]
        elif( id_ == 'vertical' ):
            return [ node_num + self.node_offset_ for node_num in self.boundaries[1][1] ]
        elif( id_ == 'horizontal' ):
            return [ node_num + self.node_offset_ for node_num in self.boundaries[2][1] ]
        else:
            raise BaseException('Not Implemented!')           

    def appendNodes( self, mesh ):
        self.node_offset_ = mesh.getNodeAmount()
        for idx, node in enumerate(self.coords):
            mesh.addNode( (idx+1) + self.node_offset_ , node[0] , node[1] , 0 , 0 )

    def appendElements( self, mesh , material_number = 1 , element_type_number = 1 ):
        self.element_offset_ = mesh.getElementAmount()
        for idx, node_nums in enumerate(self.elements):
            mesh.addElement( (idx+1) + self.element_offset_ , material_number,
                             [ node_num + self.node_offset_ for node_num in node_nums ] , element_type_number )

    def appendBoundaries( self, mesh ):
        self.boundary_offset_ = mesh.getElementAmount()
        for idx, node_nums in enumerate(self.boundaries):
            mesh.addBoundary( (idx+1) + self.boundary_offset_, [ node_num + self.node_offset_ for node_num in node_nums ] )
