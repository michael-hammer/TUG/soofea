""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2007-2010 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

from xml.etree import ElementTree
from string import split

from output_handler import OutputHandler
from soofea.base.xml_dom_parser import XMLDOMParser
from soofea.base.configuration import Configuration
from soofea.model.node import Node
from soofea.model.element import Element

class XMLOutputHandler(OutputHandler,
                       XMLDOMParser):

    def __init__(self,path,mode='w'):
        XMLDOMParser.__init__(self,'soofea',path,mode)

    def createNumberList(self, array):
        number_list = ''
        for i in array:
            number_list = number_list + str(i) + ' '
        number_list = number_list.rstrip()
        return number_list

    def writeModel(self,model):
        if hasattr(model , 'global_'):
            xml_global = ElementTree.SubElement(self.root_,'global', \
                                                    dim=str(model.global_.dimension_), \
                                                    coord_sys_type=model.global_.coord_sys_type_, \
                                                    analysis_type=model.global_.analysis_type_, \
                                                    convergence_criteria=str(model.global_.convergence_criteria_), \
                                                    sle_solution_method=model.global_.sle_solution_method_ )

        if hasattr(model , 'contact_'):
            xml_contact = ElementTree.SubElement(self.root_,'contact', \
                                                 mortar = self.createNumberList( model.contact_.mortar_boundaries_ ), \
                                                 non_mortar = self.createNumberList( model.contact_.non_mortar_boundaries_ ), \
                                                 penalty = str(model.contact_.penalty_), \
                                                 friction = str(model.contact_.friction_), \
                                                 mu = str(model.contact_.mu_) )

        node_container = self.createTreeElement(["node"])
        for i, node in model.nodes_.iteritems():
            ElementTree.SubElement(node_container,'n',N=str(node.getNumber()),x=str(node.coordinates_[0]),y=str(node.coordinates_[1]))

        element_container = self.createTreeElement(["element"])
        for i, element in model.elements_.iteritems():
            node_list = self.createNumberList(element.node_numbers_)
            ElementTree.SubElement( element_container,'e',N=str(element.getNumber()),gnn=node_list, \
                                    m=str(element.material_number_), et=str(element.element_type_number_) )

        if hasattr(model , 'boundaries_'):
            boundary_container = self.createTreeElement(["boundary"])
            for i, boundary in model.boundaries_.iteritems():
                node_list = self.createNumberList(boundary.node_numbers_)
                class_name = boundary.__class__.__name__
                if class_name == 'LineBoundary':
                    type_name = 'line'
                if class_name == 'ArcBoundary':
                    type_name = 'arc'
                xml_boundary = ElementTree.SubElement(boundary_container,'b', \
                                                          N=str(boundary.getNumber()),gnn=node_list)
                xml_boundary.set('id',type_name)
   
        self.write()
