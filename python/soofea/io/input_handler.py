""" ------------------------------------------------------------------
This file is part of SOOFEA.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2007-2010 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

Authors: Michael Hammer
------------------------------------------------------------------ """

from string import split

from soofea.model.model import Model
from soofea.model.model_progression import ModelProgression
from soofea.model.time import TimeStamp
from soofea.base.xml_dom_parser import XMLDOMParser
from soofea.base.configuration import Configuration
from soofea.model.node import Node
from soofea.model.element import Element
from soofea.model.boundary import ArcBoundary, LineBoundary

#------------------------------------------------------------------
class InputHandler( ):
    def createModel(self):
        model = Model()

        for node in self.getNodeIterator():
            model.addNode( node )

        for element in self.getElementIterator():
            model.addElement( element )

        for boundary in self.getBoundaryIterator():
            model.addBoundary( boundary )
        
        return model

    def createModelProgression(self,model):
        model_progression = ModelProgression()

        for time_stamp in self.getResultIterator():
            model_progression.time_bar_.addTimeStamp( time_stamp )

            for node in self.getNodeIterator( time_stamp ):
                model.addDisplacement( node , time_stamp )

        return model_progression

#------------------------------------------------------------------
class XMLIterator:
    def __init__(self,iterator):
        self.iter_ = iterator
    
    def __iter__(self):
        return self

#------------------------------------------------------------------
class XMLNodeIterator( XMLIterator ):
    def next(self):
        xml = self.iter_.next() # raises StopIteration if empty
        coordinates = []
        for direction in ['x','y','z']:
            val = xml.get(direction)
            if val:
                coordinates.append( float(val) )
        index = int(xml.get('N'))
        node = Node( index , coordinates )
        return node

#------------------------------------------------------------------
class XMLElementIterator( XMLIterator ):
    def next(self):
        xml = self.iter_.next() # raises StopIteration if empty
        node_number_list = [int(i) for i in split(xml.get('gnn'),' ')]
        index = int(xml.get('N'))
        element = Element( index , node_number_list , \
                           material_number=int(xml.get('m')), element_type_number=int(xml.get('et')) )
        return element

#------------------------------------------------------------------
class XMLBoundaryIterator( XMLIterator ):
    def next(self):
        xml = self.iter_.next() # raises StopIteration if empty
        node_number_list = [int(i) for i in split(xml.get('gnn'),' ')]
        index = int(xml.get('N'))
        if( xml.get('id') == 'arc' ):
            boundary = ArcBoundary( index , node_number_list )
        if( xml.get('id') == 'line' ):
            boundary = LineBoundary( index , node_number_list )
        return boundary

#------------------------------------------------------------------
class XMLResultIterator( XMLIterator ):
    def next(self):
        xml = self.iter_.next() # raises StopIteration if empty
        index = int(xml.get('N'))
        time_stamp = TimeStamp(float(xml.get('time')),index,0)
        return time_stamp

#------------------------------------------------------------------
class XMLInputHandler(InputHandler,
                      XMLDOMParser):

    def __init__(self,path,mode='r'):
        XMLDOMParser.__init__(self,"soofea",path,mode)

    def getNodeIterator(self,time_stamp=None):
        iteratable = None
        if time_stamp:
            time_stamps = self.getTreeIterator( ["result"], "time_stamp" )
            if( time_stamps ):
                for xml_time_stamp in time_stamps:
                    if( int(xml_time_stamp.get('N')) == time_stamp.time_index_ ):
                        iteratable = self.getTreeIterator( ["node"] , "n" , root_element=xml_time_stamp )
            else:
                raise IOException("There are no result time_stamp available in file")
            if iteratable is None:
                raise IOException("The time_stamp with index "+str(time_stamp.time_index_)+" couldn't be found")
        else:
            iteratable = self.getTreeIterator( ["node"] , "n" )
            if iteratable is None:
                raise IOException("We try to add nodes but there is no <node> container in input file")

        return XMLNodeIterator( iter(iteratable) )                

    def getElementIterator(self):
        iteratable = self.getTreeIterator( ["element"] , "e" )
        if( iteratable ):
            return XMLElementIterator( iter( iteratable ) )
        else:
            raise IOException("We try to add elements but there is no <element> container in input file")

    def getBoundaryIterator(self):
        iteratable = self.getTreeIterator( ["boundary"] , "b" )
        if( iteratable ):
            return XMLBoundaryIterator( iter( iteratable ) )
        else:
            raise StopIteration

    def getResultIterator(self):
        iteratable = self.getTreeIterator( ["result"] , "time_stamp" )
        if( iteratable ):
            return XMLResultIterator( iter( iteratable ) )
        else:
            raise StopIteration
