from .._SOOFEA import ModelHandler
from xml.etree import ElementTree

def loadLagrange( filename , node_list = []):
    xml_file = open( filename, 'rw' )
    tree = ElementTree.parse( xml_file )
    global_tag = tree.getroot().find( 'global' )
    dimension = int(global_tag.get('dim'))
    
    
    result_container = tree.getroot().find( 'result' )
    if result_container == None:
        raise Exception('No Results found in input file!')

    if( len(node_list) == 0 ):
        raise Exception("Need a node list were I should extract the Lagrange multipliers")

    results = {}
    results['coords'] = {} 
    results['time_stamp'] = {}
        
    node_container = tree.getroot().find( 'node' )
    for node in node_container:
        if int(node.get('N')) in node_list:
            node_coord = []
            node_coord.append(float(node.get('x')))
            node_coord.append(float(node.get('y')))
            if( dimension == 3 ):
                node_coord.append(float(node.get('z')))
            results['coords'][int(node.get('N'))] = node_coord

    time_stamps = result_container.findall('time_stamp')
    for time_stamp in time_stamps:
        results['time_stamp'][int(time_stamp.get('N'))] = {}
        node_container = time_stamp.find('node')
        for node in node_container:
            if int(node.get('N')) in node_list:
                value = 0.
                if 'lagrange' in node.keys():
                    value = float(node.get('lagrange'))
                results['time_stamp'][int(time_stamp.get('N'))][int(node.get('N'))] = value 

    return results