close all;
clear all;

% configuration
relative_path='';
example='sharp_block';
versions=['1','4'];
methods=['M','A'];

export=1;

% figure configuration
scrsz = get(0,'ScreenSize');
if( export )
    figure('visible','off','PaperUnits', 'centimeters');
else
    figure('Position',[0 scrsz(4)/3 scrsz(3)/2 scrsz(4)/3*2], 'PaperUnits', 'centimeters');
end

h1 = subplot(2,1,1);
    hold(h1,'on');
    axis([0 109 -2000 20000]);
    position = get(h1,'OuterPosition');
    position(2)=0.4;
    position(4)=0.6;
    set(h1,'OuterPosition',position);
    grid;

% set paper
paper_width = 20;
paper_height = paper_width*2.5/4;
set(gcf, 'PaperSize',[paper_width paper_height])
papersize = get(gcf, 'PaperSize');
width = 20;
height = width*2.5/4;
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition',[left,bottom,width,height]);

F_cell = cell(2,length(methods),length(versions));
version_counter = 1;
for version = versions
    method_counter = 1;
    for method = methods
        F_int_x = load( [relative_path,example,'_',version,'x',version,'.',method,'.F_int_x'] );
        F_int_y = load( [relative_path,example,'_',version,'x',version,'.',method,'.F_int_y'] );
        F_cell{1,method_counter,version_counter} = sum(F_int_x,1);
        F_cell{2,method_counter,version_counter} = sum(F_int_y,1);
        method_counter = method_counter + 1;
    end   
    version_counter = version_counter + 1;
end

xMC_plot = plot( h1, F_cell{1,1,1} , '-b');
yMC_plot = plot( h1, F_cell{2,1,1} , '-b');
xAC_plot = plot( h1, F_cell{1,2,1} , '-g');
yAC_plot = plot( h1, F_cell{2,2,1} , '-g');
xMF_plot = plot( h1, F_cell{1,1,2} , '-c');
yMF_plot = plot( h1, F_cell{2,1,2} , '-c');
xAF_plot = plot( h1, F_cell{1,2,2} , '-r');
yAF_plot = plot( h1, F_cell{2,2,2} , '-r');

MC = hggroup;
set(get(get(MC,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
AC = hggroup;
set(get(get(AC,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
MF = hggroup;
set(get(get(MF,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
AF = hggroup;
set(get(get(AF,'Annotation'),'LegendInformation'),'IconDisplayStyle','on');
set([xMC_plot,yMC_plot],'Parent',MC);
set([xAC_plot,yAC_plot],'Parent',AC);
set([xMF_plot,yMF_plot],'Parent',MF);
set([xAF_plot,yAF_plot],'Parent',AF);
l1 = legend( h1, 'Mortar Normal (coarse)','Average Normal (coarse)','Mortar Normal (fine)','Average Normal (fine)' );
set( l1,...
    'Position',[0.435731132075472 0.617679355783309 0.153694968553459 0.127196193265007]);
xlabel(h1,'Time Step');
ylabel(h1,'Reaction Force');
    
h2 = subplot(2,1,2);
    hold(h2,'on');
    axis([0 109 -1e-3 1.5e-3]);
    position = get(h2,'OuterPosition');
    position(4)=0.3;
    set(h2,'OuterPosition',position);
    grid;

plot( h2 , (F_cell{2,2,1} - F_cell{2,1,1})./F_cell{2,1,1} , '-b' );
plot( h2 , (F_cell{2,2,2} - F_cell{2,1,2})./F_cell{2,1,2} , '-r' );
l2 = legend( h2 , 'Coarse Mesh', 'Fine Mesh' );
l2_pos = get( l2, 'Position' );
l2_pos(2) = 0.28;
set( l2, 'Position', l2_pos );
xlabel(h2,'Time Step');
ylabel(h2,'Relative Deviation');


if( export )
    print( '-depsc2', [relative_path,example] );
end