%node_numbers = 1858:1870;
node_numbers = 139:142;
time_step_amount = 109;
F_int = zeros( length(node_numbers) , time_step_amount );
for node = 1:length(node_numbers)
    F_int( node , : ) = load( [num2str(node_numbers(node)),'.M_y.txt'] );
end

