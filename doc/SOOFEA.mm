<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1241516160520" ID="Freemind_Link_1140329738" MODIFIED="1241516232996" TEXT="SOOFEA">
<cloud/>
<node CREATED="1241516237174" ID="_" MODIFIED="1241516708383" POSITION="right" TEXT="Mortar Contact" VSHIFT="-34">
<node CREATED="1241516249506" ID="Freemind_Link_469809745" MODIFIED="1241516253805" TEXT="Lagrange Method">
<node CREATED="1241516267098" ID="Freemind_Link_447018661" MODIFIED="1241516510455" TEXT="2D frictionless" VSHIFT="-41">
<node CREATED="1241516381125" HGAP="23" ID="Freemind_Link_296984462" MODIFIED="1241780430307" TEXT="save Lagrange in MortarIntegrationPoint" VSHIFT="-33">
<icon BUILTIN="button_ok"/>
<node CREATED="1241516431994" ID="Freemind_Link_1535070927" MODIFIED="1241780426027" TEXT="implement Edge::updateIntegrationPointsLagrangeFactor()" VSHIFT="-26">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node CREATED="1241516258202" ID="Freemind_Link_31527244" MODIFIED="1241780463714" TEXT="Penalty Method" VSHIFT="22">
<node CREATED="1241780447349" ID="Freemind_Link_144415067" MODIFIED="1241780467890" TEXT="2D frictionless" VSHIFT="-29">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
</map>
