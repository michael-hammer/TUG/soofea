#ifndef node_container_h___
#define node_container_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>
#include <soofea_assert.h>

#include "integration_point.h"
#include "type_container.h"
#include "exceptions.h"
#include "math/array.h"
#include "node.h"
#include "base/template/vector_aggregate.h"
#include "base/template/const_vector_aggregate.h"

//------------------------------------------------------------------
class NodeContainer :
  public TypeContainer,
  public VectorAggregate<Node>,
  public ConstVectorAggregate<Node>
{
public:
  ~NodeContainer();

  void addNode(Node* node);

  inline Node* getLastNode()
  { return( node_vector_.back() ); }

  int getLocalNodeNumber(const Node& node) const;

  inline numbered getGlobalNodeNumber(int local_node_number) const
  { return( node_vector_[local_node_number] -> getNumber() ); }

  inline std::string getNodeNumbersString() const
  { return( ParserBase::vectorToString( *node_number_vector_ ) );}

  /**
     X_{iJ}

     i ... Coordinate direction
     J ... Node index
   */
  bz::Array<double,2>* getLagrangianPointTensor() const;

  bz::Array<double,2>* getEulerianPointTensor() const;

  bz::Array<double,2>* getLagrangeFactorTensor() const;

  Point* getEulerianPoint( double r, double s=0., double t=0.) const;

  bool hasNode(const Node& node);

  void prettyPrint() const;
protected:
  friend class Model;

  inline const std::vector<numbered>& getNodeNumberVector()
  {return( *node_number_vector_ );}

  void setNodes(std::vector<Node*>& node_vector);

  inline NodeContainer(std::vector<numbered>* node_number_vector, numbered type_number) :
    TypeContainer(type_number),
    VectorAggregate<Node>(&node_vector_),
    ConstVectorAggregate<Node>(&node_vector_),
    node_number_vector_(node_number_vector)
  {}
 
  std::vector<numbered>* node_number_vector_;
  std::vector<Node*> node_vector_;
};

#endif // node_container_h___
