/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "edge.h"
#include "math/array.h"

//------------------------------------------------------------------
void Edge::prettyPrint() const
{
  std::cout << "---------------------------------" << std::endl;
  std::cout << "| Edge: " << number_ << std::endl;
  std::cout << "| " << std::endl;

  for( std::vector<Node*>::const_iterator iter = node_vector_.begin();
       iter != node_vector_.end() ;
       iter++ ) {
    std::cout << "| node number : " << (*iter) -> getNumber() << std::endl;
    std::cout << "|  Lagrange: " << std::endl;
    (*iter) -> getLagrangianPoint().prettyPrint();
    std::cout << "|  Euler: " << std::endl;
    dynamic_cast<PointAggregate*>(*iter) -> getEulerianPoint().prettyPrint();
  }
  std::cout << "---------------------------------" << std::endl;
}

//------------------------------------------------------------------
bool Edge::operator==(const Edge& src)
{
  for( std::vector<Node*>::iterator node_iter = node_vector_.begin() ;
       node_iter != node_vector_.end() ;
       ++node_iter ) {
    if( find( src.node_vector_.begin() ,
	      src.node_vector_.end() ,
	      *node_iter ) == src.node_vector_.end() )
      return( false );
  }
  return( true );
}

//------------------------------------------------------------------
Vector* Edge::getEulerianNormal( double r, double s, bool normalized ) const
{
  Vector* tangent = getEulerianTangent( r );
  bz::Array<double,2>* eps_2D = Math::epsilon_2D<double>();
  bz::Array<double,1> normal_array( bz::sum( (*eps_2D)(bzt::i,bzt::j) * (*tangent)(bzt::j) , bzt::j ) );
  Vector* normal = new Vector( tangent -> getCoordSys(), normal_array );
  if( normalized )
    (*normal) /= Math::norm( *normal );
  delete tangent;

  return( normal );
}
