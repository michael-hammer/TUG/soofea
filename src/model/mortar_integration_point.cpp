/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "mortar_integration_point.h"

//------------------------------------------------------------------
MortarIntegrationPoint::~MortarIntegrationPoint()
{
  if( average_tangent_ )
    delete average_tangent_;
}

//------------------------------------------------------------------
void MortarIntegrationPoint::setGapData( double g_N, double g_T, double xi, int local_mortar_edge_number )
{ 
  if( local_mortar_edge_number >= 0 ) {
    local_mortar_edge_number_ = local_mortar_edge_number;
  }
  xi_ = xi;
  g_N_ = g_N;
  g_T_ = g_T;
}

//------------------------------------------------------------------
bool MortarIntegrationPoint::setNearestMortarNode( Node* nearest_mortar_node )
{
  if( nearest_mortar_node_ != nearest_mortar_node ) {
    nearest_mortar_node_ = nearest_mortar_node;
    mortar_edge_vector_.clear();
    return( true );
  }
  return( false );
}

//------------------------------------------------------------------
void MortarIntegrationPoint::addMortarEdge( Edge* mortar_edge )
{ 
  mortar_edge_vector_.push_back(mortar_edge);
}

//------------------------------------------------------------------
void MortarIntegrationPoint::setAverageTangent( Vector* average_tangent )
{
  if( average_tangent_ )
    delete average_tangent_;
  average_tangent_ = average_tangent;
}

//------------------------------------------------------------------
bool MortarIntegrationPoint::setActive(bool active)
{
  if( !active ) {
    xi_ = 0;
    g_N_ = 0;
    g_T_ = 0;
    last_converged_xi_ = 0;
    last_converged_edge_ = 0;  
    last_converged_g_T_ = 0; 
    local_mortar_edge_number_ = -1;
  }                         

  if( active_ != active ) {
    active_ = active;
    return( true );
  }
  return( false );
}

//------------------------------------------------------------------
Edge& MortarIntegrationPoint::getActiveMortarEdge( bool last_converged )
{
  if( !last_converged )
    return( *mortar_edge_vector_[ local_mortar_edge_number_ ] );
  else {
    return( *last_converged_edge_ );
  }
}

//------------------------------------------------------------------
double MortarIntegrationPoint::xi( bool last_converged )
{
  if( !last_converged )
    return( xi_ );
  else {
    return( last_converged_xi_ );
  }
}

//------------------------------------------------------------------
double MortarIntegrationPoint::getGT(bool last_converged)
{ 
  if( !last_converged )
    return( g_T_ ); 
  else {
    return( last_converged_g_T_ );
  }
}

//------------------------------------------------------------------
void MortarIntegrationPoint::archive()
{
  last_converged_edge_ = mortar_edge_vector_[ local_mortar_edge_number_ ];
  last_converged_xi_ = xi_;
  last_converged_g_T_ = g_T_;
}
