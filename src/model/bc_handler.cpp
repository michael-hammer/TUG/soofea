/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007  Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "bc_handler.h"

//------------------------------------------------------------------
BCHandler::~BCHandler()
{
  for( std::map<numbered,Prescribed<LoadValue>*>::iterator pliter = prescribed_load_map_.begin();
       pliter != prescribed_load_map_.end() ; 
       pliter++) {
    delete (pliter->second);
  }

  for( std::map<numbered,Prescribed<DOFValue>*>::iterator pditer = prescribed_dof_map_.begin();
       pditer != prescribed_dof_map_.end() ; 
       pditer++) {
    delete (pditer->second);
  }
}

//------------------------------------------------------------------
void BCHandler::addPrescribedDOF(Prescribed<DOFValue>* prescribed_dof)
{
  prescribed_dof_map_[prescribed_dof->getNumber()] = prescribed_dof;
}

//------------------------------------------------------------------
void BCHandler::addPrescribedLoad(Prescribed<LoadValue>* prescribed_load)
{
  prescribed_load_map_[prescribed_load->getNumber()] = prescribed_load;
}

//------------------------------------------------------------------
void BCHandler::unsetConstraintLoad()
{
  for( Iterator<Node> node_iter = model_.Aggregate<Node>::begin();
       node_iter != model_.Aggregate<Node>::end() ;
       ++node_iter ) {
    node_iter -> setConstraintLoadZero();
  }
}

//------------------------------------------------------------------
void BCHandler::unsetConstraintDOF()
{
  for( Iterator<Node> node_iter = model_.Aggregate<Node>::begin();
       node_iter != model_.Aggregate<Node>::end() ;
       ++node_iter ) {
    node_iter -> unsetConstraintDOF();
  }
}

//------------------------------------------------------------------
void BCHandler::setPrescribedDOFZero( TimeStamp& time_stamp )
{
  for( Iterator<Node> node_iter = model_.Aggregate<Node>::begin();
       node_iter != model_.Aggregate<Node>::end() ;
       ++node_iter ) {
    node_iter -> setConstraintDOFZero();
  }
}

//------------------------------------------------------------------
void BCHandler::integrateBC( TimeStamp& time_stamp )
{
  unsetConstraintLoad();
  for( Iterator<LoadProgress> prescribed_load_iter = model_.getTimeBar().getTimeEpisode( time_stamp ).Aggregate<LoadProgress>::begin();
       prescribed_load_iter != model_.getTimeBar().getTimeEpisode( time_stamp ).Aggregate<LoadProgress>::end() ;
       ++prescribed_load_iter ) {
    integratePrescribedLoad(time_stamp, *prescribed_load_iter);
  }

  unsetConstraintDOF();
  for( Iterator<DOFProgress> prescribed_dof_iter = model_.getTimeBar().getTimeEpisode( time_stamp ).Aggregate<DOFProgress>::begin();
       prescribed_dof_iter != model_.getTimeBar().getTimeEpisode( time_stamp ).Aggregate<DOFProgress>::end() ;
       ++prescribed_dof_iter ) {
    integratePrescribedDOF(time_stamp, *prescribed_dof_iter);
  }
}

//------------------------------------------------------------------
std::vector<LoadValue*>* BCHandler::getScaledLoadValues( TimeStamp& time_stamp,
                                                         LoadProgress& load_progress )
{
  TimeEpisode& time_episode = model_.getTimeBar().getTimeEpisode( time_stamp );
  std::vector<LoadValue*> *values = new std::vector<LoadValue*>;
  const std::vector<numbered>& reference_vector = load_progress.getReferenceVector();
  LoadValue *end_load_value;

  switch( load_progress.getShape() ) {
  case IDObject::CONST:
    for( Iterator<LoadValue> iter = prescribed_load_map_.find( reference_vector.at(0) )->second->Aggregate<LoadValue>::begin() ;
         iter != prescribed_load_map_.find( reference_vector.at(0) )->second->Aggregate<LoadValue>::end() ;
         ++iter ) {
      values -> push_back( new LoadValue( iter->getExternal() ,
                                          iter->getCoordID() ) );
    }
    break;
  case IDObject::RAMP:
    for( Iterator<LoadValue> iter = prescribed_load_map_.find( reference_vector.at(0) )->second->Aggregate<LoadValue>::begin() ;
         iter != prescribed_load_map_.find( reference_vector.at(0) )->second->Aggregate<LoadValue>::end() ;
         ++iter ) {
      end_load_value = &( prescribed_load_map_.find( reference_vector.at(1) )->second->getValue(iter -> getCoordID()) );
      values -> push_back( new LoadValue( ( end_load_value->getExternal() - iter->getExternal() ) *
                                          (double)(time_stamp.index() - time_episode.getStartIndex()) / (double)(time_episode.getStepAmount()) ,
                                          iter->getCoordID() ) );
    }
    break;
  default:
    throw EXC(CalcException) << "The given progress shape '" 
                             << IDObject::resolveID( load_progress.getShape() ) 
                             << "' is not implemented in BCHandler" << Exception::endl;
    break;
  }

  return( values );
}

//------------------------------------------------------------------
void BCHandler::integratePrescribedLoad( TimeStamp& time_stamp,
                                         LoadProgress& load_progress )
{
  std::vector<LoadValue*>* load_vector = getScaledLoadValues( time_stamp, load_progress ); 
  numbered target_number = prescribed_load_map_.find( load_progress.getFirstReference() )->second->getTargetNumber();
  IDObject::ID target_type = prescribed_load_map_.find( load_progress.getFirstReference() )->second->getTarget();
  IDObject::ID load_type = prescribed_load_map_.find( load_progress.getFirstReference() )->second->getType();

  for( std::vector<LoadValue*>::iterator iter = load_vector -> begin() ;
        iter != load_vector -> end() ;
        ++iter ) {
    switch( target_type ) {
    case IDObject::NODE:
      switch( load_type ) {
      case IDObject::FORCE:
        // The external loads set in nodes here have to be work conjugated!
        model_.Aggregate<Node>::get(target_number).getLoad( load_type )->
          getValue( (*iter)->getCoordID() )->setExternal( (*iter)->getExternal() );
        break;
      case IDObject::SURFACE_FORCE:
        throw EXC(CalcException) << "Force densities on nodes not implemented!" << Exception::endl;
        break;
      default:
        throw EXC(CalcException) << "Do you also feel the northern wind?" << Exception::endl;        
        break;
      }
      break;
    case IDObject::BOUNDARY:
      // Load distribution!!
      for( Iterator<BoundaryComponent> target_iter = model_.Aggregate<Boundary>::get(target_number).Aggregate<BoundaryComponent>::begin() ;
           target_iter != model_.Aggregate<Boundary>::get(target_number).Aggregate<BoundaryComponent>::end() ;
           ++target_iter ) {
        switch( load_type ) {
        case IDObject::PRESSURE:
          target_iter->setPressure( (*iter)->getExternal() );
          break;
        case IDObject::SURFACE_FORCE:
          for( Iterator<Node> node_iter = target_iter->Aggregate<Node>::begin();
               node_iter != target_iter->Aggregate<Node>::end();
               ++node_iter ) {
            throw EXC(CalcException) << "Force densities on boundaries not implemented!" << Exception::endl;
          }
          break;
        default:
          throw EXC(CalcException) << "Ist heute schon wieder Foen?" << Exception::endl;          
        }
      }
      break;
    default:
      throw EXC(CalcException) << "The given target in the Prescribed<LoadValue> '"
                               << IDObject::resolveID( target_type )
                               << "' is not implemented" << Exception::endl;
      break;
    }
    delete *iter;
  }
  delete load_vector;
}

//------------------------------------------------------------------
std::vector<DOFValue*>* BCHandler::getScaledDOFValues( TimeStamp& time_stamp ,
                                                       DOFProgress& dof_progress )
{
  TimeEpisode& time_episode = model_.getTimeBar().getTimeEpisode( time_stamp );
  std::vector<DOFValue*> *values = new std::vector<DOFValue*>;
  const std::vector<numbered>& reference_vector = dof_progress.getReferenceVector();
  DOFValue *end_dof_value;

  switch( dof_progress.getShape() ) {
  case IDObject::CONST:
    for( Iterator<DOFValue> iter = prescribed_dof_map_.find( reference_vector.at(0) )->second->Aggregate<DOFValue>::begin() ;
         iter != prescribed_dof_map_.find( reference_vector.at(0) )->second->Aggregate<DOFValue>::end() ;
         ++iter ) {
      values -> push_back( new DOFValue( iter->getValue() ,
                                         iter->getCoordID() ) );      
    }
    break;
  case IDObject::RAMP:
    for( Iterator<DOFValue> iter = prescribed_dof_map_.find( reference_vector.at(0) )->second->Aggregate<DOFValue>::begin() ;
         iter != prescribed_dof_map_.find( reference_vector.at(0) )->second->Aggregate<DOFValue>::end() ;
         ++iter ) {
      end_dof_value = &( prescribed_dof_map_.find( reference_vector.at(1) )->second->getValue(iter -> getCoordID()) );
      values -> push_back( new DOFValue( ( end_dof_value->getValue() - iter->getValue() ) / ( time_episode.getStepAmount() ) ,
                                         iter->getCoordID() ) );
    }
    break;
  default:
    throw EXC(CalcException) << "The given progress shape '" 
                             << IDObject::resolveID( dof_progress.getShape() ) 
                             << "' is not implemented in BCHandler" << Exception::endl;
    break;
  }

  return( values );
}

//------------------------------------------------------------------
void BCHandler::integratePrescribedDOF(TimeStamp& time_stamp,
                                       DOFProgress& dof_progress)
{ 
  std::vector<DOFValue*>* dof_vector = getScaledDOFValues( time_stamp, dof_progress ); 
  numbered target_number = prescribed_dof_map_.find( dof_progress.getFirstReference() )->second->getTargetNumber();
  IDObject::ID target_type = prescribed_dof_map_.find( dof_progress.getFirstReference() )->second->getTarget();
  IDObject::ID dof_type = prescribed_dof_map_.find( dof_progress.getFirstReference() )->second->getType();

  for( std::vector<DOFValue*>::iterator iter = dof_vector -> begin() ;
        iter != dof_vector -> end() ;
        ++iter ) {
    switch( target_type ) {
    case IDObject::NODE:
      model_.Aggregate<Node>::get(target_number).getDOF( dof_type )->
        getValue( (*iter)->getCoordID() )->setIncremental( (*iter)->getValue() , true );
      break;
    case IDObject::BOUNDARY:
      for( Iterator<Node> node_iter = model_.Aggregate<Boundary>::get(target_number).Aggregate<Node>::begin();
           node_iter != model_.Aggregate<Boundary>::get(target_number).Aggregate<Node>::end();
           ++node_iter ) {
        node_iter->getDOF( dof_type )->getValue( (*iter)->getCoordID() )->setIncremental( (*iter)->getValue() , true );
      }
      break;
    default:
      throw EXC(CalcException) << "The given target in the Prescribed<DOFValue> '"
                               << IDObject::resolveID( target_type )
                               << "' is not implemented" << Exception::endl;
      break;
    }
    delete *iter;
  }
  delete dof_vector;
}
