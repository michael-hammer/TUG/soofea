#ifndef time_episode_h___
#define time_episode_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include <iostream>
#include <vector>

#include "base/numbered_object.h"
#include "base/template/vector_aggregate.h"
#include "time_stamp.h"
#include "progress.h"

//------------------------------------------------------------------
class TimeEpisode :
  public NumberedObject,
  public VectorAggregate<DOFProgress>,
  public VectorAggregate<LoadProgress>
{
public:
  TimeEpisode( numbered number,
               double end_time,
               unsigned step_amount) :
    NumberedObject( number ),
    VectorAggregate<DOFProgress>(&dof_progress_vector_),
    VectorAggregate<LoadProgress>(&load_progress_vector_),
    start_time_(0.),
    end_time_(end_time),
    step_amount_(step_amount),
    start_index_(0)
  {}

  ~TimeEpisode() {}

  inline void setStart( double start_time , unsigned start_index )
  { start_time_ = start_time; start_index_ = start_index; }

  inline double getEndTime()
  { return( end_time_ ); }

  inline double getStartTime()
  { return( start_time_ ); }

  inline unsigned getStartIndex()
  { return( start_index_ ); }

  inline unsigned getStepAmount()
  { return( step_amount_ ); }

  inline unsigned getEndIndex()
  { return( start_index_ + step_amount_ ); }

  /**
     @return the TimeStamp for a given local index inside this
     episode. It's constructed with new and has to be deleted
     therefore.
   */
  TimeStamp* getTimeStamp( unsigned local_index ) const;

  /**
     @return true if given TimeStamp is part of this episode
   */
  bool containsTimeStamp( const TimeStamp& time );

  void addProgress( DOFProgress* progress )
  { dof_progress_vector_.push_back( progress ); }

  void addProgress( LoadProgress* progress )
  { load_progress_vector_.push_back( progress ); }

  void prettyPrint() const;

protected:
  double start_time_;
  double end_time_;
  unsigned step_amount_;
  unsigned start_index_;
  std::vector<DOFProgress*> dof_progress_vector_;
  std::vector<LoadProgress*> load_progress_vector_;
};


#endif
