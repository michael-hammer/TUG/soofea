#ifndef contact_h___
#define contact_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include "exceptions.h"
#include "base/numbered_object.h"
#include "base/parser_base.h"
#include "base/id_object.h"

//------------------------------------------------------------------
class Contact
{
public:
  typedef enum _ContactSide_ {
    MORTAR,
    NON_MORTAR,
    NACS
  } ContactSide;

  inline Contact():
    analysis_type_(IDObject::NAID),
    calc_contact_(false),
    penalty_(0.),
    friction_(false),
    mu_(0.),
    mortar_number_vector_( 0 ),
    non_mortar_number_vector_( 0 )
  {}

  ~Contact();

  inline void setContact(bool calc_contact)
  { calc_contact_ = calc_contact; }

  inline bool getContact()
  {return( calc_contact_ );}

  /**
     @param mortar_number_vector is deleted by Contact -> has to be
     allocated with new
   */
  inline void setMortarNumber(std::vector<numbered> *mortar_number_vector)
  {mortar_number_vector_ = mortar_number_vector;}

  /**
     @param non_mortar_number_vector is deleted by Contact -> has to
     be allocated with new
   */
  inline void setNonMortarNumber(std::vector<numbered> *non_mortar_number_vector)
  {non_mortar_number_vector_ = non_mortar_number_vector;}

  inline void setPenalty(double penalty)
  {penalty_ = penalty;}
  
  inline double getPenalty() const
  {return( penalty_ );}

  inline void setFriction(bool friction)
  {friction_ = friction;}
  
  inline bool getFriction()
  {return( friction_ );}

  inline void setMu(double mu)
  {mu_ = mu;}

  inline double getMu()
  {return( mu_ );}

  void prettyPrint();

  std::string getMortarNumbers() const;
  std::string getNonMortarNumbers() const;

  void setAnalysisType(IDObject::ID analysis_type);

  IDObject::ID getAnalysisType()
  {return( analysis_type_ );}
protected:
  friend class Model;

  IDObject::ID analysis_type_;
  bool calc_contact_;
  double penalty_;
  bool friction_;
  double mu_;

  inline std::vector<numbered>& getMortarNumberVector()
  {return( *mortar_number_vector_ );}

  inline std::vector<numbered>& getNonMortarNumberVector()
  {return( *non_mortar_number_vector_ );}

  std::vector<numbered> *mortar_number_vector_;
  std::vector<numbered> *non_mortar_number_vector_;
};

#endif // contact_h___
