/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "type.h"

//------------------------------------------------------------------
Type::Type(numbered type_number, 
           IDObject::ID type_id,
           IDObject::ID target_id,
           std::vector<int>* amount_of_int_points):
  NumberedObject(type_number),
  amount_of_int_points_(amount_of_int_points),
  int_point_vector_(0),
  shape_(0),
  type_id_(type_id),
  target_id_(target_id),
  impl_(0)
{
  if( amount_of_int_points ) {
    this -> createMathIP( amount_of_int_points ); 
  } else {
    LOG(Logger::WARN) << "created Type '" << IDObject::resolveID( type_id_ )
                      << "' for '" << IDObject::resolveID( target_id_ )
                      << "' without IntegraionPoint!" << Logger::endl;
  }
}

//------------------------------------------------------------------
Type::~Type() 
{
  if( shape_ )
    delete shape_;

  delete amount_of_int_points_;

  if( int_point_vector_ ) {
    for( std::vector<Math::IntegrationPoint*>::iterator iter = int_point_vector_ -> begin(); 
         iter != int_point_vector_ -> end();
         ++iter ) {
      delete *iter;
    }
    delete int_point_vector_;
  }
}

//------------------------------------------------------------------
void Type::initiateMathIntPoints()
{
  if( int_point_vector_ ) {
    for( std::vector<Math::IntegrationPoint*>::iterator iter = int_point_vector_ -> begin();
         iter != int_point_vector_ -> end(); 
         ++iter ) {
      (*iter) -> setShapeTensor( shape_->shapeTensor((*iter)->r(),(*iter)->s(),(*iter)->t()) );
      (*iter) -> setDerivativeShapeTensor( shape_->derivativeShapeTensor((*iter)->r(),(*iter)->s(),(*iter)->t()) );
    }
  } else {
    LOG(Logger::WARN) << "No integration points available in type - we can not initialize the math integration points" 
                      << Logger::endl;
  }
}

//------------------------------------------------------------------
void Type::createMathIP( std::vector<int>* amount_of_int_points )
{
  int_point_vector_ = Math::NumIntIO::getInstance().
    getIntegrationPointsWithPoints(Math::RECTANGULAR,
                                   Math::GAUSS_LEGENDRE,
                                   *amount_of_int_points);
}
