/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "model.h"

CoordSys* Model::world_coord_sys_ = 0;

//------------------------------------------------------------------
Model::~Model()
{
  for( NodeMap::iterator iter = node_map_.begin() ; 
       iter != node_map_.end() ; iter++)
    delete (iter->second);

  for( MaterialMap::iterator iter = material_map_.begin() ; 
       iter != material_map_.end() ; iter++)
    delete (iter->second);

  for( EdgeMap::iterator iter = edge_map_.begin() ; 
       iter != edge_map_.end() ; iter++)
    delete (iter->second);

  for( ElementMap::iterator iter = element_map_.begin() ; 
       iter != element_map_.end() ; iter++)
    delete (iter->second);

  for( TypeMap::iterator iter = type_map_.begin() ; 
       iter != type_map_.end() ; iter++)
    delete (iter->second);

  for( MortarElementMap::iterator iter = mortar_element_map_.begin() ; 
       iter != mortar_element_map_.end() ; iter++)
    delete (iter->second);

  for( BoundaryMap::iterator iter = boundary_map_.begin() ; 
       iter != boundary_map_.end() ; iter++)
    delete (iter->second);

  if( world_coord_sys_ )
    delete world_coord_sys_;

  if( contact_ )
    delete contact_;

  if( global_ )
    delete global_;
}

//------------------------------------------------------------------
void Model::setGlobal(Global* global)
{
  if( global_ ) {
    throw EXC(CalcException) << "You can not set the Global data twice on one model. This is not implemented yet." 
                             << Exception::endl;
  }
  global_ = global;
  createWorldCoordSys();
}

//------------------------------------------------------------------
CoordSys* Model::createWorldCoordSys()
{
  switch( global_ -> getCoordSysID() ) {
  case CoordSys::CARTESIAN:
    world_coord_sys_ = new CartesianCoordSys(global_ -> getDimension());
    break;
  default:
    throw EXC(IOException) << "The choosen CoordSys type is not implemented!" << Exception::endl;
  }

  return(world_coord_sys_);
}

//------------------------------------------------------------------
void Model::addNode(Node& node)
{
  if( (node.getNumber()) >= next_node_number_ )
    next_node_number_ = (node.getNumber()) + 1;
  node_map_[node.getNumber()] = &node;

  /** @fixme  Where should we put this? */
  /** @todo order important due to assembling of global Fext in
      analysis.cpp; first displacements, then twist, then
      temperature */
  if ( global_ -> getDisplacementAnalysis() )
    node.addDisplacementDOF();
  if ( global_ -> getTemperatureAnalysis() )
    node.addTemperatureDOF();
}

//------------------------------------------------------------------
void Model::addEdge(Edge*& edge)
{
  // If we construct the edges inside domain we might want to check if
  // the "same" edge allready exists
  for( EdgeMap::iterator iter = edge_map_.begin() ;
       iter != edge_map_.end() ;
       ++iter ) {
    if( *(iter->second) == *edge ) {
      delete edge;
      edge = iter -> second;
      return;
    }
  }

  addEdge(*edge);
}

//------------------------------------------------------------------
void Model::addEdge(Edge& edge)
{
  if( (edge.getNumber()) >= next_edge_number_ )
    next_edge_number_ = (edge.getNumber()) + 1;

  TypeMap::iterator iter = type_map_.find(edge.type_number_);
  if( iter == type_map_.end() )
    throw EXC(IOException) << "We try to asign to edge: " << edge.getNumber() 
                           << " the edge type: " << edge.type_number_
                           << " which does not exist in model!" << Exception::endl;
  edge.setType( dynamic_cast<EdgeType*>(iter->second) );

  std::vector<Node*> node_vector;
  for( std::vector<numbered>::const_iterator nodes_iter = edge.getNodeNumberVector().begin();
       nodes_iter != edge.getNodeNumberVector().end() ;
       ++nodes_iter )
    node_vector.push_back( node_map_[*nodes_iter] );
  edge.setNodes( node_vector );
  edge.createIntegrationPoints();

  edge_map_[edge.getNumber()] = &edge;
}

//------------------------------------------------------------------
void Model::addFace(Face*& face)
{
  for( FaceMap::iterator iter = face_map_.begin() ;
       iter != face_map_.end() ;
       ++iter ) {
    if( *(iter->second) == *face ) {
      delete face;
      face = iter -> second;
      return;
    }
  }

  addFace(*face);
}

//------------------------------------------------------------------
void Model::addFace(Face& face)
{
  if( (face.getNumber()) >= next_face_number_ )
    next_face_number_ = (face.getNumber()) + 1;

  TypeMap::iterator iter = type_map_.find(face.type_number_);
  if( iter == type_map_.end() )
    throw EXC(IOException) << "We try to asign to face: " << face.getNumber() 
                           << " the face type: " << face.type_number_
                           << " which does not exist in model!" << Exception::endl;
  face.setType( dynamic_cast<FaceType*>(iter->second) );

  std::vector<Node*> node_vector;
  for( std::vector<numbered>::const_iterator nodes_iter = face.getNodeNumberVector().begin();
       nodes_iter != face.getNodeNumberVector().end() ;
       ++nodes_iter )
    node_vector.push_back( node_map_[*nodes_iter] );
  face.setNodes( node_vector );
  face.createIntegrationPoints();

  face_map_[face.getNumber()] = &face;
}

//------------------------------------------------------------------
void Model::addType(Type& type)
{
  type_map_[type.getNumber()] = &type; 
}

//------------------------------------------------------------------
void Model::addMaterial(Material& material)
{
  material_map_[material.getNumber()] = &material;
}

//------------------------------------------------------------------
void Model::addElement(Element& element)
{
  if( (element.getNumber()) >= next_element_number_ )
    next_element_number_ = (element.getNumber()) + 1;

  TypeMap::iterator et_iter = type_map_.find(element.type_number_);
  if( et_iter == type_map_.end() )
    throw EXC(IOException) << "We try to asign to element: " << element.getNumber() 
                           << " the element type: " << element.type_number_
                           << " which does not exist in model!" << Exception::endl;
  element.setType( dynamic_cast<ElementType*>(et_iter->second) );

  MaterialMap::iterator mat_iter = material_map_.find( element.material_number_ );
  if( mat_iter == material_map_.end() )
    throw EXC(IOException) << "We try to asign to element: " << element.getNumber() 
                           << " the material: " << element.material_number_
                           << " which does not exist in model!" << Exception::endl;
  element.setMaterial( mat_iter->second );

  // Not nice acces in the depth of Element -> but efficient and the only place where it should occour
  std::vector<Node*> node_vector;
  for( std::vector<numbered>::const_iterator nodes_in_element_iter = element.getNodeNumberVector().begin();
       nodes_in_element_iter != element.getNodeNumberVector().end() ;
       ++nodes_in_element_iter )
    node_vector.push_back( node_map_[*nodes_in_element_iter] );
  element.setNodes( node_vector );
  element.createIntegrationPoints();
  
  element_map_[element.getNumber()] = &element;
}

//------------------------------------------------------------------
void Model::addMortarElement(MortarElement& mortar_element)
{
  mortar_element_map_[ mortar_element.getNonMortarEdge()->getNumber() ] = &mortar_element;
}

//------------------------------------------------------------------
MortarElement* Model::getMortarElement( Edge& non_mortar_edge )
{
  MortarElementMap::iterator iter = mortar_element_map_.find( non_mortar_edge.getNumber() );
  if( iter != mortar_element_map_.end() )
    return( iter->second );
  else
    return( 0 );
}

//------------------------------------------------------------------
void Model::addBoundary(Boundary& boundary)
{
  std::vector<BoundaryComponent*> target_vector;
  for( std::vector<numbered>::const_iterator target_num_iter = boundary.getTargetNumberVector().begin() ;
       target_num_iter != boundary.getTargetNumberVector().end() ;
       ++target_num_iter ) {
    switch( boundary.getTargetType() ) {
    case IDObject::EDGE:
      target_vector.push_back( edge_map_[*target_num_iter] );
      break;
    case IDObject::FACE:
      target_vector.push_back( face_map_[*target_num_iter] );
      break;
    default:
      break;
    }
  }
  boundary.setTargets( target_vector );

  std::set<Node*> node_set;
  for( Iterator<BoundaryComponent> target_iter = boundary.Aggregate<BoundaryComponent>::begin() ;
       target_iter != boundary.Aggregate<BoundaryComponent>::end() ;
       ++target_iter ) {
    for( Iterator<Node> node_iter = target_iter->Aggregate<Node>::begin();
         node_iter != target_iter->Aggregate<Node>::end();
         ++node_iter ) {
      node_set.insert( &(*node_iter) );
    }
  }
  boundary.setNodes( node_set );

  boundary_map_[ boundary.getNumber() ] = &boundary;
}

//------------------------------------------------------------------
std::vector<Node*>* Model::getBoundaryNeighborhood( const Node& node , unsigned distance )
{
//  std::vector<Node*>* node_vec = new std::vector<Node*>;
//
//  if( ! node.onBoundary() )
//    throw EXC(CalcException) << "Node #" << node.getNumber() 
//                             << " is not on any boundary" << Exception::endl;
//
//  numbered loop_nr = node.getBoundary()->getLoopNumber();
//  std::vector<numbered>* node_loop = node_loop_map_[ loop_nr ];
//  std::vector<numbered>::iterator iter = std::find( node_loop -> begin(), node_loop -> end(), node.getNumber() );
//  int pos = 0;
//
//  for( int count = (iter-(node_loop->begin()))-distance;
//       count != (iter-(node_loop->begin()))+distance;
//       ++count ) {
//    if( count < 0 )
//      pos = node_loop->size()+count;
//    else if( count >= (int)(node_loop->size()) )
//      pos = count-node_loop->size();
//    else
//      pos = count;
//
//    node_vec -> push_back( node_map_[ node_loop->at( pos ) ] );
//  }
//
  return( 0 );
}

//------------------------------------------------------------------
void Model::distributeContact()
{
  for( std::vector<numbered>::iterator iter = (contact_->getMortarNumberVector()).begin();
       iter != (contact_->getMortarNumberVector()).end();
       ++iter ) {
    for( Iterator<BoundaryComponent> b_iter = boundary_map_[*iter]->Aggregate<BoundaryComponent>::begin();
         b_iter != boundary_map_[*iter]->Aggregate<BoundaryComponent>::end();
         ++b_iter ) {
      b_iter->setContactSide(Contact::MORTAR);
    }
  }

  MortarElement* mortar_element = 0;
  for( std::vector<numbered>::iterator iter = (contact_->getNonMortarNumberVector()).begin();
       iter != (contact_->getNonMortarNumberVector()).end();
       ++iter ) {
    for( Iterator<BoundaryComponent> b_iter = boundary_map_[*iter]->Aggregate<BoundaryComponent>::begin();
         b_iter != boundary_map_[*iter]->Aggregate<BoundaryComponent>::end();
         ++b_iter ) {
      b_iter->setContactSide(Contact::NON_MORTAR);
      switch( global_ -> getDimension() ) {
      case 2:
        mortar_element = new MortarElement( *contact_, *dynamic_cast<Edge*>(&(*(b_iter))) );
        break;
      default:
        throw EXC(CalcException) << "Contact only implemented for 2D!" << Exception::endl;
        break;
      }
      addMortarElement( *mortar_element );
    }
  }

//  switch( global_ -> getDimension() ) {
//  case 2:
//    for( EdgeMap::iterator edge_iter = edge_map_.begin() ;
//         edge_iter != edge_map_.end() ;
//         ++edge_iter ) {
//      if( edge_iter -> second -> getContactSide() == Contact::NON_MORTAR ) {
//        mortar_element = new MortarElement( *contact_, *(edge_iter->second) );
//        addMortarElement( *mortar_element );
//      }
//    }
//
//    /** 
//        @fixme This has to be done only if we have to calc the avarage
//        normal, but info is in analysis and not in contact :(
//    **/
//  Node *first_node=0, *last_node=0;
//    for( MortarElementMap::iterator mortar_element_iter = mortar_element_map_.begin();
//       mortar_element_iter != mortar_element_map_.end() ;
//       ++mortar_element_iter ) {
//
//      first_node = &(mortar_element_iter -> second -> getNonMortarEdge() -> Aggregate<Node>::front());
//      last_node = mortar_element_iter -> second -> getNonMortarEdge() -> getLastNode();
//
//      for( MortarElementMap::iterator inner_mortar_element_iter = mortar_element_map_.begin();
//           inner_mortar_element_iter != mortar_element_map_.end() ;
//           ++inner_mortar_element_iter ) {
//        if( inner_mortar_element_iter -> second -> getNonMortarEdge() -> getNumber() != 
//            mortar_element_iter -> second -> getNonMortarEdge() -> getNumber() ) {
//          if( inner_mortar_element_iter -> second -> getNonMortarEdge() -> hasNode( *first_node ) )
//            mortar_element_iter -> second -> setNonMortarEdge(inner_mortar_element_iter -> second -> getNonMortarEdge(),MortarElement::PRECEDING);
//          if( inner_mortar_element_iter -> second -> getNonMortarEdge() -> hasNode( *last_node ) )
//            mortar_element_iter -> second -> setNonMortarEdge(inner_mortar_element_iter -> second -> getNonMortarEdge(),MortarElement::FOLLOWING);
//        }
//      }
//    }
//    break;
//  default:
//    throw EXC(CalcException) << "Contact only implemented for 2D!" << Exception::endl;
//    break;
//  }
}

//------------------------------------------------------------------
void Model::prettyPrint()
{
  std::cout << "=================================" << std::endl;
  for( ElementMap::iterator iter = element_map_.begin();
       iter != element_map_.end() ;
       ++iter ) {
    iter -> second -> prettyPrint();
    std::cout << "---------------------------------" << std::endl;
  }
}

//------------------------------------------------------------------
void Model::setContact(Contact* contact)
{
  contact_ = contact;
  if( contact_ -> getContact() ) {
    distributeContact();
  }
}

//------------------------------------------------------------------
std::vector<Edge*>* Model::getAdjacentEdges( const Node& node )
{
  std::vector<Edge*>* edge_vector = new std::vector<Edge*>;
  for( Iterator<Edge> edge_iter = Aggregate<Edge>::begin() ;
       edge_iter != Aggregate<Edge>::end() ;
       ++edge_iter ) {
    if( edge_iter -> hasNode( node ) ) {
      edge_vector->push_back( &(*edge_iter) );
    }
  }
  return( edge_vector );
}

//------------------------------------------------------------------
void Model::clearArchive()
{
  DOF* dof = 0; Load* load = 0;
  for( Iterator<Node> node_iter = Aggregate<Node>::begin() ;
       node_iter != Aggregate<Node>::end() ;
       ++node_iter ) {
    dof = node_iter -> getDOF( IDObject::DISPLACEMENT );
    if( dof ) {
      dof -> clearArchive();
    } 
    load = node_iter -> getLoad( IDObject::FORCE );
    if( load ) {
      load -> clearArchive();
    } 
  }
}
