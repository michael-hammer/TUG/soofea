/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "node_container.h"

//------------------------------------------------------------------
NodeContainer::~NodeContainer()
{
  if( node_number_vector_ )
    delete node_number_vector_;
}

//------------------------------------------------------------------
void NodeContainer::addNode(Node* node)
{
  if( std::find( node_vector_.begin(), node_vector_.end() , node ) == node_vector_.end() ) {
    node_number_vector_->push_back(node->getNumber());
    node_vector_.push_back(node);
  }
}

//------------------------------------------------------------------
void NodeContainer::setNodes(std::vector<Node*>& node_vector)
{
  if( !node_vector_.empty() ) {
    node_vector_.clear();
  }

  for( std::vector<Node*>::iterator iter = node_vector.begin();
       iter != node_vector.end();
       ++iter ) {
    node_vector_.push_back(*iter);
  }
}

//------------------------------------------------------------------
int NodeContainer::getLocalNodeNumber(const Node& node) const
{
  std::vector<Node*>::const_iterator iter = std::find( node_vector_.begin(), node_vector_.end() , &node );
  if( iter != node_vector_.end() )
    return( iter - node_vector_.begin() );
  else
    throw EXC(CalcException) << "Trying to get the local number of Node " << node.getNumber() 
                             << " which is not member of this NodeContainer" << Exception::endl;
}

//------------------------------------------------------------------
bz::Array<double,2>* NodeContainer::getLagrangianPointTensor() const
{
  int dimension = node_vector_.front() -> getLagrangianPoint().getCoordSys() -> getDimension();

  bz::Array<double,2>* node_point_tensor =
    new bz::Array<double,2>(dimension, node_vector_.size());
  (*node_point_tensor) = 0.;

  int col_count = 0;
  for( std::vector<Node*>::const_iterator iter = node_vector_.begin() ;
       iter != node_vector_.end() ;
       ++iter ) {
    for( int row_count = 0; row_count != dimension; ++row_count ) {
      (*node_point_tensor)(row_count,col_count) +=
        ((*iter) -> getLagrangianPoint()).getCoordinate(row_count);
    }
    ++col_count;
  }

  return( node_point_tensor );
}

//------------------------------------------------------------------
bz::Array<double,2>* NodeContainer::getEulerianPointTensor() const
{
  int dimension = dynamic_cast<PointAggregate*>(node_vector_.front())->getEulerianPoint().getCoordSys() -> getDimension();

  bz::Array<double,2>* node_point_tensor =
    new bz::Array<double,2>(dimension,
			    node_vector_.size());
  (*node_point_tensor) = 0.;

  int col_count = 0;
  for( std::vector<Node*>::const_iterator iter = node_vector_.begin() ;
       iter != node_vector_.end() ;
       ++iter ) {
    for( int row_count = 0; row_count != dimension; ++row_count )
      (*node_point_tensor)(row_count,col_count) +=
	dynamic_cast<PointAggregate*>(*iter)->getEulerianPoint().getCoordinate(row_count);
    ++col_count;
  }

  return( node_point_tensor );
}

//------------------------------------------------------------------
bz::Array<double,2>* NodeContainer::getLagrangeFactorTensor() const
{
  int dimension = 1;

  if( node_vector_.front() -> hasProperty("slide") )
    dimension = dynamic_cast<PointAggregate*>(node_vector_.front()) -> getLagrangianPoint().getCoordSys() -> getDimension();
  
  bz::Array<double,2>* lagrange_tensor = new bz::Array<double,2>(dimension,node_vector_.size());
  (*lagrange_tensor) = 0.;

  int col_count = 0;
  bool lagrange_factor_defined = false;

  for( std::vector<Node*>::const_iterator iter = node_vector_.begin() ;
       iter != node_vector_.end() ;
       ++iter ) {     
    // for nodal activation it might happen that only one node has a
    // lagrange factor - we will interpolate against 0 ... but 0 is set anyway!
    if( (*iter) -> getDOF( IDObject::LAGRANGE ) ) {
      lagrange_factor_defined = true;
      (*lagrange_tensor)(0,col_count) = (*iter) -> getLagrangeFactor().get(CoordSys::NORMAL);
    
      switch( dimension ) {
      case 3:
        (*lagrange_tensor)(2,col_count) = (*iter) -> getLagrangeFactor().get(CoordSys::TANGENT_S);
      case 2:
        (*lagrange_tensor)(1,col_count) = (*iter) -> getLagrangeFactor().get(CoordSys::TANGENT_R);
        break;
      }
    }
    ++col_count;
  }

  if( !lagrange_factor_defined ) {
    delete lagrange_tensor;
    return( 0 );
  } else {
    return( lagrange_tensor );
  }
}

//------------------------------------------------------------------
bool NodeContainer::hasNode(const Node& node)
{
  if( std::find( node_vector_.begin() ,
		 node_vector_.end() ,
		 &node ) != node_vector_.end() )
    return true;

  return false;
}

//------------------------------------------------------------------
void NodeContainer::prettyPrint() const
{
  for( std::vector<Node*>::const_iterator iter = node_vector_.begin() ;
       iter != node_vector_.end() ;
       ++iter ) {
    std::cout << "|  ---" << std::endl;
    (*iter) -> prettyPrint();
  }
}

//------------------------------------------------------------------
Point* NodeContainer::getEulerianPoint( double r, double s, double t ) const
{
  bz::Array<double,2>* node_tensor = getEulerianPointTensor();
  // Not all of the coordinates are really used - depends if node_container is edge,face or element
  bz::Array<double,1>* shape = getType().getShape().shapeTensor( r, s, t );
  bz::Array<double,1> mortar_point( bz::sum( (*node_tensor)(bzt::i,bzt::j) * (*shape)(bzt::j) , bzt::j ) );
  delete shape;
  delete node_tensor;

  return( new Point( dynamic_cast<PointAggregate*>(node_vector_.front())->getEulerianPoint().getCoordSys(), mortar_point) );
}
