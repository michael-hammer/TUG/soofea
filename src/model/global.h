#ifndef global_h__
#define global_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <iostream>

#include "base/id_object.h"
#include "geometry/coord_sys.h"
#include "exceptions.h"

//------------------------------------------------------------------
/**
   Class for storing global data correspinding to the Model
 */
class Global :
  public IDObject
{
public:
  Global(unsigned dimension = 3, IDObject::ID coord_sys_id = IDObject::CARTESIAN ,
         bool displacement = true , bool temperature = false, 
         IDObject::ID analysis_type_id = IDObject::NAID, double convergence_criteria = 0. , 
         IDObject::ID sle_solution_method_id = IDObject::LU) :
    dimension_(dimension),
    coord_sys_id_(coord_sys_id),
    displacement_(displacement),
    temperature_(temperature),
    analysis_type_id_(analysis_type_id),
    convergence_criteria_(convergence_criteria),
    sle_solution_method_id_(sle_solution_method_id)
  {}

  ~Global() {}

  Global& operator=(const Global& src);

  /**
     @param dimension of the problem - can be 1(1D), 2(2D)
     or 3(3D)
   */
  void setDimension(unsigned dimension)
  { dimension_=dimension; }

  /**
     Displacement DOFs in analysis: yes/no

     @param boolean
   */
  void setDisplacementAnalysis(bool boolean);

  /**
     Temperature DOFs in analysis: yes/no

     @param boolean
   */
  void setTemperatureAnalysis(bool boolean);

  /**
     @param coord_sys_id the id of Coordinate type CoordSys::SysID
     @see CoordSys
   */
  inline void setCoordSysID(IDObject::ID coord_sys_id)
  { coord_sys_id_ = coord_sys_id; }

  inline void setAnalysisType(IDObject::ID analysis_type_id)
  {analysis_type_id_ = analysis_type_id;}

  /**
     @param id_string the name of the problem
   */
  void setID(std::string id_string);

  inline void setSLESolutionMethod(IDObject::ID id)
  { sle_solution_method_id_ = id; }

  inline void setConvergenceCriteria( double convergence_criteria )
  { convergence_criteria_ = convergence_criteria; }

  /**
     @return provides the global dimension of the Problem. That
     means 1(1D), 2(2D) or 3(3D)
   */
  unsigned getDimension() const;

  /**
     @return bool, if displacement DOFs have to be calculated
   */
  bool getDisplacementAnalysis();

  /**
     @return bool, if temperature DOFs have to be calculated
   */
  bool getTemperatureAnalysis();

  /**
     @return the CoordSys::SysID (e.g. CoordSys::CARTESIAN)
   */
  IDObject::ID getCoordSysID() const
  { return(coord_sys_id_); }

  inline IDObject::ID getAnalysisType() const
  { return( analysis_type_id_ ); }

  /**
     @return the problem name string
   */
  inline const std::string& getID() const
  { return( id_ ); }

  double getConvergenceCriteria() const;

  inline IDObject::ID getSLESolutionMethod() const
  { return(sle_solution_method_id_); }

  void prettyPrint();
protected:
  unsigned dimension_;
  IDObject::ID coord_sys_id_;
  bool displacement_;
  bool temperature_;
  IDObject::ID analysis_type_id_;
  double convergence_criteria_;
  IDObject::ID sle_solution_method_id_;
  std::string id_;
};

#endif // global_h__
