/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "time_bar.h"

//------------------------------------------------------------------
TimeBar::TimeBar()
{
  TimeStamp* time_stamp = new TimeStamp( 0. , 0 ); // The Lagrangian state
  time_stamp_vector_.push_back( time_stamp );
  last_finished_time_stamp_ = time_stamp;
}

//------------------------------------------------------------------
TimeBar::~TimeBar()
{
  for( std::vector<TimeEpisode*>::iterator iter = episode_vector_.begin();
       iter != episode_vector_.end();
       ++iter ) {
    delete *iter;
  }

  for( std::vector<TimeStamp*>::iterator iter = time_stamp_vector_.begin();
       iter != time_stamp_vector_.end();
       ++iter ) {
    delete *iter;
  }
}

//------------------------------------------------------------------
void TimeBar::addTimeEpisode( TimeEpisode* time_episode )
{
  if( episode_vector_.empty() ) {
    time_episode -> setStart( 0. , 0 );
  } else {
    double last_end_time = episode_vector_.back()->getEndTime();
    if( last_end_time >= time_episode -> getEndTime() )
      throw EXC(CalcException) << "New end time of time episode can't be before the former one!" << Exception::endl;
    time_episode -> setStart( last_end_time , episode_vector_.back()->getEndIndex() );
  }
  episode_vector_.push_back( time_episode );
}

//------------------------------------------------------------------
TimeEpisode& TimeBar::getTimeEpisode( const TimeStamp& time_stamp )
{
  for( std::vector<TimeEpisode*>::iterator iter = episode_vector_.begin();
       iter != episode_vector_.end();
       ++iter ) {
    if( (*iter) -> containsTimeStamp( time_stamp ) )
      return( **iter );
  }

  throw EXC(CalcException) << "TimeBar has no Epsiode containing the time stamp with index " << time_stamp.index() << Exception::endl;
}

//------------------------------------------------------------------
TimeEpisode& TimeBar::getTimeEpisode( numbered te_number )
{
  for( std::vector<TimeEpisode*>::iterator iter = episode_vector_.begin();
       iter != episode_vector_.end();
       ++iter ) {
    if( (*iter) -> getNumber() == te_number )
      return **iter;
  }

  throw EXC(CalcException) << "TimeBar has no Epsiode " << te_number << Exception::endl;
}

//------------------------------------------------------------------
void TimeBar::prettyPrint()
{
  for( std::vector<TimeEpisode*>::iterator iter = episode_vector_.begin();
       iter != episode_vector_.end();
       ++iter ) {
    (*iter) -> prettyPrint();
  }
}

//------------------------------------------------------------------
void TimeBar::distributeTimeStamps()
{
  TimeStamp* time_stamp = 0;
  for( std::vector<TimeEpisode*>::const_iterator iter = episode_vector_.begin();
       iter != episode_vector_.end();
       ++iter ) {    
    for( unsigned local_time_index = 1;
         local_time_index <= (*iter) -> getStepAmount();
         ++local_time_index ) {
      time_stamp = (*iter) -> getTimeStamp( local_time_index );
      time_stamp_vector_.push_back( time_stamp );
    }
  }
}

//------------------------------------------------------------------
TimeStamp* TimeBar::next(TimeStamp* act_item, int& global_time_index)
{
  if( !act_item ) { // We are on beginning
    if( global_time_index < 0 )
      throw EXC(CalcException) << "You really want to increment Time Stamp on an end iterator?" << Exception::endl;
    if( time_stamp_vector_.empty() ) {
      global_time_index = -1;
      return( 0 );
    } else
      return( time_stamp_vector_[0] );
  } else {
    if( (unsigned)(++global_time_index) < time_stamp_vector_.size() )
      return( time_stamp_vector_[global_time_index] );
    else
      global_time_index = -1;
      return( 0 );
  }
}

//------------------------------------------------------------------
Iterator<TimeStamp> TimeBar::begin( TimeStamp* )
{
  if( time_stamp_vector_.empty() )
    return( Aggregate<TimeStamp>::end() );
  else
    return( Iterator<TimeStamp>( this , time_stamp_vector_.front() , 0 ) );
}

//------------------------------------------------------------------
TimeStamp* TimeBar::getTimeStampFromTimeBar( const TimeStamp& time_stamp )
{
  for( std::vector<TimeStamp*>::const_iterator iter = time_stamp_vector_.begin();
       iter != time_stamp_vector_.end();
       ++iter ) {
    if( (*iter) -> index() == time_stamp.index() ) // && fabs( (*iter)->time() - time_stamp.time() ) < DBL_EPSILON*1.e2  )
      return( *iter );
  }
  throw EXC(IOException) << "The given TimeStamp with index '" 
                         << time_stamp.index() << "' and time '"
                         << time_stamp.time() << "' can not be found in the TimeBar" << Exception::endl;
}
