/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "stress.h"

//------------------------------------------------------------------
Stress::Stress(double sigma_xx, double sigma_yy, double sigma_xy) :
  bz::Array<double,2>(2,2),
  dimension_(2)
{
  setValue(XX,sigma_xx);
  setValue(YY,sigma_yy);
  setValue(XY,sigma_xy);
}

//------------------------------------------------------------------
Stress::Stress(double sigma_xx, double sigma_yy, double sigma_zz,
	       double sigma_xy, double sigma_yz, double sigma_xz) :
  bz::Array<double,2>(3,3),
  dimension_(3)
{
  setValue(XX,sigma_xx);
  setValue(YY,sigma_yy);
  setValue(ZZ,sigma_zz);
  setValue(XY,sigma_xy);
  setValue(YZ,sigma_yz);
  setValue(XZ,sigma_xz);
}

//------------------------------------------------------------------
double Stress::getValue(Stress::Index stress_index) const
{
  switch( stress_index )
    {
    case XX:
      return( (*this)(0,0) );
      break;
    case YY:
      return( (*this)(1,1) );
      break;
    case ZZ:
      return( (*this)(2,2) );
      break;
    case XY:
      return( (*this)(0,1) );
      break;
    case YZ:
      return( (*this)(1,2) );
      break;
    case XZ:
      return( (*this)(0,2) );
      break;
    }
  return( 0. );
}

//------------------------------------------------------------------
void Stress::setValue(Index stress_index, double value)
{
  switch( stress_index )
    {
    case XX:
      (*this)(0,0) = value;
      break;
    case YY:
      (*this)(1,1) = value;
      break;
    case ZZ:
      (*this)(2,2) = value;
      break;
    case XY:
      (*this)(0,1) = value;
      (*this)(1,0) = value;
      break;
    case YZ:
      (*this)(1,2) = value;
      (*this)(2,1) = value;
      break;
    case XZ:
      (*this)(0,2) = value;
      (*this)(2,0) = value;
      break;     
    }
}

//------------------------------------------------------------------
Stress::VoigtNotation* Stress::getVoigtNotation()
{
  VoigtNotation* stress_vector = 0;
  switch(dimension_)
    {
    case 2:
      stress_vector = new VoigtNotation(3);
      (*stress_vector)(0) = getValue(XX);
      (*stress_vector)(1) = getValue(YY);
      (*stress_vector)(2) = getValue(XY);
      break;
    case 3:
      stress_vector = new VoigtNotation(6);
      (*stress_vector)(0) = getValue(XX);
      (*stress_vector)(1) = getValue(YY);
      (*stress_vector)(2) = getValue(ZZ);
      (*stress_vector)(3) = getValue(XY);
      (*stress_vector)(4) = getValue(YZ);
      (*stress_vector)(5) = getValue(XZ);
      break;
    }
  return( stress_vector );
}
