#ifndef edge_h__
#define edge_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <vector>
#include <string>

#include <soofea_assert.h>

#include "exceptions.h"

#include "math/array.h"

#include "geometry/cartesian_coord_sys.h"
#include "geometry/vector.h"
#include "geometry/geometry.h"

#include "base/numbered_object.h"

#include "boundary_component.h"
#include "edge_type.h"
#include "template/integration_component.h"

//------------------------------------------------------------------
class Edge : 
  public BoundaryComponent
{
public:
  Edge(numbered edge_number,
       std::vector<numbered>* node_number_vector,
       numbered type_number):
    BoundaryComponent(edge_number,node_number_vector, type_number)
  {}

  virtual ~Edge() {}

  /**
     Compares if two edges have the same Node(s) (it really compares
     the address pointers - this means same Node is the same in
     memory, Node(s) should exist only once anyway). We do not account
     the ordering.
   */
  bool operator==(const Edge& comp_edge);

  Vector* getEulerianNormal( double r, double s=0., bool normalized=false ) const;

  void prettyPrint() const;
};

#endif //  edge_h__
