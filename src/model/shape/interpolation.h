#ifndef interpolation_h___
#define interpolation_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

//------------------------------------------------------------------
/**
   Base class for all FE Interpolation polynoms. Those polynoms are
   characterized by having a root in all nodes except one, where the
   value has to be 1. In FE Analysis we normally need the first 
   derivative of those Interpolation polynoms. Here we define the 
   necessary interface.
 */
class Interpolation
{
public:
  virtual ~Interpolation() {}

protected:
  /**
     Returns the value of a given Interpolation polynom. 

     @param index represents the node, where the polynom value is 1.
     @param var is the value where the polynom has to be evaluated
   */
  virtual double operator()(unsigned index, double var) = 0;
  
  /**
     Returns the value of the derivative of  given Interpolation 
     polynom. 

     @param index represents the node, where the polynom value is 1.
     @param var is the value where the polynom has to be evaluated
   */
  virtual double derivative(unsigned index, double var) = 0;

  virtual double secondDerivative(unsigned index, double var) = 0;

  /**
     @return The order of the Interpolation polynom
   */
  inline virtual unsigned getOrder() = 0;
};

#endif // interpolation_h__
