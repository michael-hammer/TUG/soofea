#ifndef lagrange_h___
#define lagrange_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * ------------------------------------------------------------------*/

#include "interpolation.h"
#include "exceptions.h"

//------------------------------------------------------------------
/**
   Provides Lagrange polynoms with order \f$ n \f$
   \f[ l_k^n(\xi) = \prod_{i = 0,i \ne k} \frac{(\xi - \xi_i)}{\xi_k - \xi_i} \f]
   and their derivatives
   \f[ \frac{\partial l_k^n(\xi)}{\partial \xi} \left( \xi \right) \f]
   both can be evaluated at a given position \f$ \xi \f$ and for a
   special node where the polynomial value is 1. node 
   coordinates are stored at contruction.
 */
class Lagrange :
  public Interpolation
{
public:
  /**
     Constructs a Lagrange polynom of given order. You have to
     provide all coordinates of nodes as double[]. 

     @param order
     @param coordinates
   */
  Lagrange(unsigned order, double*& coordinates);

  /**
     Default destructor
   */
  virtual ~Lagrange() {}

  /**
     Returns the value of the Lagrange polynom for a given node 
     index (\f$ k \f$) evaluated at a position var (\f$ \xi \f$).

     @param index of the node where the polynom value is 1
     @param var coordinate where the polynom should be evaluated
  */
  inline virtual double operator()(unsigned index, double var)
  { return( (*this)(order_,index,var) ); }

  /**
     Returns the value of the Lagrange polynom for a given node 
     index (\f$ k \f$) evaluated at a position var (\f$ \xi \f$).
     The node coordinates are already given by the constructor
     but here you can provide a new order - that means the first
     (order+1) node coordinates are used to evaluate the Langrange
     polynom.

     @param order of Lagrange polynom
     @param index of the node where the polynom value is 1
     @param var coordinate where the polynom should be evaluated
   */
  double operator()(unsigned order, unsigned index, double var);

  /**
     Returns the value of the derivative for a given node index 
     (\f$ k \f$) evaluated at a position var (\f$ \xi \f$).

     @param index of the node where the polynom value is 1
     @param var coordinate where the derivatice should be evaluated
  */
  inline virtual double derivative(unsigned index, double var)
  { return( this->derivative(order_,index,var) ); }

  /**
     Returns the value of the derivative for a given node index 
     (\f$ k \f$) evaluated at a position var (\f$ \xi \f$).
     The node coordinates are already given by the constructor
     but here you can provide a new order - that means the first
     (order+1) node coordinates are used to evaluate the Langrange
     polynom.

     @param order of Lagrange polynom
     @param index of the node where the polynom value is 1
     @param var coordinate where the derivatice should be evaluated
   */
  virtual double derivative(unsigned order, unsigned index, double var);

  virtual double secondDerivative(unsigned index, double var);

  /**
     @return The order of the Interpolation polynom
   */
  inline virtual unsigned getOrder() {return (order_);}
protected:
  unsigned order_;
  double* coordinates_;
};

#endif // lagrange_h___
