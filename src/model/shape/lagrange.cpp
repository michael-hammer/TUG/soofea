/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * ------------------------------------------------------------------*/

#include "lagrange.h"

//------------------------------------------------------------------
Lagrange::Lagrange(unsigned order, double*& coordinates) :
  order_(order),
  coordinates_(coordinates)
{
}

//------------------------------------------------------------------
double Lagrange::operator()(unsigned order, unsigned index, double var)
{
  unsigned count;
  double value = 1.;
  for( count = 0 ; count <= order ; ++count)
    {
      if( count != index )
	value = value * ( var - coordinates_[count] ) / ( coordinates_[index] - coordinates_[count] );
    }
  return( value );
}

//------------------------------------------------------------------
double Lagrange::derivative(unsigned order, unsigned index, double var)
{
  unsigned count = 0;
  unsigned count_sum = 0;
  double undependent_part = 1.;
  double sum = 0.;
  double dependent_part = 1.;
  
  for( count = 0 ; count <= order ; ++count )
    {
      if( count != index )
	undependent_part = undependent_part / (coordinates_[index] - coordinates_[count]);
    }

  for( count_sum = 0 ; count_sum <= order ; ++count_sum )
    {
      if(count_sum != index)
	{
	  dependent_part = 1.;
	  for( count = 0 ; count <= order ; ++count )
	    {
	      if( (count != count_sum) && (count != index) )
		dependent_part = dependent_part * ( var - coordinates_[count] );
	    }
	  sum += dependent_part;
	}
    }

  return( sum * undependent_part );
}

//------------------------------------------------------------------
double Lagrange::secondDerivative(unsigned index, double var)
{
  throw EXC(CalcException) << "Second derivative of Lagrange Polynomials not implemented!" << Exception::endl;  
}
