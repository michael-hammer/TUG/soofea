#ifndef node_index_h___
#define node_index_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

//------------------------------------------------------------------
class NodeIndex
{
public:
  /**
   */
  inline explicit NodeIndex(unsigned I) :
    I_(I),J_(0),K_(0),L_(0),index_amount_(1)
  {}

  /**
   */
  inline explicit NodeIndex(unsigned I, unsigned J):
    I_(I),J_(J),K_(0),L_(0),index_amount_(2)
  {}

  /**
   */
  inline explicit NodeIndex(unsigned I, unsigned J, unsigned K):
    I_(I),J_(J),K_(K),L_(0),index_amount_(3)
  {}

  /**
   */
  inline explicit NodeIndex(unsigned I, unsigned J, unsigned K, unsigned L):
    I_(I),J_(J),K_(K),L_(L),index_amount_(4)
  {}
  
  /**
   */
  unsigned operator[](int index) const;

protected:
  unsigned I_;
  unsigned J_;
  unsigned K_;
  unsigned L_;

  unsigned index_amount_;
};

#endif // node_index_h___
