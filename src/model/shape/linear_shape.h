#ifndef linear_shape_h___
#define linear_shape_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include "shape.h"

//------------------------------------------------------------------
class LinearShape : public Shape
{
public:
  LinearShape(unsigned order);
  virtual ~LinearShape();

  virtual double shape(const NodeIndex& index, 
		       double r, double s = 0., double t = 0.) const;

  virtual double dualShape(const NodeIndex& index, 
			   double r, double s = 0., double t = 0.) const;

  virtual double derivativeShape(unsigned derivative,
				 const NodeIndex& index,
				 double r, double s = 0., double t = 0.) const;
  
  virtual double secondDerivativeShape(unsigned derivative1,
				       unsigned derivative2,
				       const NodeIndex& index,
				       double r = 0., double s = 0., double t = 0.) const;

  /**
   */
  virtual inline unsigned getOrder() const
  { return( lagrange_ -> getOrder() ); }

  /**
   */
  virtual unsigned getNodeAmount() const
  {return( lagrange_ -> getOrder() + 1);}

  /**
   */
  virtual LocalNumberVector* getEdgesOnSurface(unsigned local_surface_number) const;

  /**
   */
  virtual LocalNumberVector* getNodesOnEdge(unsigned local_edge_number) const;

  /**
   */
  virtual inline unsigned getEdgeAmount() const
  { return 0; }

  /**
   */
  virtual inline unsigned getSurfaceAmount() const
  { return 0; }

protected:
  void calcCoordinates(unsigned order);
  virtual void createNodeIndex();

  double* coordinates_;
  Lagrange* lagrange_;
};

#endif // linear_shape_h___
