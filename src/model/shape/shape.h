#ifndef shape_h___
#define shape_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>
#include <soofea_assert.h>

#include "exceptions.h"
#include "base/id_object.h"
#include "node_index.h"
#include "lagrange.h"
#include "cubic_polynomial.h"
#include "math/array.h"

//------------------------------------------------------------------
/**
   Base class for all kind of shape "functions" used for interpolating
   inside the finite element in reference configuration
 */
class Shape
{
public:
  typedef std::vector<NodeIndex*> NodeIndexVector;
  typedef std::vector<unsigned> LocalNumberVector;

  virtual ~Shape();

  /**
     @return The order of the Interpolation polynom
   */
  virtual unsigned getOrder() const = 0;

  /**
   */
  const NodeIndex& getNodeIndex(unsigned local_node_number) const;

  /**
     @return LocalNumberVector with the local Edge numbers on the given Surface. 
     This vector was allocated by new and <b>has to be</b> destructed with delete!
   */
  virtual LocalNumberVector* getEdgesOnSurface(unsigned local_surface_number) const = 0;

  /**
     @return LocalNumberVector with the local Node numbers on the
     given Edge. The node numbers have to be ordered!

     This vector was allocated by new and <b>has to be</b> destructed with delete!
   */
  virtual LocalNumberVector* getNodesOnEdge(unsigned local_edge_number) const = 0;

  /**
   */
  virtual unsigned getEdgeAmount() const = 0;

  /**
   */
  virtual unsigned getSurfaceAmount() const = 0;

  /**
   */
  virtual double shape(const NodeIndex& index, 
		       double r, double s = 0., double t = 0.) const = 0;

  /**
     N_J where J is the node index
   */
  bz::Array<double,1>* shapeTensor(double r, double s = 0., double t = 0) const;


  virtual double dualShape(const NodeIndex& index, 
			   double r, double s = 0., double t = 0.) const = 0;

  bz::Array<double,1>* dualShapeTensor(double r, double s = 0., double t = 0) const;

  /**
   */
  virtual double derivativeShape(unsigned derivative,
				 const NodeIndex& index,
				 double r, double s = 0., double t = 0.) const = 0;

  virtual double secondDerivativeShape(unsigned derivative1,
				       unsigned derivative2,
				       const NodeIndex& index,
				       double r = 0., double s = 0., double t = 0.) const = 0;

  /**
     N_JK where J is the node index and K the derivative index
   */
  bz::Array<double,2>* derivativeShapeTensor(double r, double s = 0., double t = 0.) const;

  bz::Array<double,2>* secondDerivativeShapeTensor(double r, double s = 0., double t = 0.) const;

  virtual unsigned getNodeAmount() const = 0;

  inline unsigned getDimension() const
  {return( dimension_ );}

  inline IDObject::ID getID() const
  {return( type_id_ );}
protected:
  inline Shape(unsigned dimension, IDObject::ID type_id) :
    dimension_(dimension),
    type_id_(type_id)
  {}

  virtual void createNodeIndex() = 0;

  NodeIndexVector node_index_vector_;

  /**
     Dimensionality of the shape. In case of LinearShape it would be 1 else in case
     of TriangleShape 2 or in case of BrickShape 3
   */
  unsigned dimension_;
  IDObject::ID type_id_;
};

#endif // shape_h___
