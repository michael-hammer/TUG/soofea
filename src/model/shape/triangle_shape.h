#ifndef triangle_shape_h___
#define triangle_shape_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include "shape.h"

//------------------------------------------------------------------
/**
   Provides the shape "functions" for finite Triangle Element. The 
   order (m) of the trial functions may be arbitrary. In the following
   drawing you can see how a node on the element can be identified with
   the help of 3 indices.
<pre>
                     (0,0,m)
                        0
                       / \
                      /   \
	    (1,0,m-1)0     0(0,1,m-1)
                    /       \
                   /         \
	 (2,0,m-2)0     0     0(0,2,m-2)
                 /  (1,1,m-2)  \
                /               \
      (3,0,m-3)0     0     0     0(0,3,m-3)
              /(2,1,m-3) (1,2,m-3)\
             .                     .
            .                       .
           .                         .
          /                           \
  (m,0,0)0----0----0----0----0--.....--0(0,m,0)
          (m-1,1,0)|    |    |
               (m-2,2,0)|    |
                    (m-3,3,0)|
                         (m-4,4,0)
</pre>
   There are 2 possible local coodinate Systems in reference configuration:

   <b>Area Coordinates</b> \f$(L_1,L_2,L_3)\f$
<pre>
                       L_3
    
                        ^
                        :
                        0(0,0,1)
                       /:\
                      / : \
                     /  :  \
                    /   :   \
                   /    :    \
                  /     :     \
                 /    .   .    \
                /  .         .  \
        (1,0,0)/.               .\(0,1,0)
             .0-------------------0.
          .                           .
    L_1 <                               > L_2
</pre>
   <b>rs Coordinates </b> \f$(r,s)\f$
<pre>
    s

    ^
    |
    0(0,1)
    |\
    | \
    |  \
    |   \
    |    \
    |     \
    |      \
    |       \
    0--------0--> r
  (0,0)    (1,0)
</pre>      
 */
class TriangleShape : 
  public Shape
{
public:
  /**
     Constructs Shape class for an element with given order

     @param order of the Interpolation polynom
   */
  TriangleShape(unsigned order);

  /**
   */
  virtual ~TriangleShape();

  /**
     Returns for a given node (I,J) (see scetch at TriangleShape
     for explanation of the node notation - third Index is calculated
     internally) the value of the shape function at a given point
     (by area coordinates <b>or</b> rs coordinates) in the reference 
     triangle. The node is the point in the triangle where the value of
     the shape function is 1.

     @param I first node index
     @param J second node index
     @param L_1 first coordinate
     @param L_2 second coordinate
   */
  virtual double shape(const NodeIndex& index, 
		       double r, double s = 0., double t = 0.) const;

  virtual double dualShape(const NodeIndex& index, 
			   double r, double s = 0., double t = 0.) const;

  /**
     Returns for a given node (I,J) (see scetch at TriangleShape
     for explanation of the node notation - third Index is calculated
     internally) the value of derivation of shape function.

     \f[ \frac{\partial N(L_1,L_2,L_3)}{\partial L_i} \f]

     The derivative is evaluated at a given point (by area coordinates 
     <b>or</b> rs coordinates) in the reference triangle. The node 
     is the point in the triangle where the value of the shape 
     function is 1.
  
     @param derivative \f$i\f$
     @param I first node index
     @param J second node index
     @param L_1 first coordinate
     @param L_2 second coordinate
   */
  double derivativeShapeL(unsigned derivative,
			  unsigned I, unsigned J,
			  double L_1, double L_2) const;

  /**
     Returns for a given node (I,J) (see scetch at TriangleShape
     for explanation of the node notation - third Index is calculated
     internally) the value of derivation of shape function.

     \f[ \frac{\partial N(r,s)}{\partial r_i} \f]

     The derivative is evaluated at a given point (by area coordinates 
     <b>or</b> rs coordinates) in the reference triangle. The node 
     is the point in the triangle where the value of the shape 
     function is 1.
  
     @param derivative \f$i\f$
     @param I first node index
     @param J second node index
     @param r first coordinate
     @param s second coordinate
   */
  virtual double derivativeShape(unsigned derivative,
				 const NodeIndex& index,
				 double r, double s = 0., double t = 0.) const;

  virtual double secondDerivativeShape(unsigned derivative1,
				       unsigned derivative2,
				       const NodeIndex& index,
				       double r = 0., double s = 0., double t = 0.) const;
  
  
  /**
     @return The order of the Interpolation polynom
   */
  virtual inline unsigned getOrder() const
  { return( lagrange_ -> getOrder() ); }

  /**
   */
  virtual inline unsigned getNodeAmount() const
  {return( (lagrange_->getOrder() +1)*(lagrange_->getOrder() + 2) / 2 );}

  /**
   */
  virtual LocalNumberVector* getEdgesOnSurface(unsigned local_surface_number) const;

  /**
   */
  virtual LocalNumberVector* getNodesOnEdge(unsigned local_edge_number) const;

  /**
   */
  virtual inline unsigned getEdgeAmount() const
  { return( 3 ); }

  /**
   */
  virtual inline unsigned getSurfaceAmount() const
  { return( 0 ); }

protected:
  /**
     Calculates the area coordinates of each node of the elment in
     the reference configuration.
   */
  void calcAreaCoordinates(unsigned order);

  /**
   */
  virtual void createNodeIndex();

  double* area_coordinates_;
  Lagrange* lagrange_;
};

#endif // triangle_shape_h___
