/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "brick_shape.h"

//------------------------------------------------------------------
BrickShape::BrickShape(unsigned order) :
  Shape(3,IDObject::BRICK),
  coordinates_(new double[order + 1]),
  lagrange_(0)
{
  calcCoordinates(order);
  lagrange_ = new Lagrange(order, coordinates_);

  createNodeIndex();
}

//------------------------------------------------------------------
BrickShape::~BrickShape()
{
  if( coordinates_ )
    delete[] coordinates_;

  if( lagrange_ )
    delete lagrange_;
}

//------------------------------------------------------------------
/**
   @todo works only for order = 1 - would be nice to find an algorithm
   for arbitrarily shape order
 */
void BrickShape::createNodeIndex()
{
  for( unsigned count = 0 ; count < getNodeAmount() ; ++count ) {
    switch( count ) {
      // numbering of nodes in parameter space
      //           t
      //           ^
      //        4--|-----7
      //       /:  |    /|
      //      / :      / |
      //     /  :     /  |
      //    5--------6   |
      //    |   0....|...3
      //    |  .     |  /
      //    | .      | /   ----> s
      //    |.       |/
      //    1--------2
      //        /
      //       /
      //      r
    case 0:
      node_index_vector_.push_back( new NodeIndex(0,0,0) );
      break;
    case 1:
      node_index_vector_.push_back( new NodeIndex(1,0,0) );
      break;
    case 2:
      node_index_vector_.push_back( new NodeIndex(1,1,0) );
      break;
    case 3:
      node_index_vector_.push_back( new NodeIndex(0,1,0) );
      break;
    case 4:
      node_index_vector_.push_back( new NodeIndex(0,0,1) );
      break;
    case 5:
      node_index_vector_.push_back( new NodeIndex(1,0,1) );
      break;
    case 6:
      node_index_vector_.push_back( new NodeIndex(1,1,1) );
      break;
    case 7:
      node_index_vector_.push_back( new NodeIndex(0,1,1) );
      break;
    }
  }
}

//------------------------------------------------------------------
double BrickShape::shape(const NodeIndex& index,
			 double r, double s, double t) const
{
  return ( (*lagrange_)( index[0], r) *
	   (*lagrange_)( index[1], s) *
	   (*lagrange_)( index[2], t) );
}

//------------------------------------------------------------------
double BrickShape::dualShape(const NodeIndex& index, 
                            double r, double s, double t ) const
{
  throw EXC(CalcException) << "Dual BrickShape not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
double BrickShape::derivativeShape(unsigned derivative,
				   const NodeIndex& index,
				   double r, double s, double t) const
{
  double derived_value = 0.;
  switch( derivative )
    {
    case 0:
      derived_value = (
		       (*lagrange_).derivative( index[0], r) *
		       (*lagrange_)( index[1], s) *
		       (*lagrange_)( index[2], t)
		       );
	break;
    case 1:
      derived_value = (
		       (*lagrange_)( index[0], r) *
		       (*lagrange_).derivative( index[1], s) *
		       (*lagrange_)( index[2], t)
		       );
      break;
    case 2:
      derived_value = (
		       (*lagrange_)( index[0], r) *
		       (*lagrange_)( index[1], s) *
		       (*lagrange_).derivative( index[2], t)
		       );
      break;
    }

  return( derived_value );
}

//------------------------------------------------------------------
double BrickShape::secondDerivativeShape(unsigned derivative1,
					 unsigned derivative2,
					 const NodeIndex& index,
					 double r, double s, double t) const
{
  throw EXC(CalcException) << "BrickShape: secondDerivativeShape is not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
void BrickShape::calcCoordinates(unsigned order)
{
  coordinates_[0] = -1.;

  for( unsigned count = 1;
       count < (order+1);
       ++count )
    {
      coordinates_[count] = coordinates_[0] + 2./((double)order) * count;
    }
}

//------------------------------------------------------------------
/** 
    @todo Has to be implemented 
*/
Shape::LocalNumberVector* BrickShape::getEdgesOnSurface(unsigned local_surface_number) const
{
  LocalNumberVector* num_vector = new LocalNumberVector;
  return( num_vector );
}

//------------------------------------------------------------------
/** 
    @todo Has to be implemented 
*/
Shape::LocalNumberVector* BrickShape::getNodesOnEdge(unsigned local_edge_number) const
{
  LocalNumberVector* num_vector = new LocalNumberVector;
  return( num_vector );
}
