/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include "triangle_shape.h"

//------------------------------------------------------------------
TriangleShape::TriangleShape(unsigned order) :
  Shape(2,IDObject::TRIANGLE),
  area_coordinates_(new double[order + 1]),
  lagrange_(0)
{
  calcAreaCoordinates(order);
  lagrange_ = new Lagrange(order, area_coordinates_);

  createNodeIndex();
}

//------------------------------------------------------------------
/**
   @todo works only for order = 1,2 - would be nice to find an algorithm
   for arbitrarily shape order
 */
void TriangleShape::createNodeIndex()
{
  for( unsigned count = 0 ; count < getNodeAmount() ; ++count )
    {
      switch( count )
	{
	case 0:
	  node_index_vector_.push_back( new NodeIndex(lagrange_ -> getOrder(),0,0) );
	  break;
	case 1:
	  node_index_vector_.push_back( new NodeIndex(0,lagrange_ -> getOrder(),0) );
	  break;
	case 2:
	  node_index_vector_.push_back( new NodeIndex(0,0,lagrange_ -> getOrder()) );
	  break;
	case 3:
	  node_index_vector_.push_back( new NodeIndex(1,1,0) );
	  break;
	case 4:
	  node_index_vector_.push_back( new NodeIndex(0,1,1) );
	  break;
	case 5:
	  node_index_vector_.push_back( new NodeIndex(1,0,1) );
	  break;
	default:
	  SOOFEA_ASSERT( count < 6 );
	  break;
	}
    }
}


//------------------------------------------------------------------
TriangleShape::~TriangleShape()
{
  delete[] area_coordinates_;

  if( lagrange_ )
    delete lagrange_;
}

//------------------------------------------------------------------
void TriangleShape::calcAreaCoordinates(unsigned order)
{
  unsigned count;
  for( count = 0 ; count <= order ; ++count )
    {
      area_coordinates_[count] = 1. / order * count;
    }
}

//------------------------------------------------------------------
double TriangleShape::shape(const NodeIndex& node_index,
			    double r, double s, double t) const
{
  double L_1 = r;
  double L_2 = s;
  double L_3 = 1.-(r + s);
  unsigned K = lagrange_->getOrder() - (node_index[0]+node_index[1]);

  return( (*lagrange_)(node_index[0],node_index[0],L_1) *
	  (*lagrange_)(node_index[1],node_index[1],L_2) *
	  (*lagrange_)(K,K,L_3) );
}

//------------------------------------------------------------------
double TriangleShape::dualShape(const NodeIndex& index, 
                                double r, double s, double t ) const
{
  throw EXC(CalcException) << "Dual TriangleShape not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
double TriangleShape::derivativeShapeL(unsigned derivative,
				       unsigned I, unsigned J,
				       double L_1, double L_2) const
{
  unsigned derivative_index = 0;
  double L_3 = 1. - (L_1 + L_2);
  unsigned K = lagrange_->getOrder() - (I+J);

  double psi = 1.;
  double derivative_coordinate = 0.;

  switch( derivative )
    {
    case 0:
      derivative_index = I;
      derivative_coordinate = L_1;
      psi = (*lagrange_)(J,J,L_2) * (*lagrange_)(K,K,L_3);
      break;
    case 1:
      derivative_index = J;
      derivative_coordinate = L_2;
      psi = (*lagrange_)(I,I,L_1) * (*lagrange_)(K,K,L_3);
      break;
    case 2:
      derivative_index = K;
      derivative_coordinate = L_3;
      psi = (*lagrange_)(I,I,L_1) * (*lagrange_)(J,J,L_2);
      break;
    }

  return( psi * (lagrange_ -> derivative(derivative_index,derivative_index,derivative_coordinate)) );
}

//------------------------------------------------------------------
double TriangleShape::derivativeShape(unsigned derivative,
				      const NodeIndex& index,
				      double r, double s, double t) const
{
  unsigned derivative_index = 0;
  unsigned non_derivative_index = 0;
  double derivative_coordinate = 0.;
  double non_derivative_coordinate = 0.;

  unsigned K = lagrange_->getOrder() - (index[0]+index[1]);

  switch( derivative )
    {
    case 0:
      derivative_index = index[0];
      non_derivative_index = index[1];
      derivative_coordinate = r;
      non_derivative_coordinate = s;
      break;
    case 1:
      derivative_index = index[1];
      non_derivative_index = index[0];
      derivative_coordinate = s;
      non_derivative_coordinate = r;
      break;
    }
  return( (*lagrange_)(non_derivative_index,non_derivative_index,non_derivative_coordinate) *
	  ( lagrange_ -> derivative(derivative_index,derivative_index,derivative_coordinate) *
	    (*lagrange_)(K,K, 1.-(r+s) ) -
	    (*lagrange_)(derivative_index,derivative_index,derivative_coordinate) *
	    lagrange_ -> derivative(K,K, 1.-(r + s) ) ) );
}

//------------------------------------------------------------------
double TriangleShape::secondDerivativeShape(unsigned derivative1,
					unsigned derivative2,
					const NodeIndex& index,
					double r, double s, double t) const
{
  throw EXC(CalcException) << "TriangleShape: secondDerivativeShape is not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
Shape::LocalNumberVector* TriangleShape::getEdgesOnSurface(unsigned local_surface_number) const
{
  SOOFEA_ASSERT(dimension_ > 2);
  return( 0 );
}

//------------------------------------------------------------------
/**
   @todo works only for order = 1,2 - would be nice to find an algorithm
   for arbitrarily shape order
 */
Shape::LocalNumberVector* TriangleShape::getNodesOnEdge(unsigned local_edge_number) const
{
  LocalNumberVector* num_vector = new LocalNumberVector;

  num_vector -> push_back(0 + local_edge_number);
  if( getOrder() == 2 )
      num_vector -> push_back(3 + local_edge_number);
  num_vector -> push_back( (1 + local_edge_number) % 3 );

  return( num_vector );
}
