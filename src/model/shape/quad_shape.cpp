/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "quad_shape.h"

//------------------------------------------------------------------
QuadShape::QuadShape(unsigned order) :
  Shape(2,IDObject::QUAD),
  coordinates_(new double[order + 1]),
  lagrange_(0)
{
  calcCoordinates(order);
  lagrange_ = new Lagrange(order, coordinates_);

  createNodeIndex();
}

//------------------------------------------------------------------
QuadShape::~QuadShape()
{
  if( coordinates_ )
    delete[] coordinates_;

  if( lagrange_ )
    delete lagrange_;
}

//------------------------------------------------------------------
/**
   @todo works only for order = 1,2 - would be nice to find an algorithm
   for arbitrarily shape order
 */
void QuadShape::createNodeIndex()
{
  for( unsigned count = 0 ; count < getNodeAmount() ; ++count )
    {
      switch( count )
	{
	case 0:
	  node_index_vector_.push_back( new NodeIndex(0,0) );
	  break;
	case 1:
	  node_index_vector_.push_back( new NodeIndex(lagrange_->getOrder(),0) );
	  break;
	case 2:
	  node_index_vector_.push_back( new NodeIndex(lagrange_->getOrder(),lagrange_->getOrder()) );
	  break;
	case 3:
	  node_index_vector_.push_back( new NodeIndex(0,lagrange_->getOrder()) );
	  break;
	case 4:
	  node_index_vector_.push_back( new NodeIndex(1,0) );
	  break;
	case 5:
	  node_index_vector_.push_back( new NodeIndex(lagrange_->getOrder(),1) );
	  break;
	case 6:
	  node_index_vector_.push_back( new NodeIndex(1,lagrange_->getOrder()) );
	  break;
	case 7:
	  node_index_vector_.push_back( new NodeIndex(0,1) );
	  break;
	case 8:
	  node_index_vector_.push_back( new NodeIndex(1,1) );
	  break;
	default:
	  SOOFEA_ASSERT( count < 9 );
	  break;
	}
    }
}

//------------------------------------------------------------------
double QuadShape::shape(const NodeIndex& index,
			double r, double s, double t) const
{
  return ( (*lagrange_)( index[0], r) *
	   (*lagrange_)( index[1], s) );
}

//------------------------------------------------------------------
double QuadShape::dualShape(const NodeIndex& index, 
                            double r, double s, double t ) const
{
  throw EXC(CalcException) << "Dual QuadShape not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
double QuadShape::derivativeShape(unsigned derivative,
				  const NodeIndex& index,
				  double r, double s, double t) const
{
  double derived_value = 0.;

  switch( derivative )
    {
    case 0:
      derived_value = ( (*lagrange_).derivative( index[0], r) *
			(*lagrange_)( index[1], s) );
	break;
    case 1:
      derived_value = ( (*lagrange_)( index[0], r) *
			(*lagrange_).derivative( lagrange_->getOrder() , index[1], s) );
      break;
    }

  return( derived_value );
}

//------------------------------------------------------------------
double QuadShape::secondDerivativeShape(unsigned derivative1,
					unsigned derivative2,
					const NodeIndex& index,
					double r, double s, double t) const
{
  throw EXC(CalcException) << "QuadShape: secondDerivativeShape is not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
void QuadShape::calcCoordinates(unsigned order)
{
  coordinates_[0] = -1.;

  for( unsigned count = 1;
       count < (order+1);
       ++count )
    {
      coordinates_[count] = coordinates_[0] + 2./((double)order) * count;
    }
}

//------------------------------------------------------------------
Shape::LocalNumberVector* QuadShape::getEdgesOnSurface(unsigned local_surface_number) const
{
  SOOFEA_ASSERT(dimension_ > 2);
  return( 0 );
}

//------------------------------------------------------------------
/**
   @todo works only for order = 1,2 - would be nice to find an algorithm
   for arbitrarily shape order
 */
Shape::LocalNumberVector* QuadShape::getNodesOnEdge(unsigned local_edge_number) const
{
  LocalNumberVector* num_vector = new LocalNumberVector;
  num_vector -> push_back(0 + local_edge_number);
  if( getOrder() == 2 )
      num_vector -> push_back(4 + local_edge_number);
  num_vector -> push_back( (1 + local_edge_number) % 4 );
  return( num_vector );
}
