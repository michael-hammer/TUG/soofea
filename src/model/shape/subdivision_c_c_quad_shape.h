#ifndef subdivision_c_c_quad_shape_h___
#define subdivision_c_c_quad_shape_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * ------------------------------------------------------------------*/

#include "shape.h"

//------------------------------------------------------------------
/**
   <pre>
   standard element with 4 nodes in both directions
   node_amount_r_ = 4
   node_amount_s_ = 4
   
                 s (-1,...,+1)
                 ^
                 |
                 |
   (0,3)---(1,3)---(2,3)---(3,3)
     |       |   |   |       |
     |       |   |   |       |
     |       |   |   |       |
   (0,2)---(1,2)---(2,2)---(3,2)
     |       |ooo|ooo|       |
     |       |ooo----|-------|------> r (-1,...,+1)
     |       |ooooooo|       |
   (0,1)---(1,1)---(2,1)---(3,1)
     |       |       |       |
     |       |       |       |
     |       |       |       |
   (0,0)---(1,0)---(2,0)---(3,0)


   boundary element with 3 nodes in r direction and 4 nodes in s direction
   node_amount_r_ = 3
   node_amount_s_ = 4

        s (-1,...,+1)
         ^
         |
         |
   (0,3)---(1,3)---(2,3)
    ||   |   |       |
    ||   |   |       |
    ||   |   |       |
   (0,2)---(1,2)---(2,2)
    ||ooo|ooo|       |
    ||ooo----|-------|------> r (-1,...,+1)
    ||ooooooo|       |
   (0,1)---(1,1)---(2,1)
    ||       |       |
    ||       |       |
    ||       |       | 
   (0,0)---(1,0)---(2,0)


   boundary element with 4 nodes in r direction and 3 nodes in s direction
   node_amount_r_ = 4
   node_amount_s_ = 3

                 s (-1,...,+1)
                 ^
                 |
                 |
   (0,2)---(1,2)---(2,2)---(3,2)
     |       |   |   |       |
     |       |   |   |       |
     |       |   |   |       |
   (0,1)---(1,1)---(2,1)---(3,1)
     |       |ooo|ooo|       |
     |       |ooo----|-------|------> r (-1,...,+1)
     |       |ooooooo|       |
   (0,0)===(1,0)===(2,0)===(3,0)


   corner element with 3 nodes in r direction and 3 nodes in s direction
   node_amount_r_ = 3
   node_amount_s_ = 3

        s (-1,...,+1)
         ^
         |
         |
   (0,2)---(1,2)---(2,2)
    ||   |   |       |
    ||   |   |       |
    ||   |   |       |
   (0,1)---(1,1)---(2,1)
    ||ooo|ooo|       |
    ||ooo----|-------|------> r (-1,...,+1)
    ||ooooooo|       |
   (0,0)===(1,0)===(2,0)
    
</pre>
*/

class SubdivisionCCQuadShape : 
public Shape
{
public:
  SubdivisionCCQuadShape(IDObject::ID type);

  virtual ~SubdivisionCCQuadShape();
  
  virtual double shape(const NodeIndex& index, 
		       double r = 0., double s = 0., double t = 0.) const;

  virtual double dualShape(const NodeIndex& index, 
			   double r = 0., double s = 0., double t = 0.) const;

  virtual double derivativeShape(unsigned derivative,
				 const NodeIndex& index,
				 double r = 0., double s = 0., double t = 0.) const;
  
  virtual double secondDerivativeShape(unsigned derivative1,
				       unsigned derivative2,
				       const NodeIndex& index,
				       double r = 0., double s = 0., double t = 0.) const;

  virtual inline unsigned getOrder() const
  { return( 3 ); }

  virtual inline unsigned getNodeAmount() const
  { return( node_amount_r_*node_amount_s_ ); }

  virtual LocalNumberVector* getEdgesOnSurface(unsigned local_surface_number) const;

  virtual LocalNumberVector* getNodesOnEdge(unsigned local_edge_number) const;

  virtual inline unsigned getEdgeAmount() const
  { return( 4 ); }

  virtual inline unsigned getSurfaceAmount() const
  { return( 0 ); }
protected:
  virtual void createNodeIndex();
  CubicPolynomial* cubic_polynomial_;
  unsigned node_amount_r_;
  unsigned node_amount_s_;
};

#endif // subdivision_c_c_quad_shape_h___
