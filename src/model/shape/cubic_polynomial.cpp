/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * ------------------------------------------------------------------*/

#include "cubic_polynomial.h"

//------------------------------------------------------------------
CubicPolynomial::CubicPolynomial()
{
}

//------------------------------------------------------------------
double CubicPolynomial::operator()(unsigned node_amount, unsigned index, double var)
{
  double xi = 0.5*( var + 1.0 );
  double value = 1.0;

  switch( node_amount )
    {
    case 3:
      switch( index )
	{
	case 0:
	  value = 1.0 - xi + 1.0/6.0*xi*xi*xi;
	  break;
	case 1:
	  value = xi - 1.0/3.0*xi*xi*xi;
	  break;
	case 2:
	  value = 1.0/6.0*xi*xi*xi;
	  break;
	default:
	  throw EXC(CalcException) << "CubicPolynomial::() -> index range 0,..,2 for node amount 3!"
				   << Exception::endl;
	  break;
	}
      break;
    case 4:
      switch( index )
	{
	case 0:
	  value = 1.0/6.0*( 1.0 - 3.0*xi + 3.0*xi*xi - xi*xi*xi );
	  break;
	case 1:
	  value = 1.0/6.0*( 4.0 - 6.0*xi*xi + 3.0*xi*xi*xi );
	  break;
	case 2:
	  value = 1.0/6.0*( 1.0 + 3.0*xi + 3.0*xi*xi - 3.0*xi*xi*xi );
	  break;
	case 3:
	  value = 1.0/6.0*( xi*xi*xi );
	  break;
	default:
	  throw EXC(CalcException) << "CubicPolynomial::() -> index range 0,..,3 for node amount 4!"
				   << Exception::endl;
	  break;
	}
      break;
    default:
      throw EXC(CalcException) << "CubicPolynomial::() -> only node amount 3 or 4 are valid!"
			       << Exception::endl;
      break;
    }
  return( value );
}

//------------------------------------------------------------------
double CubicPolynomial::derivative(unsigned node_amount, unsigned index, double var)
{
  double xi = 0.5*( var + 1.0 );
  double value = 1.0;
  switch( node_amount )
    {
    case 3:
      switch( index )
	{
	case 0:
	  value = -1.0 + 1.0/2.0*xi*xi;
	  break;
	case 1:
	  value = 1.0 - xi*xi;
	  break;
	case 2:
	  value = 1.0/2.0*xi*xi;
	  break;
	default:
	  throw EXC(CalcException) << "CubicPolynomial::derivative -> index range 0,..,2 for node amount 3!"
				   << Exception::endl;
	  break;
	}
      break;
    case 4:
      switch( index )
	{
	case 0:
	  value = 1.0/6.0*( -3.0 + 6.0*xi - 3.0*xi*xi );
	  break;
	case 1:
	  value = 1.0/6.0*( -12.0*xi + 9.0*xi*xi );
	  break;
	case 2:
	  value = 1.0/6.0*( 3.0 + 6.0*xi - 9.0*xi*xi );
	  break;
	case 3:
	  value = 1.0/6.0*( 3.0*xi*xi );
	  break;
	default:
	  throw EXC(CalcException) << "CubicPolynomial::derivative -> index range 0,..,3 for node amount 4!"
				   << Exception::endl;
	  break;
	}
      break;
    default:
      throw EXC(CalcException) << "CubicPolynomial::derivative -> only node amount 3 or 4 are valid!"
			       << Exception::endl;
      break;
    } 
  return( value );
}

//------------------------------------------------------------------
double CubicPolynomial::secondDerivative(unsigned node_amount, unsigned index, double var)
{
  double xi = 0.5*( var + 1.0 );
  double value = 1.0;
  switch( node_amount )
    {
    case 3:
      switch( index )
	{
	case 0:
	  value = xi;
	  break;
	case 1:
	  value = -2.0*xi;
	  break;
	case 2:
	  value = xi;
	  break;
	default:
	  throw EXC(CalcException) << "CubicPolynomial::derivative -> index range 0,..,2 for node amount 3!"
				   << Exception::endl;
	  break;
	}
      break;
    case 4:
      switch( index )
	{
	case 0:
	  value = 1.0/6.0*( 6.0 - 6.0*xi );
	  break;
	case 1:
	  value = 1.0/6.0*( -12.0 + 18.0*xi );
	  break;
	case 2:
	  value = 1.0/6.0*( 6.0 - 18.0*xi );
	  break;
	case 3:
	  value = 1.0/6.0*( 6.0*xi );
	  break;
	default:
	  throw EXC(CalcException) << "CubicPolynomial::secondDerivative -> index range 0,..,3 for node amount 4!"
				   << Exception::endl;
	  break;
	}
      break;
    default:
      throw EXC(CalcException) << "CubicPolynomial::secondDerivative -> only node amount 3 or 4 are valid!"
			       << Exception::endl;
      break;
    }
  return( value );
}
