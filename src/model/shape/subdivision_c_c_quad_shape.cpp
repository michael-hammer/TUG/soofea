/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * ------------------------------------------------------------------*/

#include "subdivision_c_c_quad_shape.h"

//------------------------------------------------------------------
SubdivisionCCQuadShape::SubdivisionCCQuadShape(IDObject::ID type) :
  Shape(2,type),
  cubic_polynomial_(0),
  node_amount_r_(0),
  node_amount_s_(0)
{
  switch( type ) {
  case IDObject::SUBDIVISION_CC:
    node_amount_r_ = 4;
    node_amount_s_ = 4;
    break;
  case IDObject::SUBDIVISION_CC_BOUNDARY:
    node_amount_r_ = 4;
    node_amount_s_ = 3;
    break;
  case IDObject::SUBDIVISION_CC_CORNER:
    node_amount_r_ = 3;
    node_amount_s_ = 3;
    break;
  default:
    std::cout << "Noooooooooooooo!" << std::endl;
  }

  cubic_polynomial_ = new CubicPolynomial();
  createNodeIndex();
}

//------------------------------------------------------------------
SubdivisionCCQuadShape::~SubdivisionCCQuadShape()
{
  if( cubic_polynomial_ )
    delete cubic_polynomial_;
}

//------------------------------------------------------------------
void SubdivisionCCQuadShape::createNodeIndex()
{
  for( unsigned row_count = 0; row_count < node_amount_s_; ++row_count )
    for( unsigned col_count = 0; col_count < node_amount_r_; ++col_count )
      node_index_vector_.push_back( new NodeIndex(col_count,row_count) );
}

//------------------------------------------------------------------
double SubdivisionCCQuadShape::shape(const NodeIndex& index,
				double r, double s, double t) const
{
  return ( (*cubic_polynomial_)( node_amount_r_, index[0], r) *
	   (*cubic_polynomial_)( node_amount_s_, index[1], s) );
}

//------------------------------------------------------------------
double SubdivisionCCQuadShape::dualShape(const NodeIndex& index, 
				    double r, double s, double t ) const
{
  throw EXC(CalcException) << "SubdivisionCCQuadShape: dualShape is not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
double SubdivisionCCQuadShape::derivativeShape(unsigned derivative,
					  const NodeIndex& index,
					  double r, double s, double t) const
{
  double first_derivative_value = 0.;
  
  switch( derivative )
    {
    case 0:
      first_derivative_value = ( (*cubic_polynomial_).derivative( node_amount_r_, index[0], r) *
				 (*cubic_polynomial_)( node_amount_s_, index[1], s) );
	break;
    case 1:
      first_derivative_value = ( (*cubic_polynomial_)( node_amount_r_, index[0], r) *
				 (*cubic_polynomial_).derivative( node_amount_s_, index[1], s) );
      break;
    }

  return( first_derivative_value );
}

//------------------------------------------------------------------
double SubdivisionCCQuadShape::secondDerivativeShape(unsigned derivative1,
						unsigned derivative2,
						const NodeIndex& index,
						double r, double s, double t) const
{
  double second_derivative_value = 0.0;
  switch( derivative1 )
    {
    case 0:
      if( derivative2 == 0 )
	second_derivative_value = ( (*cubic_polynomial_).secondDerivative( node_amount_r_, index[0], r) *
				    (*cubic_polynomial_)( node_amount_s_, index[1], s) );
      else if( derivative2 == 1 )
	second_derivative_value = ( (*cubic_polynomial_).derivative( node_amount_r_, index[0], r) *
				    (*cubic_polynomial_).derivative( node_amount_s_, index[1], s) );
      else
	throw EXC(CalcException) << "SubdivisionCCQuadShape: secondDerivativeShape -> derivative2 range 0,1!"
				 << Exception::endl;
      break;
    case 1:
      if( derivative2 == 0 )
	second_derivative_value = ( (*cubic_polynomial_).derivative( node_amount_r_, index[0], r) *
				    (*cubic_polynomial_).derivative( node_amount_s_, index[1], s) );
      else if( derivative2 == 1 )
	second_derivative_value = ( (*cubic_polynomial_)( node_amount_r_, index[0], r) *
				    (*cubic_polynomial_).secondDerivative( node_amount_s_, index[1], s) );
      else
	throw EXC(CalcException) << "SubdivisionCCQuadShape: secondDerivativeShape -> derivative2 range 0,1!"
				 << Exception::endl;
      break;
    default:
      throw EXC(CalcException) << "SubdivisionCCQuadShape: secondDerivativeShape -> derivative1 range 0,1!"
			       << Exception::endl;
      break;
    }
  return( second_derivative_value );
}

//------------------------------------------------------------------
Shape::LocalNumberVector* SubdivisionCCQuadShape::getEdgesOnSurface(unsigned local_surface_number) const
{
  throw EXC(CalcException) << "SubdivisionCCQuadShape: getEdgesOnSurface is not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
Shape::LocalNumberVector* SubdivisionCCQuadShape::getNodesOnEdge(unsigned local_edge_number) const
{
  throw EXC(CalcException) << "SubdivisionCCQuadShape: getNodesOnEdge is not implemented!" << Exception::endl;
}
