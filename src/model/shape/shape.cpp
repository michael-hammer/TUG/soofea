/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "shape.h"

//------------------------------------------------------------------
Shape::~Shape()
{
  for( NodeIndexVector::iterator iter = node_index_vector_.begin() ;
       iter != node_index_vector_.end() ; 
       ++iter )
    delete *iter;
}

//------------------------------------------------------------------
const NodeIndex& Shape::getNodeIndex(unsigned local_node_number) const
{
  SOOFEA_ASSERT( local_node_number < getNodeAmount() );
  return( *(node_index_vector_.at(local_node_number)) );
}

//------------------------------------------------------------------
bz::Array<double,1>* Shape::shapeTensor(double r, double s, double t) const
{
  bz::Array<double,1>* shape_tensor = 
    new bz::Array<double,1>(getNodeAmount());

  for( int row_count = 0 ; row_count < static_cast<int>(getNodeAmount()) ; ++row_count )
    (*shape_tensor)(row_count) = 
      shape(getNodeIndex(row_count),
	    r, s, t);

  return( shape_tensor );
}

//------------------------------------------------------------------
bz::Array<double,1>* Shape::dualShapeTensor(double r, double s, double t) const
{
  bz::Array<double,1>* dual_shape_tensor = 
    new bz::Array<double,1>(getNodeAmount());

  for( int row_count = 0 ; row_count < static_cast<int>(getNodeAmount()) ; ++row_count )
    (*dual_shape_tensor)(row_count) = 
      dualShape(getNodeIndex(row_count),
                r, s, t);

  return( dual_shape_tensor );
}

//------------------------------------------------------------------
bz::Array<double,2>* Shape::derivativeShapeTensor(double r, double s, double t) const
{
  bz::Array<double,2>* derived_shape_tensor = 
    new bz::Array<double,2>(getNodeAmount(),getDimension());

  for( int node_count = 0 ; node_count < static_cast<int>(getNodeAmount()) ; ++node_count )
    for( int der_count = 0 ; der_count < static_cast<int>(getDimension()) ; ++der_count )
      (*derived_shape_tensor)(node_count,der_count) = 
	derivativeShape(der_count,
			getNodeIndex(node_count),
			r, s, t);

  return( derived_shape_tensor );
}

//------------------------------------------------------------------
bz::Array<double,2>* Shape::secondDerivativeShapeTensor(double r, double s, double t) const
{
  bz::Array<double,2>* second_derivative_shape_tensor = 
    new bz::Array<double,2>(getNodeAmount(),2*getDimension());

  for( int row_count = 0 ; row_count < static_cast<int>(getNodeAmount()) ; ++row_count )
    {
      int array_count = 0;
      for( int first_count = 0 ; first_count < static_cast<int>(getDimension()) ; ++first_count )
	for( int second_count = 0 ; second_count < static_cast<int>(getDimension()) ; ++second_count )
	  {
	    (*second_derivative_shape_tensor)(row_count,array_count) = 
	      secondDerivativeShape(first_count,
				    second_count,
				    getNodeIndex(row_count),
				    r, s, t);
	    ++array_count;
	  }
    }

  return( second_derivative_shape_tensor );
}
