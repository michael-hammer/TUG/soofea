#ifndef brick_shape_h___
#define brick_shape_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 *         Manfred Ulz
 * ------------------------------------------------------------------*/

#include "shape.h"

//------------------------------------------------------------------
/**
   Provides the shape "functions" for finite Brick Element. The 
   order (m) of the trial functions may be arbitrary. In the following
   drawing you can see how a node on the element can be identified with
   the help of 3 indices.
<pre>
                    t
                    | 
                    
                    
                 (0,0,m)               (0,m,m)
                    0----0----0----....---0
                   /|                    /|
                  / |	                / |
          (1,0,m)0  0(0,0,m-1)  (1,m,m)0  0(0,m,m-1)
                /   |	              /   |
               /    |	             /    |
       (2,0,m)0     0	            0     0(0,m,m-2)
             /      |	           /      |
            .       .	          .       .
           .        .	         .        .
          /(m,1,m)  |	        /         |
  (m,0,m)0----0----0----....---0(m,m,m)   |
         |          |          |          |(0,m,0)
         |   (0,0,0)0----0----0|---....---0        -- s
         0         /	       0         / 
         |        / 	       |        / 
         |       0  	       |       0  
         0      /   	       0      /   
         |     /    	       |     /    
         .    0     	       .    0     
         .   /      	       .   /      
         |  .       	       |  .       
         | .                   | .        
         |/                    |/         
  (m,0,0)0----0----0----....---0(m,m,0)
        
       
      /
     r     
</pre>
    The coordinates are definied as follows
<pre>
             t
             ^          
             |
          0--|-----0           
         /:  |    /|           
        / :      / |           
       /  :     /  |           
      0--------0   |           
      |   0....|.--0----> s           
      |  .     |  /          
      | . /    | / 
      |. /     |/              
      0-/------0 
       /
      r
</pre>
 */
class BrickShape : 
  public Shape
{
public:
  /**
     Constructs Shape class for an element with given order

     @param order of the Interpolation polynom
   */
  BrickShape(unsigned order);

  virtual ~BrickShape();

  /**
     Returns for a given node (I,J,K) (see scetch at BrickShape
     for explanation of the node notation) the value of the shape 
     function at a given point (by rst coordinates) in the reference 
     Brick. The node is the point in the Brick where the value of
     the shape function is 1.

     @param I first node index
     @param J second node index
     @param K third node index
     @param r first coordinate
     @param s second coordinate
     @param t third coordinate
   */
  virtual double shape(const NodeIndex& index, 
		       double r, double s = 0., double t = 0.) const;

  virtual double dualShape(const NodeIndex& index, 
			   double r, double s = 0., double t = 0.) const;

  /**
     Returns for a given node (I,J,K) (see scetch at BrickShape
     for explanation of the node notation) the value of derivation 
     of shape function.

     \f[ \frac{\partial N(r,s,t)}{\partial r_i} \f]

     The derivative is evaluated at a given point (by rst coordinates)
     in the reference triangle. The node is the point in the Brick where 
     the value of the shape function is 1.
  
     @param derivative \f$i\f$
     @param I first node index
     @param J second node index
     @param K third node index
     @param r first coordinate
     @param s second coordinate
     @param t third coordinate
  */
  virtual double derivativeShape(unsigned derivative,
			 const NodeIndex& index,
			 double r, double s, double t) const;

  virtual double secondDerivativeShape(unsigned derivative1,
				       unsigned derivative2,
				       const NodeIndex& index,
				       double r = 0., double s = 0., double t = 0.) const;


  /**
     @return The order of the Interpolation polynom
   */
  virtual inline unsigned getOrder() const
  { return( lagrange_ -> getOrder() ); }

  /**
   */
  inline unsigned getNodeAmount() const
  { return( (lagrange_ -> getOrder() + 1)*
	    (lagrange_ -> getOrder() + 1)*
	    (lagrange_ -> getOrder() + 1) ); }

  /**
   */
  virtual LocalNumberVector* getEdgesOnSurface(unsigned local_surface_number) const;

  /**
   */
  virtual LocalNumberVector* getNodesOnEdge(unsigned local_edge_number) const;

  /**
   */
  virtual inline unsigned getEdgeAmount() const
  { return( 12 ); }

  /**
   */
  virtual inline unsigned getSurfaceAmount() const
  { return( 6 ); }
 protected:
  /**
     Calculates the coordinates of each node of the elment in
     the reference configuration.
   */
  virtual void calcCoordinates(unsigned order);

  /**
   */
  virtual void createNodeIndex();

  double* coordinates_;
  Lagrange* lagrange_;
};

#endif // brick_shape_h___
