/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include "linear_shape.h"

//------------------------------------------------------------------
LinearShape::LinearShape(unsigned order) :
  Shape(1,IDObject::LINEAR),
  coordinates_(new double[order + 1]),
  lagrange_(0)
{
  calcCoordinates(order);
  lagrange_ = new Lagrange(order, coordinates_);

  createNodeIndex();
}

//------------------------------------------------------------------
LinearShape::~LinearShape()
{
  delete[] coordinates_;

  if( lagrange_ )
    delete lagrange_;
}

//------------------------------------------------------------------
void LinearShape::createNodeIndex()
{
  for( unsigned count = 0 ; count < getNodeAmount() ; ++count )
    {
      node_index_vector_.push_back( new NodeIndex(count) );
    }
}

//------------------------------------------------------------------
void LinearShape::calcCoordinates(unsigned order)
{
  unsigned count = 0;
  coordinates_[0] = -1.;

  for( count = 1;
       count < (order+1);
       ++count )
    {
      coordinates_[count] = coordinates_[0] + 2./((double)order) * count; 
    }
}

//------------------------------------------------------------------
double LinearShape::shape(const NodeIndex& node_index, 
			  double r, double s, double t) const
{
  return( (*lagrange_)(node_index[0],r) );
}

//------------------------------------------------------------------
double LinearShape::dualShape(const NodeIndex& node_index, 
                              double r, double s, double t) const
{
  return( shape( node_index, r , s , t ) );
//
// FIXME: USE Dual shape functions!
//
//  if( getOrder() > 1 )
//    throw EXC(CalcException) << "Dual LinearShape only defined for linear case" << Exception::endl;
//
//  switch( node_index[0] ) {
//  case 0:
//    return( ( 1. - 3.*r ) / 2. );
//    break;
//  case 1:
//    return( ( 1. + 3.*r ) / 2. );
//    break;
//  default:
//    throw EXC(CalcException) << "Dual LinearShape calculation for node :"
//                             << node_index[0] << " makes no sense at all" << Exception::endl;
//  }
}

//------------------------------------------------------------------
double LinearShape::derivativeShape(unsigned derivative,
				    const NodeIndex& index,
				    double r, double s, double t) const
{
  return( lagrange_ -> derivative(index[0], r) );
}

//------------------------------------------------------------------
double LinearShape::secondDerivativeShape(unsigned derivative1,
					  unsigned derivative2,
					  const NodeIndex& index,
					  double r, double s, double t) const
{
  throw EXC(CalcException) << "LinearShape: secondDerivativeShape is not implemented!" << Exception::endl;
}

//------------------------------------------------------------------
Shape::LocalNumberVector* LinearShape::getEdgesOnSurface(unsigned local_surface_number) const
{
  SOOFEA_ASSERT(dimension_ > 2);
  return( 0 );
}

//------------------------------------------------------------------
Shape::LocalNumberVector* LinearShape::getNodesOnEdge(unsigned local_edge_number) const
{
  SOOFEA_ASSERT(dimension_ > 1);
  return( 0 );
}
