/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "element.h"

//------------------------------------------------------------------
void Element::prettyPrint(bool print_edge) const
{
  std::cout << "| Element: " << number_ << std::endl;
  std::cout << "| " << std::endl;
  std::cout << "| element type : " << type_number_ << std::endl;
  NodeContainer::prettyPrint();
  std::cout << "|  ---" << std::endl;
  std::cout << "| material : " << material_number_ << std::endl;
  if( print_edge ) {
    std::cout << "|  ---" << std::endl;
    for( std::vector<Edge*>::const_iterator iter = edge_vector_.begin(); 
         iter != edge_vector_.end() ; 
         ++iter) {
      std::cout << "|" << std::endl;
      (*iter) -> prettyPrint();
    }
  }
}

//------------------------------------------------------------------
void Element::createIntegrationPoints()
{
  bz::Array<double,2>* lagr_node_tensor = getLagrangianPointTensor();
  createIPs( *lagr_node_tensor, getType() , node_vector_.front()->getLagrangianPoint().getCoordSys() );
  delete lagr_node_tensor;
}

//------------------------------------------------------------------
void Element::updateIntegrationPoints(const TimeStamp& time_stamp)
{
  bz::Array<double,2>* eul_node_tensor = getEulerianPointTensor();
  updateIPs( time_stamp, *eul_node_tensor, getType() , dynamic_cast<PointAggregate*>(node_vector_.front())->getEulerianPoint().getCoordSys() );
  delete eul_node_tensor;
}

//------------------------------------------------------------------
Edge* Element::next(Edge* act_edge, int& act_local_node_number)
{
  if( !act_edge ) {
    if( act_local_node_number < 0 ) {
      throw EXC(CalcException) << "You really want to increment on an end iterator?" << Exception::endl;
    } else {
      act_local_node_number = 0;
      return( edge_vector_[act_local_node_number] );
    }
  } else {
    if( (unsigned)(++act_local_node_number) < getType().getShape().getEdgeAmount() )
      return( edge_vector_[act_local_node_number] );
    else {
      act_local_node_number = -1;
      return 0;
    }
  }
}

//------------------------------------------------------------------
Iterator<Edge> Element::begin( Edge * )
{
  if( edge_vector_.empty() )
    return( Aggregate<Edge>::end() );
  else
    return( Iterator<Edge>( this , edge_vector_[0] , 0 ) );
}
