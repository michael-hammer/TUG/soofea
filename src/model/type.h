#ifndef type_h___
#define type_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "math/num_int_io.h"
#include "base/id_object.h"
#include "base/numbered_object.h"
#include "base/parser_base.h"
#include "base/dynamic_property.h"
#include "shape/shape.h"

class Impl;

//------------------------------------------------------------------
/**
   Base class for all the ComponentTypes like EdgeType, SurfaceType
   and ElementType. They can be understood as configuration interface
   between Model and Implementation
 */
class Type :
  public NumberedObject,
  public DynamicProperty
{
public:
  virtual ~Type();

  /**
     @return the sum amount of integration points in the element.
   */
  inline unsigned getAmountOfIntPoints() const
  { return( int_point_vector_ -> size() ); }
  
  /**
     @return the Math::IntegrationPoint* vector which can be used for
     numerical integration or creation of IntegrationPoint in the element
   */
  inline std::vector<Math::IntegrationPoint*>* getMathIntegrationPointVector()
  { return( int_point_vector_ ); }

  /**
     @return the Shape of this ElementType
   */
  inline const Shape& getShape() const
  {return(*shape_);}

  inline std::string getAmountOfIntPointsString() const
  { return( ParserBase::vectorToString( *amount_of_int_points_ ) ); }

  inline IDObject::ID getID() const
  {return( type_id_ );}

  inline void setImpl( Impl* impl )
  { impl_ = impl; }

  inline Impl& getImpl()
  { return( *impl_ ); };

  inline IDObject::ID getTarget() const
  { return( target_id_ );};

protected:
  Type(numbered type_number, 
       IDObject::ID type_id,
       IDObject::ID target_id,
       std::vector<int>* amount_of_int_points);

  std::vector<int>* amount_of_int_points_;

  /**
     interface has to implemented by the derivation to fill the
     Math::IntegrationPoint vector
   */
  void createMathIP( std::vector<int>* amount_of_int_points );

  /**
     This interface has to implemented by the derivation to create the
     apropriate Shape
   */
  virtual void createShape( unsigned shape_order, IDObject::ID shape_type_id ) = 0;

  void initiateMathIntPoints();

  std::vector<Math::IntegrationPoint*>* int_point_vector_;

  Shape* shape_;
  IDObject::ID type_id_;
  IDObject::ID target_id_;
  Impl* impl_;
};

#endif
