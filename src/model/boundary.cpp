/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "boundary.h"

//------------------------------------------------------------------
void Boundary::setTargets(std::vector<BoundaryComponent*>& target_vector)
{
  if( !target_vector_.empty() )
    target_vector_.clear();

  for( std::vector<BoundaryComponent*>::iterator iter = target_vector.begin();
       iter != target_vector.end();
       ++iter ) {
    target_vector_.push_back(*iter);
  }
}

//------------------------------------------------------------------
void Boundary::setNodes(std::set<Node*>& node_set)
{
  for( std::set<Node*>::iterator iter = node_set.begin();
       iter != node_set.end();
       ++iter ) {
    node_vector_.push_back(*iter);
  }
}
