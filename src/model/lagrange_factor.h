#ifndef lagrange_factor_h___
#define lagrange_factor_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "geometry/coord_sys.h"
#include "exceptions.h"

//------------------------------------------------------------------
class LagrangeFactor {
public:
  LagrangeFactor(double normal = 0., double r = 0., double s = 0.):
    normal_(normal),
    r_(r), s_(s) 
  {}

  LagrangeFactor( const LagrangeFactor& src );

  void set( double value , CoordSys::CoordID id );

  double get( CoordSys::CoordID id );

  LagrangeFactor& operator=( const LagrangeFactor& src );

  LagrangeFactor& operator=( const double src );
 
protected:
  double normal_;
  double r_;
  double s_;
};

#endif
