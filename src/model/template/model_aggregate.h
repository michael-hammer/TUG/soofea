#ifndef model_aggregate_h___
#define model_aggregate_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <map>

#include "base/template/aggregate.h"
#include "base/template/const_aggregate.h"

//------------------------------------------------------------------
template<class T>
class ModelAggregate :
  public Aggregate<T>,
  public ConstAggregate<T>
{
public:
  using Aggregate<T>::begin;

  ModelAggregate(std::map<numbered,T*>* map):
    map_(map) {}

  virtual ~ModelAggregate() {}

  size_t size() const {
    return( this->size(static_cast<const T*>(0)) );
  }

protected:
  friend class Iterator<T>;
  std::map<numbered,T*>* map_;

  virtual size_t size( const T* ) const
  { return( map_ -> size() ); }

  virtual T* next( T* act, int& act_index );
  virtual const T* next( const T* act, int& act_index ) const;
  virtual Iterator<T> begin( T* );
  virtual ConstIterator<T> begin( const T* ) const;

  using Aggregate<T>::get;
  virtual T* get( T* , int index );
  virtual const T* get( const T* , int index ) const;

  using Aggregate<T>::at;
  virtual T* at( T* , int index );
  virtual const T* at( const T* , int index ) const;
};

#include "model_aggregate.tcc"

#endif
