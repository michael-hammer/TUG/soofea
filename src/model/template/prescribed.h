#ifndef prescribed_h__
#define prescribed_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>

#include "geometry/vector.h"
#include "base/id_object.h"
#include "base/numbered_object.h"

//------------------------------------------------------------------
template<class T>
class Prescribed :
  public NumberedObject,
  public IDObject,
  public Aggregate<T>
{
public:  
  inline Prescribed(numbered prescribed_number,
                    numbered target_number,
                    IDObject::ID target,
                    IDObject::ID type,
                    std::vector<T*> *prescribed_vector ) :
    NumberedObject(prescribed_number),
    target_number_(target_number),
    target_(target),
    type_(type),
    prescribed_vector_(prescribed_vector)
  {}

  virtual ~Prescribed();
  
  inline IDObject::ID getTarget() const
  { return(target_); }

  inline IDObject::ID getType() const
  { return(type_); }

  inline numbered getTargetNumber() const 
  {return(target_number_);};

  T& getValue( CoordSys::CoordID coord_id );

protected:
  numbered target_number_;
  IDObject::ID target_;
  IDObject::ID type_;
  std::vector<T*> *prescribed_vector_;

  using Aggregate<T>::begin;
  virtual Iterator<T> begin( T* );
  using Aggregate<T>::next;
  virtual T* next( T* act, int& index);
  using Aggregate<T>::get;
  inline T* get( T*, int local_number) { return(prescribed_vector_->at(local_number)); }
  using Aggregate<T>::at;
  inline T* at( T*, int local_number) { return(prescribed_vector_->at(local_number)); }
};

#include "prescribed.tcc"

#endif
