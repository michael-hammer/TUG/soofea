/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "prescribed.h"

//------------------------------------------------------------------
template<class T>
Prescribed<T>::~Prescribed()
{
  if( prescribed_vector_ ) { 
    for( typename std::vector<T*>::iterator iter = prescribed_vector_ -> begin();
         iter != prescribed_vector_ -> end() ; 
         ++iter ) {
      delete *iter;
    }
    delete prescribed_vector_;
  }
}

//------------------------------------------------------------------
template<class T>
T& Prescribed<T>::getValue( CoordSys::CoordID coord_id )
{
  for( typename std::vector<T*>::iterator iter = prescribed_vector_ -> begin();
       iter != prescribed_vector_ -> end();
       iter++ ) {
    if( (*iter) -> getCoordID() == coord_id )
      return( **iter );
  }

  throw EXC(CalcException) << "The prescribed with number " << getNumber()
                           << " doesn't provide a Value with CoordSys::CoordID=" << coord_id << Exception::endl;
}

//------------------------------------------------------------------
template<class T>
T* Prescribed<T>::next(T* act_value, int& index)
{
  if( (unsigned)++index < prescribed_vector_->size() )
    return( (*prescribed_vector_)[index] );
  else
    return( 0 );
}

//------------------------------------------------------------------
template<class T>
Iterator<T> Prescribed<T>::begin( T* )
{
  if( prescribed_vector_->empty() )
    return( Aggregate<T>::end() );
  else
    return( Iterator<T>( this , prescribed_vector_->front() , 0 ) );
}
