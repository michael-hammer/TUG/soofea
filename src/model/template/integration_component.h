#ifndef integration_component_h___
#define integration_component_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>

#include "base/template/aggregate.h"
#include "base/template/const_aggregate.h"

#include "../component.h"
#include "../type.h"
#include "../time_stamp.h"

#include "math/array.h"
#include "geometry/coord_sys.h"

//------------------------------------------------------------------
/**
   Holds IntegrationPoints, therefore all components which are
   integrateable like element, surface, edge aso should be derived
   from this base class
 */
template<class T>
class IntegrationComponent :
  public Aggregate<T>,
  public ConstAggregate<T>
{
public:
  virtual ~IntegrationComponent();

  inline unsigned getIntegrationPointAmount()
  { return( integration_point_vector_.size() ); }

  virtual void createIntegrationPoints() = 0;
  virtual void updateIntegrationPoints(const TimeStamp& time_stamp) = 0;

  virtual T* getLocal(Math::IntegrationPoint* math_int_point) const; 
protected:
  std::vector<T*> integration_point_vector_;

  void createIPs( bz::Array<double,2>& node_tensor,
		  Type& type,
		  CoordSys* coord_sys );
  void updateIPs( const TimeStamp& time_stamp,
		  bz::Array<double,2>& node_tensor,
		  Type& type,
		  CoordSys* coord_sys );

  using Aggregate<T>::begin;
  virtual Iterator<T> begin(T*);
  using ConstAggregate<T>::begin;
  virtual ConstIterator<T> begin(const T*) const;

  virtual T* next(T* act_int_point, int& act_local_int_point_number);
  virtual const T* next(const T* act_int_point, int& act_local_int_point_number) const;

  using Aggregate<T>::get;
  virtual T* get(T* t,int local_number)
  { return( this->at(t,local_number) ); }

  using ConstAggregate<T>::get;
  virtual const T* get(const T* t,int local_number) const
  { return( this->at(t,local_number) ); }

  using Aggregate<T>::at;
  virtual T* at(T*,int local_number)
  { return(integration_point_vector_[local_number]); }

  using ConstAggregate<T>::at;
  virtual const T* at(const T*,int local_number) const
  { return(integration_point_vector_[local_number]); }
};

#include "integration_component.tcc"

#endif
