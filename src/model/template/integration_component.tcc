/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

//------------------------------------------------------------------
template<class T> IntegrationComponent<T>::~IntegrationComponent()
{
  typename std::vector<T*>::iterator iter;
  for( iter = integration_point_vector_.begin() ; 
       iter != integration_point_vector_.end() ; 
       ++iter )
    delete *iter;
}

//------------------------------------------------------------------
template<class T>
T* IntegrationComponent<T>::next(T* act_int_point, int& act_local_int_point_number)
{
  if( act_local_int_point_number < 0 )
    return 0;

  if( (unsigned)(++act_local_int_point_number) < this->integration_point_vector_.size() )
    return( integration_point_vector_[act_local_int_point_number] );
  else {
    act_local_int_point_number = -1;
    return 0;
  }
}

//------------------------------------------------------------------
template<class T>
const T* IntegrationComponent<T>::next(const T* act_int_point, int& act_local_int_point_number) const
{
  if( act_local_int_point_number < 0 )
    return 0;

  if( (unsigned)(++act_local_int_point_number) < this->integration_point_vector_.size() )
    return( integration_point_vector_[act_local_int_point_number] );
  else {
    act_local_int_point_number = -1;
    return 0;
  }
}

//------------------------------------------------------------------
template<class T>
Iterator<T> IntegrationComponent<T>::begin(T*)
{
  return( Iterator<T>(this,this->integration_point_vector_[0],0) );
}

//------------------------------------------------------------------
template<class T>
ConstIterator<T> IntegrationComponent<T>::begin(const T*) const
{
  return( ConstIterator<T>(this,this->integration_point_vector_[0],0) );
}

//------------------------------------------------------------------
template<class T>
T* IntegrationComponent<T>::getLocal(Math::IntegrationPoint* math_int_point) const
{
  for( typename std::vector<T*>::const_iterator iter = integration_point_vector_.begin();
       iter != integration_point_vector_.end() ; 
       ++iter )
    if( *dynamic_cast<Math::IntegrationPoint*>(*iter) == *math_int_point )
      return( *iter );
  return( 0 );
}

//------------------------------------------------------------------
template<class T>
void IntegrationComponent<T>::createIPs( bz::Array<double,2>& node_tensor,
					 Type& type,
					 CoordSys* coord_sys )
{
  std::vector<Math::IntegrationPoint*>* math_int_point_vector = type.getMathIntegrationPointVector();

  if( !math_int_point_vector ) {
    LOG(Logger::DEBUG) << "Could not create IntegrationPoint(s) as no Math::IntegrationPoint(s) are given!" << Logger::endl;
    return;
  }

  double x=0.,y=0.,z=0.;
  for( std::vector<Math::IntegrationPoint*>::iterator iter = math_int_point_vector -> begin() ;
       iter != math_int_point_vector -> end() ;
       ++iter) {
    const bz::Array<double,1>* shape_tensor = (*iter)->getShapeTensor();
    bz::Array<double,1> int_point( sum( node_tensor(bzt::i,bzt::j) * (*shape_tensor)(bzt::j) , bzt::j ) );

    switch( coord_sys -> getDimension() ) {
    case 3: z = int_point(2);
    case 2: y = int_point(1);
    default:
      x = int_point(0);
      break;
    }
    
    integration_point_vector_.
      push_back( new T( *iter, x, y, z, 293.15 , coord_sys ) );
    x=0.,y=0.,z=0.;
  }
}

//------------------------------------------------------------------
template<class T>
void IntegrationComponent<T>::updateIPs( const TimeStamp& time_stamp,
                                         bz::Array<double,2>& node_tensor,
					 Type& type,
					 CoordSys* coord_sys )
{
  if( !integration_point_vector_.size() )
    return;

  double x=0.,y=0.,z=0.;
  for( typename std::vector<T*>::iterator iter = integration_point_vector_.begin() ;
       iter != integration_point_vector_.end() ;
       ++iter) {
    const bz::Array<double,1>* shape_tensor = (*iter) -> getMathIP().getShapeTensor();
    bz::Array<double,1> int_point( sum( node_tensor(bzt::i,bzt::j) * (*shape_tensor)(bzt::j) , bzt::j ) );
    
    switch( coord_sys -> getDimension() ) {
    case 3: z = int_point(2);
    case 2: y = int_point(1);
    default:
      x = int_point(0);
      break;
    }
     
    (*iter) -> updatePoint(time_stamp,x,y,z);
  }
}
