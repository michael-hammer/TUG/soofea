/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

//------------------------------------------------------------------
template<class T>
T* ModelAggregate<T>::next( T* act, int& act_index )
{
  if( !act ) { // We are on beginning
    if( act_index < 0 )
      throw EXC(CalcException) << "You really want to increment on an end iterator?" << Exception::endl;
    return( map_ -> begin() -> second );
  } else {
    typename std::map<numbered,T*>::iterator iter = map_ -> find( act -> getNumber() );
    if( ++iter != (map_ -> end()) ) {
      return( iter -> second );
    } else {
      act_index = -1;
      return( 0 );
    }
  }
}

//------------------------------------------------------------------
template<class T>
const T* ModelAggregate<T>::next( const T* act, int& act_index ) const
{
  if( !act ) { // We are on beginning
    if( act_index < 0 )
      throw EXC(CalcException) << "You really want to increment on an end iterator?" << Exception::endl;
    return( map_ -> begin() -> second );
  } else {
    typename std::map<numbered,T*>::const_iterator iter = map_ -> find( act -> getNumber() );
    if( ++iter != (map_ -> end()) ) {
      return( iter -> second );
    } else {
      act_index = -1;
      return( 0 );
    }
  }
}

//------------------------------------------------------------------
template<class T>
Iterator<T> ModelAggregate<T>::begin( T* )
{
  if( map_ -> begin() == map_ -> end() )
    return( Aggregate<T>::end() );

  return( Iterator<T>(this, map_ -> begin() -> second, 0) );
}

//------------------------------------------------------------------
template<class T>
ConstIterator<T> ModelAggregate<T>::begin( const T* ) const
{
  if( map_ -> begin() == map_ -> end() )
    return( ConstAggregate<T>::end() );

  return( ConstIterator<T>(this, map_ -> begin() -> second, 0) );
}

//------------------------------------------------------------------
template<class T>
T* ModelAggregate<T>::get( T* , int index )
{
  typename std::map<numbered,T*>::iterator iter = map_ -> find( (numbered)index );
  if( iter != map_ -> end() )
    return( iter -> second );
  else {
    throw EXC(CalcException) << "The ´" << typeid(T).name() << "´ with number " 
                             << index << " could not be found" << Exception::endl;
  }
}

//------------------------------------------------------------------
template<class T>
const T* ModelAggregate<T>::get( const T* , int index ) const
{
  typename std::map<numbered,T*>::const_iterator iter = map_ -> find( (numbered)index );
  if( iter != map_ -> end() )
    return( iter -> second );
  else {
    throw EXC(CalcException) << "The ´" << typeid(T).name() << "´ with number " 
                             << index << " could not be found" << Exception::endl;
  }
}

//------------------------------------------------------------------
template<class T>
T* ModelAggregate<T>::at( T* , int index )
{
  typename std::map<numbered,T*>::iterator iter = map_ -> begin();
  std::advance( iter, index );
  return( iter->second );
}

//------------------------------------------------------------------
template<class T>
const T* ModelAggregate<T>::at( const T* , int index ) const
{
  typename std::map<numbered,T*>::const_iterator iter = map_ -> begin();
  std::advance( iter, index );
  return( iter->second );
}
