#ifndef mortar_element_h___
#define mortar_element_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <map>
#include <utility>
#include <algorithm>
#include <iomanip>

#include "math/array.h"
#include "type_container.h"
#include "edge.h"
#include "element.h"
#include "contact.h"
#include "geometry/geometry.h"

//------------------------------------------------------------------
class MortarElement :
  public TypeContainer
{
public:
  MortarElement( Contact& contact, Edge& non_mortar_edge );
  
  ~MortarElement() {}

  /**
     Needed for avaraged normal distribution - There we need the
     preceding, main and following non mortar edge.
   */
  typedef enum PositionID_ {
    MAIN = 0,
    PRECEDING = 1,
    FOLLOWING = 2
  } PositionID;

  void setContact(Contact* contact);

  inline Contact& getContact()
  {return( contact_ );}

  //------------------------------------------------------------------
  // 2D Contact
  void setNonMortarEdge(Edge* edge, PositionID pos_id)
  {non_mortar_edge_vector_[pos_id] = edge;}

  Edge* getNonMortarEdge(PositionID pos_id = MAIN) const
  {return(non_mortar_edge_vector_[pos_id]);}

  bool hasNonMortarEdge( PositionID pos_id )
  {return( non_mortar_edge_vector_[pos_id] != 0);}

  //------------------------------------------------------------------

  /**
     @return The amount of mortar nodes for one MortarElement. This is
     the real merged amount of nodes - no duplicates.
   */
  unsigned getMortarNodeAmount();

  std::vector<Node*>* getUniqueMortarNodes();

  unsigned getNonMortarNodeAmountLA();

  /**
     @return The amount of active tntegraion points - needed only for
     penalty method (FPGM).
   */
  unsigned getActiveIntegrationPoints();

  /**
     @return The amount of active nodes for Lagrange method. See the
     active set strategy.
   */
  unsigned getActiveNonMortarNodeAmount();

  bz::Array<double,2>* getEulerianPointTensorLA();

  /**
     @return The special "difference" shape tensor $N^{me}$ for the
     gap function
   */
  bz::Array<double,1>* getShapeTensorM( BoundaryIntegrationPoint& ip );

  bz::Array<double,1>* getShapeTensorA( BoundaryIntegrationPoint& ip );

  /**
     @param contact_side Configures if we derive with respect to $\xi^{(1)}$ or $\xi^{(2)}$
   */
  bz::Array<double,1>* getDerivativeShapeTensorM( BoundaryIntegrationPoint& ip, Contact::ContactSide contact_side );

  bz::Array<double,1>* getDerivativeShapeTensorA( BoundaryIntegrationPoint& ip, Contact::ContactSide contact_side );

  bz::Array<double,1>* getAveragingTensor( double r );

  /**
     Resets properties in nodes due to Mortar Analysis back to save
     values
   */
  void resetNodalData();

  inline numbered getNumber() const 
  {return( non_mortar_edge_vector_[MAIN]->getNumber() );};

  void prettyPrint();
protected:
  Contact& contact_;

  /**
     Attention: for 2D Contact only
   */
  std::vector<Edge*> non_mortar_edge_vector_;

  bool active_contact_;
};

#endif
