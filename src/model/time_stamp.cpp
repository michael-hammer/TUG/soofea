/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2010  Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "time_stamp.h"

//------------------------------------------------------------------
void TimeStamp::prettyPrint() const
{
  std::cout << "---------------------------------" << std::endl; 
  std::cout << "| TimeStamp @time = " << time_ << " | index: " << index()
            << " iter: " << iteration_counter_ << std::endl;
}

//------------------------------------------------------------------
bool operator==( const TimeStamp &t1, const TimeStamp &t2 )
{
  return( (t1.index() == t2.index()) && 
          (t2.iteration_counter_ == t2.iteration_counter_) );
}

//------------------------------------------------------------------
bool TimeStampComp::operator()(const TimeStamp& earlier, const TimeStamp& later) const
{ 
  if( earlier.index() < later.index() ) 
    return( true );
  if( (earlier.index()           == later.index()) && 
      (earlier.iteration_counter_ < later.iteration_counter_) )
    return( true );
  return( false );
}
