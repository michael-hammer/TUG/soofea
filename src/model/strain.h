#ifndef strain_h__
#define strain_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "math/array.h"
#include "math/tensor_base/tensor_second_order.h"

//------------------------------------------------------------------
class Strain : 
  public bz::Array<double,2>,
  public TensorSecondOrder  
{ 
public:  
  inline Strain(unsigned dimension = 3) :
    bz::Array<double,2>(dimension,dimension)
  { (*dynamic_cast<bz::Array<double,2>*>(this))=0.; }

  Strain(double eps_xx, double eps_yy, double eps_xy);
  ~Strain() {}

  VoigtNotation* getVoigtNotation();

  double getValue(Index stress_index) const;
protected: 
};

#endif // strain_h__
