#ifndef time_bar_h___
#define time_bar_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include <cmath>
#include <cfloat>
#include <vector>

#include "exceptions.h"
#include "base/logger.h"
#include "time_episode.h"
#include "base/template/aggregate.h"
#include "dof_load/dof_value.h"
#include "dof_load/load_value.h"
#include "template/prescribed.h"

//------------------------------------------------------------------
class TimeBar :
  public Aggregate<TimeStamp>
{
public:
  TimeBar();

  ~TimeBar();

  void addTimeEpisode( TimeEpisode* time_episode );

  TimeEpisode& getTimeEpisode( numbered te_number );

  TimeEpisode& getTimeEpisode( const TimeStamp& time_stamp );

  TimeStamp* getTimeStampFromTimeBar( const TimeStamp& time_stamp );

  void distributeTimeStamps();

  inline void setLastFinishedTimeStamp( TimeStamp& time_stamp )
  { last_finished_time_stamp_ = &time_stamp;} 

  inline const TimeStamp& getLastFinishedTimeStamp()
  { return( *last_finished_time_stamp_ ); }

  void prettyPrint();
protected:
  std::vector<TimeEpisode*> episode_vector_;
  std::vector<TimeStamp*> time_stamp_vector_;
  TimeStamp* last_finished_time_stamp_;

  using Aggregate<TimeStamp>::next;
  virtual TimeStamp* next(TimeStamp* act_item, int& act_local_number);

  using Aggregate<TimeStamp>::begin;
  virtual Iterator<TimeStamp> begin( TimeStamp* );

  using Aggregate<TimeStamp>::get;
  virtual TimeStamp* get( TimeStamp* ts, int local_number )
  { return( this->at(ts,local_number) ); }

  using Aggregate<TimeStamp>::at;
  virtual TimeStamp* at( TimeStamp* , int local_number )
  { return( time_stamp_vector_.at( local_number ) ); }
};

#endif
