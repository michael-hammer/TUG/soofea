/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "integration_point.h"

//------------------------------------------------------------------
void IntegrationPoint::updatePoint(TimeStamp time_stamp, double x, double y, double z)
{
  point_euler_.setCoordinate(x,CoordSys::X);
  point_euler_.setCoordinate(y,CoordSys::Y);
  point_euler_.setCoordinate(z,CoordSys::Z);
}

//------------------------------------------------------------------
void IntegrationPoint::updateTemperature(TimeStamp time_stamp, double t)
{
  temperature_euler_ = t;
}

//------------------------------------------------------------------
void IntegrationPoint::updateLagrangeFactor(TimeStamp time_stamp,
					    double lambda_n,
					    double lambda_r,
					    double lambda_s)
{
  lagrange_factor_.set( lambda_n , CoordSys::NORMAL );
  lagrange_factor_.set( lambda_r , CoordSys::TANGENT_R );
  lagrange_factor_.set( lambda_s , CoordSys::TANGENT_S );
}

//------------------------------------------------------------------
void IntegrationPoint::prettyPrintLagrangeFactor()
{
  std::cout << "\t  lambda_n = " << lagrange_factor_.get( CoordSys::NORMAL ) << std::endl;
  std::cout << "\t  lambda_r = " << lagrange_factor_.get( CoordSys::TANGENT_R ) << std::endl;
  std::cout << "\t  lambda_s = " << lagrange_factor_.get( CoordSys::TANGENT_S ) << std::endl;
}
