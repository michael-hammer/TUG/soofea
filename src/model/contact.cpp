/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "contact.h"

//------------------------------------------------------------------
Contact::~Contact()
{
  if( mortar_number_vector_ )
    delete mortar_number_vector_;
  
  if( non_mortar_number_vector_ )
    delete non_mortar_number_vector_;
}

//------------------------------------------------------------------
void Contact::prettyPrint()
{
  std::cout << "---------------------------------" << std::endl;
  if( !calc_contact_ ) {
    std::cout << "| Contact Data Set: not enabled" << std::endl;
    std::cout << "---------------------------------" << std::endl;
    return;
  }
  std::cout << "| Contact Data Set:" << std::endl;
  if( mortar_number_vector_ ) {
    std::cout << "| Mortar Boundaries: ";
    for( std::vector<numbered>::iterator iter= mortar_number_vector_ -> begin(); 
	 iter != mortar_number_vector_ -> end() ;
	 ++iter ) {
      std::cout << *iter << " ";
    }
    std::cout << std::endl;
  }
  if( non_mortar_number_vector_ ) {
    std::cout << "| Non Mortar Boundaries: ";
    for( std::vector<numbered>::iterator iter = non_mortar_number_vector_ -> begin() ;
	 iter != non_mortar_number_vector_ -> end() ;
	 ++iter ) {
      std::cout << *iter << " ";
    }
    std::cout << std::endl;
  }
  std::cout << "---------------------------------" << std::endl;
}

//------------------------------------------------------------------
std::string Contact::getMortarNumbers() const
{
  return( ParserBase::vectorToString( *mortar_number_vector_ ) );
}

//------------------------------------------------------------------
std::string Contact::getNonMortarNumbers() const
{
  return( ParserBase::vectorToString( *non_mortar_number_vector_ ) );
}

//------------------------------------------------------------------
void Contact::setAnalysisType(IDObject::ID analysis_type)
{
  if( (analysis_type_ != IDObject::NAID) && (analysis_type_ != analysis_type ) ) {
    throw EXC(IOException) << "You can not mix different Mortar Analysis types in one analysis - You tried to add Mortar analysis type '" 
                           << IDObject::resolveID( analysis_type_ )
                           << "' AND '"
                           << IDObject::resolveID( analysis_type ) << Exception::endl;
  }
  analysis_type_ = analysis_type;
}
