/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "element_integration_point.h"

//------------------------------------------------------------------
ElementIntegrationPoint::ElementIntegrationPoint(Math::IntegrationPoint* int_point, 
                                                 double x, double y, double z, double t, CoordSys* system) :
  IntegrationPoint(int_point,x,y,z,t,system),
  lagrangian_jacobian_(system->getDimension(),system->getDimension()),
  inverse_lagrangian_jacobian_(system->getDimension(),system->getDimension()),
  eulerian_jacobian_(system->getDimension(),system->getDimension()),
  deformation_gradient_(system->getDimension(),system->getDimension()),
  right_cauchy_green_(system->getDimension(),system->getDimension()),
  lagrangian_strain_(system->getDimension(),system->getDimension()),
  stress_(system->getDimension(),system->getDimension()),
  elasticity_tensor_(system->getDimension(),system->getDimension(),system->getDimension(),system->getDimension()),
  NJ_(0)
{
  lagrangian_jacobian_ = 0.;
  inverse_lagrangian_jacobian_ = 0.;
  eulerian_jacobian_ = 0.;
  deformation_gradient_ = 0.;
  right_cauchy_green_ = 0.;
  lagrangian_strain_ = 0.;
  stress_ = 0.;
  elasticity_tensor_ = 0.;
}

//------------------------------------------------------------------
ElementIntegrationPoint::~ElementIntegrationPoint()
{
  if( NJ_ )
    delete NJ_;
}

