/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "global.h"

//------------------------------------------------------------------
Global& Global::operator=(const Global& src)
{
  dimension_ = src.dimension_;
  coord_sys_id_ = src.coord_sys_id_;
  id_ = src.id_;

  return( *this );
}

//------------------------------------------------------------------
void Global::setDisplacementAnalysis(bool boolean)
{
  displacement_ = boolean;
}

//------------------------------------------------------------------
void Global::setTemperatureAnalysis(bool boolean)
{
  temperature_ = boolean;
}

//------------------------------------------------------------------
void Global::setID(std::string id_string)
{
  id_ = id_string;
}

//------------------------------------------------------------------
unsigned Global::getDimension() const
{
  return(dimension_);
}

//------------------------------------------------------------------
bool Global::getDisplacementAnalysis()
{
  return(displacement_);
}

//------------------------------------------------------------------
bool Global::getTemperatureAnalysis()
{
  return(temperature_);
}

//------------------------------------------------------------------
double Global::getConvergenceCriteria() const
{
  if( convergence_criteria_ == 0.0 )
    throw EXC(IOException) << "You try to fetch a convergence_criteria out of <global> which wasn't set in input file!" 
			   << Exception::endl;

  return( convergence_criteria_ );
}


//------------------------------------------------------------------
void Global::prettyPrint()
{
  std::cout << "---------------------------------" << std::endl;
  std::cout << "| Global Data: " << std::endl;
  std::cout << "| " << std::endl;
  std::cout << "| id : " << id_ << std::endl;
  std::cout << "| coord_sys_id : " << IDObject::resolveID(coord_sys_id_) << std::endl;
  std::cout << "| dimension_ : " << dimension_ << std::endl;
  std::cout << "| convergence_criteria_ : "<< convergence_criteria_ << std::endl;
  std::cout << "| Calc Disp        DOFs : " << displacement_ << std::endl;
  std::cout << "| Calc Temperature DOFs : " << temperature_ << std::endl;
  std::cout << "---------------------------------" << std::endl;
}
