#ifndef type_container_h___
#define type_container_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "type.h"

//------------------------------------------------------------------
class TypeContainer
{
public:
  /**
     If we construct a TypeContainer in the InputHandler the
     corresponding Type might not be created! We provide the number
     only for this cases.
  */
  TypeContainer(numbered type_number) :
    type_number_(type_number),
    type_(0)
  {}

  TypeContainer(Type* type) :
    type_number_(type->getNumber()),
    type_(type)
  {}

  inline Type& getType() const
  {return(*type_);}

protected:
  friend class Model;

  numbered type_number_;

  inline void setType(Type* type)
  {type_ = type;}

  Type* type_;
};

#endif
