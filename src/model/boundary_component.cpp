/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "boundary_component.h"

//------------------------------------------------------------------
void BoundaryComponent::setContactSide(Contact::ContactSide contact_side)
{
  contact_side_ = contact_side;
  for( std::vector<Node*>::iterator iter = node_vector_.begin() ;
       iter != node_vector_.end() ;
       ++iter ) {
    (*iter) -> setContactSide( contact_side );
  }
}

//------------------------------------------------------------------
Vector* BoundaryComponent::getEulerianTangent( double r, double s, unsigned direction ) const
{
  bz::Array<double,2>* node_tensor = getEulerianPointTensor();
  bz::Array<double,2>* der_shape_2D = getType().getShape().derivativeShapeTensor(r,s);
  // we need this becaus der1_shape is 2 dimensional
  bz::Array<double,1> der_shape( (der_shape_2D->shape())(0) );
  der_shape = (*der_shape_2D)(bz::Range::all(),direction); // Here we fetch the derivative direction!
  delete der_shape_2D;
  bz::Array<double,1> tangent_array( bz::sum( (*node_tensor)(bzt::i,bzt::j) * der_shape(bzt::j) , bzt::j ) );
  Vector* tangent = new Vector( dynamic_cast<PointAggregate*>(node_vector_.front())->getEulerianPoint().getCoordSys(), tangent_array );
  delete node_tensor;
  return( tangent );
}

//------------------------------------------------------------------
void BoundaryComponent::createIntegrationPoints()
{
  bz::Array<double,2>* node_tensor = getLagrangianPointTensor();
  createIPs( *node_tensor, getType() , node_vector_.front()->getLagrangianPoint().getCoordSys() );
  delete node_tensor;
}

//------------------------------------------------------------------
void BoundaryComponent::updateIntegrationPoints(const TimeStamp& time_stamp)
{
  bz::Array<double,2>* node_tensor = getEulerianPointTensor();
  updateIPs( time_stamp, *node_tensor, getType() , dynamic_cast<PointAggregate*>(node_vector_.front())->getEulerianPoint().getCoordSys() );
  delete node_tensor;
}

//------------------------------------------------------------------
void BoundaryComponent::updateIntegrationPointsLagrangeFactor(const TimeStamp& time_stamp)
{
  unsigned debug_counter = 0;

  bz::Array<double,2>* lagrange_tensor = getLagrangeFactorTensor();
  if( !lagrange_tensor )
    return;

  bz::Array<double,1>* shape_tensor;
  for( std::vector<BoundaryIntegrationPoint*>::iterator iter = integration_point_vector_.begin() ;
       iter != integration_point_vector_.end() ;
       ++iter)
    {
      shape_tensor = getType().getShape().dualShapeTensor( (*iter) -> getMathIP().r() );
      bz::Array<double,1> lagrange_factor( sum( (*lagrange_tensor)(bzt::i,bzt::j) * (*shape_tensor)(bzt::j) , bzt::j ) );
      delete shape_tensor;

      switch( (lagrange_tensor -> shape())(0) ) {
      case 1:
	(*iter) -> updateLagrangeFactor( time_stamp, lagrange_factor(0) , 0. , 0.);
	break;
      case 2:
	(*iter) -> updateLagrangeFactor( time_stamp, lagrange_factor(0) , lagrange_factor(1) , 0.);
	break;
      case 3:
	(*iter) -> updateLagrangeFactor( time_stamp, lagrange_factor(0) , lagrange_factor(1) , lagrange_factor(2) );
	break;
      }
      LOG(Logger::DEBUG) << "Updated lambda on boundary component " << getNumber()
                         << " and BoundaryIntegration point " << debug_counter++ 
                         << " r = " << (*iter) -> getMathIP().r()
        //              << " s = " << (*iter) -> getMathIP().s()
                         << " | x = " << (*iter) -> getEulerianPoint().getCoordinate( CoordSys::X )
                         << " y = " << (*iter) -> getEulerianPoint().getCoordinate( CoordSys::Y )
        // << " z = " << (*iter) -> getEulerianPoint().getCoordinate( CoordSys::Z )
                         << " | lambda = " << lagrange_factor << Logger::endl;
    }

  delete lagrange_tensor;
  return;
}
