#ifndef boundary_component_h___
#define boundary_component_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>

#include "integration_point.h"
#include "node_container.h"
#include "contact.h"
#include "template/integration_component.h"
#include "boundary_integration_point.h"

//------------------------------------------------------------------
/**
   For all components which may lie on boundaries like edge,
   face. These boundary components maybe in contact.
 */
class BoundaryComponent :
  public NumberedObject,
  public NodeContainer,
  public IntegrationComponent<BoundaryIntegrationPoint>
{
public:
  BoundaryComponent(numbered number,
                    std::vector<numbered>* node_number_vector,
                    numbered type_number) :
    NumberedObject( number ),
    NodeContainer( node_number_vector , type_number ),
    contact_side_( Contact::NACS ),
    pressure_( 0. ) {}

  void createIntegrationPoints();

  void updateIntegrationPoints(const TimeStamp& time_stamp);

  void updateIntegrationPointsLagrangeFactor(const TimeStamp& time_stamp);
  
  void setContactSide(Contact::ContactSide contact_side);

  inline Contact::ContactSide getContactSide() {return(contact_side_);}

  bool pressureSet() { return(fabs(pressure_) > DBL_EPSILON); }

  void setPressure(double pressure) { pressure_ = pressure; }

  double getPressure() { return(pressure_); }

  Vector* getEulerianTangent( double r, double s=0., unsigned direction=0 ) const;

  virtual Vector* getEulerianNormal( double r, double s=0., bool normalized=false ) const = 0;

protected:
  Contact::ContactSide contact_side_;
  double pressure_;
};

#endif // boundary_component_h___
