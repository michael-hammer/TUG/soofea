#ifndef element_h__
#define element_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <soofea_assert.h>

#include <iostream>
#include <vector>
#include <map>

#include "base/numbered_object.h"
#include "base/parser_base.h"
#include "node_container.h"
#include "edge.h"
#include "material.h"
#include "element_type.h"
#include "template/integration_component.h"
#include "base/template/aggregate.h"
#include "element_integration_point.h"

#include "analyzer/linear_tangent_map/jacobian.h"
#include "analyzer/linear_tangent_map/deformation_gradient.h"

//------------------------------------------------------------------
class Element : 
  public NumberedObject,
  public NodeContainer,
  public Aggregate<Edge>,
  public IntegrationComponent<ElementIntegrationPoint>
{
public:
  Element(numbered element_number, 
	  numbered material_number,
	  std::vector<numbered>* node_number_vector, 
	  numbered element_type_number) :
    NumberedObject(element_number),
    NodeContainer( node_number_vector, element_type_number ),
    material_number_(material_number),
    material_(0)
  {}

  virtual ~Element() {}

  inline std::string getNodeNumbers() const
  {return( ParserBase::vectorToString( *node_number_vector_ ) );}

  inline Material& getMaterial() const
  {return(*material_);}

  virtual void createIntegrationPoints();
  virtual void updateIntegrationPoints(const TimeStamp& time_stamp);

  /**
     pretty prints the main data of the element to cout
  */
  void prettyPrint(bool print_edge = false) const;

protected:
  friend class Model;

  numbered material_number_;

  void setMaterial(Material* material)
  { material_ = material; }

  Material* material_;

  void addEdge(Edge* edge)
  { edge_vector_.push_back(edge); }

  std::vector<Edge*> edge_vector_;

  using Aggregate<Edge>::begin;
  virtual Iterator<Edge> begin(Edge*);

  using Aggregate<Edge>::next;
  virtual Edge* next(Edge* act_edge, int& act_local_node_number);

  using Aggregate<Edge>::at;
  virtual Edge* at(Edge*, int local_edge_number)
  { return(edge_vector_.at(local_edge_number)); }

  using Aggregate<Edge>::get;
  virtual Edge* get(Edge* edge, int local_edge_number)
  { return( this->at(edge, local_edge_number) ); }
};

#endif // element_h__
