#ifndef time_stamp_h___
#define time_stamp_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2010  Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "base/numbered_object.h"

#include <iostream>

//------------------------------------------------------------------
class TimeStamp :
  public NumberedObject
{
public:
  TimeStamp(double time,
            unsigned time_index):
    NumberedObject( time_index ),
    time_(time),
    iteration_counter_(0)
  {}

  TimeStamp(const TimeStamp& src) :
    NumberedObject( src ),
    time_(src.time_),
    iteration_counter_(src.iteration_counter_)
  {}

  ~TimeStamp() {}

  inline operator double() const
  {return( time_ );}

  inline double time() const
  {return( time_ );}

  inline numbered index() const
  {return( getNumber() );}

  inline unsigned& iteration()
  {return( iteration_counter_ );}

  inline unsigned iteration() const
  {return( iteration_counter_ );}

  void prettyPrint() const;

  bool lagrangianState() const
  { return( !index() && !iteration_counter_ ); }

protected:
  friend struct TimeStampComp;
  friend bool operator==( const TimeStamp &t1, const TimeStamp &t2 );

  double time_;
  unsigned iteration_counter_;
};

//------------------------------------------------------------------
bool operator==( const TimeStamp &t1, const TimeStamp &t2 );

//------------------------------------------------------------------
/**
   This functor is needed as comparison operator for ordered maps
   against the TimeStamp
 */
struct TimeStampComp {
  bool operator()(const TimeStamp& earlier, const TimeStamp& later) const;
};

#endif
