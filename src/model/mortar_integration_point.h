#ifndef mortar_integration_point_h___
#define mortar_integration_point_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>
#include <iostream>

#include "lagrange_factor.h"
#include "geometry/vector.h"

class Edge;
class Node;

//------------------------------------------------------------------
class MortarIntegrationPoint
{
public:
  MortarIntegrationPoint() :
    xi_(0),
    last_converged_xi_(0),
    last_converged_edge_(0),  
    g_N_(0),
    g_T_(0),
    last_converged_g_T_(0),  
    local_mortar_edge_number_(-1),
    nearest_mortar_node_(0),
    active_(false),
    average_tangent_(0)
  {}

  ~MortarIntegrationPoint();

  /**
     @return boolean if MortarEdge has changed or not
   */
  bool setNearestMortarNode( Node* nearest_mortar_node );

  const Node& getNearestMortarNode() const
  {return(*nearest_mortar_node_);}

  void addMortarEdge( Edge* mortar_edge );

  inline unsigned amountOfMortarEdges()
  { return( mortar_edge_vector_.size() ); }

  bool mortarEdgeSet()
  { return( !mortar_edge_vector_.empty() ); }

  Edge& getMortarEdge( unsigned index )
  { return( *mortar_edge_vector_[ index ] ); }

  bool hasActiveMortarEdge()
  { return( local_mortar_edge_number_ >= 0 ); }

  Edge& getActiveMortarEdge( bool last_converged=false );

  /**
     @param local_mortar_edge_number can be used to return the actual
     mortar edge out of mortar_edge_vector
   */
  void setGapData( double g_N, double g_T, double xi, 
		   int local_mortar_edge_number = -1 );

  double getGN()
  { return( g_N_ ); }

  double getGT(bool last_converged=false);

  void archive();

  bool setActive(bool active);
 
  inline bool getActive()
  {return( active_ );}

  double xi(bool last_converged=false);

  void setAverageTangent( Vector* average_tangent );

  inline Vector& getAverageTangent( )
  {return(*average_tangent_);}

protected:
  /**
     That's the natural coordinate of the int point projection onto
     the mortar side
  */
  double xi_;
  double last_converged_xi_;

  std::vector<Edge*> mortar_edge_vector_;
  Edge* last_converged_edge_;
  
  /**
     The normal gap between the int point on the non-mortar side and
     the projected point on the mortar side.
  */
  double g_N_;
  double g_T_;
  double last_converged_g_T_;

  int local_mortar_edge_number_;

  Node* nearest_mortar_node_;
  bool active_;

  Vector* average_tangent_;
};

#endif
