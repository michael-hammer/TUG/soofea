#ifndef edge_type_h___
#define edge_type_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "base/numbered_object.h"

#include "shape/linear_shape.h"
#include "type.h"

//------------------------------------------------------------------
class EdgeType : 
  public Type
{
public:
  EdgeType(numbered number,
	   IDObject::ID type_id,
	   unsigned shape_order,
	   std::vector<int>* amount_of_int_points);

  virtual ~EdgeType() {}

protected:
  virtual void createShape( unsigned shape_order, IDObject::ID shape_type_id );
};

#endif // edge_type_h___
