#ifndef node_h__
#define node_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <soofea_assert.h>

#include <iostream>
#include <list>
#include <vector>

#include <map>
#include <utility>

#include "base/dynamic_property.h"
#include "base/numbered_object.h"
#include "base/template/aggregate.h"
#include "base/template/const_aggregate.h"
#include "geometry/vector.h"
#include "geometry/coord_sys.h"

#include "component.h"
#include "stress.h"
#include "point_aggregate.h"
#include "contact.h"

#include "dof_load/displacement_dof.h"
#include "dof_load/temperature_dof.h"
#include "dof_load/lagrange_dof.h"
#include "dof_load/force_load.h"
#include "dof_load/heat_flux_load.h"

// class Boundary;

// Forward defines for bottom up searches!
class Edge;
class Face;
class Element;

//------------------------------------------------------------------
/**
   The classical FE Node in which the equilibrium is satisfied.
 */
class Node : 
  public NumberedObject,
  public Aggregate<DOFValue>,
  public PointAggregate,
  public DynamicProperty
{
public:
  inline Node(numbered node_number, double x, double y, double z, double t, CoordSys* system ) :
    NumberedObject(node_number),
    PointAggregate(x,y,z,t,system),
    contact_side_(Contact::NACS)
  {}

  virtual ~Node();

  virtual void updatePoint(TimeStamp time_stamp, double x=0., double y=0., double z=0.);
  virtual void updateTemperature(TimeStamp time_stamp, double t = 293.15);
  virtual void updateLagrangeFactor(TimeStamp time_stamp, double lambda_n=0.,double lambda_r=0.,double lambda_s=0.);

  using PointAggregate::getEulerianPoint;
  virtual Point getEulerianPoint(TimeStamp& time_stamp, double deformation_factor = 1.);
  const virtual Point getEulerianPoint(TimeStamp& time_stamp, double deformation_factor = 1.) const;

  //------------------------------------------------------------------
  // DOF Load Handling
  DOF* getDOF(IDObject::ID dof_type) const;
  void unsetConstraintDOF();
  void setConstraintDOFZero();

  void addDisplacementDOF();
  void addTemperatureDOF();
  void addLagrangeDOF( bool enabled, bool friction );

  void disableDOF(IDObject::ID type);

  Load* getLoad(IDObject::ID load_type) const;
  void setConstraintLoadZero();
  void resetInternalLoad();
  void addLoad(Load* load);

  Contact::ContactSide getContactSide() const
  {return( contact_side_ );}

  void setContactSide( Contact::ContactSide contact_side )
  {contact_side_ = contact_side;}

  void addAdjacentEdge(Edge* edge);
  void addAdjacentFace(Face* face);
  void addAdjacentElement(Element* element);

  void prettyPrint() const;

protected:
  friend class BoundaryComponent;
  typedef std::map<IDObject::ID,std::pair<DOF*,Load*> > DOFLoadMap;

  using Aggregate<DOFValue>::begin;
  virtual Iterator<DOFValue> begin( DOFValue* );
  virtual DOFValue* next( DOFValue* act, int& index);

  using Aggregate<DOFValue>::get;
  inline DOFValue* get( DOFValue*, int local_number)
  { return(0); /* UUPS */}
  using Aggregate<DOFValue>::at;
  inline DOFValue* at( DOFValue*, int local_number)
  { return(0); /* UUPS */}

  Contact::ContactSide contact_side_;

  std::list<Edge*> adjacent_edge_list_;
  std::list<Face*> adjacent_face_list_;
  std::list<Element*> adjacent_element_list_;

  DOFLoadMap dof_load_map_;
};

#endif // node_h__
