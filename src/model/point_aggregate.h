#ifndef point_aggregate_h___
#define point_aggregate_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "../geometry/point.h"
#include "lagrange_factor.h"
#include "time_stamp.h"

//------------------------------------------------------------------
class PointAggregate {
public:
  inline PointAggregate(double x, double y, double z, 
			double t, 
			CoordSys* system) :
    point_lagrange_(system,x,y,z),
    point_euler_(system,x,y,z),
    temperature_lagrange_(t),
    temperature_euler_(t)
  {}

    virtual ~PointAggregate() {}

  virtual void updatePoint(TimeStamp time_stamp, double x=0., double y=0., double z=0.) = 0;
  virtual void updateTemperature(TimeStamp time_stamp, double t = 293.15) = 0;
  virtual void updateLagrangeFactor(TimeStamp time_stamp, double lambda_n=0.,double lambda_r=0.,double lambda_s=0.) = 0;

  inline Point& getLagrangianPoint()
  { return(point_lagrange_); }

  inline virtual Point& getEulerianPoint()
  { return(point_euler_); }

  inline const Point& getLagrangianPoint() const
  { return(point_lagrange_); }

  inline const virtual Point& getEulerianPoint() const
  { return(point_euler_); }

  inline double getLagrangianTemperature() const
  { return(temperature_lagrange_); }

  inline double getEulerianTemperature() const
  { return(temperature_euler_); }

  inline LagrangeFactor& getLagrangeFactor()
  { return(lagrange_factor_); }

  inline const LagrangeFactor& getLagrangeFactor() const
  { return(lagrange_factor_); }

  inline void setCoordSys(CoordSys* coord_sys) {
    point_lagrange_.setCoordSys(coord_sys);
    point_euler_.setCoordSys(coord_sys);
  }

protected:
  Point point_lagrange_;
  Point point_euler_;
  double temperature_lagrange_;
  double temperature_euler_;
  LagrangeFactor lagrange_factor_;
};

#endif
