#ifndef integration_point_h___
#define integration_point_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "math/integration_point.h"
#include "point_aggregate.h"

//------------------------------------------------------------------
class IntegrationPoint :
  public PointAggregate
{
public:
  IntegrationPoint(Math::IntegrationPoint* math_int_point,
                   double x, double y, double z, double t, CoordSys* system) :
    PointAggregate(x,y,z,t,system),
    math_ip_(math_int_point)
  {}
  
  virtual void updatePoint(TimeStamp time_stamp, double x=0., double y=0., double z=0.);
  
  virtual void updateTemperature(TimeStamp time_stamp, double t = 293.15);
  
  virtual void updateLagrangeFactor(TimeStamp time_stamp, double lambda_n=0.,double lambda_r=0.,double lambda_s=0.);
  
  Math::IntegrationPoint& getMathIP()
  {return( *math_ip_ );}
  
  const Math::IntegrationPoint& getMathIP() const
  {return( *math_ip_ );}
  
  void prettyPrintLagrangeFactor();
protected:
  Math::IntegrationPoint* math_ip_;
};

#endif
