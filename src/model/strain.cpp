/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "strain.h"

//------------------------------------------------------------------
Strain::Strain(double eps_xx, double eps_yy, double eps_xy) :
  bz::Array<double,2>(2,2)
{
  *dynamic_cast< bz::Array<double,2>*>(this) = eps_xx, eps_xy, eps_yy, eps_xy;
}

//------------------------------------------------------------------
Strain::VoigtNotation* Strain::getVoigtNotation()
{
  VoigtNotation* strain_vector = new VoigtNotation(3);
  *strain_vector = (*this)(0,0), (*this)(1,1), (*this)(1,0);
  return( strain_vector );
}

//------------------------------------------------------------------
double Strain::getValue(Strain::Index stress_index) const
{
  switch( stress_index )
    {
    case XX:
      return( (*this)(0,0) );
      break;
    case YY:
      return( (*this)(1,1) );
      break;
    case ZZ:
      return( (*this)(2,2) );
      break;
    case XY:
      return( (*this)(0,1) );
      break;
    case YZ:
      return( (*this)(1,2) );
      break;
    case XZ:
      return( (*this)(0,2) );
      break;
    }
  return( 0. );
}
