#ifndef bc_handler_h__
#define bc_handler_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <list>
#include <iostream>
#include <map>

#include "exceptions.h"
#include "base/logger.h"
#include "geometry/geometry.h"
#include "model/model.h"
#include "analyzer/map.h"

#include "soofea.h"
#ifdef SOOFEA_WITH_TBB
#include "tbb/tbb.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"
#endif

//------------------------------------------------------------------
/**
   Modifies all data inside the Model which is mutable due to the
   timestep oriented analysis
 */
class BCHandler
{
public:
  BCHandler(Model& model) :
    model_(model) {}

  ~BCHandler();

  void addPrescribedDOF(Prescribed<DOFValue>* prescribed_dof);

  void addPrescribedLoad(Prescribed<LoadValue>* prescribed_load);

  /**
     Integrate the mutable BoundaryCondition into the Model. This
     includes Load as well as PrescribedDOF and has to be called on
     each NEW time step.
   */
  void integrateBC( TimeStamp& time_stamp );

  /**
     After the first iteration we have to to set the displacement
     incremental to zero if one was given.
  */
  void setPrescribedDOFZero( TimeStamp& time_stamp );
  
protected:
  Model& model_;
  std::map<numbered,Prescribed<DOFValue>* > prescribed_dof_map_;
  std::map<numbered,Prescribed<LoadValue>* > prescribed_load_map_;

  /**
     Sets all contrained DOFs to unconstrained.
   */
  void unsetConstraintDOF();

  /**
     Removes all distributed loads from nodes and boundaries. Like in
     most FE codes no load means a 0. load indeed. That's because we
     need the whole boundary defined by van neuman or dirichlet BCs
     resp.
   */
  void unsetConstraintLoad();

  /**
     Integrate the mutable Load into the Model
   */
  void integratePrescribedLoad(TimeStamp& time_stamp,
                               LoadProgress& dof_progress);

  std::vector<LoadValue*>* getScaledLoadValues( TimeStamp& time_stamp,
                                                LoadProgress& dof_progress );

  /**
     Integrate the mutable PrescribedDOF into the Model
   */
  void integratePrescribedDOF(TimeStamp& time_stamp,
                              DOFProgress& dof_progress);

  std::vector<DOFValue*>* getScaledDOFValues( TimeStamp& time_stamp ,
                                              DOFProgress& dof_progress );
};

#endif // bc_handler_h__
