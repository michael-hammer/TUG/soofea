/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "mortar_element.h"

//------------------------------------------------------------------
MortarElement::MortarElement( Contact& contact, Edge& non_mortar_edge ) :
  TypeContainer(&non_mortar_edge.getType()),
  contact_(contact),
  non_mortar_edge_vector_(3),
  active_contact_(false)
{
  contact_.setAnalysisType( getType().getID() );

  non_mortar_edge_vector_[MAIN] = &non_mortar_edge;
  non_mortar_edge_vector_[PRECEDING] = 0;
  non_mortar_edge_vector_[FOLLOWING] = 0;
}

//------------------------------------------------------------------
bz::Array<double,1>* MortarElement::getShapeTensorA(BoundaryIntegrationPoint& ip)
{
  bz::Array<double,1>* shape_tensor_m = getShapeTensorM( ip );
  int non_mortar_node_amount = getNonMortarNodeAmountLA();
  // @fixme                                                       linear shape only! -> |
  bz::Array<double,1>* shape_tensor = new bz::Array<double,1>( non_mortar_node_amount + 2 );
  (*shape_tensor) = 0.;

  int preceding_offset = 0;
  if( non_mortar_edge_vector_[PRECEDING] )
    ++preceding_offset;

  (*shape_tensor)( bz::Range(preceding_offset,preceding_offset+1) ) = (*shape_tensor_m)( bz::Range(0,1) );
  (*shape_tensor)( bz::Range(non_mortar_node_amount,non_mortar_node_amount+1) ) = (*shape_tensor_m)( bz::Range(2,3) );

  delete shape_tensor_m;

  return( shape_tensor );
}

//------------------------------------------------------------------
bz::Array<double,1>* MortarElement::getShapeTensorM(BoundaryIntegrationPoint& ip)
{
  const bz::Array<double,1>* non_mortar_shape = ip.getMathIP().getShapeTensor();
  bz::Array<double,1>* mortar_shape = ip.getActiveMortarEdge().getType().getShape().shapeTensor(ip.xi());

  bz::Array<double,1>* shape = new bz::Array<double,1>( non_mortar_shape->size() + mortar_shape->size() );
  *shape = 0.;

  (*shape)( bz::Range( 0 , ( non_mortar_shape->size()-1) )) = (-1.) * (*non_mortar_shape);
  (*shape)( bz::Range( non_mortar_shape->size() , (shape->size()-1) )) = (*mortar_shape);

  delete mortar_shape;

  return( shape );
}

//------------------------------------------------------------------
bz::Array<double,1>* MortarElement::getDerivativeShapeTensorA(BoundaryIntegrationPoint& ip, Contact::ContactSide contact_side)
{
  bz::Array<double,1>* der_shape_tensor_m = getDerivativeShapeTensorM( ip, contact_side );
  int non_mortar_node_amount = getNonMortarNodeAmountLA();
  // @fixme                                                       linear shape only! -> |
  bz::Array<double,1>* der_shape_tensor = new bz::Array<double,1>( non_mortar_node_amount + 2 );
  (*der_shape_tensor) = 0.;

  int preceding_offset = 0;
  if( non_mortar_edge_vector_[PRECEDING] )
    ++preceding_offset;
  
  (*der_shape_tensor)( bz::Range(preceding_offset,preceding_offset+1) ) = (*der_shape_tensor_m)( bz::Range(0,1) );
  (*der_shape_tensor)( bz::Range(non_mortar_node_amount,non_mortar_node_amount+1) ) = (*der_shape_tensor_m)( bz::Range(2,3) );

  delete der_shape_tensor_m;
  return( der_shape_tensor );
}

//------------------------------------------------------------------
/**
   @fixme ATM only for linear shape working! (Because of fixed size "2" for non_mortar side)
 */
bz::Array<double,1>* MortarElement::getDerivativeShapeTensorM(BoundaryIntegrationPoint& ip, Contact::ContactSide contact_side)
{
  bz::Array<double,2>* mortar_der_shape = ip.getActiveMortarEdge().getType().getShape().derivativeShapeTensor(ip.xi());
  const bz::Array<double,2>* non_mortar_der_shape = ip.getMathIP().getDerivativeShapeTensor();
  bz::Array<double,1>* der_shape = new bz::Array<double,1>( 2 + mortar_der_shape->size() );
  (*der_shape) = 0.;

  switch( contact_side ) {
  case Contact::NON_MORTAR:
    (*der_shape)( bz::Range( 0 , 1 ) ) = (-1.) * (*non_mortar_der_shape)(bz::Range::all(),0);
    break;
  case Contact::MORTAR:   
    (*der_shape)( bz::Range( 2 , (der_shape->size()-1) ) ) = (*mortar_der_shape)(bz::Range::all(),0);
    break;
  default:
    throw EXC(CalcException) << "I can only provide a Derivative Shape Tensor for either MORTAR or NON_MORTAR side!" 
                             << Exception::endl;
    break;
  }

  delete mortar_der_shape;
  return( der_shape );
}

//------------------------------------------------------------------
unsigned MortarElement::getMortarNodeAmount()
{
  std::vector<Node*>* node_vector = getUniqueMortarNodes();
  if( node_vector ) {
    int size = node_vector -> size();
    delete node_vector;
    return( size );
  } else
    return( 0 );
}

//------------------------------------------------------------------
std::vector<Node*>* MortarElement::getUniqueMortarNodes()
{
  if( non_mortar_edge_vector_[MAIN] == 0) {
    return( 0 ); // Shouldn't happen - should it?
  }

  std::vector<Node*>* node_vector = new std::vector<Node*>;
  for( Iterator<BoundaryIntegrationPoint> ip_iter = (*non_mortar_edge_vector_[MAIN]).Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != (*non_mortar_edge_vector_[MAIN]).Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( ! ip_iter -> getActive() )
      continue;
    if( ip_iter -> hasActiveMortarEdge() ) {
      for( Iterator<Node> node_iter = ip_iter->getActiveMortarEdge().Aggregate<Node>::begin();
           node_iter != ip_iter->getActiveMortarEdge().Aggregate<Node>::end();
           ++node_iter ) {
        std::vector<Node*>::iterator iter = std::find( node_vector->begin(), node_vector->end(), &(*node_iter) );
        if( iter == node_vector->end() )
          node_vector->push_back( &(*node_iter) );
      }
    } else {
      delete node_vector;
      return( 0 ); // This happens if we call this routine bevore first NPP
    }
  }

  return( node_vector );
}

//------------------------------------------------------------------
unsigned MortarElement::getNonMortarNodeAmountLA()
{
  unsigned non_mortar_node_amount = 0;

  if( non_mortar_edge_vector_[MAIN] == 0)
    return( 0 );

  non_mortar_node_amount = getNonMortarEdge()->getType().getShape().getNodeAmount();

  //@fixme: following only works for linear edge shape functions
  if( non_mortar_edge_vector_[PRECEDING] != 0)
    non_mortar_node_amount += 1;
  if( non_mortar_edge_vector_[FOLLOWING] != 0)
    non_mortar_node_amount += 1;

  return( non_mortar_node_amount );
}    

//------------------------------------------------------------------
unsigned MortarElement::getActiveIntegrationPoints()
{
  unsigned non_mortar_node_amount = 0;

  if( non_mortar_edge_vector_[MAIN] == 0)
    return( 0 );

  for( Iterator<BoundaryIntegrationPoint> ip_iter = (*non_mortar_edge_vector_[MAIN]).Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != (*non_mortar_edge_vector_[MAIN]).Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( ! ip_iter -> getActive() )
      continue;
    non_mortar_node_amount += 1;
  }

  return( non_mortar_node_amount );
}

//------------------------------------------------------------------
unsigned MortarElement::getActiveNonMortarNodeAmount()
{
  unsigned active_node_amount = 0;
  for( Iterator<Node> non_mortar_node_iter = getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter ) {
    // if property isn't defined it can't be active!
    if( non_mortar_node_iter -> hasProperty("mortar_active") ) {
      if( non_mortar_node_iter -> getProperty<bool>("mortar_active") )
        ++active_node_amount;
    }
  }
  return( active_node_amount );
}

//------------------------------------------------------------------
void MortarElement::resetNodalData( )
{
  for( Iterator<Node> node_iter = getNonMortarEdge()->Aggregate<Node>::begin();
       node_iter != getNonMortarEdge()->Aggregate<Node>::end();
       ++node_iter ) {
    if( ! node_iter -> hasProperty("mortar_normal_condition_value") )
      node_iter -> addProperty("mortar_normal_condition_value", boost::any((double)0.));
    else
      node_iter -> getProperty<double>("mortar_normal_condition_value") = 0.;

    if( ! node_iter -> hasProperty("mortar_tangential_condition_value") )
      node_iter -> addProperty("mortar_tangential_condition_value", boost::any((double)0.));
    else
      node_iter -> getProperty<double>("mortar_tangential_condition_value") = 0.;
    
    if( ! node_iter -> hasProperty("mortar_active") ) {
      node_iter -> addProperty("mortar_active", boost::any((bool)false));
      if( contact_.getFriction() ) {
        node_iter -> addProperty("slide", boost::any((int)0));
      }
    }
  }
}

//------------------------------------------------------------------
bz::Array<double,1>* MortarElement::getAveragingTensor( double r )
{
  // @fixme: only for linear shpae functions!
  unsigned non_mortar_node_amount = getNonMortarNodeAmountLA();
  // @fixme: only for linear mortar edge!
  unsigned mortar_node_amount = 2;

  bz::Array<double,1>* averaging_tensor = new bz::Array<double,1>(non_mortar_node_amount+mortar_node_amount);
  // @fixme:             |<- only for linear shape
  bz::Array<double,2> A( 2 ,non_mortar_node_amount+mortar_node_amount);
  A = 0.;

  // @fixme: Does the FOLLOWING and PRECEDING edge do have the same shape?
  bz::Array<double,2>* der_shape_plus = non_mortar_edge_vector_[MAIN] -> getType().getShape().derivativeShapeTensor(+1.);
  bz::Array<double,2>* der_shape_minus = non_mortar_edge_vector_[MAIN] -> getType().getShape().derivativeShapeTensor(-1.);
  bz::Array<double,1>* shape = non_mortar_edge_vector_[MAIN] -> getType().getShape().shapeTensor( r );
  
  int preceding_offset = 0;
  if( non_mortar_edge_vector_[PRECEDING] ) {
    ++preceding_offset;
//  }
//  A(0,preceding_offset) = 2 * (*der_shape_minus)(0,0);
//  A(1,preceding_offset) = 2 * (*der_shape_plus)(0,0);
//
//  A(0,preceding_offset+1) = 2 * (*der_shape_minus)(1,0);
//  A(1,preceding_offset+1) = 2 * (*der_shape_plus)(1,0);

    A(0, 0) = (*der_shape_plus)(0,0);

    A(0, preceding_offset) = (*der_shape_minus)(0,0) + (*der_shape_plus)(1,0);

    A(1, preceding_offset) = (*der_shape_plus)(0,0);
    if( non_mortar_edge_vector_[FOLLOWING] == 0)
      A(1, preceding_offset) *= 2.;
   
  } else {
    A(0, preceding_offset) = 2 * (*der_shape_minus)(0,0);
    A(1, preceding_offset) = (*der_shape_plus)(0,0);

    if( non_mortar_edge_vector_[FOLLOWING] == 0)
      A(1, preceding_offset) *= 2.;
  }


  if( non_mortar_edge_vector_[FOLLOWING] ) {
    A(1, preceding_offset + 2) = (*der_shape_minus)(1,0);

    A(1, preceding_offset + 1) = (*der_shape_minus)(0,0) + (*der_shape_plus)(1,0);

    A(0, preceding_offset + 1) = (*der_shape_minus)(1,0);
    if( non_mortar_edge_vector_[PRECEDING] == 0)
      A(0, preceding_offset + 1) *= 2.;
  
  } else {
    A(0, preceding_offset + 1) = (*der_shape_minus)(1,0);
    A(1, preceding_offset + 1) = 2 * (*der_shape_plus)(1,0);

    if( non_mortar_edge_vector_[PRECEDING] == 0)
      A(0, preceding_offset + 1) *= 2;
  }

  (*averaging_tensor) = 0.5 * bz::sum( (*shape)(bzt::j) * A(bzt::j,bzt::i) , bzt::j );

  delete der_shape_plus;
  delete der_shape_minus;
  delete shape;

  return( averaging_tensor );
}

//------------------------------------------------------------------
bz::Array<double,2>* MortarElement::getEulerianPointTensorLA()
{
  // @ fixme: only 2D and linear shape functions
  int non_mortar_node_amount = getNonMortarNodeAmountLA();
  int mortar_node_amount = 2;

  bz::Array<double,2>* node_point_tensor =
    new bz::Array<double,2>(2, non_mortar_node_amount + mortar_node_amount);
  (*node_point_tensor) = 0.;

  int preceding_offset = 0;
  if( non_mortar_edge_vector_[PRECEDING] != 0) {
    ++preceding_offset;
    (*node_point_tensor)(0,0) = dynamic_cast<PointAggregate&>(non_mortar_edge_vector_[PRECEDING]->Aggregate<Node>::front())
      .getEulerianPoint().getCoordinate(0);
    (*node_point_tensor)(1,0) = dynamic_cast<PointAggregate&>(non_mortar_edge_vector_[PRECEDING]->Aggregate<Node>::front())
      .getEulerianPoint().getCoordinate(1);
  }

  int col_count = preceding_offset;
  for( Iterator<Node> non_mortar_node_iter = getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter ) {
    for( int row_count = 0; row_count != 2; ++row_count ) {
      (*node_point_tensor)(row_count,col_count) +=
        dynamic_cast<PointAggregate&>(*non_mortar_node_iter).getEulerianPoint().getCoordinate(row_count);
    }
    ++col_count;
  } 

  if( non_mortar_edge_vector_[FOLLOWING] != 0) {
    (*node_point_tensor)(0,preceding_offset+2) = dynamic_cast<PointAggregate*>(non_mortar_edge_vector_[FOLLOWING]->getLastNode())
      -> getEulerianPoint().getCoordinate(0);
    (*node_point_tensor)(1,preceding_offset+2) = dynamic_cast<PointAggregate*>(non_mortar_edge_vector_[FOLLOWING]->getLastNode())
      -> getEulerianPoint().getCoordinate(1);
  }

  return( node_point_tensor );
}

//------------------------------------------------------------------
void MortarElement::prettyPrint()
{
  std::cout << "Mortar Element: "<< getNonMortarEdge()->getNumber() <<"\t | (";
  if( hasNonMortarEdge(PRECEDING) )
    std::cout << getNonMortarEdge(PRECEDING)->getNumber(); 
  else
    std::cout << "-";
  std::cout << ")\t"
            << getNonMortarEdge()->getNumber() << "\t(";
  if( hasNonMortarEdge(FOLLOWING) )
    std::cout << getNonMortarEdge(FOLLOWING)->getNumber(); 
  else
    std::cout << "-";
  std::cout << ")" << std::endl;
}
