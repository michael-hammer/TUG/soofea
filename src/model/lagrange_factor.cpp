/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "lagrange_factor.h"

//------------------------------------------------------------------
LagrangeFactor::LagrangeFactor( const LagrangeFactor& src )
{
  normal_ = src.normal_;
  r_ = src.r_;
  s_ = src.s_;
}

//------------------------------------------------------------------
void LagrangeFactor::set( double value , CoordSys::CoordID id )
{
  switch( id ) {
  case CoordSys::NORMAL:
    normal_ = value;
    break;
  case CoordSys::TANGENT_R:
    r_ = value;
    break;
  case CoordSys::TANGENT_S:
    s_ = value;
    break;
  case CoordSys::X:
  case CoordSys::Y:
  case CoordSys::Z:
  case CoordSys::PHI:
  case CoordSys::PSI:
  case CoordSys::THETA:
  case CoordSys::NAC:
  default:
    throw EXC(CalcException) << "You are trying to set an CoordID in LagrangeFactor which is not suitable!" << Exception::endl;
  }
}

//------------------------------------------------------------------
double LagrangeFactor::get( CoordSys::CoordID id )
{
  switch( id ) {
  case CoordSys::NORMAL:
    return(normal_);
    break;
  case CoordSys::TANGENT_R:
    return(r_);
    break;
  case CoordSys::TANGENT_S:
    return(s_);
    break;
  case CoordSys::X:
  case CoordSys::Y:
  case CoordSys::Z:
  case CoordSys::PHI:
  case CoordSys::PSI:
  case CoordSys::THETA:
  case CoordSys::NAC:
  default:
    throw EXC(CalcException) << "You are trying to get an CoordID in LagrangeFactor which is not suitable!" << Exception::endl;
  }
  return( 0. );
}

//------------------------------------------------------------------
LagrangeFactor& LagrangeFactor::operator=( const LagrangeFactor& src )
{
  (*this) = src;
  return( *this );
}

//------------------------------------------------------------------
LagrangeFactor& LagrangeFactor::operator=( const double src )
{
  normal_ = 0.;
  r_ = 0.;
  s_ = 0.;
  return( *this );
}
