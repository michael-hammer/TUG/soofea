#ifndef boundary_h__
#define boundary_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <vector>
#include <map>
#include <float.h>

#include "base/id_object.h"
#include "base/numbered_object.h"
#include "base/template/vector_aggregate.h"

#include "geometry/arc.h"
#include "geometry/coord_sys.h"
#include "geometry/vector.h"
#include "geometry/boundary_geometry.h"

#include "boundary_component.h"

#include "dof_load/load.h"
#include "contact.h"

//------------------------------------------------------------------
class Boundary : 
  public NumberedObject,
  public VectorAggregate<BoundaryComponent>,
  public VectorAggregate<Node>
{
public:
  Boundary(numbered boundary_number,
           std::vector<numbered>* target_number_vector,
           IDObject::ID target_type,
           numbered loop_number) :
    NumberedObject(boundary_number),
    VectorAggregate<BoundaryComponent>(&target_vector_),
    VectorAggregate<Node>(&node_vector_),
    target_number_vector_(target_number_vector),
    target_type_(target_type),
    contact_side_(Contact::NACS),
    loop_number_(loop_number) {}

  virtual ~Boundary() {}

  inline IDObject::ID getTargetType()
  { return( target_type_ ); }

  numbered getLoopNumber() const
  { return( loop_number_ ); }

protected:
  friend class Model;

  std::vector<BoundaryComponent*> target_vector_;
  std::vector<Node*> node_vector_;

  std::vector<numbered>* target_number_vector_;
  IDObject::ID target_type_;
  Contact::ContactSide contact_side_;
  numbered loop_number_;

  std::vector<numbered>& getTargetNumberVector()
  { return( *target_number_vector_ ); }

  void setTargets(std::vector<BoundaryComponent*>& target_vector);
  void setNodes(std::set<Node*>& node_set);
};

#endif // boundary_h__
