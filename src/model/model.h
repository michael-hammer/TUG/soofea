#ifndef model_h___
#define model_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <map>

#include "geometry/cartesian_coord_sys.h"
#include "edge.h"
#include "face.h"
#include "element.h"
#include "mortar_element.h"
#include "boundary.h"
#include "global.h"
#include "contact.h"
#include "material.h"
#include "time_bar.h"
#include "shape/triangle_shape.h"
#include "shape/brick_shape.h"
#include "shape/quad_shape.h"
#include "template/model_aggregate.h"

typedef std::map<numbered,Node*> NodeMap;
typedef std::map<numbered,Edge*> EdgeMap;
typedef std::map<numbered,Face*> FaceMap;
typedef std::map<numbered,EdgeType*> EdgeTypeMap;
typedef std::map<numbered,Element*> ElementMap;
typedef std::map<numbered,Type*> TypeMap;
typedef std::map<numbered,MortarElement*> MortarElementMap;
typedef std::map<numbered,Material*> MaterialMap;
typedef std::map<numbered,Boundary*> BoundaryMap;

//------------------------------------------------------------------
/**
   This class contains the whole FE Model with its Node, Edge, 
   Face, Element and all the other needed essentials to shape the FE
   Model.
 */
class Model :
  public ModelAggregate<Node>,
  public ModelAggregate<Element>,
  public ModelAggregate<Type>,
  public ModelAggregate<MortarElement>,
  public ModelAggregate<Edge>,    
  public ModelAggregate<Face>,
  public ModelAggregate<EdgeType>,
  public ModelAggregate<Boundary>,
  public ModelAggregate<Material>
{
public:
  /**
     Default constructor which is responsible for creating all maps.
   */
  inline Model() :
    ModelAggregate<Node>(&node_map_),
    ModelAggregate<Element>(&element_map_),
    ModelAggregate<Type>(&type_map_),
    ModelAggregate<MortarElement>(&mortar_element_map_),
    ModelAggregate<Edge>(&edge_map_),
    ModelAggregate<Face>(&face_map_),
    ModelAggregate<EdgeType>(&edge_type_map_),
    ModelAggregate<Boundary>(&boundary_map_),
    ModelAggregate<Material>(&material_map_),
    global_(0),
    contact_(0),
    next_element_number_(1),
    next_face_number_(1),
    next_edge_number_(1),
    next_node_number_(1)
  {}

  /**
     Destructor - he is repsonsible for destructing <b>all</b> 
     compononents (objects) of the FE Model. (Node, Edge, Element,
     aso.)
   */
  ~Model();

  /**
     sets the Global object in the Model
     
     @param global Global object created in the io library
     @see Global
   */
  void setGlobal(Global* global);

  inline Global& getGlobal() const
  {return( *global_ );}

  /**
     sets the world CoordSys

     @pre It's important that Model::setGlobal() was called bevore!
     @see CoordSys
   */
  CoordSys* createWorldCoordSys();

  static inline CoordSys* getWorldCoordSys()
  {return( world_coord_sys_ );}

  TimeBar& getTimeBar() { return( time_bar_ ); }

  void setContact(Contact* contact);
  
  inline Contact& getContact() const
  {return( *contact_ );}

  void addElement(Element& element);

  void addMortarElement(MortarElement& element);

  MortarElement* getMortarElement( Edge& non_mortar_edge );

  /**
     Here the reference is taken because we proof if the given edge
     is new or not. If the edge already exists, "Edge*& edge" is
     assigned to the existing edge.
     
     @see Edge
   */
  void addEdge(Edge*& edge);

  void addEdge(Edge& edge);

  void addFace(Face*& face);

  void addFace(Face& face);

  void addNode(Node& node);
  
  void addMaterial(Material& material);

  void addType(Type& type);

  void addBoundary(Boundary& boundary);

  std::vector<Edge*>* getAdjacentEdges( const Node& node );

  //  void setSubComponentsOnBoundary();

  //  void distributeLoad();

  void distributeContact();

  std::vector<Node*>* getBoundaryNeighborhood( const Node& node, unsigned distance );

  void clearArchive();

  void prettyPrint();

protected:
  Global *global_;
  static CoordSys *world_coord_sys_;
  Contact* contact_;
  TimeBar time_bar_;

  NodeMap node_map_;
  EdgeMap edge_map_;
  FaceMap face_map_;
  EdgeTypeMap edge_type_map_;
  ElementMap element_map_;
  TypeMap type_map_;
  MortarElementMap mortar_element_map_;
  BoundaryMap boundary_map_;
  MaterialMap material_map_;

  numbered next_element_number_;
  numbered next_face_number_;
  numbered next_edge_number_;
  numbered next_node_number_;
};

#endif // model_h___
