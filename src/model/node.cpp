/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "node.h"

//------------------------------------------------------------------
Node::~Node()
{
  for( DOFLoadMap::iterator iter = dof_load_map_.begin();
       iter != dof_load_map_.end() ;
       ++iter) {
    if( (iter->second).first )
      delete (iter->second).first;
    if( (iter->second).second )
      delete (iter->second).second;
  }

  if( hasProperty( "average_tangent" ) )
    delete getProperty<Vector*>( "average_tangent" );
}

//------------------------------------------------------------------
void Node::prettyPrint() const
{
  std::cout << "| Node: " << number_ << std::endl;
  std::cout << "| Lagrange: ";
  point_lagrange_.prettyPrint();
  std::cout << "| Euler:    ";
  point_euler_.prettyPrint();
  DOF* disp_dof = getDOF( IDObject::DISPLACEMENT );
  if( disp_dof )
    disp_dof->prettyPrint();
  DOF* lagrange_dof = getDOF( IDObject::LAGRANGE );
  if( lagrange_dof )
    lagrange_dof->prettyPrint();
}

//------------------------------------------------------------------
DOF* Node::getDOF(IDObject::ID type) const
{
  if( dof_load_map_.empty() ) {
    return(0);
  }
  
  if( dof_load_map_.find( type ) != dof_load_map_.end() )
    return( dof_load_map_.find(type)->second.first );
  
  LOG(Logger::DEBUG) << "Node::getDOF() : Type '" << IDObject::resolveID(type) << "' could not be found!";
  return( 0 );
}

//------------------------------------------------------------------
Load* Node::getLoad(IDObject::ID load_type) const
{
  IDObject::ID type = Load::resolveLoadTypeToDOFType( load_type );
  if( dof_load_map_.empty() ) {
    return( 0 );
  }
  
  if( dof_load_map_.find( type ) != dof_load_map_.end() )
    return( dof_load_map_.find(type)->second.second );

  LOG(Logger::DEBUG) << "Node::getLoad() : Type '" << IDObject::resolveID(type) << "' could not be found!";

  return( 0 );
}

//------------------------------------------------------------------
void Node::disableDOF(IDObject::ID type)
{
  if( (dof_load_map_.empty()) )
    return;

  DOF* dof = getDOF( type );
  if( dof )
    dof->setEnabled(false);
}

//------------------------------------------------------------------
void Node::addDisplacementDOF()
{
  if( ! getDOF(IDObject::DISPLACEMENT) ) {
    dof_load_map_[IDObject::DISPLACEMENT] = std::make_pair( new DisplacementDOF(point_lagrange_.getCoordSys(),
                                                                                this->getNumber(),this,
                                                                                0.,0.,0.),
                                                            new ForceLoad(point_lagrange_.getCoordSys(),
                                                                          this->getNumber(),this,
                                                                          0.,0.,0.) );
  } else {
    getDOF(IDObject::DISPLACEMENT)->setEnabled(true);
  }
}

//------------------------------------------------------------------
void Node::addTemperatureDOF()
{
  if( ! getDOF(IDObject::TEMPERATURE) ) {
    dof_load_map_[IDObject::TEMPERATURE] = std::make_pair( new TemperatureDOF(this->getNumber(),this,
                                                                              0.),
                                                           new HeatFluxLoad(this->getNumber(),this,
                                                                            0.) );
  } else {
    getDOF(IDObject::TEMPERATURE)->setEnabled(true);
  }
}

//------------------------------------------------------------------
void Node::addLagrangeDOF( bool enabled, bool stick )
{
  DOF* dof = getDOF(IDObject::LAGRANGE);
  if( ! dof ) {
    dof_load_map_[IDObject::LAGRANGE] = std::make_pair( new LagrangeDOF(point_lagrange_.getCoordSys(), enabled, stick, 
                                                                        this->getNumber(),this,
                                                                        0.,0.,0.),
                                                        (Load*)0 ); // There is no work conjugated load for a Lagrange DOF
  } else {
    dynamic_cast<LagrangeDOF*>(dof) -> getValue(CoordSys::NORMAL) -> setEnabled( enabled );
    if(stick) 
      dynamic_cast<LagrangeDOF*>(dof) -> enableTangential();
    else
      dynamic_cast<LagrangeDOF*>(dof) -> disableTangential();
  }
}

//------------------------------------------------------------------
void Node::setConstraintLoadZero()
{
  for( DOFLoadMap::iterator iter = dof_load_map_.begin();
       iter != dof_load_map_.end() ;
       ++iter) {
    if( iter->second.second ) { // Lagrange DOF do not have a corresponding Load
      for( Iterator<LoadValue> value_iter = iter->second.second->Aggregate<LoadValue>::begin() ;
           value_iter != iter->second.second->Aggregate<LoadValue>::end() ;
           ++value_iter ) {
        if( value_iter -> getConstraint() )
          value_iter -> setExternal( 0. , true );
      }
    }
  }
}

//------------------------------------------------------------------
void Node::resetInternalLoad()
{
  for( DOFLoadMap::iterator iter = dof_load_map_.begin();
       iter != dof_load_map_.end() ;
       ++iter) {
    if( iter->second.second ) { // Lagrange DOF do not have a corresponding Load
      for( Iterator<LoadValue> value_iter = iter->second.second->Aggregate<LoadValue>::begin() ;
           value_iter != iter->second.second->Aggregate<LoadValue>::end() ;
           ++value_iter ) {
        value_iter -> resetInternal();
      }
    }
  }  
}

//------------------------------------------------------------------
void Node::unsetConstraintDOF()
{
  for( DOFLoadMap::iterator iter = dof_load_map_.begin();
       iter != dof_load_map_.end() ;
       ++iter) {
    for( Iterator<DOFValue> value_iter = iter->second.first->Aggregate<DOFValue>::begin() ;
         value_iter != iter->second.first->Aggregate<DOFValue>::end() ;
         ++value_iter ) {
      if( value_iter -> getConstraint() )
        value_iter -> setConstraint(false);
    }
  }
}

//------------------------------------------------------------------
void Node::setConstraintDOFZero()
{
  for( DOFLoadMap::iterator iter = dof_load_map_.begin();
       iter != dof_load_map_.end() ;
       ++iter) {
    for( Iterator<DOFValue> value_iter = iter->second.first->Aggregate<DOFValue>::begin() ;
         value_iter != iter->second.first->Aggregate<DOFValue>::end() ;
         ++value_iter ) {
      if( value_iter -> getConstraint() )
        value_iter -> setIncremental( 0. , true );
    }
  }
}

//------------------------------------------------------------------
void Node::updatePoint(TimeStamp time_stamp, double x, double y, double z)
{
  DOFValue* dof_value = 0;
  if( getDOF(IDObject::DISPLACEMENT) != 0 ) {
    switch( point_euler_.getCoordSys() -> getDimension() ) {
    case 3:
      dof_value = getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Z);
      point_euler_.setCoordinate(point_lagrange_.getCoordinate(CoordSys::Z) + dof_value->getValue(),
				 CoordSys::Z);
      dof_value -> archive( time_stamp );
      getLoad( IDObject::FORCE ) -> getValue(CoordSys::Z) -> archive( time_stamp );
    case 2:
      dof_value = getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y);
      point_euler_.setCoordinate(point_lagrange_.getCoordinate(CoordSys::Y) + dof_value->getValue(),
				 CoordSys::Y);
      dof_value -> archive( time_stamp );
      getLoad( IDObject::FORCE ) -> getValue(CoordSys::Y) -> archive( time_stamp );
    default:
      dof_value = getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X);
      point_euler_.setCoordinate(point_lagrange_.getCoordinate(CoordSys::X) + dof_value->getValue(),
				 CoordSys::X);
      dof_value -> archive( time_stamp );
      getLoad( IDObject::FORCE ) -> getValue(CoordSys::X) -> archive( time_stamp );
      break;
    }
  }
}

//------------------------------------------------------------------
void Node::updateTemperature(TimeStamp time_stamp, double t)
{
  if(getDOF(IDObject::TEMPERATURE) != 0 ) {
    temperature_euler_ = temperature_lagrange_ + getDOF(IDObject::TEMPERATURE)->getValue(CoordSys::NAC)->getValue();
    getDOF(IDObject::TEMPERATURE) -> getValue(CoordSys::NAC)->archive( time_stamp );
    getLoad( IDObject::FORCE ) -> getValue(CoordSys::X) -> archive( time_stamp );
  }
}

//------------------------------------------------------------------
void Node::updateLagrangeFactor( TimeStamp time_stamp,
				 double lambda_n,
				 double lambda_r,
				 double lambda_s )
{
  DOFValue* dof_value = 0;
  if(getDOF(IDObject::LAGRANGE) != 0 ) {
    dof_value = getDOF( IDObject::LAGRANGE )->getValue(CoordSys::NORMAL);
    lagrange_factor_.set( dof_value->getValue() , CoordSys::NORMAL );
    if( dof_value->enabled() )
      dof_value -> archive( time_stamp );
    else
      dof_value -> removeFromArchive( time_stamp );
    if( hasProperty("slide") ) {
      if( getProperty<int>("slide") == 0 ) {
        switch( point_euler_.getCoordSys() -> getDimension() ) {
        case 3:
          dof_value = getDOF( IDObject::LAGRANGE )->getValue(CoordSys::TANGENT_S);
          lagrange_factor_.set( dof_value->getValue() , CoordSys::TANGENT_S );
          dof_value -> archive( time_stamp );
        case 2:
          dof_value = getDOF( IDObject::LAGRANGE )->getValue(CoordSys::TANGENT_R);
          lagrange_factor_.set( dof_value->getValue() , CoordSys::TANGENT_R );
          dof_value -> archive( time_stamp );
          break;
        }
      }
    }
  }
}

//------------------------------------------------------------------
DOFValue* Node::next( DOFValue* act_dof_value, int& index)
{
  ++index; /** @todo We should use the index information to accalerate
               the algorithm. This iteration algorithm is simply
               terrible. getFirst(), getNext() calls in DOF should be
               elliminated */

  DOFLoadMap::iterator dof_iter = dof_load_map_.find( act_dof_value->getDOF().getType() );
  DOFValue* next_dof_value = dof_iter->second.first->getNext( act_dof_value );
  if( ! next_dof_value ) {
    if( ++dof_iter != dof_load_map_.end() )
      next_dof_value = dof_iter->second.first->getNext( 0 );
  }

  return( next_dof_value );
}

//------------------------------------------------------------------
Iterator<DOFValue> Node::begin( DOFValue* )
{
  if( dof_load_map_.empty() )
    return( Aggregate<DOFValue>::end() );
  else
    return( Iterator<DOFValue>( this , dof_load_map_.begin()->second.first->getNext(0) , 0 ) );
}

//------------------------------------------------------------------
Point Node::getEulerianPoint(TimeStamp& time_stamp, double deformation_factor)
{
  double coord_x = 0., coord_y = 0., coord_z = 0.;

  switch( point_lagrange_.getCoordSys()->getDimension() ) {
  case 3:
    coord_z = point_lagrange_.getCoordinate(CoordSys::Z) + 
      (getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Z) -> getValue( time_stamp ))*deformation_factor ;
  case 2:
    coord_y = point_lagrange_.getCoordinate(CoordSys::Y) + 
      (getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) -> getValue( time_stamp ))*deformation_factor ;
    coord_x = point_lagrange_.getCoordinate(CoordSys::X) + 
      (getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) -> getValue( time_stamp ))*deformation_factor ;
    break;
  }

  return( Point( point_lagrange_.getCoordSys(), coord_x, coord_y, coord_z ) );
}

//------------------------------------------------------------------
const Point Node::getEulerianPoint(TimeStamp& time_stamp, double deformation_factor) const
{
  double coord_x = 0., coord_y = 0., coord_z = 0.;

  switch( point_lagrange_.getCoordSys()->getDimension() ) {
  case 3:
    coord_z = point_lagrange_.getCoordinate(CoordSys::Z) + 
      (getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Z) -> getValue( time_stamp ))*deformation_factor ;
  case 2:
    coord_y = point_lagrange_.getCoordinate(CoordSys::Y) + 
      (getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) -> getValue( time_stamp ))*deformation_factor ;
    coord_x = point_lagrange_.getCoordinate(CoordSys::X) + 
      (getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) -> getValue( time_stamp ))*deformation_factor ;
    break;
  }

  return( Point( point_lagrange_.getCoordSys(), coord_x, coord_y, coord_z ) );
}

//------------------------------------------------------------------
void Node::addAdjacentEdge(Edge* edge)
{
  if( std::find( adjacent_edge_list_.begin(), adjacent_edge_list_.end(), edge ) == adjacent_edge_list_.end() )
    adjacent_edge_list_.push_back( edge );
}

//------------------------------------------------------------------
void Node::addAdjacentFace(Face* face)
{
  if( std::find( adjacent_face_list_.begin(), adjacent_face_list_.end(), face ) == adjacent_face_list_.end() )
    adjacent_face_list_.push_back( face );
}

//------------------------------------------------------------------
void Node::addAdjacentElement(Element* element)
{
  if( std::find( adjacent_element_list_.begin(), adjacent_element_list_.end(), element ) == adjacent_element_list_.end() )
    adjacent_element_list_.push_back( element );
}
