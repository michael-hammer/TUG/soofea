/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "dof.h"

//------------------------------------------------------------------
DOF::DOF(IDObject::ID dof_type, numbered node_number, Node* node):
  ValueContainer<DOFValue>( dof_type, &dof_value_vector_ , node_number, node)
{}

//------------------------------------------------------------------
DOF::~DOF()
{
  for( std::vector<DOFValue*>::iterator iter = dof_value_vector_.begin(); 
       iter != dof_value_vector_.end() ; 
       ++iter)
    delete (*iter);
}

//------------------------------------------------------------------
void DOF::clearArchive()
{
  for( std::vector<DOFValue*>::iterator iter = dof_value_vector_.begin(); 
       iter != dof_value_vector_.end() ; 
       ++iter)
    (*iter) -> clearArchive();
}

//------------------------------------------------------------------
void DOF::setEnabled(bool enabled)
{
  for( std::vector<DOFValue*>::iterator iter = dof_value_vector_.begin(); 
       iter != dof_value_vector_.end() ; 
       ++iter)
    (*iter) -> setEnabled( enabled );
}
