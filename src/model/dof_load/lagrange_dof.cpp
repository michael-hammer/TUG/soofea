/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "lagrange_dof.h"

//------------------------------------------------------------------
LagrangeDOF::LagrangeDOF(CoordSys* coord_sys, bool enabled, bool stick, 
                         numbered node_number, Node* node,
                         double lambda_N, double lambda_R, double lambda_S) :
  DOF(DOF::LAGRANGE,node_number,node),
  stick_(stick),
  coord_sys_(coord_sys)
{
  dof_value_vector_.push_back(new DOFValue(lambda_N, 0.0, false, CoordSys::NORMAL,this));
  dof_value_vector_[0]->setEnabled( enabled );
  switch( coord_sys -> getDimension() ) {
  case 2:
    dof_value_vector_.push_back(new DOFValue(lambda_R, 0.0, false, CoordSys::TANGENT_R,this));
    if( !stick ) {
      dof_value_vector_[1]->setEnabled( false );
    }
    break;				     
  case 3:				     
    dof_value_vector_.push_back(new DOFValue(lambda_R, 0.0, false, CoordSys::TANGENT_R,this));
    dof_value_vector_.push_back(new DOFValue(lambda_S, 0.0, false, CoordSys::TANGENT_S,this));
    if( !stick ) {
      dof_value_vector_[1]->setEnabled( false );
      dof_value_vector_[2]->setEnabled( false );
    }
    break;
  }
}

//------------------------------------------------------------------
int LagrangeDOF::resolveCoordIDToIndex(CoordSys::CoordID coord_id) const
{
  switch( coord_id ) {
  case CoordSys::NORMAL:
    return( 0 );
    break;
  case CoordSys::TANGENT_R:
    return( 1 );
    break;
  case CoordSys::TANGENT_S:
    if( coord_sys_ -> getDimension() > 2 )
      return( 2 );
    break;
  default:
    return( -1 );
    break;
  }
  return( -1 );
}

//------------------------------------------------------------------
void LagrangeDOF::disableTangential()
{
  if( dof_value_vector_.size() > 1 ) { // Else they do not exist
    switch( coord_sys_ -> getDimension() ) {
    case 3:
      dof_value_vector_[2]->setEnabled(false);
    case 2:
      dof_value_vector_[1]->setEnabled(false);
    }
  }
}

//------------------------------------------------------------------
void LagrangeDOF::enableTangential()
{
  switch( coord_sys_ -> getDimension() ) {
  case 3:				     
    dof_value_vector_[2]->setEnabled(true);
  case 2:
    dof_value_vector_[1]->setEnabled(true);
  }
}

//------------------------------------------------------------------
void LagrangeDOF::prettyPrint()
{
  for( std::vector<DOFValue*>::iterator iter = dof_value_vector_.begin();
       iter != dof_value_vector_.end();
       ++iter )
    (*iter) -> prettyPrint();
}

//------------------------------------------------------------------
void LagrangeDOF::addResult( const DOF& dof, const TimeStamp& time_stamp )
{
  const LagrangeDOF& lagr_dof = dynamic_cast<const LagrangeDOF&>(dof);

  if( lagr_dof.getValue(CoordSys::NORMAL)->enabled() ) { // only enabled DOFs are archived!
    getValue( CoordSys::NORMAL )->archive( time_stamp, lagr_dof.getValue( CoordSys::NORMAL )->getValue() ) ;
  }
  if( lagr_dof.getStick() ) {
    switch( coord_sys_ -> getDimension() ) {
    case 3:
      if( lagr_dof.getValue(CoordSys::TANGENT_S)->enabled() )
        getValue( CoordSys::TANGENT_S )->archive( time_stamp, lagr_dof.getValue( CoordSys::TANGENT_S )->getValue() ) ;
    case 2:
      if( lagr_dof.getValue(CoordSys::TANGENT_R)->enabled() )
        getValue( CoordSys::TANGENT_R )->archive( time_stamp, lagr_dof.getValue( CoordSys::TANGENT_R )->getValue() ) ;
      break;				     
    }
  }
}
