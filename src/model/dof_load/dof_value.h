#ifndef dof_value_h__
#define dof_value_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <map>

#include "exceptions.h"
#include "base/logger.h"

#include "../time_stamp.h"
#include "value.h"

class DOF;
class Node;

//------------------------------------------------------------------
class DOFValue :
  public Value
{
public:
  /**
     This is the default constructor
   */
  DOFValue( double value, 
            double incremental,
            bool constrained, 
            CoordSys::CoordID coord_id,            
            DOF* dof ) :
    Value( constrained, coord_id ),
    value_(value),
    incremental_(incremental),
    enabled_(true),
    dof_(dof),
    position_in_SLE_(-1)
  {}
  
  /**
     Constructor used for PrescribedDOF

     ATTENTION: DOF object can not be provided
   */
  DOFValue( double value, 
            CoordSys::CoordID coord_id ) :
    Value( true, coord_id ),
    value_(value),
    incremental_(value),
    enabled_(true),
    dof_(0), // ATTENTION
    position_in_SLE_(-1)
  {}

  DOFValue( const DOFValue& src ) :
    Value( src ),
    value_(src.value_),
    incremental_(src.value_),
    enabled_(src.enabled_),
    dof_(src.dof_),
    position_in_SLE_(src.position_in_SLE_)
  {}

  ~DOFValue() {}

  void setIncremental(double incremental, bool constrained = false);
  inline double getIncremental() {return(incremental_);}

  void setValue(double value, bool constrained = false);
  inline double getValue() const {return(value_);}
  double getValue(const TimeStamp& time_stamp) const;

  virtual void archive( const TimeStamp& time_stamp );
  virtual void archive( const TimeStamp& time_stamp , double value );
  void removeFromArchive( const TimeStamp& time_stamp );

  bool enabled() const {return(enabled_);}
  bool enabled( const TimeStamp& time_stamp ) const;

  void setEnabled(bool enabled) {enabled_=enabled;}

  void setPositionInSLE( int position )
  { position_in_SLE_ = position; }

  int getPositionInSLE()
  { return( position_in_SLE_ ); }

  void prettyPrint(bool archive=false);

protected:
  friend class Node;

  double value_;
  double incremental_;

  bool enabled_;

  //------------------------------------------------------------------
  /** @todo inverse (upwards) access - not nice! */
  inline DOF& getDOF()
  {return( *dof_ );}

  //------------------------------------------------------------------
  /** @todo inverse (upwards) access - not nice! */
  inline const DOF& getDOF() const
  {return( *dof_ );}

  DOF* dof_;
  int position_in_SLE_;
};

#endif // dof_value_h__
