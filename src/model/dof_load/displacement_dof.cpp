/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "displacement_dof.h"

//------------------------------------------------------------------
DisplacementDOF::DisplacementDOF(CoordSys* coord_sys, 
                                 numbered node_number, Node* node,
                                 double u, double v, double w):
  DOF(DOF::DISPLACEMENT,node_number,node),
  coord_sys_(coord_sys)
{
  switch( coord_sys -> getDimension() ) {
  case 3:				     
    dof_value_vector_.push_back(new DOFValue(w, 0.0, false, CoordSys::Z,this));
  case 2:
    dof_value_vector_.push_back(new DOFValue(u, 0.0, false, CoordSys::X,this));
    dof_value_vector_.push_back(new DOFValue(v, 0.0, false, CoordSys::Y,this));
    break;				     
  }  
}

//------------------------------------------------------------------
int DisplacementDOF::resolveCoordIDToIndex(CoordSys::CoordID coord_id) const
{
  switch( coord_id ) {
  case CoordSys::X:
    return( 0 );
    break;
  case CoordSys::Y:
    if( coord_sys_ -> getDimension() > 1 )
      return( 1 );
    break;
  case CoordSys::Z:
    if( coord_sys_ -> getDimension() > 2 )
      return( 2 );
    break;
  default:
    return(-1);
    break;
  }
  return( -1 );
}

//------------------------------------------------------------------
void DisplacementDOF::prettyPrint()
{
  switch( coord_sys_ -> getDimension() )
    {
    case 2:
      dof_value_vector_[0] -> prettyPrint();
      dof_value_vector_[1] -> prettyPrint(); 
      break;
    case 3:      
      dof_value_vector_[0] -> prettyPrint();
      dof_value_vector_[1] -> prettyPrint(); 
      dof_value_vector_[2] -> prettyPrint();  
      break;
    }
}

//------------------------------------------------------------------
void DisplacementDOF::addResult( const DOF& dof, const TimeStamp& time_stamp )
{
  switch( coord_sys_ -> getDimension() ) {
  case 3:
    getValue( CoordSys::Z )->archive( time_stamp, dynamic_cast<const DisplacementDOF&>(dof).getValue( CoordSys::Z )->getValue() ) ;
  case 2:
    getValue( CoordSys::X )->archive( time_stamp, dynamic_cast<const DisplacementDOF&>(dof).getValue( CoordSys::X )->getValue() ) ;
    getValue( CoordSys::Y )->archive( time_stamp, dynamic_cast<const DisplacementDOF&>(dof).getValue( CoordSys::Y )->getValue() ) ;
    break;
  }
}
