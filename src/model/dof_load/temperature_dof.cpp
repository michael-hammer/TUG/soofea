/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "temperature_dof.h"

//------------------------------------------------------------------
TemperatureDOF::TemperatureDOF(numbered node_number, Node* node, double t) :
  DOF( DOF::TEMPERATURE , node_number, node )
{
  dof_value_vector_.push_back(new DOFValue(t,0.0,false,CoordSys::NAC,this));
}

//------------------------------------------------------------------
int TemperatureDOF::resolveCoordIDToIndex(CoordSys::CoordID coord_id) const
{
  return( 0 );
}

//------------------------------------------------------------------
void TemperatureDOF::prettyPrint()
{
  dof_value_vector_[0] -> prettyPrint();
}

//------------------------------------------------------------------
void TemperatureDOF::addResult( const DOF& dof, const TimeStamp& time_stamp )
{
  getValue( CoordSys::NAC )->archive( time_stamp, dynamic_cast<const TemperatureDOF&>(dof).getValue( CoordSys::NAC )->getValue() ) ;
}
