#ifndef heat_flux_load_h___
#define heat_flux_load_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007  Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include <iostream>

#include "load.h"

//------------------------------------------------------------------
class HeatFluxLoad : 
  public Load
{
public:
  HeatFluxLoad(numbered node_number, Node* node = 0,
               double q=0) :
    Load(Load::HEAT_FLUX,node_number,node) {}

  virtual ~HeatFluxLoad() {}

  virtual void addResult( const Load& result, const TimeStamp& time_stamp );
protected:
  virtual int resolveCoordIDToIndex(CoordSys::CoordID coord_id) const;
};

#endif
