#ifndef load_h__
#define load_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <list>
#include <vector>
#include <iostream>

#include "base/numbered_object.h"
#include "geometry/vector.h"
#include "geometry/coord_sys.h"

#include "load_value.h"
#include "template/value_container.h"

class Node;

//------------------------------------------------------------------
class Load :
  public ValueContainer<LoadValue>
{
public:
  virtual ~Load();

  virtual void clearArchive();

  static IDObject::ID resolveLoadTypeToDOFType(IDObject::ID type);

  virtual void addResult( const Load& result, const TimeStamp& time_stamp ) = 0;

protected:
  Load(IDObject::ID type, numbered node_number, Node* node = 0);

  std::vector<LoadValue*> load_value_vector_;
};

#endif // load_h__
