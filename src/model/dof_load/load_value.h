#ifndef load_value_h__
#define load_value_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <map>

#include "exceptions.h"
#include "geometry/coord_sys.h"
#include "base/logger.h"

#include "../time_stamp.h"
#include "value.h"

class Load;
class Node;

//------------------------------------------------------------------
class LoadValue :
  public Value
{
public:
  LoadValue( double internal,
             double external, 
             bool constrained, 
             CoordSys::CoordID coord_id,
             Load* load ):
    Value( constrained, coord_id ),
    internal_(internal),
    external_(external),
    load_(load)
  {}

  /**
     Constructor used for PrescribedLoad

     ATTENTION: DOF object can not be provided
   */
  LoadValue( double external, 
             CoordSys::CoordID coord_id  ):
    Value( true, coord_id ),
    internal_(0),
    external_(external),
    load_(0)
  {}
  
  LoadValue( const LoadValue& src ) :
    Value( src ),
    internal_(src.internal_),
    external_(src.external_),
    load_(src.load_)
  {}

  ~LoadValue() {}

  // INTERNAL: Entries from \delta F_{int} @ iteration n
  void addInternal(double internal_increment);
  void resetInternal() {internal_=0.;}
  inline double getInternal() const {return(internal_);}
  double getInternal(const TimeStamp& time_stamp) const;

  // EXTERNAL: Directly given work conjugated external forces
  void setExternal(double external, bool constrained = true);
  inline double getExternal() const { return(external_); }

  virtual void archive( const TimeStamp& time_stamp );
  virtual void archive( const TimeStamp& time_stamp , double value );

protected:
  friend class Node;

  double internal_;
  double external_;

  Load* load_;
};

#endif
