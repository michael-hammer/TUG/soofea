/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007  Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "force_load.h"

//------------------------------------------------------------------
ForceLoad::ForceLoad(CoordSys* coord_sys,
                     numbered node_number, Node* node,
                     double f_x, double f_y, double f_z) :
  Load(Load::FORCE, node_number, node),
  coord_sys_(coord_sys)
{
  switch( coord_sys -> getDimension() ) {
  case 3:				     
    load_value_vector_.push_back(new LoadValue(f_z, 0.0, false, CoordSys::Z,this));
  case 2:
    load_value_vector_.push_back(new LoadValue(f_x, 0.0, false, CoordSys::X,this));
    load_value_vector_.push_back(new LoadValue(f_y, 0.0, false, CoordSys::Y,this));
    break;				     
  }
}

//------------------------------------------------------------------
int ForceLoad::resolveCoordIDToIndex(CoordSys::CoordID coord_id) const
{
  switch( coord_id ) {
  case CoordSys::X:
    return( 0 );
    break;
  case CoordSys::Y:
    if( coord_sys_ -> getDimension() > 1 )
      return( 1 );
    break;
  case CoordSys::Z:
    if( coord_sys_ -> getDimension() > 2 )
      return( 2 );
    break;
  default:
    return(-1);
    break;
  }
  return( -1 );
}

//------------------------------------------------------------------
void ForceLoad::addResult( const Load& load, const TimeStamp& time_stamp )
{
  switch( coord_sys_ -> getDimension() ) {
  case 3:
    getValue( CoordSys::Z )->archive( time_stamp, dynamic_cast<const ForceLoad&>(load).getValue( CoordSys::Z )->getInternal() ) ;
  case 2:
    getValue( CoordSys::X )->archive( time_stamp, dynamic_cast<const ForceLoad&>(load).getValue( CoordSys::X )->getInternal() ) ;
    getValue( CoordSys::Y )->archive( time_stamp, dynamic_cast<const ForceLoad&>(load).getValue( CoordSys::Y )->getInternal() ) ;
    break;
  }
}
