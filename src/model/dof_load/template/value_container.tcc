/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

//------------------------------------------------------------------
template<class T>
T* ValueContainer<T>::getValue(CoordSys::CoordID coord_id)
{
  int index = resolveCoordIDToIndex(coord_id);
  if( index >= 0 && index<(int)(value_vector_ -> size()) )
    return( value_vector_ -> at(index) );
  else {
    return( 0 );
  }
}

//------------------------------------------------------------------
template<class T>
const T* ValueContainer<T>::getValue(CoordSys::CoordID coord_id) const
{ 
  int index = resolveCoordIDToIndex(coord_id);
  if( index >= 0 && index<(int)(value_vector_ -> size()) )
    return( value_vector_ -> at(index) ); 
  else {
    return( 0 );
  }
}

//------------------------------------------------------------------
template<class T>
T* ValueContainer<T>::getNext(const T* act_value)
{
  if( !act_value )
    return( value_vector_->at(0) );

  typename std::vector<T*>::iterator iter = std::find( value_vector_->begin(), value_vector_->end(), act_value );
  if( ++iter == value_vector_->end() )
    return( 0 );
  else
    return( *iter );
}
