#ifndef value_container_h__
#define value_container_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>

#include "geometry/coord_sys.h"
#include "base/numbered_object.h"
#include "base/id_object.h"
#include "base/template/vector_aggregate.h"

class Node;

//------------------------------------------------------------------
template<class T>
class ValueContainer :
  public IDObject,
  public VectorAggregate<T>
{
public:
  ValueContainer( IDObject::ID type, 
		  std::vector<T*>* value_vector,
                  numbered node_number, Node* node = 0 ):
    VectorAggregate<T>( value_vector ),
    type_(type),
    node_number_(node_number),
    node_(node),
    value_vector_(value_vector) {}

  virtual T* getNext(const T* act_value);

  T* getValue(CoordSys::CoordID coord_id);

  const T* getValue(CoordSys::CoordID coord_id) const;

  Node& getNode() {return( *node_ );}

  numbered getNodeNumber() {return( node_number_);}

  inline IDObject::ID getType() const {return(type_);}

protected:
  virtual int resolveCoordIDToIndex( CoordSys::CoordID coord_id ) const = 0;

  IDObject::ID type_;
  numbered node_number_;
  Node* node_;
  std::vector<T*>* value_vector_;
};

#include "value_container.tcc"

#endif
