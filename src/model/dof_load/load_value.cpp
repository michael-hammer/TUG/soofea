/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "load_value.h"

//------------------------------------------------------------------
void LoadValue::addInternal(double internal_increment)
{
  internal_ += internal_increment; 
}

//------------------------------------------------------------------
void LoadValue::setExternal(double external, bool constrained)
{
  external_ = external; 
  constrained_ = constrained;
}

//------------------------------------------------------------------
void LoadValue::archive( const TimeStamp& time_stamp )
{
  archive( time_stamp , internal_ );
}

//------------------------------------------------------------------
void LoadValue::archive( const TimeStamp& time_stamp , double value )
{
  if( value_archive_.find(time_stamp) != value_archive_.end() )
    LOG(Logger::DEBUG) << "You overwrite a value in the Load archive!" << Logger::endl;

  value_archive_[time_stamp] = value;
}

//------------------------------------------------------------------
double LoadValue::getInternal(const TimeStamp& time_stamp) const
{
  if( time_stamp.lagrangianState() )
    return( 0. );

  std::map<const TimeStamp, double, TimeStampComp>::const_iterator iter = value_archive_.find(time_stamp);
  if( iter != value_archive_.end() )
    return( iter->second );
  else
    throw EXC(CalcException) << "You try to get an archived Load value for time_stamp: index = " << time_stamp.index() 
                             << " iteration = " << time_stamp.iteration() << " which doesn't exist!" << Exception::endl;
}
