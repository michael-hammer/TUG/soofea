/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "dof_value.h"

//------------------------------------------------------------------
void DOFValue::setIncremental(double incremental, bool constrained)
{
  incremental_ = incremental; 
  value_ += incremental;

  constrained_ = constrained; 
}

//------------------------------------------------------------------
void DOFValue::setValue(double value, bool constrained)
{
  value_ = value; 

  constrained_ = constrained;
}

//------------------------------------------------------------------
void DOFValue::archive( const TimeStamp& time_stamp )
{
  archive( time_stamp , value_ );
}

//------------------------------------------------------------------
void DOFValue::archive( const TimeStamp& time_stamp , double value )
{
  if( value_archive_.find(time_stamp) != value_archive_.end() )
    LOG(Logger::DEBUG) << "You overwrite a value in the DOF archive!" << Logger::endl;

  value_archive_[time_stamp] = value;
}

//------------------------------------------------------------------
void DOFValue::removeFromArchive( const TimeStamp& time_stamp )
{
  for( std::map<const TimeStamp, double, TimeStampComp>::iterator iter = value_archive_.begin();
       iter != value_archive_.end() ;
       ++iter ) {
    if( iter->first.index() == time_stamp.index() )
      value_archive_.erase( iter );
  }
}

//------------------------------------------------------------------
double DOFValue::getValue(const TimeStamp& time_stamp) const
{
  /// @fixme What if initial state has NON zero displacement - is this possible?
  if( time_stamp.lagrangianState() )
    return( 0. );

  std::map<const TimeStamp, double, TimeStampComp>::const_iterator iter = value_archive_.find(time_stamp);
  if( iter != value_archive_.end() )
    return( iter->second );
  else
    throw EXC(CalcException) << "You try to get an archived DOF value for time_stamp: index = " << time_stamp.index() 
                             << " iteration = " << time_stamp.iteration() << " which doesn't exist!" << Exception::endl;
}

//------------------------------------------------------------------
bool DOFValue::enabled( const TimeStamp& time_stamp ) const
{
  /// @fixme What if initial state has NON zero displacement - is this possible?
  if( time_stamp.lagrangianState() )
    return( enabled() );
  
  std::map<const TimeStamp, double, TimeStampComp>::const_iterator iter = value_archive_.find(time_stamp);
  if( iter != value_archive_.end() )
    return( true );
  else
    return( false );
}

//------------------------------------------------------------------
void DOFValue::prettyPrint(bool archive)
{
  std::cout << "| " << "coord: " << coord_id_ << "  | (constr.? " <<  constrained_ <<  ") " <<  value_;
  if( archive ) {
    std::cout << " ||";
    for( std::map<const TimeStamp, double, TimeStampComp>::iterator iter = value_archive_.begin() ;
         iter != value_archive_.end() ;
         ++iter ) {
      std::cout << " " << iter->second << "(" << iter->first.index() << "|" << iter->first.iteration() << ")";
    }
  }
  std::cout << std::endl;
}
