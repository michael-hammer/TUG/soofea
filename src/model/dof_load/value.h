#ifndef value_h__
#define value_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <map>

#include "geometry/coord_sys.h"
#include "../time_stamp.h"

//------------------------------------------------------------------
class Value
{
public:
  Value( bool constrained, CoordSys::CoordID coord_id ):
    constrained_(constrained),
    coord_id_(coord_id) 
  {}

  Value( const Value& src ):
    constrained_(src.constrained_),
    coord_id_(src.coord_id_) {}    

  virtual ~Value() {}

  inline void setConstraint(bool constrained) { constrained_ = constrained; }
  inline bool getConstraint() const {return (constrained_);}

  inline CoordSys::CoordID getCoordID() {return(coord_id_);}

  virtual void archive( const TimeStamp& time_stamp ) = 0;
  virtual void archive( const TimeStamp& time_stamp , double value ) = 0;

  virtual void clearArchive();

protected:
  bool constrained_;
  CoordSys::CoordID coord_id_;

  std::map<const TimeStamp, double, TimeStampComp> value_archive_;
};

#endif
