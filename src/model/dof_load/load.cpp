/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "load.h"

//------------------------------------------------------------------
IDObject::ID Load::resolveLoadTypeToDOFType(IDObject::ID type)
{
  switch( type ) {
  case IDObject::FORCE:
    return( IDObject::DISPLACEMENT );
  case IDObject::HEAT_FLUX:
    return( IDObject::TEMPERATURE );
  default:
    throw EXC(CalcException) << "Can not resolve the Load Type " << resolveID( type ) 
                             << " to a work conjugated DOF Type!" << Exception::endl;
  }

  return( IDObject::NAID );
}

//------------------------------------------------------------------
Load::Load(IDObject::ID type, numbered node_number, Node* node ) :
  ValueContainer<LoadValue>( type, &load_value_vector_, node_number, node )
{}

//------------------------------------------------------------------
Load::~Load()
{
  for( std::vector<LoadValue*>::iterator iter = load_value_vector_.begin(); 
       iter != load_value_vector_.end() ; 
       ++iter)
    delete (*iter);
}

//------------------------------------------------------------------
void Load::clearArchive()
{
  for( std::vector<LoadValue*>::iterator iter = load_value_vector_.begin(); 
       iter != load_value_vector_.end() ; 
       ++iter)
    (*iter) -> clearArchive();
}
