#ifndef element_integration_point_h__
#define element_integration_point_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "strain.h"
#include "stress.h"
#include "integration_point.h"
#include "math/integration_point.h"
#include "math/lin_alg.h"

#include "analyzer/linear_tangent_map/deformation_gradient.h"

//------------------------------------------------------------------
class ElementIntegrationPoint :
  public IntegrationPoint
{
public:
  ElementIntegrationPoint(Math::IntegrationPoint* int_point, 
			  double x, double y, double z, double t, CoordSys* system);

  ~ElementIntegrationPoint();

  inline bz::Array<double,2>& getStress()
  { return(stress_); }

  inline const bz::Array<double,2>& getStress() const
  { return(stress_); }

  inline bz::Array<double,2>& getLagrangianJacobian()
  { return( lagrangian_jacobian_ ); }

  inline bz::Array<double,2>& getInvLagrangianJacobian()
  { return( inverse_lagrangian_jacobian_ ); }

  inline bz::Array<double,2>& getEulerianJacobian()
  { return( eulerian_jacobian_ ); }

  inline bz::Array<double,2>& getDeformationGradient()
  { return( deformation_gradient_ ); }

  inline bz::Array<double,2>& getRightCauchyGreen()
  { return( right_cauchy_green_ ); }

  inline bz::Array<double,2>& getLagrangianStrain()
  { return( lagrangian_strain_ ); }

  inline bz::Array<double,4>& getElasticityTensor()
  { return( elasticity_tensor_ ); }

  const inline bz::Array<double,2>& getLagrangianStrain() const
  { return( lagrangian_strain_ ); }

  inline double getLagrangianDet()
  { Math::LinAlg lin_alg; return( lin_alg.det(lagrangian_jacobian_) ); }

  inline bz::Array<double,2>*& getNJ()
  { return( NJ_ ); }
protected:
  bz::Array<double,2> lagrangian_jacobian_;
  bz::Array<double,2> inverse_lagrangian_jacobian_;
  bz::Array<double,2> eulerian_jacobian_;
  bz::Array<double,2> deformation_gradient_;
  bz::Array<double,2> right_cauchy_green_;
  bz::Array<double,2> lagrangian_strain_;
  bz::Array<double,2> stress_;
  bz::Array<double,4> elasticity_tensor_;

  // calc arrays
  bz::Array<double,2> *NJ_;
};

#endif // element_integration_point_h__
