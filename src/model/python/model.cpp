#include "model.h"

namespace py = boost::python;

Node& (Model::*getNode)(int) = &Aggregate<Node>::get;
size_t (Model::*getNodeAmount)() const = &ModelAggregate<Node>::size;
Node& (NodeContainer::*getLocalNode)(int) = &Aggregate<Node>::get;
Element& (Model::*getElement)(int) = &Aggregate<Element>::get;
size_t (Model::*getElementAmount)() const = &ModelAggregate<Element>::size;
ElementType& (Model::*getElementType)(int) = &Aggregate<ElementType>::get;
Material& (Model::*getMaterial)(int) = &Aggregate<Material>::get;
size_t (Model::*getBoundaryAmount)() const = &ModelAggregate<Boundary>::size;

//------------------------------------------------------------------
inline py::object pass_through(py::object const& o) { return o; }

//------------------------------------------------------------------
template<class T, class Iterator>
struct PyIteratorWrapper
{
  static T& next( Iterator& iter )
  {
    if( (++iter).onEnd() ) {
      PyErr_SetString(PyExc_StopIteration, "No more data.");
      py::throw_error_already_set();
    }
    return( *iter );
  }

  static void wrap( const char* python_name )
  {
    py::class_<Iterator>( python_name, py::no_init )
      .def("next", next, py::return_value_policy<py::reference_existing_object>())
      .def("__iter__", pass_through)
      ;
  }
};

//------------------------------------------------------------------
template<class T, class Aggregate>
Iterator<T> getIterator(Aggregate* self)
{
  return( Iterator<T>(self,(T*)0,0) );
}

//------------------------------------------------------------------
template<class T>
std::vector<T>* listConvertor( py::list& py_list )
{
  std::vector<T>* cpp_vec = new std::vector<T>;
  for( unsigned counter = 0 ; counter != py::len(py_list) ; ++counter ) {
    cpp_vec->push_back( py::extract<T>(py_list[counter]) );
  }
  return( cpp_vec );
}

//------------------------------------------------------------------
struct PyModel
{
  //------------------------------------------------------------------
  static void setGlobal( Model& self, unsigned dimension, IDObject::ID coord_sys_id,
                         bool displacement, bool temperature,
                         IDObject::ID analysis_type_id, double convergence_criteria, 
                         IDObject::ID sle_solution_method_id )
  { self.setGlobal( new Global( dimension, coord_sys_id, displacement, temperature, 
                                analysis_type_id, convergence_criteria, sle_solution_method_id ) ); }

  //------------------------------------------------------------------
  static void addMaterial( Model& self, numbered material_number, IDObject::ID material_id )
  { self.addMaterial( *(new Material(material_number,material_id)) ); }

  //------------------------------------------------------------------
  static void addElementType( Model& self, 
                              numbered element_type_number, 
                              IDObject::ID element_type_id,
                              IDObject::ID shape_type_id,
                              unsigned shape_order,
                              py::list& py_amount_of_int_points )
  {
    std::vector<int>* amount_of_int_points = listConvertor<int>( py_amount_of_int_points );

    self.addType( *(new ElementType( element_type_number,
                                     element_type_id,
                                     shape_type_id,
                                     shape_order,
                                     amount_of_int_points )) );
  }

  //------------------------------------------------------------------
  static void addNode( Model& self, numbered node_number, double x, double y, double z, double t )
  { self.addNode( *(new Node( node_number, x , y , z , t , Model::getWorldCoordSys() )) ); }

  //------------------------------------------------------------------
  static void addElement( Model& self,
                          numbered element_number, 
                          numbered material_number,
                          py::list& py_node_number_vector,
                          numbered type_number )
  { 
    std::vector<numbered>* node_number_vector = listConvertor<numbered>( py_node_number_vector );
    self.addElement( *(new Element( element_number, material_number, node_number_vector, type_number )) ); 
  }

  //------------------------------------------------------------------
  static void addEdge( Model& self,
                       numbered number,
                       py::list& py_node_number_vector,
                       numbered type_number)
  {
    std::vector<numbered>* node_number_vector = listConvertor<numbered>( py_node_number_vector );
    self.addEdge( *(new Edge( number, node_number_vector, type_number )) );     
  }

  //------------------------------------------------------------------
  static void addFace( Model& self,
                       numbered number,
                       py::list& py_node_number_vector,
                       numbered type_number)
  {
    std::vector<numbered>* node_number_vector = listConvertor<numbered>( py_node_number_vector );
    self.addFace( *(new Face( number, node_number_vector, type_number )) );     
  }
  

//  //------------------------------------------------------------------
//  static void addLineBoundary( Model& self, numbered boundary_number, py::list& py_node_number_vector, numbered loop_number)
//  {
//    std::vector<numbered>* node_number_vector = listConvertor<numbered>( py_node_number_vector );    
//    self.addBoundary( *(new LineBoundary(boundary_number, node_number_vector, loop_number )) );
//  }
//
//  //------------------------------------------------------------------
//  static void addArcBoundary( Model& self, numbered boundary_number, py::list& py_node_number_vector, numbered loop_number)
//  {
//    std::vector<numbered>* node_number_vector = listConvertor<numbered>( py_node_number_vector );    
//    self.addBoundary( *(new ArcBoundary(boundary_number, node_number_vector, loop_number)) );
//  }
};

//------------------------------------------------------------------
struct PyNode
{
  static Point& getLagrangianPoint( Node& self ) {
    return( dynamic_cast<PointAggregate&>(self).getLagrangianPoint() );
  }

  static Point& getEulerianPoint( Node& self ) {
    return( dynamic_cast<PointAggregate&>(self).getEulerianPoint() );
  }

  static Point getEulerianPointForTimeStamp( Node& self, TimeStamp time_stamp, double deformation_factor ) {
    return( self.getEulerianPoint(time_stamp,deformation_factor) );
  }

  static ForceLoad* getForceLoad( Node& self ) {
    return( dynamic_cast<ForceLoad*>(self.getLoad( IDObject::FORCE )) );
  }

  static DisplacementDOF* getDisplacementDOF( Node& self ) {
    return( dynamic_cast<DisplacementDOF*>(self.getDOF( IDObject::DISPLACEMENT )) );
  }

  static LagrangeDOF* getLagrangeDOF( Node& self ) {
    return( dynamic_cast<LagrangeDOF*>(self.getDOF( IDObject::LAGRANGE )) );
  }
};

//------------------------------------------------------------------
struct PyTimeBar
{
  static TimeStamp& getTimeStamp( TimeBar& self, unsigned index )
  {
    return( self.Aggregate<TimeStamp>::get( index ) );
  }
};

//------------------------------------------------------------------
template<class T,class U>
struct PyValueContainer
{
  static T* getValue( U& u, CoordSys::CoordID coord_id)
  {
    return( u.getValue(coord_id) );
  }
};

//------------------------------------------------------------------
struct PyDOFValue
{
  static double getValue( DOFValue& self ) {
    return( self.getValue() );
  }

  static double getValueForTimeStamp( DOFValue& self, const TimeStamp& time_stamp ) {
    return( self.getValue( time_stamp ) );
  }

  static double enabled( DOFValue& self ) {
    return( self.enabled() );
  }

  static double enabledForTimeStamp( DOFValue& self, const TimeStamp& time_stamp ) {
    return( self.enabled( time_stamp ) );
  }
};

//------------------------------------------------------------------
struct PyLoadValue
{
  static double getInternal( LoadValue& self ) {
    return( self.getInternal() );
  }

  static double getInternalForTimeStamp( LoadValue& self, const TimeStamp& time_stamp ) {
    return( self.getInternal( time_stamp ) );
  }
};

//------------------------------------------------------------------
struct PyBoundary : Boundary, py::wrapper<Boundary>
{
  void resolveGeometrie() {
    this->get_override("resolveGeometrie")();
  }
};

//------------------------------------------------------------------
void export_model()
{
  using namespace boost::python;

  class_<DOFValue>("DOFValue",no_init)
    .def("getValue",&PyDOFValue().getValue)
    .def("getValue",&PyDOFValue().getValueForTimeStamp)
    .def("enabled",&PyDOFValue().enabled)
    .def("enabled",&PyDOFValue().enabledForTimeStamp)
    ;

  class_<LoadValue>("LoadValue",no_init)
    .def("getInternal",&PyLoadValue().getInternal)
    .def("getInternal",&PyLoadValue().getInternalForTimeStamp)
    ;

  class_<ForceLoad>("ForceLoad",no_init)
    .def("getValue",&PyValueContainer<LoadValue,ForceLoad>().getValue,return_value_policy<reference_existing_object>())
    ;

  class_<DisplacementDOF>("DisplacementDOF",no_init)
    .def("getValue",&PyValueContainer<DOFValue,DisplacementDOF>().getValue,return_value_policy<reference_existing_object>())
    ;

  class_<LagrangeDOF>("LagrangeDOF",no_init)
    .def("getValue",&PyValueContainer<DOFValue,LagrangeDOF>().getValue,return_value_policy<reference_existing_object>())
    ;

  class_<TimeBar>("TimeBar",no_init)
    .def("getTimeStamp",&PyTimeBar().getTimeStamp,return_value_policy<reference_existing_object>())
    .def("getLastFinishedTimeStamp",&TimeBar::getLastFinishedTimeStamp,return_value_policy<copy_const_reference>())
    .def("getTimeStampIterator",&getIterator<TimeStamp,TimeBar>)
    ;

  class_<TimeStamp>("TimeStamp",no_init)
    .def("index",&TimeStamp::index)
    .def("time",&TimeStamp::time)
    ;

  class_<Model>("Model",no_init)
    .def("setGlobal",&PyModel().setGlobal)

    .def("addNode",&PyModel().addNode)
    .def("getNode",getNode,return_value_policy<reference_existing_object>())
    .def("getNodeAmount",getNodeAmount)
    .def("getNodeIterator", &getIterator<Node,Model> )

    .def("addElement",&PyModel().addElement)
    .def("getElement",getElement,return_value_policy<reference_existing_object>())
    .def("getElementAmount",getElementAmount)
    .def("getElementIterator", &getIterator<Element,Model> )

    .def("addEdge",&PyModel().addEdge)
    .def("addFace",&PyModel().addFace)

    .def("addElementType",&PyModel().addElementType)
    .def("getElementType",getElementType,return_value_policy<reference_existing_object>())

    .def("addMaterial",&PyModel().addMaterial)
    .def("getMaterial",getMaterial,return_value_policy<reference_existing_object>())

    .def("getBoundaryAmount",getBoundaryAmount)
//    .def("addLineBoundary",&PyModel().addLineBoundary)
//    .def("addArcBoundary",&PyModel().addArcBoundary)
//    .def("setSubComponentsOnBoundary",&Model::setSubComponentsOnBoundary)

    .def("getTimeBar",&Model::getTimeBar,return_value_policy<reference_existing_object>())

    .def("getEdgeIterator", &getIterator<Edge,Model> )
    ;

  class_<NodeContainer>("NodeContainer",no_init)
    .def("getLocalNode",getLocalNode,return_value_policy<reference_existing_object>())
    ;

  class_<Element, bases<NumberedObject,NodeContainer> >("Element",no_init)
    ;

  class_<ElementType, bases<NumberedObject,DynamicProperty> >("ElementType",no_init)
    ;

  class_<Material, bases<NumberedObject,DynamicProperty> >("Material",no_init)
    ;

  class_<Edge, bases<NumberedObject,NodeContainer> >("Edge",no_init)
    ;

  class_<PyBoundary, bases<NumberedObject,NodeContainer>, boost::noncopyable >("Boundary",no_init)
    ;

  class_<Node, bases<NumberedObject> >("Node",no_init)
    .def("getLagrangianPoint", &PyNode().getLagrangianPoint,return_value_policy<reference_existing_object>())
    .def("getEulerianPoint", &PyNode().getEulerianPoint,return_value_policy<reference_existing_object>())
    .def("getEulerianPoint", &PyNode().getEulerianPointForTimeStamp)
    .def("getForceLoad",&PyNode().getForceLoad,return_value_policy<reference_existing_object>())
    .def("getDisplacementDOF",&PyNode().getDisplacementDOF,return_value_policy<reference_existing_object>())
    .def("getLagrangeDOF",&PyNode().getLagrangeDOF,return_value_policy<reference_existing_object>())
    ;

  PyIteratorWrapper< Element,Iterator<Element> >().wrap("ElementIterator");
  PyIteratorWrapper< Edge,Iterator<Edge> >().wrap("EdgeIterator");
  PyIteratorWrapper< Node,Iterator<Node> >().wrap("NodeIterator");
  PyIteratorWrapper< Boundary,Iterator<Boundary> >().wrap("BoundaryIterator");
  PyIteratorWrapper< TimeStamp,Iterator<TimeStamp> >().wrap("TimeStampIterator");
}
