#ifndef material_h__
#define material_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>

#include "base/numbered_object.h"
#include "base/id_object.h"
#include "base/dynamic_property.h"

class Constitutive;

//------------------------------------------------------------------
class Material : 
  public NumberedObject,
  public IDObject,
  public DynamicProperty
{
public:
  Material(numbered material_number,
		   ID material_id);

  ~Material() {}

  inline Constitutive& getConstitutive()
  {return( *constitutive_ );}

  inline void setConstitutive(Constitutive* constitutive)
  { constitutive_ = constitutive; }

  inline ID getID()
  {return(material_id_);}

protected:
  ID material_id_;
  Constitutive* constitutive_;
};

#endif // material_h__
