#ifndef __progress_h__
#define __progress_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "base/id_object.h"

//------------------------------------------------------------------
class Progress
{
public:
  Progress(IDObject::ID progress_shape, std::vector<numbered>* reference_vector) :
    progress_shape_(progress_shape),
    reference_vector_(reference_vector)
  {}

  ~Progress() { if( reference_vector_ ) delete reference_vector_ ; }

  IDObject::ID getShape() { return( progress_shape_ ); }

  const std::vector<numbered>& getReferenceVector() { return(*reference_vector_); }

  numbered getFirstReference() { return( reference_vector_ -> at(0) ); }
protected:
  IDObject::ID progress_shape_;
  std::vector<numbered>* reference_vector_;
};

//------------------------------------------------------------------
class DOFProgress :
  public Progress
{
public:
  DOFProgress(IDObject::ID progress_shape, std::vector<numbered>* reference_vector) :
    Progress(progress_shape,reference_vector)
  {}  
};

//------------------------------------------------------------------
class LoadProgress :
  public Progress
{
public:
  LoadProgress(IDObject::ID progress_shape, std::vector<numbered>* reference_vector) :
    Progress(progress_shape,reference_vector)
  {}  
};

#endif
