/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "time_episode.h"

//------------------------------------------------------------------
void TimeEpisode::prettyPrint() const
{
  std::cout << "| TimeEpisode start_time=" << start_time_
            << " , end_time=" << end_time_
            << " | start_index=" << start_index_
            << " , step_amount=" << step_amount_ 
            << std::endl;
}

//------------------------------------------------------------------
TimeStamp* TimeEpisode::getTimeStamp( unsigned step ) const
{
  TimeStamp* time_stamp = new TimeStamp( start_time_ + (end_time_ - start_time_)/step_amount_ * step , start_index_ + step );
  return( time_stamp );
}

//------------------------------------------------------------------
bool TimeEpisode::containsTimeStamp( const TimeStamp& time )
{
  return( (time > start_time_) && (time <= end_time_) );
}
