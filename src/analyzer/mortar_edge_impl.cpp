/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "mortar_edge_impl.h"

//------------------------------------------------------------------
void MortarEdgeImpl::pointProjectionM( BoundaryIntegrationPoint& int_point, 
                                   Edge& edge,
                                   double& distance, double& xi )
 {
  double residuum = 1.e15, d_xi = 0.;
  Vector *tangent = 0, *normal = 0;
  Point* mortar_point = 0; 
  CartesianCoordSys coord_sys_3D(3);
  Vector e3( &coord_sys_3D , 0., 0., 1. );

  for( unsigned count = 0 ; ( count != 5 && fabs(residuum) > 1e-20 ) ; ++count ) {
    mortar_point = edge.getEulerianPoint( xi );
    tangent = edge.getEulerianTangent( xi );
    bz::Array<double,1> distance_vector( (*mortar_point) - int_point.getEulerianPoint() );
   
    residuum = bz::sum( distance_vector * (*tangent) );  
    // summand missing for quadratic case - see calculations
    d_xi = bz::sum( (*tangent) * (*tangent) );
    
    xi -= residuum/d_xi;

    delete mortar_point;
    delete tangent;
  }

  mortar_point = edge.getEulerianPoint( xi );
  tangent = edge.getEulerianTangent( xi ); 

  bz::Array<double,1> distance_vector( int_point.getEulerianPoint() - (*mortar_point) );
  normal = Geometry::calcNormal( *tangent, e3 , true , true );
  distance = bz::sum( distance_vector * (*normal) );

  //  std::cout << "edge = " << edge.getNumber() << " | g_N = " << distance << " | xi = " << xi << std::endl;

  delete mortar_point;
  delete normal;
  delete tangent;
}

//------------------------------------------------------------------
Node* MortarEdgeImpl::findNearestMortarCorner( BoundaryIntegrationPoint& int_point,
                                               Model& model,
                                               double& dist )
{
  Node *nearest_mortar_corner = 0, *mortar_corner = 0;
  double dist_new = 0.;
  dist = DBL_MAX;

//  if( int_point.mortarEdgeSet() ) {
//    std::vector<Node*>* node_vector = model.getBoundaryNeighborhood( int_point.getNearestMortarNode() , 2 );
//    for( std::vector<Node*>::iterator iter = node_vector->begin();
//         iter != node_vector->end();
//         ++iter ) {
//      if( iter->getContactSide() == Contact::MORTAR ) {
//        dist_new = Geometry::calcDistance( int_point.getEulerianPoint(), dynamic_cast<PointAggregate*>(*iter) -> getEulerianPoint() );
//        if( dist_new < dist ) {
//          dist = dist_new;
//          nearest_mortar_corner = &(*iter);
//        }
//      }
//    }
//    delete node_vector;
//  } else { // We have to search hole mesh
    for( Iterator<Edge> mortar_edge_iter = model.Aggregate<Edge>::begin() ;
         mortar_edge_iter != model.Aggregate<Edge>::end() ;
         ++mortar_edge_iter ) {
      if( mortar_edge_iter -> getContactSide() == Contact::MORTAR ) {
        mortar_corner = &(mortar_edge_iter -> Aggregate<Node>::front());
        dist_new = Geometry::calcDistance( int_point.getEulerianPoint(), dynamic_cast<PointAggregate*>(mortar_corner)->getEulerianPoint() );
        if( dist_new < dist ) {
          dist = dist_new;
          nearest_mortar_corner = mortar_corner;
        }
        mortar_corner = mortar_edge_iter -> getLastNode();
        dist_new = Geometry::calcDistance( int_point.getEulerianPoint(), dynamic_cast<PointAggregate*>(mortar_corner)->getEulerianPoint() );
        if( dist_new < dist ) {
          dist = dist_new;
          nearest_mortar_corner = mortar_corner;
        }
      }
    }
//  }

  return( nearest_mortar_corner );
}

//------------------------------------------------------------------
bool MortarEdgeImpl::contactSearchG( MortarElement& mortar_element , Model& model )
{
  bool contact_set_changed = false;
  double dist = 0.;
  Node *mortar_corner = 0;
  std::vector<Edge*>* mortar_edge_vector = 0;

  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    mortar_corner = findNearestMortarCorner( *ip_iter , model, dist );

    LOG(Logger::INFO) << "Nearest mortar Node on ip " << ip_iter->getMathIP().r() 
                      << " of edge #" << mortar_element.getNonMortarEdge()->getNumber() 
                      << " is set to #" << mortar_corner->getNumber() << Logger::endl;
    
    // If we have a NEW neares corner node
    if( ip_iter -> setNearestMortarNode( mortar_corner ) ) {
      mortar_edge_vector = model.getAdjacentEdges( *mortar_corner );
      for( std::vector<Edge*>::iterator iter = mortar_edge_vector -> begin();
           iter != mortar_edge_vector -> end() ;
           ++iter ) {
        if( (*iter) -> getContactSide() == Contact::MORTAR )
          ip_iter -> addMortarEdge( *iter );
      }
      LOG(Logger::INFO) << " --> and has been changed!" << Logger::endl;
      // std::cout << "Nearest mortar Node on ip " << ip_iter->getMathIP().r()
      //           << " of element #" << mortar_element.getNonMortarElement()->getNumber() 
      //           << " has changed to #" << mortar_corner->getNumber() << std::endl;
      contact_set_changed = true;
      delete mortar_edge_vector;
    }
  }

  return( contact_set_changed );
}

//------------------------------------------------------------------
void MortarEdgeImpl::calcKinematicsGM( MortarElement& mortar_element , bool research_best_edge , bool new_time_step )
{
  double xi = 0., g_N = 0., g_T = 0.;
  std::vector< boost::tuple<double,double,unsigned> > geometric_props;

  LOG(Logger::INFO) << "Calc Kinematics for edge : " 
                    << mortar_element.getNonMortarEdge()->getNumber() << Logger::endl;

  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( !ip_iter -> mortarEdgeSet() ) { 
      LOG(Logger::INFO) << "For IP no mortar edge was set!" << Logger::endl;
      break;
    }

    if( new_time_step && (ip_iter->getActive()) ) {
      ip_iter -> archive();
    }

    if( research_best_edge ) {
      for( unsigned edge_iter = 0 ;
           edge_iter != ip_iter->amountOfMortarEdges() ;
           ++edge_iter ) {
        // Here we do the projection!
        pointProjectionM( *ip_iter, ip_iter->getMortarEdge( edge_iter ) , g_N , xi );
        geometric_props.push_back( boost::tuple<double,double,unsigned>( g_N, xi, edge_iter ) );
      }
      boost::tuple<double,double,unsigned> best_projection = *std::min_element( geometric_props.begin(), geometric_props.end() , RankProjection() );
      if( (ip_iter->amountOfMortarEdges()==1) && (fabs( best_projection.get<XI>() ) > 1.) ) {
        //  std::cout << "On edge " << mortar_element.getNonMortarEdge()->getNumber() << " ip " << ip_iter->getMathIP().r() << " is outside" << std::endl;
        ip_iter -> setActive( false );
      } else {
        ip_iter -> setActive( true );
        if( ip_iter -> hasActiveMortarEdge() ) {
          g_T = slipM( ip_iter->getMortarEdge(best_projection.get<EDGE_INDEX>()) , best_projection.get<XI>() ,
                       ip_iter->getActiveMortarEdge(true) , ip_iter -> xi(true) );
          ip_iter -> setGapData( best_projection.get<DIST>(), 
                                 g_T,
                                 best_projection.get<XI>(),
                                 best_projection.get<EDGE_INDEX>());
        } else { // This must be the very time we search a best projection edge for this integration point so we have no g_T
          ip_iter -> setGapData( best_projection.get<DIST>(),
                                 0.,
                                 best_projection.get<XI>(),
                                 best_projection.get<EDGE_INDEX>());
          ip_iter -> archive();
        }
      }
    } else { // Only for first few iterations the best projection edge is "researched" -> else we use the old one again.
      if( ip_iter -> getActive() ) {
        pointProjectionM( *ip_iter, ip_iter->getActiveMortarEdge() , g_N , xi );
        g_T = slipM( ip_iter->getActiveMortarEdge() , xi ,
                     ip_iter->getActiveMortarEdge(true) , ip_iter -> xi(true) );
        ip_iter -> setGapData( g_N , g_T , xi );
      }
    }

    if( ip_iter -> getActive() ) {
      LOG(Logger::INFO) << "r = " << ip_iter -> getMathIP().r()
                        << " g_N = " << ip_iter -> getGN() 
                        << " g_T = " << ip_iter -> getGT() 
                        << " xi = " << ip_iter -> xi() 
                        << " [ onto mortar edge = " << ip_iter->getActiveMortarEdge().getNumber()
                        << " (" << ip_iter->getActiveMortarEdge().Aggregate<Node>::begin()->getNumber()
                        << "/" << ip_iter->getActiveMortarEdge().getLastNode()->getNumber() 
                        << ") last was " << ip_iter->getActiveMortarEdge(true).getNumber()
                        << " with xi = " << ip_iter -> xi(true)
                        << Logger::endl;
    }

    geometric_props.clear();
  }
}

//------------------------------------------------------------------
double MortarEdgeImpl::slipM( Edge& new_edge , double new_xi,
                          Edge& old_edge , double old_xi )
{
  Point* new_point = new_edge.getEulerianPoint( new_xi );
  Point* old_point = old_edge.getEulerianPoint( old_xi );
  Vector* mortar_tangent = new_edge.getEulerianTangent( new_xi );

  bz::Array<double,1> temp( (*new_point) - (*old_point) );
  double g_T = bz::sum( temp * (*mortar_tangent) ) / Math::norm( *mortar_tangent , 2 );

  delete new_point;
  delete old_point;
  delete mortar_tangent;

  return( g_T );
}

//------------------------------------------------------------------
void MortarEdgeImpl::resetNodalConditionL( MortarElement& mortar_element )
{
  for( Iterator<Node> node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end();
       ++node_iter ) {
    (node_iter -> getProperty<double>("mortar_normal_condition_value")) = 0.;
    (node_iter -> getProperty<double>("mortar_tangential_condition_value")) = 0.;
  }
}

//------------------------------------------------------------------
void MortarEdgeImpl::calcNodalConditionL( MortarElement& mortar_element )
{
  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( ! ip_iter -> getActive() )
      continue;

    double alpha = ip_iter->getMathIP().alpha();
    for( Iterator<Node> node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
         node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end();
         ++node_iter ) {   
      Edge* edge = mortar_element.getNonMortarEdge();
      int local_node_number = edge->getLocalNodeNumber( *node_iter );
      double dual_shape = edge->getType().getShape().dualShape( edge->getType().getShape().getNodeIndex(local_node_number) , ip_iter->getMathIP().r() );
      double shape = edge->getType().getShape().shape( edge->getType().getShape().getNodeIndex(local_node_number) , ip_iter->getMathIP().r() );
      double lambda_N = node_iter->getLagrangeFactor().get(CoordSys::NORMAL) * dual_shape;

      if( node_iter -> getProperty<bool>("mortar_active") ) {
        (node_iter -> getProperty<double>("mortar_normal_condition_value")) += shape * lambda_N * alpha;
        if( node_iter -> hasProperty("slide") ) {
          double lambda_T = node_iter->getLagrangeFactor().get(CoordSys::TANGENT_R) * dual_shape;
          if( node_iter -> getProperty<int>("slide") == 0 ) {
            (node_iter -> getProperty<double>("mortar_tangential_condition_value")) += shape * lambda_T * alpha;
          } else {
            (node_iter -> getProperty<double>("mortar_tangential_condition_value")) += dual_shape * (ip_iter -> getGT()) * alpha;
          }
        }
      } else {
        (node_iter -> getProperty<double>("mortar_normal_condition_value")) += dual_shape * (ip_iter -> getGN()) * alpha;
      }
    }
  }
}
