/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "pm_mortar_edge_impl.h"

int PMMortarEdgeImpl::AMOUNT_OF_DOF_VALUES_PER_NODE = 2;
int PMMortarEdgeImpl::MORTAR_EDGE_NODE_AMOUNT = 2;

//------------------------------------------------------------------
bool PMMortarEdgeImpl::contactSearch( MortarElement& mortar_element , Model& model )
{
  return( contactSearchG( mortar_element, model ) );
}

//------------------------------------------------------------------
void PMMortarEdgeImpl::calcKinematics( MortarElement& mortar_element , bool research_best_edge , bool new_time_step )
{
  calcKinematicsGM( mortar_element , research_best_edge, new_time_step );
}

//------------------------------------------------------------------
GlobalAssembleBase<2>* PMMortarEdgeImpl::calcMortarElementStiffness(MortarElement& mortar_element)
{ 
  int mortar_node_amount = mortar_element.getActiveIntegrationPoints() * MORTAR_EDGE_NODE_AMOUNT;
  int non_mortar_node_amount = mortar_element.getNonMortarEdge()->getType().getShape().getNodeAmount();

  if( !mortar_node_amount ) {
    return( 0 );
  }

  LOG(Logger::INFO) << "Calc MortarElement Stiffness with FPGM method of non-mortar (primary) Edge: " 
		    << mortar_element.getNonMortarEdge()->getNumber()
                    << " with mortar_node_amount = " << mortar_node_amount
                    << Logger::endl;

  bz::TinyVector<int,2> size_2D;
  size_2D = (non_mortar_node_amount + mortar_node_amount)*AMOUNT_OF_DOF_VALUES_PER_NODE ,
    (non_mortar_node_amount + mortar_node_amount)*AMOUNT_OF_DOF_VALUES_PER_NODE;
  GlobalAssembleBase<2>* element_stiffness = 
    new GlobalAssembleBase<2>( size_2D );

  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter ) {
    element_stiffness -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::X ) );
    element_stiffness -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::Y ) );
  }

  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    // We can continue if the IP isn't active, that's why we have to code the integrator here!
    if( ! ip_iter -> getActive() ) continue;
    
    for( Iterator<Node> mortar_node_iter = ip_iter->getActiveMortarEdge().Aggregate<Node>::begin();
         mortar_node_iter != ip_iter->getActiveMortarEdge().Aggregate<Node>::end() ; 
         ++mortar_node_iter ) {     
      if( !element_stiffness -> hasDOFValue( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::X ) ) ) {
        element_stiffness -> addDOFValue( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::X ) );
        element_stiffness -> addDOFValue( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::Y ) );
      }
    }

    AssembleBase<4>* ip_stiffness = calcIPStiffness( mortar_element, *ip_iter );
    element_stiffness -> assemble( *ip_stiffness );
    delete ip_stiffness;
  }

  //  std::cout << "element stiffness = " << *element_stiffness << std::endl;
  
  return( element_stiffness );
}

//------------------------------------------------------------------
AssembleBase<4>* PMMortarEdgeImpl::calcIPStiffness( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point )
{
  int non_mortar_node_amount = mortar_element.getNonMortarEdge()->getType().getShape().getNodeAmount();
  int mortar_node_amount = int_point.getActiveMortarEdge().getType().getShape().getNodeAmount();
  bz::TinyVector<int,4> size_4D;
  size_4D = AMOUNT_OF_DOF_VALUES_PER_NODE, non_mortar_node_amount + mortar_node_amount,
    AMOUNT_OF_DOF_VALUES_PER_NODE, non_mortar_node_amount + mortar_node_amount;
  AssembleBase<4>* ip_stiffness = new AssembleBase<4>(size_4D);
  ip_stiffness -> addCoordID( CoordSys::X );
  ip_stiffness -> addCoordID( CoordSys::Y );
  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter )
    ip_stiffness -> addDOF( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) );
  for( Iterator<Node> mortar_node_iter = int_point.getActiveMortarEdge().Aggregate<Node>::begin();
       mortar_node_iter != int_point.getActiveMortarEdge().Aggregate<Node>::end() ; 
       ++mortar_node_iter )
    ip_stiffness -> addDOF( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) );

  CartesianCoordSys coord_sys_3D(3);
  Vector e3( &coord_sys_3D,0,0,1  );
  double epsilon = mortar_element.getContact().getPenalty();
  bz::Array<double,1>* shape = mortar_element.getShapeTensorM( int_point );
  bz::Array<double,1>* der_shape_2 = mortar_element.getDerivativeShapeTensorM( int_point , Contact::MORTAR );
  bz::Array<double,1>* der_shape_1 = mortar_element.getDerivativeShapeTensorM( int_point , Contact::NON_MORTAR );
  Vector* tangent = int_point.getActiveMortarEdge().getEulerianTangent( int_point.xi() );
  Vector* normal = int_point.getActiveMortarEdge().getEulerianNormal( int_point.xi() );
  Vector* non_mortar_tangent = mortar_element.getNonMortarEdge()->getEulerianTangent( int_point.getMathIP().r() );

  double jacobian_det = Math::norm( *non_mortar_tangent , 2 );
  double g_N = int_point.getGN();
    
  bz::Array<double,2> delta_g_N( size_4D(0),size_4D(1) );
  deltaG_N( delta_g_N ,
            *shape, *normal );
    
  bz::Array<double,4> delta_delta_g_N(size_4D);
  deltaDeltaG_N( delta_delta_g_N ,
                 *shape , *der_shape_2 ,
                 *tangent, *normal , g_N );
  
  bz::Array<double,2> delta_J( size_4D(0),size_4D(1) );
  delta_J = 0.;
  deltaJ( delta_J,
          *der_shape_1, *non_mortar_tangent,
          jacobian_det );
    
  bz::Array<double,4> delta_delta_J( size_4D );
  delta_delta_J = 0.;
  deltaDeltaJ( delta_delta_J,
               *der_shape_1, *non_mortar_tangent, 
               jacobian_det );
    
  (*ip_stiffness) += ( delta_g_N(bzt::i,bzt::j) * delta_g_N(bzt::k,bzt::l)   // This would have contributions even if g_N = 0 ??
                     + delta_delta_g_N(bzt::i,bzt::j,bzt::k,bzt::l) * g_N ) * jacobian_det;
  (*ip_stiffness) += ( delta_g_N(bzt::i,bzt::j) * delta_J  (bzt::k,bzt::l)
                     + delta_J  (bzt::i,bzt::j) * delta_g_N(bzt::k,bzt::l) ) * g_N;
  (*ip_stiffness) += delta_delta_J(bzt::i,bzt::j,bzt::k,bzt::l) * (pow(g_N,2.)/2.);
  (*ip_stiffness) *= epsilon * int_point.getMathIP().alpha();
  
  delete shape;
  delete der_shape_1;
  delete der_shape_2;
  delete tangent;
  delete non_mortar_tangent;
  delete normal;

  return( ip_stiffness );
}

//------------------------------------------------------------------
GlobalAssembleBase<1>* PMMortarEdgeImpl::calcMortarElementLoadVector(MortarElement& mortar_element)
{
  int mortar_node_amount = mortar_element.getActiveIntegrationPoints() * MORTAR_EDGE_NODE_AMOUNT;
  int non_mortar_node_amount = mortar_element.getNonMortarEdge()->getType().getShape().getNodeAmount();

  if( !mortar_node_amount )
    return( 0 );

  LOG(Logger::INFO) << "Calc MortarElement Load with FPGM method of non-mortar Edge: " 
		    << mortar_element.getNonMortarEdge()->getNumber()
                    << " with mortar_node_amount = " << mortar_node_amount
                    << Logger::endl;

  GlobalAssembleBase<1>* element_load = 
    new GlobalAssembleBase<1>( (non_mortar_node_amount + mortar_node_amount)*AMOUNT_OF_DOF_VALUES_PER_NODE );

  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter ) {
    element_load -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::X ) );
    element_load -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::Y ) );
  }

  //  int mortar_node_offset = 0;
  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( ! ip_iter -> getActive() ) continue;
    
    for( Iterator<Node> mortar_node_iter = ip_iter->getActiveMortarEdge().Aggregate<Node>::begin();
         mortar_node_iter != ip_iter->getActiveMortarEdge().Aggregate<Node>::end() ; 
         ++mortar_node_iter ) {
      if( !element_load -> hasDOFValue( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::X ) ) ) {
        element_load -> addDOFValue( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::X ) );
        element_load -> addDOFValue( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue( CoordSys::Y ) );
      }
    }

    AssembleBase<2>* ip_load = calcIPLoad( mortar_element, *ip_iter );
    element_load -> assemble( *ip_load );
    delete ip_load;
  }
  
  //  std::cout << "element load vector = " << *element_load << std::endl;
  
  return( element_load );
}

//------------------------------------------------------------------
AssembleBase<2>* PMMortarEdgeImpl::calcIPLoad( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point )
{
  int non_mortar_node_amount = mortar_element.getNonMortarEdge()->getType().getShape().getNodeAmount();
  int mortar_node_amount = int_point.getActiveMortarEdge().getType().getShape().getNodeAmount();
  bz::TinyVector<int,2> size_2D;
  size_2D = AMOUNT_OF_DOF_VALUES_PER_NODE , non_mortar_node_amount + mortar_node_amount;
  AssembleBase<2>* ip_load = new AssembleBase<2>(size_2D);  
  ip_load -> addCoordID( CoordSys::X );
  ip_load -> addCoordID( CoordSys::Y );
  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter )
    ip_load -> addDOF( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) );
  for( Iterator<Node> mortar_node_iter = int_point.getActiveMortarEdge().Aggregate<Node>::begin();
       mortar_node_iter != int_point.getActiveMortarEdge().Aggregate<Node>::end() ; 
       ++mortar_node_iter )
    ip_load -> addDOF( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) );

  CartesianCoordSys coord_sys_3D(3);
  Vector e3( &coord_sys_3D,0,0,1  );
  double epsilon = mortar_element.getContact().getPenalty();
  bz::Array<double,1>* shape = mortar_element.getShapeTensorM( int_point );
  Vector* normal = int_point.getActiveMortarEdge().getEulerianNormal( int_point.xi() );
  bz::Array<double,1>* der_shape_1 = mortar_element.getDerivativeShapeTensorM( int_point , Contact::NON_MORTAR );
  Vector* non_mortar_tangent = mortar_element.getNonMortarEdge()->getEulerianTangent( int_point.getMathIP().r() );
  double jacobian_det = Math::norm( *non_mortar_tangent , 2 );
  double g_N = int_point.getGN();
    
  bz::Array<double,2> delta_g_N(size_2D);
  deltaG_N( delta_g_N ,
            *shape, *normal );
  
  bz::Array<double,2> delta_J(size_2D);
  deltaJ( delta_J,
          *der_shape_1, *non_mortar_tangent, 
          jacobian_det );
   
  (*ip_load) += delta_g_N * g_N * jacobian_det; 
  (*ip_load) += delta_J * (pow(g_N,2.)/2.);
  (*ip_load) *= (-1.) * epsilon * int_point.getMathIP().alpha();
    
  delete non_mortar_tangent;
  delete shape;
  delete der_shape_1;
  delete normal;
    
  return( ip_load );
}
