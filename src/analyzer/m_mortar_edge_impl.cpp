/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "m_mortar_edge_impl.h"

//------------------------------------------------------------------
void MMortarEdgeImpl::deltaN( bz::Array<double,3>& array,
                               bz::Array<double,1>& tangent_tensor, bz::Array<double,1>& tangent, bz::Array<double,1>& normal,
                               double alpha )
{
  array = -1. * tangent(bzt::k) * tangent_tensor(bzt::j) * normal(bzt::i) / alpha;
}

//------------------------------------------------------------------
void MMortarEdgeImpl::deltaDeltaN( bz::Array<double,5>& array,
                                    bz::Array<double,1>& tangent_tensor, bz::Array<double,1>& tangent, bz::Array<double,1>& normal,
                                    double alpha )
{
  bz::Array<double,2> delta( 2 , 2 );
  Math::kronecker(delta);

  array = - delta(bzt::k, bzt::m) * tangent_tensor(bzt::l) * normal(bzt::i) * tangent_tensor(bzt::j) / alpha
    + tangent_tensor(bzt::l) * tangent(bzt::k) * tangent(bzt::m) * tangent_tensor(bzt::j) * normal(bzt::i) * 2. / pow(alpha,2)
    + tangent_tensor(bzt::j) * tangent(bzt::i) * tangent(bzt::m) * tangent_tensor(bzt::l) * normal(bzt::k) / pow(alpha,2);
}

//------------------------------------------------------------------
void MMortarEdgeImpl::deltaG_N( bz::Array<double,2>& array,
                                 bz::Array<double,1>& shape, bz::Array<double,1>& normal )
{
  array = (-1. * normal(bzt::i)) * shape(bzt::j);
}

//------------------------------------------------------------------
void MMortarEdgeImpl::deltaDeltaG_N( bz::Array<double,4>& array,
                                      bz::Array<double,1>& shape, bz::Array<double,1>& der_shape_2,
                                      bz::Array<double,1>& tangent, bz::Array<double,1>& normal , double g_N )
{
  array = 1. / bz::sum( tangent * tangent ) * // \nm{\beta} is zero for linear shape
    ( + tangent(bzt::i) * shape(bzt::j)             * der_shape_2(bzt::l) * normal(bzt::k)
      + normal(bzt::i)  * der_shape_2(bzt::j)       * shape(bzt::l)       * tangent(bzt::k)
      - normal(bzt::i)  * der_shape_2(bzt::j) * g_N * der_shape_2(bzt::l) * normal(bzt::k) );
}

//------------------------------------------------------------------
void MMortarEdgeImpl::deltaXi( bz::Array<double,2>& array,
                                bz::Array<double,1>& shape, bz::Array<double,1>& der_shape_2,
                                bz::Array<double,1>& tangent, bz::Array<double,1>& normal , 
                                double g_N, double alpha )
{
  array = ( (-1.) * tangent(bzt::i) * shape(bzt::j) + g_N * normal(bzt::i) * der_shape_2(bzt::j) ) / alpha;
}

//------------------------------------------------------------------
void MMortarEdgeImpl::deltaG_T( bz::Array<double,2>& array, 
                                 bz::Array<double,2>& delta_xi, bz::Array<double,1>& tangent )
{
  array = delta_xi * Math::norm( tangent , 2 );
}

//------------------------------------------------------------------
void MMortarEdgeImpl::deltaDeltaXi( bz::Array<double,4>& array,
                                     bz::Array<double,2>& delta_xi, bz::Array<double,2>& delta_g_N, 
                                     bz::Array<double,3>& delta_N, bz::Array<double,5>& delta_delta_N,
                                     bz::Array<double,1>& der_shape_2, bz::Array<double,1>& tangent,
                                     double g_N , double alpha )
{
  bz::Array<double,2> delta_N_a( delta_xi.shape() );
  delta_N_a = bz::sum( delta_N(bzt::i,bzt::j,bzt::k) * tangent(bzt::k) , bzt::k );
  bz::Array<double,4> delta_delta_N_a( array.shape() );
  delta_delta_N_a = bz::sum( delta_delta_N(bzt::i,bzt::j,bzt::k,bzt::l,bzt::m) * tangent(bzt::m) , bzt::m );

  array  = 0.;
  array += delta_g_N(bzt::i,bzt::j) * delta_N_a(bzt::k,bzt::l);
  array += delta_g_N(bzt::k,bzt::l) * delta_N_a(bzt::i,bzt::j);
  array += g_N * delta_delta_N_a(bzt::i,bzt::j,bzt::k,bzt::l);
  array += tangent(bzt::i) * der_shape_2(bzt::j) * delta_xi(bzt::k,bzt::l);
  array += tangent(bzt::k) * der_shape_2(bzt::l) * delta_xi(bzt::i,bzt::j);
  array *= (-1.) / alpha;
}

//------------------------------------------------------------------
void MMortarEdgeImpl::deltaDeltaG_T( bz::Array<double,4>& array,
                                      bz::Array<double,2>& delta_xi, bz::Array<double,2>& delta_g_N, 
                                      bz::Array<double,3>& delta_N, bz::Array<double,5>& delta_delta_N,
                                      bz::Array<double,1>& der_shape_2, bz::Array<double,1>& tangent,
                                      double g_N , double alpha )
{
  bz::Array<double,4> delta_delta_xi( array.shape() );
  deltaDeltaXi( delta_delta_xi , delta_xi, delta_g_N, delta_N, delta_delta_N, der_shape_2, tangent, g_N, alpha );
  array  = delta_delta_xi(bzt::i,bzt::j,bzt::k,bzt::l) * sqrt(alpha);
  array += der_shape_2(bzt::l) * tangent(bzt::k) * delta_xi(bzt::i,bzt::j) / sqrt(alpha);
}

