/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "map.h"

//------------------------------------------------------------------
Map::Map(Model& model) :
  map_vector_(0),
  model_(&model)
{}

//------------------------------------------------------------------
Map::Map(Map& src_map) :
  map_vector_(new std::vector<DOFValue*>),
  model_(src_map.model_)
{
  for( std::vector<DOFValue*>::iterator iter = src_map.map_vector_->begin() ; 
       iter != src_map.map_vector_->end() ; 
       ++iter)
    map_vector_->push_back(*iter);
}

//------------------------------------------------------------------
Map::~Map()
{
  if( map_vector_ )
    {
      delete map_vector_;
      map_vector_ = 0;
    }
}

//------------------------------------------------------------------
void Map::setBaseMap(std::vector<DOFValue*>* map_vector)
{
  if( map_vector_ ) delete map_vector_;
  map_vector_ = map_vector;
}

//------------------------------------------------------------------
int Map::getAmountOfUnknown()
{
  return( map_vector_ -> size() );
}

//------------------------------------------------------------------
void Map::setDOFIncrementsInDOFValues(const SolutionVector& solution_vector)
{
  unsigned long index = 0;
  for( std::vector<DOFValue*>::iterator iter = map_vector_ -> begin() ;
       iter != map_vector_ -> end() ; 
       ++iter) {
    (*iter) -> setIncremental(solution_vector(index++));
  }
}

//------------------------------------------------------------------
void Map::updateNodesDOFMechanical( const TimeStamp& time_stamp )
{
  for( Iterator<Node> node_iter =  model_ -> Aggregate<Node>::begin();
       node_iter !=  model_ -> Aggregate<Node>::end(); 
       node_iter++)
    node_iter -> updatePoint(time_stamp);

  for( Iterator<Edge> edge_iter = model_ -> Aggregate<Edge>::begin() ;
       edge_iter != model_ -> Aggregate<Edge>::end() ;
       ++ edge_iter ) 
    edge_iter -> updateIntegrationPoints(time_stamp);

  for( Iterator<Face> face_iter = model_ -> Aggregate<Face>::begin() ;
       face_iter != model_ -> Aggregate<Face>::end() ;
       ++ face_iter ) 
    face_iter -> updateIntegrationPoints(time_stamp);

  for( Iterator<Element> element_iter = model_ -> Aggregate<Element>::begin() ;
       element_iter != model_ -> Aggregate<Element>::end() ;
       ++ element_iter ) 
    element_iter -> updateIntegrationPoints(time_stamp);
}

//------------------------------------------------------------------
void Map::updateNodesDOFThermal( const TimeStamp& time_stamp )
{
  Iterator<Node> node_iter =  model_ -> Aggregate<Node>::begin();
  for( ; node_iter !=  model_ -> Aggregate<Node>::end(); node_iter++) {
    node_iter -> updateTemperature(time_stamp);
  }
}

//------------------------------------------------------------------
void Map::updateNodesDOFLagrangeFactor( const TimeStamp& time_stamp )
{
  for( Iterator<Node> node_iter =  model_ -> Aggregate<Node>::begin(); 
       node_iter !=  model_ -> Aggregate<Node>::end(); 
       node_iter++)
    node_iter -> updateLagrangeFactor( time_stamp );

  for( Iterator<MortarElement> mortar_iter = model_ -> Aggregate<MortarElement>::begin() ;
       mortar_iter != model_ -> Aggregate<MortarElement>::end() ;
       ++ mortar_iter ) 
    mortar_iter -> getNonMortarEdge() -> updateIntegrationPointsLagrangeFactor(time_stamp);
}

//------------------------------------------------------------------
void Map::prettyPrint()
{
  std::cout << "map_vector_ loc: " << map_vector_ << std::endl;
  if (map_vector_)
    std::cout << "map_vector_ size: " << map_vector_->size() << std::endl;
  std::vector<DOFValue*>::iterator iter;
  for( iter = map_vector_ -> begin() ; iter != map_vector_ -> end() ; ++iter)
      (*iter) -> prettyPrint();
}
