/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "l_mortar_edge_impl.h"

//##################################################################
// NORMAL direction 
//##################################################################

//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPStiffnessUUN( AssembleBase<4>& ip_stiffness,
                                          MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                          bz::Array<double,2>& delta_g_N, 
                                          bz::Array<double,4>& delta_delta_g_N, 
                                          bz::Array<double,2>& delta_J,
                                          bz::Array<double,4>& delta_delta_J,
                                          double jacobian_det, double g_N, double lambda_N )
{
  ip_stiffness += delta_delta_g_N * jacobian_det;
  ip_stiffness += delta_delta_J   * g_N;
  ip_stiffness += delta_g_N(bzt::i,bzt::j) * delta_J  (bzt::k,bzt::l) + 
                  delta_J  (bzt::i,bzt::j) * delta_g_N(bzt::k,bzt::l);
  ip_stiffness *= lambda_N * int_point.getMathIP().alpha();
}

//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPStiffnessLambdaN( AssembleBase<2>& ip_stiffness,
                                              MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                              bz::Array<double,2>& delta_g_N, 
                                              bz::Array<double,2>& delta_J,
                                              double jacobian_det, double g_N, double dual_shape )
{
  ip_stiffness += delta_g_N(bzt::i,bzt::j) * jacobian_det;
  ip_stiffness += g_N * delta_J(bzt::i,bzt::j);
  ip_stiffness *= dual_shape * int_point.getMathIP().alpha();
}


//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPLoadUN( AssembleBase<2>& ip_load,
                                    MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                    bz::Array<double,2>& delta_g_N, 
                                    bz::Array<double,2>& delta_J,
                                    double jacobian_det, double g_N, double lambda_N )
{
  ip_load += delta_g_N(bzt::i,bzt::j) * jacobian_det;
  ip_load += g_N * delta_J(bzt::i,bzt::j);
  ip_load *= lambda_N * int_point.getMathIP().alpha();
}

//------------------------------------------------------------------
double LMortarEdgeImpl::calcIPLoadLambdaN( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                           double jacobian_det, double g_N, double dual_shape)
{
  return( g_N * jacobian_det * dual_shape * int_point.getMathIP().alpha() );
}

//##################################################################
// STICK CASE 
//##################################################################

//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPStiffnessUUStick( AssembleBase<4>& ip_stiffness,
                                              MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                              bz::Array<double,2>& delta_g_T,
                                              bz::Array<double,4>& delta_delta_g_T, 
                                              bz::Array<double,2>& delta_J,
                                              bz::Array<double,4>& delta_delta_J,
                                              double jacobian_det, double g_T, double lambda_T )
{
  ip_stiffness += delta_delta_g_T * jacobian_det;
  //  ip_stiffness += delta_delta_J   * g_T;
  ip_stiffness += delta_g_T(bzt::i,bzt::j) * delta_J  (bzt::k,bzt::l) + 
                  delta_J  (bzt::i,bzt::j) * delta_g_T(bzt::k,bzt::l);
  ip_stiffness *= lambda_T * int_point.getMathIP().alpha();
}

//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPStiffnessLambdaStick( AssembleBase<2>& ip_stiffness,
                                                  MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                                  bz::Array<double,2>& delta_g_T, 
                                                  bz::Array<double,2>& delta_J,
                                                  double jacobian_det, double g_T, double dual_shape)
{
  ip_stiffness += delta_g_T(bzt::i,bzt::j) * jacobian_det;
  ip_stiffness += g_T * delta_J(bzt::i,bzt::j);
  ip_stiffness *= dual_shape * int_point.getMathIP().alpha();
}

//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPLoadUStick( AssembleBase<2>& ip_load,
                                        MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                        bz::Array<double,2>& delta_g_T, 
                                        bz::Array<double,2>& delta_J,
                                        double jacobian_det, double g_T, double lambda_T)
{
  ip_load += delta_g_T(bzt::i,bzt::j) * jacobian_det;
  ip_load += g_T * delta_J(bzt::i,bzt::j);
  ip_load *= lambda_T * int_point.getMathIP().alpha();
}

//------------------------------------------------------------------
double LMortarEdgeImpl::calcIPLoadLambdaStick( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                               double jacobian_det, double g_T, double dual_shape)
{
  return( g_T * jacobian_det * dual_shape * int_point.getMathIP().alpha() );
}

//##################################################################
// SLIP CASE 
//##################################################################

//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPStiffnessUUSlip( AssembleBase<4>& ip_stiffness,
                                             MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                             bz::Array<double,2>& delta_g_T,
                                             bz::Array<double,4>& delta_delta_g_T, 
                                             bz::Array<double,2>& delta_J,
                                             double jacobian_det, double g_T, double t_T )
{ 
  ip_stiffness += delta_delta_g_T(bzt::i,bzt::j,bzt::k,bzt::l) * jacobian_det;
  ip_stiffness += delta_g_T(bzt::i,bzt::j) * delta_J(bzt::k,bzt::l);
  ip_stiffness *= t_T * int_point.getMathIP().alpha();
}

//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPStiffnessLambdaSlip( AssembleBase<2>& ip_stiffness,
                                                 MortarElement& mortar_element, BoundaryIntegrationPoint& int_point, Node& node,
                                                 bz::Array<double,2>& delta_g_T, 
                                                 double jacobian_det, double dual_shape)
{
  ip_stiffness += delta_g_T(bzt::i,bzt::j) * jacobian_det;
  ip_stiffness *= dual_shape * mortar_element.getContact().getMu() * SIGN(node.getProperty<int>("slide")) * (-1.) * 
    int_point.getMathIP().alpha();
}

//------------------------------------------------------------------
void LMortarEdgeImpl::calcIPLoadUSlip( AssembleBase<2>& ip_load,
                                       MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                       bz::Array<double,2>& delta_g_T, 
                                       double jacobian_det, double t_T)
{
  ip_load += delta_g_T(bzt::i,bzt::j) * jacobian_det;
  ip_load *= t_T * int_point.getMathIP().alpha();
}
