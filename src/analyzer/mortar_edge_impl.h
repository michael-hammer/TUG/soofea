#ifndef mortar_edge_impl_h__
#define mortar_edge_impl_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <float.h>
#include "boost/tuple/tuple.hpp"

#include "model/mortar_element.h"
#include "model/model.h"
#include "geometry/cartesian_coord_sys.h"

#include "edge_impl.h"

//------------------------------------------------------------------
class MortarEdgeImpl :
  public EdgeImpl
{
public:
  typedef enum GeomTupleOrder_ {
    DIST = 0, 
    XI,
    EDGE_INDEX
  } GeomTupleOrder;

  MortarEdgeImpl(IDObject::ID type_id):
    EdgeImpl(type_id)
  {}

  virtual ~MortarEdgeImpl() {}

  virtual GlobalAssembleBase<2>* calcMortarElementStiffness(MortarElement& element) = 0;
  virtual GlobalAssembleBase<1>* calcMortarElementLoadVector(MortarElement& element) = 0;

  /**
     The integration point projection is executed here and we calc
     $\Xi$ and $g_N$. See Box 8.12 on page 87 (Fischer) This method is
     called bevore each iteration and the calculation of the mortar
     stiffness and load entries.
   */
  virtual void calcKinematics( MortarElement& mortar_element , bool research_best_edge , bool new_time_step ) = 0;

  virtual void resetNodalCondition( MortarElement& mortar_element ) = 0;

  /**
     calculate either \tilde{\lambda}_p or \tilde{t}_p to decide the
     active set later on.
   */
  virtual void calcNodalCondition( MortarElement& mortar_element ) = 0;

  /**
     Search and find the potential contact

     @return if contact correspondency has changed in respect to last search
   */
  virtual bool contactSearch( MortarElement& mortar_element , Model& model ) = 0;
protected:
  virtual void pointProjectionM( BoundaryIntegrationPoint& int_point, 
				 Edge& edge,
				 double& distance, double& xi);  

  virtual double slipM( Edge& new_edge , double new_xi,
			Edge& old_edge , double old_xi );

  /**
     Get nearest non-mortar corner node for given non_mortar_point
  */
  Node* findNearestMortarCorner( BoundaryIntegrationPoint& int_point,
				 Model& model, 
				 double& dist );

  /**
     Search and find the potential contact for concentrated integration

     @return if contact correspondency has changed in respect to last search
   */
  virtual bool contactSearchG( MortarElement& mortar_element , Model& model );

  virtual void calcKinematicsGM( MortarElement& mortar_element , bool research_best_edge , bool new_time_step );

  virtual void resetNodalConditionL( MortarElement& mortar_element );

  virtual void calcNodalConditionL( MortarElement& mortar_element );

  bool updateActiveSetL( MortarElement& mortar_element );

  //------------------------------------------------------------------
  struct RankProjection {
    inline bool inside( const double& xi ) {
      return( ( fabs(xi) <= 1. ) ? ( true ) : ( false ) );
    }

    // returns true if geom1 < geom2
    bool operator()( const boost::tuple<double,double,unsigned>& geom1, const boost::tuple<double,double,unsigned>& geom2 ) {
      if( inside( geom1.get<MortarEdgeImpl::XI>() ) ) {
	if( inside( geom2.get<MortarEdgeImpl::XI>() ) ) // both are inside [-1,1], the shortest distance wins
	  return( geom1.get<MortarEdgeImpl::DIST>() < geom2.get<MortarEdgeImpl::DIST>() );
	else // only geom1 is inside [-1,1] -> wins
	  return( true );
      } else { // geom1 is outside range
	if( inside( geom2.get<MortarEdgeImpl::XI>() ) ) // only geom2 is inside [-1,1] -> wins
	  return( false );
	else // both are outside [-1,1], the shortest distance wins
	  return( geom1.get<MortarEdgeImpl::DIST>() < geom2.get<MortarEdgeImpl::DIST>() );
      }	
    }
  };
};

#endif
