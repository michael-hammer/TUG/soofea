/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "a_mortar_edge_impl.h"

//------------------------------------------------------------------
void AMortarEdgeImpl::deltaN( bz::Array<double,3>& array,
                               bz::Array<double,1>& averaging_tensor, bz::Array<double,1>& average_tangent,
                               double alpha )
{
  bz::Array<double,3>* epsilon = Math::epsilon<double>();
  bz::Array<double,1> e_3(3); 
  e_3 = 0., 0., 1.;
  bz::Array<double,2> delta( 3 , 3 );
  Math::kronecker(delta);

  bz::Array<double,1> average_tangent_3D(3);
  average_tangent_3D = 0.;
  average_tangent_3D(bz::Range(0,1)) = average_tangent;

  bz::Array<double,2> temp_1(delta / alpha);
  temp_1 -= average_tangent_3D(bzt::i) * average_tangent_3D(bzt::j) / pow( alpha , 3. );

  // 3D vs 2D
  bz::Array<double,2> temp_2( bz::sum( (*epsilon)(bzt::i,bzt::j,bzt::k) * e_3(bzt::k) , bzt::k ) );
  delete epsilon;
  bz::Array<double,3> temp_3( bz::sum( temp_2(bzt::k,bzt::l) * temp_1(bzt::i,bzt::l) * averaging_tensor(bzt::j) , bzt::l ) );

  array = temp_3(bz::Range(0,1),bz::Range::all(),bz::Range(0,1));
}

//------------------------------------------------------------------
void AMortarEdgeImpl::deltaXi( bz::Array<double,2>& array,
                                bz::Array<double,3>& delta_n, bz::Array<double,1>& average_tangent,
                                bz::Array<double,1>& shape, bz::Array<double,1>& mortar_tangent,
                                double g_N )
{
  bz::Array<double,2> delta( 2 , 2 );
  Math::kronecker(delta);

  double scaler = bz::sum( average_tangent * mortar_tangent );
  
  bz::Array<double,3> temp_2(2,(shape.shape())[0],2);
  temp_2 = (-1.) * shape(bzt::j) * delta(bzt::i,bzt::k);
  bz::Array<double,3> temp_3( delta_n.shape() );
  temp_3 = ( temp_2 + g_N * delta_n ) / scaler;

  array = bz::sum( temp_3(bzt::i,bzt::j,bzt::k) * average_tangent(bzt::k) , bzt::k );
}

//------------------------------------------------------------------
void AMortarEdgeImpl::deltaG_N( bz::Array<double,2>& delta_g_N , 
                                 bz::Array<double,2>& delta_xi, bz::Array<double,1>& average_normal, 
                                 bz::Array<double,1>& shape , bz::Array<double,1>& mortar_tangent )
{
  bz::Array<double,2> delta( 2 , 2 );
  Math::kronecker(delta);

  bz::Array<double,3> temp_1(2,(shape.shape())[0],2);
  temp_1 = shape(bzt::j) * delta(bzt::i,bzt::k);
  temp_1 += mortar_tangent(bzt::k) * delta_xi(bzt::i,bzt::j);

  delta_g_N = bz::sum( temp_1(bzt::i,bzt::j,bzt::k) * average_normal(bzt::k) , bzt::k );
}

//------------------------------------------------------------------
void AMortarEdgeImpl::deltaG_T( bz::Array<double,2>& array, 
                                 bz::Array<double,2>& delta_xi, 
                                 bz::Array<double,1>& mortar_tangent, bz::Array<double,1>& average_tangent )
{
  bz::Array<double,1> s(2); 
  s = average_tangent/Math::norm( average_tangent );
  array = (-1.) * delta_xi * bz::sum( mortar_tangent * s );
}

//------------------------------------------------------------------
void AMortarEdgeImpl::deltaDeltaN( bz::Array<double,5>& array,
                                    bz::Array<double,1>& averaging_tensor, bz::Array<double,1>& average_tangent,
                                    double alpha )
{
  bz::Array<double,3>* epsilon = Math::epsilon<double>();
  bz::Array<double,1> e_3(3); 
  e_3 = 0., 0., 1.;
  bz::Array<double,2> delta( 3 , 3 );
  Math::kronecker(delta);

  bz::Array<double,1> average_tangent_3D(3);
  average_tangent_3D = 0.;
  average_tangent_3D(bz::Range(0,1)) = average_tangent;

  bz::Array<double,5> temp_1( 3 , array.shape()[1] , 3 , array.shape()[3] , 3 );
  temp_1 = averaging_tensor(bzt::j) * delta(bzt::i,bzt::m) * average_tangent_3D(bzt::k) * averaging_tensor(bzt::l);
  
  bz::Array<double,5> temp_2( 3 , array.shape()[1] , 3 , array.shape()[3] , 3 );
  temp_2 = ( temp_1(bzt::i,bzt::j,bzt::k,bzt::l,bzt::m) + temp_1(bzt::k,bzt::j,bzt::i,bzt::l,bzt::m) ) / pow( alpha, 3. );
  bz::Array<double,4> temp_3( 3 , array.shape()[1] , 3 , array.shape()[3] );
  temp_3 = averaging_tensor(bzt::j) * delta(bzt::i,bzt::k) * averaging_tensor(bzt::l) / pow( alpha, 3. );
  temp_3 -= averaging_tensor(bzt::j) * average_tangent_3D(bzt::i) * 3. * average_tangent_3D(bzt::k) * averaging_tensor(bzt::l)  
    / pow( alpha, 5. );
  
  bz::Array<double,5> temp_5( 3 , array.shape()[1] , 3 , array.shape()[3] , 3 );
  temp_5 = (-1.) * temp_2(bzt::i,bzt::j,bzt::k,bzt::l,bzt::m) - 
    temp_3(bzt::i,bzt::j,bzt::k,bzt::l) * average_tangent_3D(bzt::m);

  bz::Array<double,2> temp_6( 3 ,3 );
  temp_6 = bz::sum( (*epsilon)(bzt::i,bzt::j,bzt::k) * e_3(bzt::k) , bzt::k );
  delete epsilon;
  bz::Array<double,5> temp_7( array.shape()[0] , array.shape()[1] , array.shape()[2] , array.shape()[3] , 3 );
  temp_7 = bz::sum( temp_6(bzt::m,bzt::n) * temp_5(bzt::i,bzt::j,bzt::k,bzt::l,bzt::n) , bzt::n );

  array = temp_7(bz::Range(0,1),bz::Range::all(),bz::Range(0,1),bz::Range::all(),bz::Range(0,1));
}

//------------------------------------------------------------------
void AMortarEdgeImpl::deltaDeltaXi( bz::Array<double,4>& delta_delta_xi, 
                                     bz::Array<double,2>& delta_xi, bz::Array<double,3>& delta_n, 
                                     bz::Array<double,2>& delta_g_N, bz::Array<double,5>& delta_delta_n, bz::Array<double,1>& der_shape_2, 
                                     bz::Array<double,1>& average_tangent, bz::Array<double,1>& mortar_tangent,
                                     double g_N )
{
  bz::Array<double,1> scaled_vec( average_tangent / bz::sum( average_tangent * mortar_tangent ) );

  bz::Array<double,2> temp_1( (delta_delta_xi.shape())(0), (delta_delta_xi.shape())(1) );
  temp_1 = bz::sum( delta_n(bzt::i,bzt::j,bzt::k) * scaled_vec(bzt::k) , bzt::k );

  bz::Array<double,4> temp_2( delta_delta_xi.shape() );
  temp_2 = delta_g_N(bzt::i,bzt::j) * temp_1(bzt::k,bzt::l);

  delta_delta_xi = temp_2;
  delta_delta_xi += temp_2(bzt::k,bzt::l,bzt::i,bzt::j);
  delta_delta_xi += bz::sum( delta_delta_n(bzt::i,bzt::j,bzt::k,bzt::l,bzt::m) * scaled_vec(bzt::m) , bzt::m ) * g_N;

  // b_o is 0 for linear shape functions

  bz::Array<double,4> temp_3( delta_delta_xi.shape() );
  temp_3 = scaled_vec(bzt::i) * der_shape_2(bzt::j) * delta_xi(bzt::k,bzt::l);
  delta_delta_xi -= temp_3(bzt::i,bzt::j,bzt::k,bzt::l);
  delta_delta_xi -= temp_3(bzt::k,bzt::l,bzt::i,bzt::j);
}

//------------------------------------------------------------------
void AMortarEdgeImpl::deltaDeltaG_T( bz::Array<double,4>& array, 
                                      bz::Array<double,4>& delta_delta_xi, 
                                      bz::Array<double,2>& delta_xi, bz::Array<double,1>& averaging_tensor, 
                                      bz::Array<double,1>& der_shape_2, bz::Array<double,1>& mortar_tangent, bz::Array<double,1>& average_tangent )
{
  bz::Array<double,1> s(2); 
  double alpha = Math::norm( average_tangent );
  s = average_tangent/alpha;

  bz::Array<double,2> delta( 2 , 2 );
  Math::kronecker(delta);

  bz::Array<double,2> delta_s_a( (array.shape())(0), (array.shape())(1) ); delta_s_a = 0.;
  delta_s_a = 
    averaging_tensor(bzt::j) * mortar_tangent(bzt::i) / alpha - 
    averaging_tensor(bzt::j) * bz::sum( average_tangent * mortar_tangent ) * average_tangent(bzt::i) / pow(alpha,3.);
    
  array -= delta_s_a(bzt::k,bzt::l) * delta_xi(bzt::i,bzt::j);
  array -= s(bzt::k) * der_shape_2(bzt::l) * delta_xi(bzt::i,bzt::j); 
  array -= delta_delta_xi * bz::sum( mortar_tangent * s );
}

//------------------------------------------------------------------
void AMortarEdgeImpl::deltaDeltaG_N( bz::Array<double,4>& delta_delta_g_N, 
                                      bz::Array<double,5>& delta_delta_n, bz::Array<double,4>& delta_delta_xi, 
                                      bz::Array<double,2>& delta_xi, bz::Array<double,1>& der_shape_2, 
                                      bz::Array<double,1>& average_normal, bz::Array<double,1>& mortar_tangent, 
                                      double g_N )
{
  delta_delta_g_N = bz::sum( delta_delta_n(bzt::i,bzt::j,bzt::k,bzt::l,bzt::m) * average_normal(bzt::m), bzt::m ) * g_N * (-1.);
  delta_delta_g_N += delta_delta_xi * bz::sum( average_normal * mortar_tangent );

  // b_o is 0 for linear shape functions

  bz::Array<double,4> temp_1( delta_delta_g_N.shape() );
  temp_1 = average_normal(bzt::i) * der_shape_2(bzt::j) * delta_xi(bzt::k,bzt::l);
  delta_delta_g_N += temp_1;
  delta_delta_g_N += temp_1(bzt::k,bzt::l,bzt::i,bzt::j);  
}
