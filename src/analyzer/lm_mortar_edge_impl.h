#ifndef lm_mortar_edge_impl_h___
#define lm_mortar_edge_impl_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "mortar_edge_impl.h"

#include "m_mortar_edge_impl.h"
#include "l_mortar_edge_impl.h"

//------------------------------------------------------------------
class LMMortarEdgeImpl :
  public MortarEdgeImpl,
  public MMortarEdgeImpl,
  public LMortarEdgeImpl
{
public:
  LMMortarEdgeImpl() :
    MortarEdgeImpl(IDObject::MORTAR_LM) {}

  virtual ~LMMortarEdgeImpl() {}

  virtual GlobalAssembleBase<2>* calcMortarElementStiffness(MortarElement& mortar_element);
  virtual GlobalAssembleBase<1>* calcMortarElementLoadVector(MortarElement& mortar_element);

  virtual void calcKinematics( MortarElement& mortar_element , bool research_best_edge , bool new_time_step );

  virtual void resetNodalCondition( MortarElement& mortar_element );

  virtual void calcNodalCondition( MortarElement& mortar_element );

  virtual bool contactSearch( MortarElement& mortar_element , Model& model );
protected:
  static int DISP_DOF_AMOUNT;
  
  template<int T>
  GlobalAssembleBase<T>* initiateGlobals(MortarElement& mortar_element);

  template<int T>
  AssembleBase<T>* initiateLocals(MortarElement& mortar_element, BoundaryIntegrationPoint& int_point);

#ifdef SOOFEA_WITH_TBB
  class IntPointStiffness {
  public:
    IntPointStiffness( LMMortarEdgeImpl& mortar_impl,
                       GlobalAssembleBase<2>& element_stiffness, 
		       MortarElement& mortar_element ) :
      mortar_impl_(mortar_impl),
      element_stiffness_(element_stiffness),
      mortar_element_(mortar_element) {}

    IntPointStiffness( const IntPointStiffness& src ) :
      mortar_impl_(src.mortar_impl_),
      element_stiffness_(src.element_stiffness_),
      mortar_element_(src.mortar_element_) {}

    ~IntPointStiffness() {}

    void operator()( const tbb::blocked_range<size_t>& range ) const;

  protected:
    LMMortarEdgeImpl& mortar_impl_;
    GlobalAssembleBase<2>& element_stiffness_;
    MortarElement& mortar_element_;
  };
  class IntPointLoad {
  public:
    IntPointLoad( LMMortarEdgeImpl& mortar_impl,
                       GlobalAssembleBase<1>& element_load, 
		       MortarElement& mortar_element ) :
      mortar_impl_(mortar_impl),
      element_load_(element_load),
      mortar_element_(mortar_element) {}

    IntPointLoad( const IntPointLoad& src ) :
      mortar_impl_(src.mortar_impl_),
      element_load_(src.element_load_),
      mortar_element_(src.mortar_element_) {}

    ~IntPointLoad() {}

    void operator()( const tbb::blocked_range<size_t>& range ) const;

  protected:
    LMMortarEdgeImpl& mortar_impl_;
    GlobalAssembleBase<1>& element_load_;
    MortarElement& mortar_element_;
  };
#endif

  void assemblePerIPStiffness( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                               GlobalAssembleBase<2>& element_stiffness );
  void assemblePerIPLoad( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                          GlobalAssembleBase<1>& element_load );
};


#endif
