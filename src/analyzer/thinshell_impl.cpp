/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "thinshell_impl.h"

#ifdef SOOFEA_WITH_TBB
tbb::queuing_mutex ThinshellImpl::node_internal_load_mutex;
#endif

//------------------------------------------------------------------
AssembleBase<4>* ThinshellImpl::calcElementStiffness(bool start_phase, Element& element)
{
  unsigned node_amount = element.getType().getShape().getNodeAmount();

  std::cout << "STIFFNESS node amount = " << node_amount << std::endl;

//  bz::Array<double,4> *array_constitutive = 0;
//  bz::Array<double,4> *array_stress = 0;

  bz::TinyVector<int,4> size_4D;
  size_4D = 3, node_amount, 3, node_amount;

  bz::TinyVector<int,2> size_2D;
  size_2D = 3 * node_amount, 3 * node_amount;

//  Math::TNumIntFunctorArray<ThinshellImpl,ElementIntegrationPoint,4>
//    functor_array_constitutive( this, &ThinshellImpl::integrateConstitutiveComponent );
//  array_constitutive = Math::NumInt::integrateArray<ElementIntegrationPoint,4>( functor_array_constitutive,
//                                                                                size_4D,
//                                                                                element );
//
//  Math::TNumIntFunctorArray<ThinshellImpl,ElementIntegrationPoint,4>
//    functor_array_stress( this, &ThinshellImpl::integrateStressComponent );
//  array_stress = Math::NumInt::integrateArray<ElementIntegrationPoint,4>( functor_array_stress, 
//                                                                          size_4D,
//                                                                          element );

  AssembleBase<4>* element_stiffness = new AssembleBase<4>(size_4D); // allready initialized with 0.
//  (*element_stiffness) += *array_constitutive;
//  (*element_stiffness) += *array_stress;
//  delete array_stress;
//  delete array_constitutive;

  element_stiffness -> addCoordID( CoordSys::X );
  element_stiffness -> addCoordID( CoordSys::Y );
  element_stiffness -> addCoordID( CoordSys::Z );

  for( Iterator<Node> node_iter = element.Aggregate<Node>::begin();
       node_iter != element.Aggregate<Node>::end() ; 
       ++node_iter )
    element_stiffness -> addDOF( node_iter -> getDOF( IDObject::DISPLACEMENT ) );

  return( element_stiffness );
}

//------------------------------------------------------------------
AssembleBase<2>* ThinshellImpl::calcElementLoadVector(bool start_phase, Element& element)
{
  unsigned node_amount = element.getType().getShape().getNodeAmount();

  std::cout << "LOAD node amount = " << node_amount << std::endl;

  bz::TinyVector<int,2> size;
  size = 3, node_amount;

//  Math::TNumIntFunctorArray<ThinshellImpl,ElementIntegrationPoint,2> 
//    functor_array_internal( this, &ThinshellImpl::integrateInternalForces );
//  bz::Array<double,2>* array_internal = Math::NumInt::integrateArray( functor_array_internal, 
//								      size,
//								      element);

  AssembleBase<2>* element_load = new AssembleBase<2>(size); // allready initialized with 0.
  (*element_load) += (-1.); // * (*array_internal);

  //  delete array_internal;

  element_load -> addCoordID( CoordSys::X );
  element_load -> addCoordID( CoordSys::Y );
  element_load -> addCoordID( CoordSys::Z );
 
  int node_counter = 0;
  for( Iterator<Node> node_iter = element.Aggregate<Node>::begin();
       node_iter != element.Aggregate<Node>::end() ; 
       ++node_iter ) {
    element_load -> addDOF( node_iter -> getDOF( IDObject::DISPLACEMENT ) );
    { 
#ifdef SOOFEA_WITH_TBB
      tbb::queuing_mutex::scoped_lock lock(node_internal_load_mutex);
#endif
      node_iter -> getLoad( IDObject::FORCE ) -> getValue( CoordSys::X ) -> addInternal( (*element_load)(0,node_counter) );
      node_iter -> getLoad( IDObject::FORCE ) -> getValue( CoordSys::Y ) -> addInternal( (*element_load)(1,node_counter) );
      ++node_counter;
    }
  }

  return( element_load );
}

//------------------------------------------------------------------
void ThinshellImpl::integrateConstitutiveComponent(bz::Array<double,4>& array, Element& element, 
						   ElementIntegrationPoint& int_point)
{
//  bz::Array<double,2>& def_grad_tensor = int_point.getDeformationGradient();
//  element.getMaterial().getConstitutive().setElasticityTensor( int_point );
//
//  bz::Array<double,4> buffer( 2, 2, 2, (int_point.getNJ()->shape())[0] ); 
//  buffer = def_grad_tensor(bzt::k,bzt::i) * (*(int_point.getNJ()))(bzt::l,bzt::j);
//  bz::Array<double,4> temp( bz::sum(bz::sum( buffer(bzt::m,bzt::n,bzt::i,bzt::j) *
//                                             (int_point.getElasticityTensor())(bzt::m,bzt::n,bzt::k,bzt::l) , bzt::n), bzt::m) );
//
//  array = bz::sum( bz::sum( temp(bzt::i,bzt::j,bzt::m,bzt::n) * buffer(bzt::m,bzt::n,bzt::k,bzt::l), bzt::n) , bzt::m) * 
//    int_point.getLagrangianDet();
}

//------------------------------------------------------------------
void ThinshellImpl::integrateStressComponent(bz::Array<double,4>& array, Element& element, 
					     ElementIntegrationPoint& int_point)
{
//  bz::Array<double,2> temp_1( bz::sum( (*(int_point.getNJ()))(bzt::i,bzt::k) * 
//                                       (int_point.getStress())(bzt::k,bzt::j) , bzt::k ) );
//  bz::Array<double,2> temp_2( bz::sum( temp_1(bzt::i,bzt::k) * (*(int_point.getNJ()))(bzt::j,bzt::k) , bzt::k ) );
//
//  array = temp_2(bzt::j,bzt::l) * Math::identity2x2(bzt::i,bzt::k) * int_point.getLagrangianDet();
}

//------------------------------------------------------------------
void ThinshellImpl::integrateInternalForces(bz::Array<double,2>& array, Element& element, 
					    ElementIntegrationPoint& int_point)
{
//  bz::Array<double,2>& def_grad_tensor = int_point.getDeformationGradient();
//  bz::Array<double,2> temp1( sum( (int_point.getStress())(bzt::j,bzt::k) * (*(int_point.getNJ()))(bzt::i,bzt::k) ,bzt::k) );
//
//  array = sum( def_grad_tensor(bzt::i,bzt::k) * temp1(bzt::j,bzt::k) ,bzt::k) * int_point.getLagrangianDet();
}

//------------------------------------------------------------------
void ThinshellImpl::calcStress(Element& element)
{
//  for( Iterator<ElementIntegrationPoint> iter = element.Aggregate<ElementIntegrationPoint>::begin();
//       iter != element.Aggregate<ElementIntegrationPoint>::end() ; 
//       ++iter ) {  
//    element.getMaterial().getConstitutive().setStress( *iter );
//  }
}
