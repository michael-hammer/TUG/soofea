/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "lm_mortar_edge_impl.h"

int LMMortarEdgeImpl::DISP_DOF_AMOUNT = 2;

//------------------------------------------------------------------
void LMMortarEdgeImpl::resetNodalCondition( MortarElement& mortar_element )
{
  resetNodalConditionL( mortar_element );
}

//------------------------------------------------------------------
void LMMortarEdgeImpl::calcNodalCondition( MortarElement& mortar_element )
{
  calcNodalConditionL( mortar_element );
}

//------------------------------------------------------------------
bool LMMortarEdgeImpl::contactSearch( MortarElement& mortar_element , Model& model )
{
  return( contactSearchG( mortar_element, model ) );
}

//------------------------------------------------------------------
void LMMortarEdgeImpl::calcKinematics( MortarElement& mortar_element , bool research_best_edge , bool new_time_step )
{
  calcKinematicsGM( mortar_element , research_best_edge, new_time_step );
}

//------------------------------------------------------------------
template<int T>
GlobalAssembleBase<T>* LMMortarEdgeImpl::initiateGlobals(MortarElement& mortar_element)
{
  int node_amount = mortar_element.getNonMortarEdge()->getType().getShape().getNodeAmount() +
    mortar_element.getMortarNodeAmount();

  int lagrange_dof_amount = 0;
  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter ) {
    if( non_mortar_node_iter -> getProperty<bool>("mortar_active") ) {
      ++lagrange_dof_amount;
      if( non_mortar_node_iter -> hasProperty("slide") ) {
        if( non_mortar_node_iter -> getProperty<int>("slide") == 0 )
          ++lagrange_dof_amount;
      }
    }
  }

  bz::TinyVector<int,T> size;
  if( T == 2 ) {
    size = lagrange_dof_amount + DISP_DOF_AMOUNT*node_amount,
      lagrange_dof_amount + DISP_DOF_AMOUNT*node_amount;
  } else {
    size = lagrange_dof_amount + DISP_DOF_AMOUNT*node_amount; 
  }

  GlobalAssembleBase<T>* global = new GlobalAssembleBase<T>(size);

  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter ) {
    if( non_mortar_node_iter -> getProperty<bool>("mortar_active") ) {
      global -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::LAGRANGE ) -> getValue(CoordSys::NORMAL) );
      if( non_mortar_node_iter -> hasProperty("slide") ) {
        if( non_mortar_node_iter -> getProperty<int>("slide") == 0 ) {
          global -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::LAGRANGE ) -> getValue(CoordSys::TANGENT_R) );
        }
      }
    }
    global -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) );
    global -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) );
  }

  // Here we have to do a unification so that we have each DOFValue only once!
  std::vector<Node*>* node_vector = mortar_element.getUniqueMortarNodes();
  for( std::vector<Node*>::iterator mortar_node_iter = node_vector->begin();
       mortar_node_iter != node_vector->end() ;
       ++mortar_node_iter ) {
    global -> addDOFValue( (*mortar_node_iter) -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) );
    global -> addDOFValue( (*mortar_node_iter) -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) );
  }
  delete node_vector;

  return( global );
}

//------------------------------------------------------------------
template<int T>
AssembleBase<T>* LMMortarEdgeImpl::initiateLocals(MortarElement& mortar_element, BoundaryIntegrationPoint& int_point)
{
  int node_amount = mortar_element.getNonMortarEdge()->getType().getShape().getNodeAmount() +
    int_point.getActiveMortarEdge().getType().getShape().getNodeAmount();

  bz::TinyVector<int,T> size;
  if( T == 4 ) {
    size = DISP_DOF_AMOUNT , node_amount , 
      DISP_DOF_AMOUNT , node_amount;
  } else {
    size = DISP_DOF_AMOUNT , node_amount;    
  }
  AssembleBase<T>* local = new AssembleBase<T>(size);
  local -> addCoordID( CoordSys::X );
  local -> addCoordID( CoordSys::Y );
  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter )
    local -> addDOF( non_mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) );
  for( Iterator<Node> mortar_node_iter = int_point.getActiveMortarEdge().Aggregate<Node>::begin();
       mortar_node_iter != int_point.getActiveMortarEdge().Aggregate<Node>::end() ; 
       ++mortar_node_iter )
    local -> addDOF( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) );

  return( local );
}

//------------------------------------------------------------------
GlobalAssembleBase<2>* LMMortarEdgeImpl::calcMortarElementStiffness(MortarElement& mortar_element)
{
  int active_non_mortar_node_amount = mortar_element.getActiveNonMortarNodeAmount();
  if( active_non_mortar_node_amount == 0 )
    return( 0 );

  LOG(Logger::INFO) << "Mortar Stiffness (lagrange) of non-mortar (primary) Edge: " 
                    << mortar_element.getNonMortarEdge()->getNumber()
                    << " and active nodes = " << active_non_mortar_node_amount << Logger::endl;

  GlobalAssembleBase<2>* element_stiffness = initiateGlobals<2>( mortar_element );

#ifdef SOOFEA_WITH_TBB
  tbb::parallel_for( tbb::blocked_range<size_t>(0,mortar_element.getNonMortarEdge()->getIntegrationPointAmount()), 
                     LMMortarEdgeImpl::IntPointStiffness(*this,*element_stiffness,mortar_element) );
#else
  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( ! ip_iter -> getActive() )
      continue;
    assemblePerIPStiffness( mortar_element,
                            *ip_iter,
                            *element_stiffness );
  }  
#endif

  // (*element_stiffness) *= mortar_element.getNonMortarElement()->getType().getProperty<double>("height");
  //  std::cout << "Element stiffness: " << (*element_stiffness) << std::endl;
  return( element_stiffness );
}

#ifdef SOOFEA_WITH_TBB
//------------------------------------------------------------------
void LMMortarEdgeImpl::IntPointStiffness::operator()( const tbb::blocked_range<size_t>& range ) const
{ 
  for( size_t iter = range.begin();
       iter != range.end();
       ++iter ) {
    if( ! mortar_element_.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::at( iter ).getActive() )
      continue;

    mortar_impl_.assemblePerIPStiffness( mortar_element_,
                                         mortar_element_.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::at( iter ),
                                         element_stiffness_ );
  }
}
#endif

//------------------------------------------------------------------
void LMMortarEdgeImpl::assemblePerIPStiffness( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                                 GlobalAssembleBase<2>& element_stiffness )
{
  int non_mortar_node_amount = mortar_element.getNonMortarEdge()->getType().getShape().getNodeAmount();
  CartesianCoordSys coord_sys_3D(3);
  Vector e3( &coord_sys_3D,0,0,1  );
  int local_mortar_node_amount = int_point.getActiveMortarEdge().getType().getShape().getNodeAmount();
  bz::TinyVector<int,4> local_size;
  local_size = DISP_DOF_AMOUNT , non_mortar_node_amount+local_mortar_node_amount , 
    DISP_DOF_AMOUNT , non_mortar_node_amount+local_mortar_node_amount;

  bz::Array<double,1>* shape = mortar_element.getShapeTensorM( int_point );
  bz::Array<double,1>* der_shape_1 = mortar_element.getDerivativeShapeTensorM( (int_point) , Contact::NON_MORTAR );
  bz::Array<double,1>* der_shape_2 = mortar_element.getDerivativeShapeTensorM( (int_point) , Contact::MORTAR );
  Vector* mortar_tangent = int_point.getActiveMortarEdge().getEulerianTangent( int_point.xi() );
  Vector* mortar_normal = int_point.getActiveMortarEdge().getEulerianNormal( int_point.xi(), 0., true );
  Vector* non_mortar_tangent = mortar_element.getNonMortarEdge()->getEulerianTangent( int_point.getMathIP().r() );
  double jacobian_det = Math::norm( *non_mortar_tangent , 2 );
  double alpha = bz::sum( (*mortar_tangent) * (*mortar_tangent) );
  double g_N = int_point.getGN();
  double g_T = int_point.getGT();
  
  bz::Array<double,2> delta_g_N(local_size(0),local_size(1)); delta_g_N = 0.;
  deltaG_N( delta_g_N, *shape, *mortar_normal );
  
  bz::Array<double,4> delta_delta_g_N(local_size); delta_delta_g_N = 0.;
  deltaDeltaG_N( delta_delta_g_N, *shape, *der_shape_2, *mortar_tangent, *mortar_normal, g_N );
  
  bz::Array<double,2> delta_J(local_size(0),local_size(1)); delta_J = 0.;
  deltaJ( delta_J, *der_shape_1, *non_mortar_tangent, jacobian_det );
  
  bz::Array<double,4> delta_delta_J(local_size); delta_delta_J = 0.;
  deltaDeltaJ( delta_delta_J, *der_shape_1, *non_mortar_tangent, jacobian_det );
  
  bz::Array<double,3> delta_N(local_size(0),local_size(1),local_size(0)); delta_N = 0.;
  deltaN( delta_N, *der_shape_2 , *mortar_tangent, *mortar_normal, alpha );
  
  bz::Array<double,5> delta_delta_N(local_size(0),local_size(1),local_size(2),local_size(3),local_size(0)); delta_delta_N = 0.;
  deltaDeltaN( delta_delta_N, *der_shape_2 , *mortar_tangent, *mortar_normal, alpha );
  
  bz::Array<double,2> delta_xi(local_size(0),local_size(1)); delta_xi = 0.;
  deltaXi( delta_xi, *shape, *der_shape_2, *mortar_tangent, *mortar_normal, g_N, alpha );  
  
  bz::Array<double,2> delta_g_T(local_size(0),local_size(1)); delta_g_T = 0.;
  deltaG_T( delta_g_T , delta_xi , *mortar_tangent );
  
  bz::Array<double,4> delta_delta_g_T(local_size); delta_delta_g_T = 0.;
  deltaDeltaG_T( delta_delta_g_T, delta_xi, delta_g_N, delta_N, delta_delta_N, *der_shape_2, *mortar_tangent, g_N, alpha );
  
  AssembleBase<4>* ip_stiffness_4;
  AssembleBase<2>* ip_stiffness_2;
  for( Iterator<Node> node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++node_iter ) {
    if( node_iter -> getProperty<bool>("mortar_active") ) {
      Edge* edge = mortar_element.getNonMortarEdge();
      int local_node_number = edge->getLocalNodeNumber( *node_iter );
      double dual_shape = edge->getType().getShape().dualShape( edge->getType().getShape().getNodeIndex(local_node_number) , int_point.getMathIP().r() );
      double lambda_N = node_iter->getLagrangeFactor().get(CoordSys::NORMAL) * dual_shape;
      
      ip_stiffness_4 = initiateLocals<4>( mortar_element, int_point );
      calcIPStiffnessUUN( *ip_stiffness_4,
                          mortar_element, int_point,
                          delta_g_N, 
                          delta_delta_g_N,
                          delta_J,
                          delta_delta_J,
                          jacobian_det, g_N, lambda_N );
      element_stiffness.assemble( *ip_stiffness_4 );

      ip_stiffness_2 = initiateLocals<2>( mortar_element, int_point );
      calcIPStiffnessLambdaN( *ip_stiffness_2,
                              mortar_element, int_point,
                              delta_g_N, 
                              delta_J,
                              jacobian_det, g_N, dual_shape );
      element_stiffness.assembleDual( *ip_stiffness_2,
                                      node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::NORMAL),
                                      GlobalAssembleBase<2>::ASSEMBLE_SYMMETRIC );
      
      delete ip_stiffness_4;
      delete ip_stiffness_2;
      if( node_iter -> hasProperty("slide") ) {
        if( node_iter -> getProperty<int>("slide") == 0 ) { // STICK
          double lambda_T = node_iter->getLagrangeFactor().get(CoordSys::TANGENT_R) * dual_shape;
          
          ip_stiffness_4 = initiateLocals<4>( mortar_element, int_point );
          calcIPStiffnessUUStick( *ip_stiffness_4,
                                  mortar_element, int_point,
                                  delta_g_T, 
                                  delta_delta_g_T,
                                  delta_J,
                                  delta_delta_J,
                                  jacobian_det, g_T, lambda_T );
          element_stiffness.assemble( *ip_stiffness_4 );
          
          ip_stiffness_2 = initiateLocals<2>( mortar_element, int_point );
          calcIPStiffnessLambdaStick( *ip_stiffness_2,
                                      mortar_element, int_point,
                                      delta_g_T, 
                                      delta_J,
                                      jacobian_det, g_T, dual_shape );
          element_stiffness.assembleDual( *ip_stiffness_2,
                                          node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::TANGENT_R), 
                                          GlobalAssembleBase<2>::ASSEMBLE_SYMMETRIC );
          
          delete ip_stiffness_4;
          delete ip_stiffness_2;
        } else { // SLIP
          double t_T = node_iter->getLagrangeFactor().get(CoordSys::NORMAL) * dual_shape * 
            mortar_element.getContact().getMu() * SIGN(node_iter->getProperty<int>("slide")) * (-1.);
          
          ip_stiffness_4 = initiateLocals<4>( mortar_element, int_point );
          calcIPStiffnessUUSlip( *ip_stiffness_4,
                                 mortar_element, int_point,
                                 delta_g_T, 
                                 delta_delta_g_T,
                                 delta_J,
                                 jacobian_det, g_T, t_T );
          element_stiffness.assemble( *ip_stiffness_4 );
          
          ip_stiffness_2 = initiateLocals<2>( mortar_element, int_point );
          calcIPStiffnessLambdaSlip( *ip_stiffness_2, 
                                     mortar_element, int_point, *node_iter,
                                     delta_g_T, 
                                     jacobian_det, dual_shape );
          element_stiffness.assembleDual( *ip_stiffness_2,
                                          node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::NORMAL) 
                                          // NO Symmetric assemble
                                          );
          
          delete ip_stiffness_4;
          delete ip_stiffness_2;
        }
      }
    }
  }
  
  delete shape;
  delete der_shape_1;
  delete der_shape_2;
  delete mortar_normal;
  delete mortar_tangent;
  delete non_mortar_tangent;
}

//------------------------------------------------------------------
GlobalAssembleBase<1>* LMMortarEdgeImpl::calcMortarElementLoadVector(MortarElement& mortar_element)
{
  int active_non_mortar_node_amount = mortar_element.getActiveNonMortarNodeAmount();
  if( active_non_mortar_node_amount == 0 )
    return( 0 );

  LOG(Logger::INFO) << "Mortar Load (lagrange) of non-mortar (primary) Edge: " 
                    << mortar_element.getNonMortarEdge()->getNumber()
                    << " and active nodes = " << active_non_mortar_node_amount << Logger::endl;

  GlobalAssembleBase<1>* element_load = initiateGlobals<1>( mortar_element );

#ifdef SOOFEA_WITH_TBB
  tbb::parallel_for( tbb::blocked_range<size_t>(0,mortar_element.getNonMortarEdge()->getIntegrationPointAmount()), 
                     LMMortarEdgeImpl::IntPointLoad(*this,*element_load,mortar_element) );
#else
  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( ! ip_iter -> getActive() )
      continue;

    assemblePerIPLoad( mortar_element,
                       *ip_iter,
                       *element_load );
  }  
#endif 

  (*element_load) *= (-1.); // * mortar_element.getNonMortarElement()->getType().getProperty<double>("height");
  //  std::cout << "Element load: " << (*element_load) << std::endl;
  return( element_load );
}

#ifdef SOOFEA_WITH_TBB
//------------------------------------------------------------------
void LMMortarEdgeImpl::IntPointLoad::operator()( const tbb::blocked_range<size_t>& range ) const
{ 
  for( size_t iter = range.begin();
       iter != range.end();
       ++iter ) {
    if( ! mortar_element_.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::at( iter ).getActive() )
      continue;

    mortar_impl_.assemblePerIPLoad( mortar_element_,
                                    mortar_element_.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::at( iter ),
                                    element_load_ );
  }
}
#endif

//------------------------------------------------------------------
void LMMortarEdgeImpl::assemblePerIPLoad( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                            GlobalAssembleBase<1>& element_load )
{
  int non_mortar_node_amount = mortar_element.getNonMortarEdge()->getType().getShape().getNodeAmount();
  CartesianCoordSys coord_sys_3D(3);
  Vector e3( &coord_sys_3D,0,0,1  );
  int local_mortar_node_amount = int_point.getActiveMortarEdge().getType().getShape().getNodeAmount();
  bz::TinyVector<int,2> local_size;
  local_size = DISP_DOF_AMOUNT , non_mortar_node_amount+local_mortar_node_amount;

  double g_N = int_point.getGN();
  double g_T = int_point.getGT();
  bz::Array<double,1>* shape = mortar_element.getShapeTensorM( int_point );
  bz::Array<double,1>* der_shape_1 = mortar_element.getDerivativeShapeTensorM( int_point , Contact::NON_MORTAR );
  bz::Array<double,1>* der_shape_2 = mortar_element.getDerivativeShapeTensorM( int_point , Contact::MORTAR );
  Vector* mortar_tangent = int_point.getActiveMortarEdge().getEulerianTangent( int_point.xi() );
  Vector* mortar_normal = int_point.getActiveMortarEdge().getEulerianNormal( int_point.xi() , 0. , true );
  Vector* non_mortar_tangent = mortar_element.getNonMortarEdge()->getEulerianTangent( int_point.getMathIP().r() );
  double jacobian_det = Math::norm( *non_mortar_tangent , 2 );
  double alpha = bz::sum( (*mortar_tangent) * (*mortar_tangent) );
  
  bz::Array<double,2> delta_g_N(local_size); delta_g_N = 0.;
  deltaG_N( delta_g_N, *shape, *mortar_normal );
  
  bz::Array<double,2> delta_J(local_size); delta_J = 0.;
  deltaJ( delta_J, *der_shape_1, *non_mortar_tangent, jacobian_det );
  
  bz::Array<double,2> delta_xi(local_size); delta_xi = 0.;
  deltaXi( delta_xi, *shape, *der_shape_2, *mortar_tangent, *mortar_normal, g_N, alpha );  
  
  bz::Array<double,2> delta_g_T(local_size); delta_g_T = 0.;
  deltaG_T( delta_g_T, delta_xi, *mortar_tangent );
  
  AssembleBase<2>* ip_load_2;
  for( Iterator<Node> node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++node_iter ) {
    if( node_iter -> getProperty<bool>("mortar_active") ) {
      Edge* edge = mortar_element.getNonMortarEdge();
      int local_node_number = edge->getLocalNodeNumber( *node_iter );
      double dual_shape = edge->getType().getShape().dualShape( edge->getType().getShape().getNodeIndex(local_node_number) , int_point.getMathIP().r() );
      double lambda_N = node_iter->getLagrangeFactor().get(CoordSys::NORMAL) * dual_shape;
      
      ip_load_2 = initiateLocals<2>( mortar_element, int_point );
      calcIPLoadUN( *ip_load_2,
                    mortar_element, int_point,
                    delta_g_N,
                    delta_J,
                    jacobian_det, g_N, lambda_N);
      element_load.assemble( *ip_load_2 );
      
      double ip_load_lambda_N = calcIPLoadLambdaN( mortar_element, int_point,
                                                   jacobian_det, g_N, dual_shape );
      element_load.assembleDual( ip_load_lambda_N , 
                                 node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::NORMAL) );
      
      delete ip_load_2;
      if( node_iter -> hasProperty("slide") ) {
        if( node_iter -> getProperty<int>("slide") == 0 ) { // STICK
          double lambda_T = node_iter->getLagrangeFactor().get(CoordSys::TANGENT_R) * dual_shape;
          
          ip_load_2 = initiateLocals<2>( mortar_element, int_point );
          calcIPLoadUStick( *ip_load_2,
                            mortar_element, int_point,
                            delta_g_T,
                            delta_J,
                            jacobian_det, g_T, lambda_T );
          element_load.assemble( *ip_load_2 );
          
          double ip_load_lambda_stick = calcIPLoadLambdaStick( mortar_element, int_point,
                                                               jacobian_det, g_T, dual_shape );
          element_load.assembleDual( ip_load_lambda_stick,
                                     node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::TANGENT_R) );
          
          delete ip_load_2;
        } else { // SLIDE
          double t_T = node_iter->getLagrangeFactor().get(CoordSys::NORMAL) * dual_shape * 
            mortar_element.getContact().getMu() * SIGN(node_iter->getProperty<int>("slide")) * (-1.);
          
          ip_load_2 = initiateLocals<2>( mortar_element, int_point );
          calcIPLoadUSlip( *ip_load_2,
                           mortar_element, int_point,
                           delta_g_T,
                           jacobian_det, t_T );
          element_load.assemble( *ip_load_2 );
          
          delete ip_load_2;
        }
      }
    }
  }
  
  delete shape;
  delete der_shape_1;
  delete der_shape_2;
  delete mortar_tangent;
  delete mortar_normal;
  delete non_mortar_tangent;
}

