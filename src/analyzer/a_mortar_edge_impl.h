#ifndef a_mortar_edge_impl_h__
#define a_mortar_edge_impl_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "math/array.h"

//------------------------------------------------------------------
class AMortarEdgeImpl
{
public:
  AMortarEdgeImpl() {}
  virtual ~AMortarEdgeImpl() {}

protected:
  void deltaN( bz::Array<double,3>& array,
               bz::Array<double,1>& averaging_tensor, bz::Array<double,1>& average_tangent,
               double alpha );

  void deltaXi( bz::Array<double,2>& array,
                bz::Array<double,3>& delta_n, bz::Array<double,1>& average_tangent,
                bz::Array<double,1>& shape, bz::Array<double,1>& mortar_tangent,
                double g_N );

  void deltaG_N( bz::Array<double,2>& delta_g_N , 
                 bz::Array<double,2>& delta_xi, bz::Array<double,1>& average_normal, 
                 bz::Array<double,1>& shape , bz::Array<double,1>& mortar_tangent );

  void deltaG_T( bz::Array<double,2>& array, 
		 bz::Array<double,2>& delta_xi, 
		 bz::Array<double,1>& mortar_tangent, bz::Array<double,1>& average_tangent );

  void deltaDeltaN( bz::Array<double,5>& array,
                    bz::Array<double,1>& averaging_tensor, bz::Array<double,1>& average_tangent,
                    double alpha );

  void deltaDeltaXi( bz::Array<double,4>& delta_delta_xi, 
                     bz::Array<double,2>& delta_xi, bz::Array<double,3>& delta_n, 
                     bz::Array<double,2>& delta_g_N, bz::Array<double,5>& delta_delta_n, bz::Array<double,1>& der_shape_2,
                     bz::Array<double,1>& average_tangent, bz::Array<double,1>& mortar_tangent,
                     double g_N );

  void deltaDeltaG_T( bz::Array<double,4>& array, 
		      bz::Array<double,4>& delta_delta_xi, 
		      bz::Array<double,2>& delta_xi, bz::Array<double,1>& averaging_tensor, 
		      bz::Array<double,1>& der_shape_2, bz::Array<double,1>& mortar_tangent, bz::Array<double,1>& average_tangent );

  void deltaDeltaG_N( bz::Array<double,4>& delta_delta_g_N, 
                      bz::Array<double,5>& delta_delta_n, bz::Array<double,4>& delta_delta_xi, 
                      bz::Array<double,2>& delta_xi, bz::Array<double,1>& der_shape_2, 
                      bz::Array<double,1>& average_normal, bz::Array<double,1>& mortar_tangent,
                      double g_N );

};

#endif
