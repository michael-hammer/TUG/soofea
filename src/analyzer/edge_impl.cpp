/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "edge_impl.h"

//------------------------------------------------------------------
void EdgeImpl::deltaJ( bz::Array<double,2>& array,
                       bz::Array<double,1>& der_shape, bz::Array<double,1>& tangent, 
                       double jacobian )
{
  bz::Array<double,1> buffer(der_shape.shape());
  buffer = der_shape * (-1.);
  array = ( tangent(bzt::i) * buffer(bzt::j) ) / jacobian;
}

//------------------------------------------------------------------
void EdgeImpl::deltaDeltaJ( bz::Array<double,4>& array,
                            bz::Array<double,1>& der_shape, bz::Array<double,1>& tangent, 
                            double jacobian )
{
  bz::Array<double,2> delta( (tangent.shape())(0) , (tangent.shape())(0) );
  Math::kronecker(delta);
  bz::Array<double,1> buffer(der_shape.shape());
  buffer = der_shape * (-1.);

  array = 1./jacobian * 
    ( buffer(bzt::j) * 
      ( delta(bzt::i,bzt::k) - (tangent(bzt::i) * tangent(bzt::k))/pow(jacobian,2.) ) *
      buffer(bzt::l) );
}
