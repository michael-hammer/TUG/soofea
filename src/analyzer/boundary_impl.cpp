/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "boundary_impl.h"
#include "math/array.h"

//------------------------------------------------------------------
AssembleBase<4>* BoundaryImpl::calcBoundaryStiffness(bool start_phase, BoundaryComponent& boundary_comp)
{
  unsigned node_amount = boundary_comp.getType().getShape().getNodeAmount();
  unsigned dimension = boundary_comp.Aggregate<Node>::front().getLagrangianPoint().getCoordSys()->getDimension();

  bz::TinyVector<int,4> size;
  size = dimension, node_amount, dimension, node_amount;

  bool boundary_load_defined = false;
  AssembleBase<4>* boundary_stiffness = new AssembleBase<4>(size); // allready initialized with 0.
  ///@fixme: What if we have other surface loads defined?
  if( boundary_comp.pressureSet() ) {
    Math::TNumIntFunctorArray<BoundaryImpl,BoundaryIntegrationPoint,BoundaryComponent,4> 
      functor_pressure( this, &BoundaryImpl::integratePressureStiffness );
    
    bz::Array<double,4>* array_pressure_stiffness = Math::NumInt::integrateArray( functor_pressure,
                                                                                  size,
                                                                                  boundary_comp);
    (*boundary_stiffness) += (*array_pressure_stiffness);
    delete array_pressure_stiffness;
    boundary_load_defined = true;
  }
  // if any other surface load -> do integration here!

  if( ! boundary_load_defined ) {
    delete boundary_stiffness;
    return( 0 );
  }

  boundary_stiffness -> addCoordID( CoordSys::X );
  boundary_stiffness -> addCoordID( CoordSys::Y );
  if( dimension > 2 )
    boundary_stiffness -> addCoordID( CoordSys::Z );

  for( Iterator<Node> node_iter = boundary_comp.Aggregate<Node>::begin();
       node_iter != boundary_comp.Aggregate<Node>::end() ; 
       ++node_iter ) {
    boundary_stiffness -> addDOF( node_iter -> getDOF( IDObject::DISPLACEMENT ) );
  }

  return( boundary_stiffness );
}

//------------------------------------------------------------------
AssembleBase<2>* BoundaryImpl::calcBoundaryLoadVector(bool start_phase, BoundaryComponent& boundary_comp)
{
  unsigned node_amount = boundary_comp.getType().getShape().getNodeAmount();
  unsigned dimension = boundary_comp.Aggregate<Node>::front().getLagrangianPoint().getCoordSys()->getDimension();

  bz::TinyVector<int,2> size;
  size = dimension, node_amount;

  bool boundary_load_defined = false;
  AssembleBase<2>* boundary_load = new AssembleBase<2>(size); // allready initialized with 0.
  ///@fixme: What if we have other surface loads defined?
  if( boundary_comp.pressureSet() ) {
    Math::TNumIntFunctorArray<BoundaryImpl,BoundaryIntegrationPoint,BoundaryComponent,2> 
      functor_pressure( this, &BoundaryImpl::integratePressure );
    
    bz::Array<double,2>* array_pressure = Math::NumInt::integrateArray( functor_pressure,
                                                                        size,
                                                                        boundary_comp);
    (*boundary_load) += (*array_pressure);
    delete array_pressure;
    boundary_load_defined = true;
  }
  // if any other surface load -> do integration here!

  if( ! boundary_load_defined ) {
    delete boundary_load;
    return( 0 );
  }

  boundary_load -> addCoordID( CoordSys::X );
  boundary_load -> addCoordID( CoordSys::Y );
  if( dimension > 2 )
    boundary_load -> addCoordID( CoordSys::Z );

  for( Iterator<Node> node_iter = boundary_comp.Aggregate<Node>::begin();
       node_iter != boundary_comp.Aggregate<Node>::end() ; 
       ++node_iter ) {
    boundary_load -> addDOF( node_iter -> getDOF( IDObject::DISPLACEMENT ) );
  }

  return( boundary_load );
}

//------------------------------------------------------------------
void BoundaryImpl::integratePressure(bz::Array<double,2>& array, BoundaryComponent& boundary_comp, BoundaryIntegrationPoint& int_point)
{
  Vector* normal_vector_nn = boundary_comp.getEulerianNormal( int_point.getMathIP().r(), int_point.getMathIP().s(), false );
  bz::Array<double,1>* shape = boundary_comp.getType().getShape().shapeTensor( int_point.getMathIP().r(), int_point.getMathIP().s() );

  array = (*normal_vector_nn)(bzt::i) * (*shape)(bzt::j);
  array *= (-1.) * boundary_comp.getPressure(); //@fixme: HEIGHT!!

  delete shape;
}

//------------------------------------------------------------------
void BoundaryImpl::integratePressureStiffness(bz::Array<double,4>& array, BoundaryComponent& boundary_comp, BoundaryIntegrationPoint& int_point)
{
  bz::Array<double,1>* shape = boundary_comp.getType().getShape().shapeTensor( int_point.getMathIP().r(), int_point.getMathIP().s() );
  bz::Array<double,2>* der_shape = boundary_comp.getType().getShape().derivativeShapeTensor( int_point.getMathIP().r(), int_point.getMathIP().s() );
  bz::Array<double,1> der_shape_2D = (*der_shape)(bz::Range::all(),0);
  bz::Array<double,2>* eps_2D = Math::epsilon_2D<double>();

  array = (*shape)( bzt::j) * (*eps_2D)(bzt::i,bzt::k) * der_shape_2D( bzt::l );
  array *= boundary_comp.getPressure(); //@fixme: HEIGHT!!

  delete shape;
  delete der_shape;
  delete eps_2D;
}
