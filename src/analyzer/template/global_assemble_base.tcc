/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2011 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

//------------------------------------------------------------------
template<int T>
GlobalAssembleBase<T>::GlobalAssembleBase(const bz::TinyVector<int,T>& size):
  bz::Array<double,T>(size,bz::ColumnMajorArray<T>())
{ 
  (*dynamic_cast<bz::Array<double,T>*>(this)) = 0.;
  setVector( &dof_value_vector_ );
}

//------------------------------------------------------------------
template<int T>
void GlobalAssembleBase<T>::setDOFValueVector(const std::vector<DOFValue*>& dof_value_vector)
{ 
  this->dof_value_vector_.erase( dof_value_vector_.begin(), 
                                 dof_value_vector_.end() );
  for( std::vector<DOFValue*>::const_iterator iter = dof_value_vector.begin() ;
       iter != dof_value_vector.end();
       ++iter )
    this->dof_value_vector_.push_back(*iter);
}
