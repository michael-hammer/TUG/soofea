/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

//------------------------------------------------------------------
template<int T>
AssembleBase<T>::AssembleBase(const bz::TinyVector<int,T>& size,
                              bool symmetric):
  bz::Array<double,T>(size),
  symmetric_(symmetric)
{ 
  (*dynamic_cast<bz::Array<double,T>*>(this)) = 0.;
  if( T >= 2 ) {
    coord_vector_vector_.push_back( new std::vector<CoordIndex*> );
    dof_vector_vector_.push_back( new std::vector<DOF*> );
  }
  if( T >= 4 ) {
    coord_vector_vector_.push_back( new std::vector<CoordIndex*> );
    dof_vector_vector_.push_back( new std::vector<DOF*> );
  }
  
  setVector( coord_vector_vector_[0] );
}

//------------------------------------------------------------------
template<int T>
AssembleBase<T>::~AssembleBase()
{
  if( T >= 2 ) {
    for( std::vector<CoordIndex*>::iterator iter = coord_vector_vector_[0] -> begin();
         iter != coord_vector_vector_[0] -> end() ;
         ++iter )
      delete *iter;
    delete coord_vector_vector_[0];
    delete dof_vector_vector_[0];
  }
  if( T >= 4 ) {
    for( std::vector<CoordIndex*>::iterator iter = coord_vector_vector_[1] -> begin();
         iter != coord_vector_vector_[1] -> end() ;
         ++iter )
      delete *iter;
    delete coord_vector_vector_[1];
    delete dof_vector_vector_[1];
  }
}

//------------------------------------------------------------------
template<int T>
void AssembleBase<T>::addDOF(DOF* dof, int direction)
{
  dof_vector_vector_[direction] -> push_back(dof);
}

//------------------------------------------------------------------
template<int T>
bool AssembleBase<T>::hasDOF(const DOF* dof, int direction)
{
  return( std::find( dof_vector_vector_[direction] -> begin(), 
                     dof_vector_vector_[direction] -> end(), dof ) != dof_vector_vector_[direction] -> end() );
}

//------------------------------------------------------------------
template<int T>
void AssembleBase<T>::addCoordID(CoordSys::CoordID coord_id, int direction)
{
  CoordIndex* coord_index = new int;
  *coord_index = coord_id;
  coord_vector_vector_[direction] -> push_back(coord_index);
}

//------------------------------------------------------------------
template<int T>
Iterator<DOF> AssembleBase<T>::begin( DOF* )
{
  return( Iterator<DOF>( this, dof_vector_vector_[0] -> at(0) , 0 ) );
}

//------------------------------------------------------------------
template<int T>
DOF* AssembleBase<T>::next( DOF* act, int& index)
{
  if( index < 0 )
    return 0;

  if( (unsigned)(++index) < dof_vector_vector_[0] -> size() )
    return( dof_vector_vector_[0] -> at(index) );
  else {
    index = -1;
    return( 0 );
  }
}

//------------------------------------------------------------------
template<int T>
DOF* AssembleBase<T>::at( DOF*, int index)
{
  if( index < 0 )
    return 0;

  if( (unsigned)(index) < dof_vector_vector_[0] -> size() )
    return( dof_vector_vector_[0] -> at(index) );
  else {
    index = -1;
    return( 0 );
  } 
}
