#ifndef assemble_base_h__
#define assemble_base_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>
#include <algorithm>

#include "math/array.h"
#include "model/dof_load/dof.h"
#include "geometry/coord_sys.h"

#include "base/template/index_aggregate.h"
#include "base/template/aggregate.h"

typedef int CoordIndex;

//------------------------------------------------------------------
template<int T>
class AssembleBase:
  public bz::Array<double,T>,
  public Aggregate<DOF>,
  public IndexAggregate<CoordIndex>
{
public:
  AssembleBase(const bz::TinyVector<int,T>& size, bool symmetric = true);
  ~AssembleBase();

  bool hasDOF(const DOF* dof, int direction = 0);
  void addDOF(DOF* dof, int direction = 0);
  void addCoordID(CoordSys::CoordID coord_id, int direction = 0);

  /**
     Translates an coord index (e.g. from a for loop) into the
     corresponding CoordSys::CoordID
   */
  CoordSys::CoordID getCoordIDAt(int index)
  { return( (CoordSys::CoordID)IndexAggregate<CoordIndex>::at(index) ); }

  inline bool isSymmetric()
  {return( true );}
protected:
  bool symmetric_;
  std::vector<std::vector<DOF*>*> dof_vector_vector_;
  std::vector<std::vector<CoordIndex*>*> coord_vector_vector_;

  using Aggregate<DOF>::begin;
  virtual Iterator<DOF> begin( DOF* );
  using Aggregate<DOF>::next;
  virtual DOF* next( DOF* act, int& index );
  using Aggregate<DOF>::get;
  virtual DOF* get( DOF* d, int index )
  {return( this->at(d,index) );}
  using Aggregate<DOF>::at;
  virtual DOF* at( DOF*, int index );
};

#include "assemble_base.tcc"

#endif
