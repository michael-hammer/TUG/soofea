/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "assemble_base.h"

//------------------------------------------------------------------
template<>
void AssembleBase<2>::assemble(AssembleBase<2>& to_assemble)
{
  for( int coord_counter = 0;
       coord_counter != to_assemble.IndexAggregate<CoordSys::CoordID*>::size() ;
       ++coord_counter ) {
    for( Iterator<DOF> dof_iter = to_assemble.Aggregate<DOF>::begin() ;
         dof_iter != to_assemble.Aggregate<DOF>::end() ;
         ++dof_iter ) {
      std::vector<DOF*>::iterator maj_dof_iter = 
        std::find( dof_vector_.begin(), dof_vector_.end(), &(*dof_iter) );
      std::vector<CoordSys::CoordID*>::iterator maj_coord_id_iter = 
        std::find( coord_id_vector_.begin(), coord_id_vector_.end(), to_assemble.IndexAggregate<CoordSys::CoordID>::at(coord_counter) );
      if( maj_dof_iter == dof_vector_.end() )
        throw EXC(CalcException) << "You try to assemble a DOF* which does not exist in major AssembleBase<2>" << Exception::endl;
      if( maj_coord_id_iter == coord_id_vector_.end() )
        throw EXC(CalcException) << "You try to assemble a CoordSys::CoordID which does not exist in major AssembleBase<2>" << Exception::endl;

      (*dynamic_cast<bz::Array<double,2>*>(this))((int)(maj_coord_id_iter-coord_id_vector_.begin()),(int(maj_dof_iter-dof_vector_.begin()))) += 
        to_assemble( coord_counter, dof_iter.getIndex() ); 
    }
  }
}

//------------------------------------------------------------------
template<>
void AssembleBase<4>::assemble(AssembleBase<4>& to_assemble)
{
  for( int coord_counter_row = 0;
       coord_counter_row != to_assemble.IndexAggregate<CoordSys::CoordID*>::size() ;
       ++coord_counter_row ) {
    for( Iterator<DOF> dof_iter_row = to_assemble.Aggregate<DOF>::begin() ;
         dof_iter_row != to_assemble.Aggregate<DOF>::end() ;
         ++dof_iter_row ) {
      std::vector<DOF*>::iterator maj_dof_iter_row = 
        std::find( dof_vector_.begin(), dof_vector_.end(), &(*dof_iter_row) );
      std::vector<CoordSys::CoordID*>::iterator maj_coord_id_iter_row = 
        std::find( coord_id_vector_.begin(), coord_id_vector_.end(), to_assemble.IndexAggregate<CoordSys::CoordID*>::at(coord_counter_row) );
      if( maj_dof_iter_row == dof_vector_.end() )
        throw EXC(CalcException) << "You try to assemble a DOF* which does not exist in major AssembleBase<2>" << Exception::endl;
      if( maj_coord_id_iter_row == coord_id_vector_.end() )
        throw EXC(CalcException) << "You try to assemble a CoordSys::CoordID which does not exist in major AssembleBase<2>" << Exception::endl;

      for( int coord_counter_col = 0;
           coord_counter_col != to_assemble.IndexAggregate<CoordSys::CoordID*>::size() ;
           ++coord_counter_col ) {
        for( Iterator<DOF> dof_iter_col = to_assemble.Aggregate<DOF>::begin() ;
             dof_iter_col != to_assemble.Aggregate<DOF>::end() ;
             ++dof_iter_col ) {
          std::vector<DOF*>::iterator maj_dof_iter_col = 
            std::find( dof_vector_.begin(), dof_vector_.end(), &(*dof_iter_col) );
          std::vector<CoordSys::CoordID*>::iterator maj_coord_id_iter_col = 
            std::find( coord_id_vector_.begin(), coord_id_vector_.end(), to_assemble.IndexAggregate<CoordSys::CoordID*>::at(coord_counter_col) );
          if( maj_dof_iter_col == dof_vector_.end() )
            throw EXC(CalcException) << "You try to assemble a DOF* which does not exist in major AssembleBase<2>" << Exception::endl;
          if( maj_coord_id_iter_col == coord_id_vector_.end() )
            throw EXC(CalcException) << "You try to assemble a CoordSys::CoordID which does not exist in major AssembleBase<2>" << Exception::endl;

          (*dynamic_cast<bz::Array<double,4>*>(this))( (int)(maj_coord_id_iter_row-coord_id_vector_.begin()),
                                                       (int(maj_dof_iter_row-dof_vector_.begin())),
                                                       (int)(maj_coord_id_iter_col-coord_id_vector_.begin()),
                                                       (int(maj_dof_iter_col-dof_vector_.begin()))                ) += 
            to_assemble( coord_counter_row, dof_iter_row.getIndex() , coord_counter_col, dof_iter_col.getIndex() ); 
        }
      }
    }
  }
}  
