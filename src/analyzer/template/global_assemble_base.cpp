/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2011 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "global_assemble_base.h"

//------------------------------------------------------------------
/**
   Assembles a 1D load vector (returned e.g. from *_impl_mortar_*)
   into the global load vector. The 2D to 1D conversion is here allready done.
 */
template<>
void GlobalAssembleBase<1>::assemble(GlobalAssembleBase<1>& to_assemble, char assemble_config)
{
  int global_pos = 0;
  for( int dof_value_counter = 0 ;
       dof_value_counter != to_assemble.IndexAggregate<DOFValue>::size() ;
       ++dof_value_counter ) {
    if( assemble_config & POS_FROM_DOF_VALUE ) {
      global_pos = to_assemble.IndexAggregate<DOFValue>::at(dof_value_counter).getPositionInSLE();
    } else{
      std::vector<DOFValue*>::iterator iter = 
        std::find( dof_value_vector_.begin(), dof_value_vector_.end(), &(to_assemble.IndexAggregate<DOFValue>::at(dof_value_counter)) );
      if( iter != dof_value_vector_.end() )
        global_pos = iter - dof_value_vector_.begin();
      else
        global_pos = -1;
    }
    if( global_pos >= 0 ) { 
#ifdef SOOFEA_WITH_TBB
      Mutex::scoped_lock lock(mutex_);
#endif
      (*this)(global_pos) += to_assemble(dof_value_counter);
    }
  }
}

//------------------------------------------------------------------
/**
   Assembles the 2D element load from a classical structural analysis
   into the global load vector
 */
template<>template<>
void GlobalAssembleBase<1>::assemble(AssembleBase<2>& to_assemble, char assemble_config)
{
  int global_pos = 0;
  DOFValue* dof_value = 0;
  for( int coord_counter = 0;
       coord_counter != to_assemble.IndexAggregate<CoordIndex>::size() ;
       ++coord_counter ) {
    for( Iterator<DOF> dof_iter = to_assemble.Aggregate<DOF>::begin() ;
         dof_iter != to_assemble.Aggregate<DOF>::end() ;
         ++dof_iter ) {
      dof_value = dof_iter -> getValue( to_assemble.getCoordIDAt( coord_counter ) );
      if( assemble_config & POS_FROM_DOF_VALUE ) {
        global_pos = dof_value -> getPositionInSLE();
      } else {
        std::vector<DOFValue*>::iterator iter = std::find( dof_value_vector_.begin(), dof_value_vector_.end(), dof_value );
        if( iter != dof_value_vector_.end() )
          global_pos = iter - dof_value_vector_.begin();
        else
          global_pos = -1;
      }
      if( global_pos >= 0 ) {
#ifdef SOOFEA_WITH_TBB
        Mutex::scoped_lock lock(mutex_);
#endif
        (*this)(global_pos) += to_assemble(coord_counter,dof_iter.getIndex());
      }
    }
  }
}

//------------------------------------------------------------------
/**
   Assembles a 2D stiffness matrix (returned e.g. from
   *_impl_mortar_*) into the global stiffness matrix. The 4D to 2D
   conversion is here allready done.
 */
template<>
void GlobalAssembleBase<2>::assemble(GlobalAssembleBase<2>& to_assemble, char assemble_config)
{
  int global_row = 0, global_col = 0;
  for( int dof_value_counter_row = 0 ;
       dof_value_counter_row != to_assemble.IndexAggregate<DOFValue>::size() ;
       ++dof_value_counter_row ) {
    if( assemble_config & POS_FROM_DOF_VALUE ) {
      global_row = to_assemble.IndexAggregate<DOFValue>::at( dof_value_counter_row ).getPositionInSLE();
    } else {
      std::vector<DOFValue*>::iterator iter_row = 
        std::find( dof_value_vector_.begin(), dof_value_vector_.end(), &(to_assemble.IndexAggregate<DOFValue>::at( dof_value_counter_row )) );
      if( iter_row != dof_value_vector_.end() )
        global_row = iter_row - dof_value_vector_.begin();
      else
        global_row = -1;
    }
    if( global_row >= 0 ) {
      for( int dof_value_counter_col = 0 ;
           dof_value_counter_col != to_assemble.IndexAggregate<DOFValue>::size() ;
           ++dof_value_counter_col ) {       
        if( assemble_config & POS_FROM_DOF_VALUE ) {
          global_col = to_assemble.IndexAggregate<DOFValue>::at( dof_value_counter_col ).getPositionInSLE();          
        } else {
          std::vector<DOFValue*>::iterator iter_col = 
            std::find( dof_value_vector_.begin(), dof_value_vector_.end(), &(to_assemble.IndexAggregate<DOFValue>::at( dof_value_counter_col )) );
          if( iter_col != dof_value_vector_.end() )
            global_col = iter_col - dof_value_vector_.begin();
          else
            global_col = -1;
        }
        if( global_col >= 0 ) {
#ifdef SOOFEA_WITH_TBB
          Mutex::scoped_lock lock(mutex_);
#endif
          (*this)(global_row,global_col) += to_assemble(dof_value_counter_row,dof_value_counter_col);
        }
      }
    }
  }
}

//------------------------------------------------------------------
/**
   Assembles the 4D element stiffness from a classical structural
   analysis into the global stiffness
 */
template<>template<>
void GlobalAssembleBase<2>::assemble(AssembleBase<4>& to_assemble, char assemble_config)
{
  int global_row = 0, global_col = 0;
  for( int coord_counter_row = 0;
       coord_counter_row != to_assemble.IndexAggregate<CoordIndex>::size() ;
       ++coord_counter_row ) {
    for( Iterator<DOF> dof_iter_row = to_assemble.Aggregate<DOF>::begin() ;
         dof_iter_row != to_assemble.Aggregate<DOF>::end() ;
         ++dof_iter_row ) {
      if( assemble_config & POS_FROM_DOF_VALUE ) {
        global_row = dof_iter_row -> getValue(to_assemble.getCoordIDAt( coord_counter_row )) -> getPositionInSLE();
      } else {
        std::vector<DOFValue*>::iterator iter_row = 
          std::find( dof_value_vector_.begin(), dof_value_vector_.end(), 
                     dof_iter_row -> getValue( to_assemble.getCoordIDAt( coord_counter_row ) ) );
        if( iter_row != dof_value_vector_.end() )
          global_row = iter_row - dof_value_vector_.begin();
        else
          global_row = -1;
      }
      if( global_row >= 0 ) {
        for( int coord_counter_col = 0;
             coord_counter_col != to_assemble.IndexAggregate<CoordIndex>::size() ;
             ++coord_counter_col ) {
          for( Iterator<DOF> dof_iter_col = to_assemble.Aggregate<DOF>::begin() ;
               dof_iter_col != to_assemble.Aggregate<DOF>::end() ;
               ++dof_iter_col ) {
            if( assemble_config & POS_FROM_DOF_VALUE ) {
              global_col = dof_iter_col -> getValue(to_assemble.getCoordIDAt( coord_counter_col )) -> getPositionInSLE();
            } else {
              std::vector<DOFValue*>::iterator iter_col = 
                std::find( dof_value_vector_.begin(), dof_value_vector_.end(), 
                           dof_iter_col -> getValue( to_assemble.getCoordIDAt( coord_counter_col ) ) );
              if( iter_col != dof_value_vector_.end() )
                global_col = iter_col - dof_value_vector_.begin();
              else
                global_col = -1;
            }
            if( global_col >= 0 ) {
#ifdef SOOFEA_WITH_TBB
              Mutex::scoped_lock lock(mutex_);
#endif
              (*this)(global_row,global_col) += 
                to_assemble(coord_counter_row,dof_iter_row.getIndex(),coord_counter_col,dof_iter_col.getIndex());
            }
          }
        }
      }
    }
  }
}

//------------------------------------------------------------------
template<>template<>
void GlobalAssembleBase<2>::assembleDual(AssembleBase<2>& to_assemble, DOFValue* dof_value, char assemble_config)
{
  int lambda_index, disp_index;
  for( int coord_counter = 0;
       coord_counter != to_assemble.IndexAggregate<CoordIndex>::size() ;
       ++coord_counter ) {
    for( Iterator<DOF> dof_iter = to_assemble.Aggregate<DOF>::begin() ;
         dof_iter != to_assemble.Aggregate<DOF>::end() ;
         ++dof_iter ) {
      if( assemble_config & POS_FROM_DOF_VALUE ) {
        disp_index = dof_iter -> getValue(to_assemble.getCoordIDAt( coord_counter )) -> getPositionInSLE();
        lambda_index = dof_iter -> getValue(to_assemble.getCoordIDAt( coord_counter )) -> getPositionInSLE();
      } else {
        std::vector<DOFValue*>::iterator disp_iter = 
          std::find( dof_value_vector_.begin(), dof_value_vector_.end(),
                     dof_iter -> getValue( to_assemble.getCoordIDAt( coord_counter ) ) );
        disp_index = disp_iter - dof_value_vector_.begin();
        std::vector<DOFValue*>::iterator lambda_iter = 
          std::find( dof_value_vector_.begin(), dof_value_vector_.end(), 
                     dof_value );
        lambda_index = lambda_iter - dof_value_vector_.begin();
      }   
      {
#ifdef SOOFEA_WITH_TBB
        Mutex::scoped_lock lock(mutex_);
#endif
        (*this)(disp_index,lambda_index) += to_assemble(coord_counter,dof_iter.getIndex());
        if( assemble_config & ASSEMBLE_SYMMETRIC )
          (*this)(lambda_index,disp_index) += to_assemble(coord_counter,dof_iter.getIndex());
      }
    }
  }
}

//------------------------------------------------------------------
template<>
void GlobalAssembleBase<1>::assembleDual(double to_assemble, DOFValue* dof_value, char assemble_config)
{
  double lambda_index = 0;
  if( assemble_config & POS_FROM_DOF_VALUE ) {
    lambda_index = dof_value -> getPositionInSLE();
  } else {
    std::vector<DOFValue*>::iterator lambda_iter = 
      std::find( dof_value_vector_.begin(), dof_value_vector_.end(), 
                 dof_value );
    lambda_index = lambda_iter - dof_value_vector_.begin();
  }
  {
#ifdef SOOFEA_WITH_TBB
    Mutex::scoped_lock lock(mutex_);
#endif
    (*this)(lambda_index) += to_assemble;
  }
}
