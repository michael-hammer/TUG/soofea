#ifndef global_assemble_base_h__
#define global_assemble_base_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>

#include "math/array.h"
#include "assemble_base.h"
#include "base/template/index_aggregate.h"
#include "soofea.h"
#ifdef SOOFEA_WITH_TBB
// #include "tbb/mutex.h"
// #include "tbb/spin_mutex.h"
#include "tbb/queuing_mutex.h"
#else
namespace tbb {
  class queuing_mutex;
}
#endif

class DOFValue;

//------------------------------------------------------------------
template<int T>
class GlobalAssembleBase:
  public bz::Array<double,T>,
  public IndexAggregate<DOFValue>
{
public:
  typedef enum Config_ {
    ASSEMBLE_SYMMETRIC = 0x01,
    POS_FROM_DOF_VALUE = 0x02
  } Config;

#ifdef SOOFEA_WITH_TBB
  typedef tbb::queuing_mutex Mutex;
#else
  typedef void* Mutex;
#endif

  GlobalAssembleBase(const bz::TinyVector<int,T>& size);

  inline std::vector<DOFValue*>& getValueVector() {return(dof_value_vector_);}

  inline void addDOFValue( DOFValue* dof_value )
  { dof_value_vector_.push_back( dof_value ); }

  inline bool hasDOFValue( DOFValue* dof_value )
  { return( std::find( dof_value_vector_.begin(), dof_value_vector_.end(), dof_value ) != dof_value_vector_.end() ); }

  template<int N>
    void assemble(AssembleBase<N>& to_assemble, char assemble_config = 0x00);

  void assemble(GlobalAssembleBase<T>& to_assemble, char assemble_config = 0x00);

  template<int N>
  void assembleDual(AssembleBase<N>& to_assemble, DOFValue* dof_value, char assemble_config = 0x00);

  void assembleDual(double to_assemble, DOFValue* dof_value, char assemble_config = 0x00);

  Mutex& getMutex() {return(mutex_);}
protected:
  std::vector<DOFValue*> dof_value_vector_;
  Mutex mutex_;

  void setDOFValueVector(const std::vector<DOFValue*>& dof_value_vector);
};

#include "global_assemble_base.tcc"

#endif
