#ifndef element_iterator_h___
#define element_iterator_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <boost/iterator/iterator_facade.hpp>
#include "../../model/element.h"

//------------------------------------------------------------------
/**
   Provides an Iterator Class to iterate over Nodes, Edges of an Element
 */
template<class T>
class ElementIterator : 
  public boost::iterator_facade<ElementIterator<T>, T, boost::forward_traversal_tag>
{
public:
  /*
   */
  inline ElementIterator(Element& element) : 
    element_(element),
    pointer_(0) 
  {}

  /*
   */
  inline explicit ElementIterator(Element& element,T* p) : 
    element_(element),
    pointer_(p)
  {}

private:
  friend class boost::iterator_core_access;

  /*
   */
  void increment();

  /*
   */
  bool equal(ElementIterator const& other_iter) const;

  /*
   */
  T& dereference() const;

  Element &element_;
  T* pointer_;
};

#include "element_iterator.tcc"

#endif // element_iterator_h___
