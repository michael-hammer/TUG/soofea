/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "la_mortar_edge_impl.h"

int LAMortarEdgeImpl::DISP_DOF_AMOUNT = 2;

//------------------------------------------------------------------
void LAMortarEdgeImpl::calcKinematics( MortarElement& mortar_element , bool research_best_edge , bool new_time_step )
{
  double xi = 0., g_N = 0., g_T = 0.;
  std::vector< boost::tuple<double,double,unsigned> > geometric_props;
  Vector* average_tangent = 0;

  LOG(Logger::INFO) << "Calc Kinematics for edge : " 
                    << mortar_element.getNonMortarEdge()->getNumber() << Logger::endl;

  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( !ip_iter -> mortarEdgeSet() ) { 
      LOG(Logger::INFO) << "For IP no mortar edge was set!" << Logger::endl;
      break;
    }

    average_tangent = interpolateAverageTangent( mortar_element , *ip_iter );

    if( new_time_step ) {
      ip_iter -> archive();
    }

    if( research_best_edge ) {
      for( unsigned edge_iter = 0 ;
           edge_iter != ip_iter->amountOfMortarEdges() ;
           ++edge_iter ) {
        // Here we do the projection!
        pointProjectionA( *ip_iter, ip_iter->getMortarEdge( edge_iter ) , *average_tangent , g_N , xi );
        geometric_props.push_back( boost::tuple<double,double,unsigned>( g_N, xi, edge_iter ) );
      }
      boost::tuple<double,double,unsigned> best_projection = *std::min_element( geometric_props.begin(), geometric_props.end() , RankProjection() );
      if( (fabs( best_projection.get<XI>() ) > 1.) ) {
        //  std::cout << "On edge " << mortar_element.getNonMortarEdge()->getNumber() << " ip " << ip_iter->getMathIP().r() << " is outside" << std::endl;
        ip_iter -> setActive( false );
      } else {
        ip_iter -> setActive( true );
        if( ip_iter -> hasActiveMortarEdge() ) {
          g_T = slipA( ip_iter->getMortarEdge(best_projection.get<EDGE_INDEX>()) , best_projection.get<XI>() ,
                       ip_iter->getActiveMortarEdge(true) , ip_iter -> xi(true), *average_tangent );
          ip_iter -> setGapData( best_projection.get<DIST>(),
                                 g_T,
                                 best_projection.get<XI>(),
                                 best_projection.get<EDGE_INDEX>());
        } else { // This must be the very first iteration on the very first time step
          ip_iter -> setGapData( best_projection.get<DIST>(), 
                                 0.,
                                 best_projection.get<XI>(),
                                 best_projection.get<EDGE_INDEX>());
          ip_iter -> archive();        
        }
      }
    } else { // Only for new time stamps the best projection edge is "researched" -> else we use the old one again.
      if( ip_iter -> getActive() ) {
        pointProjectionA( *ip_iter, ip_iter->getActiveMortarEdge() , *average_tangent , g_N , xi );
        g_T = slipA( ip_iter->getActiveMortarEdge() , xi ,
                     ip_iter->getActiveMortarEdge(true) , ip_iter -> xi(true), *average_tangent );
        ip_iter -> setGapData( g_N, g_T, xi );
      }
    }

    if( ip_iter -> getActive() ) {
      LOG(Logger::INFO) << "r = " << ip_iter -> getMathIP().r()
                        << " Projection gave g_N = " << ip_iter -> getGN() 
                        << " g_T = " << ip_iter -> getGT() 
                        << "| xi = " << ip_iter -> xi() 
                        << " [ onto mortar edge = " << ip_iter->getActiveMortarEdge().getNumber()
                        << " (" << ip_iter->getActiveMortarEdge().Aggregate<Node>::begin()->getNumber()
                        << "/" << ip_iter->getActiveMortarEdge().getLastNode()->getNumber() 
                        << ") last was " << ip_iter->getActiveMortarEdge(true).getNumber()
                        << " with xi = " << ip_iter -> xi(true)
                        << Logger::endl;
    }

    geometric_props.clear();
  }  
}

//------------------------------------------------------------------
double LAMortarEdgeImpl::slipA( Edge& new_edge , double new_xi,
                              Edge& old_edge , double old_xi, Vector& average_tangent )
{
  Point* new_point = new_edge.getEulerianPoint( new_xi );
  Point* old_point = old_edge.getEulerianPoint( old_xi );

  bz::Array<double,1> temp( (*new_point) - (*old_point) );
  double g_T = (-1.) * bz::sum( temp * average_tangent ) / Math::norm( average_tangent , 2 );

  delete new_point;
  delete old_point;
  
  return( g_T );
}

//------------------------------------------------------------------
bool LAMortarEdgeImpl::contactSearch( MortarElement& mortar_element , Model& model )
{
  return( contactSearchG( mortar_element, model ) );
}

//------------------------------------------------------------------
void LAMortarEdgeImpl::calcAverageTangentOnNode( MortarElement& mortar_element , 
                                                  MortarElement::PositionID position_id )
{
  /** @fixme This is only working for 2D and linear shape! */
  Node *node = 0;
  bz::Array<double,1> *averaging_tensor = 0;
  switch( position_id ) {
  case MortarElement::MAIN:
    throw EXC(CalcException) << "To calc the average normal we have to know if we need it for first or last node!" << Exception::endl;
    break;
  case MortarElement::PRECEDING:
    averaging_tensor = mortar_element.getAveragingTensor(-1.);
    node = &(mortar_element.getNonMortarEdge()->Aggregate<Node>::front());
    break;
  case MortarElement::FOLLOWING:
    averaging_tensor = mortar_element.getAveragingTensor(+1.);
    node = mortar_element.getNonMortarEdge()->getLastNode();
    break;
  }
  bz::Array<double,2> *nodal_tensor = mortar_element.getEulerianPointTensorLA();
  bz::Array<double,1> tangent( bz::sum( (*averaging_tensor)(bzt::j) * (*nodal_tensor)(bzt::i,bzt::j) , bzt::j ) );
  delete nodal_tensor;
  delete averaging_tensor;
  
  Vector *average_tangent = 0;
  if( node -> hasProperty( "average_tangent" ) )
    average_tangent = node -> getProperty<Vector*>( "average_tangent" );
  if( average_tangent )
    delete average_tangent;
  average_tangent = new Vector( dynamic_cast<PointAggregate&>(mortar_element.getNonMortarEdge( MortarElement::MAIN )->Aggregate<Node>::front())
                                .getEulerianPoint().getCoordSys(),
                                tangent );
  node -> addProperty( "average_tangent" , boost::any(average_tangent) );
}

//------------------------------------------------------------------
void LAMortarEdgeImpl::pointProjectionA( BoundaryIntegrationPoint& int_point, 
                                       Edge& edge, Vector& average_tangent, 
                                       double& distance, double& xi)
{
  double residuum = 1.e15, d_xi = 0.;
  bz::Array<double,1> GP( (int_point.getEulerianPoint())(bz::Range(0,1)).copy() );
  bz::Array<double,2>* mortar_node_tensor = edge.getEulerianPointTensor();
  Vector* mortar_tangent = edge.getEulerianTangent( xi );
  bz::Array<double,1>* shape = 0;
  bz::Array<double,1> bar_x(2);
  bz::Array<double,1> distance_vector(2);
  bar_x = 0.;
  xi = 0.;

  // Newton iteration for projection
  for( unsigned count = 0 ; ( count != 5 && fabs(residuum) > 1e-14 ) ; ++count ) {
    shape = edge.getType().getShape().shapeTensor( xi );
    bar_x = bz::sum( (*shape)(bzt::j) * (*mortar_node_tensor)(bzt::j,bzt::i) , bzt::j );
    distance_vector = bz::sum( (*shape)(bzt::j) * (*mortar_node_tensor)(bzt::i,bzt::j) , bzt::j ) - GP;
    d_xi = bz::sum( (*mortar_tangent)(bzt::i) * average_tangent(bzt::i) );
    if( fabs(d_xi) < 1.e-14 ) { // tangents are normal - no intersection possible
      LOG(Logger::INFO) << "!!! Mortar Edge : " << edge.getNumber()
                        << " Tangents are normal - no intersection!" << Logger::endl;
      distance = 1.e15;
      xi = 1.e15;
      return;
    }
    residuum = bz::sum( distance_vector(bzt::i) * average_tangent(bzt::i) );
    xi -= residuum / d_xi;
    delete shape;
  }

  // sign correct distance calculation
  shape = edge.getType().getShape().shapeTensor( xi );
  CartesianCoordSys coord_sys_3D(3);
  Vector e3( &coord_sys_3D , 0., 0., 1. );
  bar_x = bz::sum( (*shape)(bzt::j) * (*mortar_node_tensor)(bzt::i,bzt::j) , bzt::j );
  distance_vector = bar_x - GP;
  Vector* normal = Geometry::calcNormal( average_tangent, e3 ); 
  distance = bz::sum( distance_vector * (*normal) );

  delete shape;
  delete normal;
  delete mortar_node_tensor;
  delete mortar_tangent;
}

//------------------------------------------------------------------
Vector* LAMortarEdgeImpl::interpolateAverageTangent( MortarElement& mortar_element , BoundaryIntegrationPoint& int_point )
{
  bz::Array<double,1> *averaging_tensor = mortar_element.getAveragingTensor(int_point.getMathIP().r());;
  bz::Array<double,2> *nodal_tensor = mortar_element.getEulerianPointTensorLA();

  bz::Array<double,1> tangent( bz::sum( (*averaging_tensor)(bzt::j) * (*nodal_tensor)(bzt::i,bzt::j) , bzt::j ) );
  Vector *average_tangent = 
    new Vector( dynamic_cast<PointAggregate&>(mortar_element.getNonMortarEdge( MortarElement::MAIN )->Aggregate<Node>::front())
                .getEulerianPoint().getCoordSys(),
                tangent );
  int_point.setAverageTangent( average_tangent );

  delete averaging_tensor;
  delete nodal_tensor;
  return( average_tangent );
}

//------------------------------------------------------------------
void LAMortarEdgeImpl::resetNodalCondition( MortarElement& mortar_element )
{
  resetNodalConditionL( mortar_element );
}

//------------------------------------------------------------------
void LAMortarEdgeImpl::calcNodalCondition( MortarElement& mortar_element )
{
  calcNodalConditionL( mortar_element );
}

//------------------------------------------------------------------
template<int T>
GlobalAssembleBase<T>* LAMortarEdgeImpl::initiateGlobals(MortarElement& mortar_element)
{
  int node_amount = mortar_element.getNonMortarNodeAmountLA() +
    mortar_element.getMortarNodeAmount();

  int lagrange_dof_amount = 0;
  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter ) {
    if( non_mortar_node_iter -> getProperty<bool>("mortar_active") ) {
      ++lagrange_dof_amount;
      if( non_mortar_node_iter -> hasProperty("slide") ) {
        if( non_mortar_node_iter -> getProperty<int>("slide") == 0 )
          ++lagrange_dof_amount;
      }
    }
  }

  bz::TinyVector<int,T> size;
  if( T == 2 ) {
    size = lagrange_dof_amount + DISP_DOF_AMOUNT*node_amount,
      lagrange_dof_amount + DISP_DOF_AMOUNT*node_amount;
  } else {
    size = lagrange_dof_amount + DISP_DOF_AMOUNT*node_amount; 
  }

  GlobalAssembleBase<T>* global = new GlobalAssembleBase<T>(size);

  for( Iterator<Node> non_mortar_node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       non_mortar_node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++non_mortar_node_iter ) {
    if( non_mortar_node_iter -> getProperty<bool>("mortar_active") ) {
      global -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::LAGRANGE ) -> getValue(CoordSys::NORMAL) );
      if( non_mortar_node_iter -> hasProperty("slide") ) {
        if( non_mortar_node_iter -> getProperty<int>("slide") == 0 ) {
          global -> addDOFValue( non_mortar_node_iter -> getDOF( IDObject::LAGRANGE ) -> getValue(CoordSys::TANGENT_R) );
        }
      }
    }
  }

  Node* node = 0; 
  if( mortar_element.hasNonMortarEdge(MortarElement::PRECEDING) ) {
    node = &(mortar_element.getNonMortarEdge(MortarElement::PRECEDING)->Aggregate<Node>::front());
    global -> addDOFValue( node -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) );
    global -> addDOFValue( node -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) );
  }

  for( Iterator<Node> node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++node_iter ) {
    global -> addDOFValue( node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) );
    global -> addDOFValue( node_iter -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) );
  }

  if( mortar_element.hasNonMortarEdge(MortarElement::FOLLOWING) ) {
    node = mortar_element.getNonMortarEdge(MortarElement::FOLLOWING)->getLastNode();
    global -> addDOFValue( node -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) );
    global -> addDOFValue( node -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) );
  }

  // Here we have to do a unification so that we have each DOFValue only once!
  std::vector<Node*>* node_vector = mortar_element.getUniqueMortarNodes();
  for( std::vector<Node*>::iterator mortar_node_iter = node_vector->begin();
       mortar_node_iter != node_vector->end() ;
       ++mortar_node_iter ) {
    global -> addDOFValue( (*mortar_node_iter) -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) );
    global -> addDOFValue( (*mortar_node_iter) -> getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) );
  }
  delete node_vector;

  return( global );
}

//------------------------------------------------------------------
template<int T>
AssembleBase<T>* LAMortarEdgeImpl::initiateLocals(MortarElement& mortar_element, BoundaryIntegrationPoint& int_point)
{
  int node_amount = mortar_element.getNonMortarNodeAmountLA() + 
    int_point.getActiveMortarEdge().getType().getShape().getNodeAmount();

  bz::TinyVector<int,T> size;
  if( T == 4 ) {
    size = DISP_DOF_AMOUNT , node_amount,
      DISP_DOF_AMOUNT , node_amount;
  } else {
    size = DISP_DOF_AMOUNT , node_amount;    
  }
  AssembleBase<T>* local = new AssembleBase<T>(size);
  local -> addCoordID( CoordSys::X );
  local -> addCoordID( CoordSys::Y );

  Node* node = 0; 
  if( mortar_element.hasNonMortarEdge(MortarElement::PRECEDING) ) {
    node = &(mortar_element.getNonMortarEdge(MortarElement::PRECEDING)->Aggregate<Node>::front());
    local -> addDOF( node -> getDOF( IDObject::DISPLACEMENT ) );
  }
  for( Iterator<Node> node_iter = mortar_element.getNonMortarEdge()->Aggregate<Node>::begin();
       node_iter != mortar_element.getNonMortarEdge()->Aggregate<Node>::end() ; 
       ++node_iter ) {
    local -> addDOF( node_iter -> getDOF( IDObject::DISPLACEMENT ) );
  }
  if( mortar_element.hasNonMortarEdge(MortarElement::FOLLOWING) ) {
    node = mortar_element.getNonMortarEdge(MortarElement::FOLLOWING)->getLastNode();
    local -> addDOF( node -> getDOF( IDObject::DISPLACEMENT ) );
  }
  for( Iterator<Node> mortar_node_iter = int_point.getActiveMortarEdge().Aggregate<Node>::begin();
       mortar_node_iter != int_point.getActiveMortarEdge().Aggregate<Node>::end() ; 
       ++mortar_node_iter )
    local -> addDOF( mortar_node_iter -> getDOF( IDObject::DISPLACEMENT ) );

  return( local );
}

//------------------------------------------------------------------
GlobalAssembleBase<2>* LAMortarEdgeImpl::calcMortarElementStiffness(MortarElement& mortar_element)
{
  int active_non_mortar_node_amount = mortar_element.getActiveNonMortarNodeAmount();
  if( active_non_mortar_node_amount == 0 )
    return( 0 );

  LOG(Logger::INFO) << "Mortar Stiffness (lagrange) of non-mortar (primary) Edge: " 
                    << mortar_element.getNonMortarEdge()->getNumber()
                    << " and active nodes = " << active_non_mortar_node_amount << Logger::endl;

  GlobalAssembleBase<2>* element_stiffness = initiateGlobals<2>( mortar_element );

#ifdef SOOFEA_WITH_TBB
  tbb::parallel_for( tbb::blocked_range<size_t>(0,mortar_element.getNonMortarEdge()->getIntegrationPointAmount()), 
                     LAMortarEdgeImpl::IntPointStiffness(*this,*element_stiffness,mortar_element) );
#else
  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( ! ip_iter -> getActive() ) {
      continue;
    }
    assemblePerIPStiffness( mortar_element,
                            *ip_iter,
                            *element_stiffness );
  }
#endif

  return( element_stiffness );
}

#ifdef SOOFEA_WITH_TBB
//------------------------------------------------------------------
void LAMortarEdgeImpl::IntPointStiffness::operator()( const tbb::blocked_range<size_t>& range ) const
{ 
  for( size_t iter = range.begin();
       iter != range.end();
       ++iter ) {
    if( ! mortar_element_.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::at( iter ).getActive() )
      continue;

    mortar_impl_.assemblePerIPStiffness( mortar_element_,
                                         mortar_element_.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::at( iter ),
                                         element_stiffness_ );
  }
}
#endif

//------------------------------------------------------------------
void LAMortarEdgeImpl::assemblePerIPStiffness( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                                 GlobalAssembleBase<2>& element_stiffness )
{
  int non_mortar_node_amount = mortar_element.getNonMortarNodeAmountLA();
  CartesianCoordSys coord_sys_3D(3);
  Vector e3( &coord_sys_3D,0,0,1  );
  int local_mortar_node_amount = int_point.getActiveMortarEdge().getType().getShape().getNodeAmount();
  bz::TinyVector<int,4> local_size;
  local_size = DISP_DOF_AMOUNT , non_mortar_node_amount+local_mortar_node_amount , 
    DISP_DOF_AMOUNT , non_mortar_node_amount+local_mortar_node_amount;
  
  Vector& average_tangent = int_point.getAverageTangent();
  Vector* average_normal = Geometry::calcNormal( average_tangent, e3 , true );
  double alpha = Math::norm( average_tangent , 2 );
  bz::Array<double,1>* averaging_tensor = mortar_element.getAveragingTensor( int_point.getMathIP().r() );
  bz::Array<double,1>* shape = mortar_element.getShapeTensorA( int_point );
  bz::Array<double,1>* der_shape_1 = mortar_element.getDerivativeShapeTensorA( int_point, Contact::NON_MORTAR );
  bz::Array<double,1>* der_shape_2 = mortar_element.getDerivativeShapeTensorA( int_point, Contact::MORTAR );
  double g_N = int_point.getGN();   
  double g_T = int_point.getGT();
  Vector* mortar_tangent = int_point.getActiveMortarEdge().getEulerianTangent( int_point.xi() );
  Vector* non_mortar_tangent = mortar_element.getNonMortarEdge()->getEulerianTangent( int_point.getMathIP().r() );
  double jacobian_det = Math::norm( *non_mortar_tangent , 2 );
  
  bz::Array<double,2> delta_J(local_size(0),local_size(1)); delta_J = 0.;
  deltaJ( delta_J, *der_shape_1, *non_mortar_tangent, jacobian_det );
  
  bz::Array<double,4> delta_delta_J(local_size); delta_delta_J = 0.;
  deltaDeltaJ( delta_delta_J, *der_shape_1, *non_mortar_tangent, jacobian_det );
  
  bz::Array<double,3> delta_n(local_size(0),local_size(1),local_size(0)); delta_n = 0.;
  deltaN( delta_n, *averaging_tensor, average_tangent, alpha );
  
  bz::Array<double,2> delta_xi(local_size(0),local_size(1)); delta_xi = 0.;
  deltaXi( delta_xi, delta_n, average_tangent, *shape, *mortar_tangent, g_N );
  
  bz::Array<double,2> delta_g_N(local_size(0),local_size(1)); delta_g_N = 0.;
  deltaG_N( delta_g_N, delta_xi, *average_normal, *shape , *mortar_tangent );
  
  bz::Array<double,2> delta_g_T(local_size(0),local_size(1)); delta_g_T = 0.;
  deltaG_T( delta_g_T, delta_xi, *mortar_tangent, average_tangent );
  
  bz::Array<double,5> delta_delta_n(local_size(0),local_size(1),local_size(2),local_size(3),local_size(0)); delta_delta_n = 0.;
  deltaDeltaN( delta_delta_n, *averaging_tensor, average_tangent, alpha );
  
  bz::Array<double,4> delta_delta_xi(local_size); delta_delta_xi = 0.;
  deltaDeltaXi( delta_delta_xi, delta_xi, delta_n, delta_g_N, delta_delta_n, *der_shape_2, average_tangent, *mortar_tangent , g_N );
  
  bz::Array<double,4> delta_delta_g_N(local_size); delta_delta_g_N = 0.;
  deltaDeltaG_N( delta_delta_g_N, delta_delta_n, delta_delta_xi, delta_xi, *der_shape_2, *average_normal, *mortar_tangent, g_N );
  
  bz::Array<double,4> delta_delta_g_T(local_size); delta_delta_g_T = 0.;
  deltaDeltaG_T( delta_delta_g_T, delta_delta_xi, delta_xi, *averaging_tensor, *der_shape_2, *mortar_tangent, average_tangent );

  AssembleBase<4>* ip_stiffness_4;
  AssembleBase<2>* ip_stiffness_2;
  for( Iterator<Node> node_iter = mortar_element.getNonMortarEdge(MortarElement::MAIN)->Aggregate<Node>::begin();
       node_iter != mortar_element.getNonMortarEdge(MortarElement::MAIN)->Aggregate<Node>::end() ; 
       ++node_iter ) {
    if( node_iter -> getProperty<bool>("mortar_active") ) {
      Edge* edge = mortar_element.getNonMortarEdge();
      int local_node_number = edge->getLocalNodeNumber( *node_iter );
      double dual_shape = edge->getType().getShape().dualShape( edge->getType().getShape().getNodeIndex(local_node_number) , int_point.getMathIP().r() );
      double lambda_N = node_iter->getLagrangeFactor().get(CoordSys::NORMAL) * dual_shape;
      
      ip_stiffness_4 = initiateLocals<4>( mortar_element , int_point );
      calcIPStiffnessUUN( *ip_stiffness_4,
                          mortar_element, int_point,
                          delta_g_N, 
                          delta_delta_g_N,
                          delta_J,
                          delta_delta_J,
                          jacobian_det, g_N, lambda_N );
      element_stiffness.assemble( *ip_stiffness_4 );
      
      ip_stiffness_2 = initiateLocals<2>( mortar_element , int_point );
      calcIPStiffnessLambdaN( *ip_stiffness_2,
                              mortar_element, int_point,
                              delta_g_N, 
                              delta_J,
                              jacobian_det, g_N, dual_shape );

      element_stiffness.assembleDual( *ip_stiffness_2,
                                      node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::NORMAL), 
                                      GlobalAssembleBase<2>::ASSEMBLE_SYMMETRIC );
      
      delete ip_stiffness_4;
      delete ip_stiffness_2;
      if( node_iter -> hasProperty("slide") ) {
        if( node_iter -> getProperty<int>("slide") == 0 ) { // STICK
          double lambda_T = node_iter->getLagrangeFactor().get(CoordSys::TANGENT_R) * dual_shape;
          
          ip_stiffness_4 = initiateLocals<4>( mortar_element, int_point );
          calcIPStiffnessUUStick( *ip_stiffness_4,
                                  mortar_element, int_point,
                                  delta_g_T, 
                                  delta_delta_g_T,
                                  delta_J,
                                  delta_delta_J,
                                  jacobian_det, g_T, lambda_T );
          element_stiffness.assemble( *ip_stiffness_4 );
          
          ip_stiffness_2 = initiateLocals<2>( mortar_element, int_point );
          calcIPStiffnessLambdaStick( *ip_stiffness_2,
                                      mortar_element, int_point,
                                      delta_g_T, 
                                      delta_J,
                                      jacobian_det, g_T, dual_shape );
          element_stiffness.assembleDual( *ip_stiffness_2,
                                          node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::TANGENT_R), 
                                          GlobalAssembleBase<2>::ASSEMBLE_SYMMETRIC );
          
          delete ip_stiffness_4;
          delete ip_stiffness_2;
        } else { // SLIP
          double t_T = node_iter->getLagrangeFactor().get(CoordSys::NORMAL) * dual_shape * 
            mortar_element.getContact().getMu() * SIGN(node_iter->getProperty<int>("slide")) * (-1.);
          
          ip_stiffness_4 = initiateLocals<4>( mortar_element, int_point );
          calcIPStiffnessUUSlip( *ip_stiffness_4,
                                 mortar_element, int_point,
                                 delta_g_T, 
                                 delta_delta_g_T,
                                 delta_J,
                                 jacobian_det, g_T, t_T );
          element_stiffness.assemble( *ip_stiffness_4 );
          
          ip_stiffness_2 = initiateLocals<2>( mortar_element, int_point );
          calcIPStiffnessLambdaSlip( *ip_stiffness_2, 
                                     mortar_element, int_point, *node_iter,
                                     delta_g_T, 
                                     jacobian_det, dual_shape );

          element_stiffness.assembleDual( *ip_stiffness_2,
                                          node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::NORMAL)
                                          // NO symmetric assemble
                                          );
          
          delete ip_stiffness_4;
          delete ip_stiffness_2;
        }
      }
    }
  }
  
  delete average_normal;
  delete averaging_tensor;
  delete shape;
  delete der_shape_1;
  delete der_shape_2;
  delete mortar_tangent;
  delete non_mortar_tangent;
}

#ifdef SOOFEA_WITH_TBB
//------------------------------------------------------------------
void LAMortarEdgeImpl::IntPointLoad::operator()( const tbb::blocked_range<size_t>& range ) const
{ 
  for( size_t iter = range.begin();
       iter != range.end();
       ++iter ) {
    if( ! mortar_element_.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::at( iter ).getActive() )
      continue;

    mortar_impl_.assemblePerIPLoad( mortar_element_,
                                    mortar_element_.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::at( iter ),
                                    element_load_ );
  }
}
#endif

//------------------------------------------------------------------
GlobalAssembleBase<1>* LAMortarEdgeImpl::calcMortarElementLoadVector(MortarElement& mortar_element)
{
  int active_non_mortar_node_amount = mortar_element.getActiveNonMortarNodeAmount();
  if( active_non_mortar_node_amount == 0 )
    return( 0 );

  LOG(Logger::INFO) << "Mortar Load (lagrange) of non-mortar (primary) Edge: " 
                    << mortar_element.getNonMortarEdge()->getNumber()
                    << " and active nodes = " << active_non_mortar_node_amount << Logger::endl;

  GlobalAssembleBase<1>* element_load = initiateGlobals<1>( mortar_element );

#ifdef SOOFEA_WITH_TBB
  tbb::parallel_for( tbb::blocked_range<size_t>(0,mortar_element.getNonMortarEdge()->getIntegrationPointAmount()), 
                     LAMortarEdgeImpl::IntPointLoad(*this,*element_load,mortar_element) );
#else
  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    if( ! ip_iter -> getActive() )
      continue;

    assemblePerIPLoad( mortar_element,
                       *ip_iter,
                       *element_load );
  }  
#endif 

  (*element_load) *= (-1.); // * mortar_element.getNonMortarElement()->getType().getProperty<double>("height");
  return( element_load );
}

//------------------------------------------------------------------
void LAMortarEdgeImpl::assemblePerIPLoad( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                          GlobalAssembleBase<1>& element_load )
{
  int non_mortar_node_amount = mortar_element.getNonMortarNodeAmountLA();
  CartesianCoordSys coord_sys_3D(3);
  Vector e3( &coord_sys_3D,0,0,1  );
  int local_mortar_node_amount = int_point.getActiveMortarEdge().getType().getShape().getNodeAmount();
  bz::TinyVector<int,2> local_size;
  local_size = DISP_DOF_AMOUNT , non_mortar_node_amount+local_mortar_node_amount;

  double g_N = int_point.getGN();
  double g_T = int_point.getGT();
  Vector& average_tangent = int_point.getAverageTangent();
  bz::Array<double,1> s(2); s = average_tangent/Math::norm( average_tangent );
  Vector* average_normal = Geometry::calcNormal( average_tangent, e3 );
  double alpha = Math::norm( average_tangent , 2 );
  bz::Array<double,1>* averaging_tensor = mortar_element.getAveragingTensor( int_point.getMathIP().r() );
  bz::Array<double,1>* shape = mortar_element.getShapeTensorA( int_point );
  bz::Array<double,1>* der_shape_1 = mortar_element.getDerivativeShapeTensorA( int_point, Contact::NON_MORTAR );
  Vector* mortar_tangent = int_point.getActiveMortarEdge().getEulerianTangent( int_point.xi() );
  Vector* non_mortar_tangent = mortar_element.getNonMortarEdge()->getEulerianTangent( int_point.getMathIP().r() );
  double jacobian_det = Math::norm( *non_mortar_tangent , 2 );
  
  bz::Array<double,2> delta_J(local_size); delta_J = 0.;
  deltaJ( delta_J, *der_shape_1, *non_mortar_tangent, jacobian_det );
  
  bz::Array<double,3> delta_n(local_size(0),local_size(1),local_size(0)); delta_n = 0.;
  deltaN( delta_n, *averaging_tensor, average_tangent, alpha );
  
  bz::Array<double,2> delta_xi(local_size); delta_xi = 0.;
  deltaXi( delta_xi, delta_n, average_tangent, *shape, *mortar_tangent, g_N );
  
  bz::Array<double,2> delta_g_N(local_size); delta_g_N = 0.;
  deltaG_N( delta_g_N, delta_xi, *average_normal, *shape , *mortar_tangent );
  
  bz::Array<double,2> delta_g_T(local_size); delta_g_T = 0.;
  deltaG_T( delta_g_T, delta_xi, *mortar_tangent, s );
  
  AssembleBase<2>* ip_load_2;
  for( Iterator<Node> node_iter = mortar_element.getNonMortarEdge(MortarElement::MAIN)->Aggregate<Node>::begin();
       node_iter != mortar_element.getNonMortarEdge(MortarElement::MAIN)->Aggregate<Node>::end() ; 
       ++node_iter ) {
    if( node_iter -> getProperty<bool>("mortar_active") ) {
      Edge* edge = mortar_element.getNonMortarEdge();
      int local_node_number = edge->getLocalNodeNumber( *node_iter );
      double dual_shape = edge->getType().getShape().dualShape( edge->getType().getShape().getNodeIndex(local_node_number) , int_point.getMathIP().r() );
      double lambda_N = node_iter->getLagrangeFactor().get( CoordSys::NORMAL ) * dual_shape;
      
      ip_load_2 = initiateLocals<2>( mortar_element, int_point );
      calcIPLoadUN( *ip_load_2,
                    mortar_element, int_point,
                    delta_g_N,
                    delta_J,
                    jacobian_det, g_N, lambda_N );
      element_load.assemble( *ip_load_2 );
      
      double ip_load_lambda_N = calcIPLoadLambdaN( mortar_element, int_point,
                                                   jacobian_det, g_N, dual_shape );
      element_load.assembleDual( ip_load_lambda_N , 
                                 node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::NORMAL) );
      
      delete ip_load_2;
      if( node_iter -> hasProperty("slide") ) {
        if( node_iter -> getProperty<int>("slide") == 0 ) { // STICK
          double lambda_T = node_iter->getLagrangeFactor().get(CoordSys::TANGENT_R) * dual_shape;
          
          ip_load_2 = initiateLocals<2>( mortar_element, int_point );
          calcIPLoadUStick( *ip_load_2,
                            mortar_element, int_point,
                            delta_g_T,
                            delta_J,
                            jacobian_det, g_T, lambda_T );
          element_load.assemble( *ip_load_2 );

          double ip_load_lambda_stick = calcIPLoadLambdaStick( mortar_element, int_point,
                                                               jacobian_det, g_T, dual_shape );
          element_load.assembleDual( ip_load_lambda_stick,
                                     node_iter->getDOF(IDObject::LAGRANGE)->getValue(CoordSys::TANGENT_R) );
          
          delete ip_load_2;
        } else { // SLIDE
          double t_T = node_iter->getLagrangeFactor().get(CoordSys::NORMAL) * dual_shape * 
            mortar_element.getContact().getMu() * SIGN(node_iter->getProperty<int>("slide")) * (-1.);
          
          ip_load_2 = initiateLocals<2>( mortar_element, int_point );
          calcIPLoadUSlip( *ip_load_2,
                           mortar_element, int_point,
                           delta_g_T,
                           jacobian_det, t_T );
          element_load.assemble( *ip_load_2 );
          
          delete ip_load_2;
        }
      }
    }
  }

  delete average_normal;
  delete averaging_tensor;
  delete shape;
  delete der_shape_1;
  delete mortar_tangent;
  delete non_mortar_tangent;
}
