/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "st_venant_kirchhoff.h"

//------------------------------------------------------------------
void StVenantKirchhoff::setElasticityTensor(ElementIntegrationPoint& ip)
{
  int dimension = (ip.getDeformationGradient().shape())(0);
  bz::Array<double,2> identity(dimension,dimension);
  Math::kronecker(identity);

  //  double lambda, mu; calcLame(lambda, mu);

  ip.getElasticityTensor() = identity(bzt::i,bzt::j) * identity(bzt::k,bzt::l) * lambda_ + 
    ( identity(bzt::i,bzt::k) * identity(bzt::j,bzt::l) + 
      identity(bzt::i,bzt::l) * identity(bzt::j,bzt::k) ) * mu_;
}

//------------------------------------------------------------------
void StVenantKirchhoff::setStress(ElementIntegrationPoint& ip)
{
  // double lambda, mu; calcLame(lambda, mu);
  
  bz::Array<double,2>& lagr_strain_tensor = ip.getLagrangianStrain();
  bz::Array<double,2> identity(lagr_strain_tensor.shape());
  Math::kronecker(identity);

  ip.getStress() = identity * ( lambda_ * bz::sum( lagr_strain_tensor * identity ) ) + lagr_strain_tensor * 2 * mu_;
}
