/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "neo_hookean.h"

//------------------------------------------------------------------
void NeoHookean::setElasticityTensor(ElementIntegrationPoint& ip)
{
  bz::Array<double,2>& right_cauchy_green_tensor = ip.getRightCauchyGreen();
  bz::Array<double,2> inv_right_cauchy_green_tensor( right_cauchy_green_tensor.shape() );
  Math::LinAlg::luInverse(right_cauchy_green_tensor,
                          inv_right_cauchy_green_tensor);
  Math::LinAlg lin_alg; 
  double det_F = lin_alg.det( ip.getDeformationGradient() );

  //  double lambda, mu; calcLame(lambda,mu);
  int dimension = (right_cauchy_green_tensor.shape())(0);

  bz::Array<double,4> Cinv_dyad_Cinv(dimension,dimension,dimension,dimension);
  Cinv_dyad_Cinv = inv_right_cauchy_green_tensor(bzt::i,bzt::j) * 
    inv_right_cauchy_green_tensor(bzt::k,bzt::l);

  ip.getElasticityTensor() = Cinv_dyad_Cinv * lambda_ + 
    ( Cinv_dyad_Cinv(bzt::i,bzt::k,bzt::j,bzt::l) + Cinv_dyad_Cinv(bzt::i,bzt::l,bzt::j,bzt::k) ) * (mu_ - lambda_ * log(det_F) );
}

//------------------------------------------------------------------
void NeoHookean::setStress(ElementIntegrationPoint& ip)
{
  bz::Array<double,2>& right_cauchy_green_tensor = ip.getRightCauchyGreen();
  bz::Array<double,2> inv_right_cauchy_green_tensor( right_cauchy_green_tensor.shape() );
  Math::LinAlg::luInverse(right_cauchy_green_tensor,
                          inv_right_cauchy_green_tensor);
  Math::LinAlg lin_alg; 
  double det_F = lin_alg.det( ip.getDeformationGradient() );

  // double lambda, mu; calcLame(lambda,mu);

  bz::Array<double,2> identity(inv_right_cauchy_green_tensor.shape());
  Math::kronecker(identity);

  ip.getStress() = ( inv_right_cauchy_green_tensor - identity ) * (-mu_) + inv_right_cauchy_green_tensor * lambda_ * log(det_F);
}


