#ifndef constitutive_h___
#define constitutive_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <cmath>

#include "model/material.h"
#include "model/element_integration_point.h"

#include "math/integration_point.h"
#include "math/array.h"

//------------------------------------------------------------------
class Constitutive
{
public:
  Constitutive(Material& material): 
    material_(material)
  { calcLame(); }

  virtual inline ~Constitutive() {}

  virtual void setElasticityTensor(ElementIntegrationPoint& ip) = 0;
  virtual void setStress(ElementIntegrationPoint& ip) = 0;
protected:
  Material &material_;
  double lambda_;
  double mu_;

  void calcLame();
};

#endif // constitutive_h___
