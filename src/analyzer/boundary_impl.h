#ifndef boundary_impl_h__
#define boundary_impl_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "impl.h"
#include "model/model.h"
#include "math/template/num_int.h"

#include "soofea.h"
#ifdef SOOFEA_WITH_TBB
#include "tbb/queuing_mutex.h"
#else
namespace tbb {
  class queuing_mutex;
}
#endif

//------------------------------------------------------------------
class BoundaryImpl:
  public Impl
{
public:
  inline BoundaryImpl(IDObject::ID type_id=IDObject::BOUNDARY):
    Impl(type_id) {}
  
  virtual ~BoundaryImpl() {}

  virtual AssembleBase<4>* calcBoundaryStiffness(bool start_phase, BoundaryComponent& boundary_comp);
  virtual AssembleBase<2>* calcBoundaryLoadVector(bool start_phase, BoundaryComponent& boundary_comp);
protected:
  void integratePressureStiffness(bz::Array<double,4>& array, BoundaryComponent& boundary_comp, BoundaryIntegrationPoint& int_point);
  void integratePressure(bz::Array<double,2>& array, BoundaryComponent& boundary_comp, BoundaryIntegrationPoint& int_point);

  virtual void deltaJ( bz::Array<double,2>& array,
		       bz::Array<double,1>& der_shape, bz::Array<double,1>& tangent, 
		       double jacobian ) = 0;

  virtual void deltaDeltaJ( bz::Array<double,4>& array,
			    bz::Array<double,1>& der_shape, bz::Array<double,1>& non_mortar_tangent, 
			    double jacobian ) = 0;
};

#endif // boundary_impl_h__
