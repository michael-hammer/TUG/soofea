#ifndef l_mortar_edge_impl_h__
#define l_mortar_edge_impl_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "math/array.h"
#include "model/mortar_element.h"
#include "template/assemble_base.h"

#define SIGN(val) ((val >= 0.) ? +1. : -1.)

//------------------------------------------------------------------
class LMortarEdgeImpl
{
protected:
  //##################################################################
  void calcIPStiffnessUUN( AssembleBase<4>& ip_stiffness, 
                           MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                           bz::Array<double,2>& delta_g_N, 
                           bz::Array<double,4>& delta_delta_g_N, 
                           bz::Array<double,2>& delta_J,
                           bz::Array<double,4>& delta_delta_J,
                           double jacobian_det, double g_N, double lambda_N );
  
  void calcIPStiffnessLambdaN( AssembleBase<2>& ip_stiffness,
                               MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                               bz::Array<double,2>& delta_g_N, 
                               bz::Array<double,2>& delta_J,
                               double jacobian_det, double g_N, double dual_shape );

  void calcIPLoadUN( AssembleBase<2>& ip_load,
                     MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                     bz::Array<double,2>& delta_g_N, 
                     bz::Array<double,2>& delta_J,
                     double jacobian_det, double g_N, double lambda_N );

  double calcIPLoadLambdaN( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                            double jacobian_det, double g_N, double dual_shape);

  //##################################################################
  void calcIPStiffnessUUStick( AssembleBase<4>& ip_stiffness,
                               MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                               bz::Array<double,2>& delta_g_T,
                               bz::Array<double,4>& delta_delta_g_T, 
                               bz::Array<double,2>& delta_J,
                               bz::Array<double,4>& delta_delta_J,
                               double jacobian_det, double g_T, double lambda_T );

  void calcIPStiffnessLambdaStick( AssembleBase<2>& ip_stiffness,
                                   MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                   bz::Array<double,2>& delta_g_T, 
                                   bz::Array<double,2>& delta_J,
                                   double jacobian_det, double g_T, double dual_shape);

  void calcIPLoadUStick( AssembleBase<2>& ip_load,
                         MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                         bz::Array<double,2>& delta_g_T, 
                         bz::Array<double,2>& delta_J,
                         double jacobian_det, double g_T, double lambda_T);

  double calcIPLoadLambdaStick( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                                double jacobian_det, double g_T, double dual_shape);

  //##################################################################
  void calcIPStiffnessUUSlip( AssembleBase<4>& ip_stiffness,
                              MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                              bz::Array<double,2>& delta_g_T,
                              bz::Array<double,4>& delta_delta_g_T, 
                              bz::Array<double,2>& delta_J,
                              double jacobian_det, double g_T, double t_T );

  void calcIPStiffnessLambdaSlip( AssembleBase<2>& ip_stiffness,
                                  MortarElement& mortar_element, BoundaryIntegrationPoint& int_point, Node& node,
                                  bz::Array<double,2>& delta_g_T, 
                                  double jacobian_det, double dual_shape);

  void calcIPLoadUSlip( AssembleBase<2>& ip_load,
                        MortarElement& mortar_element, BoundaryIntegrationPoint& int_point,
                        bz::Array<double,2>& delta_g_T, 
                        double jacobian_det, double t_T);
};

#endif
