/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "newton_raphson_analysis.h"

//------------------------------------------------------------------
void NewtonRaphsonAnalysis::operator()()
{
  LOG(Logger::WARN) << "Newton Raphson Analysis" << Logger::endl;  

  double conv = model_handler_.getModel().getGlobal().getConvergenceCriteria();

  Iterator<TimeStamp> iter = model_handler_.getModel().getTimeBar().Aggregate<TimeStamp>::begin();
  for( ++iter ; // First TimeStamp would be Lagrangian state
       iter != model_handler_.getModel().getTimeBar().Aggregate<TimeStamp>::end() ;
       ++iter ) {
    model_handler_.getBCHandler().integrateBC( (*iter) );

    for ( double unbalanced_energy = DBL_MAX ; 
          (unbalanced_energy > conv) && (iter->iteration() < MAX_ITERATION) ; 
          ++(iter->iteration()) ) {
      unbalanced_energy = solveFESystem( (*iter) );
      LOG(Logger::WARN) << "time step (" << iter->index() << " | " << iter->iteration()
                        << ") unbalanced energy: " << unbalanced_energy 
                        << Logger::endl;
      if( iter->iteration() == 0 ) { // on first iteration the counter should be zero
        model_handler_.getBCHandler().setPrescribedDOFZero( (*iter) );
      }
      if( Configuration::getInstance().getWrite() == IDObject::ITERATION_WRITE )
        model_handler_.write();
    }
    LOG(Logger::WARN) << "---------------------------------" << Logger::endl;
    --(iter->iteration()); // We have to reduce the counter by 1!
    model_handler_.getModel().getTimeBar().setLastFinishedTimeStamp( *iter );

//    for( Iterator<Node> node_iter = model_handler_.getModel().Aggregate<Node>::begin() ;
//         node_iter != model_handler_.getModel().Aggregate<Node>::end() ;
//         ++node_iter ) {
//      std::cout << "Node " << node_iter -> getNumber() 
//                << " f_x = " << node_iter->getLoad( IDObject::FORCE )->getValue( CoordSys::X )->getInternal()
//                << " f_y = " << node_iter->getLoad( IDObject::FORCE )->getValue( CoordSys::Y )->getInternal() << std::endl;
//    }
//    std::cout << "---" << std::endl;

    if( Configuration::getInstance().getWrite() == IDObject::TIME_STEP_WRITE ) {
      model_handler_.write();
    }
    // model_handler_.getModel().prettyPrint();
  }
}
