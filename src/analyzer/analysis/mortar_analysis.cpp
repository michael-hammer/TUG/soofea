/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "mortar_analysis.h"

//------------------------------------------------------------------
void MortarAnalysis::operator()()
{
  double conv = model_handler_.getModel().getGlobal().getConvergenceCriteria();
  bool contact_set_changed = false;
  bool active_nodes_changed = false;
  bool new_time_step = false;
  unsigned contact_iteration_counter = 0;

  if( ! model_handler_.getModel().ModelAggregate<MortarElement>::size() )
    throw EXC(IOException) << "You configured to use a Mortar Fixed-Point-Newton algorithm but no Mortar Element was constructed!" << Exception::endl;

  // Fixed Point Algoritm vs. Semi Smooth Newton
  Iterator<TimeStamp> iter = model_handler_.getModel().getTimeBar().Aggregate<TimeStamp>::begin();
  switch( id_ ) {
  case MORTAR_FIXED_POINT:
    LOG(Logger::WARN) << "Mortar Fixed Point Analysis" << Logger::endl;  
    for( ++iter ; // First TimeStamp would be Lagrangian state
         iter != model_handler_.getModel().getTimeBar().Aggregate<TimeStamp>::end() ;
         ++iter ) {
      LOG(Logger::INFO) << "------------------------------------------------------------------"<< Logger::endl;
      LOG(Logger::INFO) << "========================= Next time step ========================="<< Logger::endl;
      model_handler_.getBCHandler().integrateBC( *iter );

      new_time_step = true;     
      contact_iteration_counter = 1; // that has nothing to do with newton iteration counter

      do {
        contact_set_changed = false;
        active_nodes_changed = false;      
        iter -> iteration() = 0;

        LOG(Logger::NOTICE) << "------------------------------------------------------------------"<< Logger::endl;
        LOG(Logger::NOTICE) << "MORTAR ======= Newton Iterations start for new Contact Set =======" << Logger::endl;
        LOG(Logger::NOTICE) << "Time stamp index: " << iter->index() << Logger::endl;

        std::cout << "We are on time step " << iter->index() << std::endl;

        for ( double unbalanced_energy = DBL_MAX ; 
	      (unbalanced_energy > conv) && (iter->iteration() < MAX_ITERATION ) ;
	      ++(iter->iteration()) ) {

          unbalanced_energy = solveFESystem( (*iter) );
          LOG(Logger::NOTICE) << "time step (" << iter->index() << " | " << iter->iteration()
                              << ") unbalanced energy: " << unbalanced_energy 
                              << Logger::endl;

          if( iter->iteration() == 0 ) { // on first iteration
            model_handler_.getBCHandler().setPrescribedDOFZero( (*iter) );
          }         

          mortarCalcKinematics( false , new_time_step ); // iter->iteration() < 2
          new_time_step = false;

          if( Configuration::getInstance().getWrite() == IDObject::ITERATION_WRITE ) {
            model_handler_.write();
          }

        }
        --(iter->iteration()); // We have to reduce the counter by 1!
        model_handler_.getModel().getTimeBar().setLastFinishedTimeStamp( *iter );

        switch( model_handler_.getModel().getContact().getAnalysisType() ) {
        case IDObject::MORTAR_LA:
          LOG(Logger::NOTICE) << "Lagrange (averaged)";
          if( model_handler_.getModel().getContact().getFriction() )
            LOG(Logger::NOTICE) << " | Friction mu = " << model_handler_.getModel().Aggregate<MortarElement>::begin()->getContact().getMu();
          LOG(Logger::NOTICE) << Logger::endl;
          break;
        case IDObject::MORTAR_LM:
          LOG(Logger::NOTICE) << "Lagrange (mortar)";
          if( model_handler_.getModel().getContact().getFriction() )
            LOG(Logger::NOTICE) << " | Friction mu = " << model_handler_.getModel().Aggregate<MortarElement>::begin()->getContact().getMu();
          LOG(Logger::NOTICE) << Logger::endl;
          break;
        case IDObject::MORTAR_PM:
          LOG(Logger::NOTICE) << "Penalty (mortar)" << Logger::endl;
          break;
        default:
          break;
        }
        LOG(Logger::NOTICE) << "unbalanced energy: --- REACHED --- (time : " << iter->index() << " / " << iter -> iteration() << ")" << Logger::endl;
      
        if( mortarContactSearch( ) ) {
          contact_set_changed = true;
          mortarCalcKinematics( true , new_time_step );
        }

        if( mortarUpdateActiveSet( *iter ) )
          active_nodes_changed = true;
        
        if( Configuration::getInstance().getWrite() == IDObject::CONTACT_SET_WRITE )
          model_handler_.write();

        if( contact_iteration_counter++ > 8 ) { // let's hope that's enough and go further
          // LOG(Logger::INFO) 
          std::cout << "MORTAR contact set oszilates!" << std::endl; // Logger::endl;
          break;
        }
      } while( contact_set_changed || active_nodes_changed );
      if( Configuration::getInstance().getWrite() == IDObject::TIME_STEP_WRITE )
	model_handler_.write();
    }
    break;
  default:   
    throw EXC(CalcException) << "You choosed the Analysis ID '" 
                             << resolveID( id_ ) 
                             << "' which isn't implemented in MortarAnalysis!" << Exception::endl;
    break;
  }
}

//------------------------------------------------------------------
void MortarAnalysis::assembleElementContribution(GlobalStiffness& global_stiffness,
                                                 LoadVector& load_vector,
                                                 bool first_iteration)
{
  LOG(Logger::INFO) << "MORTAR Mortar Assembling" << Logger::endl;

  // First assemble the classical structural part
  Analysis::assembleElementContribution(global_stiffness, load_vector, first_iteration );

  // Here is the right place for parallelization!!!
#ifdef SOOFEA_WITH_TBB
  tbb::parallel_for( tbb::blocked_range<size_t>(0,model_handler_.getModel().ModelAggregate<MortarElement>::size()), 
                     MortarAnalysis::ElementWorker(*map_,model_handler_.getModel(),global_stiffness,load_vector) );
#else
  GlobalAssembleBase<2>* K_mortar_element = 0;
  GlobalAssembleBase<1>* Fint_mortar_element = 0;  
  
  for( Iterator<MortarElement> iter =  model_handler_.getModel().Aggregate<MortarElement>::begin();
       iter != model_handler_.getModel().Aggregate<MortarElement>::end() ; 
       ++iter ) {
    Fint_mortar_element = dynamic_cast<MortarEdgeImpl&>(iter->getType().getImpl()).calcMortarElementLoadVector(*iter);
    K_mortar_element = dynamic_cast<MortarEdgeImpl&>(iter->getType().getImpl()).calcMortarElementStiffness(*iter);
    
    if( Fint_mortar_element ) {
      load_vector.assemble(*Fint_mortar_element, GlobalAssembleBase<2>::POS_FROM_DOF_VALUE);
      delete Fint_mortar_element;
    }
    
    if( K_mortar_element ) {
      global_stiffness.assemble(*K_mortar_element, load_vector);
      delete K_mortar_element;
    }
  }
#endif
}

#ifdef SOOFEA_WITH_TBB
//------------------------------------------------------------------
void MortarAnalysis::ElementWorker::operator()( const tbb::blocked_range<size_t>& range ) const
{
  GlobalAssembleBase<2>* K_mortar_element = 0;
  GlobalAssembleBase<1>* Fint_mortar_element = 0;  

  for( size_t iter = range.begin();
       iter != range.end();
       ++iter ) {
    MortarElement& mortar_element = model_ -> Aggregate<MortarElement>::at( iter );
    Fint_mortar_element = dynamic_cast<MortarEdgeImpl&>(mortar_element.getType().getImpl()).calcMortarElementLoadVector(mortar_element);
    K_mortar_element = dynamic_cast<MortarEdgeImpl&>(mortar_element.getType().getImpl()).calcMortarElementStiffness(mortar_element);
    if( Fint_mortar_element ) {
      load_vector_->assemble(*Fint_mortar_element, GlobalAssembleBase<1>::POS_FROM_DOF_VALUE);
      delete Fint_mortar_element;
    }
    if( K_mortar_element ) {
      global_stiffness_->assemble(*K_mortar_element, *load_vector_);
      delete K_mortar_element;
    }
  }
}
#endif

//------------------------------------------------------------------
void MortarAnalysis::calcAPriori()
{
  Analysis::calcAPriori();

  Iterator<MortarElement> iter = model_handler_.getModel().Aggregate<MortarElement>::begin();
  if( iter == model_handler_.getModel().Aggregate<MortarElement>::end() ) {
    return;
  }

  if( iter->getType().getID() == MORTAR_LA ) {
    dynamic_cast<LAMortarEdgeImpl&>(iter->getType().getImpl()).calcAverageTangentOnNode( *iter, MortarElement::PRECEDING );
  }

  for( ++iter;
       iter != model_handler_.getModel().Aggregate<MortarElement>::end() ; 
       ++iter ) {
    if( iter->getType().getID() == MORTAR_LA ) {
      dynamic_cast<LAMortarEdgeImpl&>(iter->getType().getImpl()).calcAverageTangentOnNode( *iter, MortarElement::FOLLOWING );
    }
  }
}

//------------------------------------------------------------------
void MortarAnalysis::calcAPosteriori( TimeStamp& time_stamp )
{
  /** @todo What if we have multiple ElementTypes ? */
  IDObject::ID type_id = model_handler_.getModel().getContact().getAnalysisType();
  if( (type_id == MORTAR_LM) || 
      (type_id == MORTAR_LA) ) {
    map_ -> updateNodesDOFLagrangeFactor( time_stamp );
  }

  Analysis::calcAPosteriori( time_stamp );
}

//------------------------------------------------------------------
bool MortarAnalysis::mortarUpdateActiveSet( TimeStamp& time_stamp )
{
  LOG(Logger::INFO) << "MORTAR update active set" << Logger::endl;
  bool contact_set_has_changed = false;

  for( Iterator<MortarElement> mortar_element_iter = model_handler_.getModel().Aggregate<MortarElement>::begin() ;
       mortar_element_iter != model_handler_.getModel().Aggregate<MortarElement>::end() ;
       ++mortar_element_iter ) {
    dynamic_cast<MortarEdgeImpl&>(mortar_element_iter->getType().getImpl()).resetNodalCondition( *mortar_element_iter );
  }
    
  for( Iterator<MortarElement> mortar_element_iter = model_handler_.getModel().Aggregate<MortarElement>::begin() ;
       mortar_element_iter != model_handler_.getModel().Aggregate<MortarElement>::end() ;
       ++mortar_element_iter ) {
    dynamic_cast<MortarEdgeImpl&>(mortar_element_iter->getType().getImpl()).calcNodalCondition( *mortar_element_iter );
  }
   
  switch( model_handler_.getModel().getContact().getAnalysisType() ) {
  case IDObject::MORTAR_PM:
    for( Iterator<MortarElement> mortar_element_iter = model_handler_.getModel().Aggregate<MortarElement>::begin() ;
         mortar_element_iter != model_handler_.getModel().Aggregate<MortarElement>::end() ;
         ++mortar_element_iter ) {   
      if( updateActiveSetP( *mortar_element_iter ) ) {
        contact_set_has_changed = true;
      }
    }
    std::cout << "------" << std::endl;
    break;
  case IDObject::MORTAR_LA:   
  case IDObject::MORTAR_LM:
    for( Iterator<Node> node_iter = model_handler_.getModel().Aggregate<Node>::begin() ;
         node_iter != model_handler_.getModel().Aggregate<Node>::end() ;
         ++node_iter ) {
      if( node_iter -> hasProperty("mortar_active") ) {
        if( updateActiveSetL( *node_iter , model_handler_.getModel().getContact().getMu() ) ) {
          contact_set_has_changed = true;
        }
      }
    }

    for( Iterator<MortarElement> mortar_element_iter = model_handler_.getModel().Aggregate<MortarElement>::begin() ;
         mortar_element_iter != model_handler_.getModel().Aggregate<MortarElement>::end() ;
         ++mortar_element_iter ) {
      for( Iterator<Node> node_iter = mortar_element_iter->getNonMortarEdge()->Aggregate<Node>::begin() ;
           node_iter != mortar_element_iter->getNonMortarEdge()->Aggregate<Node>::end() ;
           ++node_iter ) {
        if( node_iter -> getProperty<bool>("mortar_active") ) {
          if( node_iter -> hasProperty("slide") )
            node_iter -> addLagrangeDOF( true, node_iter -> getProperty<int>("slide") == 0 );
          else
            node_iter -> addLagrangeDOF( true, false );
        } else {
          node_iter -> disableDOF(IDObject::LAGRANGE);
        }
      }
    }

    if( contact_set_has_changed ) {
      map_ -> updateNodesDOFLagrangeFactor( time_stamp );
    }

    break;
  default:
    break;
  }

  return( contact_set_has_changed );
}

//------------------------------------------------------------------
bool MortarAnalysis::updateActiveSetP( MortarElement& mortar_element )
{
  bool contact_set_has_changed = false;
  for( Iterator<BoundaryIntegrationPoint> ip_iter = mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::begin();
       ip_iter != mortar_element.getNonMortarEdge()->Aggregate<BoundaryIntegrationPoint>::end();
       ++ip_iter ) {
    // gap must be less then zero AND inside the edge
    if( ((ip_iter -> getGN()) < 0.) && (fabs(ip_iter -> xi()) < 1.) ) {
      Logger::LOG(Logger::INFO) << "ACITVATE: r = " << ip_iter -> getMathIP().r()
                                << "\t Gap value (<0) = " << ip_iter -> getGN()
                                << "\t Xi = " << ip_iter -> xi()
                                << "\t Penalty 'force' = " << (ip_iter -> getGN())*mortar_element.getContact().getPenalty();
      std::cout << ip_iter -> getEulerianPoint().getCoordinate(CoordSys::X) << " " 
                << (ip_iter -> getGN())*mortar_element.getContact().getPenalty() << std::endl;
      if( ip_iter -> setActive(true) ) {
        Logger::LOG(Logger::INFO) << "\t Gap was previously positive!";
        contact_set_has_changed = true;
      }
      Logger::LOG(Logger::INFO) << Logger::endl;
    } else {
      if( ip_iter -> setActive(false) ) {
        Logger::LOG(Logger::INFO) << "r = " << ip_iter -> getMathIP().r() 
                                  << "\t Gap was previously negative - new gap value (>0) = " << ip_iter -> getGN() << Logger::endl;
        contact_set_has_changed = true;
      }
    } 
  }

  if( contact_set_has_changed )
    LOG(Logger::INFO) << "Contact set has changed! (active IPs have changed)" << Logger::endl;
  return( contact_set_has_changed );
}

//------------------------------------------------------------------
bool MortarAnalysis::updateActiveSetL( Node& node , double mu )
{
  bool contact_set_has_changed = false;

  Logger::LOG(Logger::DEBUG) << "Node " << node.getNumber() << " cond_value = " 
                             << node.getProperty<double>("mortar_normal_condition_value") << Logger::endl;

  if( node.getProperty<double>("mortar_normal_condition_value") < 0 ) {
    if( ! node.getProperty<bool>("mortar_active") ) {
      contact_set_has_changed = true;
      LOG(Logger::NOTICE) << "We activate node " << node.getNumber() 
                          << " because of condition value = " 
                          << node.getProperty<double>("mortar_normal_condition_value") << Logger::endl;
      if( node.hasProperty("slide") )
        (node.getProperty<int>("slide")) = 0;
    } else {
      // std::cout << "lambda_N = " << node.getLagrangeFactor().get(CoordSys::NORMAL) << std::endl;
      if( node.hasProperty("slide") ) {
        if( node.getProperty<int>("slide") == 0 ) {
          double t_T = node.getProperty<double>("mortar_tangential_condition_value");
          double t_max = fabs(node.getProperty<double>("mortar_normal_condition_value")) * mu;
          if( fabs(t_T) > t_max ) {
            LOG(Logger::NOTICE) << "We set node " << node.getNumber() 
                                << " to SLIDE because of condition value = " 
                                << fabs(t_T)
                                << " > " << t_max
                                << Logger::endl;
            (node.getProperty<int>("slide")) = ( t_T > 0 ) ? 1 : -1; // We must slide!
            contact_set_has_changed = true;
          }
        } else {
          double g_T = node.getProperty<double>("mortar_tangential_condition_value");
          if( g_T * node.getProperty<int>("slide") <= 0. ) {
            LOG(Logger::NOTICE) << "We set node " << node.getNumber() 
                                << " to STICK because of condition value = " 
                                << g_T << " changing its sign!"
                                << Logger::endl;
            (node.getProperty<int>("slide")) = 0; // We must stick!
            contact_set_has_changed = true;
          }
        }
      }
    }
    (node.getProperty<bool>("mortar_active")) = true;
  } else {
    if( node.getProperty<bool>("mortar_active") ) {
      contact_set_has_changed = true;
      LOG(Logger::NOTICE) << "We deactivate node " << node.getNumber() 
                          << " because of condition value = " 
                          << node.getProperty<double>("mortar_normal_condition_value") << Logger::endl;
    }
    (node.getProperty<bool>("mortar_active")) = false;
    if( node.hasProperty("slide") )
      (node.getProperty<int>("slide")) = 0;
  }

  return( contact_set_has_changed );
}

#ifdef SOOFEA_WITH_TBB
//------------------------------------------------------------------
void MortarAnalysis::MortarCalcKinematicsWorker::operator()( const tbb::blocked_range<size_t>& range ) const
{
  for( size_t iter = range.begin();
       iter != range.end();
       ++iter ) {
    MortarElement& mortar_element = model_.Aggregate<MortarElement>::at( iter );
    dynamic_cast<MortarEdgeImpl&>(mortar_element.getType().getImpl())
      .calcKinematics( mortar_element , research_best_edge_ , new_time_step_ );
  }
}
#endif

//------------------------------------------------------------------
void MortarAnalysis::mortarCalcKinematics( bool research_best_edge , bool new_time_step )
{
  LOG(Logger::INFO) << "MORTAR calc the kinematik gap data" << Logger::endl;
#ifdef SOOFEA_WITH_TBB
  tbb::parallel_for( tbb::blocked_range<size_t>(0, model_handler_.getModel().ModelAggregate<MortarElement>::size() ), 
                     MortarAnalysis::MortarCalcKinematicsWorker( model_handler_.getModel() , research_best_edge , new_time_step ) );
#else
  for( Iterator<MortarElement> mortar_element_iter = model_handler_.getModel().Aggregate<MortarElement>::begin() ;
       mortar_element_iter != model_handler_.getModel().Aggregate<MortarElement>::end() ;
       ++mortar_element_iter ) {
    dynamic_cast<MortarEdgeImpl&>(mortar_element_iter->getType().getImpl())
      .calcKinematics( *mortar_element_iter , research_best_edge , new_time_step );
  }
#endif
}

//------------------------------------------------------------------
bool MortarAnalysis::mortarContactSearch()
{
  LOG(Logger::INFO) << "MORTAR --- CONTACT SEARCH ---" << Logger::endl;

  bool contact_set_has_changed = false;

  for( Iterator<MortarElement> mortar_element_iter = model_handler_.getModel().Aggregate<MortarElement>::begin() ;
       mortar_element_iter != model_handler_.getModel().Aggregate<MortarElement>::end() ;
       ++mortar_element_iter ) {   
    mortar_element_iter -> resetNodalData();

    if( dynamic_cast<MortarEdgeImpl&>(mortar_element_iter->getType().getImpl())
        .contactSearch( *mortar_element_iter , model_handler_.getModel() ) )
      contact_set_has_changed = true;
  }

  return( contact_set_has_changed );
}
