/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "analysis.h"

#include <fstream>

// #ifdef SOOFEA_WITH_TBB
// tbb::queuing_mutex Math::NumInt::num_int_mutex;
// #endif

const unsigned Analysis::MAX_ITERATION = 30;

//------------------------------------------------------------------
Analysis::Analysis(ModelHandler& model_handler, ID id):
  id_(id),
  model_handler_(model_handler),
  map_(new Map(model_handler.getModel()))
{ 
  Math::kronecker(Math::identity2x2);
  Math::kronecker(Math::identity3x3);

  setImpl();

  // Here we do everything we can do a priori and what is not deformation dependent
  for( Iterator<Element> iter =  model_handler_.getModel().Aggregate<Element>::begin();
       iter != model_handler_.getModel().Aggregate<Element>::end() ; 
       ++iter ) {
    bz::Array<double,2>* lagr_node_tensor = iter->getLagrangianPointTensor();
    for( Iterator<ElementIntegrationPoint> ip_iter = iter->Aggregate<ElementIntegrationPoint>::begin();
         ip_iter != iter->Aggregate<ElementIntegrationPoint>::end();
         ++ip_iter ) {
      Jacobian::calc( ip_iter->getLagrangianJacobian(), ip_iter->getMathIP() , lagr_node_tensor );
      Math::LinAlg::luInverse( ip_iter->getLagrangianJacobian() , ip_iter->getInvLagrangianJacobian() );
      ip_iter->getEulerianJacobian() = ip_iter->getLagrangianJacobian();
      (ip_iter->getNJ()) = new bz::Array<double,2>( ip_iter->getMathIP().getDerivativeShapeTensor()->shape() );
      (*ip_iter->getNJ()) = 0.;
      (*ip_iter->getNJ()) = sum((*(ip_iter->getMathIP().getDerivativeShapeTensor()))(bzt::i,bzt::k) * 
                                (ip_iter->getInvLagrangianJacobian())(bzt::k,bzt::j), bzt::k);
      DeformationGradient::calc( ip_iter->getDeformationGradient(), ip_iter->getInvLagrangianJacobian(), ip_iter->getEulerianJacobian() );
      RightCauchyGreen::calc( ip_iter->getRightCauchyGreen(), ip_iter->getDeformationGradient() );
      LagrangianStrain::calc( ip_iter->getLagrangianStrain(), ip_iter->getRightCauchyGreen() );
    }
    delete lagr_node_tensor;
  }
}

//------------------------------------------------------------------
Analysis::~Analysis()
{
  // We have create the element implementation here so we have to destroy it too
  for( Iterator<Type> iter =  model_handler_.getModel().Aggregate<Type>::begin(); 
       iter != model_handler_.getModel().Aggregate<Type>::end() ; 
       ++iter ) {
    if( &(iter->getImpl()) )
      delete &(iter->getImpl());
  }

  for( Iterator<Material> iter =  model_handler_.getModel().Aggregate<Material>::begin(); 
       iter != model_handler_.getModel().Aggregate<Material>::end() ; 
       ++iter ) {
    delete &(iter->getConstitutive());
  }

  if( map_ )
    delete map_;
}

//------------------------------------------------------------------
std::vector<DOFValue*>* Analysis::createBaseMap()
{
  // Here would be the right place to insert a reorder handler!
  std::vector<DOFValue*>* map_vector = new std::vector<DOFValue*>;
  unsigned pos_counter = 0;

  for( Iterator<Node> node_iter = model_handler_.getModel().Aggregate<Node>::begin();
       node_iter != model_handler_.getModel().Aggregate<Node>::end() ;
       ++node_iter ) {
    for( Iterator<DOFValue> dof_iter = node_iter -> Aggregate<DOFValue>::begin();
         dof_iter !=  node_iter -> Aggregate<DOFValue>::end() ; 
         ++dof_iter ) {
      if( (!(dof_iter -> getConstraint())) && 
          dof_iter -> enabled() ) {
        map_vector -> push_back(&(*dof_iter));
        dof_iter -> setPositionInSLE( pos_counter++ );
      } else {
        dof_iter -> setPositionInSLE( -1 );
      }
    }
  }

  return(map_vector);
}

//------------------------------------------------------------------
double Analysis::solveFESystem(TimeStamp& time_stamp)
{
  calcAPriori();

  map_ -> setBaseMap(createBaseMap());
  int long amount_of_unknown =  map_ -> getAmountOfUnknown();

  GlobalStiffness* global_stiffness = new GlobalStiffness(amount_of_unknown,map_->getMapVector());
  LoadVector* load_vector = new LoadVector(amount_of_unknown,map_->getMapVector());

  assembleBoundaryContribution(*global_stiffness,*load_vector,!time_stamp.iteration());
  // std::cout << "Load Vector = " << *load_vector << std::endl;
  assembleElementContribution(*global_stiffness,*load_vector,!time_stamp.iteration());
  assembleExternalNodalForces(*load_vector);
 
  LOG(Logger::INFO) << "Solving System with mount of unknown : " << amount_of_unknown 
                    << Logger::endl;

//  double max_diff;
//  if( !Math::LinAlg::symmetric( *global_stiffness , max_diff ) ) {
//    throw EXC(CalcException) << "Global stiffness not symmetric! max difference = " 
//                             << max_diff << Exception::endl;
//  }
//  LOG(Logger::INFO) << "Symmetrie of Global Stiffness max diff = " << max_diff << Logger::endl;
//  std::cout << "Global Stiffness = " << *global_stiffness << std::endl;
//  std::cout << "Load Vector = " << *load_vector << std::endl;
//  int index = map_ -> getIndexOutOfMap( model_handler_.getModel().Aggregate<Node>::getIndex( 487 )
//                                        .getDOF(DOF::DISPLACEMENT)->getValue(CoordSys::Y) );
//  std::cout << (*load_vector)(index) << std::endl;

  SolutionVector* solution_vector = new SolutionVector(amount_of_unknown);
  switch( model_handler_.getModel().getGlobal().getSLESolutionMethod() ) {
  case Math::LinAlg::CHOLESKY:
    Math::LinAlg::choleskySolve(*global_stiffness, *load_vector, *solution_vector);
    break;
  case Math::LinAlg::LU:
    Math::LinAlg::luSolve(*global_stiffness, *load_vector, *solution_vector);
    break;
  default:
    throw EXC(CalcException) << "The given solution method '" 
                             << IDObject::resolveID( model_handler_.getModel().getGlobal().getSLESolutionMethod() )
                             << "' for the system of linear equations is not defined!" << Exception::endl;
  }

  //  std::cout << "Solution vector = " << *solution_vector << std::endl;
  map_ -> setDOFIncrementsInDOFValues(*solution_vector);

  calcAPosteriori( time_stamp );

  double unbalanced_energy = getScalarUnbalancedEnergy( *solution_vector, *load_vector );

  delete global_stiffness;
  delete load_vector;
  delete solution_vector;

  return( unbalanced_energy );
}

//------------------------------------------------------------------
void Analysis::assembleBoundaryContribution(GlobalStiffness& global_stiffness,
                                           LoadVector& load_vector,
                                           bool first_iteration)
{
  LOG(Logger::INFO) << "Assembling per Boundary" << Logger::endl;

  for( Iterator<Edge> iter =  model_handler_.getModel().Aggregate<Edge>::begin();
       iter != model_handler_.getModel().Aggregate<Edge>::end() ;
       ++iter ) {
    Analysis::calcBoundaryLoad( *iter , global_stiffness , load_vector );
  }

  for( Iterator<Face> iter =  model_handler_.getModel().Aggregate<Face>::begin();
       iter != model_handler_.getModel().Aggregate<Face>::end() ;
       ++iter ) {
    Analysis::calcBoundaryLoad( *iter , global_stiffness , load_vector );
  }
}

//------------------------------------------------------------------
void Analysis::assembleElementContribution(GlobalStiffness& global_stiffness,
                                           LoadVector& load_vector,
                                           bool first_iteration)
{
  LOG(Logger::INFO) << "Assembling per element" << Logger::endl;

#ifdef SOOFEA_WITH_TBB
  tbb::parallel_for( tbb::blocked_range<size_t>(0,model_handler_.getModel().ModelAggregate<Element>::size()), 
                     Analysis::ElementWorker(*map_,model_handler_.getModel(),
                                             global_stiffness,
                                             load_vector) );
#else
  for( Iterator<Element> iter =  model_handler_.getModel().Aggregate<Element>::begin();
       iter != model_handler_.getModel().Aggregate<Element>::end() ;
       ++iter ) {
    Analysis::calcStiffness( *iter , global_stiffness , load_vector );
  }
#endif
}

#ifdef SOOFEA_WITH_TBB
//------------------------------------------------------------------
void Analysis::ElementWorker::operator()( const tbb::blocked_range<size_t>& range ) const
{
  for( size_t iter = range.begin();
       iter != range.end();
       ++iter ) {
    Element& element = model_ -> Aggregate<Element>::at( iter );
    Analysis::calcStiffness( element, *global_stiffness_ , *load_vector_ );
  }
}

//------------------------------------------------------------------
void Analysis::StressStrainWorker::operator()( const tbb::blocked_range<size_t>& range ) const
{
  for( size_t iter = range.begin();
       iter != range.end();
       ++iter ) {
    Analysis::calcStressStrain( model_->Aggregate<Element>::at( iter ) );
  }
}
#endif

//------------------------------------------------------------------
void Analysis::calcStiffness( Element& element,
                              GlobalStiffness& global_stiffness,
                              LoadVector& load_vector )
{
  AssembleBase<2>* F_element = dynamic_cast<ElementImpl&>(element.getType().getImpl()).calcElementLoadVector(false,element);
  if( F_element )
    load_vector.assemble(*F_element, GlobalAssembleBase<1>::POS_FROM_DOF_VALUE);
  delete F_element;

  AssembleBase<4>* K_element = dynamic_cast<ElementImpl&>(element.getType().getImpl()).calcElementStiffness(false,element);
  if( K_element )
    global_stiffness.assemble(*K_element, load_vector);
  delete K_element;
}

//------------------------------------------------------------------
void Analysis::calcBoundaryLoad( BoundaryComponent& boundary_comp,
                                 GlobalStiffness& global_stiffness,
                                 LoadVector& load_vector )
{
  AssembleBase<2>* F_boundary = dynamic_cast<BoundaryImpl&>(boundary_comp.getType().getImpl()).calcBoundaryLoadVector(false,boundary_comp);
  if( F_boundary )
    load_vector.assemble(*F_boundary, GlobalAssembleBase<1>::POS_FROM_DOF_VALUE);
  delete F_boundary;
   
  AssembleBase<4>* K_boundary = dynamic_cast<BoundaryImpl&>(boundary_comp.getType().getImpl()).calcBoundaryStiffness(false,boundary_comp);
  if( K_boundary )
    global_stiffness.assemble(*K_boundary, load_vector);
  delete K_boundary;
}

//------------------------------------------------------------------
void Analysis::calcStressStrain( Element& element )
{
  bz::Array<double,2>* eul_node_tensor = element.getEulerianPointTensor();
  for( Iterator<ElementIntegrationPoint> ip_iter = element.Aggregate<ElementIntegrationPoint>::begin();
       ip_iter != element.Aggregate<ElementIntegrationPoint>::end();
       ++ip_iter ) {
    Jacobian::calc( ip_iter->getEulerianJacobian(), ip_iter->getMathIP() , eul_node_tensor );
    DeformationGradient::calc( ip_iter->getDeformationGradient(), ip_iter->getInvLagrangianJacobian(), ip_iter->getEulerianJacobian() );
    RightCauchyGreen::calc( ip_iter->getRightCauchyGreen(), ip_iter->getDeformationGradient() );
    LagrangianStrain::calc( ip_iter->getLagrangianStrain(), ip_iter->getRightCauchyGreen() );
  }
  delete eul_node_tensor;
  dynamic_cast<ElementImpl&>(element.getType().getImpl()).calcStress( element );
}

//------------------------------------------------------------------
void Analysis::calcAPriori()
{
  for( Iterator<Node> iter =  model_handler_.getModel().Aggregate<Node>::begin();
       iter != model_handler_.getModel().Aggregate<Node>::end() ;
       ++iter ) {
    iter -> resetInternalLoad(); // This is needed as we sum up the internal loads to retrieve the reaction forces
  }
}

//------------------------------------------------------------------
void Analysis::calcAPosteriori( TimeStamp& time_stamp )
{
  map_ -> updateNodesDOFMechanical(time_stamp);
  
#ifdef SOOFEA_WITH_TBB
  tbb::parallel_for( tbb::blocked_range<size_t>(0,model_handler_.getModel().ModelAggregate<Element>::size()), 
                     Analysis::StressStrainWorker(model_handler_.getModel()) );
#else
  for( Iterator<Element> iter =  model_handler_.getModel().Aggregate<Element>::begin();
       iter != model_handler_.getModel().Aggregate<Element>::end() ; 
       ++iter ) {
    Analysis::calcStressStrain( *iter );
  }
#endif
}

//------------------------------------------------------------------
void Analysis::assembleExternalNodalForces(LoadVector& load_vector)
{
  LOG(Logger::INFO) << "Assembling External Nodal Forces" << Logger::endl;
  bz::TinyVector<int,2> size; size = model_handler_.getModel().getGlobal().getDimension(), 
				model_handler_.getModel().ModelAggregate<Node>::size();
  AssembleBase<2> f_ext_nodal( size );

  if( model_handler_.getModel().getGlobal().getDimension() >= 1 ) {
    f_ext_nodal.addCoordID( CoordSys::X );
  }
  if( model_handler_.getModel().getGlobal().getDimension() >= 2 ) {
    f_ext_nodal.addCoordID( CoordSys::Y );
  }
  if( model_handler_.getModel().getGlobal().getDimension() >= 3 ) {
    f_ext_nodal.addCoordID( CoordSys::Z );
  }

  int counter = 0;
  for( Iterator<Node> node_iter = model_handler_.getModel().Aggregate<Node>::begin() ;
       node_iter !=  model_handler_.getModel().Aggregate<Node>::end();
       ++node_iter ) {
    f_ext_nodal.addDOF( node_iter -> getDOF( IDObject::DISPLACEMENT ) );

    if( node_iter -> getLoad(Load::FORCE) ) {
      if( node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::X) ) {
        if( node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::X) -> getConstraint() )
          f_ext_nodal(0,counter) = node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::X) -> getExternal();
      }
      if( node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::Y) ) {
        if( node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::Y) -> getConstraint() )
          f_ext_nodal(1,counter) = node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::Y) -> getExternal();
      }
      if( node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::Z) ) {
        if( node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::Z) -> getConstraint() )
          f_ext_nodal(2,counter) = node_iter -> getLoad(Load::FORCE) -> getValue(CoordSys::Z) -> getExternal();
      }
    }
    if( node_iter -> getLoad(Load::HEAT_FLUX) ) {
      throw EXC(CalcException) << "Heat flux assembling into load vector not implemented!" << Exception::endl;
    }
    ++counter;
  }
  load_vector.assemble(f_ext_nodal, GlobalAssembleBase<1>::POS_FROM_DOF_VALUE);
}

//------------------------------------------------------------------
double Analysis::getScalarUnbalancedEnergy(const SolutionVector& solution_vector,
					   const LoadVector& load_vector)
{
  return( bz::sum( abs( solution_vector * load_vector ) ) );
}


//------------------------------------------------------------------
void Analysis::setImpl()
{
  for( Iterator<Type> iter =  model_handler_.getModel().Aggregate<Type>::begin(); 
       iter != model_handler_.getModel().Aggregate<Type>::end() ; 
       ++iter ) {
    switch( iter -> getID() ) {
    case IDObject::BOUNDARY:
      if( model_handler_.getModel().getGlobal().getDimension() == 2 ) {
        iter -> setImpl( new EdgeImpl() );
      } else {
        // FIXME: Add FaceImpl()!!!
      }
      break;      
    case IDObject::CONTINUUM:
      iter -> setImpl( new ContinuumImpl() );
      break;
    case IDObject::THINSHELL:
      iter -> setImpl( new ThinshellImpl() );
      break;
    case IDObject::MORTAR_PM:
      iter -> setImpl( new PMMortarEdgeImpl() );
      break;
    case IDObject::MORTAR_LM:
      iter -> setImpl( new LMMortarEdgeImpl() );
      break;
    case IDObject::MORTAR_LA:
      iter -> setImpl( new LAMortarEdgeImpl() );
      break;
    default:
      throw EXC(IOException) << "Type '" 
                             << IDObject::resolveID( iter -> getID() ) 
                             << "' not implemented!" << Exception::endl;
      break;
    }
  }

  for( Iterator<Material> iter =  model_handler_.getModel().Aggregate<Material>::begin(); 
       iter != model_handler_.getModel().Aggregate<Material>::end() ; 
       ++iter ) {
    switch( iter->getID() ) {
    case Material::ST_VENANT_KIRCHHOFF:
      iter->setConstitutive( new StVenantKirchhoff( *iter ) );
      break;
    case Material::NEO_HOOKEAN:
      iter->setConstitutive( new NeoHookean( *iter ) );
      break;
    default:
      throw EXC(IOException) << "Material type '" 
                             << Material::resolveID( iter -> getID() ) 
                             << "' not implemented!" << Exception::endl;
      break;
    }
  }
}
