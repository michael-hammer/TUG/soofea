#ifndef analysis_h__
#define analysis_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <math.h>
#include <string>
#include <float.h>

#include "soofea.h"
#include "exceptions.h"

#ifdef SOOFEA_WITH_TBB
#include "tbb/tbb.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"
#endif

#include "base/id_object.h"
#include "base/logger.h"
#include "io/model_handler.h"
#include "model/bc_handler.h"

#include "../map.h"
#include "../global_stiffness.h"
#include "../load_vector.h"
#include "../solution_vector.h"

#include "../boundary_impl.h"
#include "../continuum_impl.h"
#include "../thinshell_impl.h"
#include "../pm_mortar_edge_impl.h"
#include "../lm_mortar_edge_impl.h"
#include "../la_mortar_edge_impl.h"

#include "../constitutive/st_venant_kirchhoff.h"
#include "../constitutive/neo_hookean.h"

//------------------------------------------------------------------
class Analysis :
  public IDObject
{
public:
  static const unsigned MAX_ITERATION;

  virtual ~Analysis();

  /**
     @return time stamp amount
   */
  virtual void operator()() = 0;

protected:
#ifdef SOOFEA_WITH_TBB
  class ElementWorker {
  public:
    ElementWorker(Map& map, Model& model, 
		  GlobalStiffness& global_stiffness, 
		  LoadVector& load_vector) :
      map_(&map),
      model_(&model),
      global_stiffness_(&global_stiffness),
      load_vector_(&load_vector)
    {}

    ElementWorker( const ElementWorker& src ):
      map_(src.map_),
      model_(src.model_),
      global_stiffness_(src.global_stiffness_),
      load_vector_(src.load_vector_)
    {}

    void operator()( const tbb::blocked_range<size_t>& range ) const;
  protected:
    Map* map_;
    Model* model_;
    GlobalStiffness* global_stiffness_;
    LoadVector* load_vector_;
  };

  class StressStrainWorker {
  public:
    StressStrainWorker(Model& model) :
      model_(&model)
    {}

    StressStrainWorker( const StressStrainWorker& src ):
      model_(src.model_)
    {}

    void operator()( const tbb::blocked_range<size_t>& range ) const;
  protected:
    Model* model_;
  };
#endif

  Analysis(ModelHandler& model_handler, ID id);

  std::vector<DOFValue*>* createBaseMap();

  double solveFESystem(TimeStamp& time_stamp);

  double getScalarUnbalancedEnergy(const SolutionVector& solution_vector,
				   const LoadVector& load_vector);

  //------------------------------------------------------------------
  // Hooks
  virtual void calcAPriori();
  virtual void calcAPosteriori( TimeStamp& time_stamp );
  //------------------------------------------------------------------

  virtual void assembleElementContribution(GlobalStiffness& global_stiffness,
					   LoadVector& load_vector,
					   bool new_time_step);

  virtual void assembleBoundaryContribution(GlobalStiffness& global_stiffness,
					    LoadVector& load_vector,
					    bool new_time_step);

  virtual void assembleExternalNodalForces(LoadVector& load_vector);

  virtual void setImpl();

  static void calcStressStrain( Element& element );

  static void calcStiffness( Element& element,
			     GlobalStiffness& global_stiffness,
			     LoadVector& load_vector );

  static void calcBoundaryLoad( BoundaryComponent& boundary_comp,
				GlobalStiffness& global_stiffness,
				LoadVector& load_vector );

  ID id_;

  ModelHandler& model_handler_;
  Map* map_;
};

#endif // analysis_h__
