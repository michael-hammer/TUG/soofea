#ifndef mortar_analysis_h___
#define mortar_analysis_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "newton_raphson_analysis.h"
#include "../la_mortar_edge_impl.h"

//------------------------------------------------------------------
class MortarAnalysis :
  public Analysis
{
public:
  MortarAnalysis(ModelHandler& model_handler, ID id = Analysis::MORTAR_FIXED_POINT) :
    Analysis(model_handler,id) {}

  virtual void operator()();

protected:
#ifdef SOOFEA_WITH_TBB
  class ElementWorker {
  public:
    ElementWorker(Map& map, Model& model, GlobalStiffness& global_stiffness, LoadVector& load_vector) :
      map_(&map),
      model_(&model),
      global_stiffness_(&global_stiffness),
      load_vector_(&load_vector)
    {}

    ElementWorker( const ElementWorker& src ):
      map_(src.map_),
      model_(src.model_),
      global_stiffness_(src.global_stiffness_),
      load_vector_(src.load_vector_)
    {}

    void operator()( const tbb::blocked_range<size_t>& range ) const;
  protected:
    Map* map_;
    Model* model_;
    GlobalStiffness* global_stiffness_;
    LoadVector* load_vector_;
  };

  class MortarCalcKinematicsWorker {
  public:
    MortarCalcKinematicsWorker(Model& model, bool research_best_edge , bool new_time_step ) :
      model_(model),
      research_best_edge_(research_best_edge),
      new_time_step_(new_time_step)
    {}

    MortarCalcKinematicsWorker( const MortarCalcKinematicsWorker& src ):
      model_(src.model_),
      research_best_edge_(src.research_best_edge_),
      new_time_step_(src.new_time_step_)
    {}

    void operator()( const tbb::blocked_range<size_t>& range ) const;
  protected:
    Model& model_;
    bool research_best_edge_;
    bool new_time_step_;
  };
#endif
  /**
     @fixme: This is more a boundary contribution!
   */
  virtual void assembleElementContribution(GlobalStiffness& global_stiffness,
					   LoadVector& load_vector,
					   bool first_iteration);

  virtual void calcAPosteriori( TimeStamp& time_stamp );

  virtual void calcAPriori();

  /**
     Here we integrate the Mortar Contact into our model and calculate
     contact related stuff which is dependend on the last
     solution. This method is called at the beginning and for each
     convergenced calculation. It is NOT called during newton
     iteration.
     
     @return boolean if the Active Set has changed or not
  */
  bool mortarUpdateActiveSet( TimeStamp& time_stamp );

  bool updateActiveSetL( Node& node, double mu );

  bool updateActiveSetP( MortarElement& mortar_element );

  /**
     Here all kinemtic data needed for the mortar stiffness is
     calculated. The calculation is based on the las updated Lagrange
     values.
   */
  void mortarCalcKinematics( bool research_best_edge , bool new_time_step  );

  bool mortarContactSearch( );
};

#endif
