#ifndef thinshell_impl_h__
#define thinshell_impl_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "model/model.h"
#include "math/template/num_int.h"

#include "element_impl.h"
#include "linear_tangent_map/right_cauchy_green.h"
#include "linear_tangent_map/lagrangian_strain.h"

#include "soofea.h"
#ifdef SOOFEA_WITH_TBB
#include "tbb/queuing_mutex.h"
#else
namespace tbb {
  class queuing_mutex;
}
#endif

#include "constitutive/constitutive.h"

#define SIGN(val) ((val >= 0.) ? +1. : -1.)

//------------------------------------------------------------------
class ThinshellImpl : 
  public ElementImpl
{
public:
  static tbb::queuing_mutex node_internal_load_mutex;

  inline ThinshellImpl() :
    ElementImpl(IDObject::THINSHELL) {}

  virtual ~ThinshellImpl() {}

  virtual AssembleBase<4>* calcElementStiffness(bool start_phase, Element& element);
  virtual AssembleBase<2>* calcElementLoadVector(bool start_phase, Element& element);

  virtual void calcStress(Element& element);
protected:
  void integrateConstitutiveComponent(bz::Array<double,4>& array, Element& element, ElementIntegrationPoint& int_point);
  void integrateStressComponent(bz::Array<double,4>& array, Element& element, ElementIntegrationPoint& int_point);
  void integrateInternalForces(bz::Array<double,2>& array, Element& element, ElementIntegrationPoint& int_point);
};

#endif // thinshell_impl_h__
