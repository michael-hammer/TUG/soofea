/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "global_stiffness.h"

//------------------------------------------------------------------
GlobalStiffness::GlobalStiffness( int number_of_unknown , const std::vector<DOFValue*>& dof_value_vector ):
  GlobalAssembleBase<2>( bz::TinyVector<int,2>(number_of_unknown,number_of_unknown) )
{
  setDOFValueVector( dof_value_vector );
}

//------------------------------------------------------------------
void GlobalStiffness::assemble(AssembleBase<4>& element_stiffness, LoadVector& load_vector )
{
  dynamic_cast<GlobalAssembleBase<2>*>(this) -> assemble( element_stiffness, GlobalAssembleBase<2>::POS_FROM_DOF_VALUE );
  
  DOFValue *dof_value_row = 0, *dof_value_col = 0;
  double inc_value = 0.;
  for( int coord_counter_row = 0;
       coord_counter_row != element_stiffness.IndexAggregate<CoordIndex>::size() ;
       ++coord_counter_row ) {
    for( Iterator<DOF> dof_iter_row = element_stiffness.Aggregate<DOF>::begin() ;
         dof_iter_row != element_stiffness.Aggregate<DOF>::end() ;
         ++dof_iter_row ) {
      dof_value_row = dof_iter_row -> getValue( element_stiffness.getCoordIDAt( coord_counter_row ) );
      if( dof_value_row -> getConstraint() ) {
        inc_value = dof_value_row -> getIncremental();
        for( int coord_counter_col = 0;
             coord_counter_col != element_stiffness.IndexAggregate<CoordIndex>::size() ;
             ++coord_counter_col ) {
          for( Iterator<DOF> dof_iter_col = element_stiffness.Aggregate<DOF>::begin() ;
               dof_iter_col != element_stiffness.Aggregate<DOF>::end() ;
               ++dof_iter_col ) {
            dof_value_col = dof_iter_col -> getValue( element_stiffness.getCoordIDAt( coord_counter_col ) );
            // std::vector<DOFValue*>::iterator iter = std::find( dof_value_vector_.begin(), dof_value_vector_.end(), dof_value_col );
            int major_row_index = dof_value_col -> getPositionInSLE();           
            if( major_row_index >= 0 ) {
              // int major_row_index = iter - dof_value_vector_.begin();
#ifdef SOOFEA_WITH_TBB
              GlobalAssembleBase<2>::Mutex::scoped_lock lock(load_vector.getMutex());
#endif          
              load_vector( major_row_index ) -= 
                element_stiffness(coord_counter_row,dof_iter_row.getIndex(),coord_counter_col,dof_iter_col.getIndex()) * inc_value;
            }
          }
        }
      }
    }
  }
}

//------------------------------------------------------------------
void GlobalStiffness::assemble(GlobalAssembleBase<2>& element_stiffness, LoadVector& load_vector)
{
  dynamic_cast<GlobalAssembleBase<2>*>(this) -> assemble( element_stiffness, GlobalAssembleBase<2>::POS_FROM_DOF_VALUE );
}
