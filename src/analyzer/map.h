#ifndef map_h__
#define map_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>
#include "model/stress.h"
#include "model/model.h"
#include "solution_vector.h"
#include "element_impl.h"

//------------------------------------------------------------------
class Map
{
public:
  Map(Model& model);

  Map(Map& src_map);

  ~Map();

  void setBaseMap(std::vector<DOFValue*>* map_vector);

  std::vector<DOFValue*>& getMapVector() { return(*map_vector_); }
  int getAmountOfUnknown();

  /**
     Iters of all DOFValue in the map vector and sets the incremental
     values.

     @param solution_vector is a simple vector containing the solution
     values. It's important that the ordering of this vector and the
     map vector is compatible
   */
  void setDOFIncrementsInDOFValues(const SolutionVector& solution_vector);

  void updateNodesDOFMechanical(const TimeStamp& time_stamp);
  void updateNodesDOFThermal(const TimeStamp& time_stamp);
  void updateNodesDOFLagrangeFactor(const TimeStamp& time_stamp);

  void prettyPrint();
protected:
  std::vector<DOFValue*>* map_vector_;

  Model* model_;
};

#endif // map_h__
