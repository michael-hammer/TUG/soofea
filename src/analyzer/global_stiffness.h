#ifndef global_stiffness_h__
#define global_stiffness_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "math/array.h"
#include "load_vector.h"

#include "template/global_assemble_base.h"
#include "template/assemble_base.h"

//------------------------------------------------------------------
class GlobalStiffness: 
  public GlobalAssembleBase<2>
{
public:
  GlobalStiffness( int number_of_unknown , const std::vector<DOFValue*>& dof_value_vector );
  virtual ~GlobalStiffness() {}

  /**
     assemble routine used for standard stiffness returned as 4D
     object -> element_stiffness has to be symmetric!
   */
  void assemble(AssembleBase<4>& element_stiffness, LoadVector& load_vector);

  /**
     special routine needed for contact algoritm to assemble the
     global stiffness. Mortar stiffness is returned as
     GlobalAssembleBase<2>
   */
  void assemble(GlobalAssembleBase<2>& element_stiffness, LoadVector& load_vector);
};

#endif // global_stiffness_h__
