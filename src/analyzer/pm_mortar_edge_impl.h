#ifndef pm_mortar_edge_impl_h___
#define pm_mortar_edge_impl_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "mortar_edge_impl.h"

#include "m_mortar_edge_impl.h"

//------------------------------------------------------------------
class PMMortarEdgeImpl :
  public MortarEdgeImpl,
  public MMortarEdgeImpl
{
public:
  PMMortarEdgeImpl() :
    MortarEdgeImpl(IDObject::MORTAR_PM) {}

  virtual ~PMMortarEdgeImpl() {}

  virtual GlobalAssembleBase<2>* calcMortarElementStiffness(MortarElement& element);
  virtual GlobalAssembleBase<1>* calcMortarElementLoadVector(MortarElement& element);

  virtual void calcKinematics( MortarElement& mortar_element , bool research_best_edge , bool new_time_step );

  /**
     We don't nead the nodal values to decide active set - we decide
     per integration point
   */
  void resetNodalCondition( MortarElement& mortar_element ) {}
  void calcNodalCondition( MortarElement& mortar_element ) {}

  virtual bool contactSearch( MortarElement& mortar_element , Model& model );

protected:
  static int AMOUNT_OF_DOF_VALUES_PER_NODE;
  static int MORTAR_EDGE_NODE_AMOUNT;

  AssembleBase<4>* calcIPStiffness( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point );
  AssembleBase<2>* calcIPLoad( MortarElement& mortar_element, BoundaryIntegrationPoint& int_point );

  struct XiData {
    XiData(double xi, double gap, unsigned edge_number):
      xi_(xi), gap_(gap), edge_number_(edge_number) {}
    
    /**
       @see MortarIntegrationPoint::xi_
     */
    double xi_;
    
    /**
       @see MortarIntegrationPoint::gap_
    */
    double gap_;

    unsigned edge_number_;
  };

  struct CompareXi {
    bool operator()( const XiData* xi1, const XiData* xi2 ) {
      return( fabs(xi1->xi_) < fabs(xi2->xi_) );
    }
  };

  inline XiData* getMinGap( std::vector<XiData*>& xi_vector )
  { return( *std::min_element( xi_vector.begin(), xi_vector.end() , CompareXi() ) ); }

  void clearXiDataVector( std::vector<XiData*>& vec );
};


#endif
