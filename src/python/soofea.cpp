#include "io/python/io.h"
#include "model/python/model.h"
#include "base/python/base.h"
#include "geometry/python/geometry.h"
#include "version.h"

#include "solver.h"

#include <boost/python/detail/wrap_python.hpp>
#include <boost/python.hpp>

double version() { return( VERSION__ ); }

BOOST_PYTHON_MODULE(_SOOFEA)
{
  using namespace boost::python;
 
  export_io();
  export_base();
  export_model();
  export_geometry();

  def("version",&version);

  class_<Solver>("Solver")
    .def("createModelHandler",&Solver::createModelHandler, return_value_policy<reference_existing_object>())
    .def("run",&Solver::run)
    ;
}
