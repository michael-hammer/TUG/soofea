add_custom_command(OUTPUT ${CMAKE_SOURCE_DIR}/src/base/python/py_id_object.h
  COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/constants.py --mode 'boost.python' ${CMAKE_SOURCE_DIR}/src/base/id_object.xml ${CMAKE_SOURCE_DIR}/src/base/python/py_id_object.h
  MAIN_DEPENDENCY ${CMAKE_SOURCE_DIR}/cmake/scripts/constants.py
  DEPEND ${CMAKE_SOURCE_DIR}/src/base/id_object.xml)

add_custom_command(OUTPUT ${CMAKE_SOURCE_DIR}/src/base/python/py_xml_base_constants.h
  COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/constants.py --mode 'boost.python' ${CMAKE_SOURCE_DIR}/src/base/xml_base_constants.xml ${CMAKE_SOURCE_DIR}/src/base/python/py_xml_base_constants.h
  MAIN_DEPENDENCY ${CMAKE_SOURCE_DIR}/cmake/scripts/constants.py
  DEPEND ${CMAKE_SOURCE_DIR}/src/base/xml_base_constants.xml)

add_library( _SOOFEA SHARED 
  soofea.cpp
  ../solver.cpp 
  ../model/python/model.cpp 
  ../base/python/base.cpp
  ../base/python/py_id_object.h
  ../base/python/py_xml_base_constants.h
  ../geometry/python/geometry.cpp
  ../io/python/io.cpp)

if( SOOFEA_DEBUG)
  SET( SOOFEA_PYTHON_LIB
    ${PYTHON_DEBUG_LIBRARIES} )
else()
  SET( SOOFEA_PYTHON_LIB
    ${PYTHON_LIBRARIES} )
endif()

TARGET_LINK_LIBRARIES( _SOOFEA
  analyzer
  io
  model
  geometry
  math
  base
  exceptions
  boost_python
  ${soofea_third_party_LIBS}
  ${SOOFEA_PYTHON_LIB} )

set_target_properties( _SOOFEA PROPERTIES PREFIX "" )
install(TARGETS _SOOFEA DESTINATION ${SOOFEA_SOURCE_DIR}/python/SOOFEA)
