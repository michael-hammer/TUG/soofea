/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "dynamic_property.h"

//------------------------------------------------------------------
bool DynamicProperty::hasProperty( const std::string& key )
{
  if( map_.find(key) == map_.end() )
    return( false );
  else
    return( true );
}

//------------------------------------------------------------------
void DynamicProperty::addProperty( const std::string key, boost::any value )
{
  if( hasProperty( key ) )
    map_[key] = value;
  else
    map_.insert( std::make_pair(key, value) );
}

//------------------------------------------------------------------
void DynamicProperty::prettyPrintProperties()
{
  for( std::map<const std::string, boost::any>::iterator iter = map_.begin();
       iter != map_.end();
       ++iter ) {
    std::cout << "key : " << iter->first << std::endl;
  }
}
