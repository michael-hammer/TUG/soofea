/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "configuration.h"

Configuration* Configuration::instance_ = 0;

#if BOOST_FILESYSTEM_VERSION == 3 
const fs::path Configuration::default_num_int_path_ = fs::absolute( fs::path( "vcs/git/soofea/etc/soofea/soofea_num_int.xml" ) , getenv("HOME") );
const fs::path Configuration::default_log_path_ = fs::absolute( fs::path( "soofea.log" ) , getenv("HOME") );
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
const fs::path Configuration::default_num_int_path_ = fs::complete( fs::path( "vcs/git/soofea/etc/soofea/soofea_num_int.xml" ) , getenv("HOME") );
const fs::path Configuration::default_log_path_ = fs::complete( fs::path( "soofea.log" ) , getenv("HOME") );
#endif

const Logger::PriorityLevel Configuration::default_log_level_ = Logger::NOTICE;

//------------------------------------------------------------------
Configuration& Configuration::createConfiguration(XMLDOMParser* parser)
{
  if (instance_ != 0)
    delete instance_;

  instance_ = new Configuration(parser);
  return( *instance_ );
}

//------------------------------------------------------------------
void Configuration::terminate()
{
  if( instance_ )
    delete instance_; 
  instance_ = 0;
}

//------------------------------------------------------------------
Configuration::Configuration(XMLDOMParser* parser):
  XMLBase(parser)
{
  configuration_root_ = getChild( CONFIGURATION , true );
}

//------------------------------------------------------------------
Configuration::~Configuration()
{
  if( XMLBase::parser_ )
    delete XMLBase::parser_;
}

//------------------------------------------------------------------
fs::path Configuration::getIOInputFN()
{
  return( getFilePath() );
}

//------------------------------------------------------------------
IDObject::ID Configuration::getIOInputType()
{
  // Yet there are no other types implemented ;-)
  return( IDObject::XML );
}

//------------------------------------------------------------------
void Configuration::outputFile( bool& same_as_input, fs::path& file_path , IDObject::ID& file_type , IDObject::ID& write_state )
{
  DOMElement* element = getChild( OUTPUT, false , configuration_root_ );
  if( !element ) { 
    LOG(Logger::INFO) << "No output configured" << Logger::endl;
    return;
  }

  DOMAttr* write_attr = getAttribute( element , WRITE );
  if( ! write_attr ) {
    throw EXC(IOException) << "You have to tell me when I should do the write with '<output write =...>' " << Exception::endl;
  }
  write_state = IDObject::resolveID( XX(write_attr -> getValue()) );

  DOMElement* file = getChild( FILE , false , element );
  if( !file ) {
    same_as_input = true;
    file_path = getFilePath();
    file_type = IDObject::XML;
    return;
  }
  
  DOMAttr* file_name = getAttribute( file , NAME );
  if( !file_name ) {
    throw EXC(IOException) << "A <file> tag without a 'name=' argument is REALLY useless" << Exception::endl;
  }

#if BOOST_FILESYSTEM_VERSION == 3 
  file_path = fs::absolute( fs::path(XX( file_name -> getValue() )) , getFilePath().branch_path() );
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
  file_path = fs::complete( fs::path(XX( file_name -> getValue() )) , getFilePath().branch_path() );
#endif

  if( fs::equivalent( file_path, getFilePath() ) )
    same_as_input = true;
  else
    same_as_input = false;

  DOMAttr* file_type_attr = getAttribute( file , TYPE );
  if( !file_type_attr ) {
    throw EXC(IOException) << "A <file> tag without a 'type=' argument is REALLY useless" << Exception::endl;
  }
  file_type = IDObject::resolveID( XX(file_type_attr -> getValue()) );
}

//------------------------------------------------------------------
bool Configuration::outputSameAsInputFile()
{
  bool same_as_input = false;
  fs::path file_path;
  IDObject::ID file_type = IDObject::NAID;
  IDObject::ID write_state = IDObject::NO_WRITE;

  outputFile( same_as_input, file_path , file_type , write_state );

  return( same_as_input );
}

//------------------------------------------------------------------
fs::path Configuration::getIOOutputFN()
{
  bool same_as_input = false;
  fs::path file_path;
  IDObject::ID file_type = IDObject::NAID;
  IDObject::ID write_state = IDObject::NO_WRITE;

  outputFile( same_as_input, file_path , file_type , write_state );

  return( file_path );
}

//------------------------------------------------------------------
IDObject::ID Configuration::getIOOutputType()
{
  bool same_as_input = false;
  fs::path file_path;
  IDObject::ID file_type = IDObject::NAID;
  IDObject::ID write_state = IDObject::NO_WRITE;

  outputFile( same_as_input, file_path , file_type , write_state );

  return( file_type );
}

//------------------------------------------------------------------
IDObject::ID Configuration::getWrite()
{
  bool same_as_input = false;
  fs::path file_path;
  IDObject::ID file_type = IDObject::NAID;
  IDObject::ID write_state = IDObject::NO_WRITE;

  outputFile( same_as_input, file_path , file_type , write_state );

  return( write_state );
}

//------------------------------------------------------------------
void Configuration::setIOOutput(const std::string& fn, IDObject::ID io_type, IDObject::ID write_state)
{
  DOMElement* element = getChild( OUTPUT , true, configuration_root_ );
  getAttribute( element , WRITE , IDObject::resolveID(write_state) );
  element = getChild( FILE , true , element );
  getAttribute( element , NAME , fn.c_str() ); 
  getAttribute( element , TYPE , IDObject::resolveID(io_type) );  
}

//------------------------------------------------------------------
void Configuration::numIntFile( fs::path& file_path )
{
  fs::path fn;
  if( file_path.empty() ) {
    fn = default_num_int_path_;
  }

  DOMElement* element = getChild( MATH , true , configuration_root_ );
  element = getChild( NUM_INT_CONFIG , true , element );
  element = getChild( FILE , true , element );

  if( file_path.empty() ) {
    DOMAttr* file_name = getAttribute( element , NAME );
    if( !file_name ) {
      getAttribute( element, NAME, default_num_int_path_.string() );
      file_path = default_num_int_path_;
    } else {
#if BOOST_FILESYSTEM_VERSION == 3
      file_path = fs::absolute( fs::path(XX( file_name -> getValue() )) , getFilePath().branch_path() );
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
      file_path = fs::complete( fs::path(XX( file_name -> getValue() )) , getFilePath().branch_path() );
#endif
    }
  } else {
    getAttribute( element , NAME , fn.string() );
    getAttribute( element , TYPE , IDObject::XML_STR );
    file_path = fn;
  }
}

//------------------------------------------------------------------
fs::path Configuration::getMathNumIntConfigFN()
{
  fs::path fn;
  numIntFile( fn );
  return( fn );
}

//------------------------------------------------------------------
void Configuration::setMathNumIntConfigFN(const std::string& fn)
{
  fs::path file_path(fn);
  numIntFile( file_path );
}

//------------------------------------------------------------------
void Configuration::loggingFile( fs::path& file_path , Logger::PriorityLevel& log_level )
{
  fs::path fn;
  if( file_path.empty() ) {
    fn = default_log_path_;
  }

  DOMElement* element = getChild( LOGGING , true , configuration_root_ );
  if( (file_path.empty()) && (log_level == Logger::NOTSET) ) {
    DOMAttr* log_level_attr = getAttribute( element, PRIORITY );
    if( !log_level_attr ) {
      getAttribute( element, PRIORITY , Logger::resolveLevel( default_log_level_ ) );
      log_level = default_log_level_;
    } else
      log_level = Logger::resolveLevel( XX(log_level_attr -> getValue()) );
  } else {
    getAttribute( element, PRIORITY , Logger::resolveLevel( default_log_level_ ) );
    log_level = default_log_level_;
  }

  element = getChild( FILE , true , element );
  if( file_path.empty() ) {
    DOMAttr* file_name = getAttribute( element, NAME );
    if( !file_name ) {
      getAttribute( element, NAME, default_log_path_.string() );
      file_path = default_log_path_;
    } else {
#if BOOST_FILESYSTEM_VERSION == 3
      file_path = fs::absolute( fs::path(XX( file_name -> getValue() )) , getFilePath().branch_path() );
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
      file_path = fs::complete( fs::path(XX( file_name -> getValue() )) , getFilePath().branch_path() );
#endif
    }
  } else {
    getAttribute( element , NAME , fn.string() );
    file_path = fn;
  }
}

//------------------------------------------------------------------
fs::path Configuration::getLogFN()
{
  fs::path fn;
  Logger::PriorityLevel level = Logger::NOTSET;

  loggingFile( fn , level );

  return( fn );
}

//------------------------------------------------------------------
Logger::PriorityLevel Configuration::getLoggerPriorityLevel()
{
  fs::path fn;
  Logger::PriorityLevel level = Logger::NOTSET;

  loggingFile( fn , level );

  return( level );
}
