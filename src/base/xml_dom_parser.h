#ifndef xml_dom_parser_h__
#define xml_dom_parser_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <string>

#include "x_str.h"
#include "xml_error_handler.h"
#include "parser_base.h"

#include "exceptions.h"
#include "version.h"

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <boost/filesystem.hpp>

namespace fs = boost::filesystem; 
namespace XS = XERCES_CPP_NAMESPACE;

typedef const std::string XMLIdentifier;

//------------------------------------------------------------------
class XMLDOMParser
{
public:
  XMLDOMParser(const std::string& root_element,
	       const float& version);

  ~XMLDOMParser();

  /**
     Opens file and parses it -> create the DOM tree. The root element
     name has to be the string given in the constructor
   */
  void open(const fs::path& file_path, float version) throw(IOException);

  /**
     If a new file should be created (one can provide an allready
     cloned DOMDocument as starting base)
   */
  void create(XS::DOMDocument* clone = 0) throw(IOException);

  void setOutputFileName( fs::path file_path , bool overwrite = false ) throw(IOException);

  XS::DOMDocument* cloneDOMDocument();

  /**
     Writes the DOM tree (if it exists) into the file_path.
   */
  void flush() throw(IOException);
protected:
  friend class XMLBase;
  friend class XMLInputHandler;

  /**
     @return The first child DOMElement with the id name found.

     @param parant_node is the parent DOMElement in which child list the
     method searches

     @param id identifies the DOMElement we should deliver 
   */
  XS::DOMElement* getFirstChildElement(const DOMElement* parent_element, 
				       XMLIdentifier& id);

  /**
     Adds unique child Element to the parent, sets new_element to "true" if
     the element was newly created else return the already existing
     element
   */
  XS::DOMElement* addUniqueChildElement(DOMElement* parent_element, 
					XMLIdentifier& id,
					bool& new_element);

  XS::DOMElement* addUniqueChildElement(DOMElement* parent_element, 
					XMLIdentifier& id);

  /**
     Adds child Element to the parent regardless if an similar element might exist
   */
  XS::DOMElement* addChildElement(DOMElement* parent_element, 
				  XMLIdentifier& id);

  /**
     @return The next (relative to prev_index) child DOMElement with the id name found.

     @param parant_node is the parent DOMElement in which child list the
     method searches

     @param id identifies the DOMElement we should deliver 

     @param prev_index identifies the index of the previous child
     DOMElement in the child list of the parent_node
   */
  XS::DOMElement* getNextChildElement(const XS::DOMElement* parent_element, 
				      XMLIdentifier& id, 
				      int& prev_index);

  /**
     @return The first found attribute with the id name

     @param node is the DOMElement in which attribute list the
     method searches

     @param id identifies the attribute we should deliver 
   */
  XS::DOMAttr* getAttribute(const XS::DOMElement* element, 
                            XMLIdentifier& id);

  XS::DOMAttr* addAttribute(XS::DOMElement* element, 
			    XMLIdentifier& id,
			    const std::string value);

  const std::string ROOT_ELEMENT;

  XS::DOMImplementation* impl_;
  XS::DOMLSParser* parser_;
  XS::DOMErrorHandler* error_handler_;
  XS::DOMLSSerializer* serializer_;
  XS::DOMLSOutput* output_;

  XS::DOMDocument* doc_;

  float version_;
  fs::path file_path_;
private:
  XMLDOMParser(const XMLDOMParser& src) {}
};

#endif // xml_dom_parser_h__
