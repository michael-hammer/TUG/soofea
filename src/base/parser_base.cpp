/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "parser_base.h"

//------------------------------------------------------------------
template<>
void ParserBase::push(std::vector<double>& vect, const char* token)
{
  vect.push_back( atof( token ) );
}

//------------------------------------------------------------------
template<>
void ParserBase::push(std::vector<int>& vect, const char* token)
{
  vect.push_back( atoi( token ) );
}

//------------------------------------------------------------------
template<>
void ParserBase::push(std::vector<long>& vect, const char* token)
{
  vect.push_back( atol( token ) );
}

//------------------------------------------------------------------
template<>
void ParserBase::push(std::vector<unsigned>& vect, const char* token)
{
  vect.push_back( atol( token ) );
}

//------------------------------------------------------------------
bool ParserBase::askForOverwrite( std::string file , bool overwrite )
{
  char type = 'y';
  if( overwrite ) {
    LOG(Logger::WARN) <<  "File `"
                      << file
                      << "` allready exists and will be overwritten!" << Logger::endl;
    return( true );
  } else {
    do {
      std::cout << "Should we overwrite `" << file << "` [y/n]" << std::endl;
      //      std::cin >> type;
      std::cout << "We do so per default ATM!!!!!" << std::endl;
    } while( !std::cin.fail() && type!='y' && type!='n' );
    return( type == 'y' );
  }
}
