#ifndef dynamic_property_h__
#define dynamic_property_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <map>
#include <boost/any.hpp>
#include <iostream>

//------------------------------------------------------------------
class DynamicProperty
{
public:
  void addProperty( const std::string key, boost::any value );

  template<typename T>
  T& getProperty( const std::string key ) {
    return( boost::any_cast<T&>( map_[key] ) );
  }

  template<typename T>
  T getProperty( const std::string key ) const {
    return( boost::any_cast<T>( *(map_.find(key)) ) );
  }

  bool hasProperty( const std::string& key );

  void prettyPrintProperties();

protected:
  std::map<const std::string, boost::any> map_;
};

#endif
