#ifndef parser_base_h___
#define parser_base_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include <string>
#include <sstream>
#include <vector>
#include <boost/tokenizer.hpp>

#include "logger.h"

//------------------------------------------------------------------
class ParserBase
{
public:
  ParserBase() {}
  ~ParserBase() {}

  static bool askForOverwrite( std::string file , bool overwrite = false );
  
  /**
     Converts a given string to a vector of T values (usable for given
     arrays, vectors)
  */
  template <class T>
  void extractVectorFromString(const std::string& string_to_parse,
                               std::vector<T>& vec);

  /**
     Converts T values to String - a generic "itoa"
  */
  template <class T>
  inline std::string toString(const T& t) const
  {
    std::stringstream ss; ss.precision(16);
    ss.setf( std::ios_base::scientific );
    ss << t;
    return ss.str();
  }

  /**
     Converts a vector of values into a String
   */
  template <class T>
  static std::string vectorToString(const std::vector<T>& vec);

protected:
  template <class T>
  void push(std::vector<T>& vect, const char* token);
};

//------------------------------------------------------------------
template<class T>
void ParserBase::extractVectorFromString(const std::string& string_to_parse,
					 std::vector<T>& vect)
{
  typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
  boost::char_separator<char> seperator(" :,");
  tokenizer tokens( string_to_parse, seperator );

  for (tokenizer::iterator tok_iter = tokens.begin();
       tok_iter != tokens.end(); 
       ++tok_iter)
    push( vect, tok_iter->c_str() );

  return;
}

//------------------------------------------------------------------
template <class T>
std::string ParserBase::vectorToString(const std::vector<T>& vec)
{
  std::stringstream ss;
  std::string numbers;

  if( vec.size() ) {
    for( typename std::vector<T>::const_iterator iter = vec.begin();
         iter != vec.end() ;
         ++iter ) {
      ss << *iter << " ";
    }
    numbers = ss.str();
    return( numbers.erase(numbers.length()-1) );
  }
  else
    return( "" );
}

#endif
