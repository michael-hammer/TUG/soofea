#ifndef numbered_object_h___
#define numbered_object_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Author: Michael Hammer
 * ------------------------------------------------------------------*/

#include <soofea_assert.h>

typedef unsigned numbered;

//------------------------------------------------------------------
/**
   All Objects in Model which do have a Number as ID are derived from
   this base class. We start to number with 1!
 */
class NumberedObject
{
public:
  inline NumberedObject(numbered number) : 
    number_(number) {};

  inline NumberedObject(const NumberedObject& src) : 
    number_(src.number_) {};

  inline ~NumberedObject() {};

  inline numbered getNumber() const {return(number_);};
protected:
  numbered number_;
};

#endif // element_h___

