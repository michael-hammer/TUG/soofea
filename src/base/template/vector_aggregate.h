#ifndef vector_aggregate_h___
#define vector_aggregate_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "aggregate.h"
#include "exceptions.h"

//------------------------------------------------------------------
template<class T>
class VectorAggregate :
  public Aggregate<T>
{
public:
  VectorAggregate(std::vector<T*>* vector) :
    vector_(vector) {}

  virtual ~VectorAggregate() {}

protected:
  friend class Iterator<T>;
  std::vector<T*>* vector_;

  virtual T* next(T* act_item, int& act_index);

  using Aggregate<T>::begin;
  virtual Iterator<T> begin( T* );

  using Aggregate<T>::get;
  virtual T* get( T* , int index ) 
  { return( this->at( (T*)0,index ) ); }

  using Aggregate<T>::at;
  virtual T* at( T* , int index );
};

#include "vector_aggregate.tcc"

#endif
