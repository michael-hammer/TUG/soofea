#ifndef const_vector_aggregate_h___
#define const_vector_aggregate_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "aggregate.h"
#include "exceptions.h"

//------------------------------------------------------------------
template<class T>
class ConstVectorAggregate :
  public ConstAggregate<T>
{
public:
  ConstVectorAggregate(const std::vector<T*>* vector) :
    vector_(vector) {}

  virtual ~ConstVectorAggregate() {}

protected:
  friend class Iterator<T>;
  const std::vector<T*>* vector_;

  virtual const T* next(const T* act_item, int& act_index) const;

  using ConstAggregate<T>::begin;
  virtual ConstIterator<T> begin( const T* ) const;

  using ConstAggregate<T>::get;
  virtual const T* get( const T* , int index ) const
  { return( this->at( (const T*)0,index ) ); }

  using ConstAggregate<T>::at;
  virtual const T* at( const T* , int index ) const;
};

#include "const_vector_aggregate.tcc"

#endif
