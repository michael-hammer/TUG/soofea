/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "const_vector_aggregate.h"

//------------------------------------------------------------------
template<class T>
const T* ConstVectorAggregate<T>::next(const T* act_item, int& act_index) const
{
  if( !act_item ) {
    if( act_index < 0 ) {
      throw EXC(CalcException) << "You really want to increment on an end iterator?" << Exception::endl;
    } else {
      act_index = 0;
      return( vector_->at(act_index) );
    }
  } else {
    if( (unsigned)(++act_index) < vector_->size() )
      return( vector_->at(act_index) );
    else {
      act_index = -1;
      return 0;
    }
  }
}

//------------------------------------------------------------------
template<class T>
ConstIterator<T> ConstVectorAggregate<T>::begin( const T* ) const
{
  if( vector_->empty() )
    return( this->ConstAggregate<T>::end() );
  else
    return( ConstIterator<T>( this , vector_->at(0) , 0 ) );
}

//------------------------------------------------------------------
template<class T>
const T* ConstVectorAggregate<T>::at( const T* , int index ) const
{
  return( vector_->at(index) );
}
