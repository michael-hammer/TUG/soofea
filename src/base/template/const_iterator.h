#ifndef const_iterator_h___
#define const_iterator_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <boost/iterator/iterator_facade.hpp>

template<class T> class ConstAggregate;

//------------------------------------------------------------------
template<class T>
class ConstIterator:
  public boost::iterator_facade<ConstIterator<T>, T const, boost::forward_traversal_tag>
{
public:
  inline explicit ConstIterator(const ConstAggregate<T>* aggregate,
                                const T* item, 
                                int index) : 
    aggregate_(aggregate),
    item_(item),
    index_(index) {}

  inline ConstIterator(const Iterator<T>& src) : 
    aggregate_(src.aggregate_),
    item_(src.item_),
    index_(src.index_) {}

  virtual ~ConstIterator() {}

  ConstIterator<T>& operator=(const ConstIterator<T>& src);

  inline int getIndex()
  { return( index_ ); }
protected:
  friend class boost::iterator_core_access;

  const ConstAggregate<T>* aggregate_;
  const T* item_;
  int index_;

  const T& dereference() const;

  bool equal(ConstIterator<T> const& other_iter) const;

  virtual void increment();
};

#endif
