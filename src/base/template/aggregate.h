#ifndef aggregate_h___
#define aggregate_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "iterator.h"

//------------------------------------------------------------------
template<class T>
class Aggregate 
{
public:
  virtual ~Aggregate() {}

  virtual Iterator<T> begin() 
  { return( this->begin(static_cast<T*>(0)) ); }

  virtual T& front()
  { return(*begin()); }

  virtual Iterator<T> end() 
  { return( Iterator<T>(this,0,-1)); }

  virtual T& get(int index) { 
    return( *(this->get(static_cast<T*>(0), index)) ); 
  }

  virtual T& at(int index) {
    return( *(this->at(static_cast<T*>(0), index)) ); 
  }

protected:
  friend class Iterator<T>;

  virtual T* next(T* act_item, int& act_index) = 0;

  virtual Iterator<T> begin( T* ) = 0;

  virtual T* get( T* , int index ) = 0;

  virtual T* at( T* , int index ) = 0;
};

#include "iterator.tcc"

#endif
