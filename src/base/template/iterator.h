#ifndef iterator_h___
#define iterator_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <boost/iterator/iterator_facade.hpp>

template<class T> class Aggregate;

//------------------------------------------------------------------
template<class T>
class Iterator:
  public boost::iterator_facade<Iterator<T>, T, boost::forward_traversal_tag>
{
public:
  /// Default values are given for an Iterator being ahead of first
  inline explicit Iterator(Aggregate<T>* aggregate,
			   T* item = 0, 
                           int index = 0) : 
    aggregate_(aggregate),
    item_(item),
    index_(index) {}

  inline Iterator(const Iterator<T>& src) : 
    aggregate_(src.aggregate_),
    item_(src.item_),
    index_(src.index_) {}

  virtual ~Iterator() {}

  Iterator<T>& operator=(const Iterator<T>& src);

  inline int getIndex()
  { return( index_ ); }

  inline bool onEnd()
  { return( !item_ ); }

protected:
  friend class boost::iterator_core_access;

  Aggregate<T>* aggregate_;
  T* item_;
  int index_;

  T& dereference() const;

  bool equal(Iterator<T> const& other_iter) const;

  virtual void increment();
};

#endif
