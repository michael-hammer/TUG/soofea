#ifndef const_aggregate_h___
#define const_aggregate_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "const_iterator.h"

//------------------------------------------------------------------
template<class T>
class ConstAggregate 
{
public:
  virtual ~ConstAggregate() {}

  virtual ConstIterator<T> begin() const 
  { return( this->begin(static_cast<const T*>(0)) ); }

  virtual const T& front() const
  { return(*begin()); }

  virtual ConstIterator<T> end() const
  { return( ConstIterator<T>(this,0,-1)); }

  virtual const T& get(int index) const { 
    return( *(this->get(static_cast<T*>(0), index)) ); 
  }

  virtual const T& at(int index) const {
    return( *(this->at(static_cast<T*>(0), index)) ); 
  }

protected:
  friend class ConstIterator<T>;

  virtual const T* next(const T* act_item, int& act_index) const = 0;

  virtual ConstIterator<T> begin( const T* ) const = 0;

  virtual const T* get( const T* , int index ) const = 0;

  virtual const T* at( const T* , int index ) const = 0;
};

#include "const_iterator.tcc"

#endif
