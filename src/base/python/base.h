#ifndef py__base_h__
#define py__base_h__

#include "../configuration.h"
#include "../numbered_object.h"

#include <string>
#include <boost/python.hpp>

void export_base();

#endif
