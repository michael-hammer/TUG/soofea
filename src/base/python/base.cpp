#include "base.h"
#include "py_id_object.h"
#include "py_xml_base_constants.h"

//------------------------------------------------------------------
struct PyConfiguration
{
  static std::string getIOOutputFN(Configuration& self) {
    return( self.getIOOutputFN().string() );
  }

  static IDObject::ID getIOOutputType(Configuration& self) {
    return( self.getIOOutputType() );
  }

  static std::string getIOInputFN(Configuration& self) {
    return( self.getIOInputFN().string() );
  }

  static IDObject::ID getIOInputType(Configuration& self) {
    return( self.getIOInputType() );
  }

  static std::string getMathNumIntConfigFN(Configuration& self) {
    return( self.getMathNumIntConfigFN().string() );
  }

  static std::string getLogFN(Configuration& self) {
    return( self.getLogFN().string() );
  }

  static IDObject::ID getWrite(Configuration& self) {
    return( self.getWrite() );
  }

  static Logger::PriorityLevel getLoggerPriorityLevel(Configuration& self) {
    return( self.getLoggerPriorityLevel() );
  }
};

//------------------------------------------------------------------
struct PyLogger
{
  static std::string resolveLevel_level(Logger::PriorityLevel level) {
    return( Logger::resolveLevel( level ) );
  }

  static Logger::PriorityLevel resolveLevel_string(std::string level_str) {
    return( Logger::resolveLevel( level_str ) );
  }
};

//------------------------------------------------------------------
struct PyDynamicProperty
{
  static void addDoubleProperty( DynamicProperty& self , std::string key , double value ) {
    self.addProperty( key, boost::any(value) );
  }
  
  static void addIntProperty( DynamicProperty& self , std::string key , int value ) {
    self.addProperty( key, boost::any(value) );    
  }

  static double getDoubleProperty( DynamicProperty& self , std::string key ){
    return( self.getProperty<double>( key ) );
  }

  static int getIntProperty( DynamicProperty& self , std::string key ){
    return( self.getProperty<int>( key ) );
  }
};

//------------------------------------------------------------------
void export_base()
{
  using namespace boost::python;

  class_<DynamicProperty>("DynamicProperty", no_init)
    .def("addProperty",&PyDynamicProperty().addDoubleProperty)
    .def("addProperty",&PyDynamicProperty().addIntProperty)
    .def("getDoubleProperty",&PyDynamicProperty().getDoubleProperty)
    .def("getIntProperty",&PyDynamicProperty().getIntProperty)
    ;

  class_<NumberedObject>("NumberedObject", no_init)
    .def("getNumber",&NumberedObject::getNumber)
    ;

  class_<Configuration>("Configuration", no_init)
    .def("getInstance", &Configuration::getInstance,return_value_policy<reference_existing_object>())
    .staticmethod("getInstance")
    .def("getIOOutputFN", &PyConfiguration().getIOOutputFN )
    .def("getIOOutputType", &PyConfiguration().getIOOutputType )
    .def("getWrite", &PyConfiguration().getWrite )
    .def("getIOInputFN", &PyConfiguration().getIOInputFN )
    .def("getIOInputType", &PyConfiguration().getIOInputType )
    .def("getMathNumIntConfigFN", &PyConfiguration().getMathNumIntConfigFN )
    .def("getLogFN", &PyConfiguration().getLogFN )
    .def("getLoggerPriorityLevel", &PyConfiguration().getLoggerPriorityLevel )
    ;

  def("resolveLevel",&PyLogger().resolveLevel_level);
  def("resolveLevel",&PyLogger().resolveLevel_string);

  PyIDObject();
  PyXMLBaseConstants();

  enum_<Logger::PriorityLevel>("PriorityLevel")
    .value("FATAL",Logger::FATAL)
    .value("ALERT",Logger::ALERT)
    .value("CRIT",Logger::CRIT)
    .value("ERROR",Logger::ERROR)
    .value("WARN",Logger::WARN)
    .value("NOTICE",Logger::NOTICE)
    .value("INFO",Logger::INFO)
    .value("DEBUG",Logger::DEBUG)
    .value("NOTSET",Logger::NOTSET)
    ;
}
