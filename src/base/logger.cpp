/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "logger.h"

Logger* Logger::instance_ = 0;
const ControlSig Logger::endl(ControlSig::endl);
Logger::PriorityLevel Logger::prior_level_ = Logger::DEBUG;
std::stringstream Logger::log_stream_;

const std::string Logger::NOTSET_STR_="NOTSET"; 
const std::string Logger::FATAL_STR_="FATAL"; 
const std::string Logger::ALERT_STR_="ALERT";
const std::string Logger::CRIT_STR_="CRIT";
const std::string Logger::ERROR_STR_="ERROR";
const std::string Logger::WARN_STR_="WARN";
const std::string Logger::NOTICE_STR_="NOTICE";
const std::string Logger::INFO_STR_="INFO";
const std::string Logger::DEBUG_STR_="DEBUG";
  
//------------------------------------------------------------------
Logger::Logger(const fs::path& log_file_path,
	       Logger::PriorityLevel max_priority) :
  log4cpp::Category("main_category",0)
{
  log4cpp::FileAppender* app_log = new log4cpp::FileAppender("FileAppender", log_file_path.string() );
  app_log -> setLayout( createLayout() );
  setAdditivity(false);
  setAppender(app_log); // The used method passes ownership!
  log4cpp::Category::setPriority(max_priority);

  instance_->log_stream_.precision(4);
  instance_->log_stream_.setf( std::ios_base::scientific );
  instance_->log_stream_.setf( std::ios_base::showpos );
}

//------------------------------------------------------------------
log4cpp::PatternLayout* Logger::createLayout()
{
  log4cpp::PatternLayout* layout = new log4cpp::PatternLayout();  
  std::string pattern = "%d{%d.%m %H:%M:%S} | %m %n";
  layout -> setConversionPattern(pattern);   
  return( layout );
}

//------------------------------------------------------------------
void Logger::open(const fs::path& log_file_path,
		  Logger::PriorityLevel max_priority)
{
  if( Logger::instance_ -> getAppender() )
    Logger::instance_ -> removeAllAppenders();
  log4cpp::FileAppender* app_log = new log4cpp::FileAppender("FileAppender", log_file_path.string() );
  app_log -> setLayout( Logger::instance_ -> createLayout() );
  Logger::instance_ -> setAppender(app_log);

  dynamic_cast<log4cpp::Category*>(Logger::instance_)->setPriority(max_priority);

  Logger::instance_ -> getStream(Logger::WARN) << "==================================================================" 
                                               << log4cpp::eol
                                               << "Starting SOOFEA Main Logging" << log4cpp::eol;
}
  
//------------------------------------------------------------------
Logger* Logger::createLogger(const fs::path& output_file_name,
			     Logger::PriorityLevel max_priority)
{
  if (instance_ == 0)
    instance_ = new Logger(output_file_name, max_priority);
  
  return instance_;
}

//------------------------------------------------------------------
void Logger::terminate()
{
  if( instance_ ) {
    instance_ -> getStream(Logger::WARN) << "Ending SOOFEA" 
                                         << log4cpp::eol
                                         << "=================================================================="
                                         << log4cpp::eol;
    delete instance_;
  }
  instance_ = 0;
}

//------------------------------------------------------------------
Logger::~Logger()
{
  shutdown();
}

//------------------------------------------------------------------
template<>
Logger& Logger::operator<<(const ControlSig& t)
{
  switch( t.sig() ) {
  case ControlSig::endl:
    instance_ -> getStream(instance_ -> prior_level_) << instance_ -> log_stream_.str() << log4cpp::eol;
    instance_ -> log_stream_.str("");
    break;
  }
  return( *this );
}

//------------------------------------------------------------------
Logger& Logger::log(Logger::PriorityLevel priority, const char method_name[])
{
  if( priority != instance_ -> prior_level_ ) {
    // We flush the stream because we are on new priority!
    if( !instance_ -> log_stream_.str().empty() ) {
      instance_ -> getStream(instance_ -> prior_level_) << instance_ -> log_stream_.str() << log4cpp::eol;
      instance_ -> log_stream_.str("");
    }
  }
  instance_ -> prior_level_ = priority;
  if( method_name )
    instance_ -> getStream(Logger::DEBUG) << "\t\t\t" << method_name;
  return ( *instance_ );
}

//------------------------------------------------------------------
const std::string& Logger::resolveLevel(Logger::PriorityLevel level)
{
  switch( level ) {
  case FATAL  :
    return( FATAL_STR_ );
    break;
  case ALERT  :
    return( ALERT_STR_ );
    break;
  case CRIT   :
    return( CRIT_STR_ );
    break;
  case ERROR  :
    return( ERROR_STR_ );
    break;
  case WARN   :
    return( WARN_STR_ );
    break;
  case NOTICE :
    return( NOTICE_STR_ );
    break;
  case INFO   :
    return( INFO_STR_ );
    break;
  case DEBUG  :
    return( DEBUG_STR_ );
    break;
  default:
    //    throw EXC(IOException) << "The Logger::PriorityLevel " << level << " is not valid" << Exception::endl;
    return( NOTSET_STR_ );
    break;
  }
}

//------------------------------------------------------------------
Logger::PriorityLevel Logger::resolveLevel(const std::string& priority)
{
  if( priority == DEBUG_STR_ )
    return( Logger::DEBUG );
  if( priority == INFO_STR_ )
    return( Logger::INFO );
  if( priority == NOTICE_STR_ )
    return( Logger::NOTICE );
  if( priority == WARN_STR_ )
    return( Logger::WARN );
  if( priority == ERROR_STR_ )
    return( Logger::ERROR );
  if( priority == CRIT_STR_ )
    return( Logger::CRIT );
  if( priority == ALERT_STR_ )
    return( Logger::ALERT );
  if( priority == FATAL_STR_ )
    return( Logger::FATAL );

  //  throw EXC(IOException) << "The Logger::PriorityLevel string '" << priority << "' is not valid" << Exception::endl;
  return( Logger::NOTSET );
}
