/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "xml_error_handler.h"

XERCES_CPP_NAMESPACE_USE

//------------------------------------------------------------------
bool XMLErrorHandler::handleError (const DOMError &dom_error)
{
  std::string error_type = "";
  bool should_we_continue = true;

  if (dom_error.getSeverity() == DOMError::DOM_SEVERITY_WARNING)
    error_type = "warning message";
  else if (dom_error.getSeverity() == DOMError::DOM_SEVERITY_ERROR)
    error_type = "error message";
  else {
    error_type = "FATAL error message";
    should_we_continue = false;
  }

  LOG(Logger::ERROR) << "Got XMLParser " << error_type << " at line " 
		     << dom_error.getLocation()->getLineNumber()
		     << " in column "
		     << dom_error.getLocation()->getColumnNumber()
		     << " - Parser said: " << XX(dom_error.getMessage()) << Logger::endl;

  return( should_we_continue );
}
