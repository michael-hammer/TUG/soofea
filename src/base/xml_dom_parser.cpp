/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "xml_dom_parser.h"

//------------------------------------------------------------------
XMLDOMParser::XMLDOMParser(const std::string& root_element,
                           const float& version):
  ROOT_ELEMENT(root_element),
  impl_(0),
  parser_(0),
  error_handler_(0),
  doc_(0),
  version_(version)
{
  Logger::log(Logger::NOTICE) << "DOMParser for <" <<
    root_element << "> created" <<
    Logger::endl;

  try {
    XMLPlatformUtils::Initialize();
  }
  catch (const XMLException& toCatch) {
    EXC(IOException) << "ERROR: Xerces Exception during initialization! :"
		     << XX(toCatch.getMessage()) << Exception::endl;;
    return;
  }
  impl_ = DOMImplementationRegistry::getDOMImplementation(X("Core"));

  parser_ = dynamic_cast<DOMImplementationLS*>(impl_) -> createLSParser(DOMImplementationLS::MODE_SYNCHRONOUS, NULL);
  error_handler_ = new XMLErrorHandler();

  parser_ -> getDomConfig() -> setParameter(XMLUni::fgDOMErrorHandler, error_handler_ );
  parser_ -> getDomConfig() -> setParameter(XMLUni::fgDOMNamespaces, true);
  parser_ -> getDomConfig() -> setParameter(XMLUni::fgDOMValidate, true);
  parser_ -> getDomConfig() -> setParameter(XMLUni::fgXercesSchema, true);
  parser_ -> getDomConfig() -> setParameter(XMLUni::fgXercesSchemaFullChecking, true);
  parser_ -> getDomConfig() -> setParameter(XMLUni::fgDOMElementContentWhitespace, false);

  serializer_ = dynamic_cast<DOMImplementationLS*>(impl_) -> createLSSerializer();
  serializer_ -> getDomConfig() -> setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);

  output_ = dynamic_cast<DOMImplementationLS*>(impl_) -> createLSOutput();
}

//------------------------------------------------------------------
XMLDOMParser::~XMLDOMParser()
{
  //  doc_ -> release();
  parser_ -> release();
  delete error_handler_;
  serializer_ -> release();
  output_ -> release();
  XMLPlatformUtils::Terminate();
}

//------------------------------------------------------------------
XS::DOMDocument* XMLDOMParser::cloneDOMDocument()
{
  XS::DOMDocument* clone = dynamic_cast<DOMDocument*>(doc_->cloneNode( true ));
  return( clone );
}

//------------------------------------------------------------------
void XMLDOMParser::open(const fs::path& file_path, float version)
  throw(IOException)
{
  file_path_ = file_path;
#if BOOST_FILESYSTEM_VERSION == 3
  Logger::log(Logger::NOTICE) << "Parsing document: " << file_path.string() << Logger::endl;
  try {
    if( !fs::exists(file_path) )
      throw EXC(IOException) << "XML file I should parse '" << file_path.string() << "' does not exist!" << Exception::endl;
    doc_ = parser_ -> parseURI(file_path.string().c_str());
  } catch (const XMLException& toCatch) {
    throw EXC(IOException) << "Catched XMLException: "
                           << XX(toCatch.getMessage()) << Exception::endl;
  } catch (const DOMException& toCatch) {
    throw EXC(IOException) << "Catched DOMException: "
                           << XX(toCatch.msg) << Exception::endl;
  }
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
  Logger::log(Logger::NOTICE) << "Parsing document: " << file_path.native_file_string() << Logger::endl;
  try {
    if( !fs::exists(file_path) )
      throw EXC(IOException) << "XML file I should parse '" << file_path.native_file_string() << "' does not exist!" << Exception::endl;
    doc_ = parser_ -> parseURI(file_path.native_file_string().c_str());
  } catch (const XMLException& toCatch) {
    throw EXC(IOException) << "Catched XMLException: "
                           << XX(toCatch.getMessage()) << Exception::endl;
  } catch (const DOMException& toCatch) {
    throw EXC(IOException) << "Catched DOMException: "
                           << XX(toCatch.msg) << Exception::endl;
  }
#endif
  
  if( !doc_ ) {
    throw EXC(IOException) << "XML file could not be parsed!" << Exception::endl;
  }
  if( doc_ -> getDocumentElement() == 0 ) {
    throw EXC(IOException) << "No XML root Element found!" << Exception::endl;
  }
  if( ROOT_ELEMENT != XX(doc_ -> getDocumentElement() -> getTagName()) ) {
    throw EXC(IOException) << "Wrong input file! Root Element in file should be <"
			   << ROOT_ELEMENT << ">" << Exception::endl;
  }

  DOMAttr* version_attr = dynamic_cast<DOMAttr*>(doc_ -> getDocumentElement()
						 -> getAttributes() -> getNamedItem( X("version") )); 
  if( version_attr )
    version_ = atof( XX(version_attr -> getValue()) );
  else {
    Logger::log(Logger::NOTICE) << "WARNING: Input file <"
#if BOOST_FILESYSTEM_VERSION == 3
				<< file_path.string()
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
				<< file_path.native_file_string()
#endif
				<< "> without version attribute in root tag! Version 0.1 assumed!"
				<< Logger::endl;
  }

  if( version_ != version )
    EXC(IOException) << "Wrong SOOFEA input file version " << version_ 
                     << " as we need version " << version << Exception::endl;

  return;
}

//------------------------------------------------------------------
void XMLDOMParser::create(XS::DOMDocument* clone)
  throw(IOException)
{
  if( ! clone ) {
    doc_ = impl_ -> createDocument( 0 , X(ROOT_ELEMENT) , 0 );
    doc_ -> setXmlVersion( X("1.0") );
    
    DOMAttr* version = doc_ -> createAttribute( X("version") );
    std::ostringstream str;
    str << version_; 
    version -> setValue( X(str.str()) );
    doc_ -> getDocumentElement() -> setAttributeNode( version );

    DOMAttr* xmlns = doc_ -> createAttribute( X("xmlns:xsi") );
    xmlns -> setValue( X("http://www.w3.org/2001/XMLSchema-instance") );
    doc_ -> getDocumentElement() -> setAttributeNode( xmlns );

    DOMAttr* xsi = doc_ -> createAttribute( X("xsi:noNamespaceSchemaLocation") );
    xsi -> setValue( X("http://soofea.fest.tugraz.at/xml/soofea.xsd") );
    doc_ -> getDocumentElement() -> setAttributeNode( xsi );
  } else {
    doc_ = clone;
  }
}
 
//------------------------------------------------------------------
void XMLDOMParser::setOutputFileName( fs::path file_path , bool overwrite )
  throw(IOException)
{
  bool write = false;

  if( file_path.empty() )
    throw EXC(IOException) << "We need a file path to actually set one in XMLDOMParser" << Exception::endl;
  
//  if( file_path.extension().string().compare(".xml") && file_path.extension().string().compare(".XML") ) {
//    std::cout << "We replace the output file path to have the extension `.xml`" << std::endl;
//    file_path.replace_extension(fs::path(".xml"));
//  }

  if( fs::exists(file_path) ) {
    write = ParserBase::askForOverwrite( file_path.string() , overwrite );
    if( !write ) {
      std::cout << "File allready exitsts and you choose not to overwrite!" << std::endl;
      exit(-1);
    }
  }

  file_path_ = file_path;
}
 
//------------------------------------------------------------------
void XMLDOMParser::flush()
  throw(IOException)
{
  if( !doc_ ) {
    throw EXC(IOException) << "DOM tree does not exist to write down!" << Exception::endl;;
  }
  if( doc_ -> getDocumentElement() == 0 ) {
    throw EXC(IOException) << "No XML root Element found in DOM tree!" << Exception::endl;;
  }
  if( ROOT_ELEMENT != XX(doc_ -> getDocumentElement() -> getTagName()) ) {
    throw EXC(IOException) << "Wrong DOM tree to write! Root Element DOM should be <"
			   << ROOT_ELEMENT << ">" << Exception::endl;;
  } 

#if BOOST_FILESYSTEM_VERSION == 3
  LocalFileFormatTarget xml_target(X(file_path_.string().c_str()));
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
  LocalFileFormatTarget xml_target(X(file_path_.native_file_string().c_str()));
#endif

  output_ -> setByteStream( &xml_target );
  serializer_ -> write( doc_ ,
			output_ );
}

//------------------------------------------------------------------
DOMElement* XMLDOMParser::getFirstChildElement(const DOMElement* parent_element, 
                                               XMLIdentifier& id)
{
  DOMNodeList* list = parent_element -> getElementsByTagName( X(id) );  
  if( list -> getLength() )
    return( dynamic_cast<DOMElement*>(list -> item(0)) );
  else
    return( 0 );
}

//------------------------------------------------------------------
DOMElement* XMLDOMParser::getNextChildElement(const DOMElement* parent_element, 
                                              XMLIdentifier& id, 
                                              int& prev_index)
{
  DOMNodeList* list = parent_element -> getElementsByTagName( X(id) );
  if( ++prev_index < (int)(list -> getLength()) ) {
    return( dynamic_cast<DOMElement*>( list -> item(prev_index) ) );
  } else {
    //    prev_index = 0;
    return( 0 );
  }
}

//------------------------------------------------------------------
DOMElement* XMLDOMParser::addUniqueChildElement(DOMElement* parent_element,XMLIdentifier& id)
{
  bool new_element = false;
  return( addUniqueChildElement(parent_element,id,new_element) );
}

//------------------------------------------------------------------
DOMElement* XMLDOMParser::addUniqueChildElement(DOMElement* parent_element, XMLIdentifier& id, bool& new_element)
{
  DOMElement* child_element = getFirstChildElement( parent_element , id  );
  if( !child_element ) {
    new_element = true;
    child_element = doc_ -> createElement(X(id.c_str()));
    parent_element -> appendChild( child_element );
  } else
    new_element = false;

  return child_element ;
}

//------------------------------------------------------------------
DOMElement* XMLDOMParser::addChildElement(DOMElement* parent_element, XMLIdentifier& id)
{
  DOMElement* child_element = doc_ -> createElement(X(id.c_str()));
  parent_element -> appendChild( child_element );
  return child_element ;
}

//------------------------------------------------------------------
DOMAttr* XMLDOMParser::getAttribute(const DOMElement* element, XMLIdentifier& id)
{
  DOMNamedNodeMap* attributes = element -> getAttributes();
  if( !attributes )
    return 0;

  return dynamic_cast<DOMAttr*>(attributes -> getNamedItem( X(id.c_str()) ));
}

//------------------------------------------------------------------
DOMAttr* XMLDOMParser::addAttribute(XS::DOMElement* element, 
                                    XMLIdentifier& id,
                                    const std::string value)
{
  DOMAttr* attr = getAttribute(element,id);
  if( !attr && !value.empty() ) {
    attr = doc_ -> createAttribute(X(id.c_str()));
    element -> setAttributeNode( attr );
  } else {
    throw EXC(IOException) << "We can not add an XML Attribute with an empty value" << Exception::endl;
  }
  attr -> setValue(X(value));
  return( attr );
}
