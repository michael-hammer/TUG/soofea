#ifndef x_str_h___
#define x_str_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <xercesc/util/XMLString.hpp>

XERCES_CPP_NAMESPACE_USE

//---------------------------------------------------------------------------
/** 
    This is a simple class that lets us do easy (though not terribly efficient)
    trancoding of char* data to XMLCh data and vice versa.
 */
class XStr
{
public :
  /**
   */
  inline XStr(const char* const toTranscode) :
    native_form_(0)
  {unicode_form_ = XMLString::transcode(toTranscode);}

  /**
   */
  inline XStr(const std::string& toTranscode) :
    native_form_(0)
  {unicode_form_ = XMLString::transcode(toTranscode.c_str());}

  /**
   */
  inline XStr(const XMLCh* const toTranscode) :
    unicode_form_(0)
  {native_form_ = XMLString::transcode(toTranscode);}

  inline ~XStr()
  {
    if( unicode_form_ )
      XMLString::release(&unicode_form_);
    if( native_form_ )
      XMLString::release(&native_form_);
  }
  
  /**
   */
  inline const XMLCh* unicodeForm() const
  {return unicode_form_;}
  
  /**
   */
  inline const char* nativeForm() const
  {return native_form_;}
private:
  XMLCh* unicode_form_;
  char* native_form_;
};

#define X(str) XStr(str).unicodeForm()
#define XX(str) XStr(str).nativeForm()

#endif // x_str_h___
