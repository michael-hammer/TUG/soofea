#ifndef logger_h___
#define logger_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <sstream>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/Category.hh>

#include "boost/filesystem.hpp"
#include <iostream>
namespace fs = boost::filesystem; 

#include <control_sig.h>

#define LOG(priority) Logger::log(priority,__PRETTY_FUNCTION__)

//------------------------------------------------------------------
/**
   Interface to log4cpp
 */
class Logger : 
  public log4cpp::Category
{
public:
  static const ControlSig endl;

  /**
   * Predefined Levels of Priorities. These correspond to the
   * priority levels used by syslog(3) and log4cpp
   **/
  typedef enum {FATAL  = 0,
		ALERT  = 100,
		CRIT   = 200,
		ERROR  = 300, 
		WARN   = 400,
		NOTICE = 500,
		INFO   = 600,
		DEBUG  = 700,
		NOTSET = 800
  } PriorityLevel;

  /**
     Creates Singleton Logger and log- file with specific name.
   */
  static Logger* createLogger(const fs::path& output_file_name,
			      Logger::PriorityLevel max_priority);

  static void terminate();

  static inline Logger* getInstance()
  { return(instance_); }

  virtual ~Logger();

  static void open(const fs::path& log_file_path,
		   Logger::PriorityLevel max_priority);

  log4cpp::PatternLayout* createLayout();

  //  using log4cpp::Category::operator<<;
  /**
   * Return a CategoryStream with given Priority.
   * @param priority The Priority of the CategoryStream.
   * @returns The requested CategoryStream.
   **/
  template<class T>
  Logger& operator<<(const T& t);

  using log4cpp::Category::log;
  static Logger& log(Logger::PriorityLevel priority, const char method_name[] = 0);

  using log4cpp::Category::setPriority;
  static inline void setPriority(Logger::PriorityLevel max_priority)
  {dynamic_cast<log4cpp::Category*>(instance_)->setPriority(max_priority);}

  static const std::string& resolveLevel(PriorityLevel level);
  static PriorityLevel resolveLevel(const std::string& level_string);

  static const std::string FATAL_STR_;  
  static const std::string ALERT_STR_;  
  static const std::string CRIT_STR_;   
  static const std::string ERROR_STR_;   
  static const std::string WARN_STR_;   
  static const std::string NOTICE_STR_; 
  static const std::string INFO_STR_;   
  static const std::string DEBUG_STR_;  
  static const std::string NOTSET_STR_;  

protected:
  Logger(const fs::path& output_file_name,
	 Logger::PriorityLevel max_priority);	

  static Logger* instance_;
  static std::stringstream log_stream_;
  static Logger::PriorityLevel prior_level_;
};

//------------------------------------------------------------------
template<> Logger& Logger::operator<<(const ControlSig& t);

//------------------------------------------------------------------
template<class T>
Logger& Logger::operator<<(const T& t)
{ 
  log_stream_ << t;
  return ( *this );
}

#endif // logger_h___
