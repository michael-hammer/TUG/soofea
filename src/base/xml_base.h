#ifndef xml_base_h___
#define xml_base_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "xml_dom_parser.h"
#include "parser_base.h"
#include "numbered_object.h"
#include "dynamic_property.h"
#include "id_object.h"
#include "xml_base_constants.h"

//------------------------------------------------------------------
class XMLBase :
  public ParserBase,
  public XMLBaseConstants
{
public:
  XMLBase(XMLDOMParser* parser):
    parser_(parser) {}

  ~XMLBase() {}
protected:
  XMLDOMParser* parser_;

  const fs::path getFilePath()
  { return( parser_->file_path_ ); }

  /**
     Find first DOMElement* in container
   */
  XS::DOMElement* findFirst( const DOMElement* container,
                             const XMLIdentifier& id );

  /**
     Find next DOMElement* for iteratation
   */
  XS::DOMElement* findNext( const XS::DOMElement* container,
                            const XMLIdentifier& id,
                            int& index );

  /**
     Find DOMElement* with given index in the corrsponding container
   */
  XS::DOMElement* find( const XS::DOMElement* container,
			const XMLIdentifier& id,
			int index );

  XS::DOMElement* getTreeElement(DOMElement* parent_element, 
				 const std::vector<const std::string*> &tag_tree);

  /**
     Add DOMElement* with id and given number, new_element is set to
     "true" if the element is newly created. The container is a direct
     child of the root and is also created if not found.
   */
  XS::DOMElement* addNumberedElementToContainer( XS::DOMElement* container,
						 const XMLIdentifier& element_id,
						 numbered number,
						 bool& new_element );

  XS::DOMElement* addNumberedElementToContainer( XS::DOMElement* container,
						 const XMLIdentifier& element_id,
						 numbered number );

  /**
     Find a single Entry

     @return DOMElement of the entry or 0 if it couldn't be found and create == false
     @param create if true the container is created if not existent
   */
  XS::DOMElement* getChild(const XMLIdentifier& id, 
                           bool create = false, 
                           XS::DOMElement* father = 0);

  void removeChild( XS::DOMElement* child,
                    XS::DOMElement* father = 0);

  XS::DOMAttr* getAttribute(XS::DOMElement* element,
                            const XMLIdentifier& id,
                            std::string value = "");

  inline void addAttribute(XS::DOMElement* element,
                           const XMLIdentifier& id,
                           std::string value)
  { getAttribute(element,id,value); }

  void addProperties( XS::DOMElement* xml_element, DynamicProperty& container );

  /**
     Helper function which extracts the number of the actuel Object
   */
  numbered getNumber(const XS::DOMElement* next_element);

  /**
     Helper function which returns the number of an other
     NumberedObject referenced to. Return 0 if the Object wasn't
     found. We start to number the objects with 1!
   */
  numbered getReferenceNumber(const XS::DOMElement* next_element, 
			      XMLIdentifier& attr_id);

  /**
     Helper function which returns the vector of other
     NumberedObjects referenced to. Return 0 if the List wasn't
     found. We start to number the objects with 1!     
   */
  std::vector<numbered>* getReferenceNumberVector(const XS::DOMElement* next_element,
						  XMLIdentifier& attr_id);

  /**
     Helper function which returns the bool value of an attribute
     part of the actual element or false if attribute can't be found
     
     @return If attribute value is not included in true_value_strings
     we deliver false as result -> might not be expected ...
   */
  bool getBool(const XS::DOMElement* next_element,      
	       XMLIdentifier& attr_id,
	       bool& found);

  /**
     Helper function which returns the int value of an attribute
     part of the actual element or 0 if attribute can't be found
   */
  int getInt(const XS::DOMElement* next_element,
	     XMLIdentifier& attr_id,
	     bool& found);

  /**
     Helper function which returns the values of a vector attribute
     which contains integers or NULL if attribute can't be found
   */
  std::vector<int>* getIntVector(const XS::DOMElement* next_element,
				 XMLIdentifier& attr_id);

  /**
     Helper function which returns the double value of an attribute
     part of the actual element or 0.0 if attribute can't be found
   */
  double getDouble(const XS::DOMElement* next_element, 
		   XMLIdentifier& attr_id,
		   bool& found);

  /**
     Helper function which returns the double values of a vector
     attribute part of the actual element or NULL if attribute can't be
     found
   */
  std::vector<double>* getDoubleVector(const XS::DOMElement* next_element, 
				       XMLIdentifier& attr_id);

  /**
     Helper function which returns the string value of an attribute
     part of the actual element or "" if attribute can't be found
   */
  std::string getString(const XS::DOMElement* next_element, 
			XMLIdentifier& attr_id,
			bool& found);
};


#endif
