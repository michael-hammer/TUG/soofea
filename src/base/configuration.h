#ifndef configuration_h___
#define configuration_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <cstring>
#include <vector>
#include <iostream>
#include <cstdlib>

#include "exceptions.h"
#include "version.h"

#include "base/id_object.h"
#include "xml_base.h"
#include "logger.h"

#include "boost/filesystem.hpp"
namespace fs = boost::filesystem; 

//------------------------------------------------------------------
/**
   A Singleton class which provides the configuration for the whole
   binary. Here you get all data which is configurable by the user.
 */
class Configuration :
  public XMLBase
{
public:
  virtual ~Configuration();

  /**
     Creates the Configuration singleton

     @param configuration_file the file name string from where we
     parse the configuration
   */
  static Configuration& createConfiguration(XMLDOMParser* parser);

  /**
     @return a Configuration& to the Singleton
   */
  static Configuration& getInstance()
  {return( *instance_ );}

  static bool hasInstance()
  {return( instance_ != 0 );}

  /**
     Destroys the Configuration singleton and finally writes out all 
     the configuration data. That means data modified gets
     automatically stored!
   */
  static void terminate();

  /**
     @return the file name (including path) of the input file
     for the IO library.
   */
  fs::path getIOInputFN();

  /**
     @return the file type of the input file for the IO library
   */
  IDObject::ID getIOInputType();

  bool outputSameAsInputFile();

  /**
     @return the file name base (including path) of the output file(s)
     for the IO library
   */
  fs::path getIOOutputFN();

  /**
     @return the file type of the output file for the IO library
   */
  IDObject::ID getIOOutputType();

  /**
     @return true if the output file(s) should be written. true is default if
     the value isn't provided by the input file
   */
  IDObject::ID getWrite();

  /**
     This sets the parameters for the output file for the calculation. 
     File name <b>and</b> file type have to be provided simultaneously.

     @param fn file name (including path) of the input file for 
     the IO library
     @param io_type file type of the input file for the IO library
     @param write_state sets how often results are written to output
   */
  void setIOOutput(const std::string& fn, IDObject::ID io_type, IDObject::ID write_state);

  /**
     @return the file name (including path) of the configuration file for
     the numerical integrator.
   */
  fs::path getMathNumIntConfigFN();

  /**
     @param fn is the file name (including path) of the configuration file for
     the numerical integrator.
   */
  void setMathNumIntConfigFN(const std::string& fn);

  /**
     @return the maximal reported Logging Level. See therefore enum 
     Logger::PriorityLevel
   */
  Logger::PriorityLevel getLoggerPriorityLevel();

  /**
     @return the file name (including path) of the logfile
   */
  fs::path getLogFN();

  /**
     @return a vector containing [x_min,x_max,y_min,x_max] in mm
   */
  std::vector<double> getFigureLimits();

protected:
  static Configuration* instance_;
  XS::DOMElement* configuration_root_;

  static const fs::path default_num_int_path_;
  static const fs::path default_log_path_;
  static const Logger::PriorityLevel default_log_level_;

  Configuration(XMLDOMParser* parser);

  void outputFile( bool& same_as_input, fs::path& file_path , IDObject::ID& file_type , IDObject::ID& write_state );
  void numIntFile( fs::path& file_path );
  void loggingFile( fs::path& file_path , Logger::PriorityLevel& log_level );
};

#endif // configuration_h___
