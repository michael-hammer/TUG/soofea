/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "xml_base.h"

//------------------------------------------------------------------
numbered XMLBase::getReferenceNumber(const DOMElement* next_element,
                                     XMLIdentifier& attr_id)
{
  numbered ref_number = 0; 
  DOMAttr* temp = parser_ -> getAttribute(next_element,
                                          attr_id);
  if( temp )
    ref_number = atol( XX( temp -> getValue()) );
  return( ref_number );
}

//------------------------------------------------------------------
std::vector<numbered>* XMLBase::getReferenceNumberVector(const DOMElement* next_element,
                                                         XMLIdentifier& attr_id)
{
  std::string value_list;
  std::vector<numbered>* number_vector = 0;
  DOMAttr* temp = parser_ -> getAttribute(next_element,
                                          attr_id);
  if( temp ) {
    value_list = XX( temp -> getValue());
    number_vector = new std::vector<numbered>;
    extractVectorFromString(value_list,*number_vector);
  }
  return( number_vector );
}

//------------------------------------------------------------------
bool XMLBase::getBool(const DOMElement* next_element,      
                      XMLIdentifier& attr_id,
                      bool& found)
{
  std::vector<std::string> true_value_strings;
  true_value_strings.push_back("1");
  true_value_strings.push_back("true");
  true_value_strings.push_back("True");
  true_value_strings.push_back("yes");
  true_value_strings.push_back("Yes");
  
  found = false;
  DOMAttr* temp = parser_ -> getAttribute(next_element,
                                          attr_id);
  if( temp ) {
    found = true;
    for( std::vector<std::string>::iterator iter = true_value_strings.begin() ;
	 iter != true_value_strings.end() ;
	 ++iter ) {
      if( XX( temp -> getValue() ) == *iter )
	return( true );
    }
    return( false );
  }
  return( false );
}

//------------------------------------------------------------------
int XMLBase::getInt(const DOMElement* next_element, 
                    XMLIdentifier& attr_id,
                    bool& found)
{
  found = false;
  int value = 0;
  DOMAttr* temp = parser_ -> getAttribute(next_element,
                                          attr_id);
  if( temp )
    {
      value = atoi( XX( temp -> getValue()) );
      found = true;
    }

  return( value );
}

//------------------------------------------------------------------
std::vector<int>* XMLBase::getIntVector(const DOMElement* next_element,
                                        XMLIdentifier& attr_id)
{
  std::string value_list;
  std::vector<int>* vector = 0;
  DOMAttr* temp = parser_ -> getAttribute(next_element,
                                          attr_id);
  if( temp )
    {
      value_list = XX( temp -> getValue());
      vector = new std::vector<int>;
      extractVectorFromString(value_list,*vector);
    }
  return( vector );
}

//------------------------------------------------------------------
numbered XMLBase::getNumber(const DOMElement* next_element)
{
  if( parser_ -> version_ >= 0.2 ) {
    DOMAttr* temp = parser_ -> getAttribute( next_element, NUM );
    if( temp )
      return( atol(XX(temp -> getValue())) );
    else
      throw EXC(IOException) << "Couldn't find the required unique Number for <" 
			     << XX(next_element->getNodeName()) << " N='\?\?'>" << Exception::endl;      
  } else {
    throw EXC(IOException) << "Old numbering format is not implemented!" << Exception::endl;
  }

  return( atol(XX(next_element -> getFirstChild() -> getNodeValue())) );  
}

//------------------------------------------------------------------
double XMLBase::getDouble(const DOMElement* next_element, 
                          XMLIdentifier& attr_id,
                          bool& found)
{
  found = false;
  double value;
  if( attr_id == TEMPERATURE )
    value = 293.15; // better default value for T than 0
  else
    value = 0.;
  DOMAttr* temp = parser_ -> getAttribute(next_element,
                                          attr_id);
  if( temp ) {
    value = atof( XX( temp -> getValue()) );
    found = true;
  }

  return( value );
}

//------------------------------------------------------------------
std::vector<double>* XMLBase::getDoubleVector(const DOMElement* next_element,
                                              XMLIdentifier& attr_id)
{
  std::string value_list;
  std::vector<double>* vector = 0;
  DOMAttr* temp = parser_ -> getAttribute(next_element,
                                          attr_id);
  if( temp ) {
    value_list = XX( temp -> getValue());
    vector = new std::vector<double>;
    extractVectorFromString(value_list,*vector);
  }
  return( vector );
}

//------------------------------------------------------------------
std::string XMLBase::getString(const DOMElement* next_element, 
                               XMLIdentifier& attr_id,
                               bool& found)
{
  found = false;
  std::string value = "";
  DOMAttr* temp = parser_ -> getAttribute(next_element,
                                          attr_id);
  if( temp )
    {
      value = XX( temp -> getValue() );
      found = true;
    }

  return( value );
}

//------------------------------------------------------------------
DOMElement* XMLBase::findFirst( const DOMElement* container,
                                const XMLIdentifier& id )
{
  DOMElement* element = 0;
  element = parser_ -> getFirstChildElement(container, 
                                            id);
  return(element);
}

//------------------------------------------------------------------
void XMLBase::addProperties( XS::DOMElement* xml_element, DynamicProperty& container )
{
  bool found;
  std::string key, property_type_string;
  int itmp; double dtmp;
  int index = 0;

  for( XS::DOMElement* property = findFirst( xml_element, PROPERTY ) ;
       property != 0 ;
       property = findNext( xml_element, PROPERTY, index ) ) {
    key = getString( property, ID , found );
    if( !found )
      throw EXC(IOException) << "You have to provide an `" << ID << "=` for a valid property" << Exception::endl;
    property_type_string = getString( property, TYPE , found );
    if( !found )
      throw EXC(IOException) << "You have to provide an `" << TYPE << "=` for a valid property" << Exception::endl;
    switch( IDObject::resolveID( property_type_string ) ) {
    case IDObject::INT:
      itmp = getInt( property , VALUE , found );
      if( found )
        container.addProperty( key , boost::any((int)itmp) );
      break;
    case IDObject::DOUBLE :
      dtmp = getDouble( property , VALUE , found );
      if( found ) {
        container.addProperty( key , boost::any((double)dtmp) );
      }
      break;
    default:
      throw EXC(IOException) << "Property with type `" << TYPE 
                             << "=` -> `" << property_type_string
                             << "` is not defined!" << Exception::endl;
      break;
    }
  }
}

//------------------------------------------------------------------
DOMElement* XMLBase::findNext( const DOMElement* container,
                               const XMLIdentifier& id,
                               int& index )
{		    
  DOMElement* next_element = 0;
  next_element = parser_ -> getNextChildElement(container, 
                                                id,
                                                index);
  return(next_element);
}

//------------------------------------------------------------------
DOMElement* XMLBase::find( const DOMElement* container,
                           const XMLIdentifier& id,
                           int index )
{
  DOMNodeList* list = container -> getElementsByTagName( X(id) );
  XMLSize_t list_length = list->getLength();
  for( XMLSize_t counter = 0 ; counter < list_length ; ++counter ) {
    if( (int)getNumber( dynamic_cast<DOMElement*>(list->item( counter )) ) == index )
      return( dynamic_cast<DOMElement*>(list->item( counter )) );
  }
  
  return( 0 );
}

//------------------------------------------------------------------
DOMElement* XMLBase::getTreeElement(DOMElement* parent_element, 
                                    const std::vector<const std::string*> &tag_tree)
{
  DOMElement* actual_parent = parent_element;
  for( unsigned count = 0 ; count != tag_tree.size() ; ++count ) { 
    actual_parent = parser_ -> getFirstChildElement( actual_parent , (*tag_tree[count]) );
    if( !actual_parent ) return(0);
  }
  return( actual_parent );
}

//------------------------------------------------------------------
DOMElement* XMLBase::getChild( const XMLIdentifier& id, 
                               bool create, 
                               XS::DOMElement* father)
{
  if( !father )
    father = parser_ -> doc_ -> getDocumentElement();

  DOMElement *container = 0; bool new_element = false;
  if( create ) {
    container = parser_ -> addUniqueChildElement( father , id , new_element );
  } else {
    container = parser_ -> getFirstChildElement( father , id );
  }

  return( container );
}

//------------------------------------------------------------------
void XMLBase::removeChild( XS::DOMElement* child,
                           XS::DOMElement* father)
{
  if( !father )
    father = parser_ -> doc_ -> getDocumentElement();

  DOMNode* child_node = father -> removeChild( child );
  child_node -> release();
}

//------------------------------------------------------------------
XS::DOMAttr* XMLBase::getAttribute(XS::DOMElement* element,
                                   const XMLIdentifier& id,
                                   std::string value)
{
  DOMAttr* attr = 0;
  if( value.empty() ) {
    attr = parser_ -> getAttribute( element, id );
  } else {
    attr = parser_ -> addAttribute( element, id , value );
  }
  return( attr );
}

//------------------------------------------------------------------
DOMElement* XMLBase::addNumberedElementToContainer( DOMElement* container,
                                                    const XMLIdentifier& element_id,
                                                    numbered number )
{
  DOMElement* element = parser_ -> addChildElement( container, element_id );
  parser_ -> addAttribute( element, NUM, toString( number ) );
  return( element );
}

//------------------------------------------------------------------
DOMElement* XMLBase::addNumberedElementToContainer( DOMElement* container,
                                                    const XMLIdentifier& element_id,
                                                    numbered number,
                                                    bool& new_element )
{
  DOMNodeList* list = container -> getElementsByTagName( X(element_id) );
  DOMElement* element = 0, *temp_element = 0;

  XMLSize_t list_length = list -> getLength();
  for( XMLSize_t count = 0 ; count != list_length ; ++count ) {
    temp_element = dynamic_cast<DOMElement*>(list->item(count));
    if( getNumber( temp_element ) == number )
      element = temp_element;
  }

  if( !element ) {
    element = parser_ -> addChildElement( container, element_id );
    new_element = true;
    parser_ -> addAttribute( element, NUM, toString( number ) );
  }

  return( element );
}

