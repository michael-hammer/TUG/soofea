#ifndef exception_h___
#define exception_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <control_sig.h>
#include <boost/shared_ptr.hpp>
#include <string>
#include <sstream>

#include "base/logger.h"

//------------------------------------------------------------------
class Exception 
{
public:
  static const ControlSig endl;

  explicit Exception(const char* method_name,
                     std::string error_statement) throw();

  inline Exception(const Exception& exc) throw() :
    method_name_(exc.method_name_),
    error_statement_(exc.error_statement_) {}

  inline const std::string msg() const
  { return( error_statement_->str() ); }

  virtual const std::string what() const throw()
  { return( this -> error_statement_ -> str() ); }

protected:
  const char* method_name_;
  boost::shared_ptr<std::stringstream> error_statement_;
};

#endif
