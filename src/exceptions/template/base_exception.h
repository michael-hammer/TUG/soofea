#ifndef base_exception_h___
#define base_exception_h___
  
/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <sstream>

#include "../exception.h"
#include "base/logger.h"

//------------------------------------------------------------------
template<class T>
class BaseException : 
  public Exception
{
public:
  explicit BaseException(const char* method_name,
			 std::string error_statement) throw() :
    Exception( method_name, error_statement ) {}
  
  inline BaseException(const BaseException& exc) throw() :
    Exception(exc) {}

  virtual inline ~BaseException() throw() {}

  template<class T1>
  T& operator<<(const T1& t);

  T& operator<<(const ControlSig& sig);
};

#include "base_exception.tcc"

#endif
