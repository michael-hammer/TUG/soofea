/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "solver.h"

const fs::path Solver::startup_logging_ = fs::path("soofea_startup.log");

//------------------------------------------------------------------
void Solver::run()
{
  if( !model_handler_ ) {
    std::cout << "Sorry - You have to create a ModelHandler first" << std::endl;
    return;
  }

  // We start a new calculation - therfore we clear the Archive
  model_handler_->getModel().clearArchive();

  Analysis* analysis = 0;
  IDObject::ID analysis_id = model_handler_->getModel().getGlobal().getAnalysisType();
  switch( analysis_id ) {
  case Analysis::LINEAR_ANALYSIS:
    analysis = new LinearAnalysis(*model_handler_);
    break;
  case Analysis::NEWTON_RAPHSON:
    analysis = new NewtonRaphsonAnalysis(*model_handler_);
    break;
  case Analysis::MORTAR_FIXED_POINT:
    analysis = new MortarAnalysis(*model_handler_, analysis_id);
    break;
  default:
    throw EXC(IOException) << "Sorry analysis type '" 
                           << IDObject::resolveID( analysis_id )
                           << "' not known by SOOFEA" << Exception::endl;
  }

  try {
    (*analysis)(); // We do all the analysis
  } catch( const Exception& exc) {
    std::cout << "Caught Exception : " << exc.what() << std::endl;
  }

  if( Configuration::getInstance().getWrite() == IDObject::FINAL_WRITE ) {
    model_handler_ -> write();
  }

  if( Configuration::getInstance().getWrite() != IDObject::NO_WRITE ) {
    model_handler_ -> flush();
  }

  delete analysis;
}

//------------------------------------------------------------------
ModelHandler* Solver::createModelHandler( const std::string file_name, IDObject::ID file_type, bool input_file )
{
  bool create_output_handler = false;
  IDObject::ID output_type = IDObject::NAID;
  fs::path output_fn;
  Logger::createLogger(startup_logging_,Logger::NOTICE);

#if BOOST_FILESYSTEM_VERSION == 3
  fs::path file_path = fs::absolute( fs::path( file_name.c_str()) );
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
  fs::path file_path = fs::complete( fs::path( file_name.c_str()) );
#endif

  if( model_handler_ ) {
    throw EXC(IOException) << "The ModelHandler can be created only once" << Exception::endl;
  }

  InputHandler* input_handler = 0;      OutputHandler* output_handler = 0;
  XMLDOMParser* xml_input_parser = 0;   XMLDOMParser*  xml_output_parser = 0;

  if( input_file ) {
    switch( file_type ) {
    case IDObject::XML :
      xml_input_parser = new XMLDOMParser( std::string("soofea") , XML_SOOFEA_VERSION );
      xml_input_parser -> open( file_path, XML_SOOFEA_VERSION );
      Configuration::createConfiguration( xml_input_parser );
      if( Configuration::getInstance().getWrite() ) {
        create_output_handler = true;
        output_type = Configuration::getInstance().getIOOutputType();
        output_fn = Configuration::getInstance().getIOOutputFN();
        if( Configuration::getInstance().outputSameAsInputFile() ) {
          if( !ParserBase::askForOverwrite( file_path.string() ) ) {
            std::cout << "You choose not to overwrite the results in the input file but configuration is set to write into this file!" << std::endl;
            exit(-1);
          }
          xml_output_parser = xml_input_parser;
        }
      }
      input_handler = new XMLInputHandler(xml_input_parser);
      break;
    default:
      throw EXC(IOException) << "Given Input Type `" 
                             << IDObject::resolveID(file_type)
                             << "` is not implemented!" << Exception::endl;
      break;
    }
  } else {
    create_output_handler = true;
    output_type = file_type;
    output_fn = file_path;
  }

  bool write_mesh = true;
  if( create_output_handler ) {
    switch( output_type ) {
    case IDObject::XML:
      if( !xml_output_parser ) { // Could be allready set if input and output parser are the same
        xml_output_parser = new XMLDOMParser( std::string("soofea") , XML_SOOFEA_VERSION );
        
        if( input_file && (file_type == IDObject::XML) ) { // Here we can clone the input
          std::cout << "We clone the input XML file" << std::endl;
          xml_output_parser -> create( xml_input_parser->cloneDOMDocument() );
          write_mesh = false;
        } else {
          xml_output_parser -> create();
        }
        xml_output_parser -> setOutputFileName( output_fn );
      } else {
        write_mesh = false;
      }
      output_handler = new XMLOutputHandler( xml_output_parser, write_mesh );

      if( ! Configuration::hasInstance() ) {
        Configuration::createConfiguration( xml_output_parser );
        Configuration::getInstance().setIOOutput( output_fn.string() , IDObject::XML , IDObject::FINAL_WRITE  );
      }

      break;
#ifdef SOOFEA_ENABLE_GID
    case IDObject::GID :
      // Here we would fail if we have no XML input - on wich parser should we base the Configuration on?
      output_handler = new GiDOutputHandler( output_fn );
      break;
#endif
#ifdef SOOFEA_ENABLE_VTK
    case IDObject::VTK :
      // Here we would fail if we have no XML input - on wich parser should we base the Configuration on?
      output_handler = new VTKOutputHandler( output_fn );
      break;
#endif
    default:
      throw EXC(IOException) << "Given Output Type `" 
                             << IDObject::resolveID(output_type)
                             << "` is not implemented!" << Exception::endl;
      break;
    }
  }

  Logger::open(Configuration::getInstance().getLogFN() ,
               Configuration::getInstance().getLoggerPriorityLevel() );

  model_handler_ = new ModelHandler(input_handler,output_handler);
  Math::NumIntIO::createNumIntIO(Configuration::getInstance().getMathNumIntConfigFN());

  return( model_handler_ );
}

//------------------------------------------------------------------
Solver::~Solver()
{
  Math::NumIntIO::terminate();
  Configuration::terminate();
  Logger::terminate();
  delete model_handler_;
}
