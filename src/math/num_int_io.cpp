/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "num_int_io.h"

using namespace Math;

NumIntIO* NumIntIO::instance_ = 0;

XMLIdentifier NumIntIO::TAG_INT_TYPE("int_type");
XMLIdentifier NumIntIO::TAG_ORDER("order");
XMLIdentifier NumIntIO::TAG_INT_ENTRY("i");
XMLIdentifier NumIntIO::ATTR_TYPE("type");
XMLIdentifier NumIntIO::ATTR_ID("id");
XMLIdentifier NumIntIO::ATTR_POINTS("points");
XMLIdentifier NumIntIO::ATTR_ORDER("order");
XMLIdentifier NumIntIO::ATTR_PRECISION("prec");
XMLIdentifier NumIntIO::ATTR_R("r");
XMLIdentifier NumIntIO::ATTR_S("s");
XMLIdentifier NumIntIO::ATTR_T("t");
XMLIdentifier NumIntIO::ATTR_ALPHA("alpha");
XMLIdentifier NumIntIO::ATTR_MULTIPILICITY("mul");

std::string const Math::NumIntIO::GAUSS_LEGENDRE_STRING = "gauss_legendre";
std::string const Math::NumIntIO::RECTANGULAR_STRING = "rectangular";
std::string const Math::NumIntIO::TRIANGLE_STRING = "triangle";

//------------------------------------------------------------------
NumIntIO& NumIntIO::createNumIntIO(const fs::path& num_int_file)
{
  if( !instance_ )
    instance_ = new NumIntIO(num_int_file);

  return( *instance_ );
}

//------------------------------------------------------------------
NumIntIO::NumIntIO(const fs::path& num_int_file) :
  XMLDOMParser("soofea_num_int",XML_SOOFEA_NUM_INT_VERSION)
{
  open( num_int_file, XML_SOOFEA_NUM_INT_VERSION );
}

//------------------------------------------------------------------
void NumIntIO::terminate()
{ 
  if(instance_) 
    delete instance_; 
  instance_ = 0;
}

//------------------------------------------------------------------
std::vector<IntegrationPoint*>*
NumIntIO::getIntegrationPointsWithPoints(IntegrationBoundaryType int_boundary_type,
					 IntegrationNumericalType int_num_type,
					 std::vector<int>& points_for_each_dimension)
{
  std::vector< DOMNodeList*> data_list_for_each_dimension;
  std::vector<int> precision_for_each_dimension;

  for( unsigned count = 0 ; count != points_for_each_dimension.size() ; ++count )
    {
      precision_for_each_dimension.push_back(0);
      data_list_for_each_dimension.
	push_back( getNodeListForGivenPointAmount( getNodeListForGivenIntegrationType( int_boundary_type,
										       int_num_type ) ,
						   points_for_each_dimension[count],
						   precision_for_each_dimension[count] ) ) ;
    }

  switch( int_boundary_type )
    {
    case RECTANGULAR:
      return( createRectangularIntegrationPoints(data_list_for_each_dimension,
						 points_for_each_dimension) );
      break;
    case TRIANGLE:
      return( createTriangleIntegrationPoints(data_list_for_each_dimension[0]) );
      break;
    case NAB:
      return( 0 );
      break;
    }
  return( 0 );
}

//------------------------------------------------------------------
std::vector<IntegrationPoint*>*
NumIntIO::getIntegrationPointsWithPrecision(IntegrationBoundaryType int_boundary_type,
					    IntegrationNumericalType int_num_type,
					    std::vector<int>& precision_for_each_dimension)
{
  std::vector< DOMNodeList*> data_list_for_each_dimension;
  std::vector<int> points_for_each_dimension;

  for( unsigned count = 0 ; count != precision_for_each_dimension.size() ; ++count )
    {
      points_for_each_dimension.push_back(0);
      data_list_for_each_dimension.
	push_back( getNodeListForGivenPrecision( getNodeListForGivenIntegrationType( int_boundary_type,
										     int_num_type ) ,
						 points_for_each_dimension[count],
						 precision_for_each_dimension[count] ) ) ;
    }

  switch( int_boundary_type )
    {
    case RECTANGULAR:
      return( createRectangularIntegrationPoints(data_list_for_each_dimension,
						 points_for_each_dimension) );
      break;
    case TRIANGLE:
      return( createTriangleIntegrationPoints(data_list_for_each_dimension[0]) );
      break;
    case NAB:
      return( 0 );
      break;
    }
  return( 0 );
}


//------------------------------------------------------------------
DOMNodeList*
NumIntIO::getNodeListForGivenIntegrationType(IntegrationBoundaryType int_boundary_type,
					     IntegrationNumericalType int_num_type)
{
  DOMNodeList* child_list = 0;
  DOMNodeList* node_list = doc_ -> getElementsByTagName( X(TAG_INT_TYPE.c_str()) );
  const std::string* int_num_type_string = 0;

  switch( int_num_type )
    {
    case GAUSS_LEGENDRE:
      int_num_type_string = &GAUSS_LEGENDRE_STRING;
      break;
    }

  for( unsigned count = 0 ; count != node_list->getLength() ; ++count )
    {
      if( *int_num_type_string == XX( node_list
				      -> item(count)
				      -> getAttributes()
				      -> getNamedItem( X(ATTR_TYPE.c_str()) )
				      -> getNodeValue() ) )
	{
	  switch( int_boundary_type )
	    {
	    case RECTANGULAR:
	      if( RECTANGULAR_STRING == XX( node_list -> item(count)
					    -> getAttributes()
					    -> getNamedItem( X(ATTR_ID.c_str()) )
					    -> getNodeValue() ) )
		child_list = node_list -> item(count) -> getChildNodes();
	      break;
	    case TRIANGLE:
	      if( TRIANGLE_STRING == XX( node_list -> item(count)
					 -> getAttributes()
					 -> getNamedItem( X(ATTR_ID.c_str()) )
					 -> getNodeValue() ) )
		child_list = node_list -> item(count) -> getChildNodes();
	      break;
	    case NAB:
	      child_list = 0;
	      break;
	    }
	}
      if( child_list ) return( reduceNodeList(child_list) );
    }
  return(0);
}

//------------------------------------------------------------------
DOMNodeList*
NumIntIO::getNodeListForGivenPointAmount(DOMNodeList* integration_type_list,
					 int points,
					 int& precision)
{
  for ( unsigned count = 0 ; count != integration_type_list->getLength() ; ++count )
    {
      if( points <= atoi( XX( integration_type_list -> item(count)
			      -> getAttributes()
			      -> getNamedItem( X(ATTR_POINTS.c_str()) )
			      -> getNodeValue() ) ) )
	{
	  // Here the first precision found is taken
	  precision = atoi( XX( integration_type_list -> item(count)
				-> getAttributes()
				-> getNamedItem( X(ATTR_PRECISION.c_str()) )
				-> getNodeValue() ) ) ;
	  return( reduceNodeList(integration_type_list -> item(count) -> getChildNodes()) );
	}
    }
  return( 0 );
}

//------------------------------------------------------------------
DOMNodeList*
NumIntIO::getNodeListForGivenPrecision(DOMNodeList* integration_type_list,
				       int& points,
				       int precision)
{
  for ( unsigned count = 0 ; count != integration_type_list->getLength() ; ++count )
    {
      if( precision <= atoi( XX( integration_type_list -> item(count)
				 -> getAttributes()
				 -> getNamedItem( X(ATTR_PRECISION.c_str()) )
				 -> getNodeValue() ) ) )
	{
	  points = atoi( XX( integration_type_list -> item(count)
			     -> getAttributes()
			     -> getNamedItem( X(ATTR_POINTS.c_str()) )
			     -> getNodeValue() ) ) ;
	  return( reduceNodeList(integration_type_list -> item(count) -> getChildNodes()) );
	}
    }
  return( 0 );
}

//------------------------------------------------------------------
bool NumIntIO::IntPointSorter::operator() ( IntegrationPoint* i , IntegrationPoint* j)
{
  unsigned dim = i -> getDimension();
  switch( dim ) {
  case 1:
    return( i->r() < j->r() );
    break;
  case 2:
    if( fabs( i->r() - j->r() ) < 1e-14 ) {
      return( i->s() < j->s() );
    } else {
      return( i->r() < j->r() );
    }
    break;
  case 3:
    if( fabs( i->r() - j->r() ) < 1e-14 ) {
      if( fabs( i->s() - j->s() ) < 1e-14 ) {
        return( i->t() < j->t() );
      } else {
      return( i->s() < j->s() );
      }
    } else {
      return( i->r() < j->r() );
    }    
    break;
  }
  return( true );
}

//------------------------------------------------------------------
std::vector<IntegrationPoint*>*
NumIntIO::createRectangularIntegrationPoints(std::vector<DOMNodeList*>& data_list_for_each_dimension,
					     std::vector<int>& points_for_each_dimension)
{
  std::vector<IntegrationPoint*>* int_point_vector = new std::vector<IntegrationPoint*>;
  IntegrationPoint* int_point = 0;
  double alpha[3] = {0.,0.,0.};
  double r[3] = {0.,0.,0.};
  double sign[3] = {1.,1.,1.};
  int count[3] = {0,0,0};

  switch( points_for_each_dimension.size() )
    {
    case 1:
      for( count[0] = 0 ; (count[0]) != points_for_each_dimension[0] ; ++(count[0]) )
	{
	  if( count[0] % 2 )
	    sign[0] = -1.;

	  alpha[0] = atof( XX( data_list_for_each_dimension[0]
			       -> item(count[0]/2)
			       -> getAttributes()
			       -> getNamedItem( X(ATTR_ALPHA.c_str()) )
			       -> getNodeValue() ) );
	  r[0] = atof( XX( data_list_for_each_dimension[0]
			   -> item(count[0]/2)
			   -> getAttributes()
			   -> getNamedItem( X(ATTR_R.c_str()) )
			   -> getNodeValue() ) );

	  int_point = new IntegrationPoint( alpha[0], sign[0] * r[0] );
	  int_point_vector -> push_back(int_point);

	  sign[0] = 1.;
	}
      break;
    case 2:
      for( count[0] = 0 ; (count[0]) != points_for_each_dimension[0] ; ++(count[0]) )
	{
	  for( count[1] = 0 ; (count[1]) != points_for_each_dimension[1] ; ++(count[1]) )
	    {
	      for( int dim_counter = 0 ; dim_counter != 2 ; ++dim_counter )
		{
		  if( count[dim_counter] % 2 )
		    sign[dim_counter] = -1.;

		  alpha[dim_counter] = atof( XX( data_list_for_each_dimension[dim_counter]
						 -> item(count[dim_counter]/2)
						 -> getAttributes()
						 -> getNamedItem( X(ATTR_ALPHA.c_str()) )
						 -> getNodeValue() ) );
		  r[dim_counter] = atof( XX( data_list_for_each_dimension[dim_counter]
					     -> item(count[dim_counter]/2)
					     -> getAttributes()
					     -> getNamedItem( X(ATTR_R.c_str()) )
					     -> getNodeValue() ) );
		}

	      int_point = new IntegrationPoint( alpha[0] * alpha[1],
						sign[0] * r[0],
						sign[1] * r[1] );
	      int_point_vector -> push_back(int_point);

	      for( int dim_counter = 0 ; dim_counter != 2 ; ++dim_counter )
		{
		  sign[dim_counter] = 1.;
		}
	    }
	  }
    break;
    case 3:
      for( count[0] = 0 ; (count[0]) != points_for_each_dimension[0] ; ++(count[0]) )
	{
	  for( count[1] = 0 ; (count[1]) != points_for_each_dimension[1] ; ++(count[1]) )
	    {
	      for( count[2] = 0 ; (count[2]) != points_for_each_dimension[2] ; ++(count[2]) )
		{
		  for( int dim_counter = 0 ; dim_counter != 3 ; ++dim_counter )
		    {
		      if( count[dim_counter] % 2 )
			sign[dim_counter] = -1.;

		      alpha[dim_counter] = atof( XX( data_list_for_each_dimension[dim_counter]
						     -> item(count[dim_counter]/2)
						     -> getAttributes()
						     -> getNamedItem( X(ATTR_ALPHA.c_str()) )
						     -> getNodeValue() ) );
		      r[dim_counter] = atof( XX( data_list_for_each_dimension[dim_counter]
						 -> item(count[dim_counter]/2)
						 -> getAttributes()
						 -> getNamedItem( X(ATTR_R.c_str()) )
						 -> getNodeValue() ) );
		    }

		  int_point = new IntegrationPoint( alpha[0] * alpha[1] * alpha[2],
						    sign[0] * r[0],
						    sign[1] * r[1],
						    sign[2] * r[2]);
		  int_point_vector -> push_back(int_point);

		  for( int dim_counter = 0 ; dim_counter != 3 ; ++dim_counter )
		    {
		      sign[dim_counter] = 1.;
		    }
		}
	    }
	}
      break;
    }

  std::sort( int_point_vector->begin(), int_point_vector->end() , NumIntIO::IntPointSorter() );
  return( int_point_vector );
}

//------------------------------------------------------------------
std::vector<IntegrationPoint*>*
NumIntIO::createTriangleIntegrationPoints(DOMNodeList* data_list)
{
  std::vector<IntegrationPoint*>* int_point_vector = new std::vector<IntegrationPoint*>;
  IntegrationPoint* int_point = 0;
  double r=0.,s=0.,t=0.,alpha=0.;
  int mul = 0;

  for( unsigned count = 0 ; count != data_list -> getLength() ; ++count )
    {
      r = atof( XX( data_list
		    -> item(count)
		    -> getAttributes()
		    -> getNamedItem( X(ATTR_R.c_str()) )
		    -> getNodeValue() ) );
      s = atof( XX( data_list
		    -> item(count)
		    -> getAttributes()
		    -> getNamedItem( X(ATTR_S.c_str()) )
		    -> getNodeValue() ) );
      t = 1.-(r+s);
      mul = atoi( XX( data_list
		      -> item(count)
		      -> getAttributes()
		      -> getNamedItem( X(ATTR_MULTIPILICITY.c_str()) )
			  -> getNodeValue() ) );

      // IMPORTANT: multiply by 0.5 because weights are given for an integral approximation
      // as follows: F = 0.5 * SUM ( alpha_i * f(r_i,s_i) , i , 0 , n)
      alpha = 0.5 * atof( XX( data_list
			      -> item(count)
			      -> getAttributes()
			      -> getNamedItem( X(ATTR_ALPHA.c_str()) )
			      -> getNodeValue() ) );

      switch( mul )
	{
	case 1:
	  int_point = new IntegrationPoint( alpha,r,s );
	  int_point_vector -> push_back(int_point);
	  break;
	case 3:
	  if( fabs(r - s) < DBL_EPSILON*1.e3 )
	    {
	      int_point = new IntegrationPoint( alpha,r,s );
	      int_point_vector -> push_back(int_point);
	      int_point = new IntegrationPoint( alpha,t,r );
	      int_point_vector -> push_back(int_point);
	      int_point = new IntegrationPoint( alpha,r,t );
	      int_point_vector -> push_back(int_point);
	    }
	  if( fabs(t - r) < DBL_EPSILON*1.e3 )
	    {
	      int_point = new IntegrationPoint( alpha,r,s );
	      int_point_vector -> push_back(int_point);
	      int_point = new IntegrationPoint( alpha,s,r );
	      int_point_vector -> push_back(int_point);
	      int_point = new IntegrationPoint( alpha,t,r );
	      int_point_vector -> push_back(int_point);
	    }
	  if( fabs(t - s) < DBL_EPSILON*1.e3 )
	    {
	      int_point = new IntegrationPoint( alpha,r,s );
	      int_point_vector -> push_back(int_point);
	      int_point = new IntegrationPoint( alpha,s,r );
	      int_point_vector -> push_back(int_point);
	      int_point = new IntegrationPoint( alpha,t,s );
	      int_point_vector -> push_back(int_point);
	    }
	  break;
	case 6:
	  int_point = new IntegrationPoint( alpha,r,s );
	  int_point_vector -> push_back(int_point);
	  int_point = new IntegrationPoint( alpha,s,r );
	  int_point_vector -> push_back(int_point);
	  int_point = new IntegrationPoint( alpha,t,r );
	  int_point_vector -> push_back(int_point);
	  int_point = new IntegrationPoint( alpha,r,t );
	  int_point_vector -> push_back(int_point);
	  int_point = new IntegrationPoint( alpha,t,s );
	  int_point_vector -> push_back(int_point);
	  int_point = new IntegrationPoint( alpha,s,t );
	  int_point_vector -> push_back(int_point);
	  break;
	}
    }
  return( int_point_vector );
}

//------------------------------------------------------------------
std::vector<int>*
NumIntIO::getAmountOfIntegrationPointsForPrecision(IntegrationBoundaryType int_boundary_type,
						   IntegrationNumericalType int_num_type,
						   std::vector<int>& precision_for_each_dimension)
{
  std::vector<int>* points_for_each_dimension = new std::vector<int>;

  for( unsigned count = 0 ; count != precision_for_each_dimension.size() ; ++count )
    {
      points_for_each_dimension -> push_back(0);
      getNodeListForGivenPrecision( getNodeListForGivenIntegrationType( int_boundary_type,
									int_num_type ) ,
				    (*points_for_each_dimension)[count],
				    precision_for_each_dimension[count] );
    }

  return( points_for_each_dimension );
}

//------------------------------------------------------------------
DOMNodeList* NumIntIO::reduceNodeList(DOMNodeList* node_list)
{
  //  std::cout << "length_bevore: " << node_list -> getLength() << std::endl;
  unsigned count = 0;
  while( count < node_list -> getLength() )
    {
      if( node_list -> item(count) -> getNodeType() != DOMNode::ELEMENT_NODE )
	node_list -> item(count) -> getParentNode() -> removeChild( node_list -> item(count) );
      else
	++count;
    }
  //  std::cout << "length_after: " << node_list -> getLength() << std::endl;
  return(node_list);
}

