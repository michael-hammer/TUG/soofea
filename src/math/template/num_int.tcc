/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

namespace Math {
  //------------------------------------------------------------------
  template<class IntPoint,class IntContainer,int N_rank>
  bz::Array<double,N_rank>* NumInt::integrateArray(NumIntFunctorArray<IntPoint,IntContainer,N_rank>& functor,
                                                   const bz::TinyVector<int,N_rank>& size,
                                                   IntContainer& int_container)
  {
    bz::Array<double,N_rank>* integrate_array = 
      new bz::Array<double,N_rank>(size);
    (*integrate_array) = 0.;
    
    // #ifdef SOOFEA_WITH_TBB
    //   tbb::parallel_for( tbb::blocked_range<size_t>(0,int_container.getIntegrationPointAmount()), 
    //                      NumInt::ArrayWorker<IntPoint,IntContainer,N_rank>(integrate_array,functor,int_container) );
    // #else 
    bz::Array<double,N_rank> value_array( integrate_array->shape() );
    value_array = 0.;
    for( Iterator<IntPoint> iter = int_container.Aggregate<IntPoint>::begin() ; 
         iter != int_container.Aggregate<IntPoint>::end() ; 
         ++iter ) {
      functor( value_array, int_container, *iter);
      (*integrate_array) += ( value_array * (iter->getMathIP().alpha())  );
    }
    // #endif
    
    return(integrate_array);
  }
  
#ifdef SOOFEA_WITH_TBB
  //------------------------------------------------------------------
  template<class IntPoint,class IntContainer,int N_rank>
  void NumInt::ArrayWorker<IntPoint,IntContainer,N_rank>::operator()( const tbb::blocked_range<size_t>& range ) const
  {
    bz::Array<double,N_rank> value_array( integration_array_->shape() );
    value_array = 0.;
    bz::Array<double,N_rank> int_array( integration_array_->shape() );
    int_array = 0.;
    
    for( size_t iter = range.begin();
         iter != range.end();
         ++iter ) {
      (*functor_)( value_array, *int_container_, int_container_->Aggregate<IntPoint>::at(iter) );
      int_array += value_array * (int_container_->Aggregate<IntPoint>::get(iter).getMathIP().alpha());
    }
    {
      tbb::queuing_mutex::scoped_lock lock(num_int_mutex);
      *integration_array_ += int_array;
    }
  }
#endif

  //------------------------------------------------------------------
  template<class IntPoint, class IntContainer>
  double NumInt::integrateScalar(NumIntFunctorScalar<IntPoint,IntContainer>& functor, 
                                 IntContainer& int_container)
  {
    double integrate = 0., value = 0.;
    
    for( Iterator<IntPoint> iter = int_container.Aggregate<IntPoint>::begin() ; 
         iter != int_container.Aggregate<IntPoint>::end() ; 
         ++iter ) {
      functor( int_container, *iter , value );
      integrate += value *  ((*iter) -> getMathIP().alpha());
    }
    return( integrate );
  }
}
