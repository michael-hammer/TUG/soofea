#ifndef num_int_functor_scalar_h___
#define num_int_functor_scalar_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

namespace Math {
  //------------------------------------------------------------------
  template <class IntPoint, class IntContainer>
  class NumIntFunctorScalar
  {
  public:
    virtual ~NumIntFunctorScalar() {}
    
    virtual inline void operator()(IntContainer* int_container, IntPoint& int_point, double* value) {};
  protected:
    NumIntFunctorScalar() {}
  };
  
  //------------------------------------------------------------------
  template <class Item, class IntPoint, class IntContainer>
  class TNumIntFunctorScalar :
    public NumIntFunctorScalar<IntPoint,IntContainer>
  {
  public:
    inline TNumIntFunctorScalar(Item* const obj_ptr, void (Item::*fct_ptr)(IntContainer* int_container, IntPoint& int_point, double* value)):
      obj_ptr_(obj_ptr),
      fct_ptr_(fct_ptr) {}
    
    virtual ~TNumIntFunctorScalar() {};
    
    virtual inline void operator()(IntContainer* int_container, IntPoint& int_point, double* value)
    { return( (*obj_ptr_.*fct_ptr_)( int_container, int_point , value ) ); }
  protected:
    Item* obj_ptr_;
    void (Item::*fct_ptr_)(IntContainer* int_container, IntPoint& int_point, double* value);
  };
}

#endif // num_int_functor_scalar_h___
