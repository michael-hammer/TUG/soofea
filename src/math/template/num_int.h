#ifndef num_int_h__
#define num_int_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include "../array.h"
#include "num_int_functor.h"
#include "../int_types.h"
#include "../integration_point.h"

#include "soofea.h"
#ifdef SOOFEA_WITH_TBB
#include "tbb/tbb.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"
#endif

namespace Math {
  //------------------------------------------------------------------
  /**
     NumInt is a class which provides an interface for numerical
     integration of a given Math::Functor. That Math::IntegrationPoint
     vector is thereby created by Math::NumIntIO with the help of a XML 
     file. You have the possibility to integrate over an Math::Array<>.
  */
  class NumInt
  {
  public:
    inline NumInt() {}
    
    virtual inline ~NumInt() {}
    
    template<class IntPoint, class IntContainer>
    static double integrateScalar(NumIntFunctorScalar<IntPoint,IntContainer>& functor, 
                                  IntContainer& int_container);
    
    template<class IntPoint, class IntContainer,int N_rank>
    static bz::Array<double,N_rank>* integrateArray(NumIntFunctorArray<IntPoint,IntContainer,N_rank>& functor, 
                                                    const bz::TinyVector<int,N_rank>& size,
                                                    IntContainer& int_container);
  protected:
#ifdef SOOFEA_WITH_TBB
    static tbb::queuing_mutex num_int_mutex;
    
    template<class IntPoint,class IntContainer,int N_rank>
    class ArrayWorker {
    public:
      ArrayWorker( bz::Array<double,N_rank>* integration_array, 
                   NumIntFunctorArray<IntPoint,IntContainer,N_rank>& functor,
                   IntContainer& int_container ) :
        integration_array_(integration_array),
        functor_(&functor),
        int_container_(&int_container) {}
      
      ArrayWorker( const ArrayWorker& src) :
        integration_array_(src.integration_array_),
        functor_(src.functor_),
        int_container_(src.int_container_) {}
      
      ~ArrayWorker() {}
      
      void operator()( const tbb::blocked_range<size_t>& range ) const;
      
    protected:
      bz::Array<double,N_rank>* integration_array_;
      NumIntFunctorArray<IntPoint,IntContainer,N_rank>* functor_;
      IntContainer* int_container_;
  };
#endif  
};

}

#include "num_int.tcc"

#endif // num_int_h__
