#ifndef num_int_functor_array_h___
#define num_int_functor_array_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

namespace Math {
  //------------------------------------------------------------------
  template <class IntPoint, class IntContainer, int N_rank>
  class NumIntFunctorArray
  {
  public:
    virtual ~NumIntFunctorArray() {}
    
    virtual inline void operator()(bz::Array<double,N_rank>& array, IntContainer& int_container, IntPoint& int_point) {};
  protected:
    NumIntFunctorArray() {}
  };
  
  //------------------------------------------------------------------
  template <class Item, class IntPoint, class IntContainer, int N_rank>
  class TNumIntFunctorArray :
    public NumIntFunctorArray<IntPoint,IntContainer,N_rank>
  {
  public:
    inline TNumIntFunctorArray(Item* const obj_ptr, void (Item::*fct_ptr)(bz::Array<double,N_rank>& array, IntContainer& int_container, IntPoint& int_point)):
      obj_ptr_(obj_ptr),
      fct_ptr_(fct_ptr) {}
    
    virtual ~TNumIntFunctorArray() {};
    
    inline void operator()(bz::Array<double,N_rank>& array, IntContainer& int_container, IntPoint& int_point)
    { (*obj_ptr_.*fct_ptr_)( array, int_container, int_point ); }
  protected:
    Item* obj_ptr_;
    void (Item::*fct_ptr_)(bz::Array<double,N_rank>& array, IntContainer& int_container, IntPoint& int_point);
  };
}

#endif // num_int_functor_array_h___
