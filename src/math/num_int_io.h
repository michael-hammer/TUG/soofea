#ifndef num_int_io_h___
#define num_int_io_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <iostream>
#include <cmath>
#include <cfloat>

#include "base/xml_dom_parser.h"
#include "soofea.h"
#include "version.h"

#define XS XERCES_CPP_NAMESPACE

#include "boost/filesystem.hpp"
#include <iostream>
namespace fs = boost::filesystem; 

#include "int_types.h"
#include "integration_point.h"

namespace Math {

//------------------------------------------------------------------
class NumIntIO :
  public XMLDOMParser
{
public:
  static std::string const GAUSS_LEGENDRE_STRING;
  static std::string const RECTANGULAR_STRING;
  static std::string const TRIANGLE_STRING;

  static NumIntIO& createNumIntIO(const fs::path& num_int_file);
  
  static inline NumIntIO& getInstance()
  { return( *instance_ ); }

  static void terminate();

  inline virtual ~NumIntIO() {}

  /**
     provides you with the IntegraionPoint - you can request an amount of 
     integration points in each cordinate direction

     @param int_boundary_type is the type of the reference area. It may be 
     rectangular or triangle. See Math::IntegrationBoundaryType
     
     @param int_num_type is the numerical type of integration. It may be a
     gauss-legendre integration. See Math::IntegrationNumericalType
     
     @param points_for_each_dimension is a std::vector<int> containing the 
     amount of integration points per coordinate direction
     
     @return a std::vector<IntegrationPoint*> allocated with new. So you are
     responsible for destruction of the vector <b>and</b> the created IntegrationPoint
     themselves
  */
  std::vector<Math::IntegrationPoint*>* 
  getIntegrationPointsWithPoints(IntegrationBoundaryType int_boundary_type,
				 IntegrationNumericalType int_num_type,
				 std::vector<int>& points_for_each_dimension);

  /**
     provides you with the IntegraionPoint - you can request a precision in
     each cordinate direction - i.e. you can claim that a polynom of this order
     may be integrated exactly.
     
     @param int_boundary_type is the type of the reference area. It may be 
     rectangular or triangle. See Math::IntegrationBoundaryType
     
     @param int_num_type is the numerical type of integration. It may be a
     gauss-legendre integration. See Math::IntegrationNumericalType
     
     @param points_for_each_dimension is a std::vector<int> containing the 
     amount of integration points per coordinate direction
       
     @return a std::vector<IntegrationPoint*> allocated with new. So you are
     responsible for destruction of the vector <b>and</b> the created IntegrationPoint
     themselves
  */
  std::vector<Math::IntegrationPoint*>* 
  getIntegrationPointsWithPrecision(IntegrationBoundaryType int_boundary_type,
				    IntegrationNumericalType int_num_type,
				    std::vector<int>& precision_for_each_dimension);

  std::vector<int>*
  getAmountOfIntegrationPointsForPrecision(IntegrationBoundaryType int_boundary_type,
					   IntegrationNumericalType int_num_type,
					   std::vector<int>& precision_for_each_dimension);

protected:
  struct IntPointSorter {
    bool operator() ( IntegrationPoint* i , IntegrationPoint* j);
  };

  NumIntIO(const fs::path& num_int_file);
  static NumIntIO* instance_;

  static XMLIdentifier TAG_INT_TYPE;
  static XMLIdentifier TAG_ORDER;
  static XMLIdentifier TAG_INT_ENTRY;
  static XMLIdentifier ATTR_TYPE;
  static XMLIdentifier ATTR_ID;
  static XMLIdentifier ATTR_ORDER;
  static XMLIdentifier ATTR_POINTS;
  static XMLIdentifier ATTR_PRECISION;
  static XMLIdentifier ATTR_R;
  static XMLIdentifier ATTR_S;
  static XMLIdentifier ATTR_T;
  static XMLIdentifier ATTR_ALPHA;
  static XMLIdentifier ATTR_MULTIPILICITY;
  
  /**
     Delivers childs of <int_type> selected by IntegrationBoundaryType and 
     IntegrationBoundaryType
     
     @param int_boundary_type is the type of the reference area. It may be 
     rectangular or triangle. See Math::IntegrationBoundaryType
     
     @param int_num_type is the numerical type of integration. It may be a
     gauss-legendre integration. See Math::IntegrationNumericalType
     
     @return a DOMNodeList containing all the available integration
     points and precisions for the given int_boundary_type and int_num_type
  */
  XS::DOMNodeList* 
  getNodeListForGivenIntegrationType(IntegrationBoundaryType int_boundary_type,
				     IntegrationNumericalType int_num_type);
  
  /**
     Delivers childs of <order> selected by claimed point amount
     
     @return a DOMNodeList containing the necessaty data for integration
     selected because of the provided integration points
  */
  XS::DOMNodeList* 
  getNodeListForGivenPointAmount(XS::DOMNodeList* integration_type_list, 
				 int points,
				 int& precision);
  
  /**
     Delivers childs of <order> selected by claimed precission
     
     @return a DOMNodeList containing the necessaty data for integration
     selected because of the provided and claimed precision
  */
  XS::DOMNodeList* 
  getNodeListForGivenPrecision(XS::DOMNodeList* integration_type_list, 
			       int& points,
			       int precision);
  
  /**
   */
  std::vector<Math::IntegrationPoint*>* 
  createRectangularIntegrationPoints(std::vector<XS::DOMNodeList*>& data_list_for_each_dimension,
				     std::vector<int>& points_for_each_dimension);
  
  /**
   */
  std::vector<Math::IntegrationPoint*>* 
  createTriangleIntegrationPoints(XS::DOMNodeList* data_list);
  
  /**
   */
  XS::DOMNodeList* reduceNodeList(XS::DOMNodeList* node_list);
  };

} // end namespace Math

#endif // num_int_io_h___
