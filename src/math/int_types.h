#ifndef int_types_h__
#define int_types_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

namespace Math {
  
  /**
     Provides an enumerated id for the kind of reference domain. This may
     be a 1D, 2D or 3D domain with parallel boundaries on the classical intervall
     [-1,+1] in each dimension or a triangle domain.
   */
  typedef enum _IntegrationBoundaryType_ {
    RECTANGULAR, /*!< rectangular domain */
    TRIANGLE, /*!< triangle domain */
    NAB /*!< not a integration boundary */
  } IntegrationBoundaryType;

  /**
     Provides an enumareted if for the numerical type of integration. 
     This may be a Gauss-Legendre Integration.
   */
  typedef enum _IntegrationNumericalType_ {
    GAUSS_LEGENDRE /*!< Gauss Legendre Integration*/
  } IntegrationNumericalType;
  
} // namespace Math

#endif // int_types_h__
