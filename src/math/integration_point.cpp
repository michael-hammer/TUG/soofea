/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "integration_point.h"

using namespace Math;

//------------------------------------------------------------------
IntegrationPoint::IntegrationPoint(double alpha, double r) :
  alpha_(alpha),
  dimension_(1),
  shape_tensor_(0),
  derivative_shape_tensor_(0)
{
  int_coord_.push_back(r);
}

//------------------------------------------------------------------
IntegrationPoint::IntegrationPoint(double alpha, double r, double s) :
  alpha_(alpha),
  dimension_(2),
  shape_tensor_(0),
  derivative_shape_tensor_(0)
{
  int_coord_.push_back(r);
  int_coord_.push_back(s);
}

//------------------------------------------------------------------
IntegrationPoint::IntegrationPoint(double alpha, double r, double s, double t) :
  alpha_(alpha),
  dimension_(3),
  shape_tensor_(0),
  derivative_shape_tensor_(0)
{
  int_coord_.push_back(r);
  int_coord_.push_back(s);
  int_coord_.push_back(t);
}

//------------------------------------------------------------------
IntegrationPoint::IntegrationPoint( const IntegrationPoint &src) :
  alpha_(src.alpha_),
  dimension_(src.dimension_),
  shape_tensor_(src.shape_tensor_),
  derivative_shape_tensor_(src.derivative_shape_tensor_)
{
  for( unsigned dim_counter = 0 ;
       dim_counter != dimension_ ;
       ++dim_counter )
    int_coord_.push_back( src.int_coord_[dim_counter] );
}

//------------------------------------------------------------------
IntegrationPoint::~IntegrationPoint()
{
  if( shape_tensor_ )
    delete shape_tensor_;

  if( derivative_shape_tensor_ )
    delete derivative_shape_tensor_;
}

//------------------------------------------------------------------
void IntegrationPoint::prettyPrint()
{
  std::cout << "Integration Point: " << std::endl;
  std::cout << " r = " << int_coord_[0] << std::endl;
  if(dimension_ > 1) std::cout << " s = " << int_coord_[1] << std::endl;
  if(dimension_ > 2) std::cout << " t = " << int_coord_[2] << std::endl;
}

//------------------------------------------------------------------
bool IntegrationPoint::operator==(const IntegrationPoint& compare)
{
  if( this == &compare ) return( true );

  if( dimension_ != compare.dimension_ ) return( false );

  if( fabs( alpha_ - compare.alpha_ ) > DBL_EPSILON ) return( false );

  for( unsigned count=0 ; count != int_coord_.size() ; ++count )
    if( fabs( int_coord_[count] - compare.int_coord_[count] ) > DBL_EPSILON )
      return( false );

  return( true );
}
