#ifndef array_h___
#define array_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <soofea.h>
#include <blitz/array.h>
#include <cmath>

namespace bz=blitz;
namespace bzt=blitz::tensor;

namespace Math {

extern bz::Array<double,2> identity2x2;
extern bz::Array<double,2> identity3x3;

//------------------------------------------------------------------
template<typename P_numtype, int N_rank>
P_numtype norm(const bz::Array<P_numtype,N_rank>& array, unsigned norm_order = 2)
{
  bz::Array<P_numtype,N_rank> temp( array.shape() );
  switch( norm_order ) {
  case 2: temp = pow2( array ); break;
  case 3: temp = abs( pow3( array ) ); break;
  case 4: temp = pow4( array ); break;
  case 5: temp = abs( pow5( array ) ); break;
  case 6: temp = pow6( array ); break;
  case 7: temp = abs( pow7( array ) ); break;
  case 8: temp = pow8( array ); break;
  }
  return( pow( bz::sum( temp ) , 1./norm_order ) );
}

//------------------------------------------------------------------
template<typename P_numtype>
bz::Array<P_numtype, 2>* shift4DTo2D( const bz::Array<P_numtype, 4>& src4D )
{
  int dim1 = src4D.shape()[0],
    dim2 = src4D.shape()[1],
    dim3 = src4D.shape()[2],
    dim4 = src4D.shape()[3];

  bz::Array<P_numtype, 2>* new2D = 
    new bz::Array<P_numtype, 2>(dim1 * dim2,
				dim3 * dim4);

  for( int count1 = 0 ; count1 < dim2 ; ++count1) {
    for( int count2 = 0 ; count2 < dim4 ; ++count2) {
      (*new2D)( bz::Range(count1*dim1,(count1+1)*dim1-1) ,
		bz::Range(count2*dim3,(count2+1)*dim3-1) ) = 
	src4D(bz::Range(0,dim1-1),count1,
	      bz::Range(0,dim3-1),count2);
    }}

  return( new2D );
}

//------------------------------------------------------------------
/**
   dim1: coordinate index
   dim2: node index
   dim3: lagrange index
 */
template<typename P_numtype>
bz::Array<P_numtype, 2>* shift3DTo2D( const bz::Array<P_numtype, 3>& src3D )
{
  int dim1 = src3D.shape()[0],
    dim2 = src3D.shape()[1],
    dim3 = src3D.shape()[2];

  bz::Array<P_numtype, 2>* new2D = 
    new bz::Array<P_numtype, 2>(dim1 * dim2,
				dim3);

  for( int coord_count = 0 ; coord_count < dim1 ; ++coord_count) {
    for( int node_count = 0 ; node_count < dim2 ; ++node_count) {
      for( int lagr_count = 0 ; lagr_count < dim3 ; ++lagr_count) {
	(*new2D)( lagr_count, node_count * dim1 + coord_count ) = 
	  src3D( coord_count , node_count , lagr_count );
      }
    }
  }

  return( new2D );
}

//------------------------------------------------------------------
template<typename P_numtype>
bz::Array<P_numtype, 1>* shift2DTo1D( const bz::Array<P_numtype, 2>& src2D )
{
  int dim1 = src2D.shape()[0],
    dim2 = src2D.shape()[1];

  bz::Array<P_numtype,1>* new1D = 
    new bz::Array<P_numtype,1>(dim1 * dim2);

  for( int count = 0; count < dim2; ++count)
    (*new1D)(bz::Range(count*dim1,(count+1)*dim1-1)) =
      src2D(bz::Range(0,dim1-1),count);
    
  return( new1D );
}

//------------------------------------------------------------------
template<typename P_numtype>
void kronecker( bz::Array<P_numtype, 2>& src )
{
  bz::TinyVector<int,2> index;
  src = 0.;
  for( int count = 0; count < (src.shape())(0); ++count)
    {
      index = count;
      src(index) = 1;
    }
}

//------------------------------------------------------------------
template<typename P_numtype>
P_numtype trace( bz::Array<P_numtype, 2>& src )
{
  bz::Array<P_numtype, 2> identity(src.shape());
  kronecker(identity);
  return( bz::sum(src*identity) );
}

//------------------------------------------------------------------
/**
   sets tensor to epsilon (Levi-Civita) tensor (see Klingbeil p. 70 ff 
   or http://en.wikipedia.org/wiki/Levi-Civita_symbol)
   a x b = epsilon_{ijk} * a_j * b_k
*/
template<typename P_numtype>
bz::Array<P_numtype, 3>* epsilon()
{
  bz::Array<P_numtype, 3>* epsilon = new bz::Array<P_numtype, 3>(3,3,3);
  (*epsilon) = 0.;
  bz::TinyVector<int,3> index;
  index = 0,1,2;
  for( int count = 0 ; count < 3 ; ++count ) {
    for( int index_counter = 0 ; index_counter < 3 ; ++index_counter )
      index(index_counter) = (index(index_counter) + count )%3;
    (*epsilon)(index) = 1;
  }
  index = 0,2,1;
  for( int count = 0 ; count < 3 ; ++count ) {
    for( int index_counter = 0 ; index_counter < 3 ; ++index_counter )
      index(index_counter) = (index(index_counter) + count )%3;
    (*epsilon)(index) = -1;
  }
  return( epsilon );
}

//------------------------------------------------------------------
/**
   sets tensor to epsilon (Levi-Civita) tensor (see Klingbeil p. 70 ff 
   or http://en.wikipedia.org/wiki/Levi-Civita_symbol) for the 2D case
   a x e_3 = epsilon_{ij} * a_j
*/
template<typename P_numtype>
bz::Array<P_numtype, 2>* epsilon_2D()
{
  bz::Array<P_numtype, 2>* epsilon = new bz::Array<P_numtype, 2>(2,2);
  (*epsilon) = 0.;
  (*epsilon)(0,1) = 1.;
  (*epsilon)(1,0) = -1.;
  return( epsilon );
}

} // end namespace Math

#endif // array_h___
