#ifndef lin_alg_h__
#define lin_alg_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <math.h>
#include <cassert>
#include <algorithm>
#include <cstring>
#include <float.h>

#ifdef MKL_LAPACK
#include <mkl_lapack.h>
#include <mkl_lapacke.h>
#else
#include <lapack.h>
#define COPY_DATA
#endif

#include "array.h"

#include "exceptions.h"
#include "soofea.h"
#include "base/logger.h"
#include "base/id_object.h"

namespace Math {

//------------------------------------------------------------------
class LinAlg:
  public IDObject
{
public:
  LinAlg() {}
  virtual ~LinAlg() {}

  static double det(const bz::Array<double,2>& array)
    throw(MathException);

  /** 
      Cholesky solver for positive definite matrices
   */
  static void choleskySolve(const bz::Array<double,2>& matrix, 
			    const bz::Array<double,1>& disturbance_vector, 
			    bz::Array<double,1>& solution_vector)
    throw(MathException);

  /** 
      LU decomposition solver for general matrices
  */
  static void luSolve(bz::Array<double,2>& matrix, 
		      bz::Array<double,1>& disturbance_vector, 
		      bz::Array<double,1>& solution_vector)
    throw(MathException);

  static void luInverse(const bz::Array<double,2>& array,
			bz::Array<double,2>& array_inverse)
    throw(MathException);

  static void eigen(const bz::Array<double,2>& array,
		    bz::Array<double,1>& eigenvalues,
		    bz::Array<double,2>& eigenvectors)
    throw(MathException);   

  static bool symmetric(const bz::Array<double,2>& array, double& max_diff);

//  /** 
//      returns ln of a positve definite matrix
//  */
//  void ln(const bz::Array<double,2>& source, bz::Array<double,2>& result) const
//    throw(MathLinAlgException);
//
//  /** 
//      returns e to the power of a positve definite matrix
//  */
//  void e_power(const bz::Array<double,2>& source, bz::Array<double,2>& result) const
//    throw(MathLinAlgException);

protected:
  static double *copyData( const double* data, const size_t num );
};

} // end namespace Math

#endif // lin_alg_h__
