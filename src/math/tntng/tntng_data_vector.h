#ifndef tntng_data_vector_h__
#define tntng_data_vector_h__

#include <boost/shared_array.hpp>
#include "tntng_user_types.h"

namespace TNTng{

  //------------------------------------------------------------------
  /**
     Linear data storage which provides an enclosure for the data 
     array in any array in TNTng. It is also responsible for reference
     counting and dextruction of the memory pool which has to be 
     allocated with <b>new</b>
   */
  template<class T>
  class DataVector : public boost::shared_array<T>
  {
  private:
    size_type size_;
    
  public:
    /**
       Creates a storage array with given size

       @param size of the storage array
     */
    explicit DataVector(size_type size);

    /**
       Creates a storage array with given size and uses a given array

       @param size of the storage array
       @param data is a T* array allocated with <b>new[]</b>
     */
    inline DataVector(size_type size, T* data);

    /**
       Creates a <b>shallow copy</b>
     */
    inline DataVector(const DataVector<T>& src);

    /**
       Returns the size of the data array
     */
    inline size_type size() const;
  };

}

// include implementation
#include "tntng_data_vector.tcc"

#endif // tntng_data_vector_h__
