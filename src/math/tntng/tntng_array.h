#ifndef tntng_array_h___
#define tntng_array_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <vector>
#include <boost/shared_array.hpp>
#include "tntng_array1d.h"
#include "tntng_config.h"

#ifdef TNTNG_DEBUG
#include <iostream>
#endif

namespace TNTng
{
  //------------------------------------------------------------------
  /**
     Generic array class for arbitrary dimensions. It provides an
     inheritance basis for any multidimensional array. That's the 
     reason why it has no public constructors.
   */
  template<class T>
  class Array
  {
  private:
    /**
       Calcs the Position of an Element in the data_array. Here we have the 
       dependency on \#TNTNG_ROW_MAJOR resp. \#TNTNG_COLUMN_MAJOR.
       
       @param index the index array
    */
    inline size_type calcArrayDataPosition(size_type* index) const;

  protected:
    /**
       The one-dimensional array used as storage
     */
    Array1D<T> data_array_;

    /**
       Dimension of the array
     */
    dim_type dimension_;

    /**
       Size of the array. That means it's an size_type array with 
       length = dimension_ and each element holds the size of one
       dimension.
     */
    boost::shared_array<size_type> size_;

    /**
       Create a new multidimensional array, <b>without initalizing</b>
       elements.

       @param m size of first dimension
       @param n size of second dimension
       @param o size of third dimension - default is 2 dimensional
       @param p size of fourth dimension - default is 2 dimensional
       @param q size of fifth dimension - default is 2 dimensional
       @param r size of sixth dimension - default is 2 dimensional
     */
    Array(const size_type m, const size_type n, 
	  const size_type o = 0, const size_type p = 0,
	  const size_type q = 0, const size_type r = 0);

    /**
       Create a new multidimensional array, as a view of an existing 
       one-dimensional array stored in <b>ROW_MAJOR</b> resp. 
       <b>COLUMN_MAJOR</b> order (see tntng_config.h). Note that the 
       array has to be allocated with <b>new[]</b>. The array is going 
       to be destructed by TNTng after no longer usage in this or any 
       other shallow copy.

       @param m size of first dimension
       @param n size of second dimension
       @param data the one-dimensional array allocated with <b>new[]</b>
       and the apropriate length
       @param o size of third dimension - default is 2 dimensional
       @param p size of fourth dimension - default is 2 dimensional
       @param q size of fifth dimension - default is 2 dimensional
       @param r size of sixth dimension - default is 2 dimensional
     */
    Array(const size_type m, const size_type n, T* data,
	  const size_type o = 0, const size_type p = 0,
	  const size_type q = 0, const size_type r = 0);

    /**
       Create a new multidimensional array,  initializing array 
       elements to constant specified by argument.  Most often 
       used to create an array of zeros, as in A(m, n, o, 0.0).

       @param m size of first dimension
       @param n size of second dimension
       @param value the constant value to set all elements of the
       new arary to.
       @param o size of third dimension - default is 2 dimensional
       @param p size of fourth dimension - default is 2 dimensional
       @param q size of fifth dimension - default is 2 dimensional
       @param r size of sixth dimension - default is 2 dimensional
     */
    Array(const size_type m, const size_type n, const T& value,
	  const size_type o = 0, const size_type p = 0,
	  const size_type q = 0, const size_type r = 0);

    /**
       Copy constructor. Array data is NOT copied, but shared.
       Thus, in Array2D B(A), subsequent changes to A will
       be reflected in B.  For an indepent copy of A, use
       Array B(A.copy()), or B = A.copy(), instead.
     */
    inline Array(const Array &src);
    
    /**
       Returns the size of the Array.

       @return a size_type array with length equal amount of dimensions.
     */
    inline const size_type* size() const;

    /**
       Returns the content at a given index for any multidimensional
       array.

       @param index size_type array with length dimension and the position 
       in each dimension
     */
    inline T& get(size_type i, size_type j,
		  size_type k = 0, size_type l = 0,
		  size_type m = 0, size_type n = 0);

    /**
       Same as T& getElement(size_type* index, dim_type dimension) but 
       const version.
     */
    inline const T& get(size_type i, size_type j,
			size_type k = 0, size_type l = 0,
			size_type m = 0, size_type n = 0) const;

  public:
    /**
       Provides the type of the stored values. This is most commonly
       used when requiring scalar temporaries in templated algorithms.
     */
    typedef T value_type;

    /**
       Convert 2D array into a regular single-dimensional C pointer. 
       The data order depends on \#TNTNG_COLUMN_MAJOR vs. \#TNTNG_ROW_MAJOR.
       (see tntng_config.h) Most often called automatically when 
       calling low level interfaces that expect things like double* 
       rather than Array2D<dobule>.
    */
    inline operator T*();

    /**
       Same as operator T*() but const version
     */
    inline operator const T*() const;

    /**
       Assign one Array to another. This is a <b>shallow assignment</b> - for a
       deep copy use copy()
     */
    inline Array& operator=(const Array& src);

    /**
       Assign all elements in the array the given data value.
     */
    inline Array& operator=(const T& data);

  };
}

// include implementation
#include "tntng_array.tcc"

#endif // tntng_array_h___
