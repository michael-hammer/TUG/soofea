/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

namespace TNTng
{
  //------------------------------------------------------------------
  template<class T>
  Array2D<T>::Array2D(size_type m,size_type n) :
    Array<T>(m,n)
  {}

  //------------------------------------------------------------------
  template<class T>
  Array2D<T>::Array2D(size_type m, size_type n, const T& value) :
    Array<T>(m,n,value)
  {}

  //------------------------------------------------------------------
  template<class T>
  Array2D<T>::Array2D(size_type m, size_type n, T* data) :
    Array<T>(m,n,data)
  {}

  //------------------------------------------------------------------
  template<class T>
  inline Array2D<T>::Array2D(const Array2D &src) :
    Array<T>( src )
  {}

  //------------------------------------------------------------------
  template<class T>
  inline T& Array2D<T>::operator()(size_type i, size_type j)
  {
    return( this->get(i-1,j-1) );
  }

  //------------------------------------------------------------------
  template<class T>
  inline const T& Array2D<T>::operator()(size_type i, size_type j) const
  {
    return( this->get(i-1,j-1) );
  }

  //------------------------------------------------------------------
  template<class T>
  inline Array2D<T>& Array2D<T>::operator=(const Array2D<T>& src)
  {
    if( this != &src )
      {
	dynamic_cast< Array<T> >(*this) = src;
      }
    return *this;
  }

  //------------------------------------------------------------------
  template<class T>
  inline Array2D<T>& Array2D<T>::operator=(const T& data)
  {
    return *this;
  }


  //------------------------------------------------------------------
  template<class T>
  void Array2D<T>::prettyPrint() const
  {
    for( unsigned i = 0 ; i < (this->size_)[0] ; i++ )
      {
	std::cout << "[ ";
	for( unsigned j = 0 ; j < (this->size_)[1] ; j++ )
	  {
	    std::cout << this->get(i,j) << " ";
	  }
	std::cout << "]" << std::endl;
      }
  }
}
