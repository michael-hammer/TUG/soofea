namespace TNTng
{
  //------------------------------------------------------------------
  template<class T>
  DataVector<T>::DataVector(size_type size) : boost::shared_array<T>(new T[size]),
					      size_(size)
  {}

  //------------------------------------------------------------------
  template<class T>
  DataVector<T>::DataVector(size_type size, T* data) : boost::shared_array<T>(data),
						       size_(size)
  {}

  //------------------------------------------------------------------
  template<class T>
  inline DataVector<T>::DataVector(const DataVector<T>& src) : boost::shared_array<T>(src),
							       size_(src.size())
  {}

  //------------------------------------------------------------------
  template<class T>
  inline size_type DataVector<T>::size() const
  {
    return( size_ );
  }
}
