#ifndef tntng_array1d_h___
#define tntng_array1d_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>
#include "tntng_user_types.h"
#include "tntng_data_vector.h"

//#include "../../exceptions/exceptions.h"
//#include <math.h>

namespace TNTng
{
  //------------------------------------------------------------------
  /**
     One-dimensional numerical array which is linear ordered.
   */
  template <class T>
  class Array1D
  {
  private:
    DataVector<T> data_vector_;
    size_type size_;

  protected:
    /**
       Copies a classical one dimensional C style array
     */
    void pcopy(T* cp_data_array, const T* src_data_array, size_type size) const;

    /**
       Sets array between given borders to data
     */
    void pset(T* data_array_start, T* data_array_end, const T& data) const;

  public:
    /**
       Provides the type of the stored values. This is most commonly
       used when requiring scalar temporaries in templated algorithms.
     */
    typedef T value_type;

    //------------------------------------------------------------------
    // CONSTRUCTORS
    /**
       Constructs a 1D Array with the given amount of entries uninitialised.

       @param size the length of the 1D array
     */
    explicit Array1D(size_type size);

    /**
       Constructs a 1D array with the given amount of entries and
       initialises the array with the given value.

       @param size the length of the 1D array
       @param value single value for initalising
     */
    Array1D(size_type size, const T& value);

    /**
       Constructs a 1D array with the given amount of entries and uses
       data as storage array. data must have the same size and <b>must</b>
       be allocated wit <b>new[]</b>!

       @param size the length of the 1D array
       @param data array created with <b>new[]</b> with correct size for initalising
     */
    inline Array1D(size_type size, T* data);

    /**
       Copyconstructor which creates a simple <b>shallow copy</b> - that
       means the new Array1D<T> holds the same shared_array as the src 
       array! This might be dangerous in case of multiple manipulation of
       the same data.

       @param src the source array
     */
    inline Array1D(const Array1D& src);

    /**
       Creates a <b>deep copy</b> of this instance. This can be a very
       expensive operation! Rare usage is recommended :)
     */
    Array1D copy() const;

    //------------------------------------------------------------------
    // OPERATORS
    /**
       Convert 1D array into a regular onedimensional C pointer.  Most often
       called automatically when calling C interfaces that expect things like
       double** rather than Array1D<dobule>.
    */
    inline operator T*();

    /**
       Same as operator T*() but const version
     */
    inline operator const T*();

    /**
       "C"-style acces to data - index is starting with 0 and ends with size-1
     */
    inline T& operator[](size_type i);

    /**
       "fortran"-style acces to data - index is starting with 1 and ends with size
     */
    inline T& operator()(size_type i);

    /**
       Same as T& operator[](size_type i) but const version
     */
    inline const T& operator[](size_type i) const;

    /**
       Same as T& operator()(size_type i) but const version
     */
    inline const T& operator()(size_type i) const;

    /**
       Assign one Array1D to another. This is a <b>shallow assignment</b> - for a
       deep copy use Array1D copy() const.
     */
    inline Array1D& operator=(const Array1D &src);

    /**
       Assign all elements in the array the given data value.
     */
    inline Array1D& operator=(const T& data);

    //------------------------------------------------------------------
    /**
       Returns the size of the 1D array.
     */
    inline size_type size() const;

    /**
       Simply prints content of array to std::out
     */
    void prettyPrint() const;
  };
}

// include implementation
#include "tntng_array1d.tcc"

#endif // tntng_array1d_h___
