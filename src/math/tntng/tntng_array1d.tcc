/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

namespace TNTng {

  //------------------------------------------------------------------
  template <class T>
  Array1D<T>::Array1D(size_type size) : data_vector_(size),
					size_(size)
  {}

  //------------------------------------------------------------------
  template <class T>
  Array1D<T>::Array1D(size_type size, const T& value) : data_vector_(size),
							size_(size)
  {
#ifdef TNTNG_DEBUG
    std::cout << "Set Array1D<T> of size = " << size_ << " with value = " << value << std::endl;
#endif
    pset( data_vector_.get() , data_vector_.get() + size_ , value);
  }

  //------------------------------------------------------------------
  template <class T>
  inline Array1D<T>::Array1D(size_type size, T* data) : data_vector_(size,data),
							size_(size)
  {}

  //------------------------------------------------------------------
  template <class T>
  inline Array1D<T>::Array1D(const Array1D<T>& src) :
    data_vector_( src.data_vector_ ),
    size_( src.size() )
  {}

  //------------------------------------------------------------------
  template <class T>
  Array1D<T> Array1D<T>::copy() const
  {
    Array1D<T> A( size_ );
    pcopy( A.data_vector_.get() , data_vector_.get() , size_ );
    return A;
  }

  //------------------------------------------------------------------
  template <class T>
  void Array1D<T>::pcopy(T* cp_data_array, const T* src_data_array, size_type size) const
  {
    T* end_pointer = cp_data_array + size;
    while (cp_data_array < end_pointer)
      *cp_data_array++ = *src_data_array++;
  }

  //------------------------------------------------------------------
  template <class T>
  void Array1D<T>::pset(T* data_array_start, T* data_array_end, const T& data) const
  {
    while (data_array_start < data_array_end)
      *data_array_start++ = data;
  }
  
  //------------------------------------------------------------------
  template <class T>
  inline Array1D<T>::operator T*()
  {
    return( data_vector_.get() );
  }

  //------------------------------------------------------------------
  template <class T>
  inline Array1D<T>::operator const T*()
  {
    return( data_vector_.get() );
  }

  //------------------------------------------------------------------
  template <class T>
  inline T& Array1D<T>::operator[](size_type i)
  {
    return( data_vector_[i] );
  }

  //------------------------------------------------------------------
  template <class T>
  inline T& Array1D<T>::operator()(size_type i)
  {
    return( data_vector_[i-1] );
  }

  //------------------------------------------------------------------
  template <class T>
  inline const T& Array1D<T>::operator[](size_type i) const
  {
    return( data_vector_[i] );
  }

  //------------------------------------------------------------------
  template <class T>
  inline const T& Array1D<T>::operator()(size_type i) const
  {
    return( data_vector_[i-1] );
  }

  //------------------------------------------------------------------
  template <class T>
  inline Array1D<T>& Array1D<T>::operator=(const Array1D<T> &src)
  {
    if( this != &src )
      {
	data_vector_ = src.data_vector_;
	size_ = src.size_;
      }
    return *this;
  }

  //------------------------------------------------------------------
  template <class T>
  inline Array1D<T>& Array1D<T>::operator=(const T& data)
  {
    pset( data_vector_.get(), data_vector_.get()+size_, data);

    return *this;
  }

  //------------------------------------------------------------------
  template <class T>
  inline size_type Array1D<T>::size() const
  {
    return( size_ );
  }

  //------------------------------------------------------------------
  template <class T>
  void Array1D<T>::prettyPrint() const
  {
    std::cout << "[ ";
    for( size_type count=0 ; count < size_ ; count++ )
      std::cout << data_vector_[count] << " ";
    std::cout << "]" << std::endl;
  }

} // namespace TNTng
