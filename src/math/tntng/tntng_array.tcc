/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

namespace TNTng
{
  //------------------------------------------------------------------
  template <class T>
  Array<T>::Array(const size_type m, const size_type n,
		  const size_type o, const size_type p,
		  const size_type q, const size_type r) : 
    data_array_(m * n * (o!=0 ? o : 1) * (p!=0 ? o : 1) * (q!=0 ? o : 1) * (r!=0 ? o : 1)),
    dimension_(o==0 ? 2 : (p==0 ? 3 : (q==0 ? 4 : (r==0 ? 5 : 6)))),
    size_(new size_type[dimension_])
  {
    size_[0] = m;
    size_[1] = n;
    if(o!=0) size_[2] = o;
    if(p!=0) size_[3] = p;
    if(q!=0) size_[4] = q;
    if(r!=0) size_[5] = r;

#ifdef TNTNG_DEBUG
    std::cout << "Constructed Array<T> : dimension = " << dimension_ << std::endl;
#endif
  }

  //------------------------------------------------------------------
  template <class T>
  Array<T>::Array(const size_type m, const size_type n, const T& value,
		  const size_type o, const size_type p,
		  const size_type q, const size_type r) : 
    data_array_(m * n * (o!=0 ? o : 1) * (p!=0 ? o : 1) * (q!=0 ? o : 1) * (r!=0 ? o : 1),value),
    dimension_(o==0 ? 2 : (p==0 ? 3 : (q==0 ? 4 : (r==0 ? 5 : 6)))),
    size_(new size_type[dimension_])
  {
    size_[0] = m;
    size_[1] = n;
    if(o!=0) size_[2] = o;
    if(p!=0) size_[3] = p;
    if(q!=0) size_[4] = q;
    if(r!=0) size_[5] = r;

#ifdef TNTNG_DEBUG
    std::cout << "Constructed Array<T> : dimension = " << dimension_ << " and initialized with value = " << value <<std::endl;
#endif
  }

  //------------------------------------------------------------------
  template <class T>
  Array<T>::Array(const size_type m, const size_type n, T* data, 
		  const size_type o, const size_type p,
		  const size_type q, const size_type r) : 
    data_array_(m * n * (o!=0 ? o : 1) * (p!=0 ? o : 1) * (q!=0 ? o : 1) * (r!=0 ? o : 1),data),
    dimension_(o==0 ? 2 : (p==0 ? 3 : (q==0 ? 4 : (r==0 ? 5 : 6)))),
    size_(new size_type[dimension_])
  {
    size_[0] = m;
    size_[1] = n;
    if(o!=0) size_[2] = o;
    if(p!=0) size_[3] = p;
    if(q!=0) size_[4] = q;
    if(r!=0) size_[5] = r;

#ifdef TNTNG_DEBUG
    std::cout << "Constructed Array<T> : dimension = " << dimension_ << std::endl;
#endif
  }

  //------------------------------------------------------------------
  template <class T>
  inline Array<T>::Array(const Array<T> &src) :
    data_array_(src.data_array_),
    dimension_(src.dimension_),
    size_(src.size_)
  {}

  //------------------------------------------------------------------
  template<class T>
  inline const size_type* Array<T>::size() const
  {
    return( size_.get() );
  }

  //------------------------------------------------------------------
  template<class T>
  inline Array<T>::operator T*()
  {
    return &(data_array_[0]);
  }

  //------------------------------------------------------------------
  template<class T>
  inline Array<T>::operator const T*() const
  {
    return &(data_array_[0]);
  }

  //------------------------------------------------------------------
  template<class T>
  inline T& Array<T>::get(size_type i, size_type j,
			  size_type k, size_type l,
			  size_type m, size_type n)
  {
    size_type index[6] = {i,j,k,l,m,n};   
    size_type index_position = calcArrayDataPosition(index);
    return( data_array_[index_position] );    
  }

  //------------------------------------------------------------------
  template<class T>
  inline const T& Array<T>::get(size_type i, size_type j,
				size_type k, size_type l,
				size_type m, size_type n) const
  {
    size_type index[6] = {i,j,k,l,m,n};   
    size_type index_position = calcArrayDataPosition(index);
    return( data_array_[index_position] );    
  }
  
  //------------------------------------------------------------------
  template<class T>
  inline size_type Array<T>::calcArrayDataPosition(size_type* index) const
  {
    size_type data_index = 0;
    size_type subdim_size = 1;
#ifdef TNTNG_ROW_MAJOR
    for( dim_type dim_counter = 0 ; dim_counter < dimension_ ; dim_counter++ )
      {
	subdim_size = 1;
	for( dim_type subdim_counter = (dim_counter + 1) ; subdim_counter < dimension_ ; subdim_counter++ )
	    {
	      subdim_size *= size_[subdim_counter];
	    }
	data_index += index[dim_counter] * subdim_size;
      }
#endif   
#ifdef TNTNG_COLUMN_MAJOR
    for( dim_type dim_counter = 0 ; dim_counter < dimension_ ; dim_counter++ )
      {
	subdim_size = 1;
	for( dim_type subdim_counter = 0 ; subdim_counter < dim_counter ; subdim_counter++ )
	    {
	      subdim_size *= size_[subdim_counter];
	    }
	data_index += index[dim_counter] * subdim_size;
      }
#endif
    return( data_index );
  }

  //------------------------------------------------------------------
  template<class T>
  inline Array<T>& Array<T>::operator=(const Array<T>& src)
  {
    if( this != &src )
      {
	data_array_ = src.data_array_;
	dimension_ = src.dimension_;
	size_ = src.size_;
      }
    return *this;
  }

  //------------------------------------------------------------------
  template<class T>
  inline Array<T>& Array<T>::operator=(const T& data)
  {
    return *this;
  }

}
