#ifndef tntng_array2d_h___
#define tntng_array2d_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include "tntng_array.h"

namespace TNTng
{
  //------------------------------------------------------------------
  /**
     Two-dimensional numerical array which is linear ordered.
   */

  template<class T>
  class Array2D : public Array<T>
  {
  public:
    //------------------------------------------------------------------
    // CONSTRUCTORS
    /**
       Create a new 2D array, <b>without initalizing</b> elements.

       @param m size of first dimension
       @param n size of second dimension
    */
    Array2D(size_type m, size_type n);

    /**
       Create a new multidimensional array, as a view of an existing 
       one-dimensional array stored in <b>ROW_MAJOR</b> resp. 
       <b>COLUMN_MAJOR</b> order (see tntng_config.h). Note that the 
       array has to be allocated with <b>new[]</b>. The array is going 
       to be destructed by TNTng after no longer usage in this or any 
       other shallow copy.

       @param m size of first dimension
       @param n size of second dimension
       @param data the one-dimensional array allocated with <b>new[]</b>
       and the apropriate length
     */
    Array2D(size_type m, size_type n, T* data);

    /**
       Create a new multidimensional array,  initializing array 
       elements to constant specified by argument.  Most often 
       used to create an array of zeros, as in A(m, n, o, 0.0).

       @param m size of first dimension
       @param n size of second dimension
       @param value the constant value to set all elements of the
       new arary to.
     */
    Array2D(size_type m, size_type n, const T& value);

    /**
       Copy constructor. Array data is NOT copied, but shared.
       Thus, in Array2D B(A), subsequent changes to A will
       be reflected in B.  For an indepent copy of A, use
       Array B(A.copy()), or B = A.copy(), instead.
    */
    inline Array2D(const Array2D &A);

    //------------------------------------------------------------------
    // OPERATORS
    /**
       "fortran"-style acces to data - index is starting with 1 and ends with size
     */
    inline T& operator()(size_type i, size_type j);

    /**
       Same as T& operator()(size_type i, size_type j) but const version
     */
    inline const T& operator()(size_type i, size_type j) const;
    
    /**
       Assign one Array to another. This is a <b>shallow assignment</b> - for a
       deep copy use Array[N]D copy() const in the implementation of an special
       dimension.
     */
    inline Array2D& operator=(const Array2D& src);

    /**
       Assign all elements in the array the given data value.
     */
    inline Array2D& operator=(const T& data);

    //------------------------------------------------------------------
    /**
       Simply prints content of array to std::out
     */
    void prettyPrint() const;
  };
}

// include implementation
#include "tntng_array2d.tcc"

#endif // tntng_array2d_h___



	
