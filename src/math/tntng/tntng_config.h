#ifndef tntng_config_h___
#define tntng_config_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

namespace TNTng
{

//------------------------------------------------------------------
/**
  Matrix are created in the following way:

  A[i][j] = (i+1)*10+(j+1) - for i,j=0...m,n

  that means for e.g. m=3,n=3

  [ 11 12 ] 
  [ 21 22 ]     
  [ 31 32 ]     
                
  in C-style this 3x2 matrix is stored in <b>ROW_MAJOR</b> style :
  
  [ 11 ]            
  [ 12 ]
  [ 21 ]
  [ 22 ]
  [ 31 ]    
  [ 32 ]                        

  in Fortran-style the same 3x2 matrix would be stored as 
  <b>COLUMN_MAJOR</b> style:

  [ 11 ]
  [ 21 ]
  [ 31 ]
  [ 12 ]
  [ 22 ]
  [ 32 ]

  With the following defines you can either set <b>COLUMN_MAJOR</b>
  or <b>ROW_MAJOR</b> as default behaviour. If you want to use
  Fortran based libs like LAPACK you would probably like to use 
  <b>COLUMN_MAJOR</b>
 */

#define TNTNG_COLUMN_MAJOR
  //#define TNTNG_ROW_MAJOR

}

#endif // tntng_config_h___
