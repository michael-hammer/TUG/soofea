/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 *          Manfred Ulz
 * ------------------------------------------------------------------*/

#include "lin_alg.h"

using namespace Math;

//------------------------------------------------------------------
double LinAlg::det(const bz::Array<double,2>& array)
  throw(MathException)
{
  int dimension = array.shape()(0);
  if( dimension != (int)array.shape()(1) )
    throw EXC(MathException) << "You are trying to calculate the determinant of a non square matrix!" << Exception::endl;

  int count = 0;
  int* ipiv = new int[dimension];
  int info = 0;
  int m = dimension;
  int n = dimension;

  double* A = copyData( array.data(), array.numElements() );

  dgetrf_(&m,
          &n,
          A,
          &m,
          ipiv,
          &info);

  double det = 1.;
  for(count = 0; count < dimension ; count++) {
    if(ipiv[count] != (count+1) )
      det = -det * A[ (count * dimension) + count ];
    else
      det = det * A[ (count * dimension) + count ];
  }

  delete[] A;
  delete[] ipiv;
  return(det);
}

//------------------------------------------------------------------
void LinAlg::choleskySolve(const bz::Array<double,2>& matrix,
                           const bz::Array<double,1>& disturbance_vector,
                           bz::Array<double,1>& solution_vector)
  throw(MathException)
{
  int dimension = matrix.shape()(0);
  if(dimension != matrix.shape()(1))
    throw EXC(MathException) << "You are trying to solve a non square linear equation system with cholesky!" << Exception::endl;

  double* A = copyData( matrix.data(),matrix.numElements() );
  double* b = copyData( disturbance_vector.data(),disturbance_vector.numElements() );

  char fact = 'E'; // The matrix A will be equilibrated if necessary, then copied to af and factored.
  char uplo = 'U'; // Upper triangle of A is stored
  int n = dimension; // The number of linear equations
  int nrhs = 1; // the number of columns of the matrices b and x, here b is only a vector   
  int lda = dimension; // The leading dimension of the array A
  double* af = new double[ (dimension * dimension) ]; // AF is an output argument and on exit returns the triangular factor U or L from the Cholesky factorization
  int ldaf = dimension; // The leading dimension of the array AF
  char equed = '0'; // 'N':  No equilibration / 'Y':  Equilibration was done
  double* s = new double[dimension]; // The scale factors for A
  int ldb = dimension; // The leading dimension of the array B
  double* x = new double[dimension]; // the N-by-NRHS solution matrix X to the original system of equations.
  int ldx = dimension; // The leading dimension of the array X
  double rcond = 0.; // The estimate of the reciprocal condition number of the matrix A after equilibration (if done)
  double ferr = 0.; // The estimated forward error bound for each solution vector, here only one value!
  double berr = 0.; // The componentwise relative backward error of each solution vector, here only one value!

  double* work = new double[ 3*dimension ];
  int* iwork = new int[ dimension ];
  int info = 0;

  dposvx_( &fact, &uplo, &n, &nrhs, A,
           &lda, af, &ldaf, &equed,
           s, b, &ldb, x, &ldx,
           &rcond, &ferr, &berr, work,
           iwork, &info );

  solution_vector = bz::Array<double,1>(x,bz::shape(dimension));

  delete[] A;
  delete[] af;
  delete[] s;
  delete[] b;
  delete[] work;
  delete[] iwork;
  delete[] x;

  // LAPACK: INFO = 0: successful termination
  if( (info > 0) || (info < 0) )
    {
      std::cout << "lapack info: " << info << std::endl;
      std::cout << "Determinant of Matrix: " << det(matrix) << std::endl;
//      bz::Array<double,2> eigenvector(dimension,dimension);
//      bz::Array<double,1> eigenvalues(dimension);
//      eigen(matrix, eigenvalues, eigenvector);
//      std::cout << "Eigenvalues of Matrix (have to be positive): " << eigenvalues << std::endl;
    }
  if ( info > 0 )
    throw EXC(MathException) << "Solving of linear algebra system failed! LAPACK::Failure in the course of computation!" << Exception::endl;
  if ( info < 0 )
    throw EXC(MathException) << "Solving of linear algebra system failed! LAPACK::Illegal value of one or more arguments -- no computation performed!" << Exception::endl;
}

//------------------------------------------------------------------
void LinAlg::luSolve(bz::Array<double,2>& matrix,
                     bz::Array<double,1>& disturbance_vector,
                     bz::Array<double,1>& solution_vector)
  throw(MathException)
{
  int dimension = matrix.shape()(0);
  if(dimension != matrix.shape()(1))
    throw EXC(MathException) << "You are trying to solve a non square linear equation system with LU!" << Exception::endl;

  // We copy data because A and b are input AND output
  #ifdef COPY_DATA
  double* A = copyData( matrix.data(),matrix.numElements() );
  double* b = copyData( disturbance_vector.data(),disturbance_vector.numElements() );
  #else
  double* A = matrix.data();
  double* b = disturbance_vector.data();
  #endif
  double* x = solution_vector.data(); // the N-by-NRHS solution matrix X to the original system of equations.

  int n = dimension; // The number of linear equations
  int nrhs = 1; // the number of columns of the matrices b and x, here b is only a vector
  int lda = dimension; // The leading dimension of the array A
  int* ipiv = new int[dimension]; // pivot indices from the factorization
  int ldb = dimension; // The leading dimension of the array B
  int ldx = dimension; // The leading dimension of the array X

  int info = 0;

  #ifdef MKL_LAPACK
  int iter = 0;

  info = LAPACKE_dsgesv(LAPACK_COL_MAJOR, n, nrhs, A, lda, ipiv, b, ldb, x, ldx, &iter);
  #else
  char fact = 'N'; // The matrix A will be equilibrated if necessary, then copied to af and factored.
  char trans = 'N';  // A * X = B     (No transpose)
  double* af = new double[ (dimension * dimension) ]; // AF is an output argument and on exit returns the triangular factor U or L from the Cholesky factorization
  int ldaf = dimension; // The leading dimension of the array AF
  char equed = 'N'; // 'N':  No equilibration
  double* r = new double[dimension]; // The row scale factors for A
  double* c = new double[dimension]; // The scale scale factors for A
  double rcond = 0.; // The estimate of the reciprocal condition number of the matrix A after equilibration (if done)
  double ferr = 0.; // The estimated forward error bound for each solution vector, here only one value!
  double berr = 0.; // The componentwise relative backward error of each solution vector, here only one value!
  double* work = new double[ 4*dimension ];
  int* iwork = new int[ dimension ];

  dgesvx_(&fact, &trans, &n, &nrhs, A,
          &lda, af, &ldaf, ipiv, &equed,
          r, c, b, &ldb, x, &ldx,
          &rcond, &ferr, &berr, work,
          iwork, &info);

  delete[] af;
  delete[] r;
  delete[] c;
  delete[] work;
  delete[] iwork;
  LOG(Logger::INFO) << "Condition Number: " << rcond << Logger::endl;
  #endif

  #ifdef COPY_DATA
  delete[] A;
  delete[] b;
  #endif
  delete[] ipiv;

//  // LAPACK: INFO = 0: successful termination
  if( info ) {
    std::cout << "lapack info: " << info << std::endl;
    std::cout << "Determinant of Matrix: " << det(matrix) << std::endl;
//    bz::Array<double,2> eigenvector(dimension,dimension);
//    bz::Array<double,1> eigenvalues(dimension);
//    eigen(matrix, eigenvalues, eigenvector);
//    std::cout << "Eigenvalues of Matrix: " << eigenvalues << std::endl;
  }  
  if ( info > 0 ) {
    //    assert( 0 );
    throw EXC(MathException) << "Solving of linear algebra system failed! LAPACK::Failure in the course of computation!" << Exception::endl;
  }
  if ( info < 0 ) {
    //    assert( 0 );
    throw EXC(MathException) << "Solving of linear algebra system failed! LAPACK::Illegal value of one or more arguments -- no computation performed!" << Exception::endl;
  }
}

//------------------------------------------------------------------
void LinAlg::luInverse(const bz::Array<double,2>& array,
      		  bz::Array<double,2>& array_inverse)
  throw(MathException)
{
  int dimension = array.shape()(0);
  if(dimension != (int)array.shape()(1))
    throw EXC(MathException) << "You are trying to invert a non square matrix!" << Exception::endl;

  int* ipiv = new int[dimension];
  int info = 0;
  int m = dimension;
  int n = dimension;

  double *A = copyData( array.dataZero(), array.numElements() );
  double* work = new double[1];

  dgetrf_(&m,
          &n,
          A,
          &m,
          ipiv,
          &info);

  if ( info != 0 )
    {
      delete[] A;
      delete[] work;
      delete[] ipiv;
      throw EXC(MathException) << "Inversion of matrix failed! LAPACK::LU factorization of matrix failed!" << Exception::endl;
    }

  int lwork = -1;

  dgetri_( &m,
           A,
           &m,
           ipiv,
           work,
           &lwork,
           &info );

  if ( info > 0 )
    {
      delete[] A;
      delete[] work;
      delete[] ipiv;
      throw EXC(MathException) << "Inversion of matrix failed! LAPACK::INFO = i, U(i,i) is exactly zero; the matrix is singular." << Exception::endl;
    }
  if ( info < 0 )
    {
      delete[] A;
      delete[] work;
      delete[] ipiv;
      throw EXC(MathException) << "Inversion of matrix failed! LAPACK::INFO = -i, the " << -info << "-th argument had an illegal value!" << Exception::endl;
    }

  unsigned size = (unsigned)work[0];
  delete[] work;
  work = new double[size];
  lwork = size;

  dgetri_( &m,
           A,
           &m,
           ipiv,
           work,
           &lwork,
           &info );

  array_inverse = bz::Array<double,2>(A,bz::shape(dimension,dimension));

  delete[] A;
  delete[] work;
  delete[] ipiv;

  if ( info > 0 )
    throw EXC(MathException) << "Inversion of matrix failed! LAPACK::INFO = i, U(i,i) is exactly zero; the matrix is singular." << Exception::endl;
  if ( info < 0 )
    throw EXC(MathException) << "Inversion of matrix failed! LAPACK::INFO = -i, the " << -info << "-th argument had an illegal value!" << Exception::endl;
}

//------------------------------------------------------------------
void LinAlg::eigen(const bz::Array<double,2>& array,
                   bz::Array<double,1>& eigenvalues,
                   bz::Array<double,2>& eigenvector)
  throw(MathException)
{
  int dimension = array.shape()(0);
  if(dimension != (int)array.shape()(1))
    throw EXC(MathException) << "You are trying to calc eigenvalues of a non square coefficient matrix!" << Exception::endl;

  /** @fixme dsbtrd_ is capable to solve band matrix, but kd is set
      to dimension-1 in the following **/
  char vect = 'V';
  char uplo = 'U';
  int n = dimension;
  int kd = dimension-1; // number of superdiagonals
  int ldab = kd+1;
  int ldq = dimension;
  double* AB = new double[ldab*dimension];
  double* D = new double[dimension];
  double* E = new double[dimension-1];
  double* Q = new double[ldq*dimension];
  double* work = new double[dimension];
  int info = -1;

  //    unsigned long pos[] = {0,0};
  for(int count1 = 1; count1 <= dimension; count1++) {
    for(int count2 = count1; count2 <= std::min(kd+1+count1,dimension); count2++) {
      //	    pos[0] = count1-1;
      //	    pos[1] = count2-1;
      AB[(ldab+count1-count2-1)+(count2-1)*std::min(kd+1+count1,dimension)] = array(count1-1,count2-1);
    }
  }

  dsbtrd_(&vect,
          &uplo,
          &n,
          &kd,
          AB,
          &ldab,
          D,
          E,
          Q,
          &ldq,
          work,
          &info);

  // LAPACK: INFO = 0: successful inversion
  if ( info < 0 )
    {
      delete[] AB;
      delete[] D;
      delete[] E;
      delete[] Q;
      delete[] work;
      throw EXC(MathException) << "Reduction to tridiagonal matrix failed!" << Exception::endl;
    }


  char compz = 'V';
  int ldz = dimension;
  delete[] work;
  work = new double[2*dimension-2];
  info = -1;

  dsteqr_(&compz,
          &n,
          D,
          E,
          Q,
          &ldz,
          work,
          &info);

  eigenvalues = bz::Array<double,1>(D,bz::shape(dimension));
  //std::cout << "Eigenvalues: "  << std::endl;
  //eigenvalues.prettyPrint();
  eigenvector = bz::Array<double,2>(Q,bz::shape(dimension,dimension));
  //std::cout << "Eigenvectors: "  << std::endl;
  //eigenvector.prettyPrint();

  // LAPACK: INFO = 0: successful inversion
  if ( info != 0 )
    {
      delete[] AB;
      delete[] D;
      delete[] E;
      delete[] Q;
      delete[] work;
      throw EXC(MathException) << "Reduction to tridiagonal matrix failed!";
    }

  delete[] AB;
  delete[] D;
  delete[] E;
  delete[] Q;
  delete[] work;
}

//  //------------------------------------------------------------------
//  void LinAlg::ln(const bz::Array<double>& source, bz::Array<double>& result) const
//    throw(MathLinAlgException)
//  {
//    // A = V D V'
//    // calculate ln(A) by A = V ln(D) V'
//    // V' is equal to Inverse[V]
//
//    if(source.getSize()[0] != source.getSize()[1])
//      throw MathLinAlgException(MathLinAlgException::MATRIX_IS_NOT_SQUARE);
//    if(result.getSize()[0] != result.getSize()[1])
//      throw MathLinAlgException(MathLinAlgException::MATRIX_IS_NOT_SQUARE);
//    if(source.getSize()[0] != result.getSize()[0])
//      throw MathLinAlgException("\n\nerror: MathLinAlg::Source and result matrix do not coincide!");
//
//    Math::Vector<double> eigenvalues(source.getSize()[0]);
//    Math::Matrix2D<double> eigenvectors(source.getSize());
//    Math::Matrix2D<double> eigenvalues_matrix(source.getSize());
//
//    eigenvalues_eigenvectors(source,
//			     eigenvalues,
//			     eigenvectors);
//
//    for(unsigned long count = 0; count < eigenvalues.getDim() ; count++)
//      {
//	if(eigenvalues.get(&count) < 0 )
//	  throw MathLinAlgException("error: Math::Find roots, sqrt of negative value!");
//      }
//
//    unsigned long pos[] = {0,0};
//    for(unsigned long count = 0; count < source.getSize()[0]; count++)
//      {
//	pos[0] = count;
//	pos[1] = count;
//	eigenvalues_matrix.write(pos, log(eigenvalues.get(&count)) );
//      }
//
//    Math::Matrix2D<double> eigenvectors_trans(source.getSize());
//    Math::Matrix2D<double> res(source.getSize());
//    eigenvectors_trans.trans(eigenvectors);
//    Math::Matrix2D<double> dummy(source.getSize());
//
//    // eigenvectors is in fact V', eigenvectors_trans is V
//    dummy.mul(eigenvectors_trans, eigenvalues_matrix);
//    res.mul(dummy, eigenvectors);
//
//    result = res;
//  }
//
//  //------------------------------------------------------------------
//  void LinAlg::e_power(const bz::Array<double>& source, bz::Array<double>& result) const
//    throw(MathLinAlgException)
//  {
//    // A = V D V'
//    // calculate e(A) by A = V e(D) V'
//    // V' is equal to Inverse[V]
//
//    if(source.getSize()[0] != source.getSize()[1])
//      throw MathLinAlgException(MathLinAlgException::MATRIX_IS_NOT_SQUARE);
//    if(result.getSize()[0] != result.getSize()[1])
//      throw MathLinAlgException(MathLinAlgException::MATRIX_IS_NOT_SQUARE);
//    if(source.getSize()[0] != result.getSize()[0])
//      throw MathLinAlgException("\n\nerror: MathLinAlg::Source and result matrix do not coincide!");
//
//    Math::Vector<double> eigenvalues(source.getSize()[0]);
//    Math::Matrix2D<double> eigenvectors(source.getSize());
//    Math::Matrix2D<double> eigenvalues_matrix(source.getSize());
//
//    eigenvalues_eigenvectors(source,
//			     eigenvalues,
//			     eigenvectors);
//
//    unsigned long pos[] = {0,0};
//    for(unsigned long count = 0; count < source.getSize()[0]; count++)
//      {
//	pos[0] = count;
//	pos[1] = count;
//	eigenvalues_matrix.write(pos, exp(eigenvalues.get(&count)) );
//      }
//
//    Math::Matrix2D<double> eigenvectors_trans(source.getSize());
//    Math::Matrix2D<double> res(source.getSize());
//    eigenvectors_trans.trans(eigenvectors);
//    Math::Matrix2D<double> dummy(source.getSize());
//
//    // eigenvectors is in fact V', eigenvectors_trans is V
//    dummy.mul(eigenvectors_trans, eigenvalues_matrix);
//    res.mul(dummy, eigenvectors);
//
//    result = res;
//  }
//

//------------------------------------------------------------------
double *LinAlg::copyData( const double* data, const size_t num )
{
  double *A = new double[num];
  memcpy( A , data , sizeof(double)*num );
  return( A );
}

//------------------------------------------------------------------
bool LinAlg::symmetric(const bz::Array<double,2>& array, double& max_diff)
{
  if( array.shape()(0) != array.shape()(1) )
    return( false );

  bz::Array<double,2> temp(array.shape());
  temp = 0.;
  temp = array(bzt::i,bzt::j) - array(bzt::j,bzt::i);
  double max = bz::max(temp);
  double min = bz::min(temp);

  if( fabs(max) > DBL_EPSILON * 1.e6 ) {
    max_diff = max;
    return( false );
  }

  if( fabs(min) > DBL_EPSILON * 1.e6 ) {
    max_diff = min;
    return( false );
  }

  if( fabs(max) > fabs(min) )
    max_diff = max;
  else
    max_diff = min;

  return( true );
}
