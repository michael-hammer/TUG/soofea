#ifndef integration_point_h__
#define integration_point_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>
#include <iostream>
#include <cmath>
#include <float.h>

#include "soofea.h"
#include "array.h"

class Type;

namespace Math {

  //------------------------------------------------------------------
  class IntegrationPoint
  {
  public:
    IntegrationPoint(double alpha, double r);
    IntegrationPoint(double alpha, double r, double s);
    IntegrationPoint(double alpha, double r, double s, double t);
    
    virtual ~IntegrationPoint();
    
    IntegrationPoint( const IntegrationPoint &src);
    
    inline unsigned getDimension()
    {return( dimension_ );}
    
    inline double alpha() { return( alpha_ ); }
    inline double r() const { return( int_coord_[0] ); }
    inline double s() const { return( (dimension_>1) ? int_coord_[1] : 0. ); }
    inline double t() const { return( (dimension_>2) ? int_coord_[2] : 0. ); }
    
    bool operator==(const IntegrationPoint& compare);
    
    inline const bz::Array<double,1>* getShapeTensor() const
    { return( shape_tensor_ ); }
    
    inline const bz::Array<double,2>* getDerivativeShapeTensor() const
    { return( derivative_shape_tensor_ ); }
    
    inline void setShapeTensor(bz::Array<double,1>* shape_tensor)
    {shape_tensor_ = shape_tensor;}
    
    inline void setDerivativeShapeTensor(bz::Array<double,2>* derivative_shape_tensor)
    {derivative_shape_tensor_ = derivative_shape_tensor;}
    
    void prettyPrint();
  protected:
    std::vector<double> int_coord_;
    double alpha_;
    unsigned dimension_;
    
    bz::Array<double,1>* shape_tensor_;
    bz::Array<double,2>* derivative_shape_tensor_;
  };
  
} // namespace Math

#endif // integration_point_h__
