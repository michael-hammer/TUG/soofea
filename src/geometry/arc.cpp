/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * $Id$
 * ------------------------------------------------------------------*/

#include "arc.h"

//------------------------------------------------------------------
Arc::Arc() :
  start_point_(0),
  end_point_(0),
  center_point_(0)
{}

//------------------------------------------------------------------
Arc::Arc(Point* start_point, Point* end_point, Point* third_point) :
  start_point_(start_point),
  end_point_(end_point),
  center_point_(0)
{}

//------------------------------------------------------------------
Arc::~Arc()
{
  if( center_point_ )
    delete center_point_;
}

//------------------------------------------------------------------
void Arc::setPoints(Point& start_point,
		    Point& end_point)
{
  start_point_ = &start_point;
  end_point_ = &end_point;
}

//------------------------------------------------------------------
void Arc::calcCenter( const Point& third_point )
{
//  bz::Array<double,1> lambda(2);
//  bz::Array<double,2> coeff_matrix(2,2);
//  bz::Array<double,2> inv_matrix(2,2);
//
//  bz::Array<double,1> a(*end_point_ - *start_point_);
//  bz::Array<double,1> b(third_point - *start_point_);
//  bz::Array<double,1> dist_vec((a-b)*(1/2.));
//
//  Vector* a_vec = new Vector(start_point_ -> getCoordSys(),a);
//  Vector* b_vec = new Vector(start_point_ -> getCoordSys(),b);

//  Vector* n_a = a_vec -> getNormal();
//  Vector* n_b = b_vec -> getNormal();

//  coeff_matrix(0,0) = (*n_b)(0);
//  coeff_matrix(1,0) = (*n_b)(1);
//  coeff_matrix(0,1) = (*n_a)(0);
//  coeff_matrix(1,1) = (*n_a)(1);

//  try {
//    Math::LinAlg lin_alg;
//
//    lin_alg.lu_inverse(coeff_matrix,inv_matrix);
//
//    lambda.mul(inv_matrix,dist_vec);
//  }
//  catch(const MathLinAlgException& exc) {
//    std::cout << exc.msg() << std::endl;
//  }

//  bz::Array<double,1> buffer1((b*(1/2.)) + (*start_point_));
//  bz::Array<double,1> buffer2((*n_b)*(lambda(0)) + buffer1);
//  center_point_ = new Point(buffer2,start_point_ -> getCoordSys());
//
//  bz::Array<double,1> radius_vec( *start_point_ - *center_point_ );
//
//  radius_ = Math::norm( radius_vec );
//
//  delete a_vec;
//  delete b_vec;
//  delete n_a;
//  delete n_b;
}

//------------------------------------------------------------------
Vector* Arc::getNormalVector(const Point& point)
{
  bz::Array<double,1> n_m(point - *center_point_);
  n_m *= 1 / Math::norm(n_m);
  return( new Vector(point.getCoordSys(),n_m) );
}

//------------------------------------------------------------------
void Arc::createSplittingPoints(unsigned amount_of_points,
				Point* start_point,
				Point* end_point,
				std::vector<Point*>& point_vector)
{
//  unsigned count = 0;
//  double beta;
//  bool pos_rotation;
//  unsigned long vector_size[1] = {3};

//  bz::Array<double,1> a(*start_point - *center_point_);
//  bz::Array<double,1> b(*end_point - *center_point_);
//  bz::Array<double,1> temp(2,2);
//  temp.cross(a,b);
//
//  vector_size[0] = 2;
//  if( temp.get(vector_size) > 0 )
//    pos_rotation = true;
//  else
//    pos_rotation = false;
//
//  // What happens if alpha is greater than PI ?
//  double alpha = acos( (a.in(b)) / pow(radius_,2.) );
//  if( !pos_rotation )
//    alpha *= -1.;
//
//  for( count = 1 ; count <= amount_of_points ; ++count )
//    {
//      beta = alpha/(amount_of_points + 1) * count;
//
//      Math::Matrix2D<double> A(size);
//
//      A(0,0) = cos(beta);
//      A(1,0) = sin(beta);
//      A(0,1) = -sin(beta);
//      A(1,1) = cos(beta);
//
//      Math::Vector<double> c(2);
//      c.mul(A,a);
//
//      Point* new_point = new Point( c , start_point -> getCoordSys());
//      point_vector.push_back(new_point);
//    }
}
