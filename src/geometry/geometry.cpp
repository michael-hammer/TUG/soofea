/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "geometry.h"

//------------------------------------------------------------------
Vector* Geometry::calcUnitVector(const Vector& v)
{
  Vector* unit_vector = new Vector( v );
  (*dynamic_cast<bz::Array<double,1>*>(unit_vector)) = v / Math::norm(v);
  return( unit_vector );
}

//------------------------------------------------------------------
void Geometry::normalizeVector(bz::Array<double,1>& v)
{
  v /= Math::norm(v);
}

//------------------------------------------------------------------
double Geometry::calcDistance(const Point& p1, const Point& p2 )
{
  bz::Array<double,1> diff(p1 - p2);
  return( Math::norm(diff) );
}

//------------------------------------------------------------------
Vector* Geometry::calcNormal(const Vector& v1, const Vector& v2, bool normalize, bool slice)
{
  bz::Array<double, 1> v1_3D(3); v1_3D = 0.;
  bz::Array<double, 1> v2_3D(3); v2_3D = 0.;

  if( slice && v1.getCoordSys()->getDimension() < 3 && v2.getCoordSys()->getDimension() < 3)
    throw EXC(CalcException) << "Slicing to 2 dimension makes no sense for cross product if both given vectors are 2D only!" << Exception::endl;

  if( (v1.shape())(0) == 2 ) {
    v1_3D(bz::Range(0,1)) = v1;
  } else {
    v1_3D = v1;
  }

  if( (v2.shape())(0) == 2 ) {
    v2_3D(bz::Range(0,1)) = v2;
  } else {
    v2_3D = v2;
  }

  bz::Array<double,1> normal_3D(3); normal_3D = 0.;
  bz::Array<double,3>* epsilon = Math::epsilon<double>();
  normal_3D = bz::sum( bz::sum( (*epsilon)(bzt::i,bzt::j,bzt::k) * v2_3D(bzt::k) , bzt::k) * v1_3D(bzt::j) , bzt::j );
  delete epsilon;

  if( normalize )
    normalizeVector( normal_3D );

  Vector* normal = 0;
  if( slice ) {
    if( v1.getCoordSys()->getDimension() != 2 )
      throw EXC( CalcException ) << "Wrong dimension of v1 for slicing of cross product result" << Exception::endl;
    normal = new Vector( v1.getCoordSys(),0,0 );
    (*dynamic_cast<bz::Array<double,1>*>(normal)) = normal_3D(bz::Range(0,1));
  } else {
    if( v1.getCoordSys()->getDimension() != 3 )
      throw EXC( CalcException ) << "Wrong dimension of v1 for creation of 3D cross product result" << Exception::endl;
    normal = new Vector( v1.getCoordSys(),0,0,0 );
    (*dynamic_cast<bz::Array<double,1>*>(normal)) = normal_3D;
  }

  return( normal );
}
