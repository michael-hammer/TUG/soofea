#ifndef py__geometry_h__
#define py__geometry_h__

#include "geometry/point.h"

#include <boost/python.hpp>

void export_geometry();

#endif
