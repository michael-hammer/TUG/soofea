#include "geometry.h"

//------------------------------------------------------------------
struct PyVector
{
  static double getCoordinate( const Point& self , CoordSys::CoordID coord_id )
  {
    return( self.getCoordinate( coord_id ) );
  }
};

void export_geometry()
{
  using namespace boost::python;

  class_<Vector>("Vector", no_init)
    .def("getCoordinate",&PyVector().getCoordinate)
    ;

  class_<Point, bases<Vector> >("Point",no_init)
    ;

  {
    scope in_CoordSys = class_<CoordSys>("CoordSys",no_init)
      .def("getDimension",&CoordSys::getDimension)
      ;

    enum_<CoordSys::CoordID>("CoordID")
      .value("X",CoordSys::X)
      .value("Y",CoordSys::Y)
      .value("Z",CoordSys::Z)
      .value("PHI",CoordSys::PHI)
      .value("PSI",CoordSys::PSI)
      .value("THETA",CoordSys::THETA)
      .value("NORMAL",CoordSys::NORMAL)
      .value("TANGENT_R",CoordSys::TANGENT_R)
      .value("TANGENT_S",CoordSys::TANGENT_S)
      .value("NAC",CoordSys::NAC)
      ;
  }
}
