#ifndef geometry_h___
#define geometry_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>

#include "point.h"
#include "vector.h"
#include "exceptions.h"

namespace Geometry {

  double calcDistance(const Point& p1, const Point& p2);

  /**
     @return = v1 x v2
   */
  Vector* calcNormal(const Vector& v1, const Vector& v2, bool normalize = true, bool slice = true);

  Vector* calcUnitVector(const Vector& v);

  void normalizeVector(bz::Array<double,1>& v);
}

#endif
