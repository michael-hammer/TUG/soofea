#ifndef boundary_geometry_h__
#define boundary_geometry_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

class BoundaryGeometry
{
public:
  virtual ~BoundaryGeometry() {};

  //------------------------------------------------------------------
  // This abstract method has to be implemented in the geometry base classes
  // of the boundaries
  virtual void createSplittingPoints(unsigned amount_of_points,
				     Point* start_point,
				     Point* end_point,
				     std::vector<Point*>& point_vector) = 0;
protected:
  BoundaryGeometry() {};
};

#endif
