#ifndef vector_h__
#define vector_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "coord_sys.h"
#include "math/array.h"

#include <iostream>
#include <exceptions.h>

//------------------------------------------------------------------
class Vector : 
  public bz::Array<double,1>
{
public:
  Vector(CoordSys* system, double coord_1, double coord_2 = 0., double coord_3 = 0.);

  Vector(CoordSys* system);

  Vector(CoordSys* system, const bz::Array<double,1>& vector) :
    bz::Array<double,1>(vector.copy()),
    coord_sys_(system) {}
  
  Vector(const Vector& src) :
    bz::Array<double,1>(src.copy()),
    coord_sys_(src.coord_sys_) {}

  Vector& operator=( const Vector& src );

  virtual ~Vector() {}

  inline void setCoordSys(CoordSys* coord_sys)
  { coord_sys_ = coord_sys; }

  inline CoordSys* getCoordSys() const
  { return(coord_sys_); }

  double& getCoordinate(CoordSys::CoordID coord_id)
  { return( (*this)(coordSelector(coord_id)) ); }

  double getCoordinate(CoordSys::CoordID coord_id) const
  { return( (*this)(coordSelector(coord_id)) ); }

  inline double& getCoordinate(unsigned coord_number)
  {return( (*this)(coord_number) );}

  void setCoordinate( double value, CoordSys::CoordID coord_id );

  void prettyPrint() const;
protected:
  CoordSys* coord_sys_;

  unsigned coordSelector(CoordSys::CoordID coord_id) const;
};

#endif // vector_h__
