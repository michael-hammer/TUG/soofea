/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "vector.h"

//------------------------------------------------------------------
Vector::Vector(CoordSys* system, double coord_1, double coord_2, double coord_3) :
  bz::Array<double,1>(system -> getDimension()),
  coord_sys_(system)
{
  (*this)(0) = coord_1;
  if( coord_sys_->getDimension() >= 2 )
  (*this)(1) = coord_2;
  if( coord_sys_->getDimension() >= 3 )
  (*this)(2) = coord_3;
}

//------------------------------------------------------------------
Vector::Vector(CoordSys* system) :
  bz::Array<double,1>(system -> getDimension()),
  coord_sys_(system)
{
  (*this)(bz::Range::all()) = 0.;
}

//------------------------------------------------------------------
unsigned Vector::coordSelector(CoordSys::CoordID coord_id) const
{
  switch(coord_id) {
  case CoordSys::X:
    return( 0 );
    break;
  case CoordSys::Y:
    return( 1 );
    break;
  case CoordSys::Z:
    return( 2 );
    break;
  case CoordSys::PHI:
  case CoordSys::PSI:
  case CoordSys::THETA:
  case CoordSys::NORMAL:
  case CoordSys::TANGENT_R:
  case CoordSys::TANGENT_S:
  case CoordSys::NAC:
    break;
  }

  return( -1 );
}

//------------------------------------------------------------------
void Vector::prettyPrint() const
{
  std::streamsize prec = std::cout.precision();
  switch( coord_sys_ -> getDimension() )
    {
    case 2: 
      std::cout << std::setprecision(12) << "| x = " << (*this)(0);
      std::cout << " y = " << (*this)(1) << std::setprecision(prec) << std::endl;
      break;
    case 3:
      std::cout << "| x = " << (*this)(0);
      std::cout << " y = " << (*this)(1);
      std::cout << " z = " << (*this)(2) << std::endl;
      break;
    }
}

//------------------------------------------------------------------
void Vector::setCoordinate(double value,CoordSys::CoordID coord_id)
{
  unsigned index = coordSelector( coord_id );
  if( index < coord_sys_ -> getDimension() )
    (*this)(index) = value;
}

//------------------------------------------------------------------
Vector& Vector::operator=( const Vector& src )
{
  if( coord_sys_ != src.coord_sys_ )
    throw EXC(CalcException) << "Assignment of Points with different coordinate systems is not supported" << Exception::endl;

  (*dynamic_cast<bz::Array<double,1>*>(this)) = src;
  
  return( *this );
}
