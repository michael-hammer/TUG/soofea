/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "line.h"

//------------------------------------------------------------------
Line::Line() :
  start_point_(0),
  end_point_(0),
  unit_vector_(0)
{}

//------------------------------------------------------------------
Line::Line(Point* start_point, Point* end_point) :
  start_point_(start_point),
  end_point_(end_point),
  unit_vector_(0)
{
  calcUnitVector();
}

//------------------------------------------------------------------
Line::~Line()
{
  if(unit_vector_)
    delete unit_vector_;
}

//------------------------------------------------------------------
Vector* Line::getUnitVector()
{
  if(!unit_vector_)
    calcUnitVector();

  return(unit_vector_);
}

//------------------------------------------------------------------
void Line::calcUnitVector()
{
//  CoordSys* coord_sys = start_point_ -> getCoordSys();

//  switch( coord_sys -> getDimension() )
//    {
//    case 2:
//      unit_vector_ = new Vector((*end_point_ - *start_point_)((unsigned long)0),
//				(*end_point_ - *start_point_)((unsigned long)1),
//				coord_sys);
//      break;
//    case 3:
//      unit_vector_ = new Vector((*end_point_ - *start_point_)((unsigned long)0),
//				(*end_point_ - *start_point_)((unsigned long)1),
//				(*end_point_ - *start_point_)((unsigned long)2),
//				coord_sys);
//      break;
//    }		    
//  unit_vector_ -> scale( *unit_vector_ , 1/( unit_vector_ -> norm() ) );
}

//------------------------------------------------------------------
void Line::setPoints(Point& start_point,
		     Point& end_point)
{
  start_point_ = &start_point;
  end_point_ = &end_point;
}

//------------------------------------------------------------------
void Line::createSplittingPoints(unsigned amount_of_points,
				 Point* start_point,
				 Point* end_point,
				 std::vector<Point*>& point_vector)
{
//  unsigned count = 0;

//  for( count = 1 ; count <= amount_of_points ; ++count )
//    {
//      Math::Vector<double> new_point_vector_1 = *(end_point) * ((double)count/(double)(amount_of_points + 1));
//      Math::Vector<double> new_point_vector_2 = *(start_point) * (1. - (double)count/(double)(amount_of_points + 1));
//      
//      Math::Vector<double> new_point_vector = new_point_vector_1 + new_point_vector_2;
//      Point* new_point = new Point( new_point_vector , start_point -> getCoordSys() );
//      
//      point_vector.push_back(new_point);
//    }
}
