#ifndef coord_sys_h__
#define coord_sys_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <iostream>

#include "base/id_object.h"

//------------------------------------------------------------------
class CoordSys :
  public IDObject
{
public:
  /**
     Enum with all kinds of coordinate identifier
   */
  typedef enum _CoordID_ {
    X, /*!< X (first displacement) coordinate */
    Y, /*!< Y (second displacement) coordinate */
    Z, /*!< Z (third displacement) coordinate */
    PHI, /*!< PHI (first twist) coordinate */
    PSI, /*!< PSI (second twist) coordinate */
    THETA, /*!< THETA (third twist) coordinate */
    NORMAL, /*!< NORMAL direction (for lagrange contact enforcement) */
    TANGENT_R, /*!< TANGENT_R direction (for lagrange contact enforcement) */
    TANGENT_S, /*!< TANGENT_S direction (for lagrange contact enforcement) */
    NAC /*!< NAC = <b>N</b>ot <b>A</b> <b>C</b>oordinate (for instance temperature)*/
  } CoordID;

  /**
     Default Destructor
   */
  ~CoordSys() {}

  /**
     @return CoordSys::SysId of this CoordSys
   */
  inline ID getSysID() const {return(sys_id_);}

  /**
     @return dimension of this CoordSys
   */
  inline unsigned getDimension() const {return(dim_);}
protected:
  /**
     Default constructor
   */
  inline CoordSys(ID sys_id, unsigned dim) :
    sys_id_(sys_id),
    dim_(dim) {}
  
  ID sys_id_;
  unsigned dim_;
};

#endif // coord_sys_h__
