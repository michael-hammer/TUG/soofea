#ifndef solver_h__
#define solver_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>

#include "version.h"
#include "exceptions.h"

#include "base/logger.h"
#include "base/configuration.h"
#include "math/num_int_io.h"

#ifdef SOOFEA_ENABLE_VTK
#include "io/vtk_output_handler.h"
#endif
#ifdef SOOFEA_ENABLE_GID
#include "io/gid_output_handler.h"
#endif
#include "io/xml_output_handler.h"
#include "io/xml_input_handler.h"
#include "io/model_handler.h"

#include "analyzer/analysis/linear_analysis.h"
#include "analyzer/analysis/newton_raphson_analysis.h"
#include "analyzer/analysis/mortar_analysis.h"

//------------------------------------------------------------------
class Solver
{
public:
  static const fs::path startup_logging_;

  Solver() :
    model_handler_(0) {}

  ~Solver();

  void run();

  ModelHandler* createModelHandler( const std::string file_name, IDObject::ID file_type, bool input_file );

  ModelHandler& getModelHandler() {return( *model_handler_ );}

protected:
  ModelHandler* model_handler_;
};

#endif // solver_h__
