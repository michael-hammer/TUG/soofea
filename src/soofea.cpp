/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

/** \mainpage SOOFEA - Software for Object Oriented Finite Element Analysis

\section intro_sec Introduction

<p>This software was written on the <a href="http://www.fest.tugraz.at">
Institute for Strength of Materials </a> at the <a href="http://www.tugraz.at">
Graz University of Technology</a>. The aim is to provide a framework 
for object oriented finite element code written in C++. It's of course 
more an acadamic approach than a commercial one. There was a big
need for us to have our own acadamic code where we can solve problems
the way we want to.</p>

\section install_sec Installation

<p>Until now there is now install routine. We are using CMake as build
environment. That means you can use the steps described in 
<a href="../../INSTALL">INSTALL</a> to compile and use SOOFEA.</p>
 */

#include <string>
#include <iostream>

#include "solver.h"

#include <boost/program_options.hpp>

namespace po = boost::program_options;

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  std::string binary_desc = "SOOFEA - Software for Object Oriented Finite Element Analysis";
  po::variables_map vm;        
  po::options_description desc(binary_desc);

  try {
    desc.add_options()
      ("help,h", "Help message")
      ("version,v", "Version of SOOFEA")
      ("file,f", po::value<std::string>(), "Path to calculation file")
      ;
    po::positional_options_description pos;
    pos.add("file", -1);

    po::store(po::command_line_parser(argc, argv).options(desc).positional(pos).run(), vm);
    po::notify(vm);    

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 1;
    }
    if (vm.count("version")) {
      std::cout << binary_desc << " - v"
		<< VERSION__ << std::endl;
      return 1;
    }

    if (vm.count("file")) {
      std::cout << "Calculation file is '" 
		<< vm["file"].as<std::string>() << "'" << std::endl;
    } else {
      std::cout << "A calculation file hast to be set!" << std::endl;
      std::cout << std::endl << desc << std::endl;
      return 1;
    }
  } catch(std::exception& e) {
    std::cout << "error: " << e.what() << std::endl;
    std::cout << std::endl << desc << std::endl;
    return 1;
  }

  Solver solver;
  ModelHandler* mh;

  try {
    ///@fixme: input file type is only a guess here!
    mh = solver.createModelHandler( vm["file"].as<std::string>(), IDObject::XML, true );
  } catch( const IOException &exc ) {
    std::cout << exc.what() << std::endl;
    std::cout << "Sorry - startup failed! See logfile '"
              << Solver::startup_logging_.string() << "' for further informations!" << std::endl;
    return -1;
  }

  try{
    mh -> read(); // Here we create the Model and ModelProgression
  } catch( const IOException& exc ) {
    std::cout << exc.what() << std::endl;
    std::cout << "Sorry - parsing of input file " << std::endl
              << "\t '" << Configuration::getInstance().getIOInputFN().string() << "'" << std::endl
              << " and creation of Model or ModelProgression failed - see log file" << std::endl 
              << "\t '" << Configuration::getInstance().getLogFN().string() << "'" << std::endl
              << "for further informations!" << std::endl;
    return -1;
  }  
  
  solver.run();

//  try {
//    solver.run();
//  } catch( const CalcException& exc ) {
//    std::cout << exc.what() << std::endl;
//    std::cout << "Sorry - Calculation failed!" << std::endl;
//    throw exc;
//  }

  return 0;
}
