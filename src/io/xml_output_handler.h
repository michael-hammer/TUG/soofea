#ifndef xml_output_handler_h___
#define xml_output_handler_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "output_handler.h"
#include "base/xml_base.h"

//------------------------------------------------------------------
class XMLOutputHandler :
  public OutputHandler,
  public XMLBase
{
public:
 XMLOutputHandler(XMLDOMParser* parser, bool write_mesh) :
    OutputHandler(IDObject::XML),
    XMLBase( parser ),
    write_mesh_(write_mesh) {}

  ~XMLOutputHandler() {}

  virtual void flush();

  virtual void write( Model& model );
protected:
  bool write_mesh_;

  void writeNodeResults(TimeStamp& time_stamp, XS::DOMElement* xml_time_step);
  void writeNodes();
  void writeTypes();
  void writeMaterials();
  void writeElements();
  void writeBoundaries();
  void writeGlobal();
};

#endif
