#ifndef model_handler_h__
#define model_handler_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>

#include "exceptions.h"

#include "input_handler.h"
#include "output_handler.h"

#include "base/configuration.h"
#include "model/model.h"

//------------------------------------------------------------------
class ModelHandler
{
public:
  ModelHandler(InputHandler* input_handler, OutputHandler* output_handler);
  ~ModelHandler();

  void read();
  void readResults();
  void write();
  inline void flush() {output_handler_->flush();}

  inline Model& getModel()
  {return( *model_ );}

  inline BCHandler& getBCHandler()
  {return( *bc_handler_ );}

protected:
  InputHandler* input_handler_;
  OutputHandler* output_handler_;

  Model *model_;
  BCHandler *bc_handler_;
};

#endif
