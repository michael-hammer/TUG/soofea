#ifndef vtk_output_handler_h___
#define vtk_output_handler_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "output_handler.h"
#include "vtkUnstructuredGrid.h"
#include "vtkSmartPointer.h"

//------------------------------------------------------------------
class VTKOutputHandler :
  public OutputHandler
{
public:
  VTKOutputHandler(const fs::path& file_path);

  ~VTKOutputHandler() {}

  virtual void flush();

  virtual void write( Model& model );
protected:
  fs::path file_path_;

  void writeNodes(vtkSmartPointer<vtkUnstructuredGrid>& mesh);
  void writeElements(vtkSmartPointer<vtkUnstructuredGrid>& mesh);

  void writeNodeResults(TimeStamp& time_stamp,vtkSmartPointer<vtkUnstructuredGrid>& mesh);

//  void writeTypes();
//  void writeMaterials();
//  void writeBoundaries();
//  void writeGlobal();
};

#endif
