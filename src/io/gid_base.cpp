/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "gid_base.h"

//------------------------------------------------------------------
GiDBase::GiDBase() :
  dimension_(0),
  problem_name_(new char[MAX_PROBLEM_NAME_LENGTH]),
  gid_element_type_(GiD_NoElement),
  node_amount_(0),

  begin_coordinate_already_set_(false),
  begin_element_already_set_(false),
  gauss_c_name_(0),
  begin_node_displ_set_(false),
  gauss_point_already_set_(false),
  begin_gauss_result_set_(false)
{
  std::string gauss_name = "GP";
  gauss_c_name_ = convertStringToChar(gauss_name);
}

//------------------------------------------------------------------
GiDBase::~GiDBase()
{
  delete[] problem_name_;

  if( gauss_c_name_ )
    delete[] gauss_c_name_;
}

//##################################################################
// Post mesh file

//------------------------------------------------------------------
void GiDBase::openPostMeshFile(const std::string& file_name)
{
  char* c_file_name = convertStringToChar(file_name);
  GiD_OpenPostMeshFile(c_file_name,GiD_PostAscii);
  delete[] c_file_name;
}

//------------------------------------------------------------------
void GiDBase::beginMesh()
{
  GiD_BeginMesh( problem_name_ ,
		 (GiD_Dimension)dimension_,
                 gid_element_type_,
		 node_amount_ );
}

//------------------------------------------------------------------
void GiDBase::setGlobal( const Global& global )
{
  dimension_ = global.getDimension();
  const char* temp = global.getID().c_str();
  unsigned index = 0;
  while ( *temp != '\0' ) {
    problem_name_[index++] = *(temp++);
  }
  problem_name_[index] = '\0';
}

//------------------------------------------------------------------
void GiDBase::evaluateElementType(const ElementType& element_type)
{
  switch( element_type.getID() ) {
  case IDObject::BRICK:
    gid_element_type_ = GiD_Hexahedra;
    switch( element_type.getShape().getOrder() ) {
    case 1:
      node_amount_ = 8;
      break;
    case 2:
      node_amount_ = 27;
      break;
    }
    break;
  case IDObject::QUAD:
    gid_element_type_ = GiD_Quadrilateral;
    switch( element_type.getShape().getOrder() ) {
    case 1:
      node_amount_ = 4;
      break;
    case 2:
      node_amount_ = 9;
      break;
    }
    break;
  default:
    throw EXC(IOException) << "No GiD element type defined for ElementType ID = " 
                           << IDObject::resolveID( element_type.getID() ) << Exception::endl;
    break;
  }
}

//------------------------------------------------------------------
void GiDBase::beginNodeDisplacement()
{
  char* component_id[3];
  switch( dimension_ )
    {
    case 2:
      component_id[0] = (char*)"x";
      component_id[1] = (char*)"y";
      GiD_ResultDescriptionDim((char*)"Displacement", GiD_Vector, 2);
      GiD_ResultComponents(2, component_id);
      break;
    case 3:
      component_id[0] = (char*)"x";
      component_id[1] = (char*)"y";
      component_id[2] = (char*)"z";
      GiD_ResultDescriptionDim((char*)"Displacement", GiD_Vector, 3);
      GiD_ResultComponents(3, component_id);
      break;
    }
}

//------------------------------------------------------------------
void GiDBase::beginNodeTemperature()
{
  char* component_id[1];
  component_id[0] = (char*)"t";
  GiD_ResultDescription((char*)"Temperature", GiD_Scalar);
  GiD_ResultComponents(1, component_id);
}

// //------------------------------------------------------------------
// void GiDBase::beginNodeInternalLoad()
// {
//   char* component_id[3];
// 
//   switch( dimension_ )
//     {
//     case 2:
//       component_id[0] = (char*)"x";
//       component_id[1] = (char*)"y";
//       GiD_ResultDescriptionDim((char*)"InternalLoad", GiD_Vector, 2);
//       GiD_ResultComponents(2, component_id);
//       break;
//     case 3:
//       component_id[0] = (char*)"x";
//       component_id[1] = (char*)"y";
//       component_id[2] = (char*)"z";
//       GiD_ResultDescriptionDim((char*)"InternalLoad", GiD_Vector, 3);
//       GiD_ResultComponents(3, component_id);
//       break;
//     }
// }

//------------------------------------------------------------------
void GiDBase::writeNode(const Node& node)
{
  switch( dimension_ ) {
  case 2:
    GiD_WriteCoordinates2D(node.getNumber(),
                           node.getLagrangianPoint().getCoordinate(CoordSys::X),
                           node.getLagrangianPoint().getCoordinate(CoordSys::Y));
    break;
  case 3:
    GiD_WriteCoordinates(node.getNumber(),
                         node.getLagrangianPoint().getCoordinate(CoordSys::X),
                         node.getLagrangianPoint().getCoordinate(CoordSys::Y),
                         node.getLagrangianPoint().getCoordinate(CoordSys::Z) );
    break;
  }
}

//------------------------------------------------------------------
void GiDBase::writeElement(const Element& element)
{
  std::vector<int> node_numbers;
  for( ConstIterator<Node> iter = element.ConstAggregate<Node>::begin() ;
       iter != element.ConstAggregate<Node>::end() ;
       ++iter )
    node_numbers.push_back( iter -> getNumber() );

  GiD_WriteElement( element.getNumber(), &(node_numbers.front()) );
}

//------------------------------------------------------------------
void GiDBase::writeNodeDisplacement(const Node& node, const TimeStamp& time_stamp)
{
  switch( dimension_ )
    {
    case 2:
      GiD_Write2DVector(node.getNumber(),
			node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) -> getValue(time_stamp),
			node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) -> getValue(time_stamp));
      break;
    case 3:
      GiD_WriteVector(node.getNumber(),
		      node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) -> getValue(time_stamp),
		      node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) -> getValue(time_stamp),
		      node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Z) -> getValue(time_stamp));
      break;
    }
}

// //------------------------------------------------------------------
// void GiDBase::writeNodeInternalLoad(const Node& node, const TimeStamp& time_stamp)
// {
//   switch( dimension_ )
//     {
//     case 2:
//       GiD_Write2DVector(node.getNumber(),
// 			node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) -> getInternalLoad(time_stamp),
// 			node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) -> getInternalLoad(time_stamp));
//       break;
//     case 3:
//       GiD_WriteVector(node.getNumber(),
// 		      node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::X) -> getInternalLoad(time_stamp),
// 		      node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Y) -> getInternalLoad(time_stamp),
// 		      node.getDOF( IDObject::DISPLACEMENT ) -> getValue(CoordSys::Z) -> getInternalLoad(time_stamp));
//       break;
//     }
// }

//------------------------------------------------------------------
void GiDBase::writeNodeTemperature(const Node& node, const TimeStamp& time_stamp)
{
  /** @fixme Use time_stamp here! */
  GiD_WriteScalar(node.getNumber(),
		  node.getEulerianTemperature());
}

//##################################################################
// Post result file

//------------------------------------------------------------------
void GiDBase::openPostResultFile(const std::string& file_name)
{
  char* c_file_name = convertStringToChar(file_name);
  GiD_OpenPostResultFile(c_file_name,GiD_PostAscii);
  delete[] c_file_name;
}

























//------------------------------------------------------------------
void GiDBase::writeGaussPoint(const Element* element)
{
  if( !gauss_point_already_set_ )  {
      createGaussPoints(element);
  }
}


//------------------------------------------------------------------
void GiDBase::writeGaussRes(const Element* element, double step)
{
  unsigned count = 0;

  switch( dimension_ )
    {
    case 2:
      if( !begin_gauss_result_set_ )
	{
	  GiD_BeginResultGroup((char*)"Analysis",
			       step,
			       GiD_OnGaussPoints,
			       gauss_c_name_);
	  beginGaussStrain();
	  beginGaussStress();
	  begin_gauss_result_set_ = true;
	}

      for( count = 0 ; count < (element->getType()).getAmountOfIntPoints() ; ++count)
	{
	  writeGaussStrain(element,count);
	  writeGaussStress(element,count);
	}
      break;
    case 3:
      if( !begin_gauss_result_set_ )
	{
	  GiD_BeginResultGroup((char*)"Analysis",
			       step,
			       GiD_OnGaussPoints,
			       gauss_c_name_);
	  beginGaussEquivalentInelasticStrain();
	  begin_gauss_result_set_ = true;
	}
      for( count = 0 ; count < (element->getType()).getAmountOfIntPoints(); ++count)
	writeGaussEquivalentInelasticStrain(element,count);
      break;
    }
}

//------------------------------------------------------------------
void GiDBase::beginGaussStrain()
{
  char* component_id[6];

  switch( dimension_ )
    {
    case 2:
      component_id[0] = (char*)"eps_xx";
      component_id[1] = (char*)"eps_yy";
      component_id[2] = (char*)"eps_xy";
      GiD_ResultDescriptionDim((char*)"StrainG", GiD_Matrix, 3);
      GiD_ResultComponents(3, component_id);
       break;
    case 3:
      /*
      component_id[0] = "eps_xx";
      component_id[1] = "eps_yy";
      component_id[2] = "eps_zz";
      component_id[3] = "eps_xy";
      component_id[4] = "eps_yz";
      component_id[5] = "eps_xz";
      GiD_ResultDescriptionDim("StrainG", GiD_Matrix, 6);
      GiD_ResultComponents(6, component_id);
      component_id[0] = "sigma_xx";
      component_id[1] = "sigma_yy";
      component_id[2] = "sigma_zz";
      component_id[3] = "sigma_xy";
      component_id[4] = "sigma_yz";
      component_id[5] = "sigma_xz";
      GiD_ResultDescriptionDim("StressG", GiD_Matrix, 6);
      GiD_ResultComponents(6, component_id);
      */
      break;
    }
}

//------------------------------------------------------------------
void GiDBase::beginGaussStress()
{
  char* component_id[6];

  switch( dimension_ )
    {
    case 2:
      component_id[0] = (char*)"sigma_xx";
      component_id[1] = (char*)"sigma_yy";
      component_id[2] = (char*)"sigma_xy";
      GiD_ResultDescriptionDim((char*)"StressG", GiD_Matrix, 3);
      GiD_ResultComponents(3, component_id);
      break;
    }
}

//------------------------------------------------------------------
void GiDBase::beginGaussEquivalentInelasticStrain()
{
  char* component_id[1];
  component_id[0] = (char*)"equivalent_inelastic_strain";
  GiD_ResultDescription((char*)"Equ. Plast. strain", GiD_Scalar);
  GiD_ResultComponents(1, component_id);
}

//------------------------------------------------------------------
void GiDBase::writeGaussStrain(const Element* element, unsigned local_gauss_number)
{
  switch( dimension_ )
    {
    case 2:
      GiD_Write2DMatrix(element -> getNumber(),
			(element -> ConstAggregate<ElementIntegrationPoint>::get(local_gauss_number).getLagrangianStrain())(0,0),
			(element -> ConstAggregate<ElementIntegrationPoint>::get(local_gauss_number).getLagrangianStrain())(1,1),
                        (element -> ConstAggregate<ElementIntegrationPoint>::get(local_gauss_number).getLagrangianStrain())(0,1) );
      break;
    case 3:
      /*
	GiD_Write3DMatrix(element -> getNumber(),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStrain()))(0,0),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStrain()))(1,1),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStrain()))(2,2),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStrain()))(0,1),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStrain()))(1,2),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStrain()))(0,2));
      */
      break;
    }
}

//------------------------------------------------------------------
void GiDBase::writeGaussStress(const Element* element, unsigned local_gauss_number)
{
  switch( dimension_ )
    {
    case 2:
      GiD_Write2DMatrix(element -> getNumber(),
			(element -> ConstAggregate<ElementIntegrationPoint>::get(local_gauss_number).getStress())(0,0),
                        (element -> ConstAggregate<ElementIntegrationPoint>::get(local_gauss_number).getStress())(1,1),
                        (element -> ConstAggregate<ElementIntegrationPoint>::get(local_gauss_number).getStress())(0,1) );
      break;
    case 3:
      /*
	GiD_Write3DMatrix(element -> getNumber(),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStress()))(0,0),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStress()))(1,1),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStress()))(2,2),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStress()))(0,1),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStress()))(1,2),
	(*(element -> getLocalIntegrationPoint(local_gauss_number) -> getStress()))(0,2));
      */
      break;
    }
}

//------------------------------------------------------------------
void GiDBase::writeGaussEquivalentInelasticStrain(const Element* element, unsigned local_gauss_number)
{
  double kappa = 0.0;

//  switch( dimension_ )
//    {
//    case 3:
//      bz::Array<double,2> Ep(  *(element->getPlasticityTensorEp(local_gauss_number))  );
//      unsigned long pos[] = {0,0};
//      for (unsigned long row = 0; row < 3; row++)
//	for (unsigned long col = 0; col < 3; col++)
//	  {
//	    pos[0] = row; pos[1] = col;
//	    kappa += Ep.get(pos) * Ep.get(pos);
//	  }
//      break;
//    }

  kappa = sqrt(2.0/3.0*kappa);

  GiD_WriteScalar(element -> getNumber(),kappa);
}

//------------------------------------------------------------------
void GiDBase::createGaussPoints(const Element* element)
{
  unsigned count = 0;

//  GiD_BeginGaussPoint(gauss_c_name_, actual_gid_element_type_, mesh_c_name_,
//		      element->getType().getAmountOfIntPoints() , 0, 0);
  gauss_point_already_set_ = true;

  for( count = 0; count < element->getType().getAmountOfIntPoints() ; ++count )
    {
      switch( dimension_ )
	{
	case 2:
	  GiD_WriteGaussPoint2D( element->ConstAggregate<ElementIntegrationPoint>::get(count).getMathIP().r(),
				 element->ConstAggregate<ElementIntegrationPoint>::get(count).getMathIP().s() );
	  break;
	case 3:
	  GiD_WriteGaussPoint3D( element->ConstAggregate<ElementIntegrationPoint>::get(count).getMathIP().r(),
				 element->ConstAggregate<ElementIntegrationPoint>::get(count).getMathIP().s(),
				 element->ConstAggregate<ElementIntegrationPoint>::get(count).getMathIP().t() );
	  break;
	}
    }

  GiD_EndGaussPoint();
}

//------------------------------------------------------------------
char* GiDBase::convertStringToChar(const std::string& string_to_convert)
{
  const char* string_ptr = string_to_convert.c_str();
  char* c_string = new char[string_to_convert.length() + 1];

  unsigned count = 0;
  for( count = 0; count <= string_to_convert.length() ; ++count )
    {
      c_string[count] = string_ptr[count];
    }

  return(c_string);
}

