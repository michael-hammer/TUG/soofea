/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "input_handler.h"

//------------------------------------------------------------------
void InputHandler::readModel( Model& model )
{
  std::cout << "Create Model .";

  Global* global_data = new Global;
  this -> get(*global_data);
  model.setGlobal( global_data );

  std::cout << ".";
 
  for( Iterator<Type> type_iter = Aggregate<Type>::begin();
	 type_iter != Aggregate<Type>::end(); 
       ++type_iter)
    model.addType(*type_iter);

  std::cout << ".";
 
  for( Iterator<Material> material_iter = Aggregate<Material>::begin();
	 material_iter != Aggregate<Material>::end(); 
	 ++material_iter)
    model.addMaterial(*material_iter);

  std::cout << ".";

  for( Iterator<Node> node_iter = Aggregate<Node>::begin(); 
	 node_iter != Aggregate<Node>::end(); 
	 ++node_iter)
    model.addNode(*node_iter);

  std::cout << ".";

  for( Iterator<Edge> edge_iter = Aggregate<Edge>::begin(); 
	 edge_iter != Aggregate<Edge>::end(); 
	 ++edge_iter)
    model.addEdge(*edge_iter);

  std::cout << ".";

  for( Iterator<Face> face_iter = Aggregate<Face>::begin(); 
	 face_iter != Aggregate<Face>::end(); 
	 ++face_iter)
    model.addFace(*face_iter);

  std::cout << ".";
  
  for( Iterator<Element> element_iter = Aggregate<Element>::begin() ; 
	 element_iter != Aggregate<Element>::end(); 
	 ++element_iter)
    model.addElement(*element_iter);

  std::cout << ".";
  
  for( Iterator<Boundary> boundary_iter = Aggregate<Boundary>::begin() ;
       boundary_iter != Aggregate<Boundary>::end(); 
       ++boundary_iter)
    model.addBoundary(*boundary_iter);
  //  model.setSubComponentsOnBoundary();

  std::cout << ".";

  for( Iterator<TimeEpisode> time_episode_iter = Aggregate<TimeEpisode>::begin() ; 
       time_episode_iter != Aggregate<TimeEpisode>::end(); 
       ++time_episode_iter) {
    model.getTimeBar().addTimeEpisode(&(*time_episode_iter));
  }
  model.getTimeBar().distributeTimeStamps();

  std::cout << ".";

  Contact* contact_data = new Contact;
  this -> get(*contact_data);
  model.setContact(contact_data);

  std::cout << ". finished" << std::endl;
}

//------------------------------------------------------------------
BCHandler* InputHandler::createBCHandler( Model& model )
{
  std::cout << "Create BCHandler .";

  BCHandler* bc_handler = new BCHandler( model );

  for( Iterator<Prescribed<DOFValue> > prescribed_dof_iter = Aggregate<Prescribed<DOFValue> >::begin() ; 
       prescribed_dof_iter != Aggregate<Prescribed<DOFValue> >::end(); 
       ++prescribed_dof_iter) {
    bc_handler->addPrescribedDOF(&(*prescribed_dof_iter));
  }

  std::cout << ".";

  for( Iterator<Prescribed<LoadValue> > prescribed_load_iter = Aggregate<Prescribed<LoadValue> >::begin() ; 
       prescribed_load_iter != Aggregate<Prescribed<LoadValue> >::end(); 
       ++prescribed_load_iter) {
    bc_handler->addPrescribedLoad(&(*prescribed_load_iter));
  }

  std::cout << ". finished" << std::endl;

  return( bc_handler );
}

//------------------------------------------------------------------
void InputHandler::readResults( Model& model )
{
  std::cout << "Read results .";

  model.clearArchive();
  TimeStamp* time_stamp = 0;

  DOF* dof = 0;
  Load* load = 0;

  for( Iterator<TimeStamp> time_iter = Aggregate<TimeStamp>::begin() ;
       time_iter != Aggregate<TimeStamp>::end() ;
       ++time_iter ) {

    time_stamp = model.getTimeBar().getTimeStampFromTimeBar( *time_iter );

    std::cout << ".";

    for( Iterator<DisplacementDOF> dof_iter = this -> ResultAggregate<DisplacementDOF>::begin( *time_iter  ) ;
         dof_iter != this -> ResultAggregate<DisplacementDOF>::end() ;
         ++dof_iter ) {
      dof = model.Aggregate<Node>::get( dof_iter -> getNodeNumber() ).getDOF( IDObject::DISPLACEMENT );
      if( !dof ) {
        throw EXC(IOException) << "Try to add a result displacement DOF to model - but the displacement DOF for the node '" 
                               << dof_iter -> getNodeNumber() << "' does not exist" << Exception::endl;
      }
      dof -> addResult( *dof_iter , *time_iter );
      delete &(*dof_iter);
    }

    std::cout << ".";

    for( Iterator<ForceLoad> load_iter = this -> ResultAggregate<ForceLoad>::begin( *time_iter  ) ;
         load_iter != this -> ResultAggregate<ForceLoad>::end() ;
         ++load_iter ) {
      load = model.Aggregate<Node>::get( load_iter -> getNodeNumber() ).getLoad( IDObject::FORCE );
      if( !load ) {
        throw EXC(IOException) << "Try to add a result force load to model - but the force load for the node '" 
                               << load_iter -> getNodeNumber() << "' does not exist" << Exception::endl;
      }
      load -> addResult( *load_iter, *time_iter );
      delete &(*load_iter);
    }

    std::cout << ".";

    for( Iterator<LagrangeDOF> dof_iter = this -> ResultAggregate<LagrangeDOF>::begin( *time_iter  ) ;
         dof_iter != this -> ResultAggregate<LagrangeDOF>::end() ;
         ++dof_iter ) {
      dof = model.Aggregate<Node>::get( dof_iter -> getNodeNumber() ).getDOF( IDObject::LAGRANGE );
      if( !dof ) {
        model.Aggregate<Node>::get( dof_iter -> getNodeNumber() ).addLagrangeDOF( false, dof_iter -> getStick() );
        dof = model.Aggregate<Node>::get( dof_iter -> getNodeNumber() ).getDOF( IDObject::LAGRANGE );
      }
      dof -> addResult( *dof_iter, *time_iter );
      delete &(*dof_iter);
    }

    model.getTimeBar().setLastFinishedTimeStamp( *time_stamp  );
  }

  std::cout << ". finished" << std::endl;
}
