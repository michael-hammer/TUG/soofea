#ifndef gid_base_h__
#define gid_base_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <string>

#include <stdlib.h>
#include "gidpost.h"

#include "exceptions.h"
#include "model/model.h"

#define MAX_PROBLEM_NAME_LENGTH 256

//------------------------------------------------------------------
class GiDBase
{
public:
  typedef std::pair<GiD_ElementType,int> GiDElementPair;

  GiDBase();
  ~GiDBase();
protected:
  void evaluateElementType(const ElementType& element_type);
  char* convertStringToChar(const std::string& string_to_convert);

  void openPostMeshFile(const std::string& file_name);
  void beginMesh();
  void beginNodeDisplacement();
  //  void beginNodeInternalLoad();
  void writeNode(const Node& node);
  void writeElement(const Element& element);
  void setGlobal( const Global& global );

  void openPostResultFile(const std::string& file_name);

  void writeNodeDisplacement(const Node& node, const TimeStamp& time_stamp);
  //  void writeNodeInternalLoad(const Node& node, const TimeStamp& time_stamp);

  void writeGaussPoint(const Element* element);

  void writeNodeRes(const Node* node, double step);
  void writeGaussRes(const Element* element, double step);

  void beginNodeTemperature();
  void writeNodeTemperature(const Node& node, const TimeStamp& time_stamp);

  void createGaussPoints(const Element* element);
  void beginGaussStrain();
  void beginGaussStress();
  void beginGaussEquivalentInelasticStrain();
  void writeGaussStrain(const Element* element, unsigned local_gauss_number);
  void writeGaussStress(const Element* element, unsigned local_gauss_number);
  void writeGaussEquivalentInelasticStrain(const Element* element, unsigned local_gauss_number);

  unsigned dimension_;
  char* problem_name_;
  GiD_ElementType gid_element_type_;
  int node_amount_; 

  bool begin_coordinate_already_set_;
  bool begin_element_already_set_;
  char* gauss_c_name_;
  bool begin_node_displ_set_;
  bool gauss_point_already_set_;
  bool begin_gauss_result_set_;
};

#endif //  gid_base_h__
