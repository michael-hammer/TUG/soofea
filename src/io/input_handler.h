#ifndef input_handler_h__
#define input_handler_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <vector>

#include "exceptions.h"
#include "base/id_object.h"
#include "model/model.h"
#include "model/bc_handler.h"
#include "template/result_aggregate.h"

//------------------------------------------------------------------
class InputHandler :
  public InputAggregate<Node>,
  public InputAggregate<Edge>,
  public InputAggregate<Face>,
  public InputAggregate<Element>,
  public InputAggregate<Type>,
  public InputAggregate<Boundary>,
  public InputAggregate<Material>,
  public InputAggregate<TimeEpisode>,
  public InputAggregate<Prescribed<LoadValue> >,
  public InputAggregate<Prescribed<DOFValue> >,
  public ResultAggregate<TimeStamp>,
  public ResultAggregate<DisplacementDOF>,
  public ResultAggregate<ForceLoad>,
  public ResultAggregate<LagrangeDOF>
{
public:
  inline InputHandler(IDObject::ID io_type) : 
    io_type_(io_type) {};

  inline virtual ~InputHandler() {};

  void readModel( Model& model );

  BCHandler* createBCHandler( Model& model );

  void readResults( Model& model );

  /**
     Find the Global Data tag and sets the values in the given 
     global_data object.
  */
  virtual void get(Global& global_data) = 0;

  /**
     Find the contact Data tag and sets the values in the given
     contact_data object.
   */
  virtual void get(Contact& contact_data) = 0;

  /**
     @return begin InputIterator
   */
  template<class T>
  Iterator<T> begin();

  /**
     @return end InputIterator
   */
  template<class T>
  Iterator<T> end();

  inline IDObject::ID getIOType() {return(io_type_);}
protected:
  template<class T> friend class Iterator;

  IDObject::ID io_type_;
};

#endif
