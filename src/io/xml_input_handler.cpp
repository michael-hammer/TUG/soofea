/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "xml_input_handler.h"

//------------------------------------------------------------------
Node* XMLInputHandler::createNode(XS::DOMElement* node)
{
  bool found = false;
  return(new Node(getNumber(node),
		  getDouble(node,X_COORD,found),
		  getDouble(node,Y_COORD,found),
		  getDouble(node,Z_COORD,found),
		  getDouble(node,TEMPERATURE,found),
		  Model::getWorldCoordSys()));
}

//------------------------------------------------------------------
Node* XMLInputHandler::next(Node* act_item, int& index)
{	
  DOMElement* next_node = findNext( InputAggregate<Node>::xml_container_,
                                    NODE,
                                    index );
  if(!next_node) return(0);
  return( createNode( next_node ));
}

//------------------------------------------------------------------
Iterator<Node> XMLInputHandler::begin( Node* )
{
  InputAggregate<Node>::xml_container_ = getChild(NODE_CONTAINER);
  if( !InputAggregate<Node>::xml_container_ )
    throw EXC(IOException) << "Input file without <node> container is really useless in SOOFEA!" << Exception::endl;

  numbered index = 0;
  DOMElement* first_node = findFirst( InputAggregate<Node>::xml_container_,
                                      NODE );
  if(!first_node){
    throw EXC(IOException) << "Input file without <node>'s is really useless in SOOFEA!" << Exception::endl;
  }

  return( Iterator<Node>(this, createNode( first_node ), index ) );
}

//------------------------------------------------------------------
Edge* XMLInputHandler::createEdge(XS::DOMElement* node)
{
  return(new Edge(getNumber(node),
                  getReferenceNumberVector(node,GLOBAL_NODE_NUM),
                  getReferenceNumber(node,TYPE)));
}

//------------------------------------------------------------------
Edge* XMLInputHandler::next(Edge* act_item, int& index)
{	
  DOMElement* next_edge = findNext( InputAggregate<Edge>::xml_container_,
                                    EDGE,
                                    index );
  if(!next_edge) return(0);
  return( createEdge( next_edge ));
}

//------------------------------------------------------------------
Iterator<Edge> XMLInputHandler::begin( Edge* )
{
  InputAggregate<Edge>::xml_container_ = getChild(EDGE_CONTAINER);
  if( !InputAggregate<Edge>::xml_container_ ) {
    LOG(Logger::WARN) << "No edge container found!" << Logger::endl;
    return( Aggregate<Edge>::end() );
  }

  numbered index = 0;
  DOMElement* first_edge = findFirst( InputAggregate<Edge>::xml_container_,
                                      EDGE );
  if(!first_edge){
    LOG(Logger::WARN) << "No edge found!" << Logger::endl;
    return( Aggregate<Edge>::end() );
  }

  return( Iterator<Edge>(this, createEdge( first_edge ), index ) );
}

//------------------------------------------------------------------
Face* XMLInputHandler::createFace(XS::DOMElement* node)
{
  return(new Face(getNumber(node),
                  getReferenceNumberVector(node,GLOBAL_NODE_NUM),
                  getReferenceNumber(node,TYPE)));
}

//------------------------------------------------------------------
Face* XMLInputHandler::next(Face* act_item, int& index)
{	
  DOMElement* next_face = findNext( InputAggregate<Face>::xml_container_,
                                    FACE,
                                    index );
  if(!next_face) return(0);
  return( createFace( next_face ));
}

//------------------------------------------------------------------
Iterator<Face> XMLInputHandler::begin( Face* )
{
  InputAggregate<Face>::xml_container_ = getChild(FACE_CONTAINER);
  if( !InputAggregate<Face>::xml_container_ ) {
    LOG(Logger::WARN) << "No face container found!" << Logger::endl;
    return( Aggregate<Face>::end() );
  }

  numbered index = 0;
  DOMElement* first_face = findFirst( InputAggregate<Face>::xml_container_,
                                      FACE );
  if(!first_face){
    LOG(Logger::WARN) << "No face found!" << Logger::endl;
    return( Aggregate<Face>::end() );
  }

  return( Iterator<Face>(this, createFace( first_face ), index ) );
}

//------------------------------------------------------------------
Element* XMLInputHandler::createElement(XS::DOMElement* node)
{
  return(new Element(getNumber(node),
		     getReferenceNumber(node,MATERIAL),
		     getReferenceNumberVector(node,GLOBAL_NODE_NUM),
		     getReferenceNumber(node,TYPE)) );
}

//------------------------------------------------------------------
Element* XMLInputHandler::next(Element* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<Element>::xml_container_,
				 ELEMENT,
				 index );
  if(!next_node) return(0);
  return( createElement( next_node ) );
}

//------------------------------------------------------------------
Iterator<Element> XMLInputHandler::begin( Element* )
{
  InputAggregate<Element>::xml_container_ = getChild(ELEMENT_CONTAINER);
  if( !InputAggregate<Element>::xml_container_ )
    throw EXC(IOException) << "Input file without <element> container is really useless in SOOFEA!" << Exception::endl;

  numbered index = 0;
  DOMElement* first_node = findFirst( InputAggregate<Element>::xml_container_,
                                      ELEMENT );
  if(!first_node){
    throw EXC(IOException) << "Input file without <element>'s is really useless in SOOFEA!" << Exception::endl;
  }

  return( Iterator<Element>(this, createElement( first_node ), index ) );
}

//------------------------------------------------------------------
Type* XMLInputHandler::createType(XS::DOMElement* node)
{
  bool found = false;
  Type *type = 0;
  std::string type_id_string = getString(node,ID,found);
  if( !found )
    throw EXC(IOException) << "We need an TypeID ("<<ID<<"=)!" << Exception::endl;

  IDObject::ID type_id = IDObject::resolveID(type_id_string);
  if( type_id == IDObject::NAID ) {
    throw EXC(IOException) << "'" << type_id_string
                           << "' is non defined Type ID" << Exception::endl;
  }

  std::string type_target_string = getString(node,TARGET,found);
  if( !found )
    throw EXC(IOException) << "We need a Type target to create an Type("<< TARGET <<"=)!" << Exception::endl;

  std::string shape_type_string = getString(node,SHAPE_TYPE,found);
  if( !found )
    throw EXC(IOException) << "We need a shape type to create an Type("<< SHAPE_TYPE <<"=)!" << Exception::endl;
  
  IDObject::ID shape_type = IDObject::resolveID(shape_type_string);
  if( shape_type == IDObject::NAID )
    throw EXC(IOException) << "'" << shape_type_string
                           << "' is a not defined shape type for an Type!" << Exception::endl;

  int shape_order = getInt( node, SHAPE_ORDER , found );
  if( !found )
    throw EXC(IOException) << "You have to provide a shape order for the element type" << Exception::endl;

  switch( IDObject::resolveID(type_target_string) ) {
  case IDObject::ELEMENT:
    type = new ElementType( getNumber(node),
                            type_id,
                            shape_type,
                            shape_order,
                            getIntVector(node,INT_POINTS) );
    break;
  case IDObject::EDGE:
    type = new EdgeType( getNumber(node),
                         type_id,
                         shape_order,
                         getIntVector(node,INT_POINTS) );
    break;
  case IDObject::FACE:
    type = new FaceType( getNumber(node),
                         type_id,
                         shape_type,
                         shape_order,
                         getIntVector(node,INT_POINTS) );
    break;
  default:
    throw EXC(IOException) << "The given Type target '"
                           << type_target_string
                           << "' is not implemented" << Exception::endl;
  }
    
  return( type );
}

//------------------------------------------------------------------
Type* XMLInputHandler::next(Type* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<Type>::xml_container_,
                                    TYPE,
                                    index );
  if( !next_node ) return(0); 
  return( createType( next_node ) );
}

//------------------------------------------------------------------
Iterator<Type> XMLInputHandler::begin( Type* )
{
  InputAggregate<Type>::xml_container_ = getChild(TYPE_CONTAINER);
  if( !InputAggregate<Type>::xml_container_ ) {
    LOG(Logger::WARN) << "No Type Container found!" << Logger::endl;
    return( Aggregate<Type>::end() );
  }

  numbered index = 0;
  DOMElement* first_node = findFirst( InputAggregate<Type>::xml_container_,
                                      TYPE );
  if(!first_node){
    LOG(Logger::WARN) << "No Type was found -> DANGEROUS!" << Logger::endl;
    return( Aggregate<Type>::end() );
  }

  return( Iterator<Type>(this, createType( first_node ), index ) );
}

//------------------------------------------------------------------
Material* XMLInputHandler::createMaterial(XS::DOMElement* node)
{
  bool found;
  Material* material = 0;

  std::string material_type_string = getString(node,ID,found);
  if( !found )
    throw EXC(IOException) << "You have to set a Material id=''" << Exception::endl;

  Material::ID material_type_id = Material::resolveID( material_type_string );
  if( material_type_id == Material::NAID ) {
    throw EXC(IOException) << "'" << material_type_string
                           << "' is a not defined Material ID!" << Exception::endl;
  }
  
  material = new Material(getNumber(node),
                          material_type_id);
  addProperties( node, *material );
  return( material ); 
}

//------------------------------------------------------------------
Material* XMLInputHandler::next(Material* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<Material>::xml_container_,
				 MATERIAL,
				 index );
  if(!next_node) return(0);
  return( createMaterial( next_node ) );
}

//------------------------------------------------------------------
Iterator<Material> XMLInputHandler::begin( Material* )
{
  InputAggregate<Material>::xml_container_ = getChild(MATERIAL_CONTAINER);
  if( !InputAggregate<Material>::xml_container_ ) {
    LOG(Logger::WARN) << "No Material Container found!";
    return( Aggregate<Material>::end() );
  }

  numbered index = 0;
  DOMElement* first_node = findFirst( InputAggregate<Material>::xml_container_,
                                      MATERIAL );

  if(!first_node){
    LOG(Logger::WARN) << "No Material was found -> DANGEROUS!" << Logger::endl;
    return( Aggregate<Material>::end() );
  }
  
  return( Iterator<Material>(this, createMaterial( first_node ), index ) );
}

//------------------------------------------------------------------
Prescribed<DOFValue>* XMLInputHandler::createPrescribedDOF(XS::DOMElement* node)
{
  bool found = false;
  std::vector<DOFValue*> *prescribed_dof_vector = new std::vector<DOFValue*>;
  IDObject::ID target_type = IDObject::NAID;
  
  numbered target_number = 0;
  if( (target_number = getReferenceNumber(node,GLOBAL_NODE_NUM)) != 0 )
    target_type = IDObject::NODE;
  else if ( (target_number = getReferenceNumber(node,GLOBAL_BOUNDARY_NUM)) != 0 )
    target_type = IDObject::BOUNDARY;
  else
    throw EXC(IOException) << "Prescribed DOF target must be either of type NODE (" 
                           <<  GLOBAL_NODE_NUM << "=) or BOUNDARY (" 
                           <<  GLOBAL_BOUNDARY_NUM << "=)" << Exception::endl;


  IDObject::ID dof_type = IDObject::NAID;
  double value = getDouble(node, U_DISP, found);
  if( found ) {
    prescribed_dof_vector -> push_back(new DOFValue(value,CoordSys::X));
    dof_type = IDObject::DISPLACEMENT;
  }
      
  value = getDouble(node, V_DISP, found);
  if( found ) {
    if( dof_type == IDObject::NAID || dof_type == IDObject::DISPLACEMENT) {
      prescribed_dof_vector -> push_back(new DOFValue(value,CoordSys::Y));
      dof_type = IDObject::DISPLACEMENT;
    } else {
      throw EXC(IOException) << "Prescribed DOF must be either of displacement or temperature type, not both" << Exception::endl;
    }
  }
    
  value = getDouble(node, W_DISP, found);
  if( found ) {
    if(dof_type == IDObject::NAID || dof_type == IDObject::DISPLACEMENT) {
      prescribed_dof_vector -> push_back(new DOFValue(value,CoordSys::Z));
      dof_type = IDObject::DISPLACEMENT;
    } else {
      throw EXC(IOException) << "Prescribed DOF must be either of displacement or temperature type, not both" << Exception::endl;
    }
  }

  value = getDouble(node, TEMPERATURE, found);
  if( found ) {
    if(dof_type == IDObject::NAID) {
      prescribed_dof_vector -> push_back(new DOFValue(value,CoordSys::NAC));
      dof_type = IDObject::TEMPERATURE;
    } else {
      throw EXC(IOException) << "Prescribed DOF must be either of displacement or temperature type, not both" << Exception::endl;
    }
  }

  return(new Prescribed<DOFValue>(getNumber(node),
                                  target_number,
                                  target_type,
                                  dof_type,
                                  prescribed_dof_vector));
}

//------------------------------------------------------------------
Prescribed<DOFValue>* XMLInputHandler::next(Prescribed<DOFValue>* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<Prescribed<DOFValue> >::xml_container_,
                                    PRESCRIBED_DOF,
                                    index );
  if(!next_node) return(0);
  return( createPrescribedDOF( next_node ) );
}

//------------------------------------------------------------------
Iterator<Prescribed<DOFValue> > XMLInputHandler::begin( Prescribed<DOFValue>* )
{
  InputAggregate<Prescribed<DOFValue> >::xml_container_ = getChild(PRESCRIBED_DOF_CONTAINER);
  if( !InputAggregate<Prescribed<DOFValue> >::xml_container_ ) {
    throw EXC(IOException) << "There must be a <"<<PRESCRIBED_DOF_CONTAINER<<"> defined! "
                           << "Without Dirichlet RB Finite Element Analysis won't work!" << Exception::endl; 
  }

  numbered index = 0;
  DOMElement* first_node = findFirst( InputAggregate<Prescribed<DOFValue> >::xml_container_,
                                      PRESCRIBED_DOF );

  if( !first_node ){
    throw EXC(IOException) << "There must be a <"<<PRESCRIBED_DOF<<"> defined inside <"<<PRESCRIBED_DOF_CONTAINER<<">! "
                           << "Without Dirichlet RB Finite Element Analysis won't work!" << Exception::endl; 
    return( Aggregate<Prescribed<DOFValue> >::end() );
  }
  
  return( Iterator<Prescribed<DOFValue> >(this, createPrescribedDOF( first_node ), index ) );
}

//------------------------------------------------------------------
Prescribed<LoadValue>* XMLInputHandler::createPrescribedLoad(XS::DOMElement* node)
{
  bool found = false;
  IDObject::ID target_type = IDObject::NAID;
  std::vector<LoadValue*> *prescribed_load_vector = new std::vector<LoadValue*>;

  numbered target_number = 0;
  if( (target_number = getReferenceNumber(node,GLOBAL_NODE_NUM)) != 0 )
    target_type = IDObject::NODE;
  else if( (target_number = getReferenceNumber(node,GLOBAL_BOUNDARY_NUM)) != 0 )
    target_type = IDObject::BOUNDARY;
  else
    throw EXC(IOException) << "Prescribed load target must be either of type NODE ("
                           << GLOBAL_NODE_NUM << "=) or BOUNDARY ("
                           << GLOBAL_BOUNDARY_NUM << "=)" << Exception::endl;

  IDObject::ID load_type = IDObject::NAID;
  std::vector<double>* vector = 0;
  double value = 0.;

  if( !vector )
    vector = getDoubleVector(node, FORCE);
  if( vector ) {
    load_type = IDObject::FORCE;
  }

  if(! vector )
    vector = getDoubleVector(node, SURFACE_FORCE);
  if( vector ) {
    load_type = IDObject::SURFACE_FORCE;
  }
  
  if( vector ) {
    if( vector -> empty() )
      throw EXC(IOException) << "A zero size force load vector is rather useless" << Exception::endl;
    if( vector -> size() >= 1 ) {
      prescribed_load_vector -> push_back(new LoadValue(vector->at(0),CoordSys::X));
    }
    if( vector -> size() >= 2 ) {
      prescribed_load_vector -> push_back(new LoadValue(vector->at(1),CoordSys::Y));
    }
    if( vector -> size() >= 3 ) {
      prescribed_load_vector -> push_back(new LoadValue(vector->at(2),CoordSys::Z));
    }
  }

  value = getDouble(node, HEAT_FLUX, found);
  if( found ) {
    load_type = IDObject::HEAT_FLUX;
    prescribed_load_vector -> push_back(new LoadValue(value,CoordSys::NAC));
  }

  value = getDouble(node, PRESSURE, found);
  if( found ) {
    load_type = IDObject::PRESSURE;
    prescribed_load_vector -> push_back(new LoadValue(value,CoordSys::NAC));
  }

  if( load_type == IDObject::NAID )
    throw EXC(IOException) << "Load must be either of type ("<<FORCE<<"=), ("<<HEAT_FLUX<<"=) or ("<<PRESSURE<<"=)" << Exception::endl;
 
  Prescribed<LoadValue>* p_load = new Prescribed<LoadValue>(getNumber(node),
                                                            target_number,
                                                            target_type,
                                                            load_type,
                                                            prescribed_load_vector);
  return( p_load );
}

//------------------------------------------------------------------
Prescribed<LoadValue>* XMLInputHandler::next(Prescribed<LoadValue>* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<Prescribed<LoadValue> >::xml_container_,
                                    PRESCRIBED_LOAD,
                                    index );
  if(!next_node) return(0);
  return( createPrescribedLoad( next_node ) );
}

//------------------------------------------------------------------
Iterator<Prescribed<LoadValue> > XMLInputHandler::begin( Prescribed<LoadValue>* )
{
  InputAggregate<Prescribed<LoadValue> >::xml_container_ = getChild(PRESCRIBED_LOAD_CONTAINER);
  if( !InputAggregate<Prescribed<LoadValue> >::xml_container_ ) {
    LOG(Logger::WARN) << "No prescribed load container found!" << Logger::endl;
    return( Aggregate<Prescribed<LoadValue> >::end() );
  }
  numbered index = 0;
  DOMElement* first_node = findFirst( InputAggregate<Prescribed<LoadValue> >::xml_container_,
                                      PRESCRIBED_LOAD );
  if(!first_node){
    LOG(Logger::WARN) << "No Load was found!" << Logger::endl;
    return( Aggregate<Prescribed<LoadValue> >::end() );
  }
  
  return( Iterator<Prescribed<LoadValue> >(this, createPrescribedLoad( first_node ), index ) );
}

//------------------------------------------------------------------
Boundary* XMLInputHandler::createBoundary(XS::DOMElement* node, numbered loop_number)
{
  IDObject::ID target_type;
  std::vector<numbered>* target_number_vector = 0;
  if ( (target_number_vector = getReferenceNumberVector(node,GLOBAL_EDGE_NUM)) != 0 )
    target_type = IDObject::EDGE;
  else if ( (target_number_vector = getReferenceNumberVector(node,GLOBAL_FACE_NUM)) != 0 )
    target_type = IDObject::FACE;
  else
    throw EXC(IOException) << "Boundary target must be either of type EDGE (" 
                           <<  GLOBAL_EDGE_NUM << "=) or FACE ("
                           <<  GLOBAL_FACE_NUM << "=)" << Exception::endl;

  return(new Boundary(getNumber(node), 
                      target_number_vector,
                      target_type,
                      loop_number));
}

//------------------------------------------------------------------
Boundary* XMLInputHandler::next(Boundary* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<Boundary>::xml_container_,
                                    BOUNDARY,
                                    index );
  if(!next_node) {
    InputAggregate<Boundary>::xml_container_ = InputAggregate<Boundary>::xml_container_->getNextElementSibling();
    if(!InputAggregate<Boundary>::xml_container_)
      return(0);
    else {
      index = 0;
      next_node = findFirst( InputAggregate<Boundary>::xml_container_,
                             BOUNDARY );
      if(!next_node)
        return(0);
    }
  }
  numbered loop_number = getNumber(InputAggregate<Boundary>::xml_container_); 
  return( createBoundary( next_node , loop_number ) );
}

//------------------------------------------------------------------
Iterator<Boundary> XMLInputHandler::begin( Boundary* )
{
  XS::DOMElement* boundary_cont = getChild(BOUNDARY_CONTAINER);
  if( !boundary_cont ) {
    LOG(Logger::WARN) << "No Boundary Container found!" << Logger::endl;
    return( Aggregate<Boundary>::end() );
  }
  numbered index = 0;
  InputAggregate<Boundary>::xml_container_ = getChild(LOOP,false,boundary_cont);
  if( !InputAggregate<Boundary>::xml_container_ ) {
    LOG(Logger::WARN) << "No Loop inside Boundary Container found!" << Logger::endl;
    return( Aggregate<Boundary>::end() );
  }

  numbered loop_number = getNumber(InputAggregate<Boundary>::xml_container_);
  DOMElement* first_node = findFirst( InputAggregate<Boundary>::xml_container_,
                                      BOUNDARY );
  if(!first_node){
    return( Aggregate<Boundary>::end() );
    LOG(Logger::WARN) << "No Boundary was found!" << Logger::endl;
  }
  
  return( Iterator<Boundary>(this, createBoundary( first_node, loop_number ), index ) );
}

//------------------------------------------------------------------
TimeStamp* XMLInputHandler::create( TimeStamp* ,XS::DOMElement* node)
{
  bool found = false;
  double time = getDouble(node,TIME,found);
  if( !found )
    throw EXC(IOException) << "We need the time ("<<TIME<<"=) to create a TimeStamp!" << Exception::endl;

  return( new TimeStamp( time , getNumber(node) ) );
}

//------------------------------------------------------------------
TimeStamp* XMLInputHandler::next(TimeStamp* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<TimeStamp>::xml_container_,
                                    TIME_STAMP,
                                    index );
  if(!next_node) return(0);
  return( create( (TimeStamp*)0, next_node ) ); 
}

//------------------------------------------------------------------
Iterator<TimeStamp> XMLInputHandler::begin( TimeStamp* )
{
  InputAggregate<TimeStamp>::xml_container_ = getChild( RESULT );
  if( !InputAggregate<TimeStamp>::xml_container_ ) {
    LOG(Logger::WARN) << "No Results found!" << Logger::endl;
    return( InputAggregate<TimeStamp>::end() );
  }
  numbered index = 0;
  DOMElement* first_node = findFirst( InputAggregate<TimeStamp>::xml_container_,
                                      TIME_STAMP );
  if(!first_node){
    LOG(Logger::WARN) << "No time stamps in result countainer were found!" << Logger::endl;
    return( InputAggregate<TimeStamp>::end() );
  }
  
  return( Iterator<TimeStamp>(this, create( (TimeStamp*)0, first_node ), index ) );
}

//------------------------------------------------------------------
template<class T>
Iterator<T> XMLInputHandler::begin( T* , const TimeStamp& time_stamp )
{
  XS::DOMElement* first_node = getFirstNodeResultForTimeStamp<T>( time_stamp );
  if( !first_node )
    return( InputAggregate<T>::end() );
  else {
    T* first_obj = this->create( (T*)0, first_node );
    if( !first_obj )
      return( InputAggregate<T>::end() );
    return( Iterator<T>(this, first_obj , 0 ) );
  }
}

//------------------------------------------------------------------
template<class T>
T* XMLInputHandler::next(T* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<T>::xml_container_,
                                    NODE,
                                    index );
  if(!next_node) return(0);
  return( create( act_item, next_node ) ); 
}

//------------------------------------------------------------------
template<class T>
XS::DOMElement* XMLInputHandler::getFirstNodeResultForTimeStamp( const TimeStamp& time_stamp )
{
  DOMElement* result = getChild( RESULT );
  if( !result ) {
    LOG(Logger::WARN) << "No Results found!" << Logger::endl;
    return( 0 );
  }

  DOMNodeList* list = result -> getElementsByTagName( X( TIME_STAMP ) );
  InputAggregate<T>::xml_container_ = 0;
  XMLSize_t list_length = list->getLength();
  for( XMLSize_t counter = 0 ; counter < list_length ; ++counter ) {
    if( getNumber( dynamic_cast<DOMElement*>(list->item( counter )) ) == time_stamp.index() )
      InputAggregate<T>::xml_container_ = dynamic_cast<DOMElement*>(list->item( counter ));
  }

  if( !InputAggregate<T>::xml_container_ ) {
    LOG(Logger::WARN) << "The required time stamp with index '" << time_stamp.index() << "' couldn't be found!" << Logger::endl;
    return( 0 );
  }
  DOMElement* first_node = findFirst( InputAggregate<T>::xml_container_,
                                      NODE );
  if(!first_node){
    LOG(Logger::WARN) << "No node results for given time stamp with index '" << time_stamp.index() << "' were found!" << Logger::endl;
    return( 0 );
  }
  
  return( first_node );
}

//------------------------------------------------------------------
DisplacementDOF* XMLInputHandler::create( DisplacementDOF*, XS::DOMElement* node)
{
  bool found = false;
  return(new DisplacementDOF(Model::getWorldCoordSys(),
                             getNumber(node),
                             0,
                             getDouble(node,U_DISP,found),
                             getDouble(node,V_DISP,found),
                             getDouble(node,W_DISP,found) ) );
}

//------------------------------------------------------------------
ForceLoad* XMLInputHandler::create( ForceLoad*, XS::DOMElement* node)
{
  double f_x = 0., f_y = 0., f_z = 0.;
  std::vector<double>* vector = getDoubleVector(node, FORCE);
  if( vector ) {
    if( vector -> size() != 0 )
      f_x = vector -> at(0);
    else
      throw EXC(IOException) << "Force load with 0 components makes no sense" << Exception::endl;
  }
  if( vector->size() > 1 )
    f_y = vector -> at(1);
  if( vector->size() > 2 )
    f_z = vector -> at(1);

  delete vector;
  return( new ForceLoad(Model::getWorldCoordSys(),
                        getNumber(node),
                        0, f_x, f_y , f_z) );
}

//------------------------------------------------------------------
LagrangeDOF* XMLInputHandler::create( LagrangeDOF*, XS::DOMElement* node)
{
  bool enabled = false, stick = false;
  double lambda_N = 0., lambda_R = 0., lambda_S = 0.;
  std::vector<double>* vector = getDoubleVector(node, LAGRANGE);
  if( vector ) {
    if( vector -> size() != 0 ) {
      lambda_N = vector -> at(0);
      enabled = true;
    }
    if( vector->size() > 1 ) {
      lambda_R = vector -> at(1);
      stick = true;
    }
    if( vector->size() > 2 )
      lambda_S = vector -> at(1);
  }

  delete vector;
  return( new LagrangeDOF(Model::getWorldCoordSys(), enabled, stick,
                          getNumber(node), 0, 
                          lambda_N, lambda_R , lambda_S) );
}

//------------------------------------------------------------------
TimeEpisode* XMLInputHandler::next(TimeEpisode* act_item, int& index)
{
  DOMElement* next_node = findNext( InputAggregate<TimeEpisode>::xml_container_,
                                    TIME_EPISODE,
                                    index );
  if(!next_node) return(0);
  return( createTimeEpisode( next_node ) );
}

//------------------------------------------------------------------
Iterator<TimeEpisode> XMLInputHandler::begin( TimeEpisode* )
{
  InputAggregate<TimeEpisode>::xml_container_ = getChild(TIME_BAR);
  if( !InputAggregate<TimeEpisode>::xml_container_ ) {
    LOG(Logger::WARN) << "No <"<<TIME_BAR<<"> defined - This will only work for linear analysis." << Logger::endl;
    return( Aggregate<TimeEpisode>::end() );    
  }

  numbered index = 0;
  DOMElement* first_node = findFirst( InputAggregate<TimeEpisode>::xml_container_,
                                      TIME_EPISODE );
  if(!first_node)
    throw EXC(IOException) << "A time bar ("<<TIME_BAR<<") without a time episode ("<<TIME_EPISODE<<") doesn't make any sense at all!" << Exception::endl;
  
  return( Iterator<TimeEpisode>(this, createTimeEpisode( first_node ), index ) ); 
}

//------------------------------------------------------------------
TimeEpisode* XMLInputHandler::createTimeEpisode(XS::DOMElement* node)
{
  bool found = false;
  double end_time;
  int step_amount;
   
  end_time = getDouble( node, END_TIME, found );
  if( !found )
    throw EXC(IOException) << "A time episode ("<<TIME_EPISODE<<") needs a end time ("<<END_TIME<<")!" << Exception::endl;
  step_amount = getInt( node, STEP_AMOUNT, found );
  if( !found )
    throw EXC(IOException) << "A time episode ("<<TIME_EPISODE<<") needs the step amount ("<<STEP_AMOUNT<<")!" << Exception::endl;

  TimeEpisode* time_episode = new TimeEpisode( getNumber(node) , end_time, step_amount );

  int index = 0;
  std::vector<numbered> *nr_vector = 0;
  IDObject::ID progress_shape = IDObject::NAID; 
  for( XS::DOMElement* progress = findFirst( node, PROGRESS ) ;
       progress != 0 ;
       progress = findNext( node, PROGRESS, index ) ) {
    progress_shape = IDObject::resolveID( getString( progress, ID , found ) );
    if( !found )
      throw EXC(IOException) << "We need an ("<<ID<<") to distinguish between '"
                             << IDObject::RAMP_STR<<"' and '"
                             << IDObject::CONST_STR<<"' progress" << Exception::endl;
    if( progress_shape == IDObject::NAID )
      throw EXC(IOException) << "The given progress shape '" 
                             << getString( node, ID , found ) << "' is not defined." << Exception::endl;

    nr_vector = getReferenceNumberVector( progress, DOF );
    if( nr_vector ) {
      time_episode -> addProgress( new DOFProgress(progress_shape,nr_vector) );
      continue;
    }
      
    nr_vector = getReferenceNumberVector( progress, LOAD );
    if( nr_vector ) {
      time_episode -> addProgress( new LoadProgress(progress_shape,nr_vector) );
      continue;
    }
  }
  if(!index)
    throw EXC(IOException) << "At least one Dirichlet boundary condition must be defined - else FE analysis makes no sense" << Exception::endl; 


  return( time_episode );
}

//------------------------------------------------------------------
void XMLInputHandler::get( Global& global_data )
{
  bool found = false;
  unsigned dimension = 0;
  DOMElement* node = getChild(GLOBAL_DATA);
  if( !node )
    throw EXC(IOException) << "Input file without <global> is really useless in SOOFEA!" << Exception::endl;

  global_data.setID( getString(node,ID,found) );
  
  std::string analysis_type_string = getString(node,ANALYSIS_TYPE,found);
  if( !found )
    throw EXC(IOException) << "The analysis type ("<<ANALYSIS_TYPE<<"=) has to be set in global!" << Exception::endl;
  IDObject::ID analysis_id = CoordSys::resolveID(analysis_type_string);
  if( analysis_id == IDObject::NAID )
    throw EXC(IOException) << "Analysis type '"
                           << analysis_type_string
                           << "' not defined as analysis id!" << Exception::endl;
  global_data.setAnalysisType( analysis_id );

  dimension = getInt(node,DIM,found);
  if( !found )
    throw EXC(IOException) << "The Dimension ("<<DIM<<"=) has to be set in global!" << Exception::endl;

  global_data.setDimension( dimension );
  
  std::string coord_sys_type_string = getString(node,COORD_SYS_TYPE,found);
  if( !found )
    throw EXC(IOException) << "Coordinate system type ("<<COORD_SYS_TYPE<<"=) has to be set in global!" << Exception::endl;
  CoordSys::ID coord_sys_id = CoordSys::resolveID(coord_sys_type_string);
  if( coord_sys_id == CoordSys::NAID )
    throw EXC(IOException) << "Coordinate system type '"
                           << coord_sys_type_string
                           << "' not defined as coord sys id!" << Exception::endl;
  global_data.setCoordSysID( coord_sys_id );

  double convergence_criteria = getDouble(node,CONV_CRIT,found);
  if( !found )
    LOG(Logger::WARN) << "No convergence criteria found in <global> tag - might only work for linear analysis!" << Logger::endl;
  global_data.setConvergenceCriteria( convergence_criteria );

  std::string sle_solution_method_string = getString(node,SLE_SOLUTION_METHOD,found);
  if( !found ) {
    LOG(Logger::WARN) << "No solution method given in <global> tag to solve system of linear equation - assuming \"cholesky\"!" << Logger::endl;
    sle_solution_method_string = "cholesky";
  }
  IDObject::ID sle_solution_method_id = IDObject::resolveID(sle_solution_method_string);
  if( sle_solution_method_id == IDObject::NAID )
    sle_solution_method_id = IDObject::LU;
  global_data.setSLESolutionMethod( sle_solution_method_id );
}

//------------------------------------------------------------------
void XMLInputHandler::get( Contact& contact_data )
{
  DOMElement* node = getChild(CONTACT_DATA);
  bool found = false;

  if( !node ) {
    LOG(Logger::WARN) << "No Contact input tag was found" << Logger::endl;
    contact_data.setContact(false);
    return;
  }

  contact_data.setContact(true);
  contact_data.setMortarNumber( getReferenceNumberVector(node, MORTAR) );
  contact_data.setNonMortarNumber( getReferenceNumberVector(node, NON_MORTAR) );

  contact_data.setFriction( getBool(node, FRICTION, found) );
  contact_data.setMu( getDouble(node, MU, found) );
  if( contact_data.getFriction() ) {
    if( !found )
      throw EXC(IOException) << "Coulomb friction with out mu doesn't make any sense!" << Exception::endl;
  }

  contact_data.setPenalty( getDouble( node, PENALTY, found ) );
  if( !found )
    LOG(Logger::WARN) << "Penalty factor ("<<PENALTY<<"=) not set in contact!" << Logger::endl;
}
