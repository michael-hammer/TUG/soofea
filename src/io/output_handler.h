#ifndef output_handler_h__
#define output_handler_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem; 

#include "model/model.h"
#include "base/configuration.h"
#include "input_handler.h"

//------------------------------------------------------------------
class OutputHandler
{
public:
  OutputHandler(IDObject::ID io_type_id) :
    io_type_(io_type_id) {}

  virtual ~OutputHandler() {};

  virtual void flush() = 0;

  virtual void write( Model& model ) = 0;

  IDObject::ID getIOType() {return( io_type_ );}
protected:
  IDObject::ID io_type_;
  Model* model_;
};

#endif
