#ifndef gid_output_handler_h__
#define gid_output_handler_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <string>
#include <iostream>

#include "exceptions.h"

#include "output_handler.h"
#include "gid_base.h"

#include "model/model.h"
#include "analyzer/element_impl.h"

//------------------------------------------------------------------
class GiDOutputHandler : 
  public OutputHandler,
  public GiDBase
{
public:
  /**
     Sets the file path for the file to write. Here we won't open nor
     write to the file. We only test if the file exists and warn if it
     does - because it is going to be overwritten.
   */
  GiDOutputHandler(const fs::path& file_path);

  virtual ~GiDOutputHandler() {}

  virtual void flush();

  virtual void write( Model& model );

protected:
  std::string gid_file_;

  virtual void add(const Global& global);

  virtual void writeNodes( );
  virtual void writeElements( );
};

#endif
