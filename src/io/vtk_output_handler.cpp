/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "vtk_output_handler.h"
#include "vtkCell.h"
#include "vtkHexahedron.h"
#include "vtkDoubleArray.h"
#include "vtkPointData.h"
#include "vtkXMLUnstructuredGridWriter.h"

#include <sstream>

//------------------------------------------------------------------
VTKOutputHandler::VTKOutputHandler(const fs::path& file_path) :
  OutputHandler(IDObject::VTK),
  file_path_(file_path)
{}

//------------------------------------------------------------------
void VTKOutputHandler::flush()
{}

//------------------------------------------------------------------
void VTKOutputHandler::write( Model& model )
{
  Iterator<TimeStamp> time_iter = model.getTimeBar().Aggregate<TimeStamp>::begin();
  for( ++time_iter ; // First TimeStamp would be Lagrangian state
       time_iter != model.getTimeBar().Aggregate<TimeStamp>::end() ;
       ++time_iter ) {
    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
    vtkSmartPointer<vtkUnstructuredGrid> mesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
    writer->SetInput( mesh );
    
    std::stringstream output_file;
    output_file << file_path_.string() << "." << time_iter->index();
    writer->SetFileName( output_file.str().c_str() );

    model_ = &model;
    
    writeNodes( mesh );
    writeElements( mesh );

    //  std::cout << "Serializing time step " << time_iter->index() << " / " << model.getTimeBar().getLastFinishedTimeStamp().index() << std::endl;
    
    // We need this if we were not able to finish the calculation
    if( TimeStampComp()( model.getTimeBar().getLastFinishedTimeStamp(), *time_iter) ) {
      writer->Write();
      return;
    }

    writeNodeResults( *time_iter , mesh );
    writer->Write();
  }
}

//------------------------------------------------------------------
void VTKOutputHandler::writeNodes(vtkSmartPointer<vtkUnstructuredGrid>& mesh)
{
  double xyz[3];
  vtkSmartPointer<vtkPoints> node_list = vtkSmartPointer<vtkPoints>::New();
  for( Iterator<Node> iter = model_->Aggregate<Node>::begin();
       iter != model_->Aggregate<Node>::end() ; 
       ++iter ) {
    xyz[0] = iter->getLagrangianPoint().getCoordinate(CoordSys::X);
    xyz[1] = iter->getLagrangianPoint().getCoordinate(CoordSys::Y);
    xyz[2] = iter->getLagrangianPoint().getCoordinate(CoordSys::Z);
    node_list->InsertPoint( iter->getNumber() , xyz );
  }  
  mesh->SetPoints(node_list);
}

//------------------------------------------------------------------
void VTKOutputHandler::writeElements(vtkSmartPointer<vtkUnstructuredGrid>& mesh)
{
  vtkSmartPointer<vtkCell> cell;
  for( Iterator<Element> iter = model_->Aggregate<Element>::begin();
       iter != model_->Aggregate<Element>::end() ; 
       ++iter ) {
    switch( iter->getType().getShape().getID() ) {
    case IDObject::BRICK:
      switch( iter->getType().getShape().getOrder() ) {
      case 1:
        cell = vtkSmartPointer<vtkHexahedron>::New();
        break;
      }
      break;
    default:
      throw EXC(IOException) << "VTK Output only implemented for linear Hexagons ATM" << Exception::endl;
      break;
    }

    for( unsigned node_counter = 0 ; node_counter != iter->getType().getShape().getNodeAmount() ; ++node_counter ) {
      cell -> GetPointIds()->InsertId( node_counter, iter->Aggregate<Node>::at(node_counter).getNumber() );
    }
    mesh->InsertNextCell(cell->GetCellType(), cell->GetPointIds());
  }
}

//------------------------------------------------------------------
void VTKOutputHandler::writeNodeResults(TimeStamp& time_stamp,vtkSmartPointer<vtkUnstructuredGrid>& mesh)
{
  vtkSmartPointer<vtkDoubleArray> disp_array = vtkDoubleArray::New();
  disp_array->SetNumberOfComponents(3);
  disp_array->SetName("displacement");
  double uvw[3];
  for( Iterator<Node> iter = model_->Aggregate<Node>::begin();
       iter != model_->Aggregate<Node>::end() ; 
       ++iter ) {
    if( iter->getDOF( IDObject::DISPLACEMENT ) ) {
      uvw[0] = iter->getDOF( IDObject::DISPLACEMENT )->getValue(CoordSys::X)->getValue(time_stamp);
      uvw[1] = iter->getDOF( IDObject::DISPLACEMENT )->getValue(CoordSys::Y)->getValue(time_stamp);
      uvw[2] = iter->getDOF( IDObject::DISPLACEMENT )->getValue(CoordSys::Z)->getValue(time_stamp);
      disp_array -> InsertTupleValue( iter->getNumber(), uvw );
    }
  }
  mesh->GetPointData()->SetVectors(disp_array);
}
