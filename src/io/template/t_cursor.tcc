/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

//------------------------------------------------------------------
template <class Item>
Cursor<Item>::Cursor(Item start_cursor_value)
{
  cursor_value_ = start_cursor_value;
}

//------------------------------------------------------------------
template <class Item>
Cursor<Item>::Cursor(const Cursor &src)
{
  cursor_value_ = src.cursor_value_;
}

//------------------------------------------------------------------
template <class Item>
bool Cursor<Item>::operator==(const Cursor<Item>& right)
{
  return(cursor_value_ == right.cursor_value_);
}

//------------------------------------------------------------------
template <class Item>
Cursor<Item> Cursor<Item>::operator++(int)
{
  Cursor<Item> before_cursor(cursor_value_);
  cursor_value_++;
  return before_cursor;
}

//------------------------------------------------------------------
template <class Item>
Cursor<Item> Cursor<Item>::operator--(int)
{
  Cursor<Item> before_cursor(cursor_value_);
  cursor_value_--;
  return before_cursor;  
}

//------------------------------------------------------------------
template <class Item>
const Item Cursor<Item>::getValue() const
{
  return(cursor_value_);
}
