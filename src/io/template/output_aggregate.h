#ifndef output_aggregate_h__
#define output_aggregate_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "analyzer/time_bar.h"

//------------------------------------------------------------------
template<class T>
class OutputAggregate
{
public:
  OutputAggregate() {}

  virtual ~OutputAggregate() {}

  virtual void push(const T& item, TimeStep step)
  { this -> push_back( item , step ); }

protected:
  virtual void push_back(const T& item, TimeStep step) = 0;
};

#endif // output_aggregate_h__
