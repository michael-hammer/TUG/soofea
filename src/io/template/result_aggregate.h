#ifndef result_aggregate_h___
#define result_aggregate_h___

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2010  Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "input_aggregate.h"

#include <xercesc/dom/DOM.hpp>
#define XS XERCES_CPP_NAMESPACE

//------------------------------------------------------------------
template<class T>
class ResultAggregate :
  public InputAggregate<T>
{
public:
  ResultAggregate() {}

  Iterator<T> begin( const TimeStamp& time_stamp ) 
  { return( this->begin(static_cast<T*>(0), time_stamp ) ); }

protected:
  using Aggregate<T>::begin;
  virtual Iterator<T> begin( T* );

  virtual Iterator<T> begin( T* , const TimeStamp& time_stamp ) = 0;

  using Aggregate<T>::at;
  inline virtual T* at( T* t, int index )
  { return( this->get(t,index) ); }
};

#include "result_aggregate.tcc"

#endif
