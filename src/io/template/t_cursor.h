#ifndef t_cursor_h__
#define t_cursor_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

template<class Item>
class Cursor
{
public:
  Cursor(Item cursor_start_value);
  Cursor(const Cursor &src);

  const Item getValue() const;

  bool operator==(const Cursor<Item>& right);
  Cursor<Item> operator++(int);
  Cursor<Item> operator--(int);
private:
  Cursor();
  Item cursor_value_;
};

typedef Cursor<long> InputCursor;

#include "t_cursor.tcc"

#endif // t_cursor_h__
