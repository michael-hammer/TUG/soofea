#include "io.h"

void export_io()
{
  using namespace boost::python;

  class_<ModelHandler>("ModelHandler",no_init)
    .def( "read", &ModelHandler::read )
    .def( "readResults", &ModelHandler::readResults )
    .def( "write" , &ModelHandler::write )
    .def( "getModel" , &ModelHandler::getModel , return_value_policy<reference_existing_object>() )
    .def( "flush" , &ModelHandler::flush )
    ;
}
