/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * Authors: Michael Hammer <michael@derhammer.net>
 * ------------------------------------------------------------------*/

#include "xml_output_handler.h"

//------------------------------------------------------------------
void XMLOutputHandler::flush()
{
  parser_ -> flush();
}

//------------------------------------------------------------------
void XMLOutputHandler::write( Model& model )
{
  model_ = &model;

  if( write_mesh_ ) {
    LOG(Logger::INFO) << "We also have to write the model and not only the results" << Logger::endl;
    writeGlobal();
    writeNodes();
    writeTypes();
    writeMaterials();
    writeElements();
    writeBoundaries();
  }

  XS::DOMElement* result = getChild( RESULT );
  if( result ) {
    LOG(Logger::INFO) << "Old results were found in output file - we will delete them!" << Logger::endl;
    removeChild( result );
  } 
  
  result = getChild( RESULT , true );
  addAttribute( result, LAST_TIME_INDEX , toString( model.getTimeBar().getLastFinishedTimeStamp().index() ) );

  bool new_element;
  Iterator<TimeStamp> time_iter = model.getTimeBar().Aggregate<TimeStamp>::begin();
  for( ++time_iter ; // First TimeStamp would be Lagrangian state
       time_iter != model.getTimeBar().Aggregate<TimeStamp>::end() ;
       ++time_iter ) {
    //    std::cout << "Serializing time step " << time_iter->index() << " / " << model.getTimeBar().getLastFinishedTimeStamp().index() << std::endl;

    // We need this if we were not able to finish the calculation
    if( TimeStampComp()( model.getTimeBar().getLastFinishedTimeStamp(), *time_iter) )
      break;

    XS::DOMElement* xml_time_step = addNumberedElementToContainer( result, TIME_STAMP, time_iter->index(), new_element );
    addAttribute( xml_time_step, TIME, toString( time_iter->time() ) );

    writeNodeResults( *time_iter, xml_time_step );

//    writeElementResults()
//    if( Configuration::getInstance().getWrite() == Configuration::ITERATION ) {
//      for( unsigned iteration_counter = 0 ; iteration_counter < time_iter->iteration() ; ++iteration_counter ) {
//        TimeStamp* act_time_stamp = new TimeStamp( time_iter->time() , time_iter->index() );
//        act_time_stamp -> iteration() = iteration_counter;
//        
//        delete act_time_stamp;
//      }
//    } else {
//    }

  }
}

//------------------------------------------------------------------
void XMLOutputHandler::writeNodeResults(TimeStamp& time_stamp, DOMElement* xml_time_step)
{
  DOMElement* node_container = getChild( NODE_CONTAINER , true, xml_time_step );
  std::stringstream ss; ss.precision(16);
  Load* load = 0;
  //  DOF* dof = 0;
  DOFValue* dof_value = 0;

  for( Iterator<Node> iter = model_->Aggregate<Node>::begin();
       iter != model_->Aggregate<Node>::end() ; 
       ++iter ) {
    ss.str("");
    DOMElement* node = addNumberedElementToContainer( node_container, NODE, iter -> getNumber() );
    if( iter->getDOF( IDObject::DISPLACEMENT ) ) {
      load = iter->getLoad( IDObject::FORCE );
      addAttribute( node, U_DISP, toString(iter->getDOF( IDObject::DISPLACEMENT )->getValue(CoordSys::X)->getValue(time_stamp)) );
      ss << load->getValue(CoordSys::X)->getInternal(time_stamp);
      if( dynamic_cast<DisplacementDOF*>(iter->getDOF( IDObject::DISPLACEMENT ))->getCoordSys().getDimension() > 1 ) {
        addAttribute( node, V_DISP, toString(iter->getDOF( IDObject::DISPLACEMENT )->getValue(CoordSys::Y)->getValue(time_stamp)) );
        ss << " " << load->getValue(CoordSys::Y)->getInternal(time_stamp);
      }
      if( dynamic_cast<DisplacementDOF*>(iter->getDOF( IDObject::DISPLACEMENT ))->getCoordSys().getDimension() > 2 ) {
        addAttribute( node, W_DISP, toString(iter->getDOF( IDObject::DISPLACEMENT )->getValue(CoordSys::Z)->getValue(time_stamp)) );
        ss << " " << load->getValue(CoordSys::Z)->getInternal(time_stamp);
      }
      addAttribute( node, FORCE, ss.str() );
    }
    ss.str("");
    if( iter -> getDOF( IDObject::LAGRANGE ) ) {
      dof_value = iter -> getDOF( IDObject::LAGRANGE )->getValue(CoordSys::NORMAL);
      if( dof_value -> enabled(time_stamp) ) {
        ss << dof_value -> getValue(time_stamp);
        dof_value = iter -> getDOF( IDObject::LAGRANGE )->getValue(CoordSys::TANGENT_R);
        if( dof_value ) {
          if( dof_value -> enabled(time_stamp) ) {
            ss << " " << dof_value->getValue(time_stamp);
          }
        }
        dof_value = iter -> getDOF( IDObject::LAGRANGE )->getValue(CoordSys::TANGENT_S);
        if( dof_value ) {
          if( dof_value -> enabled(time_stamp) ) {
            ss << " " << dof_value->getValue(time_stamp);
          }
        }
        addAttribute( node, LAGRANGE, ss.str() );
      }
    }
  }
}

//------------------------------------------------------------------
void XMLOutputHandler::writeNodes()
{
  DOMElement* node_container = getChild( NODE_CONTAINER , true );
  if( node_container -> getChildNodes() -> getLength() ) {
    removeChild( node_container );
    node_container = getChild( NODE_CONTAINER , true );
  }

  for( Iterator<Node> iter = model_->Aggregate<Node>::begin();
       iter != model_->Aggregate<Node>::end() ; 
       ++iter ) {
    DOMElement* node = addNumberedElementToContainer( node_container, NODE, iter -> getNumber() );
    addAttribute( node, X_COORD, toString( iter->getLagrangianPoint().getCoordinate(CoordSys::X) ));
    if( iter->getLagrangianPoint().getCoordSys()->getDimension() > 1 )
      addAttribute( node, Y_COORD, toString( iter->getLagrangianPoint().getCoordinate(CoordSys::Y) ));
    if( iter->getLagrangianPoint().getCoordSys()->getDimension() > 2 )
      addAttribute( node, Z_COORD, toString( iter->getLagrangianPoint().getCoordinate(CoordSys::Z) ));
  }  
}

//------------------------------------------------------------------
void XMLOutputHandler::writeTypes()
{
  DOMElement* type_container = getChild( TYPE_CONTAINER , true );
  if( type_container -> getChildNodes() -> getLength() ) {
    removeChild( type_container );    
    type_container = getChild( TYPE_CONTAINER , true );
  }

  for( Iterator<Type> iter = model_->Aggregate<Type>::begin();
       iter != model_->Aggregate<Type>::end() ; 
       ++iter ) {
    DOMElement* type = addNumberedElementToContainer( type_container, TYPE, iter -> getNumber() );
    addAttribute( type, ID, IDObject::resolveID( iter->getID() ) );
    addAttribute( type, TARGET, IDObject::resolveID( iter->getTarget() ) );
    addAttribute( type, SHAPE_ORDER, toString( iter->getShape().getOrder() ) );
    addAttribute( type, SHAPE_TYPE, toString( iter->getShape().getID() ) );
    addAttribute( type, INT_POINTS, iter->getAmountOfIntPointsString() );
  }
}

//------------------------------------------------------------------
void XMLOutputHandler::writeMaterials()
{
  DOMElement* material_container = getChild( MATERIAL_CONTAINER , true );
  if( material_container -> getChildNodes() -> getLength() ) {
    removeChild( material_container );    
    material_container = getChild( MATERIAL_CONTAINER , true );
  }

  for( Iterator<Material> iter = model_->Aggregate<Material>::begin();
       iter != model_->Aggregate<Material>::end() ; 
       ++iter ) {
    DOMElement* material = addNumberedElementToContainer( material_container, MATERIAL, iter -> getNumber() );
    addAttribute( material, ID, IDObject::resolveID( iter->getID() ) );
  }  
}

//------------------------------------------------------------------
void XMLOutputHandler::writeElements()
{
  DOMElement* element_container = getChild( ELEMENT_CONTAINER , true );
  if( element_container -> getChildNodes() -> getLength() ) {
    removeChild( element_container );    
    element_container = getChild( ELEMENT_CONTAINER , true );
  }

  for( Iterator<Element> iter = model_->Aggregate<Element>::begin();
       iter != model_->Aggregate<Element>::end() ; 
       ++iter ) {
    DOMElement* element = addNumberedElementToContainer( element_container, ELEMENT, iter -> getNumber() );
    addAttribute( element, TYPE , toString(iter -> getType().getNumber()) );
    addAttribute( element, MATERIAL, toString(iter -> getMaterial().getNumber()) );
    addAttribute( element, GLOBAL_NODE_NUM, iter -> getNodeNumbersString() );
  }
}

//------------------------------------------------------------------
void XMLOutputHandler::writeBoundaries()
{
  DOMElement* boundary_container = getChild( BOUNDARY_CONTAINER , true );
  if( boundary_container -> getChildNodes() -> getLength() ) {
    removeChild( boundary_container );    
    boundary_container = getChild( BOUNDARY_CONTAINER , true );
  }
  
  for( Iterator<Boundary> iter = model_->Aggregate<Boundary>::begin();
       iter != model_->Aggregate<Boundary>::end() ; 
       ++iter ) {
    // DOMElement* boundary = 
    addNumberedElementToContainer( boundary_container, BOUNDARY, iter -> getNumber() );
    // addAttribute( boundary, ID, IDObject::resolveID( iter -> getType() ) );
    // addAttribute( boundary, GLOBAL_NODE_NUM, iter -> getNodeNumbersString() );
  }
}

//------------------------------------------------------------------
void XMLOutputHandler::writeGlobal()
{
  DOMElement* global = getChild( GLOBAL_DATA , true );
  if( global -> getChildNodes() -> getLength() ) {
    removeChild( global );    
    global = getChild( GLOBAL_DATA , true );
  }
  
  addAttribute( global, ANALYSIS_TYPE, IDObject::resolveID( model_->getGlobal().getAnalysisType() ) );
  addAttribute( global, CONV_CRIT, toString( model_->getGlobal().getConvergenceCriteria() ) );
  addAttribute( global, ID, model_->getGlobal().getID() );
  addAttribute( global, COORD_SYS_TYPE, IDObject::resolveID( model_->getGlobal().getCoordSysID() ) );
  addAttribute( global, DIM, toString(model_->getGlobal().getDimension()) );
  addAttribute( global, SLE_SOLUTION_METHOD, IDObject::resolveID( model_->getGlobal().getSLESolutionMethod() ) );
}
