/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "model_handler.h"

//------------------------------------------------------------------
ModelHandler::ModelHandler(InputHandler* input_handler, OutputHandler* output_handler):
  input_handler_(input_handler),
  output_handler_(output_handler),
  model_( new Model() ),
  bc_handler_(0)
{}

//------------------------------------------------------------------
void ModelHandler::read()
{
  input_handler_ -> readModel( *model_ );
  bc_handler_ = input_handler_ -> createBCHandler( *model_ );
}

//------------------------------------------------------------------
void ModelHandler::readResults()
{
  input_handler_ -> readResults( *model_ );
}

//------------------------------------------------------------------
void ModelHandler::write()
{
  output_handler_ -> write( *model_ );
}

//------------------------------------------------------------------
ModelHandler::~ModelHandler()
{
  if(model_)
    delete model_;

  if( bc_handler_ )
    delete bc_handler_;

  if(input_handler_)
    delete input_handler_;
  
  if(output_handler_)
    delete output_handler_;
}
