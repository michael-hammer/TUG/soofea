/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include "gid_output_handler.h"

//------------------------------------------------------------------
GiDOutputHandler::GiDOutputHandler(const fs::path& file_path) :
  OutputHandler(IDObject::GID)
{
  if( fs::exists(file_path) ) {
#if BOOST_FILESYSTEM_VERSION == 3 
    LOG(Logger::WARN) <<  "Output file '" << file_path.string()
                      << "' allready exists and will be overwritten!" << Logger::endl;
  }
  gid_file_ = file_path.string();
#endif
#if BOOST_FILESYSTEM_VERSION <= 2
  LOG(Logger::WARN) <<  "Output file '" << file_path.native_file_string()
                    << "' allready exists and will be overwritten!" << Logger::endl;
 }
 gid_file_ = file_path.native_file_string();
#endif
 
 GiDBase::openPostMeshFile( gid_file_ + ".msh" );
 GiDBase::openPostResultFile( gid_file_ + ".res" );
}

//------------------------------------------------------------------
void GiDOutputHandler::flush()
{
  GiD_ClosePostMeshFile();
  GiD_ClosePostResultFile();
}

//------------------------------------------------------------------
void GiDOutputHandler::write( Model& model )
{
  model_ = &model; 

  add( model_->getGlobal() );

  // GiDBase::evaluateElementType( *model_->Aggregate<Type>::begin() );
  GiDBase::beginMesh();

  writeNodes( );
  writeElements( );

  GiD_EndMesh();
}

//------------------------------------------------------------------
void GiDOutputHandler::add(const Global& global)
{
  GiDBase::setGlobal(global);
}

//------------------------------------------------------------------
void GiDOutputHandler::writeNodes( )
{
  GiD_BeginCoordinates();
  for( Iterator<Node> iter = model_->Aggregate<Node>::begin();
       iter != model_->Aggregate<Node>::end() ; 
       ++iter )
    GiDBase::writeNode( *iter ); 
  GiD_EndCoordinates();  

  /** @todo use TimeStamp stored in model      vvv */ 
  Iterator<TimeStamp> time_iter = model_->getTimeBar().Aggregate<TimeStamp>::begin();

  for( ++time_iter ; // First TimeStamp would be Lagrangian state
       time_iter != model_->getTimeBar().Aggregate<TimeStamp>::end() ;
       ++time_iter ) {
    if( TimeStampComp()( model_->getTimeBar().getLastFinishedTimeStamp(), *time_iter) )
      break;

    if( Configuration::getInstance().getWrite() == IDObject::ITERATION_WRITE ) {
      for( unsigned iteration_counter = 0 ; iteration_counter < time_iter->iteration() ; ++iteration_counter ) {
        TimeStamp* act_time_stamp = new TimeStamp( time_iter->time() , time_iter->index() );
        act_time_stamp -> iteration() = iteration_counter;
        GiD_BeginResultGroup((char*)"Nodal Analysis", act_time_stamp->index()*100 + iteration_counter , GiD_OnNodes, 0 );
        GiDBase::beginNodeDisplacement();
        //        GiDBase::beginNodeInternalLoad(); 
        for( Iterator<Node> iter = model_->Aggregate<Node>::begin();
             iter != model_->Aggregate<Node>::end() ; 
             ++iter ) {
          GiDBase::writeNodeDisplacement( *iter , *act_time_stamp );
          //          GiDBase::writeNodeInternalLoad( *iter , *act_time_stamp );
        }
        GiD_EndResult();
        delete act_time_stamp;
      }
    } else {
      GiD_BeginResultGroup((char*)"Nodal Analysis", time_iter->index() , GiD_OnNodes, 0 );
      GiDBase::beginNodeDisplacement();
      //      GiDBase::beginNodeInternalLoad(); 
      for( Iterator<Node> iter = model_->Aggregate<Node>::begin();
           iter != model_->Aggregate<Node>::end() ; 
           ++iter ) {
        GiDBase::writeNodeDisplacement( *iter , *time_iter );
        //        GiDBase::writeNodeInternalLoad( *iter , *time_iter );
      }
      GiD_EndResult();
    }
  }
}

//------------------------------------------------------------------
void GiDOutputHandler::writeElements( )
{
  GiD_BeginElements();
  for( Iterator<Element> iter = model_->Aggregate<Element>::begin();
       iter != model_->Aggregate<Element>::end() ; 
       ++iter )
    GiDBase::writeElement( *iter );
  GiD_EndElements();
}

