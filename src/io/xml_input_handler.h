#ifndef xml_input_handler_h__
#define xml_input_handler_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * Authors: Michael Hammer
 * ------------------------------------------------------------------*/

#include <iostream>
#include <string>
#include <cmath>
#include <vector>

#include "input_handler.h"

#include "base/xml_base.h"

#define XS XERCES_CPP_NAMESPACE

//------------------------------------------------------------------
class XMLInputHandler : 
  public InputHandler,
  public XMLBase
{
public:
  inline XMLInputHandler(XMLDOMParser* parser):
    InputHandler(IDObject::XML),
    XMLBase( parser ) {}

  inline virtual ~XMLInputHandler() {}

  virtual void get(Global& global_data);

  virtual void get(Contact& contact_data);

protected:
  template<class T> friend class Iterator;

  template<class T>
  XS::DOMElement* getFirstNodeResultForTimeStamp( const TimeStamp& time_stamp );

  template<class T>
  Iterator<T> begin( T*, const TimeStamp& time_stamp );

  template<class T>
  T* next( T* act_item, int& index );

  virtual Node* next(Node* act_item, int& index);
  virtual Iterator<Node> begin( Node* );
  inline virtual Node* get( Node* , int index )
  { return( createNode( find( InputAggregate<Node>::xml_container_, NODE, index )) ); }
  inline virtual Node* at( Node* n, int index )
  { return( this->get(n,index) ); }
  Node* createNode(XS::DOMElement *node);

  virtual Edge* next(Edge* act_item, int& index);
  virtual Iterator<Edge> begin( Edge* );
  inline virtual Edge* get( Edge* , int index )
  { return(createEdge(find( InputAggregate<Edge>::xml_container_, EDGE, index ))); }
  inline virtual Edge* at( Edge* e, int index )
  { return( this->get(e,index) ); }
  Edge* createEdge(XS::DOMElement *node);

  virtual Face* next(Face* act_item, int& index);
  virtual Iterator<Face> begin( Face* );
  inline virtual Face* get( Face* , int index )
  { return(createFace(find( InputAggregate<Face>::xml_container_, FACE, index ))); }
  inline virtual Face* at( Face* e, int index )
  { return( this->get(e,index) ); }
  Face* createFace(XS::DOMElement *node);

  virtual Element* next(Element* act_item, int& index);
  virtual Iterator<Element> begin( Element* );
  inline virtual Element* get( Element* , int index )
  { return(createElement(find( InputAggregate<Element>::xml_container_, ELEMENT, index ))); }
  inline virtual Element* at( Element* e, int index )
  { return( this->get(e,index) ); }
  Element* createElement(XS::DOMElement *node);

  virtual Type* next(Type* act_item, int& index);
  virtual Iterator<Type> begin( Type* );
  inline virtual Type* get( Type* , int index )
  { return(createType(find( InputAggregate<Type>::xml_container_, TYPE, index ))); }
  inline virtual Type* at( Type* et, int index )
  { return( this->get(et,index) ); }
  Type* createType(XS::DOMElement* node);

  virtual Material* next(Material* act_item, int& index);
  virtual Iterator<Material> begin( Material* );
  inline virtual Material* get( Material* , int index )
  { return(createMaterial(find( InputAggregate<Material>::xml_container_, MATERIAL, index ))); }
  inline virtual Material* at( Material* m, int index )
  { return( this->get(m,index) ); }
  Material* createMaterial(XS::DOMElement* node);

  virtual TimeEpisode* next(TimeEpisode* act_item, int& index);
  virtual Iterator<TimeEpisode> begin( TimeEpisode* );
  inline virtual TimeEpisode* get( TimeEpisode* , int index )
  { return(createTimeEpisode(find( InputAggregate<TimeEpisode>::xml_container_, DATA_SET, index ))); }
  inline virtual TimeEpisode* at( TimeEpisode* t, int index )
  { return( this->get(t,index) ); }
  TimeEpisode* createTimeEpisode(XS::DOMElement* node);

  virtual Prescribed<DOFValue>* next(Prescribed<DOFValue>* act_item, int& index);
  virtual Iterator<Prescribed<DOFValue> > begin( Prescribed<DOFValue>* );
  inline virtual Prescribed<DOFValue>* get( Prescribed<DOFValue>* , int index )
  { return(createPrescribedDOF(find( InputAggregate<Prescribed<DOFValue> >::xml_container_, PRESCRIBED_DOF, index ))); }
  inline virtual Prescribed<DOFValue>* at( Prescribed<DOFValue>* t, int index )
  { return( this->get(t,index) ); }
  Prescribed<DOFValue>* createPrescribedDOF(XS::DOMElement* node);

  virtual Prescribed<LoadValue>* next(Prescribed<LoadValue>* act_item, int& index);
  virtual Iterator<Prescribed<LoadValue> > begin( Prescribed<LoadValue>* );
  inline virtual Prescribed<LoadValue>* get( Prescribed<LoadValue>* , int index )
  { return(createPrescribedLoad(find( InputAggregate<Prescribed<LoadValue> >::xml_container_, PRESCRIBED_LOAD, index ))); }
  inline virtual Prescribed<LoadValue>* at( Prescribed<LoadValue>* t, int index )
  { return( this->get(t,index) ); }
  Prescribed<LoadValue>* createPrescribedLoad(XS::DOMElement* node);

  virtual Boundary* next(Boundary* act_item, int& index);
  virtual Iterator<Boundary> begin( Boundary* );
  inline virtual Boundary* get( Boundary* , int index )
  { return( 0 ); } // UUPS
  inline virtual Boundary* at( Boundary* t, int index )
  { return( this->get(t,index) ); }
  Boundary* createBoundary(XS::DOMElement* node, numbered loop_number);

  inline Iterator<TimeStamp> begin( TimeStamp* t, const TimeStamp& time_stamp ) { return( begin( t ) ); }
  virtual TimeStamp* next(TimeStamp* act_item, int& index);
  virtual Iterator<TimeStamp> begin( TimeStamp* );
  inline virtual TimeStamp* get( TimeStamp* , int index )
  { return( create( (TimeStamp*)0, find( InputAggregate<TimeStamp>::xml_container_, TIME_STAMP, index )) ); }
  TimeStamp* create( TimeStamp*, XS::DOMElement* node);

  inline Iterator<DisplacementDOF> begin( DisplacementDOF* t, const TimeStamp& time_stamp ) { return( begin<DisplacementDOF>( t , time_stamp ) ); }
  virtual DisplacementDOF* next(DisplacementDOF* act_item, int& index) { return( next<DisplacementDOF>( act_item , index )); }
  inline virtual DisplacementDOF* get( DisplacementDOF* , int index )
  { return( create( (DisplacementDOF*)0, find( InputAggregate<DisplacementDOF>::xml_container_, NODE, index )) ); }
  DisplacementDOF* create( DisplacementDOF*, XS::DOMElement* node);

  inline Iterator<ForceLoad> begin( ForceLoad* t, const TimeStamp& time_stamp ) { return( begin<ForceLoad>( t , time_stamp ) ); }
  virtual ForceLoad* next(ForceLoad* act_item, int& index) { return( next<ForceLoad>( act_item , index )); }
  inline virtual ForceLoad* get( ForceLoad* , int index )
  { return( create( (ForceLoad*)0, find( InputAggregate<ForceLoad>::xml_container_, NODE, index )) ); }
  ForceLoad* create( ForceLoad*, XS::DOMElement* node);

  inline Iterator<LagrangeDOF> begin( LagrangeDOF* t, const TimeStamp& time_stamp ) { return( begin<LagrangeDOF>( t , time_stamp ) ); }
  virtual LagrangeDOF* next(LagrangeDOF* act_item, int& index) { return( next<LagrangeDOF>( act_item , index )); }
  inline virtual LagrangeDOF* get( LagrangeDOF* , int index )
  { return( create( (LagrangeDOF*)0, find( InputAggregate<LagrangeDOF>::xml_container_, NODE, index )) ); }
  LagrangeDOF* create( LagrangeDOF*, XS::DOMElement* node);
};

#endif
