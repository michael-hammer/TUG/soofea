/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>

#include "../math/template/tensor.h"
#include "../exceptions/exceptions.h"
#include "../math/template/array.h"

using std::cout;
using std::endl;

static double EPS = 1e-7;

double calcPsi(double mu, double lambda, Math::Tensor<double>& strain);
void calcAnalyticalSolution(double mu, double lambda,
			    Math::Tensor<double>& strain, Math::Tensor<double>& stress);
void calcAnalyticalTangent(double mu, double lambda);
void calcNumericalSolution(double mu, double lambda, Math::Tensor<double>& strain);
void calcNumericalTangent(double mu, double lambda,
			  Math::Tensor<double>& strain, Math::Tensor<double>& stress);

//------------------------------------------------------------------
int main()
{
  cout << "Test Numerical Tangent of Scalar" << endl;

  cout << "EPS: " << EPS << endl;  

  unsigned long size2d[] = {3,3};
  double A[] = {
    8.66476711e-03,  5.76990595e-04,  2.01386876e-04,
    5.76990595e-04,  -5.64962573e-03, 1.92436746e-03,
    2.01386876e-04,  1.92436746e-03,  -3.01514138e-03};
  
  Math::Tensor<double> TEST(size2d,2);
  TEST.setArrayData(A);
  cout << "TEST Array" << endl;
  TEST("IJ").prettyPrint();

  double mu = 3.33111;
  double lambda = 4.9955; 

  double psi = calcPsi(mu, lambda, TEST);
  cout << "Psi: " << psi << endl;  

  Math::Tensor<double> stress(size2d,2);
  
  calcAnalyticalSolution(mu, lambda, TEST, stress);
  cout << "Analytical Solution: IJ" << endl;
  stress("IJ").prettyPrint();
  calcNumericalSolution(mu, lambda, TEST);

  calcAnalyticalTangent(mu, lambda);
  calcNumericalTangent(mu, lambda, TEST, stress);
  return(0);
}

//------------------------------------------------------------------
double calcPsi(double mu, double lambda, Math::Tensor<double>& strain)
{
  unsigned long size2d[] = {3,3};
  unsigned long size4d[] = {3,3,3,3};

  Math::Tensor<double> strain2(size2d,2);
  strain2 = strain;
  
  double trace_e = Math::trace(strain);

  double value = 0.5 * lambda * trace_e * trace_e;
  double second = Math::convolve(strain("ab"), strain2("ab") );
  value += mu * second;

  return value;
}

//------------------------------------------------------------------
void calcAnalyticalSolution(double mu, double lambda,
			    Math::Tensor<double>& strain, Math::Tensor<double>& stress)
{
  unsigned long size2d[] = {3,3};
  unsigned long size4d[] = {3,3,3,3};

  double trace_e = Math::trace(strain);

  Math::Tensor<double> I1(size2d,2);
  Math::Tensor<double> I2(size2d,2);
  Math::Tensor<double> sec_part(size2d,2);
  Math::Tensor<double> dummy_1(size4d,4);
  Math::Tensor<double> dummy_2(size4d,4);
  Math::Tensor<double> dummy_3(size4d,4);

  I1.identity();
  I2.identity();

  dummy_1("IJKL").mul(I1("IJ"),I2("KL"));
  dummy_2 = dummy_1;
  dummy_3("IJKL").add(dummy_1("KILJ"),dummy_2("LIKJ"));

  sec_part("IJ").mul(dummy_3("IJKL"),(strain)("KL"));
  sec_part.scale(mu);
  
  I1.scale(lambda);
  I1.scale(trace_e);
 
  stress("IJ").add(I1("IJ"),sec_part("IJ"));
}

//------------------------------------------------------------------
void calcAnalyticalTangent(double mu, double lambda)
{
  unsigned long size2d[] = {3,3};
  unsigned long size4d[] = {3,3,3,3};

  Math::Tensor<double> solution(size4d,4);

  Math::Tensor<double> I1(size2d,2);
  Math::Tensor<double> I2(size2d,2);
  Math::Tensor<double> dummy_1(size4d,4);
  Math::Tensor<double> dummy_2(size4d,4);
  Math::Tensor<double> dummy_3(size4d,4);
  I1.identity();
  I2.identity();

  dummy_1("IJKL").mul(I1("IJ"),I2("KL"));
  dummy_3 = dummy_1;
  dummy_2("IJKL").add(dummy_1("KILJ"),dummy_3("LIKJ"));

  dummy_1.scale(lambda);
  dummy_2.scale(mu);

  solution("IJKL").add(dummy_1("IJKL"),dummy_2("IJKL"));
  cout << "Analytical Solution TANGENT: IJKL" << endl;
  solution("IJKL").prettyPrint("IJKL");
}

//------------------------------------------------------------------
void calcNumericalSolution(double mu, double lambda, Math::Tensor<double>& strain)
{
  unsigned long pos[] = {0,0};
  unsigned long size2d[] = {3,3};

  Math::Tensor<double> solution(size2d,2);

  for(unsigned long row = 0; row < 3; row++)
    {
      for(unsigned long col = 0; col < 3; col++)
	{
	  pos[0] = row;
	  pos[1] = col;
	  Math::Tensor<double> strainleft(size2d,2);
	  strainleft = strain;
	  strainleft.write( pos,  strainleft.get(pos) + EPS);
	  Math::Tensor<double> strainright(size2d,2);
	  strainright = strain;
	  strainright.write( pos, strainright.get(pos) - EPS);
	  solution.write(pos, ( calcPsi(mu, lambda, strainleft) - calcPsi(mu, lambda, strainright) ) / (2*EPS) );
	}
    }
  cout << "Numerical Solution: IJ" << endl;
  solution("IJ").prettyPrint();
}

//------------------------------------------------------------------
void calcNumericalTangent(double mu, double lambda,
			  Math::Tensor<double>& strain, Math::Tensor<double>& stress)
{
  unsigned long pos_strain[] = {0,0};
  unsigned long pos_stress[] = {0,0};
  unsigned long pos4[] = {0,0,0,0};
  unsigned long size2d[] = {3,3};
  unsigned long size4d[] = {3,3,3,3};

  Math::Tensor<double> solution(size4d,4);

  for(unsigned long count1 = 0; count1 < 3; count1++)
    {
      for(unsigned long count2 = 0; count2 < 3; count2++)
	{
	  pos4[0] = count1;
	  pos4[1] = count2;
	  pos_strain[0] = count1;
	  pos_strain[1] = count2;
	  Math::Tensor<double> strainleft(size2d,2);
	  strainleft = strain;
	  strainleft.write( pos_strain,  strainleft.get(pos_strain) + EPS);
	  Math::Tensor<double> strainright(size2d,2);
	  strainright = strain;
	  strainright.write( pos_strain, strainright.get(pos_strain) - EPS);
	  Math::Tensor<double> stressleft(size2d,2);
	  Math::Tensor<double> stressright(size2d,2);
	  calcAnalyticalSolution(mu, lambda, strainleft, stressleft);
	  calcAnalyticalSolution(mu, lambda, strainright, stressright);
	  for(unsigned long count3 = 0; count3 < 3; count3++)
	    {
	      for(unsigned long count4 = 0; count4 < 3; count4++)
		{
		  pos4[2] = count3;
		  pos4[3] = count4;
		  pos_stress[0] = count3;
		  pos_stress[1] = count4;		  
		  double value = stressleft.get(pos_stress) - stressright.get(pos_stress);
		  solution.write(pos4, value / (2*EPS) );
		}
	    }
	}
    }
  cout << "Numerical Solution TANGENT: IJKL" << endl;
  solution("IJKL").prettyPrint("IJKL");
}
