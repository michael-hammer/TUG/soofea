/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>

using std::cout;
using std::endl;

void index_counter(unsigned long*& index, unsigned index_count, unsigned dimension, unsigned long* size);
void prettyPrint(unsigned long*& index, unsigned dimension);

//------------------------------------------------------------------
int main()
{
  unsigned long size[3] = {3,2,2};
  unsigned dimension = 3;
  unsigned long* index = new unsigned long[dimension];
  unsigned index_count = 0;

  index_counter(index, index_count, dimension, size);

  delete[] index;
}

//------------------------------------------------------------------
void index_counter(unsigned long*& index, unsigned index_count, unsigned dimension, unsigned long* size)
{
  bool jump_back = false;
  
  for( index[index_count] = 0; index[index_count] < size[index_count] ; ++(index[index_count]) )
    {
      if( index_count < (dimension-1) )
	{
	  index_counter(index,index_count+1,dimension,size);
	  jump_back = true;
	}

      if( !jump_back )
	prettyPrint(index,dimension);

      jump_back = false;
    }

  return;
}

//------------------------------------------------------------------
void prettyPrint(unsigned long*& index, unsigned dimension)
{
  unsigned disp_counter;
  static unsigned counter = 1;

  cout << "{";
  for( disp_counter = 0 ; disp_counter < (dimension-1) ; ++disp_counter )
    {
      cout << index[disp_counter] << ",";
    }
  cout << index[dimension-1] << "}" << " counter = " << (counter++) << endl;

  return;
}
