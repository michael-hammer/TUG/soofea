# ifdef _OPENMP
# include <omp.h>
# endif

# include <stdio.h>
# include <iostream>

int main ( void )
{
  /////////////////////////////////////////////////////////////////

  int i;
  int N = 5;
  double b[] = {1,2,3,4,5};
  double c[] = {11,22,33,44,55};

  double a[] = {0,0,0,0,0};
  double d[] = {0,0,0,0,0};

# pragma omp   parallel
  {
# pragma omp   for nowait
    for ( i =0; i < N ; i ++)
      {
	a [ i ] = b[ i ] + c [ i ];
	std::cout << "a[" << i << "] = " << a[i] << std::endl;
      }
# pragma omp   barrier
# pragma omp   for
    for ( i =0; i < N ; i ++)
      {
	d [ i ] = a[ i ] + b [ i ];
	std::cout << "d[" << i << "] = " << d[i] << std::endl;
      }
  }

  /////////////////////////////////////////////////////////////////

  double A[1000000];
# pragma omp parallel for
  for ( i =0; i <1000000; i ++) A[ i ]= i ;
  double summe = 0;
# pragma omp parallel for shared ( summe ) private ( i )
  for ( i =0; i < 1000000; i ++) {
# pragma omp critical ( summierung )
    summe = summe + A[i];
  }
  std::cout << "summe = " << summe << std::endl;

  /////////////////////////////////////////////////////////////////

  return 0;

}
