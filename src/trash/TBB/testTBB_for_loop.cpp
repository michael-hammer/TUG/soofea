// run as: g++ -O2 my_test.cpp -ltbb

/*
    Copyright 2005-2007 Intel Corporation.  All Rights Reserved.

    This file is part of Threading Building Blocks.

    Threading Building Blocks is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

    Threading Building Blocks is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Threading Building Blocks; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    As a special exception, you may use this file as part of a free software
    library without restriction.  Specifically, if other files instantiate
    templates or use macros or inline functions from this file, or you compile
    this file and link it with other files to produce an executable, this
    file does not by itself cause the resulting executable to be covered by
    the GNU General Public License.  This exception does not however
    invalidate any other reasons why the executable file might be covered by
    the GNU General Public License.
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <map>							
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"

using namespace std;

void SerialPrintOut(double a[], size_t n )
{
  for(size_t i = 0 ; i < n; ++i )
    std::cout << "a[i] = " << a[i] << ",   a[i])^2 = " << a[i]*a[i] << endl;
}

class ParallelPrintOut
{
  double *const my_a;
public:
  void operator()(const tbb::blocked_range<size_t>& r) const
  {
    double *a = my_a;
    for(size_t i = r.begin(); i != r.end(); ++i )
      std::cout << "a[i] = " << a[i] << ",   a[i])^2 = " << a[i]*a[i] << endl;
  }
  ParallelPrintOut(double a[]) : my_a(a) {}
};

void SerialVector(std::vector<double>* vec)
{
  std::vector<double>::iterator iter =  vec->begin();
  for( ; iter != vec->end() ; ++iter )
    {
      std::cout << "serial iter: " << *iter << std::endl;
    }
}

class ParallelVector{
public:
  std::vector<double>* const my_vec;
  void operator()(const tbb::blocked_range<std::vector<double>::iterator>& r) const
  {
    std::vector<double>* vec = my_vec;
    for(std::vector<double>::iterator i = r.begin(); i != r.end(); ++i )
      std::cout << "parallel iter = " << *i << std::endl;
  }
  ParallelVector(std::vector<double>* vec) : my_vec(vec) {}
};

void SerialMap(std::map<int,double>* m)
{
  std::map<int,double>::iterator iter =  m->begin();
  for( ; iter != m->end() ; ++iter )
    {
      std::cout << "m[" << iter->first << "] = " << iter->second << std::endl;
    }
}

class ParallelMap{
public:
  std::map<int,double>* const my_map;
  void operator()(const tbb::blocked_range<std::map<int,double>::iterator>& r) const
  {
    std::map<int,double>* m = my_map;
    for(std::map<int,double>::iterator i = r.begin(); i != r.end(); ++i )
      std::cout << "m[" << i->first << "] = " << i->second << std::endl;
  }
  ParallelMap(std::map<int,double>* m) : my_map(m) {}
};

int main(size_t argc, char *argv[])
{
  // ##############################################################################
  std::cout << "FIRST TEST (double array)" << std::endl;
  size_t n = 1000;
  double* a = new double[n];
  for(int count = 0 ; count < n; ++count )
    a[count] = count;

  std::cout << "Serial " << std::endl;
  SerialPrintOut(a,n);
 
  // first step in parallelizing a loop is to convert
  // the loop body into a form that operates on a chunk of iterations
  // It defines a method operator() that contains a modified version of the original loop.
  // The method operator() receives a blocked_range<size_t> object r that
  // describes the subset of the original iteration space on which the function should operate
  std::cout << "Parallel " << endl;
  tbb::task_scheduler_init init;
  
  // last parameter in tbb::blocked_range<size_t>(0,n,1) -> here 1, shows the size of the chunks
  // so, if loop has 100 iterations, 1 means 100 chunks, 5 means 20 chunks
  // 0,n defines the open interval, from 0 to <100
  tbb::parallel_for(tbb::blocked_range<size_t>(0,n,1), 
		    ParallelPrintOut(a) );

  // ##############################################################################
  std::cout << "SECOND TEST std::vector<double>" << std::endl;

  std::vector<double>* vec = 0;
  vec = new std::vector<double>(100);
  double count = 0.0;
  std::vector<double>::iterator iter =  vec->begin();
  for( ; iter != vec->end() ; ++iter )
    {
      *iter = count;
      count++;
    }

  std::cout << "Serial " << std::endl;
  SerialVector(vec);

  std::cout << "Parallel " << endl;
  tbb::parallel_for(tbb::blocked_range<std::vector<double>::iterator>(vec->begin(), vec->end()),
		    ParallelVector(vec), tbb::auto_partitioner() );

  // ##############################################################################
  std::cout << "THIRD TEST std::map<int,double>" << std::endl;

  std::map<int,double>* m = 0;
  m = new std::map<int,double>;
  for(int counter = 0; counter < 100; counter++ )
    {
      (*m)[counter] = counter*counter;
    }

  std::cout << "Serial " << std::endl;
  SerialMap(m);
  std::map<int,double>::iterator ite =  m->begin();
  for( ; ite != m->end() ; ++ite )
    {
      std::cout << "m[" << ite->first << "] = " << ite->second << std::endl;
    }
  std::cout << "Parallel " << endl;
  //not possible, whyever
  //tbb::parallel_for(tbb::blocked_range<std::map<int,double>::iterator>(m->begin(), m->end()),
  //		    ParallelMap(m), tbb::auto_partitioner() );

  delete vec;
  delete m;

  return 0;
}

