// run as: g++ -O2 -DTBB  my_test.cpp -ltbb

/*
    Copyright 2005-2007 Intel Corporation.  All Rights Reserved.

    This file is part of Threading Building Blocks.

    Threading Building Blocks is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

    Threading Building Blocks is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Threading Building Blocks; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    As a special exception, you may use this file as part of a free software
    library without restriction.  Specifically, if other files instantiate
    templates or use macros or inline functions from this file, or you compile
    this file and link it with other files to produce an executable, this
    file does not by itself cause the resulting executable to be covered by
    the GNU General Public License.  This exception does not however
    invalidate any other reasons why the executable file might be covered by
    the GNU General Public License.
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <map>	

#include <unistd.h>
					
#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
#include "tbb/spin_mutex.h"
#include "tbb/queuing_mutex.h"

class Singleton
{
public:
  static Singleton* createSingleton()
  {
    if (instance_ == 0)
      instance_ = new Singleton(); 
    return instance_;
  };
  static Singleton* const getInstance()
  {
    return(instance_);
  };
  static void terminate()
  {
    delete instance_;
  };
  void call()
  {
    member_ = 0;
    usleep(3);
    std::cout << "Member should be 0; member_ = " << member_ << std::endl;
    member_ = 1;
    usleep(7);
    std::cout << "Member should be 1; member_ = " << member_ << std::endl;
  };
  virtual ~Singleton() {};
protected:
  Singleton() {};	
  static Singleton* instance_;
  int member_;
};

Singleton* Singleton::instance_ = 0;

#ifdef TBB
class ParallelVectorWO{
public:
  std::vector<Singleton*>* const my_vec;
  void operator()(const tbb::blocked_range<std::vector<Singleton*>::iterator>& r) const
  {
    std::vector<Singleton*>* vec = my_vec;
    for(std::vector<Singleton*>::iterator i = r.begin(); i != r.end(); ++i )
      (*i)->call();
  }
  ParallelVectorWO(std::vector<Singleton*>* vec) : my_vec(vec) {}
};

Singleton* FreeSingleton;
// easy to change, see reference for difference
//typedef tbb::spin_mutex FreeSingletonMutexType;
typedef tbb::queuing_mutex FreeSingletonMutexType;
FreeSingletonMutexType FreeSingletonMutex;

class ParallelVectorW{
public:
  std::vector<Singleton*>* const my_vec;
  void operator()(const tbb::blocked_range<std::vector<Singleton*>::iterator>& r) const
  {
    std::vector<Singleton*>* vec = my_vec;
    for(std::vector<Singleton*>::iterator i = r.begin(); i != r.end(); ++i )
      {
	FreeSingletonMutexType::scoped_lock lock(FreeSingletonMutex);
	(*i)->call();
      }
  }
  ParallelVectorW(std::vector<Singleton*>* vec) : my_vec(vec) {}
};
#endif

int main(size_t argc, char *argv[])
{
  // ##############################################################################
  std::cout << "Serial" << std::endl;

  std::vector<Singleton*>* vec = 0;
  vec = new std::vector<Singleton*>;
  for(unsigned count = 0; count < 10; count++)
    {
      Singleton* object = Singleton::createSingleton();
      vec->push_back(object);
    }
  std::vector<Singleton*>::iterator iter =  vec->begin();
  for( ; iter != vec->end() ; ++iter )
    {
      (*iter)->call();
    }

#ifdef TBB
  // ##############################################################################
  std::cout << "Parallel w/o mutex" << std::endl;
  tbb::task_scheduler_init init;
  tbb::parallel_for(tbb::blocked_range<std::vector<Singleton*>::iterator>(vec->begin(), vec->end()),
  		    ParallelVectorWO(vec), tbb::auto_partitioner() );

  std::cout << "Parallel w mutex" << std::endl;
  tbb::parallel_for(tbb::blocked_range<std::vector<Singleton*>::iterator>(vec->begin(), vec->end()),
  		    ParallelVectorW(vec), tbb::auto_partitioner() );
#endif

  iter =  vec->begin();
  (*iter)->terminate();

  return 0;
}

