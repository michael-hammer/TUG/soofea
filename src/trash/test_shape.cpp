/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>

#include "../model/shape/triangle_shape.h"
#include "../model/shape/linear_shape.h"

using std::cout;
using std::endl;

void testTriangle();
void testEdge();

//------------------------------------------------------------------
int main()
{
  //  testTriangle();
  testEdge();

  return(0);
}

//------------------------------------------------------------------
void testTriangle()
{
//  unsigned order = 1;
//  double L_1 = .13;
//  double L_2 = 0.78;
//
//  double delta = 1e-8;
//
//  unsigned count_I;
//  unsigned count_J;
//  unsigned index;
//  double value;
//
//  TriangleShape* shape = new TriangleShape(order);
//
//  double L_3 = 1. - (L_1 + L_2);
//
//  for( count_I = 0; count_I <= order ; ++count_I)
//    {
//      for( count_J = 0; count_J <= (order - count_I) ; ++ count_J)
//	{
//	  value = shape -> shape(count_I , count_J ,L_1,L_2);
//	  cout << "Shape: (" << count_I << "," << count_J << "," << (order - (count_I+count_J) ) << ") bei {L_1 = " 
//	       << L_1 << " L_2 = " << L_2 << " L_3 = " << L_3 << "} == " << value << endl;
//	  
//	  for( index = 1; index <= 3; ++index )
//	    {
//	      value = shape -> derivativeShapeL(index, count_I , count_J ,L_1,L_2);
//	      cout << "Derivative Shape: " << index << "(" << count_I << "," << count_J << "," << (order - (count_I+count_J) ) 
//		   << ") bei {L_1 = " << L_1 << " L_2 = " << L_2 << " L_3 = " << L_3 << "} == " << value << endl;
//	    }
//
//	  cout << "----- For Jacobian -----" << endl;
//
//	  for( index = 0; index < 2; ++index )
//	    {
//	      value = shape -> derivativeShapeRS( index , count_I , count_J,L_1,L_2);
//	      cout << "Derivative Shape: " << index << "(" << count_I << "," << count_J << "," << (order - (count_I+count_J) ) 
//		   << ") bei {L_1 = " << L_1 << " L_2 = " << L_2 << " L_3 = " << L_3 << "} == " << value;
//
//	      switch(index)
//		{
//		case 0:
//		  value = (shape -> shape( count_I , count_J,L_1+delta,L_2) - shape -> shape( count_I , count_J,L_1,L_2))/delta;
//		  break;
//		case 1:
//		  value = (shape -> shape( count_I , count_J,L_1,L_2+delta) - shape -> shape( count_I , count_J,L_1,L_2))/delta;
//		  break;
//		}
//	      cout << " / numerically == " << value << endl;
//	    }
//	}
//    }
//  delete shape;
}

//------------------------------------------------------------------
void testEdge()
{
  unsigned order = 350;
  unsigned element_amount = 10;
  double element_length = 1.;

  LinearShape* shape = new LinearShape(order);

//  cout << "Node 0: " << shape -> shape(0,0.5) << endl;
//  cout << "Node 1: " << shape -> shape(1,0.5) << endl;
//  cout << "Node 2: " << shape -> shape(2,0.5) << endl;
//
//  cout << "derivative:" << endl;
//  cout << "-----------" << endl;
//
//  cout << "der Node 0: " << shape -> derivativeShape(0,0.5) << endl;
//  cout << "der Node 1: " << shape -> derivativeShape(1,0.5) << endl;
//  cout << "der Node 2: " << shape -> derivativeShape(2,0.5) << endl;

  double value = 0;
  NodeIndex* node_index;

  for( unsigned element_count = 0 ; element_count != element_amount ; ++element_count )
    {
      value = 0;
      for( unsigned node_count = 0 ; node_count != (order+1) ; ++node_count )
	{
	  node_index = new NodeIndex(node_count);
	  //	  value += element_length * (element_count + node_count/((double)(order)) ) * shape -> shape(*node_index,0.253562346456);
	  value += shape -> shape(*node_index,0.253562346456);
	  delete node_index;
	}
      std::cout << "value : " << element_count << " = " << value << std::endl;
    }

  delete shape;
}
