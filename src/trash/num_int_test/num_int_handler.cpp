/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include "num_int_handler.h"

using std::cout;
using std::endl;
using std::string;

namespace Math {
  
  //------------------------------------------------------------------
  NumIntHandler::NumIntHandler(Math::IntTypes int_type) :
    act_int_type_(int_type),
    ready_(true),
    number_of_int_points_(0),
    array_vec_(new vector<Math::Array<double>*>),
    array_func_vec_(new vector<Functor*>),
    scalar_vec_(new vector<double*>),
    scalar_func_vec_(new vector<Functor*>)
  {
  }
  
  //------------------------------------------------------------------
  NumIntHandler::~NumIntHandler()
  {
    if(array_vec_)
      delete array_vec_;
    
    if(array_func_vec_)
      delete array_func_vec_;

    if(scalar_vec_)
      delete scalar_vec_;

    if(scalar_func_vec_)
      delete scalar_func_vec_;
  }
  
  //------------------------------------------------------------------
  void NumIntHandler::addArray(Functor& functor, Math::Array<double>* array)
  {
    array_func_vec_->push_back(&functor);
    array_vec_->push_back(array);
  }
  
  //------------------------------------------------------------------
  void NumIntHandler::addScalar(Functor& functor, double* scalar)
  {
    scalar_func_vec_->push_back(&functor);
    scalar_vec_->push_back(scalar);
  }
  
  //------------------------------------------------------------------
  void NumIntHandler::go(unsigned dimension, unsigned* points)
  {
    number_of_int_points_ = calcNumberOfIntPoints(dimension, points);

    Math::NumInt::createNumInt("./num_int_test/num_int_test.xml");
    Math::NumInt::getInstance() -> createIntPoints(act_int_type_, points);

    for (unsigned count = 0; count < number_of_int_points_; count++)
      {
	std::vector<double*>::iterator iter_scalar_vec;
	std::vector<Functor*>::iterator iter_scalar_func_vec = scalar_func_vec_ -> begin();
	for( iter_scalar_vec = scalar_vec_ -> begin() ; iter_scalar_vec != scalar_vec_ -> end() ; ++iter_scalar_vec )
	{
	  Math::NumInt::getInstance() -> intScalarAtOneGaussPoint(*(*iter_scalar_vec), act_int_type_, *(*iter_scalar_func_vec), count);
	  iter_scalar_func_vec++;
	}
	std::vector<Math::Array<double>* >::iterator iter_array_vec;
	std::vector<Functor*>::iterator iter_array_func_vec = array_func_vec_ -> begin();
	for( iter_array_vec = array_vec_ -> begin() ; iter_array_vec != array_vec_ -> end() ; ++iter_array_vec )
	{
	  Math::NumInt::getInstance() -> intArrayAtOneGaussPoint(*iter_array_vec, act_int_type_, *(*iter_array_func_vec), count);
	  iter_array_func_vec++;
	}
      }
  }
  
  //------------------------------------------------------------------
  void NumIntHandler::prettyPrint() const
  {
    std::cout << "Numerical Integration Handler: " << std::endl;   
    std::cout << "number_of_int_points_: " << number_of_int_points_ << std::endl;
    std::cout << "ready?: " << ready_ << std::endl;
    std::cout << "Number of Arrays to be integrated: " << array_vec_->size() << std::endl;
    std::cout << "Number of Scalars to be integrated: " << scalar_vec_->size() << std::endl;
    std::cout << "Integration Type: " << act_int_type_ << std::endl;
  }
  
  //------------------------------------------------------------------
  unsigned long NumIntHandler::calcNumberOfIntPoints(const unsigned dimension, const unsigned* points)
  {
    unsigned long noip = 1; 
    for (unsigned count = 0; count < dimension; count++)
      {
	noip *= points[count];
      }

    return noip;
  }

} // namespace Math
