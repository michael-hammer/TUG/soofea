#ifndef num_int_handler_h__
#define num_int_handler_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>
#include <vector>

#include "../../exceptions/exceptions.h"
#include "../../math/template/array.h"
#include "../../math/template/functor.h"

#include "num_int_test.h"

namespace Math {
  
  class NumIntHandler
    {
    public:
      NumIntHandler(Math::IntTypes int_type);
      ~NumIntHandler();
      
      void addArray(Functor& functor, Math::Array<double>* array);
      void addScalar(Functor& functor, double* scalar);
      
      void go(unsigned dimension, unsigned* points);

      void prettyPrint() const;
      
    protected:
      unsigned long calcNumberOfIntPoints(const unsigned dimension, const unsigned* points);

      Math::IntTypes act_int_type_;
      bool ready_;
      unsigned long number_of_int_points_; 
      std::vector<Math::Array<double>* > *array_vec_;
      std::vector<Functor*> *array_func_vec_;
      std::vector<double*> *scalar_vec_;
      std::vector<Functor*> *scalar_func_vec_;
    };
  
} // namespace Math

#endif // num_int_handler_h__
