#ifndef num_int_h__
#define num_int_h__

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>
#include <string>
#include <vector>
#include <math.h>

#include "../../exceptions/exceptions.h"
#include "../../math/template/array.h"
#include "../../math/template/functor.h"
#include "../../math/int_types.h"

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

XERCES_CPP_NAMESPACE_USE

namespace Math {

  //------------------------------------------------------------------
  class NumInt
  {
  public:

    class IntCoord
    {
    public:
      IntCoord(double r, double s=0., double t=0.);
      IntCoord(const IntCoord& src);
      double r_;
      double s_;
      double t_;
    };

    class IntCoordVector : public std::vector<IntCoord*>
    {
    public:
      ~IntCoordVector();
    };

    class IntPoint
    {
    public:
      IntPoint();
      IntPoint(double alpha, double r, double s=0., double t=0.);
      ~IntPoint();

      IntCoord* int_coord_;
      void prettyPrint() const;
      double alpha_;
    };

    static std::string const GAUSS_LEGENDRE_STRING;
    static std::string const ONE_DIM_STRING;
    static std::string const TRIANGLE_STRING;

    typedef std::vector<IntPoint*> IntPointVector;

    XMLCh* XML_TAG_INT_TYPE;
    XMLCh* XML_TAG_ORDER;
    XMLCh* XML_TAG_INT_ENTRY;
    XMLCh* XML_ATTR_TYPE;
    XMLCh* XML_ATTR_ID;
    XMLCh* XML_ATTR_ORDER;
    XMLCh* XML_ATTR_POINTS;
    XMLCh* XML_ATTR_PRECISION;
    XMLCh* XML_ATTR_R;
    XMLCh* XML_ATTR_S;
    XMLCh* XML_ATTR_T;
    XMLCh* XML_ATTR_ALPHA;
    XMLCh* XML_ATTR_MULTIPILICITY;


    static NumInt* createNumInt(const std::string& num_int_file);
    static NumInt* const getInstance();
    static void terminate();
    virtual ~NumInt();

    //------------------------------------------------------------------
    double intOneDimGaussLegendre(Functor& functor, unsigned points);

    //------------------------------------------------------------------
    double intTwoDimGaussLegendre(Functor& functor, unsigned points);
    double intTwoDimGaussLegendre(Functor& functor, unsigned* points);

    //------------------------------------------------------------------
    double intThreeDimGaussLegendre(Functor& functor, unsigned points);
    double intThreeDimGaussLegendre(Functor& functor, unsigned* points);

    //------------------------------------------------------------------
    double intTriangleGaussLegendre(Functor& functor, unsigned points);

    //------------------------------------------------------------------
    Array<double>* intOneDimArrayGaussLegendre(Functor& functor, unsigned points, 
					       unsigned dimension, unsigned long* size); 
    //------------------------------------------------------------------
    Array<double>* intTwoDimArrayGaussLegendre(Functor& functor, unsigned points, 
					       unsigned dimension, unsigned long* size);
    Array<double>* intTwoDimArrayGaussLegendre(Functor& functor, unsigned* points, 
					       unsigned dimension, unsigned long* size);
    
    //------------------------------------------------------------------
    Array<double>* intThreeDimArrayGaussLegendre(Functor& functor, unsigned points, 
						 unsigned dimension, unsigned long* size);
    Array<double>* intThreeDimArrayGaussLegendre(Functor& functor, unsigned* points, 
						 unsigned dimension, unsigned long* size);

    //------------------------------------------------------------------
    Array<double>* intTriangleArrayGaussLegendre(Functor& functor, unsigned points, 
						 unsigned dimension, unsigned long* size);

    //------------------------------------------------------------------
    void prettyPrintData();
    void prettyPrintIntegrationPoints();
    //------------------------------------------------------------------
    IntCoordVector* getIntCoord(IntTypes int_type, unsigned points);
    
    //------------------------------------------------------------------
    unsigned* getPointsForPrecision(Math::IntTypes int_type, unsigned* precision);

    void createIntPoints(IntTypes int_type, unsigned* points);
    void intScalarAtOneGaussPoint(double& scalar, Math::IntTypes int_type, Functor& functor, unsigned gauss_point_no);
    void intArrayAtOneGaussPoint( Math::Array<double>* array, Math::IntTypes int_type, Functor& functor, unsigned gauss_point_no);

  protected:
    void getDataForPoints(IntTypes int_type, unsigned* points);
    void getDataForPrecision(IntTypes int_type, unsigned* precision);
    DOMNodeList* getDataListForIntType(IntTypes int_type, unsigned& coord_amount);
    void getPointsAndPrecision(const DOMNodeList* child_list, 
			       DOMNodeList** value_list, 
			       unsigned& points,
			       unsigned& dimension);
    void getPrecisionAndPoints(const DOMNodeList* child_list, 
			       DOMNodeList** value_list, 
			       unsigned& precision,
			       unsigned& dimension);
    void getDataForSelectedList(const DOMNodeList* selected_data_list, unsigned coord_amount);
    void checkCalculationOfPoints(Math::IntTypes int_type, unsigned* points);
    void freeDataMembers();

    void indexCounter(unsigned long*& index, unsigned index_count, 
		      unsigned dimension, unsigned long* size, IntTypes int_type,
		      IntPoint* int_point);

    void Integrator1D(double& integrate, unsigned long* index, IntPoint* int_point);
    void Integrator1D(double& integrate, IntPoint* int_point);
    void Integrator2D(double& integrate, unsigned long* index, IntPoint* int_point);
    void Integrator2D(double& integrate, IntPoint* int_point);
    void Integrator3D(double& integrate, unsigned long* index, IntPoint* int_point);
    void Integrator3D(double& integrate, IntPoint* int_point);

    void prettyPrintIndex(unsigned long*& index, unsigned dimension);

    IntPointVector* createIntPoints(IntTypes int_type);

    NumInt(const std::string& num_int_file);	
    static NumInt* instance_;

    std::vector<vector<double> > *alpha_;
    std::vector<vector<IntCoord*> > *r_;
    std::vector<vector<unsigned> > *multiplicity_;

    const char* num_int_file_;
    DOMBuilder* dom_parser_;
    DOMDocument* dom_document_;

    IntTypes act_int_type_;
    IntPointVector* act_int_point_vector_;
    unsigned* calculation_of_point_;
    unsigned* act_amount_of_points_;
    unsigned* act_precision_;
    Functor* act_functor_;
    Array<double>* act_array_;
  };

} // namespace Math

#endif // num_int_h__
