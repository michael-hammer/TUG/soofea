/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>

#include "num_int_test.h"
#include "num_int_handler.h"

#include "../../math/int_types.h"

using std::cout;
using std::endl;

//------------------------------------------------------------------
class A
{
public:
  A(unsigned long* size, unsigned dimension);
  ~A();
  
  double function1D(double r);
  double function2D(double r, double s);
  double function3D(double r, double s, double t);
  
  double array1D(double r, unsigned long* index);
  double array2D(double r, double s, unsigned long* index);
  double array3D(double r, double s, double t, unsigned long* index);
protected:
  Math::Array<double>* array_;
};

//------------------------------------------------------------------
A::A(unsigned long* size, unsigned dimension) :
  array_(new Math::Array<double>(size,dimension))
{
}

//------------------------------------------------------------------
A::~A()
{
  delete array_;
}

//------------------------------------------------------------------
double A::function1D(double r)
{
  return( 5.*r*r*r - r*r + r);
}

//------------------------------------------------------------------
double A::function2D(double r, double s)
{
  return( 5. + r*r*s + 2.*r*r*s*s );
}

//------------------------------------------------------------------
double A::function3D(double r, double s, double t)
{
  return( 3. + r*s*t + 5.*r*r );
}

//------------------------------------------------------------------
double A::array1D(double r, unsigned long* index)
{
  array_ -> reset( 5.*r*r*r - r*r + r );

  return((*array_)(index));
}

//------------------------------------------------------------------
double A::array2D(double r, double s, unsigned long* index)
{
  array_ -> reset( 5. + r*r*s + 2.*r*r*s*s );
  
  return((*array_)(index));
}

//------------------------------------------------------------------
double A::array3D(double r, double s, double t, unsigned long* index)
{
  array_ -> reset( 3. + r*s*t + 5.*r*r );
  
  return((*array_)(index));
}

//------------------------------------------------------------------
int main()
{
  cout << "Test NumInt" << endl;
  cout << "------------" << endl;  

  Math::NumInt::createNumInt("./num_int_test/num_int_test.xml");

  unsigned dimension = 2;
  unsigned long size[2] = {3,3};

  A* a_class = new A(size,dimension);

  Math::FunctorScalar<A> functor1D = Math::FunctorScalar<A>( a_class, &A::function1D );
  Math::FunctorScalar<A> functor2D = Math::FunctorScalar<A>( a_class, &A::function2D );
  Math::FunctorScalar<A> functor3D = Math::FunctorScalar<A>( a_class, &A::function3D );

  Math::FunctorArray<A> functor1D_array = Math::FunctorArray<A>( a_class, &A::array1D );
  Math::FunctorArray<A> functor2D_array = Math::FunctorArray<A>( a_class, &A::array2D );
  Math::FunctorArray<A> functor3D_array = Math::FunctorArray<A>( a_class, &A::array3D );

  //Math::NumInt::getInstance() -> prettyPrintData();

  cout << "Integrate1D:{1} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,1) << endl;;
  cout << "Integrate1D:{2} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,2) << endl;;
  cout << "Integrate1D:{3} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,3) << endl;;
  cout << "Integrate1D:{4} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,4) << endl;;
  cout << "Integrate1D:{5} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,5) << endl;;
  cout << "Integrate1D:{6} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,6) << endl;;
  cout << "Integrate1D:{7} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,7) << endl;;
  cout << "Integrate1D:{8} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,8) << endl;;
  cout << "Integrate1D:{9} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,9) << endl;;
  cout << "Integrate1D:{10} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,10) << endl;;
  cout << "Integrate2D:{1} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,1) << endl;;
  cout << "Integrate2D:{2} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,2) << endl;;
  cout << "Integrate2D:{3} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,3) << endl;;
  cout << "Integrate2D:{4} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,4) << endl;;
  cout << "Integrate2D:{5} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,5) << endl;;
  cout << "Integrate2D:{6} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,6) << endl;;
  cout << "Integrate2D:{7} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,7) << endl;;
  cout << "Integrate2D:{8} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,8) << endl;;
  cout << "Integrate2D:{9} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,9) << endl;;
  cout << "Integrate2D:{10} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,10) << endl;;
  unsigned int_size[] = {5,6};
  cout << "Integrate2D{5,6}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 2;
  int_size[1] = 8;
  cout << "Integrate2D{2,8}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 5;
  int_size[1] = 5;
  cout << "Integrate2D{5,5}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 7;
  int_size[1] = 1;
  cout << "Integrate2D{7,1}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 10;
  int_size[1] = 4;
  cout << "Integrate2D{10,4}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 6;
  int_size[1] = 3;
  cout << "Integrate2D{6,3}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 1;
  int_size[1] = 1;
  cout << "Integrate2D{1,1}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 9;
  int_size[1] = 9;
  cout << "Integrate2D{9,9}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  cout << "Integrate3D:{1} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,1) << endl;;
  cout << "Integrate3D:{2} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,2) << endl;;
  cout << "Integrate3D:{3} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,3) << endl;;
  cout << "Integrate3D:{4} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,4) << endl;;
  cout << "Integrate3D:{5} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,5) << endl;;
  cout << "Integrate3D:{6} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,6) << endl;;
  cout << "Integrate3D:{7} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,7) << endl;;
  cout << "Integrate3D:{8} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,8) << endl;;
  cout << "Integrate3D:{9} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,9) << endl;;
  cout << "Integrate3D:{10} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,10) << endl;;
  unsigned int_siz[] = {5,6,7};
  cout << "Integrate3D{5,6,7}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 2;
  int_siz[1] = 8;
  int_siz[2] = 2;
  cout << "Integrate3D{2,8,2}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 5;
  int_siz[1] = 5;
  int_siz[2] = 5;
  cout << "Integrate3D{5,5,5}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 7;
  int_siz[1] = 1;
  int_siz[2] = 1;
  cout << "Integrate3D{7,1,1}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 10;
  int_siz[1] = 4;
  int_siz[2] = 5;
  cout << "Integrate3D{10,4,5}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 2;
  int_siz[1] = 3;
  int_siz[2] = 1;
  cout << "Integrate3D{2,3,1}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 1;
  int_siz[1] = 1;
  int_siz[2] = 6;
  cout << "Integrate3D{1,1,6}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 10;
  int_siz[1] = 10;
  int_siz[2] = 2;
  cout << "Integrate3D{10,10,2}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  //Math::NumInt::getInstance() -> prettyPrintData();
  cout << "IntegrateTriangle:{3} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,3) << endl;;
  cout << "IntegrateTriangle:{4} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,4) << endl;;
  cout << "IntegrateTriangle:{6} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,6) << endl;;
  cout << "IntegrateTriangle:{7} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,7) << endl;;
  cout << "IntegrateTriangle:{9} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,9) << endl;;
  cout << "IntegrateTriangle:{12} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,12) << endl;;
  cout << "IntegrateTriangle:{13} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,13) << endl;;

  Math::Array<double>* array = 0;

  cout << "Integrate1DArray:{7} " << endl; 
  array = Math::NumInt::getInstance() -> intOneDimArrayGaussLegendre(functor1D_array,7,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{7} " << endl;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,7,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{5,6} " << endl;
  int_size[0] = 5;
  int_size[1] = 6;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,int_size,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{3,3} " << endl;
  int_size[0] = 3;
  int_size[1] = 3;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,int_size,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{7,2} " << endl;
  int_size[0] = 7;
  int_size[1] = 2;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,int_size,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{1,10} " << endl;
  int_size[0] = 1;
  int_size[1] = 10;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,int_size,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{6} " << endl;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,6,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{7,2,8} " << endl;
  int_siz[0] = 7;
  int_siz[1] = 2;
  int_siz[2] = 8;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{10,4,2} " << endl;
  int_siz[0] = 10;
  int_siz[1] = 4;
  int_siz[2] = 2;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{1,1,2} " << endl;
  int_siz[0] = 1;
  int_siz[1] = 1;
  int_siz[2] = 2;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{7,7,7} " << endl;
  int_siz[0] = 7;
  int_siz[1] = 7;
  int_siz[2] = 7;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{4,4,8} " << endl;
  int_siz[0] = 4;
  int_siz[1] = 4;
  int_siz[2] = 8;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{3,2,2} " << endl;
  int_siz[0] = 3;
  int_siz[1] = 2;
  int_siz[2] = 2;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{6,10,6} " << endl;
  int_siz[0] = 6;
  int_siz[1] = 10;
  int_siz[2] = 6;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "IntegrateTriangleArray: " << endl;
  array = Math::NumInt::getInstance() -> intTriangleArrayGaussLegendre(functor2D_array,13,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "--- " << endl; 
  int_siz[0] = 4;
  int_siz[1] = 10;
  int_siz[2] = 8;
  cout << "Numerical Integration Handler: " << endl;
  cout << "Gauss Point: {"<< int_siz[0]<< ","<< int_siz[1]<<","<<int_siz[2] <<"}"<< endl; 

  Math::NumIntHandler *my_num_int_handler = 0;
  my_num_int_handler = new Math::NumIntHandler(Math::THREE_DIM);

  // has te be 3by3 matrices due to start implementation
  Math::Array<double>* array1 = 0;
  unsigned long array_dim1[] = {3,3};
  array1 = new Math::Array<double>(array_dim1,2);
  Math::Array<double>* array2 = 0;
  unsigned long array_dim2[] = {3,3};
  array2 = new Math::Array<double>(array_dim2,2);
  Math::Array<double>* array3 = 0;
  unsigned long array_dim3[] = {3,3};
  array3 = new Math::Array<double>(array_dim3,2);

  double int1 = 0;
  double int2 = 0;

  my_num_int_handler->addArray(functor3D_array, array1);
  my_num_int_handler->addArray(functor3D_array, array2);
  my_num_int_handler->addArray(functor3D_array, array3);

  my_num_int_handler->addScalar(functor3D, &int1);
  my_num_int_handler->addScalar(functor3D, &int2);

  my_num_int_handler->go(3,int_siz);

  //my_num_int_handler -> prettyPrint();
  cout << "Integrate Array1: " << endl;
  array1 -> prettyPrint();
  cout << "Integrate Array2: " << endl;
  array2 -> prettyPrint();
  cout << "Integrate Array3: " << endl;
  array3 -> prettyPrint();

  cout << "Scalar int1: " << endl;
  std::cout << int1 << std::endl;
  cout << "Scalar int2: " << endl;
  std::cout << int2 << std::endl;

  cout << "getPointsForPrecision: " << endl;
  unsigned precision[] = {3,6,10};
  cout << "Precision: " << endl;
  cout << precision[0] << "  " << precision[1] << "  " << precision[2] << endl;
  unsigned* points = Math::NumInt::getInstance() -> getPointsForPrecision(Math::THREE_DIM, precision);
  cout << "Points: " << endl;
  cout << points[0] << "  " << points[1] << "  " << points[2] << endl;

  delete my_num_int_handler;
  cout << "END Numerical Integration Handler: " << endl;
  cout << "--- " << endl;

  delete array1;
  delete array2;
  delete array3;

  delete a_class;
  Math::NumInt::terminate();
  return(0);
}
