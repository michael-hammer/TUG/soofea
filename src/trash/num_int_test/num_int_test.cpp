/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 *
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 *
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * $Id$
 * ------------------------------------------------------------------*/

#include "num_int_test.h"

using std::cout;
using std::endl;
using std::string;
using std::vector;

std::string const Math::NumInt::GAUSS_LEGENDRE_STRING = "gauss_legendre";

std::string const Math::NumInt::ONE_DIM_STRING = "one_dim";
std::string const Math::NumInt::TRIANGLE_STRING = "triangle";

namespace Math {

  NumInt* NumInt::instance_ = 0;

  //------------------------------------------------------------------
  NumInt::NumInt(const string& num_int_file):
    alpha_(new vector<vector<double> >),
    r_(new vector<vector<IntCoord*> >),
    multiplicity_(new vector<vector<unsigned> >),
    num_int_file_(num_int_file.c_str()),
    dom_parser_(0),
    dom_document_(0),
    act_int_type_(NAIT),
    act_int_point_vector_(0),
    calculation_of_point_(new unsigned[3]),
    act_amount_of_points_(new unsigned[3]),
    act_precision_(new unsigned[3])
  {
    act_amount_of_points_[0] = 0;
    act_amount_of_points_[1] = 0;
    act_amount_of_points_[2] = 0;
    act_precision_[0] = 0;
    act_precision_[1] = 0;
    act_precision_[2] = 0;
    calculation_of_point_[0] = 0;
    calculation_of_point_[1] = 1;
    calculation_of_point_[2] = 2;

    try {
      XMLPlatformUtils::Initialize();
    }
    catch (const XMLException& toCatch) {
      char* message = XMLString::transcode(toCatch.getMessage());
      cout << "Error during initialization! :" << endl;
      cout << "Exception message is: " << endl
	   << message << endl;
      XMLString::release(&message);
    }

    XMLCh tempStr[100];
    XMLString::transcode("LS", tempStr, 99);
    DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(tempStr);
    dom_parser_ = ((DOMImplementationLS*)impl)->createDOMBuilder(DOMImplementationLS::MODE_SYNCHRONOUS, 0);

    try {
      dom_document_ = dom_parser_ -> parseURI( num_int_file_ );
    }
    catch (const XMLException& toCatch) {
        char* message = XMLString::transcode(toCatch.getMessage());
        cout << "Exception message is: " << endl
             << message << endl;
        XMLString::release(&message);
    }
    catch (const DOMException& toCatch) {
        char* message = XMLString::transcode(toCatch.msg);
        cout << "Exception message is: " << endl
             << message << endl;
        XMLString::release(&message);
    }
    catch (...) {
        cout << "Unexpected Exception" << endl;
    }

    XML_TAG_INT_TYPE = XMLString::transcode("int_type");
    XML_TAG_ORDER = XMLString::transcode("order");
    XML_TAG_INT_ENTRY = XMLString::transcode("i");
    XML_ATTR_TYPE = XMLString::transcode("type");
    XML_ATTR_ID = XMLString::transcode("id");
    XML_ATTR_POINTS = XMLString::transcode("points");
    XML_ATTR_ORDER = XMLString::transcode("order");
    XML_ATTR_PRECISION = XMLString::transcode("prec");
    XML_ATTR_R = XMLString::transcode("r");
    XML_ATTR_S = XMLString::transcode("s");
    XML_ATTR_T = XMLString::transcode("t");
    XML_ATTR_ALPHA = XMLString::transcode("alpha");
    XML_ATTR_MULTIPILICITY = XMLString::transcode("mul");
  }

  //------------------------------------------------------------------
  NumInt::IntCoord::IntCoord(double r, double s, double t):
    r_(r),
    s_(s),
    t_(t)
  {
  }

  //------------------------------------------------------------------
  NumInt::IntCoord::IntCoord(const IntCoord& src) :
    r_(src.r_),
    s_(src.s_),
    t_(src.t_)
  {
  }

  //------------------------------------------------------------------
  NumInt::IntPoint::IntPoint() :
    int_coord_(new NumInt::IntCoord(0.)),
    alpha_(0)
  {
  }

  //------------------------------------------------------------------
  NumInt::IntPoint::IntPoint(double alpha, double r, double s, double t) :
    int_coord_(new NumInt::IntCoord(r,s,t)),
    alpha_(alpha)
  {
  }

  //------------------------------------------------------------------
  NumInt::IntPoint::~IntPoint()
  {
    delete int_coord_;
  }

  //------------------------------------------------------------------
  void NumInt::IntPoint::prettyPrint() const
  {
    cout << "r = " << int_coord_ -> r_ << ", s = " <<
      int_coord_ -> s_ << ", t = " << int_coord_ -> t_ << ", alpha = "<< alpha_ << endl;
  }

  //------------------------------------------------------------------
  NumInt::IntCoordVector::~IntCoordVector()
  {
    std::vector<IntCoord*>::iterator iter;

    for( iter = this -> begin() ; iter != this -> end() ; ++iter )
      {
	delete *iter;
      }
  }


  //------------------------------------------------------------------
  NumInt::~NumInt()
  {
    if( alpha_ )
      delete alpha_;

    if( r_ )
      {
	vector<vector<IntCoord*> >::iterator iter;
	for( iter = r_ -> begin() ; iter != r_ -> end() ; ++iter )
	  {
	    vector<IntCoord*>::iterator theIter;
	    for( theIter = iter->begin() ; theIter != iter->end() ; ++theIter )
	      {
		delete *theIter;
	      }
	  }
	delete r_;
      }

    if( multiplicity_ )
      delete multiplicity_;

    if( act_int_point_vector_ )
      {
	vector<IntPoint*>::iterator iter;
	for( iter = act_int_point_vector_ -> begin() ; iter != act_int_point_vector_ -> end() ; ++iter )
	  {
	    delete *iter;
	  }
	delete act_int_point_vector_;
      }

    if( calculation_of_point_ )
      delete[] calculation_of_point_;

    if( act_amount_of_points_ )
      delete[] act_amount_of_points_;

    if( act_precision_ )
      delete[] act_precision_;

    //if( act_array_ )
    // delete act_array_;

    if(dom_parser_ != 0)
      dom_parser_ -> release();

    XMLString::release(&XML_TAG_INT_TYPE);
    XMLString::release(&XML_TAG_ORDER);
    XMLString::release(&XML_TAG_INT_ENTRY);
    XMLString::release(&XML_ATTR_TYPE);
    XMLString::release(&XML_ATTR_ID);
    XMLString::release(&XML_ATTR_ORDER);
    XMLString::release(&XML_ATTR_POINTS);
    XMLString::release(&XML_ATTR_PRECISION);
    XMLString::release(&XML_ATTR_R);
    XMLString::release(&XML_ATTR_S);
    XMLString::release(&XML_ATTR_T);
    XMLString::release(&XML_ATTR_ALPHA);
    XMLString::release(&XML_ATTR_MULTIPILICITY);

    XMLPlatformUtils::Terminate();
  }

  //------------------------------------------------------------------
  NumInt* NumInt::createNumInt(const string& num_int_file)
  {
    if (instance_ == 0)
      instance_ = new NumInt(num_int_file);

    return instance_;
  }

  //------------------------------------------------------------------
  NumInt* const NumInt::getInstance()
  {
    return(instance_);
  }

  //------------------------------------------------------------------
  void NumInt::terminate()
  {
    delete instance_;
  }

  //------------------------------------------------------------------
  DOMNodeList* NumInt::getDataListForIntType(Math::IntTypes int_type, unsigned& coord_amount)
  {
    unsigned count;
    char* search_str = 0;
    char* id_str = 0;
    DOMNodeList* child_list = 0;

    DOMNodeList* node_list = dom_document_ -> getElementsByTagName( XML_TAG_INT_TYPE );

    for ( count = 0 ; count < (node_list->getLength()) ; count++ )
      {
	search_str = XMLString::transcode( node_list
					   -> item(count)
					   -> getAttributes()
					   -> getNamedItem( XML_ATTR_TYPE )
					   -> getNodeValue() );

	id_str = XMLString::transcode( node_list -> item(count)
				       -> getAttributes()
				       -> getNamedItem( XML_ATTR_ID )
				       -> getNodeValue() );

	if( GAUSS_LEGENDRE_STRING == search_str )
	  {
	    switch( int_type )
	      {
	      case ONE_DIM:
	      case TWO_DIM:
	      case THREE_DIM:
		if( ONE_DIM_STRING == id_str )
		  {
		    child_list = node_list -> item(count) -> getChildNodes();
		    coord_amount = 1;
		  }
		break;
	      case TRIANGLE:
		if( TRIANGLE_STRING == id_str )
		  {
		    child_list = node_list -> item(count) -> getChildNodes();
		    coord_amount = 2;
		  }
		break;
	      case NAIT:
		return(0);
	      }
	  }

	XMLString::release(&search_str);
	XMLString::release(&id_str);

	if( child_list )
	  {
	    act_int_type_ = int_type;
	    return( child_list );
	  }
      }

    return(0);
  }

  //------------------------------------------------------------------
  void NumInt::getPointsAndPrecision(const DOMNodeList* child_list, DOMNodeList** value_list, unsigned& points,
				     unsigned& dimension)
  {
    unsigned count;
    char* search_str = 0;

    for ( count = 0 ; count < (child_list->getLength()) ; count++ )
      {
	if( child_list -> item(count) -> hasAttributes() )
	  {
	    search_str = XMLString::transcode( child_list -> item(count)
					       -> getAttributes()
					       -> getNamedItem( XML_ATTR_POINTS )
					       -> getNodeValue() ) ;

	    act_amount_of_points_[dimension] = (unsigned)atoi( search_str );
	    XMLString::release(&search_str);

	    if( act_amount_of_points_[dimension] == points )
	      {
		*value_list = child_list -> item(count) -> getChildNodes();
		// FIXME: Here the first precision found is taken
		search_str = XMLString::transcode( child_list -> item(count)
						   -> getAttributes()
						   -> getNamedItem( XML_ATTR_PRECISION )
						   -> getNodeValue() ) ;
		act_precision_[dimension] = (unsigned)atoi( search_str );
		XMLString::release(&search_str);
		return;
	      }
	  }
      }
    *value_list = 0;
  }

  //------------------------------------------------------------------
  void NumInt::getPrecisionAndPoints(const DOMNodeList* child_list, DOMNodeList** value_list, unsigned& precision,
				     unsigned& dimension)
  {
    unsigned count;
    char* search_str = 0;

    for ( count = 0 ; count < (child_list->getLength()) ; count++ )
      {
	if( child_list -> item(count) -> hasAttributes() )
	  {
	    search_str = XMLString::transcode( child_list -> item(count)
					       -> getAttributes()
					       -> getNamedItem( XML_ATTR_PRECISION )
					       -> getNodeValue() ) ;

	    act_precision_[dimension] = (unsigned)atoi( search_str );
	    XMLString::release(&search_str);

	    if( act_precision_[dimension] == precision )
	      {
		*value_list = child_list -> item(count) -> getChildNodes();

		// FIXME: Here the first point found is taken
		search_str = XMLString::transcode( child_list -> item(count)
						   -> getAttributes()
						   -> getNamedItem( XML_ATTR_POINTS )
						   -> getNodeValue() ) ;
		act_amount_of_points_[dimension] = (unsigned)atoi( search_str );
		XMLString::release(&search_str);
		return;
	      }
	  }
      }
    *value_list = 0;
  }

  //------------------------------------------------------------------
  void NumInt::getDataForSelectedList(const DOMNodeList* value_list, unsigned coord_amount)
  {
    char* search_str = 0;
    unsigned count = 0;
    double alpha = 0.;
    double r = 0.;
    IntCoord* int_coord = 0;
    unsigned mul = 0;

    vector<double> alpha_vec;
    vector<IntCoord*> r_vec;
    vector<unsigned> multiplicity_vec;


    for ( count = 0 ; count < (value_list->getLength()) ; count++ )
      {
	if( value_list -> item(count) -> hasAttributes() )
	  {
	    search_str = XMLString::transcode( value_list -> item(count)
					       -> getAttributes()
					       -> getNamedItem( XML_ATTR_ALPHA )
					       -> getNodeValue() ) ;

	    alpha = atof( search_str );
	    XMLString::release(&search_str);
	    alpha_vec.push_back(alpha);

	    search_str = XMLString::transcode( value_list -> item(count)
					       -> getAttributes()
					       -> getNamedItem( XML_ATTR_MULTIPILICITY )
					       -> getNodeValue() ) ;

	    mul = (unsigned)atoi( search_str );
	    XMLString::release(&search_str);
	    multiplicity_vec.push_back(mul);

	    if( coord_amount > 0)
	      {
		search_str = XMLString::transcode( value_list -> item(count)
						   -> getAttributes()
						   -> getNamedItem( XML_ATTR_R )
						   -> getNodeValue() );

		r = atof( search_str );
		int_coord = new IntCoord(r);
		XMLString::release(&search_str);
	      }
	    if( coord_amount > 1)
	      {
		search_str = XMLString::transcode( value_list -> item(count)
						   -> getAttributes()
						   -> getNamedItem( XML_ATTR_S )
						   -> getNodeValue() );

		r = atof( search_str );
		int_coord -> s_ = r;
		XMLString::release(&search_str);
	      }
	    if( coord_amount > 2)
	      {
		search_str = XMLString::transcode( value_list -> item(count)
						   -> getAttributes()
						   -> getNamedItem( XML_ATTR_T )
						   -> getNodeValue() );

		r = atof( search_str );
		int_coord -> t_ = r;
		XMLString::release(&search_str);
	      }

	    r_vec.push_back(int_coord);
	  }
      }
    alpha_->push_back(alpha_vec);
    multiplicity_->push_back(multiplicity_vec);
    r_->push_back(r_vec);

  }

  //------------------------------------------------------------------
  void NumInt::getDataForPrecision(Math::IntTypes int_type, unsigned* precision)
  {
    unsigned final_value = 0;
    switch( int_type )
      {
      case ONE_DIM:
	if( (int_type == act_int_type_) && (precision[0] == act_precision_[0])  )
	  return;
	final_value = 1;
	break;
      case TWO_DIM:
	if( (int_type == act_int_type_) && (precision[0] == act_precision_[0]) &&
	    (precision[1] == act_precision_[1]) )
	  return;
	final_value = 2;
	break;
      case THREE_DIM:
	if( (int_type == act_int_type_) && (precision[0] == act_precision_[0]) &&
	    (precision[1] == act_precision_[1]) && (precision[2] == act_precision_[2]) )
	  return;
	final_value = 3;
	break;
      case TRIANGLE:
	if( (int_type == act_int_type_) && (precision[0] == act_precision_[0])  )
	  return;
	final_value = 1;
	break;
      case NAIT:
	final_value = 0;
	break;
      }

    unsigned coord_amount = 0;
    DOMNodeList* int_type_data_list = 0;
    DOMNodeList* selected_data_list = 0;

    freeDataMembers();

    int_type_data_list = getDataListForIntType(int_type,coord_amount);

    checkCalculationOfPoints(int_type, precision);
    for(unsigned dimension = 0; dimension < final_value; dimension++)
      {
	if(calculation_of_point_[dimension] == dimension || (calculation_of_point_[dimension] == 1 && dimension == 2) )
	  {
	    getPrecisionAndPoints(int_type_data_list, &selected_data_list, precision[dimension], calculation_of_point_[dimension]);
	    while( ! selected_data_list )
	      {
		++(precision[dimension]);
		getPrecisionAndPoints(int_type_data_list, &selected_data_list, precision[dimension], calculation_of_point_[dimension]);
	      }
	    getDataForSelectedList(selected_data_list, coord_amount);
	  }
      }



    //getPrecisionAndPoints(int_type_data_list, &selected_data_list, precision);
    //while( ! selected_data_list )
    //  getPrecisionAndPoints(int_type_data_list, &selected_data_list, ++precision);
    //
    //getDataForSelectedList(selected_data_list, coord_amount);
  }

  //------------------------------------------------------------------
  void NumInt::getDataForPoints(Math::IntTypes int_type, unsigned* points)
  {
    unsigned final_value = 0;
    switch( int_type )
      {
      case ONE_DIM:
	if( (int_type == act_int_type_) && (points[0] == act_amount_of_points_[0])  )
	  return;
	final_value = 1;
	break;
      case TWO_DIM:
	if( (int_type == act_int_type_) && (points[0] == act_amount_of_points_[0]) &&
	    (points[1] == act_amount_of_points_[1]) )
	  return;
	  final_value = 2;
	break;
      case THREE_DIM:
	if( (int_type == act_int_type_) && (points[0] == act_amount_of_points_[0]) &&
	    (points[1] == act_amount_of_points_[1]) && (points[2] == act_amount_of_points_[2]) )
	  return;
	final_value = 3;
	break;
      case TRIANGLE:
	if( (int_type == act_int_type_) && (points[0] == act_amount_of_points_[0])  )
	  return;
	final_value = 1;
	break;
      case NAIT:
	final_value = 0;
	break;
      }

    unsigned coord_amount = 0;
    DOMNodeList* int_type_data_list = 0;
    DOMNodeList* selected_data_list = 0;

    freeDataMembers();

    int_type_data_list = getDataListForIntType(int_type,coord_amount);

    checkCalculationOfPoints(int_type, points);
    for(unsigned dimension = 0; dimension < final_value; dimension++)
      {
	if(calculation_of_point_[dimension] == dimension || (calculation_of_point_[dimension] == 1 && dimension == 2) )
	  {
	    getPointsAndPrecision(int_type_data_list, &selected_data_list, points[dimension], calculation_of_point_[dimension]);
	    while( ! selected_data_list )
	      {
		++(points[dimension]);
		getPointsAndPrecision(int_type_data_list, &selected_data_list, points[dimension], calculation_of_point_[dimension]);
	      }
	    getDataForSelectedList(selected_data_list, coord_amount);
	  }
      }
  }

  //------------------------------------------------------------------
  void NumInt::createIntPoints(Math::IntTypes int_type, unsigned* points)
  {
    switch( int_type )
      {
      case ONE_DIM:
	getDataForPoints(ONE_DIM, points);
	act_int_point_vector_ = createIntPoints(ONE_DIM);
	break;
      case TWO_DIM:
	getDataForPoints(TWO_DIM, points);
	act_int_point_vector_ = createIntPoints(TWO_DIM);
	break;
      case THREE_DIM:
	getDataForPoints(THREE_DIM, points);
	act_int_point_vector_ = createIntPoints(THREE_DIM);
	break;
      case TRIANGLE:
	getDataForPoints(TRIANGLE, points);
	act_int_point_vector_ = createIntPoints(TRIANGLE);
	break;
      case NAIT:
	break;
      }
  }

  //------------------------------------------------------------------
  void NumInt::checkCalculationOfPoints(Math::IntTypes int_type, unsigned *points)
  {
    calculation_of_point_[0] = 0;
    calculation_of_point_[1] = 1;
    calculation_of_point_[2] = 2;
    // controls access to vector<vector<IntCoord*> > *r_ etc
    // numerical integration of 2x2x4 should lead to calculation_of_point_{0,0,1} for instance
    // numerical integration of 2x4x2 should lead to calculation_of_point_{0,1,0} for instance
    // numerical integration of 2x4x6 should lead to calculation_of_point_{0,1,2} for instance
    switch( int_type )
      {
      case ONE_DIM:
	break;
      case TWO_DIM:
	if( points[0] == points[1])
	  calculation_of_point_[1] = 0;
	break;
      case THREE_DIM:
	if( points[0] == points[1])
	  {
	    calculation_of_point_[1] = 0;
	    calculation_of_point_[2] = 1;
	  }
	if( points[1] == points[2] )
	  calculation_of_point_[2] = 1;
	if( points[0] == points[2])
	  calculation_of_point_[2] = 0;
	break;
      case TRIANGLE:
	break;
      case NAIT:
	break;
      }
  }

  //------------------------------------------------------------------
  void NumInt::freeDataMembers()
  {
    alpha_ -> clear();
    multiplicity_ -> clear();

    if( r_ )
      {
	vector<vector<IntCoord*> >::iterator iter;
	for( iter = r_ -> begin() ; iter != r_ -> end() ; ++iter )
	  {
	    vector<IntCoord*>::iterator theIter;
	    for( theIter = iter->begin() ; theIter != iter->end() ; ++theIter )
	      {
		delete *theIter;
	      }
	  }
      }
    r_ -> clear();

    act_amount_of_points_[0] = 0;
    act_amount_of_points_[1] = 0;
    act_amount_of_points_[2] = 0;
    act_precision_[0] = 0;
    act_precision_[1] = 0;
    act_precision_[2] = 0;
    calculation_of_point_[0] = 0;
    calculation_of_point_[1] = 1;
    calculation_of_point_[2] = 2;
  }

  //------------------------------------------------------------------
  unsigned* NumInt::getPointsForPrecision(Math::IntTypes int_type, unsigned* precision)
  {
    unsigned final_value = 0;
    switch( int_type )
      {
      case ONE_DIM:
	if( (int_type == act_int_type_) && (precision[0] == act_precision_[0])  )
	  return(act_amount_of_points_);
	final_value = 1;
	break;
      case TWO_DIM:
	if( (int_type == act_int_type_) && (precision[0] == act_precision_[0]) &&
	    (precision[1] == act_precision_[1]) )
	  return(act_amount_of_points_);
	final_value = 2;
	break;
      case THREE_DIM:
	if( (int_type == act_int_type_) && (precision[0] == act_precision_[0]) &&
	    (precision[1] == act_precision_[1]) && (precision[2] == act_precision_[2]) )
	  return(act_amount_of_points_);
	final_value = 3;
	break;
      case TRIANGLE:
	if( (int_type == act_int_type_) && (precision[0] == act_precision_[0])  )
	  return(act_amount_of_points_);
	final_value = 1;
	break;
      case NAIT:
	final_value = 0;
	break;
      }

    unsigned coord_amount = 0;
    DOMNodeList* int_type_data_list = 0;
    DOMNodeList* selected_data_list = 0;

    freeDataMembers();

    int_type_data_list = getDataListForIntType(int_type,coord_amount);

    checkCalculationOfPoints(int_type, precision);
    for(unsigned dimension = 0; dimension < final_value; dimension++)
      {
	if(calculation_of_point_[dimension] == dimension || (calculation_of_point_[dimension] == 1 && dimension == 2) )
	  {
	    getPrecisionAndPoints(int_type_data_list, &selected_data_list, precision[dimension], calculation_of_point_[dimension]);
	    while( ! selected_data_list )
	      {
		++(precision[dimension]);
		getPrecisionAndPoints(int_type_data_list, &selected_data_list, precision[dimension], calculation_of_point_[dimension]);
	      }
	    getDataForSelectedList(selected_data_list, coord_amount);
	  }
      }

    return( act_amount_of_points_);
  }

  //------------------------------------------------------------------
  void NumInt::intScalarAtOneGaussPoint(double& scalar, Math::IntTypes int_type, Functor& functor, unsigned gauss_point_no)
  {
    act_functor_ = &functor;

    double integrate = 0;
    switch( int_type )
      {
      case ONE_DIM:
      	Integrator1D(integrate, act_int_point_vector_ -> at(gauss_point_no));
	break;
      case TWO_DIM:
      	Integrator2D(integrate, act_int_point_vector_ -> at(gauss_point_no));
	break;
      case THREE_DIM:
      	Integrator3D(integrate, act_int_point_vector_ -> at(gauss_point_no));
	break;
      case TRIANGLE:
      	Integrator2D(integrate, act_int_point_vector_ -> at(gauss_point_no));
	break;
      case NAIT:
	break;
      }
    scalar += integrate;
  }

  //------------------------------------------------------------------
  void NumInt::intArrayAtOneGaussPoint( Math::Array<double>* array, Math::IntTypes int_type, Functor& functor, unsigned gauss_point_no)
  {
    act_functor_ = &functor;
    act_array_ = new Array<double>(array->getSize(), array->getDim());
    unsigned long* index = new unsigned long[array->getDim()];
    unsigned index_count = 0;

    switch( int_type )
      {
      case ONE_DIM:
	indexCounter(index,index_count,array->getDim(),array->getSize(),ONE_DIM, act_int_point_vector_ -> at(gauss_point_no));
	break;
      case TWO_DIM:
      	indexCounter(index,index_count,array->getDim(),array->getSize(),TWO_DIM, act_int_point_vector_ -> at(gauss_point_no));
	break;
      case THREE_DIM:
      	indexCounter(index,index_count,array->getDim(),array->getSize(),THREE_DIM, act_int_point_vector_ -> at(gauss_point_no));
	break;
      case TRIANGLE:
      	indexCounter(index,index_count,array->getDim(),array->getSize(),TRIANGLE, act_int_point_vector_ -> at(gauss_point_no));
	break;
      case NAIT:
	break;
      }
    (*array) += (*act_array_);

    delete act_array_;
    act_array_ = 0;
    delete[] index;
  }

  //------------------------------------------------------------------
  double NumInt::intOneDimGaussLegendre(Functor& functor, unsigned points)
  {
    double integrate = 0.;
    act_functor_ = &functor;

    getDataForPoints(ONE_DIM, &points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(ONE_DIM);
    std::vector<IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
      {
	Integrator1D(integrate, *iter);
	delete *iter;
      }

    delete int_point_vector;

    return( integrate );
  }

  //------------------------------------------------------------------
  double NumInt::intTwoDimGaussLegendre(Functor& functor, unsigned points)
  {
    act_functor_ = &functor;
    unsigned points_array[] = {points,points};

    return( intTwoDimGaussLegendre(functor, points_array) );
  }

  //------------------------------------------------------------------
  double NumInt::intTwoDimGaussLegendre(Functor& functor, unsigned* points)
  {
    double integrate = 0.;
    act_functor_ = &functor;

    getDataForPoints(TWO_DIM, points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(TWO_DIM);
    std::vector<IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
      {
	Integrator2D(integrate, *iter);
	delete *iter;
      }

    delete int_point_vector;

    return( integrate );
  }

  //------------------------------------------------------------------
  double NumInt::intThreeDimGaussLegendre(Functor& functor, unsigned points)
  {
    act_functor_ = &functor;
    unsigned points_array[] = {points,points,points};

    return( intThreeDimGaussLegendre(functor, points_array) );
  }

  //------------------------------------------------------------------
  double NumInt::intThreeDimGaussLegendre(Functor& functor, unsigned* points)
  {
    double integrate = 0.;
    act_functor_ = &functor;

    getDataForPoints(THREE_DIM, points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(THREE_DIM);
    std::vector<IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
      {
	Integrator3D(integrate, *iter);
	delete *iter;
      }

    delete int_point_vector;

    return( integrate );
  }

  //------------------------------------------------------------------
  double NumInt::intTriangleGaussLegendre(Functor& functor, unsigned points)
  {
    double integrate = 0.;
    act_functor_ = &functor;

    getDataForPoints(TRIANGLE, &points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(TRIANGLE);
    std::vector<IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
      {
	Integrator2D(integrate, *iter);
	delete *iter;
      }

    delete int_point_vector;

    return( 0.5 * integrate );
  }

  //------------------------------------------------------------------
  Array<double>* NumInt::intOneDimArrayGaussLegendre(Functor& functor, unsigned points,
						     unsigned dimension, unsigned long* size)
  {
    act_array_ = new Array<double>(size,dimension);
    act_functor_ = &functor;

    unsigned long* index = new unsigned long[dimension];
    unsigned index_count = 0;

    getDataForPoints(ONE_DIM, &points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(ONE_DIM);
    std::vector<IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
      {
	indexCounter(index,index_count,dimension,size,ONE_DIM,*iter);
	delete *iter;
      }

    delete[] index;
    delete int_point_vector;

    return(act_array_);
  }

  //------------------------------------------------------------------
  Array<double>* NumInt::intTwoDimArrayGaussLegendre(Functor& functor, unsigned points,
						     unsigned dimension, unsigned long* size)
  {
    act_functor_ = &functor;
    unsigned points_array[] = {points,points};

    return( intTwoDimArrayGaussLegendre(functor, points_array, dimension, size) );
  }

  //------------------------------------------------------------------
  Array<double>* NumInt::intTwoDimArrayGaussLegendre(Functor& functor, unsigned* points,
						     unsigned dimension, unsigned long* size)
  {
    act_array_ = new Array<double>(size,dimension);
    act_functor_ = &functor;

    unsigned long* index = new unsigned long[dimension];
    unsigned index_count = 0;

    getDataForPoints(TWO_DIM, points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(TWO_DIM);
    std::vector<IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
      {
	indexCounter(index,index_count,dimension,size,TWO_DIM,*iter);
	delete *iter;
      }

    delete[] index;
    delete int_point_vector;

    return(act_array_);
  }

  //------------------------------------------------------------------
  Array<double>* NumInt::intThreeDimArrayGaussLegendre(Functor& functor, unsigned points,
						       unsigned dimension, unsigned long* size)
  {
    act_functor_ = &functor;
    unsigned points_array[] = {points,points,points};

    return( intThreeDimArrayGaussLegendre(functor, points_array, dimension, size) );
  }

  //------------------------------------------------------------------
  Array<double>* NumInt::intThreeDimArrayGaussLegendre(Functor& functor, unsigned* points,
						       unsigned dimension, unsigned long* size)
  {
    act_array_ = new Array<double>(size,dimension);
    act_functor_ = &functor;

    unsigned long* index = new unsigned long[dimension];
    unsigned index_count = 0;

    getDataForPoints(THREE_DIM, points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(THREE_DIM);
    std::vector<IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
      {
	indexCounter(index,index_count,dimension,size,THREE_DIM,*iter);
	delete *iter;
      }

    delete[] index;
    delete int_point_vector;

    return(act_array_);
  }

  //------------------------------------------------------------------
  Array<double>* NumInt::intTriangleArrayGaussLegendre(Functor& functor, unsigned points,
						       unsigned dimension, unsigned long* size)
  {
    act_array_ = new Array<double>(size,dimension);
    act_functor_ = &functor;

    unsigned long* index = new unsigned long[dimension];
    unsigned index_count = 0;

    getDataForPoints(TRIANGLE, &points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(TRIANGLE);
    std::vector<IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
      {
	indexCounter(index,index_count,dimension,size,TRIANGLE,*iter);
	delete *iter;
      }

    delete[] index;
    delete int_point_vector;

    act_array_ -> scale(0.5);

    return(act_array_);
  }

  //------------------------------------------------------------------
  void NumInt::Integrator1D(double& integrate, unsigned long* index, IntPoint* int_point)
  {
    integrate +=
      (int_point -> alpha_) *
      (*act_functor_)( (int_point -> int_coord_ -> r_) , index);
  }

  //------------------------------------------------------------------
  void NumInt::Integrator1D(double& integrate, IntPoint* int_point)
  {
    integrate +=
      (int_point -> alpha_) *
      (*act_functor_)( (int_point -> int_coord_ -> r_));
  }

  //------------------------------------------------------------------
  void NumInt::Integrator2D(double& integrate, unsigned long* index, IntPoint* int_point)
  {
    integrate +=
      (int_point -> alpha_) *
      (*act_functor_)( (int_point -> int_coord_ -> r_),
		       (int_point -> int_coord_ -> s_), index);
  }

  //------------------------------------------------------------------
  void NumInt::Integrator2D(double& integrate, IntPoint* int_point)
  {
    integrate +=
      (int_point -> alpha_) *
      (*act_functor_)( (int_point -> int_coord_ -> r_),
		       (int_point -> int_coord_ -> s_));
  }


  //------------------------------------------------------------------
  void NumInt::Integrator3D(double& integrate, unsigned long* index, IntPoint* int_point)
  {
    integrate +=
      (int_point -> alpha_) *
      (*act_functor_)( (int_point -> int_coord_ -> r_) ,
		       (int_point -> int_coord_ -> s_) ,
		       (int_point -> int_coord_ -> t_) , index);
  }

  //------------------------------------------------------------------
  void NumInt::Integrator3D(double& integrate, IntPoint* int_point)
  {
    integrate +=
      (int_point -> alpha_) *
      (*act_functor_)( (int_point -> int_coord_ -> r_) ,
		       (int_point -> int_coord_ -> s_) ,
		       (int_point -> int_coord_ -> t_));
  }

  //------------------------------------------------------------------
  void NumInt::indexCounter(unsigned long*& index, unsigned index_count,
			    unsigned dimension, unsigned long* size, IntTypes int_type,
			    IntPoint* int_point)
  {
    bool jump_back = false;

    for( index[index_count] = 0; index[index_count] < size[index_count] ; ++(index[index_count]) )
      {
	if( index_count < (dimension-1) )
	  {
	    indexCounter(index,index_count+1,dimension,size,int_type, int_point);
	    jump_back = true;
	  }

	if( !jump_back )
	  {
	    // prettyPrintIndex(index,dimension);
	    switch( int_type )
	      {
	      case ONE_DIM:
		Integrator1D((act_array_ -> get(index)), index, int_point);
		break;
	      case TWO_DIM:
		Integrator2D((act_array_ -> get(index)), index, int_point);
		break;
	      case THREE_DIM:
		Integrator3D((act_array_ -> get(index)), index, int_point);
		break;
	      case TRIANGLE:
		Integrator2D((act_array_ -> get(index)), index, int_point);
		break;
	      case NAIT:
		break;
	      }
	  }

	jump_back = false;
      }

    return;
  }

  //------------------------------------------------------------------
  void NumInt::prettyPrintIndex(unsigned long*& index, unsigned dimension)
  {
    unsigned disp_counter;
    static unsigned counter = 1;

    cout << "{";
    for( disp_counter = 0 ; disp_counter < (dimension-1) ; ++disp_counter )
      {
	cout << index[disp_counter] << ",";
      }
    cout << index[dimension-1] << "}" << " counter = " << (counter++) << endl;

    return;
  }

  //------------------------------------------------------------------
  void NumInt::prettyPrintData()
  {
    std::vector<vector<IntCoord*> >::iterator r_iter = r_ -> begin();
    std::vector<vector<double> >::iterator alpha_iter;
    std::vector<vector<unsigned> >::iterator mul_iter = multiplicity_ -> begin();

    std::cout << "--- " << std::endl;
    unsigned count = 1;
    for( alpha_iter = alpha_ -> begin() ; alpha_iter != alpha_ -> end() ; ++alpha_iter)
      {
	std::cout << "Dimension: " << count << std::endl;
	vector<IntCoord*>::iterator r_theIter = r_iter -> begin();
	vector<double>::iterator alpha_theIter;
	vector<unsigned>::iterator mul_theIter = mul_iter -> begin();
	for( alpha_theIter = alpha_iter -> begin() ; alpha_theIter != alpha_iter -> end() ; ++alpha_theIter)
	  {
	    cout << "r = " << (*r_theIter) -> r_ <<
	      " s = " << (*r_theIter) -> s_ <<
	      " t = " << (*r_theIter) -> t_ <<
	      " alpha = " << *(alpha_theIter) <<
	      " mul = "<< *(mul_theIter) << endl;
	    r_theIter++;
	    mul_theIter++;
	  }
	r_iter++;
	mul_iter++;
	count++;
	std::cout << "- " << std::endl;
      }
    /*
    cout << "act_amount_of_points_ = " << act_amount_of_points_[0] << "  " <<
      act_amount_of_points_[1] << "  " <<act_amount_of_points_[2] << endl;

    cout << "act_precision_        = " << act_precision_[0] << "  " <<
      act_precision_[1] << "  " <<act_precision_[2] << endl;
    cout << "calculation_of_point_ = " << calculation_of_point_[0] << "  " <<
      calculation_of_point_[1] << "  " <<calculation_of_point_[2] << endl;

    cout << "size r: " <<  r_->size() << endl;
    cout << "size alpha: " <<  alpha_->size() << endl;
    cout << "size multiplicity: " <<  multiplicity_->size() << endl;
    */
    std::cout << "--- " << std::endl;
  }

  //------------------------------------------------------------------
  void NumInt::prettyPrintIntegrationPoints()
  {
    std::vector<IntPoint*>* int_point_vector;
    std::vector<IntPoint*>::iterator iter;
    switch( act_int_type_ )
      {
      case ONE_DIM:
	int_point_vector = createIntPoints(ONE_DIM);
	cout << "ONE_DIM" << endl;
	break;
      case TWO_DIM:
	int_point_vector = createIntPoints(TWO_DIM);
	cout << "TWO_DIM" << endl;
	break;
      case THREE_DIM:
	int_point_vector = createIntPoints(THREE_DIM);
	cout << "THREE_DIM" << endl;
	break;
      case TRIANGLE:
	int_point_vector = createIntPoints(TRIANGLE);
	cout << "TRIANGLE" << endl;
	break;
      case NAIT:
	break;
      }
    if (act_int_type_ != NAIT)
      {
	for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter )
	  {
	    (*iter) -> prettyPrint();
	    delete *iter;
	  }
      }
    delete int_point_vector;
  }

  //------------------------------------------------------------------
  NumInt::IntCoordVector* NumInt::getIntCoord(IntTypes int_type, unsigned points)
  {
    IntCoordVector*int_coord_vector = new IntCoordVector;

    getDataForPoints(int_type, &points);

    std::vector<IntPoint*>* int_point_vector = createIntPoints(int_type);
    std::vector<NumInt::IntPoint*>::iterator iter;

    for( iter = int_point_vector -> begin() ; iter != int_point_vector -> end() ; ++iter)
      {
	int_coord_vector -> push_back(new IntCoord(*((*iter) -> int_coord_)));
	delete *iter;
      }

    delete int_point_vector;

    return( int_coord_vector );
  }

  //------------------------------------------------------------------
  std::vector<NumInt::IntPoint*>* NumInt::createIntPoints(IntTypes int_type)
  {
    std::vector<IntPoint*>* int_point_vector = new std::vector<IntPoint*>;
    IntPoint* int_point = 0;

    double EPS = 1.e-10;
    double r = 0.;
    double s = 0.;
    double t = 0.;

    unsigned count1 = 0;
    unsigned count2 = 0;
    unsigned count3 = 0;
    double sign1 = 1.;
    double sign2 = 1.;
    double sign3 = 1.;

    unsigned dim0 = calculation_of_point_[0];
    unsigned dim1 = calculation_of_point_[1];
    unsigned dim2 = calculation_of_point_[2];

    switch( int_type )
      {
      case ONE_DIM:
	for( count1 = 0 ; count1 < (act_amount_of_points_[dim0]) ; ++count1 )
	  {
	    if( count1 % 2 )
	      sign1 = -1.;

	    int_point = new IntPoint( ((*alpha_)[dim0])[count1/2], sign1 * ( ((*r_)[dim0])[count1/2] -> r_));
	    int_point_vector -> push_back(int_point);

	    sign1 = 1;
	  }
	break;
      case TWO_DIM:
	for( count1 = 0 ; count1 < (act_amount_of_points_[dim0]) ; ++count1 )
	  {
	    for( count2 = 0 ; count2 < (act_amount_of_points_[dim1]) ; ++count2 )
	      {
		if( count1 % 2 )
		  sign1 = -1.;
		if( count2 % 2 )
		  sign2 = -1.;

		int_point = new IntPoint( ((*alpha_)[dim0])[count1/2] * ((*alpha_)[dim1])[count2/2],
					 sign1 * ( ((*r_)[dim0])[count1/2] -> r_),
					 sign2 * ( ((*r_)[dim1])[count2/2] -> r_));
		int_point_vector -> push_back(int_point);

		sign1 = 1.;
		sign2 = 1.;
	      }
	  }
	break;
      case THREE_DIM:
	for( count1 = 0 ; count1 < (act_amount_of_points_[dim0]) ; ++count1 )
	  {
	    for( count2 = 0 ; count2 < (act_amount_of_points_[dim1]) ; ++count2 )
	      {
		for( count3 = 0 ; count3 < (act_amount_of_points_[dim2]) ; ++count3 )
		  {
		    if( count1 % 2 )
		      sign1 = -1.;
		    if( count2 % 2 )
		      sign2 = -1.;
		    if( count3 % 2 )
		      sign3 = -1.;

		    int_point = new IntPoint( ((*alpha_)[dim0])[count1/2] * ((*alpha_)[dim1])[count2/2] * ((*alpha_)[dim2])[count3/2],
					     sign1 * ( ((*r_)[dim0])[count1/2] -> r_),
					     sign2 * ( ((*r_)[dim1])[count2/2] -> r_),
					     sign3 * ( ((*r_)[dim2])[count3/2] -> r_));
		    int_point_vector -> push_back(int_point);

		    sign1 = 1.;
		    sign2 = 1.;
		    sign3 = 1.;
		  }
	      }
	  }
	break;
      case TRIANGLE:
	for( count1 = 0 ; count1 < ( ((*alpha_)[dim0]).size()) ; ++count1 )
	  {
	    r = ( ((*r_)[dim0])[count1] -> r_);
	    s = ( ((*r_)[dim0])[count1] -> s_);
	    t = 1.-(r+s);

	    switch( ((*multiplicity_)[dim0])[count1] )
	      {
	      case 1:
		int_point = new IntPoint(((*alpha_)[dim0])[count1],r,s);
		int_point_vector -> push_back(int_point);
		break;
	      case 3:
		if( fabs(r - s) < EPS )
		  {
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],r,s);
		    int_point_vector -> push_back(int_point);
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],t,r);
		    int_point_vector -> push_back(int_point);
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],r,t);
		    int_point_vector -> push_back(int_point);
		  }
		if( fabs(t - r) < EPS )
		  {
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],r,s);
		    int_point_vector -> push_back(int_point);
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],s,r);
		    int_point_vector -> push_back(int_point);
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],t,r);
		    int_point_vector -> push_back(int_point);
		  }
		if( fabs(t - s) < EPS )
		  {
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],r,s);
		    int_point_vector -> push_back(int_point);
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],s,r);
		    int_point_vector -> push_back(int_point);
		    int_point = new IntPoint( ((*alpha_)[dim0])[count1],t,s);
		    int_point_vector -> push_back(int_point);
		  }
		break;
	      case 6:
		int_point = new IntPoint( ((*alpha_)[dim0])[count1],r,s);
		int_point_vector -> push_back(int_point);
		int_point = new IntPoint( ((*alpha_)[dim0])[count1],s,r);
		int_point_vector -> push_back(int_point);
		int_point = new IntPoint( ((*alpha_)[dim0])[count1],t,r);
		int_point_vector -> push_back(int_point);
		int_point = new IntPoint( ((*alpha_)[dim0])[count1],r,t);
		int_point_vector -> push_back(int_point);
		int_point = new IntPoint( ((*alpha_)[dim0])[count1],t,s);
		int_point_vector -> push_back(int_point);
		int_point = new IntPoint( ((*alpha_)[dim0])[count1],s,t);
		int_point_vector -> push_back(int_point);
		break;
	      }
	  }
	break;
      case NAIT:
	return(0);
      }

    return( int_point_vector );
  }

} // namespace Math
