/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <boost/shared_ptr.hpp>
#include <iostream>
#include <vector>

using namespace std;

//------------------------------------------------------------------
class Node
{

};

typedef boost::shared_ptr<Node> NodePtr;

//------------------------------------------------------------------
class Element
{
public:
  Element(unsigned number);
  ~Element();

  void prettyPrint();
protected:
  vector<NodePtr> node_vector; 

  unsigned number_;
};

//------------------------------------------------------------------
Element::Element(unsigned number) :
  number_(number)
{
}

//------------------------------------------------------------------
void Element::prettyPrint()
{
  cout << "Element: " << number_ << endl;
}

//------------------------------------------------------------------
Element::~Element()
{
  cout << "destructed Element: " << number_ << endl;
}

typedef boost::shared_ptr<Element> ElementPtr;

//------------------------------------------------------------------
int main()
{
//Element* element = new Element;
  ElementPtr element( new Element(1) );

//vector<Element*> vector_1;
  vector<ElementPtr> vector_1; 
  vector<ElementPtr> vector_2; 

  vector_1.push_back(element);
  vector_2.push_back(element);

  vector_1.at(0) -> prettyPrint();
  vector_2.at(0) -> prettyPrint();


}
