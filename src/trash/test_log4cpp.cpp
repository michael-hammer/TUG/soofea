// TestLog4CPP.cpp : A small exerciser for log4cpp
#include <log4cpp/FileAppender.hh>
#include <log4cpp/BasicLayout.hh>
#include <log4cpp/Category.hh>
#include <log4cpp/Appender.hh>
#include <log4cpp/Priority.hh>
#include <log4cpp/NDC.hh>
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/BasicConfigurator.hh>
#include <iostream>

#include <string>

void test(std::string pattern, log4cpp::PatternLayout* layout, log4cpp::Category& cat)
{
    try {	
        layout->setConversionPattern(pattern);
        cat.error("message");
    } catch(log4cpp::ConfigureFailure& f) {
        std::cerr << "configure failure: " << f.what() << std::endl;
    }	
}

int main(int argc, char* argv[])
{
  // Setting up logger ###############################################################
  // 1 instantiate an appender object that 
  // will append to a log file
  log4cpp::Appender* app = new log4cpp::FileAppender("FileAppender", "testlog4cpp.log");

  // 2. Instantiate a layout object
  // Two layouts come already available in log4cpp
  // unless you create your own.
  // BasicLayout includes a time stamp
  //log4cpp::Layout* layout = new log4cpp::BasicLayout();
  log4cpp::PatternLayout* layout = new log4cpp::PatternLayout();
  /*
   * Sets the format of log lines handled by this
   * PatternLayout. By default, set to "%%m%%n".<br>
   * Format characters are as follows:<br>
   * <li><b>%%%%</b> - a single percent sign</li>
   * <li><b>%%c</b> - the category</li>
   * <li><b>%%d</b> - the date\n
   *  Date format: The date format character may be followed by a date format 
   *  specifier enclosed between braces. For example, %%d{%%H:%%M:%%S,%%l} or %%d{%%d %%m %%Y %%H:%%M:%%S,%%l}.
   *  If no date format specifier is given then the following format is used:
   *  "Wed Jan 02 02:03:55 1980". The date format specifier admits the same syntax 
   *  as the ANSI C function strftime, with 1 addition. The addition is the specifier
   *  %%l for milliseconds, padded with zeros to make 3 digits.</li>
   * <li><b>%%m</b> - the message</li>
   * <li><b>%%n</b> - the platform specific line separator</li>
   * <li><b>%%p</b> - the priority</li>
   * <li><b>%%r</b> - milliseconds since this layout was created.</li>
   * <li><b>%%R</b> - seconds since Jan 1, 1970</li>
   * <li><b>%%u</b> - clock ticks since process start</li>
   * <li><b>%%x</b> - the NDC</li>
   * @param conversionPattern the conversion pattern
   * @exception ConfigureFailure if the pattern is invalid
   */
  std::string pattern = "%d{%d.%m.%Y %H:%M:%S(%lms)} --- %p: %x %m %n";
  //std::string pattern = "%d{%d %b %Y %H:%M:%S.%l} %m %n";
  layout->setConversionPattern(pattern); 
  
  // 3. attach the layout object to the 
  // appender object
  app->setLayout(layout);
  
  // 4. Instantiate the category object
  // you may extract the root category, but it is
  // usually more practical to directly instance
  // a child category
  
  log4cpp::Category &main_cat = log4cpp::Category::getInstance("main_cat");
  
  // 5. Step 1 
  // an Appender when added to a category becomes
  // an additional output destination unless 
  // Additivity is set to false when it is false,
  // the appender added to the category replaces
  // all previously existing appenders
  main_cat.setAdditivity(false);

  // 5. Step 2
  // this appender becomes the only one
  main_cat.setAppender(app);

  // 6. Set up the priority for the category
  // NOTSET will catch all messages
  // ALERT will only catch ALERT and FATAL messages
  /*
    NOTSET < DEBUG < INFO < NOTICE < WARN < ERROR < CRIT < ALERT < FATAL = EMERG 
    DEBUG < INFO < NOTICE < WARN < ERROR < CRIT < ALERT < FATAL = EMERG 
    INFO < NOTICE < WARN < ERROR < CRIT < ALERT < FATAL = EMERG 
    NOTICE < WARN < ERROR < CRIT < ALERT < FATAL = EMERG 
    WARN < ERROR < CRIT < ALERT < FATAL = EMERG 
    ERROR < CRIT < ALERT < FATAL = EMERG 
    CRIT < ALERT < FATAL = EMERG 
    ALERT < FATAL = EMERG 
    FATAL = EMERG 
  */
  main_cat.setPriority(log4cpp::Priority::INFO);

  //log4cpp::PatternLayout* layout_pattern = new log4cpp::PatternLayout();
  //test("%% %r %c:%d (%R / %r) [%p] %x %m %% (%u) %n", layout_pattern, main_cat);
  

  // BEGIN TO WRITE LOG MESSAGES #####################################################
  // so we log some examples
  main_cat.info("This is some info");
  main_cat.debug("This is some debug message"); // not logged in this example
  main_cat.alert("This is some alert message");
  // you can log by using a log() method with a priority
  main_cat.log(log4cpp::Priority::WARN, "This will be a logged warning");
  // or by using the method
  main_cat.warn("This is some warning message");

  // gives you some programmatic control over priority levels
  log4cpp::Priority::PriorityLevel priority;
  bool this_is_critical = true;
  if(this_is_critical)
    priority = log4cpp::Priority::CRIT;
  else
    priority = log4cpp::Priority::DEBUG;
  main_cat.log(priority,"Logged message, priority is critical");
	
  // You may also log by using stream style operations on 
  main_cat.critStream() << "This will show up << as " << 1 << " critical message" << log4cpp::CategoryStream::ENDLINE;
  main_cat.emergStream() << "This will show up as " << 1 << " emergency message" << log4cpp::CategoryStream::ENDLINE;
  // for more lines
  main_cat.critStream() << "1. Line\n"
			<<"                                       "
			<< "2. Line" << log4cpp::CategoryStream::ENDLINE;

  // Stream operations can be used directly with the main object, but are preceded by the severity level
  main_cat << log4cpp::Priority::ERROR << "And this will be an error" << log4cpp::CategoryStream::ENDLINE;

  // clean up and flush all appenders
  log4cpp::Category::shutdown();
  return 0;
}
