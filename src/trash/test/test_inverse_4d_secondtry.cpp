// run as: g++ test_inverse_4d_secondtry.cpp ../../exceptions/exceptions.cpp ../../math/lin_alg.cpp -llapack

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>

#include "../../math/template/tensor.h"
#include "../../exceptions/exceptions.h"
#include "../../math/template/array.h"

using std::cout;
using std::endl;

//------------------------------------------------------------------
int main()
{
  cout << "----- TEST inverse 4d-------" << endl;
  unsigned long size2d[] = {3,3};
  unsigned long size2dsym[] = {6,6};
  unsigned long size4d[] = {3,3,3,3};
 
  // ###############################################################

  cout << "Inversion of well defined A" << endl;
  double values_A[] = {
    2.82692308e+05,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  1.21153856e+05,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  1.21153856e+05,
    0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,
    0.00000000e+00,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  0.00000000e+00,
    0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,
    1.21153856e+05,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  2.82692308e+05,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  1.21153856e+05,
    0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,
    0.00000000e+00,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  0.00000000e+00,
    0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,  8.07692308e+04,  0.00000000e+00,
    1.21153856e+05,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  1.21153856e+05,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  2.82692308e+05};
  
  Math::Tensor<double> A(size4d, 4);
  A.setArrayData(values_A);
  cout << "A" << endl;
  A("abcd").prettyPrint("abcd");

  Math::Tensor<double> A66(size2dsym,2);
  A66.convert4dSymTo2d(A);
  cout << "A 66" << endl;
  A66.prettyPrint();

  Math::Tensor<double> A_inverse(size4d, 4);
  A_inverse = A.inverse4d();
  cout << "A_inverse" << endl;
  A_inverse("abcd").prettyPrint("abcd");

  Math::Tensor<double> A_A_inverse(size4d, 4);
  A_A_inverse("ABCD").mul(A("ABgh"),A_inverse("ghCD"));
  cout << "A_A_inverse" << endl;
  A_A_inverse("abcd").prettyPrint("abcd");
  Math::Tensor<double> dmy(size4d, 4);
  dmy.identity4d();
  cout << "Identity 4d" << endl;
  dmy("abcd").prettyPrint("abcd");

  // ###############################################################

  cout << "------------" << endl;
  cout << "Inversion of arbitrary B" << endl;

  double values_B[] = {
    1.50549816e+07,  5.47453875e+06,  5.27424613e+06,  6.84326624e+06,  8.51649447e+06,  2.87262684e+06,
    2.94972890e+07,  7.08925557e+06,  5.53545759e+06,  8.81261409e+06,  9.84918670e+06,  3.72626672e+06,
    3.75959814e+07,  6.39263260e+06,  7.99565909e+06,  7.92626637e+06,  1.27879443e+06,  3.32626282e+06,
    1.94972890e+07,  7.08937377e+06,  2.53545759e+06,  8.83273409e+06,  3.84918670e+06,  3.72262672e+06,
    1.73559814e+07,  6.33273730e+06,  1.99565909e+06,  7.99373337e+06,  5.27879443e+06,  3.35262682e+06,
    9.06230770e+06,  4.29538462e+06,  0.57287154e+06,  4.11651077e+06,  3.71869231e+06,  1.73272792e+06};

  Math::Tensor<double> b1(size2dsym, 2);
  Math::Tensor<double> B(size4d, 4);
  b1.setArrayData(values_B);
  B.convert2dTo4dSym(b1);

  cout << "B abcd - minor symmetry" << endl;
  B("abcd").prettyPrint("abcd");
  cout << "B abdc - minor symmetry" << endl;
  B("abcd").prettyPrint("abdc");
  cout << "B bacd - minor symmetry" << endl;
  B("abcd").prettyPrint("bacd");
  cout << "B abdc - minor symmetry" << endl;
  B("abcd").prettyPrint("badc");
  cout << "B cdab - major symmetry" << endl;
  B("abcd").prettyPrint("cdab");
  cout << "B dcab - major symmetry" << endl;
  B("abcd").prettyPrint("dcab");

  Math::Tensor<double> B66(size2dsym,2);
  B66.convert4dSymTo2d(B);
  cout << "B 66" << endl;
  B66.prettyPrint(); 

  Math::Tensor<double> AB(size4d, 4);
  AB("ABCD").mul(A("ABgh"),B("ghCD"));
  cout << "AB" << endl;
  AB("abcd").prettyPrint("abcd");

  Math::Tensor<double> AB66_1(size2dsym,2);
  AB66_1.convert4dSymTo2d(AB);
  cout << "AB 66 convert" << endl;
  AB66_1.prettyPrint();

  Math::Tensor<double> AB66_2(size2dsym, 2);
  Math::Tensor<double> W_inverse(size2dsym, 2);
  unsigned long pos2d[] = {0,0};
  for (unsigned runner = 0; runner < 6; runner++)
    {
      pos2d[0] = runner; pos2d[1] = runner;
      if(runner < 3)
	W_inverse.write(pos2d,1.0);
      else
	W_inverse.write(pos2d,sqrt(2.0));
    }
  Math::Tensor<double> dummy(size2dsym, 2);
  Math::Tensor<double> AB66_22(size2dsym, 2);
  dummy("AB").mul(A66("Ac"),W_inverse("cB"));
  AB66_22("AB").mul(W_inverse("Ac"),dummy("cB"));
  AB66_2("AB").mul(AB66_22("Ac"),B66("cB"));
  cout << "AB 66 mul" << endl;
  AB66_2.prettyPrint(); 

  cout << "------------" << endl;
  Math::Tensor<double> B_inverse(size4d, 4);
  B_inverse = B.inverse4d();
  cout << "B_inverse" << endl;
  B_inverse("abcd").prettyPrint("abcd");
  
  Math::Tensor<double> B_B_inverse(size4d, 4);
  B_B_inverse("ABCD").mul(B("ABgh"),B_inverse("ghCD"));
  cout << "B_B_inverse" << endl;
  B_B_inverse("abcd").prettyPrint("abcd");
  cout << "Identity 4d" << endl;
  dmy("abcd").prettyPrint("abcd");

  cout << "------------" << endl;
  return(0);
}

