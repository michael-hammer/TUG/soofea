// run as: g++ test_inverse_4d_secondtry.cpp ../../exceptions/exceptions.cpp ../../math/lin_alg.cpp -llapack

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>

#include "../../math/template/tensor.h"
#include "../../exceptions/exceptions.h"
#include "../../math/template/array.h"

using std::cout;
using std::endl;

//------------------------------------------------------------------
int main()
{
  cout << "----- Sherman Morrison Woodbury Formula -------" << endl;
  unsigned long size2d[] = {3,3};
  unsigned long size2dsym[] = {6,6};
  unsigned long size4d[] = {3,3,3,3};
 
  // ###############################################################

  cout << "Inversion of well defined A" << endl;

  double values_A[] = {
    6.66666666666666666667e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,-3.33333333333333333333333333333333e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,-3.33333333333333333333333333333333e-01,
    0.00000000e+00,5.00000000e-01,0.00000000e+00,5.00000000e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,
    0.00000000e+00,0.00000000e+00,5.00000000e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,5.00000000e-01,0.00000000e+00,0.00000000e+00,
    0.00000000e+00,5.00000000e-01,0.00000000e+00,5.00000000e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,
    -3.33333333333333333333333333333333e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,6.66666666666666666667e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,-3.33333333333333333333333333333333e-01,
    0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,5.00000000e-01,0.00000000e+00,5.00000000e-01,0.00000000e+00,
    0.00000000e+00,0.00000000e+00,5.00000000e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,5.00000000e-01,0.00000000e+00,0.00000000e+00,
    0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,5.00000000e-01,0.00000000e+00,5.00000000e-01,0.00000000e+00,
    -3.33333333333333333333333333333333e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,-3.33333333333333333333333333333333e-01,0.00000000e+00,0.00000000e+00,0.00000000e+00,6.66666666666666666667e-01};
  
  Math::Tensor<double> A(size4d, 4);
  A.setArrayData(values_A);
  cout << "A" << endl;
  A("abcd").prettyPrint("abcd");
  
  double values_U[] = {
    8.00667552e-01,  -1.35865692e-02, 4.37294383e-03,
    -1.35865692e-02, -5.37100810e-01, 1.70195396e-02,
    4.37294383e-03,  1.70195396e-02,  -2.63566742e-01};
  
  Math::Tensor<double> U(size2d, 2);
  Math::Tensor<double> Utwin(size2d, 2);
  U.setArrayData(values_U);
  Utwin = U;
  cout << "U" << endl;
  U.prettyPrint();
  
  Math::Tensor<double> UU(size4d, 4);
  UU("ABCD").mul(U("AB"),Utwin("CD"));

  Math::Tensor<double> A_UU(size4d, 4);
  A_UU = A;
  A_UU += UU;  
  cout << "A_UU" << endl;
  A_UU("abcd").prettyPrint("abcd");

  Math::Tensor<double> A_UU_inverse(size4d, 4);
  A_UU_inverse = A_UU.inverse4d();
  cout << "A_UU_inverse" << endl;
  A_UU_inverse("abcd").prettyPrint("abcd");

  Math::Tensor<double> inv1(size4d, 4);
  inv1("ABCD").mul(A_UU("ABef"),A_UU_inverse("efCD"));
  cout << "inv1" << endl;
  inv1("abcd").prettyPrint("abcd");

  Math::Tensor<double> res(size4d, 4);
  res.inverse4dShermanMorrisonWoodburyFormula(A, U);
  cout << "res" << endl;
  res("abcd").prettyPrint("abcd");

  Math::Tensor<double> inv2(size4d, 4);
  inv2("ABCD").mul(A_UU("ABef"),res("efCD"));
  cout << "inv2" << endl;
  inv2("abcd").prettyPrint("abcd");

  cout << "------------" << endl;
  return(0);
}

