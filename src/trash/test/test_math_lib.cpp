/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>
#include <cstdlib> 

#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/util/XMLString.hpp>

using std::cout;
using std::endl;

XERCES_CPP_NAMESPACE_USE

#include "../math/template/array.h"
#include "../math/template/vector.h"
#include "../math/template/matrix.h"
#include "../math/template/tensor.h"
#include "../math/lin_alg.h"
#include "../math/num_int.h"
#include "../math/num_int_handler.h"

#include <exceptions.h>

void testArray1D(double * a_a, double * a_b, unsigned long size, unsigned dim);
void testArray2D(double * aa_a, double * aa_b, unsigned long* size, unsigned dim);
void testVector(double * a_a, double * a_b, unsigned long size, unsigned dim);
void testMatrix2D(double * aa_a, double * aa_b, unsigned long* size, unsigned dim);
void testTensor();

void testLinAlg(Math::Matrix2D<double>& A, Math::Vector<double>& b);
void testNumInt();

//------------------------------------------------------------------
int main()
{

  cout << "Start Test Arrays" << endl;
  cout << "------------" << endl;

  double a_a[] = {1.2, 3.4, 4.5, 5.6};
  double a_b[] = {7.8, 9.0, 2.3, 4.5};
  //  double a_c[] = {4.6, 2.5, 9.3, 1.9};
  //  double a_d[] = {3.8, 2.9, 3.7, 5.5};
  
  double aa_a[] = {1.2, 3.4, 4.5, 5.6, 7.8, 9.0, 2.3, 4.5,
		   4.6, 2.5, 9.3, 1.9, 3.8, 2.9, 3.7, 5.5, 3.8, 2.9, 3.7, 5.5};
  //   double* aa_a[5];
  //   aa_a[0] = a_a;
  //   aa_a[1] = a_b;
  //   aa_a[2] = a_c;
  //   aa_a[3] = a_d;
  //   aa_a[4] = a_d;
  
  double aa_b[] = {3.8, 2.9, 3.7, 5.5,4.6, 2.5, 9.3, 1.9,
		   7.8, 9.0, 2.3, 4.5, 1.2, 3.4, 4.5, 5.6, 1.2, 3.4, 4.5, 5.6};
  //   double* aa_b[5];
  //   aa_b[4] = a_a;
  //   aa_b[3] = a_a;
  //   aa_b[2] = a_b;
  //   aa_b[1] = a_c;
  //   aa_b[0] = a_d;
  
  double A_d[] = {1.2, 3.4, 4.5, 5.6, 7.8, 9.0, 2.3, 4.5,
		  4.6, 2.5, 9.3, 1.9, 3.8, 2.9, 3.7, 5.5};
  //   double* A_d[4];
  //   A_d[0] = a_a;
  //   A_d[1] = a_b;
  //   A_d[2] = a_c;
  //   A_d[3] = a_d;
  
  unsigned long size1 = 4;
  Math::Vector<double> f(size1);
  f.setArrayData(a_a);

  unsigned long size2[] = {5, 4};
  unsigned long new_size2[] = {4, 4};
  Math::Matrix2D<double> A(new_size2);
  A.setArrayData(A_d);

  testArray1D(a_a, a_b, size1, 1);
  testVector(a_a, a_b, size1, 1);

  testArray2D(aa_a, aa_b, size2, 2);
  testMatrix2D(aa_a, aa_b, size2, 2);

  testTensor();

  testLinAlg(A,f);
  testNumInt();

  return(0);
}

//------------------------------------------------------------------
void testArray1D(double * a_a, double * a_b, unsigned long size, unsigned dim)
{
  cout << "Test Array1D" << endl;
  cout << "------------" << endl;

  Math::Array1D<double> a(&size);
  a.setArrayData(a_a);

  Math::Array1D<double> b(&size);
  b.setArrayData(a_b);

  cout << "Create Array1D c, length=4, value=3.0" << endl;
  cout << "--" << endl;
  Math::Array1D<double> c(&size);
  c.reset(3.0);
  c.prettyPrint();

  cout << "c = a" << endl;
  cout << "--" << endl;
  c = a;
  c.prettyPrint();

  cout << "Reset c to 7.9" << endl;
  c.reset(7.9);
  c.prettyPrint();

  cout << "------------" << endl;
  return;
}

//------------------------------------------------------------------
void testVector(double * a_a, double * a_b, unsigned long size, unsigned dim)
{
  cout << "Test Vector" << endl;
  cout << "-----------" << endl;

  Math::Vector<double> a(size);
  a.setArrayData(a_a);
  Math::Vector<double> b(size);
  b.setArrayData(a_b);

  cout << "Create Vector, length=4, value=2.5" << endl;
  Math::Vector<double> c(size);
  c.reset(3.0);
  c.prettyPrint();

  cout << "Vector a = " << endl;
  a.prettyPrint();

  cout << "Vector b = " << endl;
  b.prettyPrint();

  cout << "c = a" << endl;
  c = a;
  c.prettyPrint();

  cout << "c.add(a,b)" << endl;
  c.add(a,b);
  c.prettyPrint();

  cout << "c = a + b" << endl;
  c = a + b;
  c.prettyPrint();

  cout << "c.sub(a,b)" << endl;
  c.sub(a,b);
  c.prettyPrint();

  cout << "c = a - b" << endl;
  c = a - b;
  c.prettyPrint();
  
  double result = a * b;
  cout << "Dotproduct: a * b : " << result << endl;
  result = a.norm();
  cout << "Norm von Vektor a : " << result << endl;
  
  cout << "c.scale(a,2.0)" << endl;
  c.scale(a,2.0);
  c.prettyPrint();

  cout << "c = a * 2.0" << endl;
  c = (a * 2.0);
  c.prettyPrint();

  cout << "Reset c to 7.9" << endl;
  c.reset(7.9);
  c.prettyPrint();

  cout << "------------" << endl;
  return;
}

//------------------------------------------------------------------
void testArray2D(double * aa_a, double * aa_b, unsigned long* size, unsigned dim)
{
  cout << "Test Array2D" << endl;
  cout << "------------" << endl;

  Math::Array2D<double> A(size);
  A.setArrayData(aa_a);
  Math::Array2D<double> B(size);
  B.setArrayData(aa_b);
  Math::Array2D<double> C(size);

  cout << "Array2D A:" << endl;
  A.prettyPrint();
 
  cout << "Array2D B:" << endl;
  B.prettyPrint();

  cout << "C = A" << endl;
  C = A;
  C.prettyPrint();

  cout << "get C(2,3) = " << C(2,3) << endl;
  
  cout << "write C(2,2) = 999999" << endl;
  unsigned long new_size[] = {2,2};
  C.write(new_size,99999);
  C.prettyPrint();

  cout << "Reset C to 7.9" << endl;
  C.reset(7.9);
  C.prettyPrint();

  cout << "------------" << endl;
  return;
}

//------------------------------------------------------------------
void testMatrix2D(double * aa_a, double * aa_b, unsigned long* size, unsigned dim)
{
  cout << "Test Matrix2D" << endl;
  cout << "------------" << endl;
  cout << "------------"<< endl;
  Math::Matrix2D<double> A(size);
  A.setArrayData(aa_a);
  Math::Matrix2D<double> B(size);
  B.setArrayData(aa_b);
  Math::Matrix2D<double> C(size);
  Math::Matrix2D<double> D(size);

//   double A_1[] = {1, 3, 1, 3, 4, 2};
//   double A_2[] = {2, 3, 1, 2, 2, 1};
//   unsigned long size_1[] = {3, 2};
//   unsigned long size_2[] = {2, 3};
//   Math::Matrix2D<double> Aa(A_1,size_1);
//   Math::Matrix2D<double> Bb(A_2,size_2);

  cout << "Matrix2D A:" << endl;
  A.prettyPrint();
 
  cout << "Matrix2D B:" << endl;
  B.prettyPrint();

  cout << "C = A" << endl;
  C = A;
  C.prettyPrint();

  cout << "C.add(A,B)" << endl;
  C.add(A,B);
  C.prettyPrint();

  cout << "C = A + B" << endl;
  C = A + B;
  C.prettyPrint();

  cout << "C.sub(A,B)" << endl;
  C.sub(A,B);
  C.prettyPrint();

  cout << "C = A - B" << endl;
  C = A - B;
  C.prettyPrint();

  cout << "C.scale(A,2.0)" << endl;
  C.scale(A,2.0);
  C.prettyPrint();

  cout << "C = B * 2." << endl;
  C = B * 2.0;
  C.prettyPrint();

  cout << "D.trans(A)" << endl;
  D.trans(A);
  D.prettyPrint();

  cout << "C.mul(D,B)" << endl;
  C.mul(D,B);
  C.prettyPrint();

  try {
    cout << "C = D * B" << endl;
    C = D * B;
    C.prettyPrint();
  }
  catch(MathException& Exc)
    {
      cout << Exc.getErrorStatement() << endl;
    }

  cout << "C(0,0) = 2" << endl;
  unsigned long size_c[] = {0,0};
  C.write(size_c,2);
  C.prettyPrint();

  cout << "Reset C to 7.9" << endl;
  C.reset(7.9);
  C.prettyPrint();

  cout << "------------" << endl;
  return;
}

//------------------------------------------------------------------
void testTensor()
{
  cout << "Test Tensor" << endl;
  cout << "------------" << endl; 

  double A[] = {1.2, 3.4, 4.5, 5.6, 7.8, 9.0, 2.3, 4.5,
		4.6, 2.5, 9.3, 1.9, 3.8, 2.9, 3.7, 5.5};
  double B[] = {0.0, 5.2, 1.1, 7.6, 9.9, 0.0, 2.3, 1.5,
		2.1, 0.0, 3.3, 0.0, 3.8, 0.0, 2.7, 0.5}; 
  double D[] = {10.0, 3.4, 4.5, 7.6, 7.4, 0.0, 2.3, 4.5,
		2.1, 0.0, 9.3, 0.0, 3.8, 0.0, 3.7, 0.5};

  unsigned long size1[] = {2,3};
  unsigned long size2[] = {3,2};
  unsigned long size3[] = {2,2};
  unsigned dim2 = 2;
  string char_string = "ij";

  Math::Tensor<double> Ta(size1, dim2);
  Ta.setArrayData(A);
  Math::Tensor<double> Tb(size2, dim2);
  Tb.setArrayData(B);
  Math::Tensor<double> Tc(size2, dim2);
  Tc.setArrayData(D);
  Math::Tensor<double> Td(size3, dim2);
  Math::Tensor<double> Td_inv_check(size3, dim2);
  Math::Tensor<double> Td_inv(size3, dim2);

  unsigned long size4[] = {3};
  unsigned long size5[] = {3,3};
  unsigned dim1 = 1;
  double a[] = {1.2, 3.4, 4.5};
  double b[] = {3.0, 1.9, 9.5};

  Math::Tensor<double> a1(size4, dim1);
  a1.setArrayData(a);
  Math::Tensor<double> b1(size4, dim1);
  b1.setArrayData(b);
  Math::Tensor<double> c1(size4, dim1);
  Math::Tensor<double> c2(size5, dim2);


  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check 1d tensors" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "a1" << endl;
  a1.printIndices();
  a1.prettyPrint();

  cout << "b1" << endl;
  b1.printIndices();
  b1.prettyPrint();

  cout << "c1 = a1 + b1 =  c1(''i'').add(a1(''i''),b1(''i''))" << endl;
  c1("i").add(a1("i"),b1("i"));
  c1.printIndices();
  c1.prettyPrint();

  cout << "c2 = a1 * b1 =  c2(''ij'').mul(a1(''i''),b1(''j''))" << endl;
  c2("ij").mul(a1("i"),b1("j"));
  c2.prettyPrint();

  Math::Tensor<double> d(size5, dim2);
  double dat[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  d.setArrayData(dat);
  cout << "d" << endl;
  d.printIndices();
  d.prettyPrint();

  cout << "ds = d squared" << endl;
  Math::Tensor<double> ds(size5, dim2);
  try{
  ds("op") = d("ab").squared("b");
  }
  catch(MathTensCalcException& exc) {
    cout << exc.getErrorStatement() << endl;
  }
  ds.printIndices();
  ds.prettyPrint();

  cout << "scalar = a1 * b1 =  scalar = Math::convolve(a1(''i''),b1(''i''))" << endl;
  double scalar = 0;
  scalar = Math::convolve(a1("i"),b1("i"));
  cout << scalar << endl;

  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check 2d tensors" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Ta(''ab'')" << endl;
  Ta("ab").printIndices();
  Ta.prettyPrint();
 
  cout << "Tb(''ij'')" << endl;
  Tb("ij");
  Tb.printIndices();
  Tb.prettyPrint();

  unsigned long si = 2;
  Math::Tensor<double> t1(&si, dim1);
  cout << "t1(''b'') = c1(''a'') * Tb(''ab'')" << endl;
  t1("b").mul(c1("a"),Tb("ab"));
  t1.prettyPrint();

  cout << "Tc" << endl;
  Tc.printIndices();
  Tc.prettyPrint();

  cout << "Tc = Tb" << endl;
  Tc = Tb;
  Tc.printIndices();
  Tc.prettyPrint();

  cout << "Tc = Ta + Tb =  Tc(''ki'').add(Ta(''ik''),Tb(''ki''))" << endl;
  Tc("ki").add(Ta("ik"),Tb("ki"));
  // Tc.add(Ta("ki"),Tb("ki"));    // throws exception
  Tc.printIndices();
  Tc.prettyPrint(); 

  cout << "Td = Ta * Tb =  Tc(''ij'').mul(Ta(''ik''),Tb(''kj''))" << endl;
  Td("ij").mul(Ta("ik"),Tb("kj"));
  Td.prettyPrint();

  cout << "Ta(''ab'')" << endl;
  Ta.reset(3.3);
  Ta.prettyPrint();

  cout << "Tb(''ab'')" << endl;
  Tb.reset(3.3);
  Tb.prettyPrint();

  cout << "Td = Ta * Tb =  Td(''er'').mul(Ta(''et''),Tb(''tr''))" << endl;
  //Td.clear();
  Td("er").mul(Ta("et"),Tb("tr"));
  Td.prettyPrint();

  cout << "Td(''ab'')" << endl;
  double pp[] = {1.2, 3.4, 4.5, 4,2};
  Td.setArrayData(pp);
  Td.prettyPrint();

  Math::LinAlg::createLinAlg();
  cout << "det(Td) = " << Math::LinAlg::getInstance()->det(Td) << endl;

  cout << "Td_inv = inverse of Td" << endl;
  Td_inv = Td.inverse();
  Td_inv.printIndices();
  Td_inv.prettyPrint();

  double value_con = 0;
  cout << "value = Td * Td_inv =  value = Math::convolve(Td(''ij''),Td_inv(''ji''))" << endl;
  cout << "   (value equals sum of all items of identity matrix)" << endl;  
  value_con = Math::convolve(Td("ij"),Td_inv("ji"));
  cout << value_con << endl;

  cout << "Td_inv_check = Td * Td_inv =  Td_inv_check(''ij'').mul(Td(''ik''),Td_inv(''kj''))" << endl;
  Td_inv_check("ij").mul(Td("ik"),Td_inv("kj"));
  Td_inv_check.printIndices();
  Td_inv_check.prettyPrint();

  cout << "Ta" << endl;
   Ta.printIndices();
  Ta.prettyPrint();
 
  cout << "Tb" << endl;
  Tb.printIndices();
  Tb.prettyPrint();
 
  cout << "scalar = Ta * Tb =  scalar = Math::convolve(Ta(''ij''),Tb(''ji''))" << endl;
  scalar = Math::convolve(Ta("ij"),Tb("ji"));
  cout << scalar << endl;

  unsigned long size_6[] = {5,5};
  Math::Tensor<double> Te(size_6, dim2);
  cout << "Set Te to identity tensor" << endl;
  Te.identity();
  Te.prettyPrint();

  cout << "Reset Te to 3.3 and [1,1] to 7.0" << endl;
  Te.reset(3.3);
  unsigned long index_set[] = {1,1};
  Te.write(index_set, 7.0);
  Te.prettyPrint();

  cout << "Scale Te with 0.5" << endl;
  Te.scale(0.5);
  Te.prettyPrint();

  double trace_te = Math::trace(Te);
  cout << "Trace of Te: " << trace_te << endl;

  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check 4d tensors" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  unsigned long size_7[] = {3,3,3,3};
  unsigned long dim4 = 4;
  Math::Tensor<double> T4a(size_7, dim4);
  cout << "T4a, T4a(''ijkl'').prettyPrint(''ijkl'')" << endl;
  T4a.identity();
  unsigned long pos_t[] = {1,1,1,1};
  T4a.write(pos_t, 7.77777);
  T4a("ijkl").prettyPrint("ijkl");

  double trace_t4a = Math::trace(T4a);
  cout << "Trace of T4a: " << trace_t4a << endl;

  cout << "Definition of C- Matrix: " << endl;
  double mY = 2.1e11;
  double mP = 0.3;
  double mMu = mY / (2 * ( 1 + mP));
  double mL = (mY / (3 * ( 1 - 2 * mP))) - (2 * mMu/3);

  unsigned long sizec_2[] = {3,3};
  Math::Tensor<double> I1(sizec_2, dim2);
  Math::Tensor<double> I2(sizec_2, dim2);
  I1.identity();
  I2.identity();

  unsigned long sizec_4[] = {3,3,3,3};
  Math::Tensor<double> dummy_1(sizec_4, dim4);
  Math::Tensor<double> c(sizec_4, dim4);
  Math::Tensor<double> c_inv(sizec_4, dim4);
  Math::Tensor<double> dummy_3(sizec_4, dim4);
  Math::Tensor<double> dummy_4(sizec_4, dim4);

  dummy_1("ijkl").mul(I1("ij"),I2("kl"));
  c = dummy_1;
  dummy_3("ijkl").add(dummy_1("kilj"),c("likj"));
  dummy_4("ijkl").add(dummy_1("ikjl"),c("iljk"));

  dummy_1.scale(mL);
  cout << "1st part: " << endl;
  dummy_1("ijkl").prettyPrint("ijkl");
  dummy_4.scale(0.5);
  dummy_3.scale(mMu);
  cout << "2nd part: " << endl;
  dummy_3("ijkl").prettyPrint("ijkl");

  cout << "--" << endl;
  cout << "Check symmetry of C- tensor" << endl;
  c("ijkl").add(dummy_1("ijkl"),dummy_3("ijkl"));
  cout << "C- Matrix: ijkl" << endl;
  c.prettyPrint("ijkl");
  cout << "C- Matrix: jikl" << endl;
  c.prettyPrint("jikl");
  cout << "C- Matrix: jilk" << endl;
  c.prettyPrint("jilk");
  cout << "C- Matrix: ijlk" << endl;
  c.prettyPrint("ijlk");
  cout << "C- Matrix: ikjl" << endl;
  c.prettyPrint("ikjl");
  cout << "C- Matrix: jlki" << endl;
  c.prettyPrint("jlki");

  cout << "Definition of E- Matrix: " << endl;
  Math::Tensor<double> E(size5, dim2);
  E("KL").prettyPrint();
  cout << "Definition of S- Matrix = C * E: " << endl;
  Math::Tensor<double> S(size5, dim2);
  S("IJ").mul(c("IJKL"),E("KL"));
  S("IJ").prettyPrint();
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check correct inversion of 4d tensor" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;

  cout << "c_inv = inverse of C, c_inv(''ijkl'').prettyPrint(''ijkl'')" << endl;
  c_inv = c("ijkl").inverse("ijkl");
  c_inv.printIndices();
  c_inv("ijkl").prettyPrint("ijkl");

  cout << "1/2 * (delta_ik * delta_jl + delta_il * delta_jk): dim = 3" << endl;
  dummy_4.prettyPrint("ijkl");

  cout << "scalar = c * c_inv =  scalar = Math::convolve(c(''ijkl''),c_inv(''ijkl''))" << endl;
  cout << "   (scalar has to be sum of all items of corresponding 4d identity tensor!)" << endl;
  scalar = Math::convolve(c("ijkl"),c_inv("ijkl"));
  cout << scalar << endl;

  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check inverse of arbitrary C- Matrix" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  unsigned long v = 2;
  unsigned long size_8[] = {v,v,v,v};
  unsigned long count1, count2, count3, count4;
  Math::Tensor<double> C(size_8, dim4);
  Math::Tensor<double> Tcont(size_8, dim4);
  Math::Tensor<double> C_inv(size_8, dim4);
  double value =  0;
  unsigned long* index_4d = new unsigned long[4];
  for (count1 = 0; count1 < size_8[0]; ++count1)
    {
      for (count2 = 0; count2 < size_8[1]; ++count2)
	{
	  for (count3 = 0; count3 < size_8[2]; ++count3)
	    {
	      for (count4 = 0; count4 < size_8[3]; ++count4)
		{
		  value = rand();
		  index_4d[0] = count1;
		  index_4d[1] = count2;
		  index_4d[2] = count3;
		  index_4d[3] = count4;
		  v++;
		  C.write( index_4d, value );
		}
	    }
	}
    }
  delete[] index_4d;
  index_4d = 0;

  cout << "C, C(''ijkl'').prettyPrint(''ijkl'')" << endl;
  C("ijkl").prettyPrint("ijkl");

  cout << "C_inv = inverse of C, C_inv(''ijkl'').prettyPrint(''ijkl'')" << endl;
  C_inv = C("ijkl").inverse("ijkl");

  cout << "C_inv: ijkl" << endl;
  C_inv("ijkl").prettyPrint("ijkl");
  cout << "C_inv: jikl" << endl;
  C_inv("ijkl").prettyPrint("jikl");
  cout << "C_inv: klij" << endl;
  C_inv("ijkl").prettyPrint("klij");
  cout << "C_inv: ikjl" << endl;
  C_inv("ijkl").prettyPrint("iljk");
  cout << "C_inv: lkij" << endl;
  C_inv.prettyPrint("lkij");

  cout << "scalar = C * C_inv =  scalar = Math::convolve(C(''ijkl''),C_inv(''ijkl''))" << endl;
  cout << "   has to be sum of all items of corresponding identity matrix" << endl;
  scalar = Math::convolve(C("ijkl"),C_inv("ijkl"));
  cout << scalar << endl;

  unsigned long sizec_22[] = {2,2};
  Math::Tensor<double> i1(sizec_22, dim2);
  Math::Tensor<double> i2(sizec_22, dim2);
  i1.identity();
  i2.identity();

  Math::Tensor<double> d_1(size_8, dim4);
  Math::Tensor<double> d_2(size_8, dim4);
  Math::Tensor<double> d_3(size_8, dim4);

  d_1("ijkl").mul(i1("ij"),i2("kl"));
  d_2 = d_1;
  d_3("ijkl").add(d_1("ikjl"),d_2("iljk"));
  d_3.scale(0.5);
  
  cout << "1/2 * (delta_ik * delta_jl + delta_il * delta_jk): dim = 2" << endl;
  d_3.prettyPrint("ijkl");

  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check 6d tensor" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;

  unsigned long size_d6[] = {2,2,2,2,2,2};
  Math::Tensor<double> T1_6(size_d6, 6);
  size_d6[0] = 1;
  size_d6[1] = 0;
  size_d6[2] = 0;
  size_d6[3] = 1;
  size_d6[4] = 0;
  size_d6[5] = 1;
  //T1_6.write(size_d6,34);
  T1_6("ijklab").mul(d_3("ijkw"),C_inv("labw"));
  unsigned long si_d6[] = {2,2};
  Math::Tensor<double> res(si_d6, 2);
  res("ab").mul(T1_6("ijklab"),C_inv("ijkl"));

  cout << "  res(''ab'').mul(T1_6(''ijklab''),C_inv(''ijkl''))" << endl;
  res("ab").prettyPrint();

  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check Invariants and Spectral Decomposition" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;

  unsigned long size33[] = {3,3};
  Math::Tensor<double> tenA(size33, 2);
  double data_1[] = {10.1, 3.4, 4.5, 7.6, 7.4, 3.3, 2.3, 4.5, 2.1};
  tenA.setArrayData(data_1);
  cout << "tenA " << endl;
  tenA("ab").prettyPrint();
  double I = 0;
  double II = 0;
  double III = 0;
  tenA.invariants(I,II,III);
  std::cout << "I: " << I << " II: " << II << " III: " << III << std::endl;
  Math::Tensor<double> R(size33, 2);
  Math::Tensor<double> U(size33, 2);
  try {
  Math::polarRight(tenA,R,U);
  }
  catch(MathTensCalcException& exc) {
    cout << exc.getErrorStatement() << endl;
  }
  cout << "Polar right decompositon " << endl;
  cout << "R: " << endl;
  R.prettyPrint();
  cout << "U: " << endl;
  U.prettyPrint();
  Math::Tensor<double> res_tenA(size33, 2);
  res_tenA("ab").mul(R("ac"),U("cb"));
  cout << "re- calculation of tenA: " << endl;
  res_tenA.prettyPrint();
  cout << "==   ==   ==   ==   ==" << endl;
  Math::Tensor<double> tenA1(size33, 2);
  tenA1.identity();
  cout << "tenA1 " << endl;
  tenA1("ab").prettyPrint();
  I = 0;
  II = 0;
  III = 0;
  tenA1.invariants(I,II,III);
  std::cout << "I: " << I << " II: " << II << " III: " << III << std::endl;
  Math::Tensor<double> R1(size33, 2);
  Math::Tensor<double> U1(size33, 2);
  try {
  Math::polarRight(tenA1,R1,U1);
  }
  catch(MathTensCalcException& exc) {
    cout << exc.getErrorStatement() << endl;
  }
  cout << "Polar right decompositon " << endl;
  cout << "R1: " << endl;
  R1.prettyPrint();
  cout << "U1: " << endl;
  U1.prettyPrint();
  Math::Tensor<double> res_tenA1(size33, 2);
  res_tenA1("ab").mul(R1("ac"),U1("cb"));
  cout << "re- calculation of tenA1: " << endl;
  res_tenA1.prettyPrint();
  cout << "==   ==   ==   ==   ==" << endl;
  unsigned long size55[] = {5,5};
  Math::Tensor<double> tenA2(size55, 2);
  double data_2[] = {33.8926789267909026780087272, 35.262652626264, 3.2627872616715265, 73.3738738386, 51.27267367467474, 13.38868483, 2.38068984323, 14.262652465, 82.26262661,33.8926789267909026780087272, 35.262652626264, 3.2627872616715265, 73.3738738386, 51.27267367467474, 13.38868483, 2.38068984323, 14.262652465, 82.26262661,33.8926789267909026780087272, 35.262652626264, 3.2627872616715265, 73.3738738386, 51.27267367467474, 13.38868483, 2.38068984323, 14.262652465, 82.26262661,33.89267892 };
  tenA2.setArrayData(data_2);
  cout << "tenA2 " << endl;
  tenA2("ab").prettyPrint();
  I = 0;
  II = 0;
  III = 0;
  tenA2.invariants(I,II,III);
  std::cout << "I: " << I << " II: " << II << " III: " << III << std::endl;
  Math::Tensor<double> R2(size55, 2);
  Math::Tensor<double> V2(size55, 2);
  try {
  Math::polarLeft(tenA2,V2,R2);
  }
  catch(MathTensCalcException& exc) {
    cout << exc.getErrorStatement() << endl;
  }
  cout << "Polar left decompositon " << endl;
  cout << "R2: " << endl;
  R2.prettyPrint();
  cout << "V2: " << endl;
  V2.prettyPrint();
  Math::Tensor<double> res_tenA2(size55, 2);
  res_tenA2("ab").mul(V2("ac"),R2("cb"));
  cout << "re- calculation of tenA2: " << endl;
  res_tenA2.prettyPrint();

  Math::Tensor<double>  dummy(size55, 2);
  Math::Tensor<double> tenA2_root(size55, 2);
  Math::Tensor<double> tenA2_rootd1(size55, 2);
  Math::Tensor<double> tenA2_rootd2(size55, 2);
  double data_3[] = {1,2,3,4,5,2,7,3,4,5,3,3,5.5,4,5,4,4,4,3.3,6,5,5,5,6,2.11};
  tenA2_rootd1.setArrayData(data_3);
  tenA2_rootd2.setArrayData(data_3);
  tenA2("IJ").mul(tenA2_rootd1("IK"),tenA2_rootd2("KJ"));
  double data_33[] = {6,-2,2,0,2,-2,5,0,0,0,0,0,7,0,0,0,0,0,1,1,0,0,0,1,1};
  tenA2.setArrayData(data_33);
  cout << "ten_A2: " << endl;
  tenA2.prettyPrint();
  try {
    Math::findroots(tenA2, tenA2_root, dummy );
  }
  catch(MathTensCalcException& exc) {
    cout << exc.getErrorStatement() << endl;
  }
  Math::Tensor<double> tenA2_check(size55, 2);
  tenA2_check = tenA2_root("ab").squared("a");
  cout << "Check of Root ten_A2: " << endl;
  tenA2_check.prettyPrint();

  unsigned long size44[] = {4,4};
  Math::Tensor<double>  dummy44(size44, 2);
  Math::Tensor<double> tenA244(size44, 2);
  Math::Tensor<double> tenA2_root44(size44, 2);
  double data_4444[] = {6,-2,2,2,-2,5,0,0,0,0,7,0,0,0,0,1,1,0,0,1,1};
  tenA244.setArrayData(data_4444);
  cout << "ten_A244: " << endl;
  tenA244.prettyPrint();
  try {
    Math::findroots(tenA244, tenA2_root44, dummy44 );
  }
  catch(MathTensCalcException& exc) {
    cout << exc.getErrorStatement() << endl;
  }
  Math::Tensor<double> tenA2_check44(size44, 2);
  tenA2_check44 = tenA2_root44("ab").squared("a");
  cout << "Check of Root ten_A244: " << endl;
  tenA2_check44.prettyPrint();

  unsigned long size22[] = {2,2};
  Math::Tensor<double>  dummyy(size22, 2);
  Math::Tensor<double> t22(size22, 2);
  Math::Tensor<double> t22r(size22, 2);
  double data_4[] = {33.33,22.22,22.22,25.25 };
  t22.setArrayData(data_4);
  cout << "t22: " << endl;
  t22.prettyPrint();
  try {
    Math::findroots(t22, t22r, dummyy );
  }
  catch(MathTensCalcException& exc) {
    cout << exc.getErrorStatement() << endl;
  }
  Math::Tensor<double> t22c(size22, 2);
  t22c = t22r("ab").squared("a");
  cout << "Check of Root t22: " << endl;
  t22c.prettyPrint();


  cout << "------------" << endl;
  return;
}

//------------------------------------------------------------------
void testLinAlg(Math::Matrix2D<double>& A, Math::Vector<double>& b)
{
  cout << "Test LinAlg" << endl;
  cout << "------------" << endl;  

  Math::LinAlg::createLinAlg();

  cout << "Matrix A:" << endl;
  A.prettyPrint();
  cout << "det(A) = " << Math::LinAlg::getInstance()->det(A) << endl;
  
  //  double b_1[] = {10, 2.4};
  //  double b_2[] = {3, 5.3};
  double B_d[] = {10, 2.4, 3, 5.3};
  //   double* B_d[2];
  //   B_d[0] = b_1;
  //   B_d[1] = b_2;
  unsigned long size[] = {2,2};

  Math::Matrix2D<double> B(size);
  B.setArrayData(B_d);

  cout << "Matrix B:" << endl;
  B.prettyPrint();

  cout << "B(0,0) = 3.2" << endl;
  size[0] = 0;
  size[1] = 0;
  B.write(size,3.2);
  cout << "Matrix B:" << endl;
  B.prettyPrint();

  cout << "det(B) = " << Math::LinAlg::getInstance()->det(B) << endl;

  cout << "Reset B to 0:" << endl;
  B.reset(0.0);
  B.prettyPrint();


  size[0] = 2;
  size[1] = 2;
  try {
    Math::Matrix2D<double> C(size);
    C.reset(1.0);
    size[0] = 0;
    size[1] = 0;
    C.write(size,3.2);
    C.prettyPrint();
    Math::LinAlg::getInstance()->lu_inverse(C, B);
    cout << "Matrix B (inverse of C):" << endl;
    B.prettyPrint();
  }
  catch(MathLinAlgException& exc) {
    cout << exc.getErrorStatement() << endl;
  }

  unsigned long size3[] = {3,3};
  Math::Array<double> lnT(size3,2);
  Math::Array<double> T(size3,2);

  double Tdata[] = {3.13,1.91,5,1.91,2.2,3.3,5,3.3,234};
  T.setArrayData(Tdata);

  Math::LinAlg::getInstance()->ln(T, lnT);
  cout << "T:" << endl;
  T.prettyPrint();
  cout << "ln(T):" << endl;
  lnT.prettyPrint();


  cout << "------------" << endl;  
  Math::LinAlg::terminate();
  
  cout << "------------" << endl;
  return;
}

//------------------------------------------------------------------
class A
{
public:
  A(unsigned long* size, unsigned dimension);
  ~A();
  
  double function1D(double r);
  double function2D(double r, double s);
  double function3D(double r, double s, double t);
  
  double array1D(double r, unsigned long* index);
  double array2D(double r, double s, unsigned long* index);
  double array3D(double r, double s, double t, unsigned long* index);
  double array3D_2(double r, double s, double t, unsigned long* index);
  double array3D_3(double r, double s, double t, unsigned long* index);
protected:
  Math::Array<double>* array_;
};

//------------------------------------------------------------------
A::A(unsigned long* size, unsigned dimension) :
  array_(new Math::Array<double>(size,dimension))
{
}

//------------------------------------------------------------------
A::~A()
{
  delete array_;
}

//------------------------------------------------------------------
double A::function1D(double r)
{
  return( 5.*r*r*r - r*r + r);
}

//------------------------------------------------------------------
double A::function2D(double r, double s)
{
  return( 5. + r*r*s + 2.*r*r*s*s );
}

//------------------------------------------------------------------
double A::function3D(double r, double s, double t)
{
  return( 3. + r*s*t + 5.*r*r );
}

//------------------------------------------------------------------
double A::array1D(double r, unsigned long* index)
{
  array_ -> reset( 5.*r*r*r - r*r + r );

  return((*array_)(index));
}

//------------------------------------------------------------------
double A::array2D(double r, double s, unsigned long* index)
{
  array_ -> reset( 5. + r*r*s + 2.*r*r*s*s );
  
  return((*array_)(index));
}

//------------------------------------------------------------------
double A::array3D(double r, double s, double t, unsigned long* index)
{
  array_ -> reset( 3. + r*s*t + 5.*r*r );
  
  return((*array_)(index));
}

//------------------------------------------------------------------
double A::array3D_2(double r, double s, double t, unsigned long* index)
{
  array_ -> reset( 1. + s*t + 2.*r*r );
  
  return((*array_)(index));
}

//------------------------------------------------------------------
double A::array3D_3(double r, double s, double t, unsigned long* index)
{
  array_ -> reset( 1 );
  
  return((*array_)(index));
}

//------------------------------------------------------------------
void testNumInt()
{
  cout << "Test NumInt" << endl;
  cout << "------------" << endl;  

  Math::NumInt::createNumInt("math/num_int.xml");

  unsigned dimension = 2;
  unsigned long size[2] = {3,3};
  unsigned long size_b[2] = {4,4};
  unsigned long size_c[2] = {2,2};

  A* a_class = new A(size,dimension);
  A* b_class = new A(size_b,dimension);
  A* c_class = new A(size_c,dimension);

  Math::FunctorScalar<A> functor1D = Math::FunctorScalar<A>( a_class, &A::function1D );
  Math::FunctorScalar<A> functor2D = Math::FunctorScalar<A>( a_class, &A::function2D );
  Math::FunctorScalar<A> functor3D = Math::FunctorScalar<A>( a_class, &A::function3D );

  Math::FunctorArray<A> functor1D_array = Math::FunctorArray<A>( a_class, &A::array1D );
  Math::FunctorArray<A> functor2D_array = Math::FunctorArray<A>( a_class, &A::array2D );
  Math::FunctorArray<A> functor3D_array = Math::FunctorArray<A>( a_class, &A::array3D );
  Math::FunctorArray<A> functor3D_array_2 = Math::FunctorArray<A>( b_class, &A::array3D_2 );
  Math::FunctorArray<A> functor3D_array_3 = Math::FunctorArray<A>( c_class, &A::array3D_3 );
  //Math::NumInt::getInstance() -> prettyPrintData();

  cout << "Integrate1D:{1} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,1) << endl;;
  cout << "Integrate1D:{2} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,2) << endl;;
  cout << "Integrate1D:{3} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,3) << endl;;
  cout << "Integrate1D:{4} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,4) << endl;;
  cout << "Integrate1D:{5} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,5) << endl;;
  cout << "Integrate1D:{6} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,6) << endl;;
  cout << "Integrate1D:{7} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,7) << endl;;
  cout << "Integrate1D:{8} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,8) << endl;;
  cout << "Integrate1D:{9} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,9) << endl;;
  cout << "Integrate1D:{10} " << Math::NumInt::getInstance() -> intOneDimGaussLegendre(functor1D,10) << endl;;
  cout << "Integrate2D:{1} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,1) << endl;;
  cout << "Integrate2D:{2} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,2) << endl;;
  cout << "Integrate2D:{3} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,3) << endl;;
  cout << "Integrate2D:{4} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,4) << endl;;
  cout << "Integrate2D:{5} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,5) << endl;;
  cout << "Integrate2D:{6} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,6) << endl;;
  cout << "Integrate2D:{7} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,7) << endl;;
  cout << "Integrate2D:{8} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,8) << endl;;
  cout << "Integrate2D:{9} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,9) << endl;;
  cout << "Integrate2D:{10} " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,10) << endl;;
  unsigned int_size[] = {5,6};
  cout << "Integrate2D{5,6}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 2;
  int_size[1] = 8;
  cout << "Integrate2D{2,8}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 5;
  int_size[1] = 5;
  cout << "Integrate2D{5,5}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 7;
  int_size[1] = 1;
  cout << "Integrate2D{7,1}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 10;
  int_size[1] = 4;
  cout << "Integrate2D{10,4}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 6;
  int_size[1] = 3;
  cout << "Integrate2D{6,3}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 1;
  int_size[1] = 1;
  cout << "Integrate2D{1,1}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  int_size[0] = 9;
  int_size[1] = 9;
  cout << "Integrate2D{9,9}: " << Math::NumInt::getInstance() -> intTwoDimGaussLegendre(functor2D,int_size) << endl;;
  cout << "Integrate3D:{1} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,1) << endl;;
  cout << "Integrate3D:{2} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,2) << endl;;
  cout << "Integrate3D:{3} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,3) << endl;;
  cout << "Integrate3D:{4} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,4) << endl;;
  cout << "Integrate3D:{5} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,5) << endl;;
  cout << "Integrate3D:{6} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,6) << endl;;
  cout << "Integrate3D:{7} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,7) << endl;;
  cout << "Integrate3D:{8} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,8) << endl;;
  cout << "Integrate3D:{9} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,9) << endl;;
  cout << "Integrate3D:{10} " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,10) << endl;;
  unsigned int_siz[] = {5,6,7};
  cout << "Integrate3D{5,6,7}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 2;
  int_siz[1] = 8;
  int_siz[2] = 2;
  cout << "Integrate3D{2,8,2}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 5;
  int_siz[1] = 5;
  int_siz[2] = 5;
  cout << "Integrate3D{5,5,5}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 7;
  int_siz[1] = 1;
  int_siz[2] = 1;
  cout << "Integrate3D{7,1,1}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 10;
  int_siz[1] = 4;
  int_siz[2] = 5;
  cout << "Integrate3D{10,4,5}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 2;
  int_siz[1] = 3;
  int_siz[2] = 1;
  cout << "Integrate3D{2,3,1}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 1;
  int_siz[1] = 1;
  int_siz[2] = 6;
  cout << "Integrate3D{1,1,6}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  int_siz[0] = 10;
  int_siz[1] = 10;
  int_siz[2] = 2;
  cout << "Integrate3D{10,10,2}: " << Math::NumInt::getInstance() -> intThreeDimGaussLegendre(functor3D,int_siz) << endl;;
  //Math::NumInt::getInstance() -> prettyPrintData();
  cout << "IntegrateTriangle:{3} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,3) << endl;;
  cout << "IntegrateTriangle:{4} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,4) << endl;;
  cout << "IntegrateTriangle:{6} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,6) << endl;;
  cout << "IntegrateTriangle:{7} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,7) << endl;;
  cout << "IntegrateTriangle:{9} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,9) << endl;;
  cout << "IntegrateTriangle:{12} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,12) << endl;;
  cout << "IntegrateTriangle:{13} " << Math::NumInt::getInstance() -> intTriangleGaussLegendre(functor2D,13) << endl;;

  Math::Array<double>* array = 0;

  cout << "Integrate1DArray:{7} " << endl; 
  array = Math::NumInt::getInstance() -> intOneDimArrayGaussLegendre(functor1D_array,7,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{7} " << endl;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,7,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{5,6} " << endl;
  int_size[0] = 5;
  int_size[1] = 6;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,int_size,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{3,3} " << endl;
  int_size[0] = 3;
  int_size[1] = 3;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,int_size,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{7,2} " << endl;
  int_size[0] = 7;
  int_size[1] = 2;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,int_size,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate2DArray:{1,10} " << endl;
  int_size[0] = 1;
  int_size[1] = 10;
  array = Math::NumInt::getInstance() -> intTwoDimArrayGaussLegendre(functor2D_array,int_size,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{6} " << endl;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,6,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{7,2,8} " << endl;
  int_siz[0] = 7;
  int_siz[1] = 2;
  int_siz[2] = 8;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{10,4,2} " << endl;
  int_siz[0] = 10;
  int_siz[1] = 4;
  int_siz[2] = 2;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{1,1,2} " << endl;
  int_siz[0] = 1;
  int_siz[1] = 1;
  int_siz[2] = 2;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{7,7,7} " << endl;
  int_siz[0] = 7;
  int_siz[1] = 7;
  int_siz[2] = 7;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{4,4,8} " << endl;
  int_siz[0] = 4;
  int_siz[1] = 4;
  int_siz[2] = 8;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{3,2,2} " << endl;
  int_siz[0] = 3;
  int_siz[1] = 2;
  int_siz[2] = 2;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "Integrate3DArray:{6,10,6} " << endl;
  int_siz[0] = 6;
  int_siz[1] = 10;
  int_siz[2] = 6;
  array = Math::NumInt::getInstance() -> intThreeDimArrayGaussLegendre(functor3D_array,int_siz,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "IntegrateTriangleArray: " << endl;
  array = Math::NumInt::getInstance() -> intTriangleArrayGaussLegendre(functor2D_array,13,dimension,size);
  array -> prettyPrint();
  delete array;

  cout << "--- " << endl; 
  int_siz[0] = 4;
  int_siz[1] = 10;
  int_siz[2] = 8;
  cout << "Numerical Integration Handler: " << endl;
  cout << "Gauss Point: {"<< int_siz[0]<< ","<< int_siz[1]<<","<<int_siz[2] <<"}"<< endl; 

  Math::NumIntHandler *my_num_int_handler = 0;
  my_num_int_handler = new Math::NumIntHandler(Math::THREE_DIM);

  // has te be 3by3 matrices due to start implementation
  Math::Array<double>* array1 = 0;
  unsigned long array_dim1[] = {3,3};
  array1 = new Math::Array<double>(array_dim1,2);
  Math::Array<double>* array2 = 0;
  unsigned long array_dim2[] = {4,4};
  array2 = new Math::Array<double>(array_dim2,2);
  Math::Array<double>* array3 = 0;
  unsigned long array_dim3[] = {2,2};
  array3 = new Math::Array<double>(array_dim3,2);

  double int1 = 0;
  double int2 = 0;

  my_num_int_handler->addArray(functor3D_array, array1);
  my_num_int_handler->addArray(functor3D_array_2, array2);
  my_num_int_handler->addArray(functor3D_array_3, array3);

  my_num_int_handler->addScalar(functor3D, &int1);
  my_num_int_handler->addScalar(functor3D, &int2);

  my_num_int_handler->go(3,int_siz);

  //my_num_int_handler -> prettyPrint();
  cout << "Integrate Array1: " << endl;
  array1 -> prettyPrint();
  cout << "Integrate Array2: " << endl;
  array2 -> prettyPrint();
  cout << "Integrate Array3: " << endl;
  array3 -> prettyPrint();

  cout << "Scalar int1: " << endl;
  std::cout << int1 << std::endl;
  cout << "Scalar int2: " << endl;
  std::cout << int2 << std::endl;

  cout << "getPointsForPrecision: " << endl;
  unsigned precision[] = {3,6,10};
  cout << "Precision: " << endl;
  cout << precision[0] << "  " << precision[1] << "  " << precision[2] << endl;
  unsigned* points = Math::NumInt::getInstance() -> getPointsForPrecision(Math::THREE_DIM, precision);
  cout << "Points: " << endl;
  cout << points[0] << "  " << points[1] << "  " << points[2] << endl;

  delete my_num_int_handler;
  cout << "END Numerical Integration Handler: " << endl;
  cout << "--- " << endl;

  delete array1;
  delete array2;
  delete array3;

  delete a_class;
  delete b_class;
  delete c_class;
  Math::NumInt::terminate();
}

