/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>

#include "../../math/template/tensor.h"
#include "../../exceptions/exceptions.h"
#include "../../math/template/array.h"

using std::cout;
using std::endl;

//------------------------------------------------------------------
int main()
{
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check inverse of arbitrary C- Matrix" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  unsigned long v = 2;
  unsigned long dim4 = 4;
  unsigned long dim2 = 2;
  unsigned long size_4[] = {v,v,v,v};
  unsigned long size_2[] = {v,v,v,v};
  unsigned long count1, count2, count3, count4;
  Math::Tensor<double> C(size_4, dim4);
  Math::Tensor<double> C_inv(size_4, dim4);
  Math::Tensor<double> C_inv_inv(size_4, dim4);
  double value =  0;
  unsigned long* index_4d = new unsigned long[4];
  for (count1 = 0; count1 < size_4[0]; ++count1)
    {
      for (count2 = 0; count2 < size_4[1]; ++count2)
	{
	  for (count3 = 0; count3 < size_4[2]; ++count3)
	    {
	      for (count4 = 0; count4 < size_4[3]; ++count4)
		{
		  value = rand();
		  index_4d[0] = count1;
		  index_4d[1] = count2;
		  index_4d[2] = count3;
		  index_4d[3] = count4;
		  v++;
		  C.write( index_4d, value );
		}
	    }
	}
    }
  delete[] index_4d;
  index_4d = 0;

  cout << "C, C(''ijkl'').prettyPrint(''ijkl'')" << endl;
  C("ijkl").prettyPrint("ijkl");

  cout << "C_inv = inverse of C, C_inv(''ijkl'').prettyPrint(''ijkl'')" << endl;
  C_inv = C("ijkl").inverse();
  C_inv("ijkl").prettyPrint("ijkl");

  cout << "C_inv_inv = inverse of C_inv, C_inv_inv(''ijkl'').prettyPrint(''ijkl'')" << endl;
  C_inv_inv = C_inv("ijkl").inverse();
  C_inv_inv("ijkl").prettyPrint("ijkl");

  double scalar = 0;
  cout << "scalar = C * C_inv =  scalar = Math::convolve(C(''ijkl''),C_inv(''ijkl''))" << endl;
  cout << "   has to be sum of all items of corresponding identity matrix" << endl;
  scalar = Math::convolve(C("ijkl"),C_inv("ijkl"));
  cout << scalar << endl;

  Math::Tensor<double> i1(size_2, dim2);
  Math::Tensor<double> i2(size_2, dim2);
  i1.identity();
  i2.identity();

  Math::Tensor<double> d_1(size_4, dim4);
  Math::Tensor<double> d_2(size_4, dim4);
  Math::Tensor<double> d_3(size_4, dim4);

  d_1("ijkl").mul(i1("ij"),i2("kl"));
  d_2 = d_1;
  d_3("ijkl").add(d_1("ikjl"),d_2("iljk"));
  d_3.scale(0.5);
  
  cout << "Identity Tensor" << endl;
  cout << "1/2 * (delta_ik * delta_jl + delta_il * delta_jk): dim = 2" << endl;
  d_3.prettyPrint("ijkl");
 
  cout << "------------" << endl;

  cout << "==   ==   ==   ==   ==   ==   ==" << endl;
  cout << "Check inverse of symmetric C- Matrix" << endl;
  cout << "==   ==   ==   ==   ==   ==   ==" << endl;

  Math::Tensor<double> Csym(size_4, dim4);
  Math::Tensor<double> Csym_inv(size_4, dim4);
  Math::Tensor<double> Csym_inv_inv(size_4, dim4);

  double A[] = {111, 45.6, 45.6, 444, 59, 66, 170, 8.78, 59, 170, 66, 8.78, 13, 184, 184, 16};
  Csym.setArrayData(A);

  cout << "Csym, Csym(''ijkl'').prettyPrint(''ijkl'')" << endl;
  Csym("ijkl").prettyPrint("ijkl");

  cout << "Csym_inv = inverse of Csym, Csym_inv(''ijkl'').prettyPrint(''ijkl'')" << endl;
  Csym_inv = Csym("ijkl").inverse();
  Csym_inv("ijkl").prettyPrint("ijkl");

  cout << "Csym_inv_inv = inverse of Csym_inv, Csym_inv_inv(''ijkl'').prettyPrint(''ijkl'')" << endl;
  Csym_inv_inv = Csym_inv("ijkl").inverse();
  Csym_inv_inv("ijkl").prettyPrint("ijkl");

  scalar = 0;
  cout << "scalar = Csym * Csym_inv =  scalar = Math::convolve(C(''ijkl''),C_inv(''ijkl''))" << endl;
  cout << "   has to be sum of all items of corresponding identity matrix" << endl;
  scalar = Math::convolve(Csym("ijkl"),Csym_inv("ijkl"));
  cout << scalar << endl;

  cout << "Identity Tensor" << endl;
  cout << "1/2 * (delta_ik * delta_jl + delta_il * delta_jk): dim = 2" << endl;
  d_3.prettyPrint("ijkl");
 
  cout << "------------" << endl;
  return(0);
}
