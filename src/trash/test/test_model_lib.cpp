/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include "../model/model_handler.h"
#include "../math/lin_alg.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

int main(int argc, char *argv[])
{
  string input_name;
  string config_file;
  string output_file;

  cout << "Testprogramm fuer model_handler_lib" << endl;
  cout << "-----------------------------------" << endl;

  if (argc < 4)
    {
      cout << "ERROR" << endl;
      cout << "program usage:" << endl;
      cout << "test_libsoofea_model_handler <input_file_name> <output_file_name> <config_file_name>" << endl;
      return -1;
    }

  if (argc > 4)
    {
      cout << "ERROR" << endl;
      cout << "too many arguments given!" << endl;
    }

  input_name = argv[1];
  output_file = argv[2];
  config_file = argv[3];

  Math::LinAlg::createLinAlg();

  ModelHandler *model_handler = new ModelHandler(input_name,XML,output_file,GiD,&config_file);
  model_handler -> createModel();

  delete model_handler;
  Math::LinAlg::terminate();
}
