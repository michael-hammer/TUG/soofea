/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>
#include <cstdlib> 

using std::cout;
using std::endl;

#include "../math/template/tensor.h"

//------------------------------------------------------------------
int main()
{
  cout << "Start Speed Check" << endl;
  cout << "------------" << endl;
  
  double B[] = {1.2, 3.4, 4.5, 5.6, 7.8, 9.0, 2.3, 4.5, 3.8,
		7.8, 9.0, 2.3, 4.5, 1.2, 3.4, 4.5, 5.6, 1.2,
		7.8, 9.0, 2.3, 4.5, 5.6, 1.2, 3.4, 4.5, 5.6,
		3.7, 5.5, 4.6, 2.5, 4.5, 1.2, 3.4, 3.7, 1.5,
		7.8, 9.0, 2.3, 4.5, 1.2, 3.4, 4.5, 5.6, 1.2,
		7.8, 9.0, 2.3, 4.5, 5.6, 1.2, 3.4, 4.5, 5.6,
		3.7, 5.5, 4.6, 2.5, 9.5, 4.5, 1.2, 3.4, 3.7,
		3.7, 5.5, 4.6, 2.5, 4.5, 1.2, 3.4, 3.7, 1.5,
		4.6, 2.5, 9.3, 1.9, 3.8, 2.9, 3.7, 5.5, 3.8};

  int dim = 3;
  unsigned long size_4[] = {dim,dim,dim,dim};
  unsigned long dim_4 = 4;
  Math::Tensor<double> *tA = 0;
  Math::Tensor<double> tB(size_4, dim_4);
  Math::Tensor<double> tC(size_4, dim_4);
 
  tB.setArrayData(B);
  tC.setArrayData(B);
 
  time_t start, end;
  double time_difference;
  time (&start);

  unsigned count;
  for ( count = 0; count < 250; ++count)
    {
      tA = new Math::Tensor<double>(size_4, dim_4);
      (*tA)("baji").mul(tC("abcd"),tB("dcij"));
      delete tA;
    }

  time (&end);
  time_difference =  difftime (end,start);

  int modulo_hours = static_cast<int>(time_difference) % 3600;
  int hours = (static_cast<int>(time_difference) - modulo_hours) / 3600;
  int modulo_minutes = modulo_hours % 60;
  int minutes = (modulo_hours - modulo_minutes) / 60;
  int seconds = modulo_minutes;
  std::cout << endl << endl << "Calculation needed " << hours << "h " << minutes << "min " << seconds << "s. ";

  cout << "End Speed Check" << endl;
  cout << "------------" << endl;
  return 0;
}
