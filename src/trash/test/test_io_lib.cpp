/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <libsoofea_io.h>
#include <iostream>
#include <string>

#include <exceptions.h>
#include <global.h>
#include <node.h>
#include "../element/element.h"
#include "../element/element_type.h"
#include <material.h>
#include <real_const_set.h>
#include "../condition/load.h"
#include "../condition/load_case.h"
#include "../geometry/boundary.h"
#include "../geometry/coord_sys.h"

using std::cout;
using std::endl;
using std::string;

int main(int argc, char *argv[])
{
  string input_name;
  string output_name;
  string config_file;

  cout << "Testprogramm fuer IO_lib" << endl;
  cout << "------------------------" << endl;

  if (argc < 4)
    {
      cout << "ERROR" << endl;
      cout << "program usage:" << endl;
      cout << "test_libsoofea_io <input_file_name> <output_file_name> <config_file_name>" << endl;
      return -1;
    }

  if (argc > 4)
    {
      cout << "ERROR" << endl;
      cout << "too many arguments given!" << endl;
    }


  input_name = argv[1];
  output_name = argv[2];
  config_file = argv[3];

  Math::LinAlg::createLinAlg();
  IOHandler *io_handler = new IOHandler;

  io_handler -> addInputHandler(XML);
  io_handler -> addOutputHandler(GiD);

  try
    {
      io_handler -> setInputDocument(input_name, XML);
      io_handler -> setOutputDocument(output_name, GiD);
    }
  catch(IOHandlerException& exc)
    {
      cout << "ERROR" << endl;
      cout << exc.getErrorStatement() << endl;

      delete io_handler;
      return -1;
    }

  try
    {
      XMLConfig::createXMLConfig(config_file);
    }
  catch(XMLConfigException &exc)
    {
      cout << "ERROR" << endl;
      cout << exc.getErrorStatement() << endl;
      
      delete io_handler;
      return -1;
    }


  //##################################################################
  // starting fetching data

  //------------------------------------------------------------------
  // fetch Global Data

  Global* global_data = new Global;
  io_handler -> getActualInputHandler() -> get(global_data);

  cout << "Problem Dimension: " << global_data -> getDimension() << endl;
  cout << "CoordSysType: " << global_data -> getCoordSysType() << endl;

  CoordSys* coord_sys = 0;

  // We have to create the world CoordSys
  if( global_data -> getCoordSysType() == CoordSys::CARTESIAN_ID)
    {
      switch( global_data -> getDimension() )
	{
	case 2:
	  coord_sys = new Cartesian2D;
	  break;
	case 3:
	  coord_sys = new Cartesian3D;
	  break;
	default:
	  break;
	}
    }

  delete global_data;

  //------------------------------------------------------------------
  // fetch Nodes

  InputAggregate<Node*> *node_aggregate = new InputAggregate<Node*>(io_handler);
  InputAggregate<Node*>::iterator node_iter = node_aggregate -> begin();
  
  Node* node = 0;

  for( ; node_iter !=  node_aggregate -> end(); node_iter++)
    {
      node = node_aggregate -> get(node_iter);
      node -> setCoordSys(coord_sys);
      node -> prettyPrint();
      delete node;
    }
    
  delete node_aggregate;
    
  //------------------------------------------------------------------
  // fetch Elements

  InputAggregate<Element*> *element_aggregate = new InputAggregate<Element*>(io_handler);
  InputAggregate<Element*>::iterator element_iter = element_aggregate -> begin();
  
  Element* element = 0;

  for( ; element_iter !=  element_aggregate -> end(); element_iter++)
    {
      element = element_aggregate -> get(element_iter);
      element -> prettyPrint();
      delete element;
    }

  delete element_aggregate;

  //------------------------------------------------------------------
  // fetch Material

  InputAggregate<Material*> *material_aggregate = new InputAggregate<Material*>(io_handler);
  InputAggregate<Material*>::iterator material_iter = material_aggregate -> begin();

  Material* material = 0;

  for( ; material_iter !=  material_aggregate -> end(); material_iter++)
    {
      material = material_aggregate -> get(material_iter);
      material -> prettyPrint();
      delete material;
    }
  
  delete material_aggregate;

  //------------------------------------------------------------------
  // fetch ElementTypes

  InputAggregate<ElementType*> *element_type_aggregate = new InputAggregate<ElementType*>(io_handler);
  InputAggregate<ElementType*>::iterator element_type_iter = element_type_aggregate -> begin();

  ElementType* element_type = 0;

  for( ; element_type_iter !=  element_type_aggregate -> end(); element_type_iter++)
    {
      element_type = element_type_aggregate -> get(element_type_iter);
      element_type -> prettyPrint();
      delete element_type;
    }

  delete element_type_aggregate;

  //------------------------------------------------------------------
  // fetch Constraints

  cout << "--- Constraints --- " << endl;
  InputAggregate<Constraint*> *constraint_aggregate = new InputAggregate<Constraint*>(io_handler);
  InputAggregate<Constraint*>::iterator constraint_iter = constraint_aggregate -> begin();

  Constraint* constraint = 0;

  for( ; constraint_iter !=  constraint_aggregate -> end(); constraint_iter++)
    {
      constraint = constraint_aggregate -> get(constraint_iter);
      constraint -> prettyPrint();
      delete constraint;
    }
  
  delete constraint_aggregate;
  cout << "--- End Constraints --- " << endl;

  //------------------------------------------------------------------
  // fetch Boundary Conditions

  cout << "--- Boundary Conditions --- " << endl;
  InputAggregate<Constraint*> *bc_aggregate = new InputAggregate<Constraint*>(io_handler);
  InputAggregate<Constraint*>::iterator bc_iter = bc_aggregate -> begin();

  Constraint* bc = 0;

  for( ; bc_iter !=  bc_aggregate -> end(); bc_iter++)
    {
      bc = bc_aggregate -> get(bc_iter);
      bc -> prettyPrint();
      delete bc;
    }
  
  delete bc_aggregate;
  cout << "--- End Boundary Conditions --- " << endl;

  //------------------------------------------------------------------
  // fetch Loads

  InputAggregate<Load*> *load_aggregate = new InputAggregate<Load*>(io_handler);
  InputAggregate<Load*>::iterator load_iter = load_aggregate -> begin();

  Load* load = 0;

  for( ; load_iter !=  load_aggregate -> end(); load_iter++)
    {
      load = load_aggregate -> get(load_iter);
      load -> prettyPrint();
      delete load;
    }

  delete load_aggregate;

  //------------------------------------------------------------------
  // fetch Load Cases

  InputAggregate<LoadCase*> *load_case_aggregate = new InputAggregate<LoadCase*>(io_handler);
  InputAggregate<LoadCase*>::iterator load_case_iter = load_case_aggregate -> begin();

  LoadCase* load_case = 0;

  for( ; load_case_iter !=  load_case_aggregate -> end(); load_case_iter++)
    {
      load_case = load_case_aggregate -> get(load_case_iter);
      load_case -> prettyPrint();
      delete load_case;
    }

  delete load_case_aggregate;

  //------------------------------------------------------------------
  // fetch RealConstSets

  InputAggregate<RealConstSet*> *real_const_set_aggregate = new InputAggregate<RealConstSet*>(io_handler);
  InputAggregate<RealConstSet*>::iterator real_const_set_iter = real_const_set_aggregate -> begin();

  RealConstSet* real_const_set = 0;

  for( ; real_const_set_iter !=  real_const_set_aggregate -> end(); real_const_set_iter++)
    {
      real_const_set = real_const_set_aggregate -> get(real_const_set_iter);
      real_const_set -> prettyPrint();
      delete real_const_set;
    }
  
  delete real_const_set_aggregate;

  //------------------------------------------------------------------
  // fetch Boundarys

  InputAggregate<Boundary*> *boundary_aggregate = new InputAggregate<Boundary*>(io_handler);
  InputAggregate<Boundary*>::iterator boundary_iter = boundary_aggregate -> begin();

  Boundary* boundary = 0;

  for( ; boundary_iter !=  boundary_aggregate -> end(); boundary_iter++)
    {
      boundary = boundary_aggregate -> get(boundary_iter);
      boundary -> prettyPrint();
      delete boundary;
    }
  
  delete boundary_aggregate;

  //##################################################################

  delete coord_sys;

  cout << "Program ended successfully!" << endl;

  delete io_handler;
  Math::LinAlg::terminate();
}
