/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

/* ------------------------------------------------------------------
 * This file is part of SOOFEA.
 * 
 * SOOFEA - Software for Object Oriented Finite Element Analysis
 * Copyright (C) 2007-2010 Michael Hammer
 * 
 * SOOFEA is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * $Id$
 * ------------------------------------------------------------------*/

#include <iostream>
#include <string>
#include <list>

using namespace std;

//------------------------------------------------------------------
void extractUnsignedFromString(char* string_to_parse,
			       std::list<unsigned>* unsigned_list)
{
  char* local_value_ptr = string_to_parse;
  
  unsigned_list -> push_back( (unsigned)atoi( local_value_ptr ) );

  while ( (*local_value_ptr) != '\0' )
    {
      while ( ( ('0' <= (*local_value_ptr)) && ((*local_value_ptr) <= '9')) )
	{
	  local_value_ptr++;
	}
      if ( (*local_value_ptr) == '\0' )
	break;
      local_value_ptr++;
      unsigned_list -> push_back( (unsigned)atoi( local_value_ptr ) );
    }

  return;
}

//------------------------------------------------------------------
int main(int argc, char *argv[])
{
  unsigned long elem_number = 1;
  unsigned m_number = 1;
  unsigned et_number = 1;
  char input[100];

  list<unsigned>* unsigned_list;
  list<unsigned>::iterator iter;

  std::cin.getline(input,100);

  //  cout.setf(ios_base::scientific,ios_base::floatfield);
  //  cout.precision(11);

  while(*input != '\0')
    {
      unsigned_list = new list<unsigned>;
      extractUnsignedFromString(input,unsigned_list);
 
      cout << "<e gnn=\"";

      for( iter = unsigned_list -> begin() ; iter != unsigned_list -> end() ; ++iter)
	{
	  cout << *iter << " ";
	}
      cout << "\" m=\"" << m_number << "\" et=\"" << et_number << "\">" << elem_number++ << "</e>" << endl;

      delete unsigned_list;
      std::cin.getline(input,100);
    }
}
